<?php

namespace App\Libraries;

use CodeIgniter\Test\CIUnitTestCase;
use Tools\Utilities;

class UtilitiesTest extends CIUnitTestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Quote test with default settings
     */
    public function testQuote()
    {
        $v = Utilities::quote("test");
        $expected = "'test'";

        $this->assertEquals($expected, $v);
    }

    /**
     * Quote test with custom parameter
     */
    public function testQuote2()
    {
        $v = Utilities::quote("test", "!");
        $expected = "!test!";

        $this->assertEquals($expected, $v);
    }
}
