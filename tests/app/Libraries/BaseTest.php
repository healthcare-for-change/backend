<?php

namespace App\Libraries;

use CodeIgniter\Test\CIUnitTestCase;

class BaseTest extends CIUnitTestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testBaseSetup()
    {
        // Dump test to keep this file in track:
        $this->assertEquals(true, true);
    }
}
