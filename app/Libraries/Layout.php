<?php

namespace App\Libraries;

use Config\Services;

class Layout
{
    protected $helpers = [];


    /**
     * Constructor
     */
    public function __construct()
    {

    }

    public function generateHeadMenu()
    {
        $auth = Services::auth();

        return view("navigation/head", ["auth" => $auth]);
    }

    public function generateSideMenu()
    {
        $auth = Services::auth();

        return view("navigation/side", ["auth" => $auth]);
    }

    public function generateHeader()
    {
        $auth = Services::auth();

        return view("layout/header", ["auth" => $auth]);
    }
}
