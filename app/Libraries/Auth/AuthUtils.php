<?php

namespace App\Libraries\Auth;

use CodeIgniter\Shield\Entities\User;
use Propel\Tests\Helpers\Namespaces\NamespacesTestBase;

class AuthUtils
{

    public static function create()
    {
        return new AuthUtils();
    }

    /**
     * @return integer|false `User::id` when successful. Suggestion strings when fails.
     */
    public function validateLoginAndTennant()
    {
        auth()->setAuthenticator('tokens');

        if(!auth()->loggedIn()) {
            return false;
        }

        $user = auth()->user();

        // Get Tenant:
        if(!$this->getTenant()) {
            return false;
        }

        return $user->id;
    }


    public function getTenant()
    {
        $request = service('request');
        $tenantHeader = $request->getHeader("X-Auth-Tenant");

        if($tenantHeader) {
            $tenant = $tenantHeader->getValue();
            $tenantObject = \TenantQuery::create()
                ->findOneByUuid($tenant);

            if($tenantObject instanceof \Tenant ) {
                return $tenantObject;
            }
        }

        return false;
    }


    /**
     * Get header Authorization
     * */
    private function getAuthorizationHeader()
    {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    /**
     * get access token from header
     * */
    public function getBearerToken()
    {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
}