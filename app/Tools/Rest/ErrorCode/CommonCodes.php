<?php

namespace Tools\Rest\ErrorCode;


/**
 * Contains information in common error codes that are related to the REST service framework itself.
 * Codes must by smaller than 1000.
 */
class CommonCodes
{

    /**
     * Code container
     * @var int
     */
    protected $code;

    /**
     * 400 Bad Request – client sent an invalid request, such as lacking required request body or parameter
     */
    const CODE_BAD_REQUEST = 400;

    /**
     * 401 Unauthorized – client failed to authenticate with the server
     */
    const CODE_UNAUTHORIZED = 401;

    /**
     * 403 Forbidden – client authenticated but does not have permission to access the requested resource
     */
    const CODE_FORBIDDEN = 403;

    /**
     * 404 Not Found – the requested resource does not exist
     */
    const CODE_NOT_FOUND = 404;

    /**
     * 412 Precondition Failed – one or more conditions in the request header fields evaluated to false
     */
    const CODE_PRECONDITION_FAILED = 412;

    /**
     * 420 Missing parameter - not all required parameters have been submitted
     */
    const CODE_MISSING_PARAMETERS = 420;

    /**
     * 500 Internal Server Error – a generic error occurred on the server
     */
    const CODE_INTERNAL_SERVER_ERROR = 500;

    /**
     * 503 Service Unavailable – the requested service is not available
     */
    const CODE_SERVICE_UNAVAILABLE = 503;


    /**
     * Provided error code
     *
     * @param int $code
     */
    public function __construct(int $code)
    {
        $this->code = $code;
    }

    /**
     * Returns a list of all error codes including definitions
     * @return array[]
     */
    private function getErrorInformation(): array
    {
        return [
            self::CODE_PRECONDITION_FAILED => [
                "details" => "The content type that has been provided is invalid and cannot be processed with this endpoint. Please use a proper content type to provide data.",
                "url"     => null,
            ],
            self::CODE_MISSING_PARAMETERS => [
                "details" => "Not all parameters that are needed by the server have been submitted by the client. Please ensure that all required parameters will be provided with the request.",
                "url"     => null,
            ],
            self::CODE_UNAUTHORIZED => [
                "details" => "The requested user is not allowed to access the requested resource or the user is not loggedin.",
                "url"     => null,
            ],
            self::CODE_FORBIDDEN => [
                "details" => "Access to the resource has been denied, eg. by providing incorrect CSRF tokens or insufficient access privilege.",
                "url"     => null,
            ],
        ];
    }

    /**
     * Returns the detailed description of the error code
     * @return string
     */
    public function getDetails(): string
    {
        $details          = "";
        $errorInformation = $this->getErrorInformation();

        if (isset($errorInformation[$this->code]["details"])) {
            $details = $errorInformation[$this->code]["details"];
        }

        return $details;
    }

    /**
     * Returns the support URL of the error code
     * @return string
     */
    public function getUrl(): string
    {
        $url              = "";
        $errorInformation = $this->getErrorInformation();

        if (isset($errorInformation[$this->code]["url"])) {
            $url = $errorInformation[$this->code]["url"];
        }

        return $url;
    }
}
