<?php

namespace Tools\Rest\ErrorHandler;


use Tools\Rest\ErrorCode\CommonCodes;

class MissingParameterError
{
    /**
     * Contains the structure of information
     * @var array
     */
    protected $data;


    /**
     * @param string     $message            Human readable message
     * @param array      $providedParameters Provided parameters by the request
     * @param array      $missingParameters  Missing parameters
     * @param array|null $requiredParameters Required parameters to be submitted
     * @param int|null   $code               Error Code
     */
    public function __construct(string $message, array $providedParameters, array $missingParameters, ?array $requiredParameters = null, ?int $code = null)
    {
        $this->data = [
            "message"  => $message,
            "provided" => $providedParameters,
            "missing"  => $missingParameters,
        ];

        if ($requiredParameters) {
            $this->data["required"] = $requiredParameters;
        }

        if ($code) {
            $this->data["code"] = $code;
            if ($code < 1000) {
                $codeHint             = new CommonCodes($code);
                $this->data["detail"] = $codeHint->getDetails();
                $url                  = $codeHint->getUrl();
                if ( ! empty($url)) {
                    $this->data["help"] = $url;
                }
            }
        }
    }

    public function toArray(): array
    {
        return $this->data;
    }
}
