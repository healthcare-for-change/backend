<?php

namespace Tools\Rest\ErrorHandler;


use Tools\Rest\ErrorCode\CommonCodes;

class InvalidDataError
{
    /**
     * Contains the structure of information
     * @var array
     */
    protected $data;

    /**
     * @param string     $message       Message as human readable string
     * @param mixed      $providedValue Provided value by the client
     * @param array|null $allowedValues Allowed values that are accepted, either array or string
     * @param int|null   $code          Optional code that helps to identify the error
     */
    public function __construct(string $message, mixed $providedValue, ?array $allowedValues = null, ?int $code = null)
    {
        $this->data = [
            "message"  => $message,
            "provided" => $providedValue,
        ];

        if ($allowedValues) {
            $this->data["required"] = $allowedValues;
        }

        if ($code) {
            $this->data["code"] = $code;
            if ($code < 1000) {
                $codeHint             = new CommonCodes($code);
                $this->data["detail"] = $codeHint->getDetails();
                $url                  = $codeHint->getUrl();
                if ( ! empty($url)) {
                    $this->data["help"] = $url;
                }
            }
        }
    }

    public function toArray(): array
    {
        return $this->data;
    }
}
