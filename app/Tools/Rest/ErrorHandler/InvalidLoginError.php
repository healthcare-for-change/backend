<?php

namespace Tools\Rest\ErrorHandler;


use Tools\Rest\ErrorCode\CommonCodes;

class InvalidLoginError
{
    /**
     * Contains the structure of information
     * @var array
     */
    protected $data;

    /**
     *
     */
    public function __construct()
    {
        $this->data = [
            "message" => "Access denied! Login required.",
        ];

        $this->data["code"]   = CommonCodes::CODE_UNAUTHORIZED;
        $codeHint             = new CommonCodes(CommonCodes::CODE_UNAUTHORIZED);
        $this->data["detail"] = $codeHint->getDetails();
        $url                  = $codeHint->getUrl();
        if ( ! empty($url)) {
            $this->data["help"] = $url;
        }
    }

    public function toArray(): array
    {
        return $this->data;
    }
}
