<?php

namespace Tools\Rest\ErrorHandler;


use Tools\Rest\ErrorCode\CommonCodes;

class CsrfError
{
    /**
     * Contains the structure of information
     * @var array
     */
    protected $data;

    /**
     *
     */
    public function __construct()
    {
        $this->data = [
            "message" => "Access denied! Invalid CSRF token.",
        ];

        $this->data["code"]   = CommonCodes::CODE_FORBIDDEN;
        $codeHint             = new CommonCodes(CommonCodes::CODE_FORBIDDEN);
        $this->data["detail"] = $codeHint->getDetails();
        $url                  = $codeHint->getUrl();
        if ( ! empty($url)) {
            $this->data["help"] = $url;
        }
    }

    public function toArray(): array
    {
        return $this->data;
    }
}
