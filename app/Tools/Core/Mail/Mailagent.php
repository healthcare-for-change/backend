<?php

namespace Tools\Core\Mail;

use Http\Client\Exception;
use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle7\Client as GuzzleAdapter;


class Mailagent
{

    /**
     * @var GuzzleAdapter
     */
    protected $httpClient;

    /**
     * @var SparkPost
     */
    protected $agent;

    /**
     * Array of variables as key => value
     * @var array
     */
    protected $variables = [];

    /**
     * List of CC receipients as array with "name" and "email"
     * @var array
     */
    protected $cc = [];

    /**
     * List of BCC receipients as array with "name" and "email"
     * @var array
     */
    protected $bcc = [];

    /**
     * List of Sender as array with "name" and "email"
     * @var array
     */
    protected $from = [];

    /**
     * List of attachments with "name" and "storage_filename"
     * @var array
     */
    protected $attachments = [];


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->httpClient = new GuzzleAdapter(new Client());
        $this->agent      = new SparkPost($this->httpClient, [
            'key'   => getenv("sparkpost.api.key"),
            'async' => false,
            'host'  => 'api.eu.sparkpost.com',
        ]);
    }


    /**
     * Send a text mail
     *
     * @param string     $subject
     * @param array      $to
     * @param string     $message
     * @param bool|null  $html
     * @param array|null $variables
     * @param array|null $cc
     *
     * @return array
     * @throws MailagentException
     */
    public function sendMail(
        string $subject,
        array $to,
        string $message,
        ?bool $html = true,
        ?array $variables = [],
        ?array $cc = []
    ): array {
        // Combine date to class variables:
        $this->variables = array_merge($this->variables, $variables);
        $this->cc        = array_merge($this->cc, $cc);

        if ( ! isset($this->from["email"])) {
            $this->from = [
                "name"  => getenv("sparkpost.default.sender"),
                "email" => getenv("sparkpost.default.email"),
            ];
        }

        if ($html) {
            $messageHtml = $message;
            $messageText = strip_tags($message);
        } else {
            $messageText = $message;
            $messageHtml = $message;
        }

        $recipients = $this->getReceipientsArray($to);

        $struct = [
            'content'    => [
                'from'    => $this->from,
                'subject' => $subject,
                'html'    => $messageHtml,
                'text'    => $messageText,
            ],
            'recipients' => $recipients,
        ];

        $struct = $this->addVariables($struct);
        $struct = $this->addCopyRecipients($struct);
        $struct = $this->addAttachmentsToEmail($struct);

        try {
            $response = $this->agent->transmissions->post($struct);
        } catch (Exception $exception) {
            throw new MailagentException($exception);
        }

        return $response->getBody()['results'];
    }


    /**
     * Send an email based on a registered Template
     * @param string     $templateId
     * @param array      $to
     * @param array|null $variables
     * @param array|null $cc
     *
     * @return array
     * @throws MailagentException
     */
    public function sendMailWithTemplate(
        string $templateId,
        array $to,
        ?array $variables = [],
        ?array $cc = []
    ): array {
        // Combine date to class variables:
        $this->variables = array_merge($this->variables, $variables);
        $this->cc        = array_merge($this->cc, $cc);

        if ( ! isset($this->from["email"])) {
            $this->from = [
                "name"  => getenv("sparkpost.default.sender"),
                "email" => getenv("sparkpost.default.email"),
            ];
        }

        $recipients = $this->getReceipientsArray($to);

        $struct = [
            'content'    => [
                'template_id' => $templateId,
            ],
            'recipients' => $recipients,
        ];

        $struct = $this->addVariables($struct);
        $struct = $this->addCopyRecipients($struct);
        $struct = $this->addAttachmentsToEmail($struct);

        try {
            $response = $this->agent->transmissions->post($struct);
        } catch (Exception $exception) {
            throw new MailagentException($exception);
        }

        return $response->getBody()['results'];
    }

    /**
     * Add CC and BCC recipients
     *
     * @param array $struct
     *
     * @return array
     */
    private function addCopyRecipients(array $struct): array
    {
        $recipientsCCArray = [];
        foreach ($this->cc as $name => $email) {
            $recipientsCCArray[] = [
                "address" => [
                    "name"  => $name,
                    "email" => $email,
                ],
            ];
        }

        $recipientsBCCArray = [];
        foreach ($this->bcc as $name => $email) {
            $recipientsBCCArray[] = [
                "address" => [
                    "name"  => $name,
                    "email" => $email,
                ],
            ];
        }

        if ($recipientsCCArray) {
            $struct['cc'] = $recipientsCCArray;
        }

        if ($recipientsBCCArray) {
            $struct['bcc'] = $recipientsBCCArray;
        }

        return $struct;
    }


    /**
     * Add variables to e-mail. These will replace variables in the content or template
     * @param array $struct
     *
     * @return array
     */
    private function addVariables(array $struct): array
    {
        if ($this->variables) {
            $struct['substitution_data'] = $this->variables;
        }

        return $struct;
    }


    /**
     * Add attachments to email
     * @param array $struct
     *
     * @return array
     */
    private function addAttachmentsToEmail(array $struct): array
    {
        if ($this->attachments) {
            foreach ($this->attachments as $attachment) {
                $path     = $attachment["path"];
                $fileType = mime_content_type($path);
                $data     = base64_encode(file_get_contents($path));

                $struct['attachments'][] = [
                    'name' => $attachment["filename"],
                    'type' => $fileType,
                    'data' => $data,
                ];
            }
        }

        return $struct;
    }


    /**
     * Convert $to array to structure
     *
     * @param array $to
     *
     * @return array
     */
    private function getReceipientsArray(array $to): array
    {
        $recipients = [];
        foreach ($to as $name => $email) {
            $recipients[] = [
                "address" => [
                    "name"  => $name,
                    "email" => $email,
                ],
            ];
        }

        return $recipients;
    }

    /**
     * @return array
     */
    public function getVariables(): array
    {
        return $this->variables;
    }

    /**
     * @param array $variables
     */
    public function setVariables(array $variables): void
    {
        $this->variables = $variables;
    }

    /**
     * @return array
     */
    public function getCc(): array
    {
        return $this->cc;
    }

    /**
     * @param array $cc
     */
    public function setCc(array $cc): void
    {
        $this->cc = $cc;
    }

    /**
     * Add a CC recipient to existing list
     *
     * @param string $name
     * @param string $email
     */
    public function addCc(string $name, string $email): void
    {
        $ccArray = [
            "address" => [
                "name"  => $name,
                "email" => $email,
            ],
        ];

        $this->cc[] = $ccArray;
    }

    /**
     * @return array
     */
    public function getBcc(): array
    {
        return $this->bcc;
    }

    /**
     * @param array $bcc
     */
    public function setBcc(array $bcc): void
    {
        $this->bcc = $bcc;
    }

    /**
     * Add a CC recipient to existing list
     *
     * @param string $name
     * @param string $email
     */
    public function addBcc(string $name, string $email): void
    {
        $bccArray = [
            "address" => [
                "name"  => $name,
                "email" => $email,
            ],
        ];

        $this->bcc[] = $bccArray;
    }

    /**
     * @return array
     */
    public function getFrom(): array
    {
        return $this->from;
    }

    /**
     * @param array $from
     */
    public function setFrom(array $from): void
    {
        $this->from = $from;
    }

    /**
     * @return array
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * @param array $attachments
     */
    public function setAttachments(array $attachments): void
    {
        $this->attachments = $attachments;
    }

    /**
     * Add an attachment
     * @param array       $attachment
     * @param string|null $uuid         Optional UUID in order to be able to remove the attachment afterwards again
     */
    public function addAttachment(array $attachment, ?string $uuid = null): void
    {
        if ($uuid) {
            $this->attachments[$uuid] = $attachment;
        } else {
            $this->attachments[] = $attachment;
        }
    }
}
