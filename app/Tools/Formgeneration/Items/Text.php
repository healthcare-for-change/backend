<?php

namespace Tools\Formgeneration\Items;

use Tools\Utilities;

class Text extends BaseItem
{
    private $type;

    const TYPE_TEXT = "text";
    const TYPE_PASSWORD = "password";
    const TYPE_EMAIL = "email";

    public function __construct(string $name, string $label, ?string $value = null, ?array $options = [], ?string $type = self::TYPE_TEXT)
    {
        $this->type          = $type;
        $this->name          = $name;
        $this->label         = $label;
        $this->value         = $value;

        $this->readOptions($options);

        if ($this->type === self::TYPE_PASSWORD) {
            // Force autocomplete to false for password inputs:
            $this->autocomplete = false;
        }
    }

    public function generateHtmlInline()
    {
        return '
        <div class="form-group label-floating ' . $this->class . '">
            <label class="control-label" for="' . $this->name . '">' . $this->label . '</label>
            <input class="form-control" id="' . $this->name . '" name="' . $this->name . '" type="' . $this->type . '" value="' . $this->value . '" ' . $this->disabled . '>
        </div>
        ';
    }

    public function generateHtmlRowBased()
    {
        return '
        <div class="form-group row ' . $this->class . '">
            <label for="' . $this->name . '" autocomplete="' . Utilities::booltostring($this->autocomplete) . '" class="' . $this->classRowLabel . ' control-label">' . $this->label . '</label>

            <div class="' . $this->classRowItem . '">
                <input type="' . $this->type . '" class="form-control" id="' . $this->name . '" name="' . $this->name . '" value="' . $this->value . '" placeholder="' . $this->label . '" ' . $this->disabled . '>
            </div>
        </div>
        ';
    }
}
