<?php

namespace Tools\Formgeneration\Items;

use Tools\Utilities;

class Section extends BaseItem
{

    public function __construct(string $title, ?array $options = [])
    {
        $this->title         = $title;
        $this->class         = Utilities::array_get($options, "class", null);
    }

    public function generateHtmlInline()
    {
        return '
        <div class="form-group section-container label-floating ' . $this->class . '">
            <section class="section">'.$this->title.'</section>
        </div>
        ';
    }

    public function generateHtmlRowBased()
    {
        return '
        <div class="form-group section-container row ' . $this->class . '">
            <section>'.$this->title.'</section>
        </div>
        ';
    }
}
