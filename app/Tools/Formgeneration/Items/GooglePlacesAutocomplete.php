<?php

namespace Tools\Formgeneration\Items;

class GooglePlacesAutocomplete extends BaseItem
{
    protected $googlePlacesJavascript = 'https://maps.googleapis.com/maps/api/js?key=%s&libraries=places&callback=%s';

    public function __construct(string $name, string $label, ?string $value = null, ?array $options = [])
    {
        $this->name  = $name;
        $this->label = $label;
        $this->value = $value;

        $this->readOptions($options);
    }

    public function generateHtmlInline()
    {
        $html = '
        <div class="form-group label-floating ' . $this->class . '">
            <label class="control-label" for="' . $this->name . '">' . $this->label . '</label>
            <div class="input-group">
                <input class="form-control" id="' . $this->name . '" name="' . $this->name . '" type="text" value="' . $this->value . '" ' . $this->disabled . ' autocomplete="new-password">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-fab btn-fab-mini" id="'.$this->name.'_manual_edit" onclick="displayDetails_'.$this->name.'()">
                    <i class="material-icons">edit</i>
                  </button>
                </span>
            </div>
        </div>
        <div class="detail-edit inline-details" id="detail-edit-'.$this->name.'" style="display: none;">'.$this->detailEditFields('HtmlInline').'
        </div>
        ';

        $html .= $this->addJavascript();

        return $html;
    }

    public function generateHtmlRowBased()
    {
        $html = '
        <div class="form-group row ' . $this->class . '">
            <label for="' . $this->name . '" class="' . $this->classRowLabel . ' control-label">' . $this->label . '</label>

            <div class="' . $this->classRowItem . '">
                <div class="input-group">
                    <input type="text" class="form-control" id="' . $this->name . '" name="' . $this->name . '" value="' . $this->value . '" autocomplete="new-password" placeholder="' . $this->label . '" ' . $this->disabled . '>
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-fab btn-fab-mini" id="'.$this->name.'_manual_edit">
                        <i class="material-icons">edit</i>
                      </button>
                    </span>
                </div>
            </div>
            <div class="detail-edit rowbased-details" id="detail-edit-'.$this->name.'" style="display: none;">'.$this->detailEditFields('HtmlRowBased').'
            </div>
        </div>
        ';

        $html .= $this->addJavascript();

        return $html;
    }

    private function detailEditFields(string $type) : string
    {
        $generationType = "generate".$type;
        return (new Text($this->name."_street", _("Street"), null, ["autocomplete" => "new-password"]))->{$generationType}()
                    .(new Text($this->name."_zip", _("Postal code"), null, ["autocomplete" => "new-password"]))->{$generationType}()
                    .(new Text($this->name."_city", _("City"), null, ["autocomplete" => "new-password"]))->{$generationType}()
                    .(new CountryList($this->name."_country", _("Country"), null, ["autocomplete" => "new-password"]))->{$generationType}()
                    .(new Hidden($this->name."_geo_lat"))->{$generationType}()
                    .(new Hidden($this->name."_geo_long"))->{$generationType}()
                    .(new Hidden($this->name."_google_places_id"))->{$generationType}();
    }

    public function getDelayedScriptSources()
    {
        $token        = getenv("google.places.apiToken");
        $initFunction = sprintf("initAutocomplete_%s", $this->name);
        $url          = sprintf($this->googlePlacesJavascript, $token, $initFunction);

        return [
            "googlePlaces" => $url,
        ];
    }

    public function addJavascript()
    {
        return '
            <script>
                function initAutocomplete_'.$this->name.'() {
					const input = document.getElementById("'.$this->name.'");
                    const options = {
                      fields: ["address_components", "geometry", "name", "place_id"],
                      strictBounds: false,
                      types: ["geocode", "establishment"],
                    };
                    const autocomplete = new google.maps.places.Autocomplete(input, options);
                    google.maps.event.addListener(autocomplete, "place_changed", () => {
                      let place = autocomplete.getPlace();
                      
                      jQuery("#'.$this->name.'_street").attr("value", place.address_components[1]["long_name"]+" "+place.address_components[0]["long_name"]);
                      jQuery("#'.$this->name.'_street").parent().removeClass("is-empty");
                      jQuery("#'.$this->name.'_city").val(place.address_components[2]["long_name"]);
                      jQuery("#'.$this->name.'_city").parent().removeClass("is-empty");
                      jQuery("#'.$this->name.'_zip").val(place.address_components[6]["long_name"]);
                      jQuery("#'.$this->name.'_zip").parent().removeClass("is-empty");
                      jQuery("#'.$this->name.'_country").selectpicker("val", place.address_components[5]["short_name"]);
                      jQuery("#'.$this->name.'_country").selectpicker("refresh");
                      jQuery("#'.$this->name.'_geo_lat").val(place.geometry.location.lat());
                      jQuery("#'.$this->name.'_geo_long").val(place.geometry.location.lng());
                      jQuery("#'.$this->name.'_google_places_id").val(place.place_id);
                    });
					
					setTimeout(function() {
					    // Force disable autocomplete in Chrome browsers:
					    let element = jQuery("#'.$this->name.'"); 
						element.attr("autocomplete", "new-password");
						element.attr("placeholder", "");
					}, 1000);					
                }
				
				function displayDetails_'.$this->name.'() { 
					jQuery("#detail-edit-'.$this->name.'").show();
					jQuery("#'.$this->name.'").parent().parent().hide();
				 }
            </script>
        ';
    }
}
