<?php

namespace Tools\Formgeneration\Items;

use Tools\Utilities;

class Checkbox extends BaseItem
{
    const TYPE_CHECKBOX = "checkbox";
    const TYPE_TOGGLE = "toggle";

    public function __construct(string $name, string $label, ?bool $checked = false, ?array $options = [], ?string $type = self::TYPE_CHECKBOX)
    {
        $this->type          = $type;
        $this->name          = $name;
        $this->label         = $label;
        $this->checked       = $checked ? "checked" : null;
        $this->readOptions($options);

        switch ($type) {
            case self::TYPE_CHECKBOX:
                $this->type = "checkbox";
                break;
            case self::TYPE_TOGGLE:
                $this->type = "togglebutton";
                break;
            default:
                throw new \Exception("Incorrect type submitted");
        }
    }

    public function generateHtmlInline()
    {
        return '
        <div class="form-group label-floating checkbox-type-'.$this->type.' ' . $this->class . '">
            <div class="'.$this->type.'">
                <label>
                    <input type="checkbox" class="checkbox-element" id="' . $this->name . '" name="' . $this->name . '" ' . $this->disabled . ' ' . $this->checked . '> ' . $this->label . '
                    <div class="error-container"></div>
                </label>
            </div>
        </div>
        ';
    }

    public function generateHtmlRowBased()
    {
        return '
        <div class="form-group row checkbox-type-'.$this->type.' justify-content-end row ' . $this->class . '" style="margin-top: 0;">
            <div class="' . $this->classRowItem . '">
                <div class="'.$this->type.'">
                    <label>
                        <input type="checkbox" class="checkbox-element" id="' . $this->name . '" name="' . $this->name . '" ' . $this->disabled . ' ' . $this->checked . '> ' . $this->label . '
                        <div class="error-container"></div>
                    </label>
                </div>
            </div>
        </div>
        ';
    }
}
