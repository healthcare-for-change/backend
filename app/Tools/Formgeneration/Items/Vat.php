<?php

namespace Tools\Formgeneration\Items;

use Tools\Utilities;

class Vat extends Text
{

    public function __construct(string $name, string $label, ?string $value = null, ?array $options = [])
    {

        $validations            = Utilities::array_get($options, "validations", []);
        $validations["remote"]  = "/common/formhandling/vat_validation";
        $options["validations"] = $validations;

        parent::__construct($name, $label, $value, $options, parent::TYPE_TEXT);
    }
}
