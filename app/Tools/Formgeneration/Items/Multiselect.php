<?php

namespace Tools\Formgeneration\Items;

class Multiselect extends Select
{
    public function __construct(string $name, string $label, array $values, ?string $value, ?array $options = [])
    {
        parent::__construct($name, $label, $values, $value, $options, self::TYPE_MULTI);
    }
}
