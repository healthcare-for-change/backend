<?php

namespace Tools\Formgeneration\Items;

class Email extends Text
{
    public function __construct(string $name, string $label, ?string $value = null, ?array $options = [])
    {
        parent::__construct($name, $label, $value, $options, Text::TYPE_EMAIL);
    }
}
