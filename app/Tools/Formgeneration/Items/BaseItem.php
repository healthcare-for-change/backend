<?php

namespace Tools\Formgeneration\Items;

use Tools\Utilities;

class BaseItem
{
    public function getItemValidation()
    {
        if(isset($this->validations) && count($this->validations) > 0) {
            return $this->validations;
        }

        return FALSE;
    }

    protected function readOptions($options)
    {
        $this->class         = Utilities::array_get($options, "class", null);
        $this->classRowLabel = Utilities::array_get($options, "classRowLabel", "col-lg-2");
        $this->classRowItem  = Utilities::array_get($options, "classRowItem", "col-lg-10");
        $this->autocomplete  = Utilities::array_get($options, "autocomplete", false);
        $this->disabled      = Utilities::array_get($options, "disabled", false) ? "disabled" : null;
        $this->required      = Utilities::array_get($options, "required", false);
        $this->validations   = Utilities::array_get($options, "validations", []);
        $this->placeholder   = Utilities::array_get($options, "placeholder", "");
        $this->locale        = Utilities::array_get($options, "locale", "en_US");

        if($this->required) {
            // Also add to validations to be sure:
            $this->validations["required"] = true;
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDelayedScriptSources()
    {
        return FALSE;
    }
}
