<?php

namespace Tools\Formgeneration\Items;

use Tools\Utilities;

class TextArea extends BaseItem
{

    public function __construct(string $name, string $label, ?string $value = null, ?array $options = [])
    {
        $this->name          = $name;
        $this->label         = $label;
        $this->value         = $value;

        $this->readOptions($options);
    }

    public function generateHtmlInline()
    {
        return '
        <div class="form-group label-floating ' . $this->class . '">
            <label class="control-label" for="' . $this->name . '">' . $this->label . '</label>
            <textarea class="form-control" id="' . $this->name . '" name="' . $this->name . '" rows="3" ' . $this->disabled . '>' . $this->value . '</textarea>
        </div>
        ';
    }

    public function generateHtmlRowBased()
    {
        return '
        <div class="form-group row ' . $this->class . '">
            <label for="' . $this->name . '" autocomplete="' . Utilities::booltostring($this->autocomplete) . '" class="' . $this->classRowLabel . ' control-label">' . $this->label . '</label>

            <div class="' . $this->classRowItem . '">
                <textarea class="form-control" id="' . $this->name . '" name="' . $this->name . '" rows="3" ' . $this->disabled . '>' . $this->value . '</textarea>
            </div>
        </div>
        ';
    }
}
