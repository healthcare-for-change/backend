<?php

namespace Tools\Formgeneration\Items;

use GrowthSquare\CountryQuery;
use Tools\Utilities;

class CountryList extends Select
{
    const TYPE_SINGLE = "single";
    const TYPE_MULTI = "multi";

    public function __construct(string $name, string $label, ?string $value = null, ?array $options = [], ?string $type = self::TYPE_SINGLE)
    {
        $this->readOptions($options);

        $this->values = [];
        $countries = CountryQuery::create()
            ->useI18nQuery($this->locale)
            ->orderByName()
            ->endUse()
            ->find();

        foreach($countries as $country) {
            $this->values[$country->getAlpha2()] = $country->getName();
        }

        $options["search"] = true;

        parent::__construct( $name,  $label, $this->values,  $value, $options, $type);
    }

}
