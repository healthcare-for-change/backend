<?php

namespace Tools\Formgeneration\Items;

use Tools\Utilities;

class Hidden extends BaseItem
{

    public function __construct(string $name, ?string $value = "")
    {
        $this->name = $name;
        $this->value = $value;
    }

    public function generateHtmlInline()
    {
        return '<input type="hidden" id="'.$this->name.'" name="'.$this->name.'" value="'.$this->value.'" />';
    }

    public function generateHtmlRowBased()
    {
        return '<input type="hidden" id="'.$this->name.'" name="'.$this->name.'" value="'.$this->value.'" />';
    }
}
