<?php

namespace Tools\Formgeneration\Items;

class Toggle extends Checkbox
{
    public function __construct(string $name, string $label, ?bool $checked = false, ?array $options = [])
    {
        parent::__construct($name, $label, $checked, $options, Checkbox::TYPE_TOGGLE);
    }
}
