<?php

namespace Tools\Formgeneration\Items;

use Tools\Utilities;

class DatePicker extends BaseItem
{

    public function __construct(string $name, string $label, ?string $value = null, ?array $options = [])
    {
        $this->name          = $name;
        $this->label         = $label;
        $this->value         = $value;

        $this->readOptions($options);
    }

    public function generateHtmlInline()
    {
        $html = '
        <div class="form-group label-floating ' . $this->class . '">
            <label class="control-label" for="' . $this->name . '">' . $this->label . '</label>
            <input type="text" class="form-control" id="' . $this->name . '" name="' . $this->name . '" value="' . $this->value . '" ' . $this->disabled . '>
        </div>
        ';

        $html .= $this->addJavascript();

        return $html;
    }

    public function generateHtmlRowBased()
    {
        $html = '
        <div class="form-group row ' . $this->class . '">
            <label for="' . $this->name . '" autocomplete="' . Utilities::booltostring($this->autocomplete) . '" class="' . $this->classRowLabel . ' control-label">' . $this->label . '</label>

            <div class="' . $this->classRowItem . '">
                <input type="text" class="form-control" id="' . $this->name . '" name="' . $this->name . '" value="' . $this->value . '" ' . $this->disabled . '>
            </div>
        </div>
        ';

        $html .= $this->addJavascript();

        return $html;
    }

    private function addJavascript()
    {
        return '
            <script>
                jQuery(document).ready(function() {
					jQuery("#'.$this->name.'").datepicker({
					    format: "dd.mm.yyyy",
					    autoclose: true,
					});
                });
            </script>
        ';
    }
}
