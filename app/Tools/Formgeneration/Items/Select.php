<?php

namespace Tools\Formgeneration\Items;

use Tools\Utilities;

class Select extends BaseItem
{
    private $type;

    const TYPE_SINGLE = "single";
    const TYPE_MULTI = "multi";

    public function __construct(string $name, string $label, array $values, ?string $value = null, ?array $options = [], ?string $type = self::TYPE_SINGLE)
    {
        $this->type          = $type;
        $this->name          = $name;
        $this->label         = $label;
        $this->value         = $value;
        $this->values        = $values;
        $this->multiple      = "";
        $this->search        = Utilities::array_get($options, "search", false);
        $this->readOptions($options);

        if ($this->type === self::TYPE_MULTI) {
            // Force autocomplete to false for multiselects
            $this->autocomplete = false;
            $this->multiple = "multiple";
        }
    }

    public function generateHtmlInline()
    {
        $html = '
        <div class="form-group ' . $this->class . '">
            <label class="control-label" for="' . $this->name . '">' . $this->label . '</label>
            <select class="form-control selectpicker" data-live-search="'.Utilities::booltostring($this->search).'" data-dropup-auto="false" id="' . $this->name . '" name="' . $this->name . '" type="' . $this->type . '" ' . $this->disabled . ' '.$this->multiple.'>';

        foreach($this->values as $key => $value) {
            $html .= '<option value="'.$key.'">'.$value.'</option>';
        }

        $html .= '</select></div>';

        return $html;
    }

    public function generateHtmlRowBased()
    {
        $html = '
        <div class="form-group row ' . $this->class . '">
            <label for="' . $this->name . '" autocomplete="' . Utilities::booltostring($this->autocomplete) . '" class="' . $this->classRowLabel . ' control-label">' . $this->label . '</label>

            <div class="' . $this->classRowItem . '">
                <select class="form-control selectpicker" data-live-search="'.Utilities::booltostring($this->search).'" data-dropup-auto="false" id="' . $this->name . '" name="' . $this->name . '" type="' . $this->type . '" ' . $this->disabled . ' '.$this->multiple.'>';

        foreach($this->values as $key => $value) {
            $html .= '<option id="'.$key.'">'.$value.'</option>';
        }

        $html .= '</select></div></div>';

        return $html;
    }
}
