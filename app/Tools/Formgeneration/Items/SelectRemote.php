<?php

namespace Tools\Formgeneration\Items;

use Tools\Utilities;

class SelectRemote extends BaseItem
{
    private $type;

    const TYPE_SINGLE = "single";
    const TYPE_MULTI = "multi";

    public function __construct(string $name, string $label, string $remoteUrl, ?array $value = [], ?array $options = [], ?string $type = self::TYPE_SINGLE)
    {
        $this->type          = $type;
        $this->name          = $name;
        $this->label         = $label;
        $this->value         = $value;
        $this->remoteUrl     = $remoteUrl;
        $this->multiple      = "";
        $this->readOptions($options);

        if ($this->type === self::TYPE_MULTI) {
            // Force autocomplete to false for multiselects
            $this->autocomplete = false;
            $this->multiple = "multiple";
        }
    }

    public function generateHtmlInline()
    {
        $html = '
        <div class="form-group ' . $this->class . '">
            <label class="control-label" for="' . $this->name . '">' . $this->label . '</label>
            <select class="form-control" data-dropup-auto="false" id="' . $this->name . '" name="' . $this->name . '" type="' . $this->type . '" ' . $this->disabled . ' '.$this->multiple.'>';
        $html .= '</select></div>';
        $html .= $this->generateJavascript($this->name);

        return $html;
    }

    public function generateHtmlRowBased()
    {
        $html = '
        <div class="form-group row ' . $this->class . '">
            <label for="' . $this->name . '" autocomplete="' . Utilities::booltostring($this->autocomplete) . '" class="' . $this->classRowLabel . ' control-label">' . $this->label . '</label>

            <div class="' . $this->classRowItem . '">
                <select class="form-control" data-dropup-auto="false" id="' . $this->name . '" name="' . $this->name . '" type="' . $this->type . '" ' . $this->disabled . ' '.$this->multiple.'>';
        $html .= '</select></div></div>';
        $html .= $this->generateJavascript($this->name);

        return $html;
    }

    private function generateJavascript($id)
    {
        $url = $this->remoteUrl;

        $javascript = '';
        $javascript .= "
            <script>
                jQuery(document).ready(function() {
                    jQuery('#$id').selectpicker({
                        liveSearch: true
                    })
                    .ajaxSelectPicker({
                        ajax          : {
                            url     : '$url',
                            type    : 'GET',
                            dataType: 'json',
                            data    : {
                                q: '{{{q}}}'
                            }
                        },
                        preprocessData: function (data) {
                            var i, l = data.length, array = [];
                            if (l) {
                                for (i = 0; i < l; i++) {
                                    array.push($.extend(true, data[i], {
                                        value: data[i].value,
                                        text : data[i].name
                                    }));
                                }
                            }
                            return array;
                        }
                    });";

        if(count($this->value) === 1) {
            // A predefined value has been submitted.
            // We need to append this:

            $javascript .= "
                jQuery('#$id').append('<option value=\"".array_key_first($this->value)."\" selected=\"selected\">".$this->value[array_key_first($this->value)]."</option>').selectpicker('refresh');
                jQuery('#$id').trigger('change');
            ";
        }

        $javascript .= "
                });
            </script>
        ";

        return $javascript;
    }
}
