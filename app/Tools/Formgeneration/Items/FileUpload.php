<?php

namespace Tools\Formgeneration\Items;

use Tools\Utilities;

class FileUpload extends BaseItem
{

    public function __construct(string $name, string $label, ?string $value = null, ?array $options = [])
    {
        $this->name          = $name;
        $this->label         = $label;
        $this->value         = $value;
        $this->multiple      = Utilities::array_get($options, "multiple", false);

        $this->readOptions($options);
    }

    public function generateHtmlInline()
    {
        return '
        <div class="form-group label-floating ' . $this->class . '">
            <input type="file" id="' . $this->name . '" name="' . $this->name . '" '.($this->multiple ? "multiple" : "").'>
            <div class="input-group">
                <input type="text" readonly class="form-control" id="' . $this->name . '_label" name="' . $this->name . '_label" placeholder="'.($this->placeholder ? $this->placeholder : $this->label).'">
                <span class="input-group-btn input-group-sm">
                <button type="button" class="btn btn-fab btn-fab-mini">
                  <i class="material-icons">attach_file</i>
                </button>
            </div>
        </div>
        ';
    }

    public function generateHtmlRowBased()
    {
        return '
        <div class="form-group row ' . $this->class . '">
            <input type="file" id="' . $this->name . '" name="' . $this->name . '" '.($this->multiple ? "multiple" : "").'>
            <label for="' . $this->name . '" autocomplete="' . Utilities::booltostring($this->autocomplete) . '" class="' . $this->classRowLabel . ' control-label">' . $this->label . '</label>
            <div class="' . $this->classRowItem . '">
                <div class="input-group">
                    <input type="text" readonly class="form-control" id="' . $this->name . '_label" name="' . $this->name . '_label" placeholder="'.$this->placeholder.'">
                    <span class="input-group-btn input-group-sm">
                    <button type="button" class="btn btn-fab btn-fab-mini">
                      <i class="material-icons">attach_file</i>
                    </button>
                </div>
            </div>
        </div>
        ';
    }
}
