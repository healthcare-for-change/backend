<?php

namespace Tools\Formgeneration;

use Tools\Utilities;

class Formgeneration
{
    const FORM_TYPE_INLINE = 1;
    const FORM_TYPE_ROWBASED = 2;
    const HTTP_METHOD_GET = 'GET';
    const HTTP_METHOD_PUT = 'PUT';
    const HTTP_METHOD_POST = 'POST';
    const HTTP_METHOD_DELETE = 'DELETE';

    protected $formId;
    protected $title;
    protected $autocomplete = true;
    protected $formValidations = [];
    protected $additionalJavaScript = null;
    protected $delayedJavaScriptSources = [];
    protected $httpMethod;
    protected $endpointUrl;
    protected $buttonCancel = false;
    protected $buttonCancelLabel = "Cancel";
    protected $buttonSubmitLabel = "Submit";
    protected $successUrl;
    protected $classRowItem;
    protected $alertError;
    protected $alertSuccess;

    public function __construct(?array $options = [])
    {
        $this->formId = uniqid("form_", false);
        $this->httpMethod = self::HTTP_METHOD_POST;

        $this->classRowItem  = Utilities::array_get($options, "classRowItem", "col-lg-10");
    }

    public function generateFormInline($formFields)
    {
        $html = "";
        $html .= $this->generateFormStart();

        foreach($formFields as $field) {
            $html .= $field->generateHtmlInline();
            $this->addFormValidation($field);
            $this->addDelayedJavascriptSources($field);
        }

        $html .= $this->generateFormEnd(self::FORM_TYPE_INLINE);

        return $html;
    }

    public function generateFormRowBased($formFields) : string
    {
        $html = "";
        $html .= $this->generateFormStart();

        foreach($formFields as $field) {
            $html .= $field->generateHtmlRowBased();
            $this->addFormValidation($field);
            $this->addDelayedJavascriptSources($field);
        }

        $html .= $this->generateFormEnd(self::FORM_TYPE_ROWBASED);

        return $html;
    }


    private function generateFormStart() : string
    {
        $autocompleteParameter = "";
        $html = "";
        if(!$this->autocomplete) {
            $autocompleteParameter = 'autocomplete="off"';
        }
        $html .= '<div id="'.$this->getFormId().'-container"><form id="'.$this->getFormId().'" class="form-horizontal" '.$autocompleteParameter.' method="'.$this->httpMethod.'"><fieldset>';
        $html .= csrf_field();

        if($this->title) {
            $html .= '<legend>'.$this->title.'</legend>';
        }

        return $html;
    }

    private function generateFormEnd(float $type) : string
    {
        $formEnd = $this->generateSaveFunctionality($type);
        $formEnd .= '</fieldset></form></div>';

        if(count($this->formValidations)) {
            $formEnd .= '<script>';
            $formEnd .= 'jQuery(document).ready(function() {';
            $formEnd .= 'jQuery.fn.modal.Constructor.prototype.enforceFocus = function() {};';
            $formEnd .= 'jQuery("#'.$this->getFormId().'").validate({
                    highlight: function (element) {
						jQuery(element).closest(".form-group").removeClass("has-success").addClass("has-error");
					},
					unhighlight: function (element) {
					    jQuery(element).closest(".form-group").removeClass("has-error").addClass("has-success");
					},
					errorPlacement: function(error, element) {
                       var elem = jQuery(element);
                       if (elem.hasClass("selectpicker")) {
                           element = jQuery(element).parent(); 
                           error.insertAfter(element);
                       } else {
                           if(elem.hasClass("checkbox-element")) {
                               element = jQuery(".error-container", jQuery(element).parent()); 
                               element.html(error);
                           } else {
                               error.insertAfter(element);
                           }
                       }
                    }
            });'.PHP_EOL;
            foreach($this->formValidations as $fieldName => $validation) {
                $i=0;
                $formEnd .= 'jQuery("#'.$this->getFormId().' #'.$fieldName.'").rules( "add", {';
                foreach($validation as $validate => $valid)
                {
                    if($i>0) { $formEnd .= ','; }
                    switch($validate)
                    {
                        case "remote":
                            $formEnd .= $validate.': \''.$valid.'\'';
                            break;
                        case "skip_or_fill_minimum":
                        case "pattern":
                            $formEnd .= $validate.': '. json_encode($valid);
                            break;
                        default:
                            $formEnd .= $validate.': '.$valid;
                    }
                    $i++;
                }
                $formEnd .= '});'.PHP_EOL;
                $formEnd .= 'jQuery("#'.$fieldName.'", '.$this->getFormId().').change(function() { jQuery("#'.$this->getFormId().'").validate().element("#'.$fieldName.'"); });'.PHP_EOL;
            }

            $formEnd .= '});</script>';
        }

        if($this->additionalJavaScript) {
            $formEnd .= '<script>';
            $formEnd .= $this->additionalJavaScript;
            $formEnd .= '</script>';
        }

        if($this->delayedJavaScriptSources) {
            foreach($this->delayedJavaScriptSources as $url) {
                $formEnd .= '<script async src="'.$url.'"></script>';
            }
        }

        return $formEnd;
    }

    private function addFormValidation($field) : void
    {
        if($field->getItemValidation()) {
            $this->formValidations[$field->getName()] = $field->getItemValidation();
        }
    }

    private function addDelayedJavascriptSources($field) : void
    {
        if(is_array($field->getDelayedScriptSources())) {
            $this->delayedJavaScriptSources = array_merge($this->delayedJavaScriptSources, $field->getDelayedScriptSources());
        }
    }

    private function generateSaveFunctionality(float $type) : string
    {
        $class = $type == self::FORM_TYPE_ROWBASED ? "row" : "";
        $buttonCancelStr = "";
        if($this->buttonCancel) {
            $buttonCancelStr = '<button type="button" class="btn btn-danger" id="'.$this->formId.'_submit">'.$this->buttonCancelLabel.'</button>';
        }

        $html = '
        <div class="form-group '.$class.' justify-content-end">
            <div class="'.$this->classRowItem.'">
                <button type="submit" class="btn btn-raised btn-primary" id="'.$this->formId.'_submit">'.$this->buttonSubmitLabel.'</button>
                '.$buttonCancelStr.'
            </div>
        </div>';

        // Handle forwards for success:
        $forwardSuccess = '';
        if($this->successUrl) {
            $forwardSuccess = 'location.href="'.$this->successUrl.'";';
        }

        $html .= '
        <script>
        jQuery("#'.$this->formId.'_submit").on("click", function(e, data) {
            e.preventDefault();
			if(jQuery("#'.$this->formId.'").valid()) {
				jQuery.ajax({
				    method: "'.$this->httpMethod.'",
				    url: "'.$this->endpointUrl.'",
				    data: jQuery("#'.$this->formId.'").serialize(),
				    success: function(response) {
				        let message = "";
				        if(typeof response.message != "undefined") {
				            message = response.message;
				        } else {
				            message = '.Utilities::quote($this->alertError ? $this->alertError : _("It was not possible to submit the form.")).';
				        }
				        
						Snackbar.show({
						    pos: "top-right",
						    customClass: "success",
						    text: message
						});
						
						'.$forwardSuccess.'
				    },
				    error: function(response) {
				        if(typeof response.responseJSON.refreshToken != "undefined") {
				            jQuery("input[name=\''.csrf_token().'\']", "#'.$this->formId.'").val(response.responseJSON.refreshToken);
				        }
				        
				        let message = "";
				        if(typeof response.responseJSON.message != "undefined") {
				            message = response.responseJSON.message;
				        } else {
				            message = '.Utilities::quote($this->alertError ? $this->alertError : _("It was not possible to submit the form.")).';
				        }
				   
						Snackbar.show({
						    pos: "top-right",
						    customClass: "warning",
						    text: message
						});
				    }
				});
            }
			else {
				Snackbar.show({
                    pos: "top-right",
                    customClass: "warning",
                    text: '.Utilities::quote(_("Please fill out the form completely in order to proceed.")).'
                });
			}
        });
        </script>
        ';

        return $html;
    }

    /**
     * @return string
     */
    public function getFormId(): string
    {
        return $this->formId;
    }

    /**
     * @param string $formId
     */
    public function setFormId(string $formId): void
    {
        $this->formId = $formId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return bool
     */
    public function isAutocomplete(): bool
    {
        return $this->autocomplete;
    }

    /**
     * @param bool $autocomplete
     */
    public function setAutocomplete(bool $autocomplete): void
    {
        $this->autocomplete = $autocomplete;
    }

    /**
     * @return array
     */
    public function getFormValidations(): array
    {
        return $this->formValidations;
    }

    /**
     * @param array $formValidations
     */
    public function setFormValidations(array $formValidations): void
    {
        $this->formValidations = $formValidations;
    }

    /**
     * @return null
     */
    public function getAdditionalJavaScript()
    {
        return $this->additionalJavaScript;
    }

    /**
     * @param null $additionalJavaScript
     */
    public function setAdditionalJavaScript($additionalJavaScript): void
    {
        $this->additionalJavaScript = $additionalJavaScript;
    }

    /**
     * @return string
     */
    public function getHttpMethod(): string
    {
        return $this->httpMethod;
    }

    /**
     * @param string $httpMethod
     */
    public function setHttpMethod(string $httpMethod): void
    {
        $this->httpMethod = $httpMethod;
    }

    /**
     * @return mixed
     */
    public function getEndpointUrl()
    {
        return $this->endpointUrl;
    }

    /**
     * @param mixed $endpointUrl
     */
    public function setEndpointUrl($endpointUrl): void
    {
        $this->endpointUrl = $endpointUrl;
    }

    /**
     * @return bool
     */
    public function isButtonCancel(): bool
    {
        return $this->buttonCancel;
    }

    /**
     * @param bool $buttonCancel
     */
    public function setButtonCancel(bool $buttonCancel): void
    {
        $this->buttonCancel = $buttonCancel;
    }

    /**
     * @return string
     */
    public function getButtonCancelLabel(): string
    {
        return $this->buttonCancelLabel;
    }

    /**
     * @param string $buttonCancelLabel
     */
    public function setButtonCancelLabel(string $buttonCancelLabel): void
    {
        $this->buttonCancelLabel = $buttonCancelLabel;
    }

    /**
     * @return string
     */
    public function getButtonSubmitLabel(): string
    {
        return $this->buttonSubmitLabel;
    }

    /**
     * @param string $buttonSubmitLabel
     */
    public function setButtonSubmitLabel(string $buttonSubmitLabel): void
    {
        $this->buttonSubmitLabel = $buttonSubmitLabel;
    }

    /**
     * @return mixed
     */
    public function getSuccessUrl()
    {
        return $this->successUrl;
    }

    /**
     * @param mixed $successUrl
     */
    public function setSuccessUrl($successUrl): void
    {
        $this->successUrl = $successUrl;
    }

    /**
     * @return mixed
     */
    public function getClassRowItem(): mixed
    {
        return $this->classRowItem;
    }

    /**
     * @param mixed $classRowItem
     */
    public function setClassRowItem(mixed $classRowItem): void
    {
        $this->classRowItem = $classRowItem;
    }

    /**
     * @return mixed
     */
    public function getAlertError()
    {
        return $this->alertError;
    }

    /**
     * @param mixed $alertError
     */
    public function setAlertError($alertError): void
    {
        $this->alertError = $alertError;
    }

    /**
     * @return mixed
     */
    public function getAlertSuccess()
    {
        return $this->alertSuccess;
    }

    /**
     * @param mixed $alertSuccess
     */
    public function setAlertSuccess($alertSuccess): void
    {
        $this->alertSuccess = $alertSuccess;
    }


}
