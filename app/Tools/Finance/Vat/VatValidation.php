<?php

namespace App\Tools\Finance\Vat;

class VatValidation
{
    /**
     * @var string our own VAT ID
     */
    private $ownVAT;

    const VIES_VALIDATION_URL = "https://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl";

    public function __construct()
    {
        $this->ownVAT = getenv("finance.vat.ownvat");
        $this->client = new \SoapClient(self::VIES_VALIDATION_URL);
    }

    /**
     * Validate given VAT ID
     *
     * @param $vat
     */
    public function validate(string $vat): bool
    {
        [$countryCode, $vatNumber] = $this->split($vat);
        [$requesterCountryCode, $requesterVatNumber] = $this->split($this->ownVAT);

        try {
            $this->res = $this->getClient()->checkVatApprox([
                'countryCode'          => $countryCode,
                'vatNumber'            => $vatNumber,
                'requesterCountryCode' => $requesterCountryCode,
                'requesterVatNumber'   => $requesterVatNumber,
            ]);

            if ($this->res->valid) {
                return true;
            }
        } catch (\SoapFault $e) {
            throw new VatValidationException("Verification resulted in an error.");
        }

        return false;
    }

    /**
     * Validate given VAT ID and provide the details of the verification process as result
     *
     * @param string $vat
     *
     * @return array|bool
     * @throws VatValidationException
     */
    public function validateWithDetails(string $vat): array|bool
    {
        [$countryCode, $vatNumber] = $this->split($vat);
        [$requesterCountryCode, $requesterVatNumber] = $this->split($this->ownVAT);

        try {
            $this->res = $this->getClient()->checkVatApprox([
                'countryCode'          => $countryCode,
                'vatNumber'            => $vatNumber,
                'requesterCountryCode' => $requesterCountryCode,
                'requesterVatNumber'   => $requesterVatNumber,
            ]);

            if ($this->res->valid) {
                return [
                    "vatNumber"   => $this->res->countryCode . $this->res->vatNumber,
                    "requestDate" => $this->res->requestDate,
                    "requestId"   => $this->res->requestIdentifier,
                    "traderName"  => (isset($this->res->traderName) && ! empty($this->res->traderName)) ? $this->res->traderName : null,
                ];
            }
        } catch (\SoapFault $e) {
            throw new VatValidationException("Verification resulted in an error.");
        }

        return false;
    }


    private function getClient()
    {
        if ( ! $this->client) {
            throw new VatValidationException("Verification currently not possible (EU VIES system down)");
        }

        return $this->client;
    }

    /**
     * Splits a given VAT ID into countryCode and vatNumber
     *
     * @param $vat
     *
     * @return array countryCode, vatNumber
     */
    private function split($vat)
    {
        $vat = str_replace([' ', '.', '-', ',', ', '], '', trim($vat));
        $cc  = substr($vat, 0, 2);
        $vn  = substr($vat, 2);

        return [$cc, $vn];
    }


    public static function getVatRegex()
    {
        return '/^((AT)?U[0-9]{8}|(BE)?0[0-9]{9}|(BG)?[0-9]{9,10}|(CY)?[0-9]{8}[A-Z]|(CZ)?[0-9]{8,10}|(DE)?[0-9]{9}|'
               . '(DK)?[0-9]{8}|(EE)?[0-9]{9}|(EL|GR)?[0-9]{9}|(ES)?[0-9A-Z][0-9]{7}[0-9A-Z]|(FI)?[0-9]{8}|'
               . '(FR)?[0-9A-Z]{2}[0-9]{9}|(GB)?([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3})|(HU)?[0-9]{8}|(IE)([0-9A-Z\*\+]{7}[A-Z]{1,2})|'
               . '(IT)?[0-9]{11}|(LT)?([0-9]{9}|[0-9]{12})|(LU)?[0-9]{8}|(LV)?[0-9]{11}|(MT)?[0-9]{8}|(NL)?[0-9]{9}B[0-9]{2}|'
               . '(PL)?[0-9]{10}|(PT)?[0-9]{9}|(RO)?[0-9]{2,10}|(SE)?[0-9]{12}|(SI)?[0-9]{8}|(SK)?[0-9]{10}|'
               . '((CHE)(-|\s)?|)\d{3}(\.|\s)?\d{3}(\.|\s)?\d{3}(\s)?(IVA|TVA|MWST|VAT)?)$/';
    }

    public static function validateString($vat)
    {

        $vatRegex = VatValidation::getVatRegex();

        return (bool)preg_match($vatRegex, $vat);
    }
}
