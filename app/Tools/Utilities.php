<?php

namespace Tools;

class Utilities
{
    /**
     * Converts a string to boolen by calling filter_var
     * with the FILTER_VALIDATE_BOOLEAN flag.
     * $strict is set to FALSE as default - as this ensures strtobool returns only TRUE|FALSE
     * which is the prefered result in most cases.
     *
     * Pay attention that if strict is used int values like 1 are FALSE
     *
     * @param string $value  The value to convert
     * @param bool   $strict If set to TRUE - FILTER_NULL_ON_FAILURE condition is used
     *
     * @return bool|null Returns TRUE for "1", "true", "on" and "yes".
     * If FILTER_NULL_ON_FAILURE is set, FALSE is returned only for "0", "false", "off", "no", and "", and NULL
     * is returned for all non-boolean values.
     * Returns FALSE otherwise.
     */
    public static function strtobool($value, $strict = false)
    {
        // ensures that all forms of 'YES', 'Yes', 'yEs' ... are validated correctly.
        //this is handled by filter_var
        // if(is_string($value))
        //     $value = strtolower($value);

        //FILTER_VALIDATE_BOOLEAN requires the options to be passed without array - have tested this - won't work 2017
        if ($strict) {
            $result = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        } else {
            $result = filter_var($value, FILTER_VALIDATE_BOOLEAN);
        }

        return $result;
    }


    /**
     * Will return TRUE or FALSE to a human readable string
     *
     * @param bool $value     Value to be converted
     * @param bool $uppercase Set to true if result should be uppercase
     *
     * @return string
     */
    public static function booltostring($value, bool $uppercase = false)
    {
        if ($value) {
            return $uppercase ? "TRUE" : "true";
        }

        return $uppercase ? "FALSE" : "false";
    }


    /**
     * Return an array value, and a fallback if the given key does not exist.
     *
     * In effect, calling
     *
     *     array_get($array, 'mykey', '08/15')
     *
     * is the same as
     *
     *     array_key_exists('mykey', $array) ? $my_array['mykey'] : '08/15'
     *
     * @param array $array
     * @param mixed $key
     * @param mixed $default The value to return if `$key` is not an
     *                       existing key of `$array`.
     *                       (Default: `null`).
     *
     * @return mixed `$array[$key]` if `$key` is an existing key of
     *               `$array`, and `$default` otherwise.
     */
    public static function array_get(array $array, $key, $default = null)
    {
        if (array_key_exists($key, $array)) {
            return $array[$key];
        }

        return $default;
    }

    /**
     * Return an object property value, and a fallback if the given key does not exist.
     *
     * In effect, calling
     *
     *     object_get($object, 'mykey', '08/15')
     *
     * is the same as
     *
     *     isset($object->mykey) ? $object->mykey : '08/15'
     *
     * @param        $object
     * @param string $key
     * @param mixed  $default The value to return if `$key` is not an
     *                        existing key of `$array`.
     *                        (Default: `null`).
     *
     * @return mixed `$object->key` if `key` is an existing key of
     *               `$object`, and `$default` otherwise.
     */
    public static function object_get($object, $key, $default = null)
    {
        if (isset($object->{$key})) {
            return $object->{$key};
        }

        return $default;
    }


    /**
     * Includes a string into quotes in order to handle JavaScript-Quoting issues.
     *
     * @param string $string
     * @param string $quote
     *
     * @return string
     */
    public static function quote(string $string, string $quote = "'"): string
    {
        $string = str_replace($quote, '\\' . $quote, $string);

        return $quote . $string . $quote;
    }

}
