<?php

namespace App\Tools\Subscriber;

class Avatar
{
    protected $session;

    public function __construct()
    {
        $config = new \Config\App();
        $this->session = \Config\Services::session($config);
    }

    public function getAvatarUrl()
    {
        $email = $this->session->get("subscriberInfo")["email"];

        return 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($email))).'?s=160'; //NOSONAR
    }

    public static function getAvatar()
    {
        $instance = new Avatar();
        return $instance->getAvatarUrl();
    }
}
