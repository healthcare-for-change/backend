<?php

namespace App\Controllers\Api;

use App\Controllers\RestController;
use App\Libraries\Auth\AuthUtils;
use Map\PrescriptionTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Propel;
use Ramsey\Uuid\Uuid;

class Prescription extends RestController
{
    public function history_get($patient_uuid, $pageNo = 0, $rowsPerPage = ROWS_PER_PAGE)
    {
        try {
            $data = [];
            $user = AuthUtils::create()->validateLoginAndTennant();
            if (!$user) {
                $this->respondAccessDenied();
            }

            $totalItems = \PrescriptionQuery::create()
                ->filterByPatientUuid($patient_uuid)
                ->count();

            $prescriptionItems = \PrescriptionQuery::create()
                ->filterByPatientUuid($patient_uuid)
                ->orderByCreatedAt(Criteria::DESC)
                ->leftJoinMedication()
                ->leftJoinMedicationAdministrationMethod()
                ->leftJoinUser()
                ->paginate($pageNo, $rowsPerPage);

            foreach ($prescriptionItems as $prescriptionItem) {
                $data[] = $this->generateRecordArray($prescriptionItem);
            }

            $this->response([
                "count"         => $totalItems,
                "total_pages"   => ceil($totalItems / $rowsPerPage),
                "pageno"        => $pageNo,
                "rows_per_page" => $rowsPerPage,
                "rows"          => $data
            ]);

        } catch (\Exception $exception) {
            log_message("error", "Exception while reading prescription history. Exception: " . $exception->getMessage());
            $this->response([]);
        }
    }

    public function details_get($uuid)
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        $prescription = \PrescriptionQuery::create()
            ->filterByUuid($uuid)
            ->findOne();

        if (!$prescription instanceof \Prescription) {
            $this->respondNotFound();
        }

        $data = $this->generateRecordArray($prescription);

        $this->response($data);
    }


    public function record_post()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(PrescriptionTableMap::DATABASE_NAME);

            $record = new \Prescription();
            $record
                ->setUuid(Uuid::uuid4())
                ->setUserId($user);

            $record = $this->updateData($record);

            $record->save($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not save prescription information caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save prescription data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    public function record_put()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(PrescriptionTableMap::DATABASE_NAME);

            $record = \PrescriptionQuery::create()
                ->filterByUuid($this->get("uuid"))
                ->findOne($con);

            if (!$record) {
                $this->response(["status" => "error", "message" => "Record not found!"], 500);
            }

            $record->setUserId($user);
            $record = $this->updateData($record);

            $record->save($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not save prescription data caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save prescription data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    public function record_delete()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(PrescriptionTableMap::DATABASE_NAME);

            $record = \PrescriptionQuery::create()
                ->filterByUuid($this->get("uuid"))
                ->findOne($con);

            if (!$record) {
                $this->response(["status" => "error", "message" => "Record not found!"], 500);
            }

            $record->delete($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not delete prescription data caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not delete prescription data."], 500);
        }

        $this->response(["status" => "success"]);
    }


    private function generateRecordArray(\Prescription $prescriptionItem): array
    {
        return [
            "uuid"                      => $prescriptionItem->getUuid(),
            "medication_uuid"           => $prescriptionItem->getMedicationUuid(),
            "medication_name"           => $prescriptionItem->getMedication() ? $prescriptionItem->getMedication()->getName() : null,
            "created_at"                => $prescriptionItem->getCreatedAt("Y-m-d"),
            "updated_at"                => $prescriptionItem->getUpdatedAt("Y-m-d"),
            "dose_quantity"             => $prescriptionItem->getDoseQuantity(),
            "dose_morning"              => $prescriptionItem->getDoseMorning(),
            "dose_noon"                 => $prescriptionItem->getDoseNoon(),
            "dose_evening"              => $prescriptionItem->getDoseEvening(),
            "dose_night"                => $prescriptionItem->getDoseNight(),
            "max_dose_period"           => $prescriptionItem->getMaxDosePeriod(),
            "patient_instruction"       => $prescriptionItem->getPatientInstruction(),
            "dose_start"                => $prescriptionItem->getDoseStart("Y-m-d"),
            "dose_end"                  => $prescriptionItem->getDoseEnd("Y-m-d"),
            "interval"                  => $prescriptionItem->getInterval(),
            "user_name"                 => $prescriptionItem->getUser() ? $prescriptionItem->getUser()->getUsername() : null,
            "medication_administration" => $prescriptionItem->getMedicationAdministrationMethod() ? $prescriptionItem->getMedicationAdministrationMethod()->getDescription() : null,
            "comment"                   => $prescriptionItem->getComment(),
        ];
    }


    private function updateData(\Prescription $record): \Prescription {

        $start = !empty($this->get("dose_start")) ? strtotime($this->get("dose_start")) : null;
        $end = !empty($this->get("dose_end")) ? strtotime($this->get("dose_end")) : null;

        $record
            ->setPatientUuid($this->get("patient_uuid"))
            ->setMedicationUuid($this->get("medication_uuid"))
            ->setDoseQuantity($this->get("dose_quantity"))
            ->setDoseMorning($this->get("dose_morning"))
            ->setDoseNoon($this->get("dose_noon"))
            ->setDoseEvening($this->get("dose_evening"))
            ->setDoseNight($this->get("dose_night"))
            ->setMaxDosePeriod($this->get("max_dose_period"))
            ->setPatientInstruction($this->get("patient_instruction"))
            ->setDoseStart($start)
            ->setDoseEnd($end)
            ->setInterval($this->get("interval"))
            ->setComment($this->get("comment"));


        return $record;
    }

}