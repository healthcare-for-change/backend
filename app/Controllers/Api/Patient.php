<?php

namespace App\Controllers\Api;

use App\Controllers\RestController;
use App\Libraries\Auth\AuthUtils;
use CodeIgniter\HTTP\IncomingRequest;
use Map\PatientTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Propel;
use Ramsey\Uuid\Uuid;

class Patient extends RestController
{
    protected \Tenant $tenant;

    public function __construct()
    {
        $this->tenant = AuthUtils::create()->getTenant();
    }

    public function list_get($pageNumber = 0, $rowsPerPage = ROWS_PER_PAGE)
    {
        try {
            $data = [];

            $user = AuthUtils::create()->validateLoginAndTennant();
            if (!$user) {
                $this->respondAccessDenied();
            }

            // Get all possible patients:
            $allPatients = \PatientQuery::create()
                ->filterByTenant($this->tenant)
                ->count();

            /* @var $request IncomingRequest */
            $request = service('request');
            $filter = $request->getGet("filter");


            $rowPatients = \PatientQuery::create()
                ->filterByTenant($this->tenant);

            if (!empty($filter)) {
                // Convenience match all
                $filter = str_replace("*", "%", $filter);

                $rowPatients
                    ->filterByName($filter, Criteria::LIKE)
                    ->_or()
                    ->filterByFirstName($filter, Criteria::LIKE);
            }
            $rowPatients->paginate($pageNumber, $rowsPerPage);

            /* @var $patient \Patient */
            foreach ($rowPatients as $patient) {
                $data[] = [
                    "uuid"              => $patient->getUuid(),
                    "first_name"        => $patient->getFirstName(),
                    "name"              => $patient->getName(),
                    "gender"            => $patient->getGender(),
                    "date_of_birth"     => $patient->getDateOfBirth() ? $patient->getDateOfBirth("Y-m-d") : null,
                    "date_of_death"     => $patient->getDateOfDeath() ? $patient->getDateOfDeath("Y-m-d") : null,
                    "unclear_birthdate" => $patient->getUnclearBirthdate(),
                    "mobile_number"     => $patient->getMobileNumber(),
                    "tenant_uuid"       => $patient->getTenantUuid()
                ];
            }

            $this->response([
                "count"         => $allPatients,
                "total_pages"   => ceil($allPatients / $rowsPerPage),
                "pageno"        => $pageNumber,
                "rows_per_page" => $rowsPerPage,
                "rows"          => $data
            ]);
        } catch (\Exception $exception) {
            $this->response([]);
        }

    }

    public function details_get($uuid)
    {
        $data = [];

        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        $patient = \PatientQuery::create()
            ->filterByUuid($uuid)
            ->filterByTenant($this->tenant)
            ->findOne();

        if (!$patient instanceof \Patient) {
            $this->respondNotFound();
        }

        $age = "";
        $ageType = "years";
        if ($patient->getDateOfBirth()) {
            $age = date("Y") - $patient->getDateOfBirth("Y");

            if ($age < 2) {
                $now = new \DateTime();
                $interval = $now->diff($patient->getDateOfBirth());
                $age = (int)$interval->m + ($interval->y * 12);
                $ageType = "months";

            }
        }

        // Key medical KPIs
        $lastPhysicalExaminationDate = null;
        $lastPhysicalExaminationRecord = \MedicalHistoryQuery::create()
            ->filterByPatient($patient)
            ->orderByCreatedAt(Criteria::DESC)
            ->findOne();
        if($lastPhysicalExaminationRecord) {
            $lastPhysicalExaminationDate = $lastPhysicalExaminationRecord->getCreatedAt("Y-m-d");
        }


        $lastDewormingDate = null;
        $lastDewormingRecord = \DewormingHistoryQuery::create()
            ->filterByPatient($patient)
            ->orderByTimestamp(Criteria::DESC)
            ->findOne();
        if($lastDewormingRecord) {
            $lastDewormingDate = $lastDewormingRecord->getTimestamp("Y-m-d");
        }

        $data = [
            "uuid"                      => $patient->getUuid(),
            "first_name"                => $patient->getFirstName(),
            "name"                      => $patient->getName(),
            "gender"                    => $patient->getGender(),
            "age"                       => $age,
            "ageType"                   => $ageType,
            "date_of_birth"             => $patient->getDateOfBirth() ? $patient->getDateOfBirth("Y-m-d") : null,
            "date_of_death"             => $patient->getDateOfDeath() ? $patient->getDateOfDeath("Y-m-d") : null,
            "unclear_birthdate"         => $patient->getUnclearBirthdate(),
            "mobile_number"             => $patient->getMobileNumber(),
            "religion_id"               => $patient->getReligionId(),
            "religion_name"             => $patient->getReligion() ? $patient->getReligion()->getName() : null,
            "tenant_uuid"               => $patient->getTenantUuid(),
            "last_physical_examination" => $lastPhysicalExaminationDate,
            "last_deworming"            => $lastDewormingDate
        ];

        $this->response($data);
    }

    public function store_post()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(PatientTableMap::DATABASE_NAME);

            $patient = new \Patient();
            $patient
                ->setUuid(Uuid::uuid4())
                ->setTenant($this->tenant);

            // Update fields:
            $patient = $this->updatePatientData($patient);
            $patient->save($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not save patient data during exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save patient data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    public function store_put()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(PatientTableMap::DATABASE_NAME);

            $patient = \PatientQuery::create()
                ->filterByUuid($this->get("uuid"))
                ->filterByTenant($this->tenant)
                ->findOne($con);

            if (!$patient) {
                $this->response(["status" => "error", "message" => "Patient not found!"], 500);
            }

            // Update fields:
            $patient = $this->updatePatientData($patient);
            $patient->save($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not save patient data during exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save patient data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    /**
     * Update patient fields from incomming REST request
     * @param \Patient $patient
     * @return \Patient
     * @throws PropelException
     */
    private function updatePatientData(\Patient $patient)
    {
        // Prepare data:
        $date_of_birth = !empty($this->get("date_of_birth")) ? strtotime($this->get("date_of_birth")) : null;
        $date_of_death = !empty($this->get("date_of_death")) ? strtotime($this->get("date_of_death")) : null;

        $patient
            ->setName($this->get("name"))
            ->setFirstName($this->get("first_name"))
            ->setGender($this->get("gender"))
            ->setDateOfBirth($date_of_birth)
            ->setUnclearBirthdate($this->get("unclear_birthday"))
            ->setDateOfDeath($date_of_death)
            ->setMobileNumber($this->get("mobile_number"))
            ->setReligionId($this->get("religion_id"));

        return $patient;
    }

    public function chartdata_gender_get()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        $male = \PatientQuery::create()->filterByGender(\Patient::GENDER_MALE)->count();
        $female = \PatientQuery::create()->filterByGender(\Patient::GENDER_FEMALE)->count();
        $diverse = \PatientQuery::create()->filterByGender(\Patient::GENDER_FEMALE)->count();

        $data = [
            [
                "value" => $male,
                "name"  => 'Male'
            ],
            [
                "value" => $female,
                "name"  => 'Female'
            ],
            [
                "value" => $diverse,
                "name"  => 'Diverse'
            ],
        ];

        $this->response($data);
    }
}