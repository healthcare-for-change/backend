<?php

namespace App\Controllers\Api;

use App\Controllers\RestController;
use App\Libraries\Auth\AuthUtils;
use Propel\Runtime\ActiveQuery\Criteria;

class Medication extends RestController
{
    protected int $items_per_page = 20;

    public function autocomplete_list_get()
    {
        try {
            $data = [];

            $user = AuthUtils::create()->validateLoginAndTennant();
            if (!$user) {
                $this->respondAccessDenied();
            }

            $medications = \MedicationQuery::create()
                ->limit($this->items_per_page)
                ->filterByActive(true)
                ->orderByName();

            if(!empty($this->get("q"))) {
                $medications
                    ->filterByName($this->get("q")."%", Criteria::ILIKE)
                    ->_or()
                    ->filterByCode($this->get("q")."%", Criteria::ILIKE);

                if(is_numeric($this->get("q"))) {
                    $medications
                        ->_or()
                        ->filterByMedicationDoseFormId($this->get("q"));
                }
            }

            foreach ($medications->find() as $medication) {
                $data[] = [
                    "id"     => $medication->getUuid(),
                    "name"   => $medication->getName()
                ];
            }

            $this->response($data);
        } catch (\Exception $exception) {
            log_message("error", "Error while reading medications. Exception: " . $exception->getMessage());
            $this->response([]);
        }
    }

}