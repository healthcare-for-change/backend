<?php

namespace App\Controllers\Api;

use App\Controllers\RestController;
use App\Libraries\Auth\AuthUtils;
use Map\MedicalHistoryTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Propel;
use Ramsey\Uuid\Uuid;

class Medical extends RestController
{
    public function history_get($patient_uuid, $pageNo = 0, $rowsPerPage = ROWS_PER_PAGE)
    {
        try {
            $data = [];
            $user = AuthUtils::create()->validateLoginAndTennant();
            if (!$user) {
                $this->respondAccessDenied();
            }

            $totalItems = \MedicalHistoryQuery::create()
                ->filterByPatientUuid($patient_uuid)
                ->count();

            $medicalHistoryItems = \MedicalHistoryQuery::create()
                ->orderByCreatedAt(Criteria::DESC)
                ->leftJoinUser()
                ->leftJoinMedicalDiagnosisCode()
                ->filterByPatientUuid($patient_uuid);

            if($this->get("active")) {
                $medicalHistoryItems
                    ->filterByClinicalStatus(\MedicalHistory::CLINICAL_STATUS_ACTIVE);
            }

            foreach ($medicalHistoryItems->paginate($pageNo, $rowsPerPage) as $medicalHistory) {
                $data[] = [
                    "uuid"                          => $medicalHistory->getUuid(),
                    "patient_uuid"                  => $medicalHistory->getPatientUuid(),
                    "created_at"                    => $medicalHistory->getCreatedAt("Y-m-d"),
                    "updated_at"                    => $medicalHistory->getUpdatedAt("Y-m-d"),
                    "clinical_status"               => $medicalHistory->getClinicalStatus(),
                    "clinical_status_name"          => $medicalHistory->getClinicalStatusName(),
                    "verification_status"           => $medicalHistory->getVerificationStatus(),
                    "verification_status_name"      => $medicalHistory->getVerificationStatusName(),
                    "severity"                      => $medicalHistory->getSeverity(),
                    "severity_name"                 => $medicalHistory->getSeverityName(),
                    "onset_date"                    => $medicalHistory->getOnsetDate("Y-m-d"),
                    "onset_age"                     => $medicalHistory->getOnsetAge(),
                    "abatement_date"                => $medicalHistory->getAbatementDate("Y-m-d"),
                    "abatement_age"                 => $medicalHistory->getAbatementAge(),
                    "medical_diagnosis_code_id"     => $medicalHistory->getMedicalDiagnosisCodeId(),
                    "medical_diagnosis_description" => $medicalHistory->getMedicalDiagnosisCodeId() ? $medicalHistory->getMedicalDiagnosisCode()->getDescription() : null,
                    "user_name"                     => $medicalHistory->getUser() ? $medicalHistory->getUser()->getUsername() : null,
                    "note"                          => $medicalHistory->getNote()
                ];
            }
            $this->response([
                "count"         => $totalItems,
                "total_pages"   => ceil($totalItems / $rowsPerPage),
                "pageno"        => $pageNo,
                "rows_per_page" => $rowsPerPage,
                "rows"          => $data
            ]);

        } catch (\Exception $exception) {
            log_message("error", "Exception while reading deworming history. Exception: " . $exception->getMessage());
            $this->response([]);
        }
    }


    public function details_get($uuid)
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        $medicalHistory = \MedicalHistoryQuery::create()
            ->filterByUuid($uuid)
            ->findOne();

        if (!$medicalHistory instanceof \MedicalHistory) {
            $this->respondNotFound();
        }

        $data = [
            "uuid"                          => $medicalHistory->getUuid(),
            "patient_uuid"                  => $medicalHistory->getPatientUuid(),
            "created_at"                    => $medicalHistory->getCreatedAt("Y-m-d"),
            "updated_at"                    => $medicalHistory->getUpdatedAt("Y-m-d"),
            "clinical_status"               => $medicalHistory->getClinicalStatus(),
            "clinical_status_name"          => $medicalHistory->getClinicalStatusName(),
            "verification_status"           => $medicalHistory->getVerificationStatus(),
            "verification_status_name"      => $medicalHistory->getVerificationStatusName(),
            "severity"                      => $medicalHistory->getSeverity(),
            "severity_name"                 => $medicalHistory->getSeverityName(),
            "onset_date"                    => $medicalHistory->getOnsetDate("Y-m-d"),
            "onset_age"                     => $medicalHistory->getOnsetAge() ?: null,
            "abatement_date"                => $medicalHistory->getAbatementDate("Y-m-d"),
            "abatement_age"                 => $medicalHistory->getAbatementAge() ?: null,
            "medical_diagnosis_code_id"     => $medicalHistory->getMedicalDiagnosisCodeId(),
            "medical_diagnosis_description" => $medicalHistory->getMedicalDiagnosisCodeId() ? $medicalHistory->getMedicalDiagnosisCode()->getDescription() : null,
            "user_name"                     => $medicalHistory->getUser() ? $medicalHistory->getUser()->getUsername() : null,
            "note"                          => $medicalHistory->getNote()
        ];

        $this->response($data);
    }


    public function record_post()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(MedicalHistoryTableMap::DATABASE_NAME);

            $record = new \MedicalHistory();
            $record
                ->setUuid(Uuid::uuid4())
                ->setPatientUuid($this->get("patient_uuid"))
                ->setUserId($user);

            $record = $this->updateData($record);

            $record->save($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not save medical history information caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save medical history data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    public function record_put()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(MedicalHistoryTableMap::DATABASE_NAME);

            $record = \MedicalHistoryQuery::create()
                ->filterByUuid($this->get("uuid"))
                ->findOne($con);

            if (!$record) {
                $this->response(["status" => "error", "message" => "Record not found!"], 500);
            }

            $record->setUserId($user);
            $record = $this->updateData($record);

            $record->save($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not save medical history data caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save medical history data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    public function record_delete()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(MedicalHistoryTableMap::DATABASE_NAME);

            $record = \MedicalHistoryQuery::create()
                ->filterByUuid($this->get("uuid"))
                ->findOne($con);

            if (!$record) {
                $this->response(["status" => "error", "message" => "Record not found!"], 500);
            }

            $record->delete($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not delete medical history data caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not delete medical data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    private function updateData(\MedicalHistory $record): \MedicalHistory
    {
        $onsetDate = !empty($this->get("onset_date")) ? strtotime($this->get("onset_date")) : null;
        $abatementDate = !empty($this->get("abatement_date")) ? strtotime($this->get("abatement_date")) : null;

        $record->setClinicalStatus($this->get("clinical_status"))
            ->setVerificationStatus($this->get("verification_status"))
            ->setSeverity($this->get("severity"))
            ->setNote($this->get("note"))
            ->setOnsetAge($this->get("onset_age"))
            ->setOnsetDate($onsetDate)
            ->setAbatementAge($this->get("abatement_age"))
            ->setAbatementDate($abatementDate)
            ->setMedicalDiagnosisCodeId($this->get("medical_diagnosis_code_id") ?: null);

        return $record;
    }
}