<?php

namespace App\Controllers\Api;

use App\Controllers\RestController;
use App\Libraries\Auth\AuthUtils;
use Propel\Runtime\ActiveQuery\Criteria;

class Medical_diagnosis extends RestController
{
    protected int $items_per_page = 20;

    public function autocomplete_list_get()
    {
        try {
            $data = [];

            $user = AuthUtils::create()->validateLoginAndTennant();
            if (!$user) {
                $this->respondAccessDenied();
            }

            $medicalDiagnosisItems = \MedicalDiagnosisCodeQuery::create()
                ->limit($this->items_per_page)
                ->orderByDescription();

            if(!empty($this->get("q"))) {
                $medicalDiagnosisItems
                    ->filterByDescription("%".$this->get("q")."%", Criteria::ILIKE);

                if(is_numeric($this->get("q"))) {
                    $medicalDiagnosisItems
                        ->_or()
                        ->filterById($this->get("q"));
                }
            }

            foreach ($medicalDiagnosisItems->find() as $diagnose) {
                $data[] = [
                    "id"     => $diagnose->getId(),
                    "name"   => $diagnose->getDescription()
                ];
            }

            $this->response($data);
        } catch (\Exception $exception) {
            log_message("error", "Error while reading medications. Exception: " . $exception->getMessage());
            $this->response([]);
        }
    }

}