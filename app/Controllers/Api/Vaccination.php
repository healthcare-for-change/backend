<?php

namespace App\Controllers\Api;

use App\Controllers\RestController;
use App\Libraries\Auth\AuthUtils;
use Map\VaccinationHistoryTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Propel;
use Ramsey\Uuid\Uuid;

class Vaccination extends RestController
{
    public function history_get($patient_uuid, $pageNo = 0, $rowsPerPage = ROWS_PER_PAGE)
    {
        try {
            $data = [];
            $user = AuthUtils::create()->validateLoginAndTennant();
            if (!$user) {
                $this->respondAccessDenied();
            }

            $totalItems = \VaccinationHistoryQuery::create()
                ->filterByPatientUuid($patient_uuid)
                ->count();

            $vaccinationHistoryItems = \VaccinationHistoryQuery::create()
                ->orderByTimestamp(Criteria::DESC)
                ->leftJoinVaccine()
                ->leftJoinUser()
                ->filterByPatientUuid($patient_uuid)
                ->paginate($pageNo, $rowsPerPage);

            foreach ($vaccinationHistoryItems as $vaccinationHistory) {
                $data[] = [
                    "uuid"         => $vaccinationHistory->getUuid(),
                    "timestamp"    => $vaccinationHistory->getTimestamp("Y-m-d"),
                    "vaccine_name" => $vaccinationHistory->getVaccine() ? $vaccinationHistory->getVaccine()->getName() : null,
                    "user_name"    => $vaccinationHistory->getUser() ? $vaccinationHistory->getUser()->getUsername() : null,
                    "comment"      => $vaccinationHistory->getComment(),
                    "lot"          => $vaccinationHistory->getLot()
                ];
            }

            $this->response([
                "count"         => $totalItems,
                "total_pages"   => ceil($totalItems / $rowsPerPage),
                "pageno"        => $pageNo,
                "rows_per_page" => $rowsPerPage,
                "rows"          => $data
            ]);

        } catch (\Exception $exception) {
            log_message("error", "Exception while reading vaccination history. Exception: " . $exception->getMessage());
            $this->response([]);
        }
    }

    public function details_get($uuid)
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        $vaccinationHistory = \VaccinationHistoryQuery::create()
            ->filterByUuid($uuid)
            ->findOne();

        if (!$vaccinationHistory instanceof \VaccinationHistory) {
            $this->respondNotFound();
        }

        $data = [
            "uuid"         => $vaccinationHistory->getUuid(),
            "patient_uuid" => $vaccinationHistory->getPatientUuid(),
            "timestamp"    => $vaccinationHistory->getTimestamp("Y-m-d"),
            "vaccine_uuid" => $vaccinationHistory->getVaccineUuid(),
            "lot"          => $vaccinationHistory->getLot(),
            "user_name"    => $vaccinationHistory->getUser() ? $vaccinationHistory->getUser()->getUsername() : null,
            "comment"      => $vaccinationHistory->getComment()
        ];

        $this->response($data);
    }


    public function record_post()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(VaccinationHistoryTableMap::DATABASE_NAME);

            $record = new \VaccinationHistory();
            $record
                ->setUuid(Uuid::uuid4())
                ->setTimestamp(new \DateTime())
                ->setPatientUuid($this->get("patient_uuid"))
                ->setComment($this->get("comment"))
                ->setLot($this->get("lot"))
                ->setVaccineUuid($this->get("vaccine_uuid") ?: null)
                ->setUserId($user);

            $record->save($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not save vaccination information caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save vaccination data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    public function record_put()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(VaccinationHistoryTableMap::DATABASE_NAME);

            $record = \VaccinationHistoryQuery::create()
                ->filterByUuid($this->get("uuid"))
                ->findOne($con);

            if (!$record) {
                $this->response(["status" => "error", "message" => "Record not found!"], 500);
            }

            $record
                ->setTimestamp(new \DateTime())
                ->setPatientUuid($this->get("patient_uuid"))
                ->setComment($this->get("comment"))
                ->setLot($this->get("lot"))
                ->setVaccineUuid($this->get("vaccine_uuid") ?: null)
                ->setUserId($user);

            $record->save($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not save vaccination data caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save vaccination data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    public function record_delete()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(VaccinationHistoryTableMap::DATABASE_NAME);

            $record = \VaccinationHistoryQuery::create()
                ->filterByUuid($this->get("uuid"))
                ->findOne($con);

            if (!$record) {
                $this->response(["status" => "error", "message" => "Record not found!"], 500);
            }

            $record->delete($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not delete vaccination data caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save vaccination data."], 500);
        }

        $this->response(["status" => "success"]);
    }

}