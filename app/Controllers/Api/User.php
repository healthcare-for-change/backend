<?php

namespace App\Controllers\Api;

use App\Controllers\RestController;

class User extends RestController
{
    public function logout_post()
    {
        try {
            auth()->setAuthenticator('tokens');
            $user = auth()->check(["token" => $this->getBearerToken()]);
            if ($user) {
                // Revoke tokens and logout user:
                $userRecord = auth()->user();
                if ($userRecord) {
                    $userRecord->revokeAllAccessTokens();
                }
                auth()->logout();
            }

            $this->response([]);
        } catch (\Exception $exception) {
            $this->response([]);
        }

    }

    public function login_post()
    {
        $credentials = [
            'username' => $this->get("username"),
            'password' => $this->get('password')
        ];

        try {
            $loggedIn = auth()->loggedIn();
            if($loggedIn) {
                // User seems to be loggedin from any other sesseion. We will disconnect anything else:
                auth()->logout();
            }
            $isValid = auth()->attempt($credentials);

            if ($isValid->isOK()) {

                // We are now generating an access token to further API access
                $user = auth()->user();

                $userInfo = \UserQuery::create()
                    ->findOneById(user_id());

                $tenant = $userInfo->getTenants()->getFirst();
                $tenantUuid = $tenant->getUuid();

                // Revoke current tokens and create a new one with this login:
                $user->revokeAllAccessTokens();
                $token = $user->generateAccessToken('SessionAccess');

                $this->response([
                    "status" => "success",
                    "token" => $token->raw_token,
                    "username" => $isValid->extraInfo()->username,
                    "tenant" => $tenantUuid,
                    "extraInfo" => $isValid->extraInfo()
                ]);
            } else {
                $this->response([
                    "status" => "failure",
                    "token" => "",
                    "username" => "",
                    "extraInfo" => $isValid->reason()
                ], 403);
            }
        } catch (\Exception $exception) {
            $this->response([
                "status" => "failure",
                "token" => "",
                "username" => "",
                "extraInfo" => ""
            ], 403);
        } catch (\TypeError $exception) {
            $this->response([
                "status" => "failure",
                "token" => "",
                "username" => "",
                "extraInfo" => "Identity issue."
            ], 403);
        }
    }
}