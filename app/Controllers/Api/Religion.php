<?php

namespace App\Controllers\Api;

use App\Controllers\RestController;
use App\Libraries\Auth\AuthUtils;
use Propel\Runtime\ActiveQuery\Criteria;

class Religion extends RestController
{
    protected int $items_per_page = 20;

    public function autocomplete_list_get()
    {
        try {
            $data = [];

            $user = AuthUtils::create()->validateLoginAndTennant();
            if (!$user) {
                $this->respondAccessDenied();
            }

            $religions = \ReligionQuery::create()
                ->limit($this->items_per_page)
                ->orderByName();

            if(!empty($this->get("q"))) {
                $religions
                    ->filterByName($this->get("q")."%", Criteria::ILIKE);
            }

            foreach ($religions->find() as $religion) {
                $data[] = [
                    "id"     => $religion->getId(),
                    "name"   => $religion->getName(),
                    "impact" => $religion->getImpact()
                ];
            }

            $this->response($data);
        } catch (\Exception $exception) {
            log_message("error", "Error while reading religions. Exception: " . $exception->getMessage());
            $this->response([]);
        }

    }

}