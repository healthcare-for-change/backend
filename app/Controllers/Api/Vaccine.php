<?php

namespace App\Controllers\Api;

use App\Controllers\RestController;
use App\Libraries\Auth\AuthUtils;
use Propel\Runtime\ActiveQuery\Criteria;

class Vaccine extends RestController
{
    protected int $items_per_page = 20;

    public function autocomplete_list_get()
    {
        try {
            $data = [];

            $user = AuthUtils::create()->validateLoginAndTennant();
            if (!$user) {
                $this->respondAccessDenied();
            }

            $vaccines = \VaccineQuery::create()
                ->limit($this->items_per_page)
                ->orderByName();

            if(!empty($this->get("q"))) {
                $vaccines
                    ->filterByName($this->get("q")."%", Criteria::ILIKE);
            }

            foreach ($vaccines->find() as $vaccine) {
                $data[] = [
                    "id"     => $vaccine->getUuid(),
                    "name"   => $vaccine->getName()
                ];
            }

            $this->response($data);
        } catch (\Exception $exception) {
            log_message("error", "Error while reading vaccines. Exception: " . $exception->getMessage());
            $this->response([]);
        }
    }

}