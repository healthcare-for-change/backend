<?php

namespace App\Controllers\Api;

use App\Controllers\RestController;
use App\Libraries\Auth\AuthUtils;
use Map\PhysicalExaminationTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Propel;
use Ramsey\Uuid\Uuid;

class Physical extends RestController
{
    public function history_get($patient_uuid, $pageNo = 0, $rowsPerPage = ROWS_PER_PAGE)
    {
        try {
            $data = [];
            $user = AuthUtils::create()->validateLoginAndTennant();
            if (!$user) {
                $this->respondAccessDenied();
            }

            $totalItems = \PhysicalExaminationQuery::create()
                ->filterByPatientUuid($patient_uuid)
                ->count();

            $physicalExaminationItems = \PhysicalExaminationQuery::create()
                ->orderByTimestamp(Criteria::DESC)
                ->leftJoinUser()
                ->filterByPatientUuid($patient_uuid);

            foreach ($physicalExaminationItems->paginate($pageNo, $rowsPerPage) as $physicalExamination) {
                $data[] = $this->getRecordArray($physicalExamination);
            }
            $this->response([
                "count"         => $totalItems,
                "total_pages"   => ceil($totalItems / $rowsPerPage),
                "pageno"        => $pageNo,
                "rows_per_page" => $rowsPerPage,
                "rows"          => $data
            ]);

        } catch (\Exception $exception) {
            log_message("error", "Exception while reading physical examination history. Exception: " . $exception->getMessage());
            $this->response([]);
        }
    }


    public function details_get($uuid)
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        $physicalHistory = \PhysicalExaminationQuery::create()
            ->filterByUuid($uuid)
            ->findOne();

        if (!$physicalHistory instanceof \PhysicalExamination) {
            $this->respondNotFound();
        }

        $data = $this->getRecordArray($physicalHistory);

        $this->response($data);
    }


    public function record_post()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(PhysicalExaminationTableMap::DATABASE_NAME);

            $record = new \PhysicalExamination();
            $record
                ->setUuid(Uuid::uuid4())
                ->setPatientUuid($this->get("patient_uuid"))
                ->setUserId($user);

            $record = $this->updateData($record);

            $record->save($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not save physical examination information caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save physical examination data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    public function record_put()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(PhysicalExaminationTableMap::DATABASE_NAME);

            $record = \PhysicalExaminationQuery::create()
                ->filterByUuid($this->get("uuid"))
                ->findOne($con);

            if (!$record) {
                $this->response(["status" => "error", "message" => "Record not found!"], 500);
            }

            $record->setUserId($user);
            $record = $this->updateData($record);

            $record->save($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not save physical examination data caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save physical examination data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    public function record_delete()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(PhysicalExaminationTableMap::DATABASE_NAME);

            $record = \PhysicalExaminationQuery::create()
                ->filterByUuid($this->get("uuid"))
                ->findOne($con);

            if (!$record) {
                $this->response(["status" => "error", "message" => "Record not found!"], 500);
            }

            $record->delete($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not delete physical examination data caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not delete physical examination data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    private function updateData(\PhysicalExamination $record): \PhysicalExamination
    {
        $timestamp = !empty($this->get("timestamp")) ? strtotime($this->get("timestamp")) : null;

        $record
            ->setTimestamp($timestamp)
            ->setBloodPressureSystolic($this->get("blood_pressure_systolic") ?: null)
            ->setBloodPressureDiastolic($this->get("blood_pressure_diastolic") ?: null)
            ->setHeartRate($this->get("heart_rate") ?: null)
            ->setWeight($this->get("weight") ?: null)
            ->setHeight($this->get("height") ?: null)
            ->setMuac($this->get("muac") ?: null)
            ->setTemperature($this->get("temperature") ?: null)
            ->setSkinExamination($this->get("skin_examination"))
            ->setDetailsEnt($this->get("details_ent"))
            ->setDetailsEyes($this->get("details_eyes"))
            ->setDetailsHead($this->get("details_head"))
            ->setDetailsMusclesBones($this->get("details_muscles_bones"))
            ->setDetailsHeart($this->get("details_heart"))
            ->setDetailsLung($this->get("details_lung"))
            ->setDetailsGastrointestinal($this->get("details_gastrointestinal"))
            ->setDetailsUrinaryTract($this->get("details_urinary_tract"))
            ->setDetailsReproductiveSystem($this->get("details_reproductive_system"))
            ->setDetailsOther($this->get("details_other"))
            ->setGeneralImpression($this->get("general_impression"));

        return $record;
    }

    private function getRecordArray(\PhysicalExamination $item): array {
        return [
            "uuid"                        => $item->getUuid(),
            "patient_uuid"                => $item->getPatientUuid(),
            "created_at"                  => $item->getCreatedAt("Y-m-d"),
            "updated_at"                  => $item->getUpdatedAt("Y-m-d"),
            "timestamp"                   => $item->getTimestamp("Y-m-d"),
            "blood_pressure_systolic"     => $item->getBloodPressureSystolic(),
            "blood_pressure_diastolic"    => $item->getBloodPressureDiastolic(),
            "heart_rate"                  => $item->getHeartRate(),
            "weight"                      => $item->getWeight(),
            "height"                      => $item->getHeight(),
            "muac"                        => $item->getMuac(),
            "temperature"                 => $item->getTemperature(),
            "skin_examination"            => $item->getSkinExamination(),
            "details_ent"                 => $item->getDetailsEnt(),
            "details_eyes"                => $item->getDetailsEyes(),
            "details_head"                => $item->getDetailsHead(),
            "details_muscles_bones"       => $item->getDetailsMusclesBones(),
            "details_heart"               => $item->getDetailsHeart(),
            "details_lung"                => $item->getDetailsLung(),
            "details_gastrointestinal"    => $item->getDetailsGastrointestinal(),
            "details_urinary_tract"       => $item->getDetailsUrinaryTract(),
            "details_reproductive_system" => $item->getDetailsReproductiveSystem(),
            "details_other"               => $item->getDetailsOther(),
            "general_impression"          => $item->getGeneralImpression(),
            "general_impression_string"   => $item->getGeneralImpressionString(),
            "user_name"                   => $item->getUser() ? $item->getUser()->getUsername() : null
        ];
    }
}