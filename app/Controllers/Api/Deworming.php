<?php

namespace App\Controllers\Api;

use App\Controllers\RestController;
use App\Libraries\Auth\AuthUtils;
use Map\DewormingHistoryTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Propel;
use Ramsey\Uuid\Uuid;

class Deworming extends RestController
{
    public function history_get($patient_uuid, $pageNo = 0, $rowsPerPage = ROWS_PER_PAGE)
    {
        try {
            $data = [];
            $user = AuthUtils::create()->validateLoginAndTennant();
            if (!$user) {
                $this->respondAccessDenied();
            }

            $totalItems = \DewormingHistoryQuery::create()
                ->filterByPatientUuid($patient_uuid)
                ->count();

            $dewormingHistoryItems = \DewormingHistoryQuery::create()
                ->orderByTimestamp(Criteria::DESC)
                ->leftJoinMedication()
                ->leftJoinUser()
                ->filterByPatientUuid($patient_uuid)
                ->paginate($pageNo, $rowsPerPage);

            foreach ($dewormingHistoryItems as $dewormingHistory) {
                $data[] = [
                    "timestamp"       => $dewormingHistory->getTimestamp("Y-m-d"),
                    "medication_name" => $dewormingHistory->getMedication() ? $dewormingHistory->getMedication()->getName() : null,
                    "user_name"       => $dewormingHistory->getUser() ? $dewormingHistory->getUser()->getUsername() : null,
                    "comment"         => $dewormingHistory->getComment()
                ];
            }

            $this->response([
                "count"         => $totalItems,
                "total_pages"   => ceil($totalItems / $rowsPerPage),
                "pageno"        => $pageNo,
                "rows_per_page" => $rowsPerPage,
                "rows"          => $data
            ]);

        } catch (\Exception $exception) {
            log_message("error", "Exception while reading deworming history. Exception: " . $exception->getMessage());
            $this->response([]);
        }
    }


    public function record_post()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(DewormingHistoryTableMap::DATABASE_NAME);

            $record = new \DewormingHistory();
            $record
                ->setUuid(Uuid::uuid4())
                ->setTimestamp(new \DateTime())
                ->setPatientUuid($this->get("patient_uuid"))
                ->setComment($this->get("comment"))
                ->setMedicationId($this->get("medication_id") ?: null)
                ->setUserId($user);

            $record->save($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not save deworming information caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save deworming data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    public function record_put()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(DewormingHistoryTableMap::DATABASE_NAME);

            $record = \DewormingHistoryQuery::create()
                ->filterByUuid($this->get("uuid"))
                ->findOne($con);

            if (!$record) {
                $this->response(["status" => "error", "message" => "Record not found!"], 500);
            }

            $record
                ->setTimestamp(new \DateTime())
                ->setPatientUuid($this->get("patient_uuid"))
                ->setComment($this->get("comment"))
                ->setMedicationId($this->get("medication_id") ?: null)
                ->setUserId($user);

            $record->save($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not save deworming data caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save deworming data."], 500);
        }

        $this->response(["status" => "success"]);
    }

    public function record_delete()
    {
        $user = AuthUtils::create()->validateLoginAndTennant();
        if (!$user) {
            $this->respondAccessDenied();
        }

        try {
            $con = Propel::getWriteConnection(DewormingHistoryTableMap::DATABASE_NAME);

            $record = \DewormingHistoryQuery::create()
                ->filterByUuid($this->get("uuid"))
                ->findOne($con);

            if (!$record) {
                $this->response(["status" => "error", "message" => "Record not found!"], 500);
            }

            $record->delete($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            log_message("error", "Could not delete deworming data caused by an exception. " . $e->getMessage());
            $this->response(["status" => "error", "message" => "Could not save deworming data."], 500);
        }

        $this->response(["status" => "success"]);
    }

}