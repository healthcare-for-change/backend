<?php

namespace App\Controllers;

use App\Libraries\Auth0Management;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use GrowthSquare\SubscriberQuery;
use Psr\Log\LoggerInterface;

class Auth extends BaseController
{

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        $this->disableRegistrationCheck = true;
        parent::initController($request, $response, $logger);
    }

    /**
     * Fallback index call, normally not used
     *
     * @param null $value
     *
     * @throws \Auth0\SDK\Exception\ConfigurationException
     */
    public function index($value = null)
    {
        $this->auth->validateOrForceLogin();
        Header("Location: /");
    }

    /**
     * Handle necessary callbacks after the authentication has been processed at Auth0
     * @throws \Auth0\SDK\Exception\NetworkException
     * @throws \Auth0\SDK\Exception\StateException
     */
    public function callback()
    {
        // Handle the callback of the registration call
        $this->auth->handleCallback();

        if ($this->auth->isLoggedIn()) {
            $authId = $this->auth->getAuthId();

            $subscriber = SubscriberQuery::create()
                ->filterByAuthId($authId)
                ->findOne();

            if ( ! $subscriber) {

                // Forward to registration form:
                // Register session information to require registration form
                $this->session->set([
                    "requiresRegistration" => true,
                ]);

                // Redirect user to the registration form page
                header("Location: /registration/form");
                exit;
            }
        }

        // Register user information in the session for later use
        // The information is registered as structured data in order to get access also via JavaScript
        $this->session->set([
            "requiresRegistration" => false,
            "subscriber"           => $subscriber->getUuid(),
            "subscriberInfo"       => [
                "name"    => $subscriber->getFirstName() . " " . $subscriber->getName(),
                "email"   => $subscriber->getEmail(),
                "created" => $subscriber->getCreatedAt("d.m.Y"),
            ],
        ]);

        header("Location: /");
        exit;
    }

    /**
     * Perform bridge to login
     * @throws \Auth0\SDK\Exception\ConfigurationException
     */
    public function login()
    {
        $redirect = false;
        if ($this->request->getGet("redirect")) {
            $redirect = true;
        }

        $this->auth->validateOrForceLogin($redirect);
    }

    /**
     * Perform logout and redirect to start page
     * @throws \Auth0\SDK\Exception\ConfigurationException
     */
    public function logout()
    {
        $this->auth->logout();
    }


    public function profile()
    {
        // Validate if the user is loggedin or otherwise force login for this user account
        $this->auth->validateOrForceLogin();

        $management = new Auth0Management();
        $users = $management->management->users()->get($this->auth->getAuthId());

        $userDetails = json_decode($users->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);

        // Show profile page of current user showing
        return view('subscriber/profile/profile', [
            "subscriberInfo" => $this->session->get("subscriberInfo"),
            "credentials"    => $this->auth->getCredentials(),
            "userDetails"    => $userDetails,
            "subscriber"     => $this->session->get("subscriber"),
        ]);
    }
}
