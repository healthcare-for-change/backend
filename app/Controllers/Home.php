<?php

namespace App\Controllers;

use Tools\Core\Mail\Mailagent;

class Home extends BaseController
{
    public function index($value = null)
    {
        return view('welcome');
    }
}
