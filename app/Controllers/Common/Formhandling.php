<?php

namespace App\Controllers\Common;

use App\Controllers\RestController;
use App\Tools\Finance\Vat\VatValidation;
use App\Tools\Finance\Vat\VatValidationException;

class Formhandling extends RestController
{

    public function vat_validation_get()
    {
        $this->validateParameterExistence(["vat"]);

        $vat = $this->get("vat");

        try {
            $vatValidation = new VatValidation();

            // First perform syntax check:
            if(! VatValidation::validateString($vat)) {
                die('"'._("The syntax of the provided VAT number is not valid! (Avoid spaces, etc.)").'"');
            }

            if ($vatValidation->validate($vat)) {
                die('"true"');
            }
        } catch (VatValidationException $e) {
            die('"'._("It was not possible to perform a VAT validation.").'"');
        } catch (\SoapFault $e) {
            die('"'._("It was not possible to perform a VAT validation.").'"');
        }

        echo '"'._("The provided VAT is not valid!").'"';
    }
}
