<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\Shield\Entities\User;


/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */
class Cli extends Controller
{
    public function user_create($username, $email, $password, $tenantUuid = null)
    {
        echo "Start creating of new user $username".PHP_EOL;

        $users = model('UserModel');
        $user = new User([
            'username' => $username,
            'email' => $email,
            'password' => $password,
            'active' => true
        ]);

        $users->save($user);
        $userId = $users->getInsertID();

        // Add Tenant:
        if(!$tenantUuid) {
            $tenantUuid = 'd7a90fbb-fe0b-464f-b86d-2a7b7af925ea';
        }
        $tenant = \TenantQuery::create()->findOneByUuid($tenantUuid);

        if(!$tenant) {
            $tenant = new \Tenant();
            $tenant
                ->setUuid($tenantUuid)
                ->setName("Default tenant")
                ->save();
        }

        $userHasTenant = new \UserHasTenant();
        $userHasTenant
            ->setUserId($userId)
            ->setTenant($tenant)
            ->save();

        echo "Created user with UserID $userId".PHP_EOL;
        echo "Complete.".PHP_EOL;
    }

    public function user_delete($username)
    {
        echo "Start deleting of user.".PHP_EOL;

        $users = model('UserModel');
        $user = $users->findByCredentials(["username" => $username]);

        if($user) {
            $users->delete($user->id, true);
            echo "User deleted.".PHP_EOL;
        }
    }
}