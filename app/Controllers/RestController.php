<?php

namespace App\Controllers;

use App\Libraries\Auth;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Security\Exceptions\SecurityException;
use CodeIgniter\Security\Security;
use Config\App;
use Psr\Log\LoggerInterface;
use Tools\Rest\ErrorCode\CommonCodes;
use Tools\Rest\ErrorHandler\CsrfError;
use Tools\Rest\ErrorHandler\InvalidDataError;
use Tools\Rest\ErrorHandler\InvalidLoginError;
use Tools\Rest\ErrorHandler\MissingParameterError;
use voku\helper\AntiXSS;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */
class RestController extends BaseController
{
    /**
     * Instance of the main Request object.
     *
     * @var CLIRequest|IncomingRequest
     */
    protected $request;

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var array
     */
    protected $helpers = [];

    /**
     * @var Security
     */
    protected $security;

    /**
     * Common HTTP status codes and their respective description.
     *
     * @link http://www.restapitutorial.com/httpstatuscodes.html
     */
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_NOT_MODIFIED = 304;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_METHOD_NOT_ALLOWED = 405;
    const HTTP_NOT_ACCEPTABLE = 406;
    const HTTP_INTERNAL_ERROR = 500;

    /**
     * List of allowed HTTP methods.
     *
     * @var array
     */
    protected $allowed_http_methods = ['get', 'delete', 'post', 'put', 'options', 'patch', 'head'];

    /**
     * The arguments for the query parameters.
     *
     * @var array
     */
    protected $_query_args = [];

    /**
     * The arguments from GET, POST, PUT, DELETE, PATCH, HEAD and OPTIONS request methods combined.
     *
     * @var array
     */
    private $_args = [];


    /**
     * Constructor.
     */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        // Preload any models, libraries, etc, here.

        $this->session  = \Config\Services::session();
        $this->security = \Config\Services::security();

        // Perform basic processing:
        $this->_query_args = $this->request->getGet();
        $this->_parse_body();
        $this->_args = array_merge($this->_args, $this->_query_args);
    }

    /**
     * Necessary remapping of incoming requests that will allow a CALL_METHOD() function call.
     *
     * @param string $method
     * @param        ...$params
     *
     * @throws \Exception
     */
    public function _remap(string $method, ...$params): void
    {
        $functionName = $method . "_" . $this->request->getMethod();
        if (method_exists($this, $functionName)) {
            $this->{$functionName}(...$params);
        } else {
            throw new \Exception("Could not identify function");
        }
    }

    /**
     * Will return the value of a provided key from the payload
     *
     * @param string $key      Requested key
     * @param bool   $xssClean Perform cleaning of variable
     *
     * @return mixed
     */
    public function get(string $key, ?bool $xssClean = true): mixed
    {
        $value = null;

        if (isset($this->_args[$key])) {
            if ($xssClean) {
                $antiXss = new AntiXSS();
                $value   = $antiXss->xss_clean($this->_args[$key]);
            } else {
                $value = $this->_args[$key];
            }
        }

        return $value;
    }

    /**
     * Return all keys and values of parameters provided
     *
     * @param bool $xssClean Enable XSS protection
     *
     * @return array
     */
    public function getArgs(bool $xssClean = true): array
    {
        $response = [];
        $antiXss  = new AntiXSS();

        foreach ($this->_args as $key => $value) {
            if ($xssClean) {
                $response[$antiXss->xss_clean($key)] = $antiXss->xss_clean($value);
            } else {
                $response[$key] = $value;
            }
        }

        return $response;
    }


    /**
     * Validate if all required parameters have been submitted
     * @param array $parameters
     */
    public function validateParameterExistence(array $parameters)
    {
        $missingParameters = [];
        foreach($parameters as $parameter) {
            if(!array_key_exists($parameter, $this->_args)) {
                $missingParameters[] = $parameter;
            }
        }

        if(!empty($missingParameters)) {
            $providedParams = array_keys($this->_args);
            $error = new MissingParameterError("Missing parameters", $providedParams, $missingParameters, $parameters, CommonCodes::CODE_MISSING_PARAMETERS);
            $this->throwError($error);
        }
    }


    public function validateCSRF()
    {
        try {
            $verification = new Security(new App());
            $verification->verify($this->request);
        } catch (SecurityException $exception) {
            $error = new CsrfError();
            $this->throwError($error);
        }

    }


    /**
     * Verify if the current user is been logged in or exit processing
     */
    public function validateLogin()
    {
        // TODO
    }


    /**
     * Respond the error message to the requesting client
     *
     * @param string     $message
     * @param mixed      $providedValue
     * @param array|null $allowedInput
     * @param int|null   $code
     */
    public function throwError(InvalidDataError|MissingParameterError|InvalidLoginError|CsrfError $response): void
    {
        $this->response($response->toArray(), self::HTTP_BAD_REQUEST);
        exit;
    }


    /**
     * Generate the response to the client
     *
     * @param array $data
     * @param int   $code
     */
    protected function response(array $data, int $code = 200, ?bool $refreshToken = false): void
    {
        $payload = $data;

        if($refreshToken) {
            $payload["refreshToken"] = $this->session->get($this->security->getCSRFTokenName());
        }

        $this->response
            ->setStatusCode($code)
            ->setJSON($payload)
            ->send();
        exit;
    }


    /**
     * Will get the contents of the request and transform them into an array, that is stored at $this->_args
     */
    private function _parse_body(): void
    {
        $body        = $this->request->getBody();
        $contentType = $this->request->getHeaderLine("content-type");
        $content     = null;

        if (str_contains($contentType, "application/json")) {
            $content = json_decode($body, true);
        } elseif (str_contains($contentType, "application/x-www-form-urlencoded")) {
            $content = $this->request->getRawInput();
        }
        else {
            if($this->request->getMethod() !== "get") {
                $error = new InvalidDataError(
                    "Unsupported Content Type submitted!",
                    $contentType,
                    [
                        "application/json",
                        "application/x-www-form-urlencoded",
                    ],
                    CommonCodes::CODE_PRECONDITION_FAILED
                );

                $this->throwError($error);
            }
        }

        if (is_array($content)) {
            $this->_args = $content;
        }
    }

    public function respondAccessDenied()
    {
        $this->response(["status" => "error", "message" => "Access denied. Please use a valid login in order to access the requested resource."], 403);
    }

    public function respondNotFound()
    {
        $this->response(["status" => "error", "message" => "The requested record was not found!"], 403);
    }


    /**
     * Get header Authorization
     * */
    function getAuthorizationHeader()
    {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    /**
     * get access token from header
     * */
    function getBearerToken()
    {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
}
