<?= $this->extend('layout/base_layout') ?>

<?= $this->section('content') ?>

<div class="ms-hero-page-override ms-hero-img-team ms-hero-bg-primary">
    <div class="container">
        <div class="text-center">
            <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5"><?= _("Complete registration"); ?></h1>
            <p class="lead lead-lg color-light text-center center-block mt-2 mw-800 text-uppercase fw-400 animated fadeInUp animation-delay-7"><?= _("Thanks for joining GrowthSquare!")."<br />"._("Please complete your personal information!"); ?></p>
        </div>
    </div>
</div>


<div class="container">
    <div class="card card-hero animated fadeInUp animation-delay-7">
        <div class="card-body">
            <?= $form; ?>
        </div>
    </div>
</div>

<?= $this->endSection() ?>
