<?= $this->extend('layout/base_layout') ?>

<?= $this->section('content') ?>

<div class="ms-hero-page-override ms-hero-img-coffee ms-bg-fixed ms-hero-bg-primary">
    <div class="container">
        <div class="text-center mt-2">
            <img src="<?= \App\Tools\Subscriber\Avatar::getAvatar(); ?>" alt="..." class="ms-avatar-hero animated zoomIn animation-delay-7">
            <h1 class="color-white mt-4 animated fadeInUp animation-delay-10"><?= $subscriberInfo["name"]; ?></h1>
        </div>
    </div>
</div>
<div class="container">
    <div class="card card-hero card-primary animated fadeInUp animation-delay-7">
        <div class="card-header-100 bg-primary-dark">
            <div class="row justify-content-center">
                <div class="col col-md-2">
                    <div class="text-center">
                        <h3><strong>0</strong></h3> <?= _("Startups"); ?>
                    </div>
                </div>
                <div class="col col-md-2">
                    <div class="text-center">
                        <h3><strong>0</strong> </h3> <?= _("Investments"); ?>
                    </div>
                </div>
                <div class="col col-md-2">
                    <div class="text-center">
                        <h3><strong>0</strong></h3> <?= _("Friends"); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-12">
                <div class="card-body">
                    <h2 class="color-primary no-mb" id=personal-information"><?= _("Personal Information"); ?></h2>
                </div>
                <table class="table table-no-border " aria-describedby="personal-information">
                    <tr>
                        <th scope="row"><i class="zmdi zmdi-account mr-1 color-primary"></i> <?= _("Fullname"); ?></th>
                        <td><?= $subscriberInfo["name"]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><i class="zmdi zmdi-email mr-1 color-primary"></i> <?= _("Email"); ?></th>
                        <td><a href="mailto:<?= $subscriberInfo["email"]; ?>"><?= $subscriberInfo["email"]; ?></a></td>
                    </tr>
                    <tr>
                        <th scope="row"><i class="zmdi zmdi-calendar mr-1 color-info"></i> <?= _("Member Since"); ?></th>
                        <td><?= $subscriberInfo["created"]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><i class="zmdi zmdi-shield-security mr-1 color-danger"></i> <?= _("Last Login"); ?></th>
                        <td><?= date("d.m.Y H:i", strtotime($userDetails["last_login"])); ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><i class="zmdi zmdi-router mr-1 color-danger"></i> <?= _("Login IP"); ?></th>
                        <td><?= $userDetails["last_ip"]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><i class="zmdi zmdi-time-interval mr-1 color-primary"></i> <?= _("Login Count"); ?></th>
                        <td><?= $userDetails["logins_count"]; ?></td>
                    </tr>
                </table>

                <?php
                    if(str_contains($userDetails["user_id"], "auth0")) {
                        ?>
                        <a href="#" onclick="change_password();" class="btn btn-block btn-default"><?= _("Change password"); ?></a>
                        <?php
                    } // End if
                ?>

                <a href="/auth/logout" class="btn btn-block btn-raised btn-danger"><?= _("Logout"); ?></a>
            </div>
            <div class="col-xl-8 col-md-12">
                <div class="card-body">
                    <h2 class="color-primary"><?= _("My Subscriptions"); ?></h2>
                    <?php
                        $subscriptions = \GrowthSquare\SubscriberHasSubscriptionQuery::create()
                            ->leftJoinSubscription()
                            ->useSubscriberQuery()
                            ->filterByUuid($subscriber)
                            ->endUse()
                            ->filterByValidTo(NULL)->_or()->filterByValidTo(["min" => time()])
                            ->find();

                        /* @var \GrowthSquare\Subscription */
                        foreach($subscriptions as $subscription) {
                            ?>
                            <div class="media mb-3">
                                <div class="media-left mr-3">
                                    <i class="zmdi zmdi-puzzle-piece zmdi-hc-2x"></i>
                                </div>
                                <div class="media-body">
                                    <h4 class="no-m"><?= $subscription->getSubscription()->getName(); ?></h4>
                                    <p><?= _("Monthly costs").": ".number_format($subscription->getSubscription()->getCostMonthly(), 2, ",", ".")." ".$subscription->getSubscription()->getCurrency(); ?></p>
                                </div>
                            </div>
                            <?php
                        } // Foreach
                    ?>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container -->

<script>
    function change_password()
	{
		bootbox.dialog({
			title: <?= quote(_("Change password")); ?>,
			message: <?= quote(_("You can now trigger a password reset workflow. This will send you an e-mail to the registered e-mail address in order to set a new password.")) ?>,
			size: 'large',
			onEscape: true,
			backdrop: true,
			buttons: {
				cancel: {
					label: <?= quote(_("Cancel")); ?>,
					className: 'btn btn-default',
					callback: function(){
                        bootbox.hideAll();
					}
				},
				submit: {
					label: <?= quote(_("Trigger process")); ?>,
					className: 'btn btn-raised btn-success',
					callback: function(){
                        jQuery.ajax({
							method: 'POST',
							url: 'https://<?= getenv("auth0.domain"); ?>/dbconnections/change_password',
							headers: {'content-type': 'application/json'},
							data: {
								client_id: '<?= getenv("auth0.client_id"); ?>',
								email: '<?= $userDetails["email"] ?>',
								connection: 'Username-Password-Authentication'
							},
							success: function(response) {
								Snackbar.show({
									pos:         "top-right",
									customClass: 'success',
									text:        <?= quote(_("Process started! You will now receive an e-mail with further instructions.")); ?>
								});
							},
                            error: function() {
								Snackbar.show({
									pos: "top-right",
									customClass: "warning",
									text: <?= quote(_("It was not possible to trigger the reset process. Please try again.")); ?>
								});
                            }
                        });
					}
				}
			}
        });
    }
</script>

<?= $this->endSection() ?>
