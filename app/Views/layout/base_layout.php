<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>GrowthSquare - Home of successful investing</title>
    <meta name="description" content="<?= _("GrowthSquare manages startup portfolios for investors and enable startups to scale at ease."); ?>">
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="/assets/css/preload.min.css">
    <link rel="stylesheet" href="/assets/css/plugins.min.css">
    <link rel="stylesheet" href="/assets/css/style.themed.css">
    <script src="/assets/js/plugins.min.js"></script>
    <!--[if lt IE 9]>
    <script src="/assets/js/html5shiv.min.js"></script>
    <script src="/assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="ms-preload" class="ms-preload">
    <div id="status">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
            <div class="dot3"></div>
        </div>
    </div>
</div>

<?= view_cell('\App\Libraries\Layout::generateSideMenu') ?>

<div class="ms-site-container">

    <?= view_cell('\App\Libraries\Layout::generateHeader') ?>

    <?= view_cell('\App\Libraries\Layout::generateHeadMenu') ?>

    <?= $this->renderSection('content') ?>

    <aside class="ms-footbar">
        <div class="container">
            <div class="row">
                <?= $this->renderSection('footer') ?>
            </div>
        </div>
    </aside>
    <footer class="ms-footer">
        <div class="container">
            <p>&copy; <?= date("Y"); ?> GrowthSquare GmbH</p>
        </div>
    </footer>
    <div class="btn-back-top">
        <a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised "><i class="zmdi zmdi-long-arrow-up"></i></a>
    </div>
</div> <!-- ms-site-container -->
<script src="/assets/js/app.min.js"></script>
<script src="/assets/js/bootbox.all.min.js"></script>
</body>
</html>
