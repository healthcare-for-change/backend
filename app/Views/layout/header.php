<?php
/* @var $auth \App\Libraries\Auth; */
?>

<header class="ms-header ms-header-dark">
    <!--ms-header-primary-->
    <div class="container container-full">
        <div class="ms-title">
            <a href="/">
                <img id="main-logo" src="/assets/img/logo-transparent-188.png" alt="">
            </a>
        </div>
        <div class="header-right">
            <div class="share-menu">
                <ul class="share-menu-list">
                    <li class="animated fadeInRight animation-delay-3"><a href="javascript:void(0)" class="btn-circle btn-google"><i class="zmdi zmdi-google"></i></a></li>
                    <li class="animated fadeInRight animation-delay-2"><a href="javascript:void(0)" class="btn-circle btn-facebook"><i class="zmdi zmdi-facebook"></i></a></li>
                    <li class="animated fadeInRight animation-delay-1"><a href="javascript:void(0)" class="btn-circle btn-twitter"><i class="zmdi zmdi-twitter"></i></a></li>
                </ul>
                <a href="javascript:void(0)" class="btn-circle btn-circle-primary animated zoomInDown animation-delay-7"><i class="zmdi zmdi-share"></i></a>
            </div>
            <?php
                if($auth->isLoggedIn()) {
                    ?><a href="/auth/profile" class="btn-circle btn-circle-primary no-focus animated zoomInDown animation-delay-8"><i class="zmdi zmdi-account"></i></a><?php
                }
                else {
                    ?><a href="/auth/login" class="btn-circle btn-circle-primary no-focus animated zoomInDown animation-delay-8"><i class="zmdi zmdi-sign-in"></i></a><?php
                }
            ?>

            <form class="search-form animated zoomInDown animation-delay-9">
                <input id="search-box" type="text" class="search-input" placeholder="Search..." name="q" />
                <label for="search-box"><i class="zmdi zmdi-search"></i></label>
            </form>
            <a href="javascript:void(0)" class="btn-ms-menu btn-circle btn-circle-primary ms-toggle-left animated zoomInDown animation-delay-10"><i class="zmdi zmdi-menu"></i></a>
        </div>
    </div>
</header>
