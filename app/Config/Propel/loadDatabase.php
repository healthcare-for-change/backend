<?php
$serviceContainer = \Propel\Runtime\Propel::getServiceContainer();
$serviceContainer->initDatabaseMaps(array (
  'HealtchareForChange' => 
  array (
    0 => '\\Map\\AuthGroupsUserTableMap',
    1 => '\\Map\\AuthIdentityTableMap',
    2 => '\\Map\\AuthLoginTableMap',
    3 => '\\Map\\AuthPermissionsUserTableMap',
    4 => '\\Map\\AuthRememberTokenTableMap',
    5 => '\\Map\\AuthTokenLoginTableMap',
    6 => '\\Map\\CodeigniterSettingsTableMap',
    7 => '\\Map\\DewormingHistoryTableMap',
    8 => '\\Map\\MedicalDiagnosisCodeTableMap',
    9 => '\\Map\\MedicalHistoryTableMap',
    10 => '\\Map\\MedicationAdministrationMethodTableMap',
    11 => '\\Map\\MedicationDoseFormTableMap',
    12 => '\\Map\\MedicationDoseInstructionTableMap',
    13 => '\\Map\\MedicationMarketingAuthorizationHolderTableMap',
    14 => '\\Map\\MedicationTableMap',
    15 => '\\Map\\PatientTableMap',
    16 => '\\Map\\PhysicalExaminationTableMap',
    17 => '\\Map\\PrescriptionDoseInstructionTableMap',
    18 => '\\Map\\PrescriptionTableMap',
    19 => '\\Map\\ReligionTableMap',
    20 => '\\Map\\TenantTableMap',
    21 => '\\Map\\UserHasTenantTableMap',
    22 => '\\Map\\UserTableMap',
    23 => '\\Map\\VaccinationHistoryTableMap',
    24 => '\\Map\\VaccinationPlanTableMap',
    25 => '\\Map\\VaccineDoseScheduleTableMap',
    26 => '\\Map\\VaccineTableMap',
  ),
));
