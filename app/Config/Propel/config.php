<?php
$serviceContainer = \Propel\Runtime\Propel::getServiceContainer();
$serviceContainer->checkVersion(2);
$serviceContainer->setAdapterClass('HealtchareForChange', getenv('propel.adapter.class'));
$manager = new \Propel\Runtime\Connection\ConnectionManagerSingle('HealtchareForChange');

$dsn = sprintf(
    'pgsql:host=%s;port=%s;dbname=%s;user=%s;password=%s',
    getenv('propel.dsn.host'),
    getenv('propel.dsn.port'),
    getenv('propel.dsn.dbname'),
    getenv('propel.dsn.user'),
    getenv('propel.dsn.password')
);
$manager->setConfiguration([
    //'classname'  => 'Propel\\Runtime\\Connection\\DebugPDO',
    'classname' => '\\Propel\\Runtime\\Connection\\ConnectionWrapper',
    'dsn'        => $dsn,
    'user'       => getenv('propel.dsn.user'),
    'password'   => getenv('propel.dsn.password'),
    'attributes' =>
        [
            'ATTR_EMULATE_PREPARES' => false,
            'ATTR_TIMEOUT'          => 30,
        ],
    'settings'   =>
        [
            'charset' => 'utf8',
            'queries' =>
                [
                    'utf8' => 'SET NAMES \'UTF8\'',
                ],
        ],
]);
$manager->setName('HealtchareForChange');
$serviceContainer->setConnectionManager($manager);
$serviceContainer->setDefaultDatasource('HealtchareForChange');

require_once __DIR__ . '/loadDatabase.php';
