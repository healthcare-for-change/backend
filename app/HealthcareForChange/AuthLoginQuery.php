<?php

use Base\AuthLoginQuery as BaseAuthLoginQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'auth_logins' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class AuthLoginQuery extends BaseAuthLoginQuery
{

}
