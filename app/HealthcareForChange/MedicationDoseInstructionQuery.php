<?php

use Base\MedicationDoseInstructionQuery as BaseMedicationDoseInstructionQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'medication_dose_instruction' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class MedicationDoseInstructionQuery extends BaseMedicationDoseInstructionQuery
{

}
