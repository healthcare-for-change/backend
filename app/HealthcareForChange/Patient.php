<?php

use Base\Patient as BasePatient;

/**
 * Skeleton subclass for representing a row from the 'patient' table.
 *
 * Contains patient information
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class Patient extends BasePatient
{
    public const GENDER_MALE = "m";
    public const GENDER_FEMALE = "f";
    public const GENDER_DIVERSE = "d";
}
