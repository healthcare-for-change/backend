<?php

use Base\MedicalHistory as BaseMedicalHistory;

/**
 * Skeleton subclass for representing a row from the 'medical_history' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class MedicalHistory extends BaseMedicalHistory
{
    public const CLINICAL_STATUS_ACTIVE = 1;
    public const CLINICAL_STATUS_INACTIVE = 2;
    public const CLINICAL_STATUS_RESOLVED = 3;
    protected array $clinicalStatusName = [
        1 => "active",
        2 => "inactive",
        3 => "resolved"
    ];


    public const VERIFICATION_STATUS_UNCONFIRMED = "unconfirmed";
    public const VERIFICATION_STATUS_PROVISIONAL = "provisional";
    public const VERIFICATION_STATUS_REFUTED = "refuted";
    protected array $verificationStatusName = [
        1 => self::VERIFICATION_STATUS_UNCONFIRMED,
        2 => self::VERIFICATION_STATUS_PROVISIONAL,
        3 => self::VERIFICATION_STATUS_REFUTED
    ];


    public const SEVERITY_SEVERE = "severe";
    public const SEVERITY_MODERATE = "moderate";
    public const SEVERITY_MILD = "mild";
    protected array $severityName = [
        1 => self::SEVERITY_SEVERE,
        2 => self::SEVERITY_MODERATE,
        3 => self::SEVERITY_MILD
    ];

    /**
     * Returns the plaintext clinical status; if no status is set returns false
     * @return false|string
     */
    public function getClinicalStatusName()
    {
        if($this->getClinicalStatus()) {
            return $this->clinicalStatusName[$this->getClinicalStatus()];
        }

        return false;
    }


    /**
     * Returns the plaintext verification status; if no status is set returns false
     * @return false|string
     */
    public function getVerificationStatusName()
    {
        if($this->getVerificationStatus()) {
            return $this->verificationStatusName[$this->getVerificationStatus()];
        }

        return false;
    }


    /**
     * Returns the plaintext severity; if no status is set returns false
     * @return false|string
     */
    public function getSeverityName()
    {
        if($this->getSeverity()) {
            return $this->severityName[$this->getSeverity()];
        }

        return false;
    }
}
