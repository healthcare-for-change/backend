<?php

use Base\VaccinationPlan as BaseVaccinationPlan;

/**
 * Skeleton subclass for representing a row from the 'vaccination_plan' table.
 *
 * Vaccination plan based on the tenant configuration. This shall enable the tenent to provide an individual vaccination plan that is shown in the patient datasheet as upcoming vaccinations
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class VaccinationPlan extends BaseVaccinationPlan
{

}
