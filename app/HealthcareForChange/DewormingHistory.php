<?php

use Base\DewormingHistory as BaseDewormingHistory;

/**
 * Skeleton subclass for representing a row from the 'deworming_history' table.
 *
 * History of deworming
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class DewormingHistory extends BaseDewormingHistory
{

}
