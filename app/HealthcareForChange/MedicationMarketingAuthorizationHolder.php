<?php

use Base\MedicationMarketingAuthorizationHolder as BaseMedicationMarketingAuthorizationHolder;

/**
 * Skeleton subclass for representing a row from the 'medication_marketing_authorization_holder' table.
 *
 * Contains the manufacturer information of a medication
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class MedicationMarketingAuthorizationHolder extends BaseMedicationMarketingAuthorizationHolder
{

}
