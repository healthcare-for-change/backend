<?php

use Base\PhysicalExamination as BasePhysicalExamination;

/**
 * Skeleton subclass for representing a row from the 'physical_examination' table.
 *
 * Contains physical examination history of the respective patient
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class PhysicalExamination extends BasePhysicalExamination
{
    public const GENERAL_IMPRESSION_HEALTHY = 1;
    public const GENERAL_IMPRESSION_MINOR = 2;
    public const GENERAL_IMPRESSION_MAJOR = 3;
    public const GENERAL_IMPRESSION_CRITICAL = 4;

    public function getGeneralImpressionString()
    {
        switch ($this->getGeneralImpression()) {
            case self::GENERAL_IMPRESSION_HEALTHY:
                $string = "healthy impression";
                break;
            case self::GENERAL_IMPRESSION_MINOR:
                $string = "minor medical impairments";
                break;
            case self::GENERAL_IMPRESSION_MAJOR:
                $string = "major medical impairments";
                break;
            case self::GENERAL_IMPRESSION_CRITICAL:
                $string = "critical medical impairments";
                break;
            default:
                $string = "not defined";
        }

        return $string;
    }
}
