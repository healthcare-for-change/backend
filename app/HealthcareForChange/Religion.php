<?php

use Base\Religion as BaseReligion;

/**
 * Skeleton subclass for representing a row from the 'religion' table.
 *
 * Table containing religions to be assigned to patients
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class Religion extends BaseReligion
{

}
