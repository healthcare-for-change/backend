<?php

use Base\DewormingHistoryQuery as BaseDewormingHistoryQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'deworming_history' table.
 *
 * History of deworming
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class DewormingHistoryQuery extends BaseDewormingHistoryQuery
{

}
