<?php

use Base\VaccinationPlanQuery as BaseVaccinationPlanQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'vaccination_plan' table.
 *
 * Vaccination plan based on the tenant configuration. This shall enable the tenent to provide an individual vaccination plan that is shown in the patient datasheet as upcoming vaccinations
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class VaccinationPlanQuery extends BaseVaccinationPlanQuery
{

}
