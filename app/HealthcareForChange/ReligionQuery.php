<?php

use Base\ReligionQuery as BaseReligionQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'religion' table.
 *
 * Table containing religions to be assigned to patients
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class ReligionQuery extends BaseReligionQuery
{

}
