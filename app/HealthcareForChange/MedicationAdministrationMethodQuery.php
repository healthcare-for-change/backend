<?php

use Base\MedicationAdministrationMethodQuery as BaseMedicationAdministrationMethodQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'medication_administration_method' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class MedicationAdministrationMethodQuery extends BaseMedicationAdministrationMethodQuery
{

}
