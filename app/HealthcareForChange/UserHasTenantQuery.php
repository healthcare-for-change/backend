<?php

use Base\UserHasTenantQuery as BaseUserHasTenantQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'user_has_tenant' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class UserHasTenantQuery extends BaseUserHasTenantQuery
{

}
