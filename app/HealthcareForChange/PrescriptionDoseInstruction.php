<?php

use Base\PrescriptionDoseInstruction as BasePrescriptionDoseInstruction;

/**
 * Skeleton subclass for representing a row from the 'prescription_dose_instruction' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class PrescriptionDoseInstruction extends BasePrescriptionDoseInstruction
{

}
