<?php

use Base\TenantQuery as BaseTenantQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'tenant' table.
 *
 * Core table for holding tenant information
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class TenantQuery extends BaseTenantQuery
{

}
