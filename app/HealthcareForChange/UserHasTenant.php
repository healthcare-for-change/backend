<?php

use Base\UserHasTenant as BaseUserHasTenant;

/**
 * Skeleton subclass for representing a row from the 'user_has_tenant' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class UserHasTenant extends BaseUserHasTenant
{

}
