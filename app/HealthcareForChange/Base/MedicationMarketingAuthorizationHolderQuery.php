<?php

namespace Base;

use \MedicationMarketingAuthorizationHolder as ChildMedicationMarketingAuthorizationHolder;
use \MedicationMarketingAuthorizationHolderQuery as ChildMedicationMarketingAuthorizationHolderQuery;
use \Exception;
use \PDO;
use Map\MedicationMarketingAuthorizationHolderTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'medication_marketing_authorization_holder' table.
 *
 * Contains the manufacturer information of a medication
 *
 * @method     ChildMedicationMarketingAuthorizationHolderQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildMedicationMarketingAuthorizationHolderQuery orderByName($order = Criteria::ASC) Order by the name column
 *
 * @method     ChildMedicationMarketingAuthorizationHolderQuery groupById() Group by the id column
 * @method     ChildMedicationMarketingAuthorizationHolderQuery groupByName() Group by the name column
 *
 * @method     ChildMedicationMarketingAuthorizationHolderQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMedicationMarketingAuthorizationHolderQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMedicationMarketingAuthorizationHolderQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMedicationMarketingAuthorizationHolderQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMedicationMarketingAuthorizationHolderQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMedicationMarketingAuthorizationHolderQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMedicationMarketingAuthorizationHolderQuery leftJoinMedication($relationAlias = null) Adds a LEFT JOIN clause to the query using the Medication relation
 * @method     ChildMedicationMarketingAuthorizationHolderQuery rightJoinMedication($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Medication relation
 * @method     ChildMedicationMarketingAuthorizationHolderQuery innerJoinMedication($relationAlias = null) Adds a INNER JOIN clause to the query using the Medication relation
 *
 * @method     ChildMedicationMarketingAuthorizationHolderQuery joinWithMedication($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Medication relation
 *
 * @method     ChildMedicationMarketingAuthorizationHolderQuery leftJoinWithMedication() Adds a LEFT JOIN clause and with to the query using the Medication relation
 * @method     ChildMedicationMarketingAuthorizationHolderQuery rightJoinWithMedication() Adds a RIGHT JOIN clause and with to the query using the Medication relation
 * @method     ChildMedicationMarketingAuthorizationHolderQuery innerJoinWithMedication() Adds a INNER JOIN clause and with to the query using the Medication relation
 *
 * @method     \MedicationQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMedicationMarketingAuthorizationHolder|null findOne(?ConnectionInterface $con = null) Return the first ChildMedicationMarketingAuthorizationHolder matching the query
 * @method     ChildMedicationMarketingAuthorizationHolder findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildMedicationMarketingAuthorizationHolder matching the query, or a new ChildMedicationMarketingAuthorizationHolder object populated from the query conditions when no match is found
 *
 * @method     ChildMedicationMarketingAuthorizationHolder|null findOneById(int $id) Return the first ChildMedicationMarketingAuthorizationHolder filtered by the id column
 * @method     ChildMedicationMarketingAuthorizationHolder|null findOneByName(string $name) Return the first ChildMedicationMarketingAuthorizationHolder filtered by the name column *

 * @method     ChildMedicationMarketingAuthorizationHolder requirePk($key, ?ConnectionInterface $con = null) Return the ChildMedicationMarketingAuthorizationHolder by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicationMarketingAuthorizationHolder requireOne(?ConnectionInterface $con = null) Return the first ChildMedicationMarketingAuthorizationHolder matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMedicationMarketingAuthorizationHolder requireOneById(int $id) Return the first ChildMedicationMarketingAuthorizationHolder filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicationMarketingAuthorizationHolder requireOneByName(string $name) Return the first ChildMedicationMarketingAuthorizationHolder filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMedicationMarketingAuthorizationHolder[]|Collection find(?ConnectionInterface $con = null) Return ChildMedicationMarketingAuthorizationHolder objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildMedicationMarketingAuthorizationHolder> find(?ConnectionInterface $con = null) Return ChildMedicationMarketingAuthorizationHolder objects based on current ModelCriteria
 * @method     ChildMedicationMarketingAuthorizationHolder[]|Collection findById(int $id) Return ChildMedicationMarketingAuthorizationHolder objects filtered by the id column
 * @psalm-method Collection&\Traversable<ChildMedicationMarketingAuthorizationHolder> findById(int $id) Return ChildMedicationMarketingAuthorizationHolder objects filtered by the id column
 * @method     ChildMedicationMarketingAuthorizationHolder[]|Collection findByName(string $name) Return ChildMedicationMarketingAuthorizationHolder objects filtered by the name column
 * @psalm-method Collection&\Traversable<ChildMedicationMarketingAuthorizationHolder> findByName(string $name) Return ChildMedicationMarketingAuthorizationHolder objects filtered by the name column
 * @method     ChildMedicationMarketingAuthorizationHolder[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildMedicationMarketingAuthorizationHolder> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MedicationMarketingAuthorizationHolderQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MedicationMarketingAuthorizationHolderQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\MedicationMarketingAuthorizationHolder', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMedicationMarketingAuthorizationHolderQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMedicationMarketingAuthorizationHolderQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildMedicationMarketingAuthorizationHolderQuery) {
            return $criteria;
        }
        $query = new ChildMedicationMarketingAuthorizationHolderQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMedicationMarketingAuthorizationHolder|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MedicationMarketingAuthorizationHolderTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MedicationMarketingAuthorizationHolderTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMedicationMarketingAuthorizationHolder A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name FROM medication_marketing_authorization_holder WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMedicationMarketingAuthorizationHolder $obj */
            $obj = new ChildMedicationMarketingAuthorizationHolder();
            $obj->hydrate($row);
            MedicationMarketingAuthorizationHolderTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildMedicationMarketingAuthorizationHolder|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(MedicationMarketingAuthorizationHolderTableMap::COL_ID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(MedicationMarketingAuthorizationHolderTableMap::COL_ID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterById($id = null, ?string $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MedicationMarketingAuthorizationHolderTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MedicationMarketingAuthorizationHolderTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicationMarketingAuthorizationHolderTableMap::COL_ID, $id, $comparison);

        return $this;
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * $query->filterByName(['foo', 'bar']); // WHERE name IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $name The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByName($name = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicationMarketingAuthorizationHolderTableMap::COL_NAME, $name, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \Medication object
     *
     * @param \Medication|ObjectCollection $medication the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedication($medication, ?string $comparison = null)
    {
        if ($medication instanceof \Medication) {
            $this
                ->addUsingAlias(MedicationMarketingAuthorizationHolderTableMap::COL_ID, $medication->getMedicationMarketingAuthorizationHolderId(), $comparison);

            return $this;
        } elseif ($medication instanceof ObjectCollection) {
            $this
                ->useMedicationQuery()
                ->filterByPrimaryKeys($medication->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByMedication() only accepts arguments of type \Medication or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Medication relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinMedication(?string $relationAlias = null, ?string $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Medication');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Medication');
        }

        return $this;
    }

    /**
     * Use the Medication relation Medication object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MedicationQuery A secondary query class using the current class as primary query
     */
    public function useMedicationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMedication($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Medication', '\MedicationQuery');
    }

    /**
     * Use the Medication relation Medication object
     *
     * @param callable(\MedicationQuery):\MedicationQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withMedicationQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useMedicationQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Medication table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \MedicationQuery The inner query object of the EXISTS statement
     */
    public function useMedicationExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Medication', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Medication table for a NOT EXISTS query.
     *
     * @see useMedicationExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \MedicationQuery The inner query object of the NOT EXISTS statement
     */
    public function useMedicationNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Medication', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Exclude object from result
     *
     * @param ChildMedicationMarketingAuthorizationHolder $medicationMarketingAuthorizationHolder Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($medicationMarketingAuthorizationHolder = null)
    {
        if ($medicationMarketingAuthorizationHolder) {
            $this->addUsingAlias(MedicationMarketingAuthorizationHolderTableMap::COL_ID, $medicationMarketingAuthorizationHolder->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the medication_marketing_authorization_holder table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicationMarketingAuthorizationHolderTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MedicationMarketingAuthorizationHolderTableMap::clearInstancePool();
            MedicationMarketingAuthorizationHolderTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicationMarketingAuthorizationHolderTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MedicationMarketingAuthorizationHolderTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MedicationMarketingAuthorizationHolderTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MedicationMarketingAuthorizationHolderTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

}
