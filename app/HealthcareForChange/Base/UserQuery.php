<?php

namespace Base;

use \User as ChildUser;
use \UserQuery as ChildUserQuery;
use \Exception;
use \PDO;
use Map\UserTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'users' table.
 *
 *
 *
 * @method     ChildUserQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method     ChildUserQuery orderByDisplayName($order = Criteria::ASC) Order by the display_name column
 * @method     ChildUserQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildUserQuery orderByStatusMessage($order = Criteria::ASC) Order by the status_message column
 * @method     ChildUserQuery orderByActive($order = Criteria::ASC) Order by the active column
 * @method     ChildUserQuery orderByLastActive($order = Criteria::ASC) Order by the last_active column
 * @method     ChildUserQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildUserQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildUserQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildUserQuery groupById() Group by the id column
 * @method     ChildUserQuery groupByUsername() Group by the username column
 * @method     ChildUserQuery groupByDisplayName() Group by the display_name column
 * @method     ChildUserQuery groupByStatus() Group by the status column
 * @method     ChildUserQuery groupByStatusMessage() Group by the status_message column
 * @method     ChildUserQuery groupByActive() Group by the active column
 * @method     ChildUserQuery groupByLastActive() Group by the last_active column
 * @method     ChildUserQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildUserQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildUserQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildUserQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserQuery leftJoinVaccinationHistory($relationAlias = null) Adds a LEFT JOIN clause to the query using the VaccinationHistory relation
 * @method     ChildUserQuery rightJoinVaccinationHistory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VaccinationHistory relation
 * @method     ChildUserQuery innerJoinVaccinationHistory($relationAlias = null) Adds a INNER JOIN clause to the query using the VaccinationHistory relation
 *
 * @method     ChildUserQuery joinWithVaccinationHistory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the VaccinationHistory relation
 *
 * @method     ChildUserQuery leftJoinWithVaccinationHistory() Adds a LEFT JOIN clause and with to the query using the VaccinationHistory relation
 * @method     ChildUserQuery rightJoinWithVaccinationHistory() Adds a RIGHT JOIN clause and with to the query using the VaccinationHistory relation
 * @method     ChildUserQuery innerJoinWithVaccinationHistory() Adds a INNER JOIN clause and with to the query using the VaccinationHistory relation
 *
 * @method     ChildUserQuery leftJoinPhysicalExamination($relationAlias = null) Adds a LEFT JOIN clause to the query using the PhysicalExamination relation
 * @method     ChildUserQuery rightJoinPhysicalExamination($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PhysicalExamination relation
 * @method     ChildUserQuery innerJoinPhysicalExamination($relationAlias = null) Adds a INNER JOIN clause to the query using the PhysicalExamination relation
 *
 * @method     ChildUserQuery joinWithPhysicalExamination($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PhysicalExamination relation
 *
 * @method     ChildUserQuery leftJoinWithPhysicalExamination() Adds a LEFT JOIN clause and with to the query using the PhysicalExamination relation
 * @method     ChildUserQuery rightJoinWithPhysicalExamination() Adds a RIGHT JOIN clause and with to the query using the PhysicalExamination relation
 * @method     ChildUserQuery innerJoinWithPhysicalExamination() Adds a INNER JOIN clause and with to the query using the PhysicalExamination relation
 *
 * @method     ChildUserQuery leftJoinDewormingHistory($relationAlias = null) Adds a LEFT JOIN clause to the query using the DewormingHistory relation
 * @method     ChildUserQuery rightJoinDewormingHistory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DewormingHistory relation
 * @method     ChildUserQuery innerJoinDewormingHistory($relationAlias = null) Adds a INNER JOIN clause to the query using the DewormingHistory relation
 *
 * @method     ChildUserQuery joinWithDewormingHistory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DewormingHistory relation
 *
 * @method     ChildUserQuery leftJoinWithDewormingHistory() Adds a LEFT JOIN clause and with to the query using the DewormingHistory relation
 * @method     ChildUserQuery rightJoinWithDewormingHistory() Adds a RIGHT JOIN clause and with to the query using the DewormingHistory relation
 * @method     ChildUserQuery innerJoinWithDewormingHistory() Adds a INNER JOIN clause and with to the query using the DewormingHistory relation
 *
 * @method     ChildUserQuery leftJoinMedicalHistory($relationAlias = null) Adds a LEFT JOIN clause to the query using the MedicalHistory relation
 * @method     ChildUserQuery rightJoinMedicalHistory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MedicalHistory relation
 * @method     ChildUserQuery innerJoinMedicalHistory($relationAlias = null) Adds a INNER JOIN clause to the query using the MedicalHistory relation
 *
 * @method     ChildUserQuery joinWithMedicalHistory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MedicalHistory relation
 *
 * @method     ChildUserQuery leftJoinWithMedicalHistory() Adds a LEFT JOIN clause and with to the query using the MedicalHistory relation
 * @method     ChildUserQuery rightJoinWithMedicalHistory() Adds a RIGHT JOIN clause and with to the query using the MedicalHistory relation
 * @method     ChildUserQuery innerJoinWithMedicalHistory() Adds a INNER JOIN clause and with to the query using the MedicalHistory relation
 *
 * @method     ChildUserQuery leftJoinPrescription($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prescription relation
 * @method     ChildUserQuery rightJoinPrescription($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prescription relation
 * @method     ChildUserQuery innerJoinPrescription($relationAlias = null) Adds a INNER JOIN clause to the query using the Prescription relation
 *
 * @method     ChildUserQuery joinWithPrescription($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prescription relation
 *
 * @method     ChildUserQuery leftJoinWithPrescription() Adds a LEFT JOIN clause and with to the query using the Prescription relation
 * @method     ChildUserQuery rightJoinWithPrescription() Adds a RIGHT JOIN clause and with to the query using the Prescription relation
 * @method     ChildUserQuery innerJoinWithPrescription() Adds a INNER JOIN clause and with to the query using the Prescription relation
 *
 * @method     ChildUserQuery leftJoinUserHasTenant($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserHasTenant relation
 * @method     ChildUserQuery rightJoinUserHasTenant($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserHasTenant relation
 * @method     ChildUserQuery innerJoinUserHasTenant($relationAlias = null) Adds a INNER JOIN clause to the query using the UserHasTenant relation
 *
 * @method     ChildUserQuery joinWithUserHasTenant($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserHasTenant relation
 *
 * @method     ChildUserQuery leftJoinWithUserHasTenant() Adds a LEFT JOIN clause and with to the query using the UserHasTenant relation
 * @method     ChildUserQuery rightJoinWithUserHasTenant() Adds a RIGHT JOIN clause and with to the query using the UserHasTenant relation
 * @method     ChildUserQuery innerJoinWithUserHasTenant() Adds a INNER JOIN clause and with to the query using the UserHasTenant relation
 *
 * @method     ChildUserQuery leftJoinAuthIdentity($relationAlias = null) Adds a LEFT JOIN clause to the query using the AuthIdentity relation
 * @method     ChildUserQuery rightJoinAuthIdentity($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AuthIdentity relation
 * @method     ChildUserQuery innerJoinAuthIdentity($relationAlias = null) Adds a INNER JOIN clause to the query using the AuthIdentity relation
 *
 * @method     ChildUserQuery joinWithAuthIdentity($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the AuthIdentity relation
 *
 * @method     ChildUserQuery leftJoinWithAuthIdentity() Adds a LEFT JOIN clause and with to the query using the AuthIdentity relation
 * @method     ChildUserQuery rightJoinWithAuthIdentity() Adds a RIGHT JOIN clause and with to the query using the AuthIdentity relation
 * @method     ChildUserQuery innerJoinWithAuthIdentity() Adds a INNER JOIN clause and with to the query using the AuthIdentity relation
 *
 * @method     ChildUserQuery leftJoinAuthLogin($relationAlias = null) Adds a LEFT JOIN clause to the query using the AuthLogin relation
 * @method     ChildUserQuery rightJoinAuthLogin($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AuthLogin relation
 * @method     ChildUserQuery innerJoinAuthLogin($relationAlias = null) Adds a INNER JOIN clause to the query using the AuthLogin relation
 *
 * @method     ChildUserQuery joinWithAuthLogin($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the AuthLogin relation
 *
 * @method     ChildUserQuery leftJoinWithAuthLogin() Adds a LEFT JOIN clause and with to the query using the AuthLogin relation
 * @method     ChildUserQuery rightJoinWithAuthLogin() Adds a RIGHT JOIN clause and with to the query using the AuthLogin relation
 * @method     ChildUserQuery innerJoinWithAuthLogin() Adds a INNER JOIN clause and with to the query using the AuthLogin relation
 *
 * @method     ChildUserQuery leftJoinAuthTokenLogin($relationAlias = null) Adds a LEFT JOIN clause to the query using the AuthTokenLogin relation
 * @method     ChildUserQuery rightJoinAuthTokenLogin($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AuthTokenLogin relation
 * @method     ChildUserQuery innerJoinAuthTokenLogin($relationAlias = null) Adds a INNER JOIN clause to the query using the AuthTokenLogin relation
 *
 * @method     ChildUserQuery joinWithAuthTokenLogin($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the AuthTokenLogin relation
 *
 * @method     ChildUserQuery leftJoinWithAuthTokenLogin() Adds a LEFT JOIN clause and with to the query using the AuthTokenLogin relation
 * @method     ChildUserQuery rightJoinWithAuthTokenLogin() Adds a RIGHT JOIN clause and with to the query using the AuthTokenLogin relation
 * @method     ChildUserQuery innerJoinWithAuthTokenLogin() Adds a INNER JOIN clause and with to the query using the AuthTokenLogin relation
 *
 * @method     ChildUserQuery leftJoinAuthRememberToken($relationAlias = null) Adds a LEFT JOIN clause to the query using the AuthRememberToken relation
 * @method     ChildUserQuery rightJoinAuthRememberToken($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AuthRememberToken relation
 * @method     ChildUserQuery innerJoinAuthRememberToken($relationAlias = null) Adds a INNER JOIN clause to the query using the AuthRememberToken relation
 *
 * @method     ChildUserQuery joinWithAuthRememberToken($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the AuthRememberToken relation
 *
 * @method     ChildUserQuery leftJoinWithAuthRememberToken() Adds a LEFT JOIN clause and with to the query using the AuthRememberToken relation
 * @method     ChildUserQuery rightJoinWithAuthRememberToken() Adds a RIGHT JOIN clause and with to the query using the AuthRememberToken relation
 * @method     ChildUserQuery innerJoinWithAuthRememberToken() Adds a INNER JOIN clause and with to the query using the AuthRememberToken relation
 *
 * @method     ChildUserQuery leftJoinAuthGroupsUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the AuthGroupsUser relation
 * @method     ChildUserQuery rightJoinAuthGroupsUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AuthGroupsUser relation
 * @method     ChildUserQuery innerJoinAuthGroupsUser($relationAlias = null) Adds a INNER JOIN clause to the query using the AuthGroupsUser relation
 *
 * @method     ChildUserQuery joinWithAuthGroupsUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the AuthGroupsUser relation
 *
 * @method     ChildUserQuery leftJoinWithAuthGroupsUser() Adds a LEFT JOIN clause and with to the query using the AuthGroupsUser relation
 * @method     ChildUserQuery rightJoinWithAuthGroupsUser() Adds a RIGHT JOIN clause and with to the query using the AuthGroupsUser relation
 * @method     ChildUserQuery innerJoinWithAuthGroupsUser() Adds a INNER JOIN clause and with to the query using the AuthGroupsUser relation
 *
 * @method     ChildUserQuery leftJoinAuthPermissionsUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the AuthPermissionsUser relation
 * @method     ChildUserQuery rightJoinAuthPermissionsUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AuthPermissionsUser relation
 * @method     ChildUserQuery innerJoinAuthPermissionsUser($relationAlias = null) Adds a INNER JOIN clause to the query using the AuthPermissionsUser relation
 *
 * @method     ChildUserQuery joinWithAuthPermissionsUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the AuthPermissionsUser relation
 *
 * @method     ChildUserQuery leftJoinWithAuthPermissionsUser() Adds a LEFT JOIN clause and with to the query using the AuthPermissionsUser relation
 * @method     ChildUserQuery rightJoinWithAuthPermissionsUser() Adds a RIGHT JOIN clause and with to the query using the AuthPermissionsUser relation
 * @method     ChildUserQuery innerJoinWithAuthPermissionsUser() Adds a INNER JOIN clause and with to the query using the AuthPermissionsUser relation
 *
 * @method     \VaccinationHistoryQuery|\PhysicalExaminationQuery|\DewormingHistoryQuery|\MedicalHistoryQuery|\PrescriptionQuery|\UserHasTenantQuery|\AuthIdentityQuery|\AuthLoginQuery|\AuthTokenLoginQuery|\AuthRememberTokenQuery|\AuthGroupsUserQuery|\AuthPermissionsUserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUser|null findOne(?ConnectionInterface $con = null) Return the first ChildUser matching the query
 * @method     ChildUser findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildUser matching the query, or a new ChildUser object populated from the query conditions when no match is found
 *
 * @method     ChildUser|null findOneById(int $id) Return the first ChildUser filtered by the id column
 * @method     ChildUser|null findOneByUsername(string $username) Return the first ChildUser filtered by the username column
 * @method     ChildUser|null findOneByDisplayName(string $display_name) Return the first ChildUser filtered by the display_name column
 * @method     ChildUser|null findOneByStatus(string $status) Return the first ChildUser filtered by the status column
 * @method     ChildUser|null findOneByStatusMessage(string $status_message) Return the first ChildUser filtered by the status_message column
 * @method     ChildUser|null findOneByActive(int $active) Return the first ChildUser filtered by the active column
 * @method     ChildUser|null findOneByLastActive(string $last_active) Return the first ChildUser filtered by the last_active column
 * @method     ChildUser|null findOneByDeletedAt(string $deleted_at) Return the first ChildUser filtered by the deleted_at column
 * @method     ChildUser|null findOneByCreatedAt(string $created_at) Return the first ChildUser filtered by the created_at column
 * @method     ChildUser|null findOneByUpdatedAt(string $updated_at) Return the first ChildUser filtered by the updated_at column *

 * @method     ChildUser requirePk($key, ?ConnectionInterface $con = null) Return the ChildUser by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOne(?ConnectionInterface $con = null) Return the first ChildUser matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUser requireOneById(int $id) Return the first ChildUser filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByUsername(string $username) Return the first ChildUser filtered by the username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByDisplayName(string $display_name) Return the first ChildUser filtered by the display_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByStatus(string $status) Return the first ChildUser filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByStatusMessage(string $status_message) Return the first ChildUser filtered by the status_message column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByActive(int $active) Return the first ChildUser filtered by the active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByLastActive(string $last_active) Return the first ChildUser filtered by the last_active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByDeletedAt(string $deleted_at) Return the first ChildUser filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByCreatedAt(string $created_at) Return the first ChildUser filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByUpdatedAt(string $updated_at) Return the first ChildUser filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUser[]|Collection find(?ConnectionInterface $con = null) Return ChildUser objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildUser> find(?ConnectionInterface $con = null) Return ChildUser objects based on current ModelCriteria
 * @method     ChildUser[]|Collection findById(int $id) Return ChildUser objects filtered by the id column
 * @psalm-method Collection&\Traversable<ChildUser> findById(int $id) Return ChildUser objects filtered by the id column
 * @method     ChildUser[]|Collection findByUsername(string $username) Return ChildUser objects filtered by the username column
 * @psalm-method Collection&\Traversable<ChildUser> findByUsername(string $username) Return ChildUser objects filtered by the username column
 * @method     ChildUser[]|Collection findByDisplayName(string $display_name) Return ChildUser objects filtered by the display_name column
 * @psalm-method Collection&\Traversable<ChildUser> findByDisplayName(string $display_name) Return ChildUser objects filtered by the display_name column
 * @method     ChildUser[]|Collection findByStatus(string $status) Return ChildUser objects filtered by the status column
 * @psalm-method Collection&\Traversable<ChildUser> findByStatus(string $status) Return ChildUser objects filtered by the status column
 * @method     ChildUser[]|Collection findByStatusMessage(string $status_message) Return ChildUser objects filtered by the status_message column
 * @psalm-method Collection&\Traversable<ChildUser> findByStatusMessage(string $status_message) Return ChildUser objects filtered by the status_message column
 * @method     ChildUser[]|Collection findByActive(int $active) Return ChildUser objects filtered by the active column
 * @psalm-method Collection&\Traversable<ChildUser> findByActive(int $active) Return ChildUser objects filtered by the active column
 * @method     ChildUser[]|Collection findByLastActive(string $last_active) Return ChildUser objects filtered by the last_active column
 * @psalm-method Collection&\Traversable<ChildUser> findByLastActive(string $last_active) Return ChildUser objects filtered by the last_active column
 * @method     ChildUser[]|Collection findByDeletedAt(string $deleted_at) Return ChildUser objects filtered by the deleted_at column
 * @psalm-method Collection&\Traversable<ChildUser> findByDeletedAt(string $deleted_at) Return ChildUser objects filtered by the deleted_at column
 * @method     ChildUser[]|Collection findByCreatedAt(string $created_at) Return ChildUser objects filtered by the created_at column
 * @psalm-method Collection&\Traversable<ChildUser> findByCreatedAt(string $created_at) Return ChildUser objects filtered by the created_at column
 * @method     ChildUser[]|Collection findByUpdatedAt(string $updated_at) Return ChildUser objects filtered by the updated_at column
 * @psalm-method Collection&\Traversable<ChildUser> findByUpdatedAt(string $updated_at) Return ChildUser objects filtered by the updated_at column
 * @method     ChildUser[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildUser> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\UserQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\User', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildUserQuery) {
            return $criteria;
        }
        $query = new ChildUserQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUser|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUser A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, username, display_name, status, status_message, active, last_active, deleted_at, created_at, updated_at FROM users WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUser $obj */
            $obj = new ChildUser();
            $obj->hydrate($row);
            UserTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildUser|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(UserTableMap::COL_ID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(UserTableMap::COL_ID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterById($id = null, ?string $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UserTableMap::COL_ID, $id, $comparison);

        return $this;
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE username LIKE '%fooValue%'
     * $query->filterByUsername(['foo', 'bar']); // WHERE username IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $username The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUsername($username = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UserTableMap::COL_USERNAME, $username, $comparison);

        return $this;
    }

    /**
     * Filter the query on the display_name column
     *
     * Example usage:
     * <code>
     * $query->filterByDisplayName('fooValue');   // WHERE display_name = 'fooValue'
     * $query->filterByDisplayName('%fooValue%', Criteria::LIKE); // WHERE display_name LIKE '%fooValue%'
     * $query->filterByDisplayName(['foo', 'bar']); // WHERE display_name IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $displayName The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDisplayName($displayName = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($displayName)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UserTableMap::COL_DISPLAY_NAME, $displayName, $comparison);

        return $this;
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%', Criteria::LIKE); // WHERE status LIKE '%fooValue%'
     * $query->filterByStatus(['foo', 'bar']); // WHERE status IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $status The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByStatus($status = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UserTableMap::COL_STATUS, $status, $comparison);

        return $this;
    }

    /**
     * Filter the query on the status_message column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusMessage('fooValue');   // WHERE status_message = 'fooValue'
     * $query->filterByStatusMessage('%fooValue%', Criteria::LIKE); // WHERE status_message LIKE '%fooValue%'
     * $query->filterByStatusMessage(['foo', 'bar']); // WHERE status_message IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $statusMessage The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByStatusMessage($statusMessage = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statusMessage)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UserTableMap::COL_STATUS_MESSAGE, $statusMessage, $comparison);

        return $this;
    }

    /**
     * Filter the query on the active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(1234); // WHERE active = 1234
     * $query->filterByActive(array(12, 34)); // WHERE active IN (12, 34)
     * $query->filterByActive(array('min' => 12)); // WHERE active > 12
     * </code>
     *
     * @param mixed $active The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByActive($active = null, ?string $comparison = null)
    {
        if (is_array($active)) {
            $useMinMax = false;
            if (isset($active['min'])) {
                $this->addUsingAlias(UserTableMap::COL_ACTIVE, $active['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($active['max'])) {
                $this->addUsingAlias(UserTableMap::COL_ACTIVE, $active['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UserTableMap::COL_ACTIVE, $active, $comparison);

        return $this;
    }

    /**
     * Filter the query on the last_active column
     *
     * Example usage:
     * <code>
     * $query->filterByLastActive('2011-03-14'); // WHERE last_active = '2011-03-14'
     * $query->filterByLastActive('now'); // WHERE last_active = '2011-03-14'
     * $query->filterByLastActive(array('max' => 'yesterday')); // WHERE last_active > '2011-03-13'
     * </code>
     *
     * @param mixed $lastActive The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByLastActive($lastActive = null, ?string $comparison = null)
    {
        if (is_array($lastActive)) {
            $useMinMax = false;
            if (isset($lastActive['min'])) {
                $this->addUsingAlias(UserTableMap::COL_LAST_ACTIVE, $lastActive['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastActive['max'])) {
                $this->addUsingAlias(UserTableMap::COL_LAST_ACTIVE, $lastActive['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UserTableMap::COL_LAST_ACTIVE, $lastActive, $comparison);

        return $this;
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, ?string $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(UserTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(UserTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UserTableMap::COL_DELETED_AT, $deletedAt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, ?string $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(UserTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(UserTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UserTableMap::COL_CREATED_AT, $createdAt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, ?string $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(UserTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(UserTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UserTableMap::COL_UPDATED_AT, $updatedAt, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \VaccinationHistory object
     *
     * @param \VaccinationHistory|ObjectCollection $vaccinationHistory the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVaccinationHistory($vaccinationHistory, ?string $comparison = null)
    {
        if ($vaccinationHistory instanceof \VaccinationHistory) {
            $this
                ->addUsingAlias(UserTableMap::COL_ID, $vaccinationHistory->getUserId(), $comparison);

            return $this;
        } elseif ($vaccinationHistory instanceof ObjectCollection) {
            $this
                ->useVaccinationHistoryQuery()
                ->filterByPrimaryKeys($vaccinationHistory->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByVaccinationHistory() only accepts arguments of type \VaccinationHistory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VaccinationHistory relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinVaccinationHistory(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VaccinationHistory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VaccinationHistory');
        }

        return $this;
    }

    /**
     * Use the VaccinationHistory relation VaccinationHistory object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VaccinationHistoryQuery A secondary query class using the current class as primary query
     */
    public function useVaccinationHistoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVaccinationHistory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VaccinationHistory', '\VaccinationHistoryQuery');
    }

    /**
     * Use the VaccinationHistory relation VaccinationHistory object
     *
     * @param callable(\VaccinationHistoryQuery):\VaccinationHistoryQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withVaccinationHistoryQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useVaccinationHistoryQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to VaccinationHistory table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \VaccinationHistoryQuery The inner query object of the EXISTS statement
     */
    public function useVaccinationHistoryExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('VaccinationHistory', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to VaccinationHistory table for a NOT EXISTS query.
     *
     * @see useVaccinationHistoryExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \VaccinationHistoryQuery The inner query object of the NOT EXISTS statement
     */
    public function useVaccinationHistoryNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('VaccinationHistory', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \PhysicalExamination object
     *
     * @param \PhysicalExamination|ObjectCollection $physicalExamination the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPhysicalExamination($physicalExamination, ?string $comparison = null)
    {
        if ($physicalExamination instanceof \PhysicalExamination) {
            $this
                ->addUsingAlias(UserTableMap::COL_ID, $physicalExamination->getUserId(), $comparison);

            return $this;
        } elseif ($physicalExamination instanceof ObjectCollection) {
            $this
                ->usePhysicalExaminationQuery()
                ->filterByPrimaryKeys($physicalExamination->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByPhysicalExamination() only accepts arguments of type \PhysicalExamination or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PhysicalExamination relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPhysicalExamination(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PhysicalExamination');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PhysicalExamination');
        }

        return $this;
    }

    /**
     * Use the PhysicalExamination relation PhysicalExamination object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PhysicalExaminationQuery A secondary query class using the current class as primary query
     */
    public function usePhysicalExaminationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPhysicalExamination($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PhysicalExamination', '\PhysicalExaminationQuery');
    }

    /**
     * Use the PhysicalExamination relation PhysicalExamination object
     *
     * @param callable(\PhysicalExaminationQuery):\PhysicalExaminationQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPhysicalExaminationQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePhysicalExaminationQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to PhysicalExamination table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PhysicalExaminationQuery The inner query object of the EXISTS statement
     */
    public function usePhysicalExaminationExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('PhysicalExamination', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to PhysicalExamination table for a NOT EXISTS query.
     *
     * @see usePhysicalExaminationExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PhysicalExaminationQuery The inner query object of the NOT EXISTS statement
     */
    public function usePhysicalExaminationNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('PhysicalExamination', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \DewormingHistory object
     *
     * @param \DewormingHistory|ObjectCollection $dewormingHistory the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDewormingHistory($dewormingHistory, ?string $comparison = null)
    {
        if ($dewormingHistory instanceof \DewormingHistory) {
            $this
                ->addUsingAlias(UserTableMap::COL_ID, $dewormingHistory->getUserId(), $comparison);

            return $this;
        } elseif ($dewormingHistory instanceof ObjectCollection) {
            $this
                ->useDewormingHistoryQuery()
                ->filterByPrimaryKeys($dewormingHistory->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByDewormingHistory() only accepts arguments of type \DewormingHistory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DewormingHistory relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinDewormingHistory(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DewormingHistory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DewormingHistory');
        }

        return $this;
    }

    /**
     * Use the DewormingHistory relation DewormingHistory object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \DewormingHistoryQuery A secondary query class using the current class as primary query
     */
    public function useDewormingHistoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDewormingHistory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DewormingHistory', '\DewormingHistoryQuery');
    }

    /**
     * Use the DewormingHistory relation DewormingHistory object
     *
     * @param callable(\DewormingHistoryQuery):\DewormingHistoryQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withDewormingHistoryQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useDewormingHistoryQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to DewormingHistory table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \DewormingHistoryQuery The inner query object of the EXISTS statement
     */
    public function useDewormingHistoryExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('DewormingHistory', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to DewormingHistory table for a NOT EXISTS query.
     *
     * @see useDewormingHistoryExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \DewormingHistoryQuery The inner query object of the NOT EXISTS statement
     */
    public function useDewormingHistoryNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('DewormingHistory', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \MedicalHistory object
     *
     * @param \MedicalHistory|ObjectCollection $medicalHistory the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicalHistory($medicalHistory, ?string $comparison = null)
    {
        if ($medicalHistory instanceof \MedicalHistory) {
            $this
                ->addUsingAlias(UserTableMap::COL_ID, $medicalHistory->getUserId(), $comparison);

            return $this;
        } elseif ($medicalHistory instanceof ObjectCollection) {
            $this
                ->useMedicalHistoryQuery()
                ->filterByPrimaryKeys($medicalHistory->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByMedicalHistory() only accepts arguments of type \MedicalHistory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MedicalHistory relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinMedicalHistory(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MedicalHistory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MedicalHistory');
        }

        return $this;
    }

    /**
     * Use the MedicalHistory relation MedicalHistory object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MedicalHistoryQuery A secondary query class using the current class as primary query
     */
    public function useMedicalHistoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMedicalHistory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MedicalHistory', '\MedicalHistoryQuery');
    }

    /**
     * Use the MedicalHistory relation MedicalHistory object
     *
     * @param callable(\MedicalHistoryQuery):\MedicalHistoryQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withMedicalHistoryQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useMedicalHistoryQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to MedicalHistory table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \MedicalHistoryQuery The inner query object of the EXISTS statement
     */
    public function useMedicalHistoryExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('MedicalHistory', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to MedicalHistory table for a NOT EXISTS query.
     *
     * @see useMedicalHistoryExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \MedicalHistoryQuery The inner query object of the NOT EXISTS statement
     */
    public function useMedicalHistoryNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('MedicalHistory', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \Prescription object
     *
     * @param \Prescription|ObjectCollection $prescription the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrescription($prescription, ?string $comparison = null)
    {
        if ($prescription instanceof \Prescription) {
            $this
                ->addUsingAlias(UserTableMap::COL_ID, $prescription->getUserId(), $comparison);

            return $this;
        } elseif ($prescription instanceof ObjectCollection) {
            $this
                ->usePrescriptionQuery()
                ->filterByPrimaryKeys($prescription->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByPrescription() only accepts arguments of type \Prescription or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prescription relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPrescription(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prescription');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prescription');
        }

        return $this;
    }

    /**
     * Use the Prescription relation Prescription object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrescriptionQuery A secondary query class using the current class as primary query
     */
    public function usePrescriptionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrescription($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prescription', '\PrescriptionQuery');
    }

    /**
     * Use the Prescription relation Prescription object
     *
     * @param callable(\PrescriptionQuery):\PrescriptionQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPrescriptionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePrescriptionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Prescription table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PrescriptionQuery The inner query object of the EXISTS statement
     */
    public function usePrescriptionExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Prescription', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Prescription table for a NOT EXISTS query.
     *
     * @see usePrescriptionExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PrescriptionQuery The inner query object of the NOT EXISTS statement
     */
    public function usePrescriptionNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Prescription', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \UserHasTenant object
     *
     * @param \UserHasTenant|ObjectCollection $userHasTenant the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUserHasTenant($userHasTenant, ?string $comparison = null)
    {
        if ($userHasTenant instanceof \UserHasTenant) {
            $this
                ->addUsingAlias(UserTableMap::COL_ID, $userHasTenant->getUserId(), $comparison);

            return $this;
        } elseif ($userHasTenant instanceof ObjectCollection) {
            $this
                ->useUserHasTenantQuery()
                ->filterByPrimaryKeys($userHasTenant->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByUserHasTenant() only accepts arguments of type \UserHasTenant or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserHasTenant relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinUserHasTenant(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserHasTenant');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserHasTenant');
        }

        return $this;
    }

    /**
     * Use the UserHasTenant relation UserHasTenant object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserHasTenantQuery A secondary query class using the current class as primary query
     */
    public function useUserHasTenantQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserHasTenant($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserHasTenant', '\UserHasTenantQuery');
    }

    /**
     * Use the UserHasTenant relation UserHasTenant object
     *
     * @param callable(\UserHasTenantQuery):\UserHasTenantQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUserHasTenantQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUserHasTenantQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to UserHasTenant table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \UserHasTenantQuery The inner query object of the EXISTS statement
     */
    public function useUserHasTenantExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('UserHasTenant', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to UserHasTenant table for a NOT EXISTS query.
     *
     * @see useUserHasTenantExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \UserHasTenantQuery The inner query object of the NOT EXISTS statement
     */
    public function useUserHasTenantNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('UserHasTenant', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \AuthIdentity object
     *
     * @param \AuthIdentity|ObjectCollection $authIdentity the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByAuthIdentity($authIdentity, ?string $comparison = null)
    {
        if ($authIdentity instanceof \AuthIdentity) {
            $this
                ->addUsingAlias(UserTableMap::COL_ID, $authIdentity->getUserId(), $comparison);

            return $this;
        } elseif ($authIdentity instanceof ObjectCollection) {
            $this
                ->useAuthIdentityQuery()
                ->filterByPrimaryKeys($authIdentity->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByAuthIdentity() only accepts arguments of type \AuthIdentity or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AuthIdentity relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinAuthIdentity(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AuthIdentity');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AuthIdentity');
        }

        return $this;
    }

    /**
     * Use the AuthIdentity relation AuthIdentity object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \AuthIdentityQuery A secondary query class using the current class as primary query
     */
    public function useAuthIdentityQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAuthIdentity($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AuthIdentity', '\AuthIdentityQuery');
    }

    /**
     * Use the AuthIdentity relation AuthIdentity object
     *
     * @param callable(\AuthIdentityQuery):\AuthIdentityQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withAuthIdentityQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useAuthIdentityQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to AuthIdentity table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \AuthIdentityQuery The inner query object of the EXISTS statement
     */
    public function useAuthIdentityExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('AuthIdentity', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to AuthIdentity table for a NOT EXISTS query.
     *
     * @see useAuthIdentityExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \AuthIdentityQuery The inner query object of the NOT EXISTS statement
     */
    public function useAuthIdentityNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('AuthIdentity', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \AuthLogin object
     *
     * @param \AuthLogin|ObjectCollection $authLogin the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByAuthLogin($authLogin, ?string $comparison = null)
    {
        if ($authLogin instanceof \AuthLogin) {
            $this
                ->addUsingAlias(UserTableMap::COL_ID, $authLogin->getUserId(), $comparison);

            return $this;
        } elseif ($authLogin instanceof ObjectCollection) {
            $this
                ->useAuthLoginQuery()
                ->filterByPrimaryKeys($authLogin->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByAuthLogin() only accepts arguments of type \AuthLogin or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AuthLogin relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinAuthLogin(?string $relationAlias = null, ?string $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AuthLogin');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AuthLogin');
        }

        return $this;
    }

    /**
     * Use the AuthLogin relation AuthLogin object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \AuthLoginQuery A secondary query class using the current class as primary query
     */
    public function useAuthLoginQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAuthLogin($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AuthLogin', '\AuthLoginQuery');
    }

    /**
     * Use the AuthLogin relation AuthLogin object
     *
     * @param callable(\AuthLoginQuery):\AuthLoginQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withAuthLoginQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useAuthLoginQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to AuthLogin table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \AuthLoginQuery The inner query object of the EXISTS statement
     */
    public function useAuthLoginExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('AuthLogin', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to AuthLogin table for a NOT EXISTS query.
     *
     * @see useAuthLoginExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \AuthLoginQuery The inner query object of the NOT EXISTS statement
     */
    public function useAuthLoginNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('AuthLogin', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \AuthTokenLogin object
     *
     * @param \AuthTokenLogin|ObjectCollection $authTokenLogin the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByAuthTokenLogin($authTokenLogin, ?string $comparison = null)
    {
        if ($authTokenLogin instanceof \AuthTokenLogin) {
            $this
                ->addUsingAlias(UserTableMap::COL_ID, $authTokenLogin->getUserId(), $comparison);

            return $this;
        } elseif ($authTokenLogin instanceof ObjectCollection) {
            $this
                ->useAuthTokenLoginQuery()
                ->filterByPrimaryKeys($authTokenLogin->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByAuthTokenLogin() only accepts arguments of type \AuthTokenLogin or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AuthTokenLogin relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinAuthTokenLogin(?string $relationAlias = null, ?string $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AuthTokenLogin');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AuthTokenLogin');
        }

        return $this;
    }

    /**
     * Use the AuthTokenLogin relation AuthTokenLogin object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \AuthTokenLoginQuery A secondary query class using the current class as primary query
     */
    public function useAuthTokenLoginQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAuthTokenLogin($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AuthTokenLogin', '\AuthTokenLoginQuery');
    }

    /**
     * Use the AuthTokenLogin relation AuthTokenLogin object
     *
     * @param callable(\AuthTokenLoginQuery):\AuthTokenLoginQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withAuthTokenLoginQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useAuthTokenLoginQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to AuthTokenLogin table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \AuthTokenLoginQuery The inner query object of the EXISTS statement
     */
    public function useAuthTokenLoginExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('AuthTokenLogin', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to AuthTokenLogin table for a NOT EXISTS query.
     *
     * @see useAuthTokenLoginExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \AuthTokenLoginQuery The inner query object of the NOT EXISTS statement
     */
    public function useAuthTokenLoginNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('AuthTokenLogin', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \AuthRememberToken object
     *
     * @param \AuthRememberToken|ObjectCollection $authRememberToken the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByAuthRememberToken($authRememberToken, ?string $comparison = null)
    {
        if ($authRememberToken instanceof \AuthRememberToken) {
            $this
                ->addUsingAlias(UserTableMap::COL_ID, $authRememberToken->getUserId(), $comparison);

            return $this;
        } elseif ($authRememberToken instanceof ObjectCollection) {
            $this
                ->useAuthRememberTokenQuery()
                ->filterByPrimaryKeys($authRememberToken->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByAuthRememberToken() only accepts arguments of type \AuthRememberToken or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AuthRememberToken relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinAuthRememberToken(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AuthRememberToken');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AuthRememberToken');
        }

        return $this;
    }

    /**
     * Use the AuthRememberToken relation AuthRememberToken object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \AuthRememberTokenQuery A secondary query class using the current class as primary query
     */
    public function useAuthRememberTokenQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAuthRememberToken($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AuthRememberToken', '\AuthRememberTokenQuery');
    }

    /**
     * Use the AuthRememberToken relation AuthRememberToken object
     *
     * @param callable(\AuthRememberTokenQuery):\AuthRememberTokenQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withAuthRememberTokenQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useAuthRememberTokenQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to AuthRememberToken table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \AuthRememberTokenQuery The inner query object of the EXISTS statement
     */
    public function useAuthRememberTokenExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('AuthRememberToken', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to AuthRememberToken table for a NOT EXISTS query.
     *
     * @see useAuthRememberTokenExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \AuthRememberTokenQuery The inner query object of the NOT EXISTS statement
     */
    public function useAuthRememberTokenNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('AuthRememberToken', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \AuthGroupsUser object
     *
     * @param \AuthGroupsUser|ObjectCollection $authGroupsUser the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByAuthGroupsUser($authGroupsUser, ?string $comparison = null)
    {
        if ($authGroupsUser instanceof \AuthGroupsUser) {
            $this
                ->addUsingAlias(UserTableMap::COL_ID, $authGroupsUser->getUserId(), $comparison);

            return $this;
        } elseif ($authGroupsUser instanceof ObjectCollection) {
            $this
                ->useAuthGroupsUserQuery()
                ->filterByPrimaryKeys($authGroupsUser->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByAuthGroupsUser() only accepts arguments of type \AuthGroupsUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AuthGroupsUser relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinAuthGroupsUser(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AuthGroupsUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AuthGroupsUser');
        }

        return $this;
    }

    /**
     * Use the AuthGroupsUser relation AuthGroupsUser object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \AuthGroupsUserQuery A secondary query class using the current class as primary query
     */
    public function useAuthGroupsUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAuthGroupsUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AuthGroupsUser', '\AuthGroupsUserQuery');
    }

    /**
     * Use the AuthGroupsUser relation AuthGroupsUser object
     *
     * @param callable(\AuthGroupsUserQuery):\AuthGroupsUserQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withAuthGroupsUserQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useAuthGroupsUserQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to AuthGroupsUser table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \AuthGroupsUserQuery The inner query object of the EXISTS statement
     */
    public function useAuthGroupsUserExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('AuthGroupsUser', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to AuthGroupsUser table for a NOT EXISTS query.
     *
     * @see useAuthGroupsUserExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \AuthGroupsUserQuery The inner query object of the NOT EXISTS statement
     */
    public function useAuthGroupsUserNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('AuthGroupsUser', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \AuthPermissionsUser object
     *
     * @param \AuthPermissionsUser|ObjectCollection $authPermissionsUser the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByAuthPermissionsUser($authPermissionsUser, ?string $comparison = null)
    {
        if ($authPermissionsUser instanceof \AuthPermissionsUser) {
            $this
                ->addUsingAlias(UserTableMap::COL_ID, $authPermissionsUser->getUserId(), $comparison);

            return $this;
        } elseif ($authPermissionsUser instanceof ObjectCollection) {
            $this
                ->useAuthPermissionsUserQuery()
                ->filterByPrimaryKeys($authPermissionsUser->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByAuthPermissionsUser() only accepts arguments of type \AuthPermissionsUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AuthPermissionsUser relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinAuthPermissionsUser(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AuthPermissionsUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AuthPermissionsUser');
        }

        return $this;
    }

    /**
     * Use the AuthPermissionsUser relation AuthPermissionsUser object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \AuthPermissionsUserQuery A secondary query class using the current class as primary query
     */
    public function useAuthPermissionsUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAuthPermissionsUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AuthPermissionsUser', '\AuthPermissionsUserQuery');
    }

    /**
     * Use the AuthPermissionsUser relation AuthPermissionsUser object
     *
     * @param callable(\AuthPermissionsUserQuery):\AuthPermissionsUserQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withAuthPermissionsUserQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useAuthPermissionsUserQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to AuthPermissionsUser table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \AuthPermissionsUserQuery The inner query object of the EXISTS statement
     */
    public function useAuthPermissionsUserExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('AuthPermissionsUser', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to AuthPermissionsUser table for a NOT EXISTS query.
     *
     * @see useAuthPermissionsUserExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \AuthPermissionsUserQuery The inner query object of the NOT EXISTS statement
     */
    public function useAuthPermissionsUserNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('AuthPermissionsUser', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related Tenant object
     * using the user_has_tenant table as cross reference
     *
     * @param Tenant $tenant the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByTenant($tenant, string $comparison = Criteria::EQUAL)
    {
        $this
            ->useUserHasTenantQuery()
            ->filterByTenant($tenant, $comparison)
            ->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param ChildUser $user Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($user = null)
    {
        if ($user) {
            $this->addUsingAlias(UserTableMap::COL_ID, $user->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the users table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserTableMap::clearInstancePool();
            UserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param int $nbDays Maximum age of the latest update in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        $this->addUsingAlias(UserTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by update date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        $this->addDescendingOrderByColumn(UserTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by update date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        $this->addAscendingOrderByColumn(UserTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by create date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        $this->addDescendingOrderByColumn(UserTableMap::COL_CREATED_AT);

        return $this;
    }

    /**
     * Filter by the latest created
     *
     * @param int $nbDays Maximum age of in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        $this->addUsingAlias(UserTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by create date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        $this->addAscendingOrderByColumn(UserTableMap::COL_CREATED_AT);

        return $this;
    }

}
