<?php

namespace Base;

use \VaccinationPlan as ChildVaccinationPlan;
use \VaccinationPlanQuery as ChildVaccinationPlanQuery;
use \Exception;
use \PDO;
use Map\VaccinationPlanTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'vaccination_plan' table.
 *
 * Vaccination plan based on the tenant configuration. This shall enable the tenent to provide an individual vaccination plan that is shown in the patient datasheet as upcoming vaccinations
 *
 * @method     ChildVaccinationPlanQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildVaccinationPlanQuery orderByTenantUuid($order = Criteria::ASC) Order by the tenant_uuid column
 * @method     ChildVaccinationPlanQuery orderByVaccineUuid($order = Criteria::ASC) Order by the vaccine_uuid column
 *
 * @method     ChildVaccinationPlanQuery groupById() Group by the id column
 * @method     ChildVaccinationPlanQuery groupByTenantUuid() Group by the tenant_uuid column
 * @method     ChildVaccinationPlanQuery groupByVaccineUuid() Group by the vaccine_uuid column
 *
 * @method     ChildVaccinationPlanQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildVaccinationPlanQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildVaccinationPlanQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildVaccinationPlanQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildVaccinationPlanQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildVaccinationPlanQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildVaccinationPlanQuery leftJoinTenant($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tenant relation
 * @method     ChildVaccinationPlanQuery rightJoinTenant($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tenant relation
 * @method     ChildVaccinationPlanQuery innerJoinTenant($relationAlias = null) Adds a INNER JOIN clause to the query using the Tenant relation
 *
 * @method     ChildVaccinationPlanQuery joinWithTenant($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Tenant relation
 *
 * @method     ChildVaccinationPlanQuery leftJoinWithTenant() Adds a LEFT JOIN clause and with to the query using the Tenant relation
 * @method     ChildVaccinationPlanQuery rightJoinWithTenant() Adds a RIGHT JOIN clause and with to the query using the Tenant relation
 * @method     ChildVaccinationPlanQuery innerJoinWithTenant() Adds a INNER JOIN clause and with to the query using the Tenant relation
 *
 * @method     ChildVaccinationPlanQuery leftJoinVaccine($relationAlias = null) Adds a LEFT JOIN clause to the query using the Vaccine relation
 * @method     ChildVaccinationPlanQuery rightJoinVaccine($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Vaccine relation
 * @method     ChildVaccinationPlanQuery innerJoinVaccine($relationAlias = null) Adds a INNER JOIN clause to the query using the Vaccine relation
 *
 * @method     ChildVaccinationPlanQuery joinWithVaccine($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Vaccine relation
 *
 * @method     ChildVaccinationPlanQuery leftJoinWithVaccine() Adds a LEFT JOIN clause and with to the query using the Vaccine relation
 * @method     ChildVaccinationPlanQuery rightJoinWithVaccine() Adds a RIGHT JOIN clause and with to the query using the Vaccine relation
 * @method     ChildVaccinationPlanQuery innerJoinWithVaccine() Adds a INNER JOIN clause and with to the query using the Vaccine relation
 *
 * @method     \TenantQuery|\VaccineQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildVaccinationPlan|null findOne(?ConnectionInterface $con = null) Return the first ChildVaccinationPlan matching the query
 * @method     ChildVaccinationPlan findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildVaccinationPlan matching the query, or a new ChildVaccinationPlan object populated from the query conditions when no match is found
 *
 * @method     ChildVaccinationPlan|null findOneById(string $id) Return the first ChildVaccinationPlan filtered by the id column
 * @method     ChildVaccinationPlan|null findOneByTenantUuid(string $tenant_uuid) Return the first ChildVaccinationPlan filtered by the tenant_uuid column
 * @method     ChildVaccinationPlan|null findOneByVaccineUuid(string $vaccine_uuid) Return the first ChildVaccinationPlan filtered by the vaccine_uuid column *

 * @method     ChildVaccinationPlan requirePk($key, ?ConnectionInterface $con = null) Return the ChildVaccinationPlan by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccinationPlan requireOne(?ConnectionInterface $con = null) Return the first ChildVaccinationPlan matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVaccinationPlan requireOneById(string $id) Return the first ChildVaccinationPlan filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccinationPlan requireOneByTenantUuid(string $tenant_uuid) Return the first ChildVaccinationPlan filtered by the tenant_uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccinationPlan requireOneByVaccineUuid(string $vaccine_uuid) Return the first ChildVaccinationPlan filtered by the vaccine_uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVaccinationPlan[]|Collection find(?ConnectionInterface $con = null) Return ChildVaccinationPlan objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildVaccinationPlan> find(?ConnectionInterface $con = null) Return ChildVaccinationPlan objects based on current ModelCriteria
 * @method     ChildVaccinationPlan[]|Collection findById(string $id) Return ChildVaccinationPlan objects filtered by the id column
 * @psalm-method Collection&\Traversable<ChildVaccinationPlan> findById(string $id) Return ChildVaccinationPlan objects filtered by the id column
 * @method     ChildVaccinationPlan[]|Collection findByTenantUuid(string $tenant_uuid) Return ChildVaccinationPlan objects filtered by the tenant_uuid column
 * @psalm-method Collection&\Traversable<ChildVaccinationPlan> findByTenantUuid(string $tenant_uuid) Return ChildVaccinationPlan objects filtered by the tenant_uuid column
 * @method     ChildVaccinationPlan[]|Collection findByVaccineUuid(string $vaccine_uuid) Return ChildVaccinationPlan objects filtered by the vaccine_uuid column
 * @psalm-method Collection&\Traversable<ChildVaccinationPlan> findByVaccineUuid(string $vaccine_uuid) Return ChildVaccinationPlan objects filtered by the vaccine_uuid column
 * @method     ChildVaccinationPlan[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildVaccinationPlan> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class VaccinationPlanQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\VaccinationPlanQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\VaccinationPlan', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildVaccinationPlanQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildVaccinationPlanQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildVaccinationPlanQuery) {
            return $criteria;
        }
        $query = new ChildVaccinationPlanQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildVaccinationPlan|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VaccinationPlanTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = VaccinationPlanTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVaccinationPlan A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, tenant_uuid, vaccine_uuid FROM vaccination_plan WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildVaccinationPlan $obj */
            $obj = new ChildVaccinationPlan();
            $obj->hydrate($row);
            VaccinationPlanTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildVaccinationPlan|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(VaccinationPlanTableMap::COL_ID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(VaccinationPlanTableMap::COL_ID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%', Criteria::LIKE); // WHERE id LIKE '%fooValue%'
     * $query->filterById(['foo', 'bar']); // WHERE id IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $id The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterById($id = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccinationPlanTableMap::COL_ID, $id, $comparison);

        return $this;
    }

    /**
     * Filter the query on the tenant_uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByTenantUuid('fooValue');   // WHERE tenant_uuid = 'fooValue'
     * $query->filterByTenantUuid('%fooValue%', Criteria::LIKE); // WHERE tenant_uuid LIKE '%fooValue%'
     * $query->filterByTenantUuid(['foo', 'bar']); // WHERE tenant_uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $tenantUuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByTenantUuid($tenantUuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tenantUuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccinationPlanTableMap::COL_TENANT_UUID, $tenantUuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the vaccine_uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByVaccineUuid('fooValue');   // WHERE vaccine_uuid = 'fooValue'
     * $query->filterByVaccineUuid('%fooValue%', Criteria::LIKE); // WHERE vaccine_uuid LIKE '%fooValue%'
     * $query->filterByVaccineUuid(['foo', 'bar']); // WHERE vaccine_uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $vaccineUuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVaccineUuid($vaccineUuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($vaccineUuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccinationPlanTableMap::COL_VACCINE_UUID, $vaccineUuid, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \Tenant object
     *
     * @param \Tenant|ObjectCollection $tenant The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByTenant($tenant, ?string $comparison = null)
    {
        if ($tenant instanceof \Tenant) {
            return $this
                ->addUsingAlias(VaccinationPlanTableMap::COL_TENANT_UUID, $tenant->getUuid(), $comparison);
        } elseif ($tenant instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(VaccinationPlanTableMap::COL_TENANT_UUID, $tenant->toKeyValue('PrimaryKey', 'Uuid'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByTenant() only accepts arguments of type \Tenant or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tenant relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinTenant(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tenant');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tenant');
        }

        return $this;
    }

    /**
     * Use the Tenant relation Tenant object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TenantQuery A secondary query class using the current class as primary query
     */
    public function useTenantQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTenant($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tenant', '\TenantQuery');
    }

    /**
     * Use the Tenant relation Tenant object
     *
     * @param callable(\TenantQuery):\TenantQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withTenantQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useTenantQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Tenant table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \TenantQuery The inner query object of the EXISTS statement
     */
    public function useTenantExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Tenant', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Tenant table for a NOT EXISTS query.
     *
     * @see useTenantExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \TenantQuery The inner query object of the NOT EXISTS statement
     */
    public function useTenantNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Tenant', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \Vaccine object
     *
     * @param \Vaccine|ObjectCollection $vaccine The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVaccine($vaccine, ?string $comparison = null)
    {
        if ($vaccine instanceof \Vaccine) {
            return $this
                ->addUsingAlias(VaccinationPlanTableMap::COL_VACCINE_UUID, $vaccine->getUuid(), $comparison);
        } elseif ($vaccine instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(VaccinationPlanTableMap::COL_VACCINE_UUID, $vaccine->toKeyValue('PrimaryKey', 'Uuid'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByVaccine() only accepts arguments of type \Vaccine or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Vaccine relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinVaccine(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Vaccine');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Vaccine');
        }

        return $this;
    }

    /**
     * Use the Vaccine relation Vaccine object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VaccineQuery A secondary query class using the current class as primary query
     */
    public function useVaccineQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVaccine($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Vaccine', '\VaccineQuery');
    }

    /**
     * Use the Vaccine relation Vaccine object
     *
     * @param callable(\VaccineQuery):\VaccineQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withVaccineQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useVaccineQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Vaccine table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \VaccineQuery The inner query object of the EXISTS statement
     */
    public function useVaccineExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Vaccine', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Vaccine table for a NOT EXISTS query.
     *
     * @see useVaccineExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \VaccineQuery The inner query object of the NOT EXISTS statement
     */
    public function useVaccineNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Vaccine', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Exclude object from result
     *
     * @param ChildVaccinationPlan $vaccinationPlan Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($vaccinationPlan = null)
    {
        if ($vaccinationPlan) {
            $this->addUsingAlias(VaccinationPlanTableMap::COL_ID, $vaccinationPlan->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the vaccination_plan table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VaccinationPlanTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VaccinationPlanTableMap::clearInstancePool();
            VaccinationPlanTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VaccinationPlanTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(VaccinationPlanTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            VaccinationPlanTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            VaccinationPlanTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

}
