<?php

namespace Base;

use \Medication as ChildMedication;
use \MedicationAdministrationMethod as ChildMedicationAdministrationMethod;
use \MedicationAdministrationMethodQuery as ChildMedicationAdministrationMethodQuery;
use \MedicationDoseInstruction as ChildMedicationDoseInstruction;
use \MedicationDoseInstructionQuery as ChildMedicationDoseInstructionQuery;
use \MedicationQuery as ChildMedicationQuery;
use \Patient as ChildPatient;
use \PatientQuery as ChildPatientQuery;
use \Prescription as ChildPrescription;
use \PrescriptionDoseInstruction as ChildPrescriptionDoseInstruction;
use \PrescriptionDoseInstructionQuery as ChildPrescriptionDoseInstructionQuery;
use \PrescriptionQuery as ChildPrescriptionQuery;
use \User as ChildUser;
use \UserQuery as ChildUserQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\PrescriptionDoseInstructionTableMap;
use Map\PrescriptionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'prescription' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Prescription implements ActiveRecordInterface
{
    /**
     * TableMap class name
     *
     * @var string
     */
    public const TABLE_MAP = '\\Map\\PrescriptionTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var bool
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var bool
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = [];

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = [];

    /**
     * The value for the uuid field.
     *
     * @var        string
     */
    protected $uuid;

    /**
     * The value for the medication_uuid field.
     *
     * @var        string
     */
    protected $medication_uuid;

    /**
     * The value for the patient_uuid field.
     *
     * @var        string
     */
    protected $patient_uuid;

    /**
     * The value for the dose_quantity field.
     *
     * @var        string|null
     */
    protected $dose_quantity;

    /**
     * The value for the dose_morning field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $dose_morning;

    /**
     * The value for the dose_noon field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $dose_noon;

    /**
     * The value for the dose_evening field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $dose_evening;

    /**
     * The value for the dose_night field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $dose_night;

    /**
     * The value for the max_dose_period field.
     * Defines the max dosis per period
     * @var        string|null
     */
    protected $max_dose_period;

    /**
     * The value for the patient_instruction field.
     * Clear text instruction for patient
     * @var        string|null
     */
    protected $patient_instruction;

    /**
     * The value for the medication_administration_method_id field.
     *
     * @var        string|null
     */
    protected $medication_administration_method_id;

    /**
     * The value for the dose_start field.
     * Startdate of medication
     * @var        DateTime|null
     */
    protected $dose_start;

    /**
     * The value for the dose_end field.
     * Enddate of medication
     * @var        DateTime|null
     */
    protected $dose_end;

    /**
     * The value for the interval field.
     * Interval in full days
     * @var        int|null
     */
    protected $interval;

    /**
     * The value for the user_id field.
     *
     * @var        int
     */
    protected $user_id;

    /**
     * The value for the comment field.
     * Comment
     * @var        string|null
     */
    protected $comment;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        ChildMedicationAdministrationMethod
     */
    protected $aMedicationAdministrationMethod;

    /**
     * @var        ChildUser
     */
    protected $aUser;

    /**
     * @var        ChildPatient
     */
    protected $aPatient;

    /**
     * @var        ChildMedication
     */
    protected $aMedication;

    /**
     * @var        ObjectCollection|ChildPrescriptionDoseInstruction[] Collection to store aggregation of ChildPrescriptionDoseInstruction objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildPrescriptionDoseInstruction> Collection to store aggregation of ChildPrescriptionDoseInstruction objects.
     */
    protected $collPrescriptionDoseInstructions;
    protected $collPrescriptionDoseInstructionsPartial;

    /**
     * @var        ObjectCollection|ChildMedicationDoseInstruction[] Cross Collection to store aggregation of ChildMedicationDoseInstruction objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildMedicationDoseInstruction> Cross Collection to store aggregation of ChildMedicationDoseInstruction objects.
     */
    protected $collMedicationDoseInstructions;

    /**
     * @var bool
     */
    protected $collMedicationDoseInstructionsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var bool
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMedicationDoseInstruction[]
     * @phpstan-var ObjectCollection&\Traversable<ChildMedicationDoseInstruction>
     */
    protected $medicationDoseInstructionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrescriptionDoseInstruction[]
     * @phpstan-var ObjectCollection&\Traversable<ChildPrescriptionDoseInstruction>
     */
    protected $prescriptionDoseInstructionsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues(): void
    {
        $this->dose_morning = false;
        $this->dose_noon = false;
        $this->dose_evening = false;
        $this->dose_night = false;
    }

    /**
     * Initializes internal state of Base\Prescription object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return bool True if the object has been modified.
     */
    public function isModified(): bool
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param string $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return bool True if $col has been modified.
     */
    public function isColumnModified(string $col): bool
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns(): array
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return bool True, if the object has never been persisted.
     */
    public function isNew(): bool
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param bool $b the state of the object.
     */
    public function setNew(bool $b): void
    {
        $this->new = $b;
    }

    /**
     * Whether this object has been deleted.
     * @return bool The deleted state of this object.
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param bool $b The deleted state of this object.
     * @return void
     */
    public function setDeleted(bool $b): void
    {
        $this->deleted = $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified(?string $col = null): void
    {
        if (null !== $col) {
            unset($this->modifiedColumns[$col]);
        } else {
            $this->modifiedColumns = [];
        }
    }

    /**
     * Compares this with another <code>Prescription</code> instance.  If
     * <code>obj</code> is an instance of <code>Prescription</code>, delegates to
     * <code>equals(Prescription)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param mixed $obj The object to compare to.
     * @return bool Whether equal to the object specified.
     */
    public function equals($obj): bool
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns(): array
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return bool
     */
    public function hasVirtualColumn(string $name): bool
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return mixed
     *
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getVirtualColumn(string $name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of nonexistent virtual column `%s`.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @param mixed $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn(string $name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param string $msg
     * @param int $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log(string $msg, int $priority = Propel::LOG_INFO): void
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param \Propel\Runtime\Parser\AbstractParser|string $parser An AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string The exported data
     */
    public function exportTo($parser, bool $includeLazyLoadColumns = true, string $keyType = TableMap::TYPE_PHPNAME): string
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     *
     * @return array<string>
     */
    public function __sleep(): array
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [uuid] column value.
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Get the [medication_uuid] column value.
     *
     * @return string
     */
    public function getMedicationUuid()
    {
        return $this->medication_uuid;
    }

    /**
     * Get the [patient_uuid] column value.
     *
     * @return string
     */
    public function getPatientUuid()
    {
        return $this->patient_uuid;
    }

    /**
     * Get the [dose_quantity] column value.
     *
     * @return string|null
     */
    public function getDoseQuantity()
    {
        return $this->dose_quantity;
    }

    /**
     * Get the [dose_morning] column value.
     *
     * @return boolean|null
     */
    public function getDoseMorning()
    {
        return $this->dose_morning;
    }

    /**
     * Get the [dose_morning] column value.
     *
     * @return boolean|null
     */
    public function isDoseMorning()
    {
        return $this->getDoseMorning();
    }

    /**
     * Get the [dose_noon] column value.
     *
     * @return boolean|null
     */
    public function getDoseNoon()
    {
        return $this->dose_noon;
    }

    /**
     * Get the [dose_noon] column value.
     *
     * @return boolean|null
     */
    public function isDoseNoon()
    {
        return $this->getDoseNoon();
    }

    /**
     * Get the [dose_evening] column value.
     *
     * @return boolean|null
     */
    public function getDoseEvening()
    {
        return $this->dose_evening;
    }

    /**
     * Get the [dose_evening] column value.
     *
     * @return boolean|null
     */
    public function isDoseEvening()
    {
        return $this->getDoseEvening();
    }

    /**
     * Get the [dose_night] column value.
     *
     * @return boolean|null
     */
    public function getDoseNight()
    {
        return $this->dose_night;
    }

    /**
     * Get the [dose_night] column value.
     *
     * @return boolean|null
     */
    public function isDoseNight()
    {
        return $this->getDoseNight();
    }

    /**
     * Get the [max_dose_period] column value.
     * Defines the max dosis per period
     * @return string|null
     */
    public function getMaxDosePeriod()
    {
        return $this->max_dose_period;
    }

    /**
     * Get the [patient_instruction] column value.
     * Clear text instruction for patient
     * @return string|null
     */
    public function getPatientInstruction()
    {
        return $this->patient_instruction;
    }

    /**
     * Get the [medication_administration_method_id] column value.
     *
     * @return string|null
     */
    public function getMedicationAdministrationMethodId()
    {
        return $this->medication_administration_method_id;
    }

    /**
     * Get the [optionally formatted] temporal [dose_start] column value.
     * Startdate of medication
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getDoseStart($format = null)
    {
        if ($format === null) {
            return $this->dose_start;
        } else {
            return $this->dose_start instanceof \DateTimeInterface ? $this->dose_start->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [dose_end] column value.
     * Enddate of medication
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getDoseEnd($format = null)
    {
        if ($format === null) {
            return $this->dose_end;
        } else {
            return $this->dose_end instanceof \DateTimeInterface ? $this->dose_end->format($format) : null;
        }
    }

    /**
     * Get the [interval] column value.
     * Interval in full days
     * @return int|null
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * Get the [user_id] column value.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Get the [comment] column value.
     * Comment
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [uuid] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uuid !== $v) {
            $this->uuid = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_UUID] = true;
        }

        return $this;
    }

    /**
     * Set the value of [medication_uuid] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setMedicationUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->medication_uuid !== $v) {
            $this->medication_uuid = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_MEDICATION_UUID] = true;
        }

        if ($this->aMedication !== null && $this->aMedication->getUuid() !== $v) {
            $this->aMedication = null;
        }

        return $this;
    }

    /**
     * Set the value of [patient_uuid] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setPatientUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->patient_uuid !== $v) {
            $this->patient_uuid = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_PATIENT_UUID] = true;
        }

        if ($this->aPatient !== null && $this->aPatient->getUuid() !== $v) {
            $this->aPatient = null;
        }

        return $this;
    }

    /**
     * Set the value of [dose_quantity] column.
     *
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDoseQuantity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dose_quantity !== $v) {
            $this->dose_quantity = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_DOSE_QUANTITY] = true;
        }

        return $this;
    }

    /**
     * Sets the value of the [dose_morning] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param bool|integer|string|null $v The new value
     * @return $this The current object (for fluent API support)
     */
    public function setDoseMorning($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->dose_morning !== $v) {
            $this->dose_morning = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_DOSE_MORNING] = true;
        }

        return $this;
    }

    /**
     * Sets the value of the [dose_noon] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param bool|integer|string|null $v The new value
     * @return $this The current object (for fluent API support)
     */
    public function setDoseNoon($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->dose_noon !== $v) {
            $this->dose_noon = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_DOSE_NOON] = true;
        }

        return $this;
    }

    /**
     * Sets the value of the [dose_evening] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param bool|integer|string|null $v The new value
     * @return $this The current object (for fluent API support)
     */
    public function setDoseEvening($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->dose_evening !== $v) {
            $this->dose_evening = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_DOSE_EVENING] = true;
        }

        return $this;
    }

    /**
     * Sets the value of the [dose_night] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param bool|integer|string|null $v The new value
     * @return $this The current object (for fluent API support)
     */
    public function setDoseNight($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->dose_night !== $v) {
            $this->dose_night = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_DOSE_NIGHT] = true;
        }

        return $this;
    }

    /**
     * Set the value of [max_dose_period] column.
     * Defines the max dosis per period
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setMaxDosePeriod($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->max_dose_period !== $v) {
            $this->max_dose_period = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_MAX_DOSE_PERIOD] = true;
        }

        return $this;
    }

    /**
     * Set the value of [patient_instruction] column.
     * Clear text instruction for patient
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setPatientInstruction($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->patient_instruction !== $v) {
            $this->patient_instruction = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_PATIENT_INSTRUCTION] = true;
        }

        return $this;
    }

    /**
     * Set the value of [medication_administration_method_id] column.
     *
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setMedicationAdministrationMethodId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->medication_administration_method_id !== $v) {
            $this->medication_administration_method_id = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID] = true;
        }

        if ($this->aMedicationAdministrationMethod !== null && $this->aMedicationAdministrationMethod->getId() !== $v) {
            $this->aMedicationAdministrationMethod = null;
        }

        return $this;
    }

    /**
     * Sets the value of [dose_start] column to a normalized version of the date/time value specified.
     * Startdate of medication
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setDoseStart($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->dose_start !== null || $dt !== null) {
            if ($this->dose_start === null || $dt === null || $dt->format("Y-m-d") !== $this->dose_start->format("Y-m-d")) {
                $this->dose_start = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PrescriptionTableMap::COL_DOSE_START] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Sets the value of [dose_end] column to a normalized version of the date/time value specified.
     * Enddate of medication
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setDoseEnd($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->dose_end !== null || $dt !== null) {
            if ($this->dose_end === null || $dt === null || $dt->format("Y-m-d") !== $this->dose_end->format("Y-m-d")) {
                $this->dose_end = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PrescriptionTableMap::COL_DOSE_END] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Set the value of [interval] column.
     * Interval in full days
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setInterval($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->interval !== $v) {
            $this->interval = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_INTERVAL] = true;
        }

        return $this;
    }

    /**
     * Set the value of [user_id] column.
     *
     * @param int $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->user_id !== $v) {
            $this->user_id = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_USER_ID] = true;
        }

        if ($this->aUser !== null && $this->aUser->getId() !== $v) {
            $this->aUser = null;
        }

        return $this;
    }

    /**
     * Set the value of [comment] column.
     * Comment
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setComment($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->comment !== $v) {
            $this->comment = $v;
            $this->modifiedColumns[PrescriptionTableMap::COL_COMMENT] = true;
        }

        return $this;
    }

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PrescriptionTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PrescriptionTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return bool Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues(): bool
    {
            if ($this->dose_morning !== false) {
                return false;
            }

            if ($this->dose_noon !== false) {
                return false;
            }

            if ($this->dose_evening !== false) {
                return false;
            }

            if ($this->dose_night !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    }

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by DataFetcher->fetch().
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param bool $rehydrate Whether this object is being re-hydrated from the database.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int next starting column
     * @throws \Propel\Runtime\Exception\PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate(array $row, int $startcol = 0, bool $rehydrate = false, string $indexType = TableMap::TYPE_NUM): int
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PrescriptionTableMap::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PrescriptionTableMap::translateFieldName('MedicationUuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->medication_uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PrescriptionTableMap::translateFieldName('PatientUuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->patient_uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PrescriptionTableMap::translateFieldName('DoseQuantity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dose_quantity = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PrescriptionTableMap::translateFieldName('DoseMorning', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dose_morning = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PrescriptionTableMap::translateFieldName('DoseNoon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dose_noon = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PrescriptionTableMap::translateFieldName('DoseEvening', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dose_evening = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PrescriptionTableMap::translateFieldName('DoseNight', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dose_night = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : PrescriptionTableMap::translateFieldName('MaxDosePeriod', TableMap::TYPE_PHPNAME, $indexType)];
            $this->max_dose_period = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : PrescriptionTableMap::translateFieldName('PatientInstruction', TableMap::TYPE_PHPNAME, $indexType)];
            $this->patient_instruction = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : PrescriptionTableMap::translateFieldName('MedicationAdministrationMethodId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->medication_administration_method_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : PrescriptionTableMap::translateFieldName('DoseStart', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dose_start = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : PrescriptionTableMap::translateFieldName('DoseEnd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dose_end = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : PrescriptionTableMap::translateFieldName('Interval', TableMap::TYPE_PHPNAME, $indexType)];
            $this->interval = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : PrescriptionTableMap::translateFieldName('UserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : PrescriptionTableMap::translateFieldName('Comment', TableMap::TYPE_PHPNAME, $indexType)];
            $this->comment = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : PrescriptionTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : PrescriptionTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 18; // 18 = PrescriptionTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Prescription'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function ensureConsistency(): void
    {
        if ($this->aMedication !== null && $this->medication_uuid !== $this->aMedication->getUuid()) {
            $this->aMedication = null;
        }
        if ($this->aPatient !== null && $this->patient_uuid !== $this->aPatient->getUuid()) {
            $this->aPatient = null;
        }
        if ($this->aMedicationAdministrationMethod !== null && $this->medication_administration_method_id !== $this->aMedicationAdministrationMethod->getId()) {
            $this->aMedicationAdministrationMethod = null;
        }
        if ($this->aUser !== null && $this->user_id !== $this->aUser->getId()) {
            $this->aUser = null;
        }
    }

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param bool $deep (optional) Whether to also de-associated any related objects.
     * @param ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload(bool $deep = false, ?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PrescriptionTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPrescriptionQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aMedicationAdministrationMethod = null;
            $this->aUser = null;
            $this->aPatient = null;
            $this->aMedication = null;
            $this->collPrescriptionDoseInstructions = null;

            $this->collMedicationDoseInstructions = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param ConnectionInterface $con
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     * @see Prescription::setDeleted()
     * @see Prescription::isDeleted()
     */
    public function delete(?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrescriptionTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPrescriptionQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    public function save(?ConnectionInterface $con = null): int
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrescriptionTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(PrescriptionTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(PrescriptionTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(PrescriptionTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PrescriptionTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con): int
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aMedicationAdministrationMethod !== null) {
                if ($this->aMedicationAdministrationMethod->isModified() || $this->aMedicationAdministrationMethod->isNew()) {
                    $affectedRows += $this->aMedicationAdministrationMethod->save($con);
                }
                $this->setMedicationAdministrationMethod($this->aMedicationAdministrationMethod);
            }

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->aPatient !== null) {
                if ($this->aPatient->isModified() || $this->aPatient->isNew()) {
                    $affectedRows += $this->aPatient->save($con);
                }
                $this->setPatient($this->aPatient);
            }

            if ($this->aMedication !== null) {
                if ($this->aMedication->isModified() || $this->aMedication->isNew()) {
                    $affectedRows += $this->aMedication->save($con);
                }
                $this->setMedication($this->aMedication);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->medicationDoseInstructionsScheduledForDeletion !== null) {
                if (!$this->medicationDoseInstructionsScheduledForDeletion->isEmpty()) {
                    $pks = [];
                    foreach ($this->medicationDoseInstructionsScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[1] = $this->getUuid();
                        $entryPk[0] = $entry->getId();
                        $pks[] = $entryPk;
                    }

                    \PrescriptionDoseInstructionQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->medicationDoseInstructionsScheduledForDeletion = null;
                }

            }

            if ($this->collMedicationDoseInstructions) {
                foreach ($this->collMedicationDoseInstructions as $medicationDoseInstruction) {
                    if (!$medicationDoseInstruction->isDeleted() && ($medicationDoseInstruction->isNew() || $medicationDoseInstruction->isModified())) {
                        $medicationDoseInstruction->save($con);
                    }
                }
            }


            if ($this->prescriptionDoseInstructionsScheduledForDeletion !== null) {
                if (!$this->prescriptionDoseInstructionsScheduledForDeletion->isEmpty()) {
                    \PrescriptionDoseInstructionQuery::create()
                        ->filterByPrimaryKeys($this->prescriptionDoseInstructionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prescriptionDoseInstructionsScheduledForDeletion = null;
                }
            }

            if ($this->collPrescriptionDoseInstructions !== null) {
                foreach ($this->collPrescriptionDoseInstructions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    }

    /**
     * Insert the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con): void
    {
        $modifiedColumns = [];
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PrescriptionTableMap::COL_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'uuid';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_MEDICATION_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'medication_uuid';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_PATIENT_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'patient_uuid';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_QUANTITY)) {
            $modifiedColumns[':p' . $index++]  = 'dose_quantity';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_MORNING)) {
            $modifiedColumns[':p' . $index++]  = 'dose_morning';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_NOON)) {
            $modifiedColumns[':p' . $index++]  = 'dose_noon';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_EVENING)) {
            $modifiedColumns[':p' . $index++]  = 'dose_evening';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_NIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'dose_night';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_MAX_DOSE_PERIOD)) {
            $modifiedColumns[':p' . $index++]  = 'max_dose_period';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_PATIENT_INSTRUCTION)) {
            $modifiedColumns[':p' . $index++]  = 'patient_instruction';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'medication_administration_method_id';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_START)) {
            $modifiedColumns[':p' . $index++]  = 'dose_start';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_END)) {
            $modifiedColumns[':p' . $index++]  = 'dose_end';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_INTERVAL)) {
            $modifiedColumns[':p' . $index++]  = 'interval';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'user_id';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_COMMENT)) {
            $modifiedColumns[':p' . $index++]  = 'comment';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO prescription (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'uuid':
                        $stmt->bindValue($identifier, $this->uuid, PDO::PARAM_STR);
                        break;
                    case 'medication_uuid':
                        $stmt->bindValue($identifier, $this->medication_uuid, PDO::PARAM_STR);
                        break;
                    case 'patient_uuid':
                        $stmt->bindValue($identifier, $this->patient_uuid, PDO::PARAM_STR);
                        break;
                    case 'dose_quantity':
                        $stmt->bindValue($identifier, $this->dose_quantity, PDO::PARAM_STR);
                        break;
                    case 'dose_morning':
                        $stmt->bindValue($identifier, $this->dose_morning, PDO::PARAM_BOOL);
                        break;
                    case 'dose_noon':
                        $stmt->bindValue($identifier, $this->dose_noon, PDO::PARAM_BOOL);
                        break;
                    case 'dose_evening':
                        $stmt->bindValue($identifier, $this->dose_evening, PDO::PARAM_BOOL);
                        break;
                    case 'dose_night':
                        $stmt->bindValue($identifier, $this->dose_night, PDO::PARAM_BOOL);
                        break;
                    case 'max_dose_period':
                        $stmt->bindValue($identifier, $this->max_dose_period, PDO::PARAM_STR);
                        break;
                    case 'patient_instruction':
                        $stmt->bindValue($identifier, $this->patient_instruction, PDO::PARAM_STR);
                        break;
                    case 'medication_administration_method_id':
                        $stmt->bindValue($identifier, $this->medication_administration_method_id, PDO::PARAM_INT);
                        break;
                    case 'dose_start':
                        $stmt->bindValue($identifier, $this->dose_start ? $this->dose_start->format("Y-m-d") : null, PDO::PARAM_STR);
                        break;
                    case 'dose_end':
                        $stmt->bindValue($identifier, $this->dose_end ? $this->dose_end->format("Y-m-d") : null, PDO::PARAM_STR);
                        break;
                    case 'interval':
                        $stmt->bindValue($identifier, $this->interval, PDO::PARAM_INT);
                        break;
                    case 'user_id':
                        $stmt->bindValue($identifier, $this->user_id, PDO::PARAM_INT);
                        break;
                    case 'comment':
                        $stmt->bindValue($identifier, $this->comment, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @return int Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con): int
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName(string $name, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PrescriptionTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos Position in XML schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition(int $pos)
    {
        switch ($pos) {
            case 0:
                return $this->getUuid();

            case 1:
                return $this->getMedicationUuid();

            case 2:
                return $this->getPatientUuid();

            case 3:
                return $this->getDoseQuantity();

            case 4:
                return $this->getDoseMorning();

            case 5:
                return $this->getDoseNoon();

            case 6:
                return $this->getDoseEvening();

            case 7:
                return $this->getDoseNight();

            case 8:
                return $this->getMaxDosePeriod();

            case 9:
                return $this->getPatientInstruction();

            case 10:
                return $this->getMedicationAdministrationMethodId();

            case 11:
                return $this->getDoseStart();

            case 12:
                return $this->getDoseEnd();

            case 13:
                return $this->getInterval();

            case 14:
                return $this->getUserId();

            case 15:
                return $this->getComment();

            case 16:
                return $this->getCreatedAt();

            case 17:
                return $this->getUpdatedAt();

            default:
                return null;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param bool $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array An associative array containing the field names (as keys) and field values
     */
    public function toArray(string $keyType = TableMap::TYPE_PHPNAME, bool $includeLazyLoadColumns = true, array $alreadyDumpedObjects = [], bool $includeForeignObjects = false): array
    {
        if (isset($alreadyDumpedObjects['Prescription'][$this->hashCode()])) {
            return ['*RECURSION*'];
        }
        $alreadyDumpedObjects['Prescription'][$this->hashCode()] = true;
        $keys = PrescriptionTableMap::getFieldNames($keyType);
        $result = [
            $keys[0] => $this->getUuid(),
            $keys[1] => $this->getMedicationUuid(),
            $keys[2] => $this->getPatientUuid(),
            $keys[3] => $this->getDoseQuantity(),
            $keys[4] => $this->getDoseMorning(),
            $keys[5] => $this->getDoseNoon(),
            $keys[6] => $this->getDoseEvening(),
            $keys[7] => $this->getDoseNight(),
            $keys[8] => $this->getMaxDosePeriod(),
            $keys[9] => $this->getPatientInstruction(),
            $keys[10] => $this->getMedicationAdministrationMethodId(),
            $keys[11] => $this->getDoseStart(),
            $keys[12] => $this->getDoseEnd(),
            $keys[13] => $this->getInterval(),
            $keys[14] => $this->getUserId(),
            $keys[15] => $this->getComment(),
            $keys[16] => $this->getCreatedAt(),
            $keys[17] => $this->getUpdatedAt(),
        ];
        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('Y-m-d');
        }

        if ($result[$keys[12]] instanceof \DateTimeInterface) {
            $result[$keys[12]] = $result[$keys[12]]->format('Y-m-d');
        }

        if ($result[$keys[16]] instanceof \DateTimeInterface) {
            $result[$keys[16]] = $result[$keys[16]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[17]] instanceof \DateTimeInterface) {
            $result[$keys[17]] = $result[$keys[17]]->format('Y-m-d H:i:s.u');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aMedicationAdministrationMethod) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'medicationAdministrationMethod';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'medication_administration_method';
                        break;
                    default:
                        $key = 'MedicationAdministrationMethod';
                }

                $result[$key] = $this->aMedicationAdministrationMethod->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'users';
                        break;
                    default:
                        $key = 'User';
                }

                $result[$key] = $this->aUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPatient) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'patient';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'patient';
                        break;
                    default:
                        $key = 'Patient';
                }

                $result[$key] = $this->aPatient->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMedication) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'medication';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'medication';
                        break;
                    default:
                        $key = 'Medication';
                }

                $result[$key] = $this->aMedication->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collPrescriptionDoseInstructions) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prescriptionDoseInstructions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'prescription_dose_instructions';
                        break;
                    default:
                        $key = 'PrescriptionDoseInstructions';
                }

                $result[$key] = $this->collPrescriptionDoseInstructions->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this
     */
    public function setByName(string $name, $value, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PrescriptionTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        $this->setByPosition($pos, $value);

        return $this;
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return $this
     */
    public function setByPosition(int $pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setUuid($value);
                break;
            case 1:
                $this->setMedicationUuid($value);
                break;
            case 2:
                $this->setPatientUuid($value);
                break;
            case 3:
                $this->setDoseQuantity($value);
                break;
            case 4:
                $this->setDoseMorning($value);
                break;
            case 5:
                $this->setDoseNoon($value);
                break;
            case 6:
                $this->setDoseEvening($value);
                break;
            case 7:
                $this->setDoseNight($value);
                break;
            case 8:
                $this->setMaxDosePeriod($value);
                break;
            case 9:
                $this->setPatientInstruction($value);
                break;
            case 10:
                $this->setMedicationAdministrationMethodId($value);
                break;
            case 11:
                $this->setDoseStart($value);
                break;
            case 12:
                $this->setDoseEnd($value);
                break;
            case 13:
                $this->setInterval($value);
                break;
            case 14:
                $this->setUserId($value);
                break;
            case 15:
                $this->setComment($value);
                break;
            case 16:
                $this->setCreatedAt($value);
                break;
            case 17:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param array $arr An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return $this
     */
    public function fromArray(array $arr, string $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PrescriptionTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setUuid($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setMedicationUuid($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPatientUuid($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDoseQuantity($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDoseMorning($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDoseNoon($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDoseEvening($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDoseNight($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setMaxDosePeriod($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setPatientInstruction($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setMedicationAdministrationMethodId($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setDoseStart($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setDoseEnd($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setInterval($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setUserId($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setComment($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setCreatedAt($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setUpdatedAt($arr[$keys[17]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this The current object, for fluid interface
     */
    public function importFrom($parser, string $data, string $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria(): Criteria
    {
        $criteria = new Criteria(PrescriptionTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PrescriptionTableMap::COL_UUID)) {
            $criteria->add(PrescriptionTableMap::COL_UUID, $this->uuid);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_MEDICATION_UUID)) {
            $criteria->add(PrescriptionTableMap::COL_MEDICATION_UUID, $this->medication_uuid);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_PATIENT_UUID)) {
            $criteria->add(PrescriptionTableMap::COL_PATIENT_UUID, $this->patient_uuid);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_QUANTITY)) {
            $criteria->add(PrescriptionTableMap::COL_DOSE_QUANTITY, $this->dose_quantity);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_MORNING)) {
            $criteria->add(PrescriptionTableMap::COL_DOSE_MORNING, $this->dose_morning);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_NOON)) {
            $criteria->add(PrescriptionTableMap::COL_DOSE_NOON, $this->dose_noon);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_EVENING)) {
            $criteria->add(PrescriptionTableMap::COL_DOSE_EVENING, $this->dose_evening);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_NIGHT)) {
            $criteria->add(PrescriptionTableMap::COL_DOSE_NIGHT, $this->dose_night);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_MAX_DOSE_PERIOD)) {
            $criteria->add(PrescriptionTableMap::COL_MAX_DOSE_PERIOD, $this->max_dose_period);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_PATIENT_INSTRUCTION)) {
            $criteria->add(PrescriptionTableMap::COL_PATIENT_INSTRUCTION, $this->patient_instruction);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID)) {
            $criteria->add(PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID, $this->medication_administration_method_id);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_START)) {
            $criteria->add(PrescriptionTableMap::COL_DOSE_START, $this->dose_start);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_DOSE_END)) {
            $criteria->add(PrescriptionTableMap::COL_DOSE_END, $this->dose_end);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_INTERVAL)) {
            $criteria->add(PrescriptionTableMap::COL_INTERVAL, $this->interval);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_USER_ID)) {
            $criteria->add(PrescriptionTableMap::COL_USER_ID, $this->user_id);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_COMMENT)) {
            $criteria->add(PrescriptionTableMap::COL_COMMENT, $this->comment);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_CREATED_AT)) {
            $criteria->add(PrescriptionTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(PrescriptionTableMap::COL_UPDATED_AT)) {
            $criteria->add(PrescriptionTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria(): Criteria
    {
        $criteria = ChildPrescriptionQuery::create();
        $criteria->add(PrescriptionTableMap::COL_UUID, $this->uuid);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int|string Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getUuid();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getUuid();
    }

    /**
     * Generic method to set the primary key (uuid column).
     *
     * @param string|null $key Primary key.
     * @return void
     */
    public function setPrimaryKey(?string $key = null): void
    {
        $this->setUuid($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     *
     * @return bool
     */
    public function isPrimaryKeyNull(): bool
    {
        return null === $this->getUuid();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of \Prescription (or compatible) type.
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param bool $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function copyInto(object $copyObj, bool $deepCopy = false, bool $makeNew = true): void
    {
        $copyObj->setUuid($this->getUuid());
        $copyObj->setMedicationUuid($this->getMedicationUuid());
        $copyObj->setPatientUuid($this->getPatientUuid());
        $copyObj->setDoseQuantity($this->getDoseQuantity());
        $copyObj->setDoseMorning($this->getDoseMorning());
        $copyObj->setDoseNoon($this->getDoseNoon());
        $copyObj->setDoseEvening($this->getDoseEvening());
        $copyObj->setDoseNight($this->getDoseNight());
        $copyObj->setMaxDosePeriod($this->getMaxDosePeriod());
        $copyObj->setPatientInstruction($this->getPatientInstruction());
        $copyObj->setMedicationAdministrationMethodId($this->getMedicationAdministrationMethodId());
        $copyObj->setDoseStart($this->getDoseStart());
        $copyObj->setDoseEnd($this->getDoseEnd());
        $copyObj->setInterval($this->getInterval());
        $copyObj->setUserId($this->getUserId());
        $copyObj->setComment($this->getComment());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getPrescriptionDoseInstructions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrescriptionDoseInstruction($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Prescription Clone of current object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function copy(bool $deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildMedicationAdministrationMethod object.
     *
     * @param ChildMedicationAdministrationMethod|null $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setMedicationAdministrationMethod(ChildMedicationAdministrationMethod $v = null)
    {
        if ($v === null) {
            $this->setMedicationAdministrationMethodId(NULL);
        } else {
            $this->setMedicationAdministrationMethodId($v->getId());
        }

        $this->aMedicationAdministrationMethod = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMedicationAdministrationMethod object, it will not be re-added.
        if ($v !== null) {
            $v->addPrescription($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMedicationAdministrationMethod object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildMedicationAdministrationMethod|null The associated ChildMedicationAdministrationMethod object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getMedicationAdministrationMethod(?ConnectionInterface $con = null)
    {
        if ($this->aMedicationAdministrationMethod === null && (($this->medication_administration_method_id !== "" && $this->medication_administration_method_id !== null))) {
            $this->aMedicationAdministrationMethod = ChildMedicationAdministrationMethodQuery::create()->findPk($this->medication_administration_method_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMedicationAdministrationMethod->addPrescriptions($this);
             */
        }

        return $this->aMedicationAdministrationMethod;
    }

    /**
     * Declares an association between this object and a ChildUser object.
     *
     * @param ChildUser $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setUser(ChildUser $v = null)
    {
        if ($v === null) {
            $this->setUserId(NULL);
        } else {
            $this->setUserId($v->getId());
        }

        $this->aUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUser object, it will not be re-added.
        if ($v !== null) {
            $v->addPrescription($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUser object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildUser The associated ChildUser object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getUser(?ConnectionInterface $con = null)
    {
        if ($this->aUser === null && ($this->user_id != 0)) {
            $this->aUser = ChildUserQuery::create()->findPk($this->user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUser->addPrescriptions($this);
             */
        }

        return $this->aUser;
    }

    /**
     * Declares an association between this object and a ChildPatient object.
     *
     * @param ChildPatient $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setPatient(ChildPatient $v = null)
    {
        if ($v === null) {
            $this->setPatientUuid(NULL);
        } else {
            $this->setPatientUuid($v->getUuid());
        }

        $this->aPatient = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPatient object, it will not be re-added.
        if ($v !== null) {
            $v->addPrescription($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPatient object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildPatient The associated ChildPatient object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getPatient(?ConnectionInterface $con = null)
    {
        if ($this->aPatient === null && (($this->patient_uuid !== "" && $this->patient_uuid !== null))) {
            $this->aPatient = ChildPatientQuery::create()->findPk($this->patient_uuid, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPatient->addPrescriptions($this);
             */
        }

        return $this->aPatient;
    }

    /**
     * Declares an association between this object and a ChildMedication object.
     *
     * @param ChildMedication $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setMedication(ChildMedication $v = null)
    {
        if ($v === null) {
            $this->setMedicationUuid(NULL);
        } else {
            $this->setMedicationUuid($v->getUuid());
        }

        $this->aMedication = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMedication object, it will not be re-added.
        if ($v !== null) {
            $v->addPrescription($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMedication object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildMedication The associated ChildMedication object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getMedication(?ConnectionInterface $con = null)
    {
        if ($this->aMedication === null && (($this->medication_uuid !== "" && $this->medication_uuid !== null))) {
            $this->aMedication = ChildMedicationQuery::create()->findPk($this->medication_uuid, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMedication->addPrescriptions($this);
             */
        }

        return $this->aMedication;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName): void
    {
        if ('PrescriptionDoseInstruction' === $relationName) {
            $this->initPrescriptionDoseInstructions();
            return;
        }
    }

    /**
     * Clears out the collPrescriptionDoseInstructions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addPrescriptionDoseInstructions()
     */
    public function clearPrescriptionDoseInstructions()
    {
        $this->collPrescriptionDoseInstructions = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collPrescriptionDoseInstructions collection loaded partially.
     *
     * @return void
     */
    public function resetPartialPrescriptionDoseInstructions($v = true): void
    {
        $this->collPrescriptionDoseInstructionsPartial = $v;
    }

    /**
     * Initializes the collPrescriptionDoseInstructions collection.
     *
     * By default this just sets the collPrescriptionDoseInstructions collection to an empty array (like clearcollPrescriptionDoseInstructions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrescriptionDoseInstructions(bool $overrideExisting = true): void
    {
        if (null !== $this->collPrescriptionDoseInstructions && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrescriptionDoseInstructionTableMap::getTableMap()->getCollectionClassName();

        $this->collPrescriptionDoseInstructions = new $collectionClassName;
        $this->collPrescriptionDoseInstructions->setModel('\PrescriptionDoseInstruction');
    }

    /**
     * Gets an array of ChildPrescriptionDoseInstruction objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPrescription is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrescriptionDoseInstruction[] List of ChildPrescriptionDoseInstruction objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescriptionDoseInstruction> List of ChildPrescriptionDoseInstruction objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getPrescriptionDoseInstructions(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collPrescriptionDoseInstructionsPartial && !$this->isNew();
        if (null === $this->collPrescriptionDoseInstructions || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPrescriptionDoseInstructions) {
                    $this->initPrescriptionDoseInstructions();
                } else {
                    $collectionClassName = PrescriptionDoseInstructionTableMap::getTableMap()->getCollectionClassName();

                    $collPrescriptionDoseInstructions = new $collectionClassName;
                    $collPrescriptionDoseInstructions->setModel('\PrescriptionDoseInstruction');

                    return $collPrescriptionDoseInstructions;
                }
            } else {
                $collPrescriptionDoseInstructions = ChildPrescriptionDoseInstructionQuery::create(null, $criteria)
                    ->filterByPrescription($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrescriptionDoseInstructionsPartial && count($collPrescriptionDoseInstructions)) {
                        $this->initPrescriptionDoseInstructions(false);

                        foreach ($collPrescriptionDoseInstructions as $obj) {
                            if (false == $this->collPrescriptionDoseInstructions->contains($obj)) {
                                $this->collPrescriptionDoseInstructions->append($obj);
                            }
                        }

                        $this->collPrescriptionDoseInstructionsPartial = true;
                    }

                    return $collPrescriptionDoseInstructions;
                }

                if ($partial && $this->collPrescriptionDoseInstructions) {
                    foreach ($this->collPrescriptionDoseInstructions as $obj) {
                        if ($obj->isNew()) {
                            $collPrescriptionDoseInstructions[] = $obj;
                        }
                    }
                }

                $this->collPrescriptionDoseInstructions = $collPrescriptionDoseInstructions;
                $this->collPrescriptionDoseInstructionsPartial = false;
            }
        }

        return $this->collPrescriptionDoseInstructions;
    }

    /**
     * Sets a collection of ChildPrescriptionDoseInstruction objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $prescriptionDoseInstructions A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setPrescriptionDoseInstructions(Collection $prescriptionDoseInstructions, ?ConnectionInterface $con = null)
    {
        /** @var ChildPrescriptionDoseInstruction[] $prescriptionDoseInstructionsToDelete */
        $prescriptionDoseInstructionsToDelete = $this->getPrescriptionDoseInstructions(new Criteria(), $con)->diff($prescriptionDoseInstructions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->prescriptionDoseInstructionsScheduledForDeletion = clone $prescriptionDoseInstructionsToDelete;

        foreach ($prescriptionDoseInstructionsToDelete as $prescriptionDoseInstructionRemoved) {
            $prescriptionDoseInstructionRemoved->setPrescription(null);
        }

        $this->collPrescriptionDoseInstructions = null;
        foreach ($prescriptionDoseInstructions as $prescriptionDoseInstruction) {
            $this->addPrescriptionDoseInstruction($prescriptionDoseInstruction);
        }

        $this->collPrescriptionDoseInstructions = $prescriptionDoseInstructions;
        $this->collPrescriptionDoseInstructionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PrescriptionDoseInstruction objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related PrescriptionDoseInstruction objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countPrescriptionDoseInstructions(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collPrescriptionDoseInstructionsPartial && !$this->isNew();
        if (null === $this->collPrescriptionDoseInstructions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrescriptionDoseInstructions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrescriptionDoseInstructions());
            }

            $query = ChildPrescriptionDoseInstructionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrescription($this)
                ->count($con);
        }

        return count($this->collPrescriptionDoseInstructions);
    }

    /**
     * Method called to associate a ChildPrescriptionDoseInstruction object to this object
     * through the ChildPrescriptionDoseInstruction foreign key attribute.
     *
     * @param ChildPrescriptionDoseInstruction $l ChildPrescriptionDoseInstruction
     * @return $this The current object (for fluent API support)
     */
    public function addPrescriptionDoseInstruction(ChildPrescriptionDoseInstruction $l)
    {
        if ($this->collPrescriptionDoseInstructions === null) {
            $this->initPrescriptionDoseInstructions();
            $this->collPrescriptionDoseInstructionsPartial = true;
        }

        if (!$this->collPrescriptionDoseInstructions->contains($l)) {
            $this->doAddPrescriptionDoseInstruction($l);

            if ($this->prescriptionDoseInstructionsScheduledForDeletion and $this->prescriptionDoseInstructionsScheduledForDeletion->contains($l)) {
                $this->prescriptionDoseInstructionsScheduledForDeletion->remove($this->prescriptionDoseInstructionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrescriptionDoseInstruction $prescriptionDoseInstruction The ChildPrescriptionDoseInstruction object to add.
     */
    protected function doAddPrescriptionDoseInstruction(ChildPrescriptionDoseInstruction $prescriptionDoseInstruction): void
    {
        $this->collPrescriptionDoseInstructions[]= $prescriptionDoseInstruction;
        $prescriptionDoseInstruction->setPrescription($this);
    }

    /**
     * @param ChildPrescriptionDoseInstruction $prescriptionDoseInstruction The ChildPrescriptionDoseInstruction object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removePrescriptionDoseInstruction(ChildPrescriptionDoseInstruction $prescriptionDoseInstruction)
    {
        if ($this->getPrescriptionDoseInstructions()->contains($prescriptionDoseInstruction)) {
            $pos = $this->collPrescriptionDoseInstructions->search($prescriptionDoseInstruction);
            $this->collPrescriptionDoseInstructions->remove($pos);
            if (null === $this->prescriptionDoseInstructionsScheduledForDeletion) {
                $this->prescriptionDoseInstructionsScheduledForDeletion = clone $this->collPrescriptionDoseInstructions;
                $this->prescriptionDoseInstructionsScheduledForDeletion->clear();
            }
            $this->prescriptionDoseInstructionsScheduledForDeletion[]= clone $prescriptionDoseInstruction;
            $prescriptionDoseInstruction->setPrescription(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prescription is new, it will return
     * an empty collection; or if this Prescription has previously
     * been saved, it will retrieve related PrescriptionDoseInstructions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prescription.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrescriptionDoseInstruction[] List of ChildPrescriptionDoseInstruction objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescriptionDoseInstruction}> List of ChildPrescriptionDoseInstruction objects
     */
    public function getPrescriptionDoseInstructionsJoinMedicationDoseInstruction(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrescriptionDoseInstructionQuery::create(null, $criteria);
        $query->joinWith('MedicationDoseInstruction', $joinBehavior);

        return $this->getPrescriptionDoseInstructions($query, $con);
    }

    /**
     * Clears out the collMedicationDoseInstructions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMedicationDoseInstructions()
     */
    public function clearMedicationDoseInstructions()
    {
        $this->collMedicationDoseInstructions = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collMedicationDoseInstructions crossRef collection.
     *
     * By default this just sets the collMedicationDoseInstructions collection to an empty collection (like clearMedicationDoseInstructions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initMedicationDoseInstructions()
    {
        $collectionClassName = PrescriptionDoseInstructionTableMap::getTableMap()->getCollectionClassName();

        $this->collMedicationDoseInstructions = new $collectionClassName;
        $this->collMedicationDoseInstructionsPartial = true;
        $this->collMedicationDoseInstructions->setModel('\MedicationDoseInstruction');
    }

    /**
     * Checks if the collMedicationDoseInstructions collection is loaded.
     *
     * @return bool
     */
    public function isMedicationDoseInstructionsLoaded(): bool
    {
        return null !== $this->collMedicationDoseInstructions;
    }

    /**
     * Gets a collection of ChildMedicationDoseInstruction objects related by a many-to-many relationship
     * to the current object by way of the prescription_dose_instruction cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPrescription is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildMedicationDoseInstruction[] List of ChildMedicationDoseInstruction objects
     * @phpstan-return ObjectCollection&\Traversable<ChildMedicationDoseInstruction> List of ChildMedicationDoseInstruction objects
     */
    public function getMedicationDoseInstructions(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collMedicationDoseInstructionsPartial && !$this->isNew();
        if (null === $this->collMedicationDoseInstructions || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collMedicationDoseInstructions) {
                    $this->initMedicationDoseInstructions();
                }
            } else {

                $query = ChildMedicationDoseInstructionQuery::create(null, $criteria)
                    ->filterByPrescription($this);
                $collMedicationDoseInstructions = $query->find($con);
                if (null !== $criteria) {
                    return $collMedicationDoseInstructions;
                }

                if ($partial && $this->collMedicationDoseInstructions) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collMedicationDoseInstructions as $obj) {
                        if (!$collMedicationDoseInstructions->contains($obj)) {
                            $collMedicationDoseInstructions[] = $obj;
                        }
                    }
                }

                $this->collMedicationDoseInstructions = $collMedicationDoseInstructions;
                $this->collMedicationDoseInstructionsPartial = false;
            }
        }

        return $this->collMedicationDoseInstructions;
    }

    /**
     * Sets a collection of MedicationDoseInstruction objects related by a many-to-many relationship
     * to the current object by way of the prescription_dose_instruction cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $medicationDoseInstructions A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setMedicationDoseInstructions(Collection $medicationDoseInstructions, ?ConnectionInterface $con = null)
    {
        $this->clearMedicationDoseInstructions();
        $currentMedicationDoseInstructions = $this->getMedicationDoseInstructions();

        $medicationDoseInstructionsScheduledForDeletion = $currentMedicationDoseInstructions->diff($medicationDoseInstructions);

        foreach ($medicationDoseInstructionsScheduledForDeletion as $toDelete) {
            $this->removeMedicationDoseInstruction($toDelete);
        }

        foreach ($medicationDoseInstructions as $medicationDoseInstruction) {
            if (!$currentMedicationDoseInstructions->contains($medicationDoseInstruction)) {
                $this->doAddMedicationDoseInstruction($medicationDoseInstruction);
            }
        }

        $this->collMedicationDoseInstructionsPartial = false;
        $this->collMedicationDoseInstructions = $medicationDoseInstructions;

        return $this;
    }

    /**
     * Gets the number of MedicationDoseInstruction objects related by a many-to-many relationship
     * to the current object by way of the prescription_dose_instruction cross-reference table.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param bool $distinct Set to true to force count distinct
     * @param ConnectionInterface $con Optional connection object
     *
     * @return int The number of related MedicationDoseInstruction objects
     */
    public function countMedicationDoseInstructions(?Criteria $criteria = null, $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collMedicationDoseInstructionsPartial && !$this->isNew();
        if (null === $this->collMedicationDoseInstructions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMedicationDoseInstructions) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getMedicationDoseInstructions());
                }

                $query = ChildMedicationDoseInstructionQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByPrescription($this)
                    ->count($con);
            }
        } else {
            return count($this->collMedicationDoseInstructions);
        }
    }

    /**
     * Associate a ChildMedicationDoseInstruction to this object
     * through the prescription_dose_instruction cross reference table.
     *
     * @param ChildMedicationDoseInstruction $medicationDoseInstruction
     * @return ChildPrescription The current object (for fluent API support)
     */
    public function addMedicationDoseInstruction(ChildMedicationDoseInstruction $medicationDoseInstruction)
    {
        if ($this->collMedicationDoseInstructions === null) {
            $this->initMedicationDoseInstructions();
        }

        if (!$this->getMedicationDoseInstructions()->contains($medicationDoseInstruction)) {
            // only add it if the **same** object is not already associated
            $this->collMedicationDoseInstructions->push($medicationDoseInstruction);
            $this->doAddMedicationDoseInstruction($medicationDoseInstruction);
        }

        return $this;
    }

    /**
     *
     * @param ChildMedicationDoseInstruction $medicationDoseInstruction
     */
    protected function doAddMedicationDoseInstruction(ChildMedicationDoseInstruction $medicationDoseInstruction)
    {
        $prescriptionDoseInstruction = new ChildPrescriptionDoseInstruction();

        $prescriptionDoseInstruction->setMedicationDoseInstruction($medicationDoseInstruction);

        $prescriptionDoseInstruction->setPrescription($this);

        $this->addPrescriptionDoseInstruction($prescriptionDoseInstruction);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$medicationDoseInstruction->isPrescriptionsLoaded()) {
            $medicationDoseInstruction->initPrescriptions();
            $medicationDoseInstruction->getPrescriptions()->push($this);
        } elseif (!$medicationDoseInstruction->getPrescriptions()->contains($this)) {
            $medicationDoseInstruction->getPrescriptions()->push($this);
        }

    }

    /**
     * Remove medicationDoseInstruction of this object
     * through the prescription_dose_instruction cross reference table.
     *
     * @param ChildMedicationDoseInstruction $medicationDoseInstruction
     * @return ChildPrescription The current object (for fluent API support)
     */
    public function removeMedicationDoseInstruction(ChildMedicationDoseInstruction $medicationDoseInstruction)
    {
        if ($this->getMedicationDoseInstructions()->contains($medicationDoseInstruction)) {
            $prescriptionDoseInstruction = new ChildPrescriptionDoseInstruction();
            $prescriptionDoseInstruction->setMedicationDoseInstruction($medicationDoseInstruction);
            if ($medicationDoseInstruction->isPrescriptionsLoaded()) {
                //remove the back reference if available
                $medicationDoseInstruction->getPrescriptions()->removeObject($this);
            }

            $prescriptionDoseInstruction->setPrescription($this);
            $this->removePrescriptionDoseInstruction(clone $prescriptionDoseInstruction);
            $prescriptionDoseInstruction->clear();

            $this->collMedicationDoseInstructions->remove($this->collMedicationDoseInstructions->search($medicationDoseInstruction));

            if (null === $this->medicationDoseInstructionsScheduledForDeletion) {
                $this->medicationDoseInstructionsScheduledForDeletion = clone $this->collMedicationDoseInstructions;
                $this->medicationDoseInstructionsScheduledForDeletion->clear();
            }

            $this->medicationDoseInstructionsScheduledForDeletion->push($medicationDoseInstruction);
        }


        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     *
     * @return $this
     */
    public function clear()
    {
        if (null !== $this->aMedicationAdministrationMethod) {
            $this->aMedicationAdministrationMethod->removePrescription($this);
        }
        if (null !== $this->aUser) {
            $this->aUser->removePrescription($this);
        }
        if (null !== $this->aPatient) {
            $this->aPatient->removePrescription($this);
        }
        if (null !== $this->aMedication) {
            $this->aMedication->removePrescription($this);
        }
        $this->uuid = null;
        $this->medication_uuid = null;
        $this->patient_uuid = null;
        $this->dose_quantity = null;
        $this->dose_morning = null;
        $this->dose_noon = null;
        $this->dose_evening = null;
        $this->dose_night = null;
        $this->max_dose_period = null;
        $this->patient_instruction = null;
        $this->medication_administration_method_id = null;
        $this->dose_start = null;
        $this->dose_end = null;
        $this->interval = null;
        $this->user_id = null;
        $this->comment = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);

        return $this;
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param bool $deep Whether to also clear the references on all referrer objects.
     * @return $this
     */
    public function clearAllReferences(bool $deep = false)
    {
        if ($deep) {
            if ($this->collPrescriptionDoseInstructions) {
                foreach ($this->collPrescriptionDoseInstructions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMedicationDoseInstructions) {
                foreach ($this->collMedicationDoseInstructions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collPrescriptionDoseInstructions = null;
        $this->collMedicationDoseInstructions = null;
        $this->aMedicationAdministrationMethod = null;
        $this->aUser = null;
        $this->aPatient = null;
        $this->aMedication = null;
        return $this;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PrescriptionTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return $this The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[PrescriptionTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preSave(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postSave(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before inserting to database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preInsert(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postInsert(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preUpdate(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postUpdate(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preDelete(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postDelete(?ConnectionInterface $con = null): void
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
