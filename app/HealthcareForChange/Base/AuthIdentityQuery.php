<?php

namespace Base;

use \AuthIdentity as ChildAuthIdentity;
use \AuthIdentityQuery as ChildAuthIdentityQuery;
use \Exception;
use \PDO;
use Map\AuthIdentityTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'auth_identities' table.
 *
 *
 *
 * @method     ChildAuthIdentityQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildAuthIdentityQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildAuthIdentityQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildAuthIdentityQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildAuthIdentityQuery orderBySecret($order = Criteria::ASC) Order by the secret column
 * @method     ChildAuthIdentityQuery orderBySecret2($order = Criteria::ASC) Order by the secret2 column
 * @method     ChildAuthIdentityQuery orderByExpires($order = Criteria::ASC) Order by the expires column
 * @method     ChildAuthIdentityQuery orderByExtra($order = Criteria::ASC) Order by the extra column
 * @method     ChildAuthIdentityQuery orderByForceReset($order = Criteria::ASC) Order by the force_reset column
 * @method     ChildAuthIdentityQuery orderByLastUsedAt($order = Criteria::ASC) Order by the last_used_at column
 * @method     ChildAuthIdentityQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAuthIdentityQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildAuthIdentityQuery groupById() Group by the id column
 * @method     ChildAuthIdentityQuery groupByUserId() Group by the user_id column
 * @method     ChildAuthIdentityQuery groupByType() Group by the type column
 * @method     ChildAuthIdentityQuery groupByName() Group by the name column
 * @method     ChildAuthIdentityQuery groupBySecret() Group by the secret column
 * @method     ChildAuthIdentityQuery groupBySecret2() Group by the secret2 column
 * @method     ChildAuthIdentityQuery groupByExpires() Group by the expires column
 * @method     ChildAuthIdentityQuery groupByExtra() Group by the extra column
 * @method     ChildAuthIdentityQuery groupByForceReset() Group by the force_reset column
 * @method     ChildAuthIdentityQuery groupByLastUsedAt() Group by the last_used_at column
 * @method     ChildAuthIdentityQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAuthIdentityQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildAuthIdentityQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAuthIdentityQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAuthIdentityQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAuthIdentityQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAuthIdentityQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAuthIdentityQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAuthIdentityQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildAuthIdentityQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildAuthIdentityQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildAuthIdentityQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildAuthIdentityQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildAuthIdentityQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildAuthIdentityQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     \UserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildAuthIdentity|null findOne(?ConnectionInterface $con = null) Return the first ChildAuthIdentity matching the query
 * @method     ChildAuthIdentity findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildAuthIdentity matching the query, or a new ChildAuthIdentity object populated from the query conditions when no match is found
 *
 * @method     ChildAuthIdentity|null findOneById(int $id) Return the first ChildAuthIdentity filtered by the id column
 * @method     ChildAuthIdentity|null findOneByUserId(int $user_id) Return the first ChildAuthIdentity filtered by the user_id column
 * @method     ChildAuthIdentity|null findOneByType(string $type) Return the first ChildAuthIdentity filtered by the type column
 * @method     ChildAuthIdentity|null findOneByName(string $name) Return the first ChildAuthIdentity filtered by the name column
 * @method     ChildAuthIdentity|null findOneBySecret(string $secret) Return the first ChildAuthIdentity filtered by the secret column
 * @method     ChildAuthIdentity|null findOneBySecret2(string $secret2) Return the first ChildAuthIdentity filtered by the secret2 column
 * @method     ChildAuthIdentity|null findOneByExpires(string $expires) Return the first ChildAuthIdentity filtered by the expires column
 * @method     ChildAuthIdentity|null findOneByExtra(string $extra) Return the first ChildAuthIdentity filtered by the extra column
 * @method     ChildAuthIdentity|null findOneByForceReset(int $force_reset) Return the first ChildAuthIdentity filtered by the force_reset column
 * @method     ChildAuthIdentity|null findOneByLastUsedAt(string $last_used_at) Return the first ChildAuthIdentity filtered by the last_used_at column
 * @method     ChildAuthIdentity|null findOneByCreatedAt(string $created_at) Return the first ChildAuthIdentity filtered by the created_at column
 * @method     ChildAuthIdentity|null findOneByUpdatedAt(string $updated_at) Return the first ChildAuthIdentity filtered by the updated_at column *

 * @method     ChildAuthIdentity requirePk($key, ?ConnectionInterface $con = null) Return the ChildAuthIdentity by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthIdentity requireOne(?ConnectionInterface $con = null) Return the first ChildAuthIdentity matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAuthIdentity requireOneById(int $id) Return the first ChildAuthIdentity filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthIdentity requireOneByUserId(int $user_id) Return the first ChildAuthIdentity filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthIdentity requireOneByType(string $type) Return the first ChildAuthIdentity filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthIdentity requireOneByName(string $name) Return the first ChildAuthIdentity filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthIdentity requireOneBySecret(string $secret) Return the first ChildAuthIdentity filtered by the secret column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthIdentity requireOneBySecret2(string $secret2) Return the first ChildAuthIdentity filtered by the secret2 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthIdentity requireOneByExpires(string $expires) Return the first ChildAuthIdentity filtered by the expires column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthIdentity requireOneByExtra(string $extra) Return the first ChildAuthIdentity filtered by the extra column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthIdentity requireOneByForceReset(int $force_reset) Return the first ChildAuthIdentity filtered by the force_reset column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthIdentity requireOneByLastUsedAt(string $last_used_at) Return the first ChildAuthIdentity filtered by the last_used_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthIdentity requireOneByCreatedAt(string $created_at) Return the first ChildAuthIdentity filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthIdentity requireOneByUpdatedAt(string $updated_at) Return the first ChildAuthIdentity filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAuthIdentity[]|Collection find(?ConnectionInterface $con = null) Return ChildAuthIdentity objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> find(?ConnectionInterface $con = null) Return ChildAuthIdentity objects based on current ModelCriteria
 * @method     ChildAuthIdentity[]|Collection findById(int $id) Return ChildAuthIdentity objects filtered by the id column
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> findById(int $id) Return ChildAuthIdentity objects filtered by the id column
 * @method     ChildAuthIdentity[]|Collection findByUserId(int $user_id) Return ChildAuthIdentity objects filtered by the user_id column
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> findByUserId(int $user_id) Return ChildAuthIdentity objects filtered by the user_id column
 * @method     ChildAuthIdentity[]|Collection findByType(string $type) Return ChildAuthIdentity objects filtered by the type column
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> findByType(string $type) Return ChildAuthIdentity objects filtered by the type column
 * @method     ChildAuthIdentity[]|Collection findByName(string $name) Return ChildAuthIdentity objects filtered by the name column
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> findByName(string $name) Return ChildAuthIdentity objects filtered by the name column
 * @method     ChildAuthIdentity[]|Collection findBySecret(string $secret) Return ChildAuthIdentity objects filtered by the secret column
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> findBySecret(string $secret) Return ChildAuthIdentity objects filtered by the secret column
 * @method     ChildAuthIdentity[]|Collection findBySecret2(string $secret2) Return ChildAuthIdentity objects filtered by the secret2 column
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> findBySecret2(string $secret2) Return ChildAuthIdentity objects filtered by the secret2 column
 * @method     ChildAuthIdentity[]|Collection findByExpires(string $expires) Return ChildAuthIdentity objects filtered by the expires column
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> findByExpires(string $expires) Return ChildAuthIdentity objects filtered by the expires column
 * @method     ChildAuthIdentity[]|Collection findByExtra(string $extra) Return ChildAuthIdentity objects filtered by the extra column
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> findByExtra(string $extra) Return ChildAuthIdentity objects filtered by the extra column
 * @method     ChildAuthIdentity[]|Collection findByForceReset(int $force_reset) Return ChildAuthIdentity objects filtered by the force_reset column
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> findByForceReset(int $force_reset) Return ChildAuthIdentity objects filtered by the force_reset column
 * @method     ChildAuthIdentity[]|Collection findByLastUsedAt(string $last_used_at) Return ChildAuthIdentity objects filtered by the last_used_at column
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> findByLastUsedAt(string $last_used_at) Return ChildAuthIdentity objects filtered by the last_used_at column
 * @method     ChildAuthIdentity[]|Collection findByCreatedAt(string $created_at) Return ChildAuthIdentity objects filtered by the created_at column
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> findByCreatedAt(string $created_at) Return ChildAuthIdentity objects filtered by the created_at column
 * @method     ChildAuthIdentity[]|Collection findByUpdatedAt(string $updated_at) Return ChildAuthIdentity objects filtered by the updated_at column
 * @psalm-method Collection&\Traversable<ChildAuthIdentity> findByUpdatedAt(string $updated_at) Return ChildAuthIdentity objects filtered by the updated_at column
 * @method     ChildAuthIdentity[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildAuthIdentity> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AuthIdentityQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AuthIdentityQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\AuthIdentity', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAuthIdentityQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAuthIdentityQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildAuthIdentityQuery) {
            return $criteria;
        }
        $query = new ChildAuthIdentityQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAuthIdentity|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AuthIdentityTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AuthIdentityTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAuthIdentity A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, type, name, secret, secret2, expires, extra, force_reset, last_used_at, created_at, updated_at FROM auth_identities WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAuthIdentity $obj */
            $obj = new ChildAuthIdentity();
            $obj->hydrate($row);
            AuthIdentityTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildAuthIdentity|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(AuthIdentityTableMap::COL_ID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(AuthIdentityTableMap::COL_ID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterById($id = null, ?string $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthIdentityTableMap::COL_ID, $id, $comparison);

        return $this;
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUserId($userId = null, ?string $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthIdentityTableMap::COL_USER_ID, $userId, $comparison);

        return $this;
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * $query->filterByType(['foo', 'bar']); // WHERE type IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $type The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByType($type = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthIdentityTableMap::COL_TYPE, $type, $comparison);

        return $this;
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * $query->filterByName(['foo', 'bar']); // WHERE name IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $name The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByName($name = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthIdentityTableMap::COL_NAME, $name, $comparison);

        return $this;
    }

    /**
     * Filter the query on the secret column
     *
     * Example usage:
     * <code>
     * $query->filterBySecret('fooValue');   // WHERE secret = 'fooValue'
     * $query->filterBySecret('%fooValue%', Criteria::LIKE); // WHERE secret LIKE '%fooValue%'
     * $query->filterBySecret(['foo', 'bar']); // WHERE secret IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $secret The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterBySecret($secret = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($secret)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthIdentityTableMap::COL_SECRET, $secret, $comparison);

        return $this;
    }

    /**
     * Filter the query on the secret2 column
     *
     * Example usage:
     * <code>
     * $query->filterBySecret2('fooValue');   // WHERE secret2 = 'fooValue'
     * $query->filterBySecret2('%fooValue%', Criteria::LIKE); // WHERE secret2 LIKE '%fooValue%'
     * $query->filterBySecret2(['foo', 'bar']); // WHERE secret2 IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $secret2 The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterBySecret2($secret2 = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($secret2)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthIdentityTableMap::COL_SECRET2, $secret2, $comparison);

        return $this;
    }

    /**
     * Filter the query on the expires column
     *
     * Example usage:
     * <code>
     * $query->filterByExpires('2011-03-14'); // WHERE expires = '2011-03-14'
     * $query->filterByExpires('now'); // WHERE expires = '2011-03-14'
     * $query->filterByExpires(array('max' => 'yesterday')); // WHERE expires > '2011-03-13'
     * </code>
     *
     * @param mixed $expires The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByExpires($expires = null, ?string $comparison = null)
    {
        if (is_array($expires)) {
            $useMinMax = false;
            if (isset($expires['min'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_EXPIRES, $expires['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expires['max'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_EXPIRES, $expires['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthIdentityTableMap::COL_EXPIRES, $expires, $comparison);

        return $this;
    }

    /**
     * Filter the query on the extra column
     *
     * Example usage:
     * <code>
     * $query->filterByExtra('fooValue');   // WHERE extra = 'fooValue'
     * $query->filterByExtra('%fooValue%', Criteria::LIKE); // WHERE extra LIKE '%fooValue%'
     * $query->filterByExtra(['foo', 'bar']); // WHERE extra IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $extra The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByExtra($extra = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extra)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthIdentityTableMap::COL_EXTRA, $extra, $comparison);

        return $this;
    }

    /**
     * Filter the query on the force_reset column
     *
     * Example usage:
     * <code>
     * $query->filterByForceReset(1234); // WHERE force_reset = 1234
     * $query->filterByForceReset(array(12, 34)); // WHERE force_reset IN (12, 34)
     * $query->filterByForceReset(array('min' => 12)); // WHERE force_reset > 12
     * </code>
     *
     * @param mixed $forceReset The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByForceReset($forceReset = null, ?string $comparison = null)
    {
        if (is_array($forceReset)) {
            $useMinMax = false;
            if (isset($forceReset['min'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_FORCE_RESET, $forceReset['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($forceReset['max'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_FORCE_RESET, $forceReset['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthIdentityTableMap::COL_FORCE_RESET, $forceReset, $comparison);

        return $this;
    }

    /**
     * Filter the query on the last_used_at column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUsedAt('2011-03-14'); // WHERE last_used_at = '2011-03-14'
     * $query->filterByLastUsedAt('now'); // WHERE last_used_at = '2011-03-14'
     * $query->filterByLastUsedAt(array('max' => 'yesterday')); // WHERE last_used_at > '2011-03-13'
     * </code>
     *
     * @param mixed $lastUsedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByLastUsedAt($lastUsedAt = null, ?string $comparison = null)
    {
        if (is_array($lastUsedAt)) {
            $useMinMax = false;
            if (isset($lastUsedAt['min'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_LAST_USED_AT, $lastUsedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUsedAt['max'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_LAST_USED_AT, $lastUsedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthIdentityTableMap::COL_LAST_USED_AT, $lastUsedAt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, ?string $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthIdentityTableMap::COL_CREATED_AT, $createdAt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, ?string $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AuthIdentityTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthIdentityTableMap::COL_UPDATED_AT, $updatedAt, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \User object
     *
     * @param \User|ObjectCollection $user The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUser($user, ?string $comparison = null)
    {
        if ($user instanceof \User) {
            return $this
                ->addUsingAlias(AuthIdentityTableMap::COL_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(AuthIdentityTableMap::COL_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinUser(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\UserQuery');
    }

    /**
     * Use the User relation User object
     *
     * @param callable(\UserQuery):\UserQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUserQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUserQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to User table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \UserQuery The inner query object of the EXISTS statement
     */
    public function useUserExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('User', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to User table for a NOT EXISTS query.
     *
     * @see useUserExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \UserQuery The inner query object of the NOT EXISTS statement
     */
    public function useUserNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('User', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Exclude object from result
     *
     * @param ChildAuthIdentity $authIdentity Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($authIdentity = null)
    {
        if ($authIdentity) {
            $this->addUsingAlias(AuthIdentityTableMap::COL_ID, $authIdentity->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the auth_identities table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AuthIdentityTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AuthIdentityTableMap::clearInstancePool();
            AuthIdentityTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AuthIdentityTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AuthIdentityTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AuthIdentityTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AuthIdentityTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param int $nbDays Maximum age of the latest update in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        $this->addUsingAlias(AuthIdentityTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by update date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        $this->addDescendingOrderByColumn(AuthIdentityTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by update date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        $this->addAscendingOrderByColumn(AuthIdentityTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by create date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        $this->addDescendingOrderByColumn(AuthIdentityTableMap::COL_CREATED_AT);

        return $this;
    }

    /**
     * Filter by the latest created
     *
     * @param int $nbDays Maximum age of in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        $this->addUsingAlias(AuthIdentityTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by create date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        $this->addAscendingOrderByColumn(AuthIdentityTableMap::COL_CREATED_AT);

        return $this;
    }

}
