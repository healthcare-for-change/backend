<?php

namespace Base;

use \DewormingHistory as ChildDewormingHistory;
use \DewormingHistoryQuery as ChildDewormingHistoryQuery;
use \Medication as ChildMedication;
use \MedicationDoseForm as ChildMedicationDoseForm;
use \MedicationDoseFormQuery as ChildMedicationDoseFormQuery;
use \MedicationMarketingAuthorizationHolder as ChildMedicationMarketingAuthorizationHolder;
use \MedicationMarketingAuthorizationHolderQuery as ChildMedicationMarketingAuthorizationHolderQuery;
use \MedicationQuery as ChildMedicationQuery;
use \Prescription as ChildPrescription;
use \PrescriptionQuery as ChildPrescriptionQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\DewormingHistoryTableMap;
use Map\MedicationTableMap;
use Map\PrescriptionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'medication' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Medication implements ActiveRecordInterface
{
    /**
     * TableMap class name
     *
     * @var string
     */
    public const TABLE_MAP = '\\Map\\MedicationTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var bool
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var bool
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = [];

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = [];

    /**
     * The value for the uuid field.
     *
     * @var        string
     */
    protected $uuid;

    /**
     * The value for the name field.
     *
     * @var        string
     */
    protected $name;

    /**
     * The value for the code field.
     * Codes that identify this medication e.g. SNOMED CT Medication Codes
     * @var        string|null
     */
    protected $code;

    /**
     * The value for the active field.
     * Identifies if this record is currently active
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $active;

    /**
     * The value for the medication_dose_form_id field.
     *
     * @var        string
     */
    protected $medication_dose_form_id;

    /**
     * The value for the medication_marketing_authorization_holder_id field.
     *
     * @var        int|null
     */
    protected $medication_marketing_authorization_holder_id;

    /**
     * The value for the strength field.
     * Optional information to strength
     * @var        string|null
     */
    protected $strength;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        ChildMedicationDoseForm
     */
    protected $aMedicationDoseForm;

    /**
     * @var        ChildMedicationMarketingAuthorizationHolder
     */
    protected $aMedicationMarketingAuthorizationHolder;

    /**
     * @var        ObjectCollection|ChildDewormingHistory[] Collection to store aggregation of ChildDewormingHistory objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildDewormingHistory> Collection to store aggregation of ChildDewormingHistory objects.
     */
    protected $collDewormingHistories;
    protected $collDewormingHistoriesPartial;

    /**
     * @var        ObjectCollection|ChildPrescription[] Collection to store aggregation of ChildPrescription objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildPrescription> Collection to store aggregation of ChildPrescription objects.
     */
    protected $collPrescriptions;
    protected $collPrescriptionsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var bool
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDewormingHistory[]
     * @phpstan-var ObjectCollection&\Traversable<ChildDewormingHistory>
     */
    protected $dewormingHistoriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrescription[]
     * @phpstan-var ObjectCollection&\Traversable<ChildPrescription>
     */
    protected $prescriptionsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues(): void
    {
        $this->active = false;
    }

    /**
     * Initializes internal state of Base\Medication object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return bool True if the object has been modified.
     */
    public function isModified(): bool
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param string $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return bool True if $col has been modified.
     */
    public function isColumnModified(string $col): bool
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns(): array
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return bool True, if the object has never been persisted.
     */
    public function isNew(): bool
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param bool $b the state of the object.
     */
    public function setNew(bool $b): void
    {
        $this->new = $b;
    }

    /**
     * Whether this object has been deleted.
     * @return bool The deleted state of this object.
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param bool $b The deleted state of this object.
     * @return void
     */
    public function setDeleted(bool $b): void
    {
        $this->deleted = $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified(?string $col = null): void
    {
        if (null !== $col) {
            unset($this->modifiedColumns[$col]);
        } else {
            $this->modifiedColumns = [];
        }
    }

    /**
     * Compares this with another <code>Medication</code> instance.  If
     * <code>obj</code> is an instance of <code>Medication</code>, delegates to
     * <code>equals(Medication)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param mixed $obj The object to compare to.
     * @return bool Whether equal to the object specified.
     */
    public function equals($obj): bool
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns(): array
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return bool
     */
    public function hasVirtualColumn(string $name): bool
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return mixed
     *
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getVirtualColumn(string $name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of nonexistent virtual column `%s`.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @param mixed $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn(string $name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param string $msg
     * @param int $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log(string $msg, int $priority = Propel::LOG_INFO): void
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param \Propel\Runtime\Parser\AbstractParser|string $parser An AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string The exported data
     */
    public function exportTo($parser, bool $includeLazyLoadColumns = true, string $keyType = TableMap::TYPE_PHPNAME): string
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     *
     * @return array<string>
     */
    public function __sleep(): array
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [uuid] column value.
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [code] column value.
     * Codes that identify this medication e.g. SNOMED CT Medication Codes
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the [active] column value.
     * Identifies if this record is currently active
     * @return boolean|null
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get the [active] column value.
     * Identifies if this record is currently active
     * @return boolean|null
     */
    public function isActive()
    {
        return $this->getActive();
    }

    /**
     * Get the [medication_dose_form_id] column value.
     *
     * @return string
     */
    public function getMedicationDoseFormId()
    {
        return $this->medication_dose_form_id;
    }

    /**
     * Get the [medication_marketing_authorization_holder_id] column value.
     *
     * @return int|null
     */
    public function getMedicationMarketingAuthorizationHolderId()
    {
        return $this->medication_marketing_authorization_holder_id;
    }

    /**
     * Get the [strength] column value.
     * Optional information to strength
     * @return string|null
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [uuid] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uuid !== $v) {
            $this->uuid = $v;
            $this->modifiedColumns[MedicationTableMap::COL_UUID] = true;
        }

        return $this;
    }

    /**
     * Set the value of [name] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[MedicationTableMap::COL_NAME] = true;
        }

        return $this;
    }

    /**
     * Set the value of [code] column.
     * Codes that identify this medication e.g. SNOMED CT Medication Codes
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[MedicationTableMap::COL_CODE] = true;
        }

        return $this;
    }

    /**
     * Sets the value of the [active] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * Identifies if this record is currently active
     * @param bool|integer|string|null $v The new value
     * @return $this The current object (for fluent API support)
     */
    public function setActive($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->active !== $v) {
            $this->active = $v;
            $this->modifiedColumns[MedicationTableMap::COL_ACTIVE] = true;
        }

        return $this;
    }

    /**
     * Set the value of [medication_dose_form_id] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setMedicationDoseFormId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->medication_dose_form_id !== $v) {
            $this->medication_dose_form_id = $v;
            $this->modifiedColumns[MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID] = true;
        }

        if ($this->aMedicationDoseForm !== null && $this->aMedicationDoseForm->getId() !== $v) {
            $this->aMedicationDoseForm = null;
        }

        return $this;
    }

    /**
     * Set the value of [medication_marketing_authorization_holder_id] column.
     *
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setMedicationMarketingAuthorizationHolderId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->medication_marketing_authorization_holder_id !== $v) {
            $this->medication_marketing_authorization_holder_id = $v;
            $this->modifiedColumns[MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID] = true;
        }

        if ($this->aMedicationMarketingAuthorizationHolder !== null && $this->aMedicationMarketingAuthorizationHolder->getId() !== $v) {
            $this->aMedicationMarketingAuthorizationHolder = null;
        }

        return $this;
    }

    /**
     * Set the value of [strength] column.
     * Optional information to strength
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setStrength($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->strength !== $v) {
            $this->strength = $v;
            $this->modifiedColumns[MedicationTableMap::COL_STRENGTH] = true;
        }

        return $this;
    }

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MedicationTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MedicationTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return bool Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues(): bool
    {
            if ($this->active !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    }

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by DataFetcher->fetch().
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param bool $rehydrate Whether this object is being re-hydrated from the database.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int next starting column
     * @throws \Propel\Runtime\Exception\PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate(array $row, int $startcol = 0, bool $rehydrate = false, string $indexType = TableMap::TYPE_NUM): int
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : MedicationTableMap::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : MedicationTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : MedicationTableMap::translateFieldName('Code', TableMap::TYPE_PHPNAME, $indexType)];
            $this->code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : MedicationTableMap::translateFieldName('Active', TableMap::TYPE_PHPNAME, $indexType)];
            $this->active = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : MedicationTableMap::translateFieldName('MedicationDoseFormId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->medication_dose_form_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : MedicationTableMap::translateFieldName('MedicationMarketingAuthorizationHolderId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->medication_marketing_authorization_holder_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : MedicationTableMap::translateFieldName('Strength', TableMap::TYPE_PHPNAME, $indexType)];
            $this->strength = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : MedicationTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : MedicationTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = MedicationTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Medication'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function ensureConsistency(): void
    {
        if ($this->aMedicationDoseForm !== null && $this->medication_dose_form_id !== $this->aMedicationDoseForm->getId()) {
            $this->aMedicationDoseForm = null;
        }
        if ($this->aMedicationMarketingAuthorizationHolder !== null && $this->medication_marketing_authorization_holder_id !== $this->aMedicationMarketingAuthorizationHolder->getId()) {
            $this->aMedicationMarketingAuthorizationHolder = null;
        }
    }

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param bool $deep (optional) Whether to also de-associated any related objects.
     * @param ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload(bool $deep = false, ?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MedicationTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildMedicationQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aMedicationDoseForm = null;
            $this->aMedicationMarketingAuthorizationHolder = null;
            $this->collDewormingHistories = null;

            $this->collPrescriptions = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param ConnectionInterface $con
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     * @see Medication::setDeleted()
     * @see Medication::isDeleted()
     */
    public function delete(?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicationTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildMedicationQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    public function save(?ConnectionInterface $con = null): int
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicationTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(MedicationTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(MedicationTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(MedicationTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MedicationTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con): int
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aMedicationDoseForm !== null) {
                if ($this->aMedicationDoseForm->isModified() || $this->aMedicationDoseForm->isNew()) {
                    $affectedRows += $this->aMedicationDoseForm->save($con);
                }
                $this->setMedicationDoseForm($this->aMedicationDoseForm);
            }

            if ($this->aMedicationMarketingAuthorizationHolder !== null) {
                if ($this->aMedicationMarketingAuthorizationHolder->isModified() || $this->aMedicationMarketingAuthorizationHolder->isNew()) {
                    $affectedRows += $this->aMedicationMarketingAuthorizationHolder->save($con);
                }
                $this->setMedicationMarketingAuthorizationHolder($this->aMedicationMarketingAuthorizationHolder);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->dewormingHistoriesScheduledForDeletion !== null) {
                if (!$this->dewormingHistoriesScheduledForDeletion->isEmpty()) {
                    foreach ($this->dewormingHistoriesScheduledForDeletion as $dewormingHistory) {
                        // need to save related object because we set the relation to null
                        $dewormingHistory->save($con);
                    }
                    $this->dewormingHistoriesScheduledForDeletion = null;
                }
            }

            if ($this->collDewormingHistories !== null) {
                foreach ($this->collDewormingHistories as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prescriptionsScheduledForDeletion !== null) {
                if (!$this->prescriptionsScheduledForDeletion->isEmpty()) {
                    \PrescriptionQuery::create()
                        ->filterByPrimaryKeys($this->prescriptionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prescriptionsScheduledForDeletion = null;
                }
            }

            if ($this->collPrescriptions !== null) {
                foreach ($this->collPrescriptions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    }

    /**
     * Insert the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con): void
    {
        $modifiedColumns = [];
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(MedicationTableMap::COL_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'uuid';
        }
        if ($this->isColumnModified(MedicationTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(MedicationTableMap::COL_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'code';
        }
        if ($this->isColumnModified(MedicationTableMap::COL_ACTIVE)) {
            $modifiedColumns[':p' . $index++]  = 'active';
        }
        if ($this->isColumnModified(MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'medication_dose_form_id';
        }
        if ($this->isColumnModified(MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'medication_marketing_authorization_holder_id';
        }
        if ($this->isColumnModified(MedicationTableMap::COL_STRENGTH)) {
            $modifiedColumns[':p' . $index++]  = 'strength';
        }
        if ($this->isColumnModified(MedicationTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(MedicationTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO medication (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'uuid':
                        $stmt->bindValue($identifier, $this->uuid, PDO::PARAM_STR);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'code':
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case 'active':
                        $stmt->bindValue($identifier, $this->active, PDO::PARAM_BOOL);
                        break;
                    case 'medication_dose_form_id':
                        $stmt->bindValue($identifier, $this->medication_dose_form_id, PDO::PARAM_INT);
                        break;
                    case 'medication_marketing_authorization_holder_id':
                        $stmt->bindValue($identifier, $this->medication_marketing_authorization_holder_id, PDO::PARAM_INT);
                        break;
                    case 'strength':
                        $stmt->bindValue($identifier, $this->strength, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @return int Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con): int
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName(string $name, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MedicationTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos Position in XML schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition(int $pos)
    {
        switch ($pos) {
            case 0:
                return $this->getUuid();

            case 1:
                return $this->getName();

            case 2:
                return $this->getCode();

            case 3:
                return $this->getActive();

            case 4:
                return $this->getMedicationDoseFormId();

            case 5:
                return $this->getMedicationMarketingAuthorizationHolderId();

            case 6:
                return $this->getStrength();

            case 7:
                return $this->getCreatedAt();

            case 8:
                return $this->getUpdatedAt();

            default:
                return null;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param bool $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array An associative array containing the field names (as keys) and field values
     */
    public function toArray(string $keyType = TableMap::TYPE_PHPNAME, bool $includeLazyLoadColumns = true, array $alreadyDumpedObjects = [], bool $includeForeignObjects = false): array
    {
        if (isset($alreadyDumpedObjects['Medication'][$this->hashCode()])) {
            return ['*RECURSION*'];
        }
        $alreadyDumpedObjects['Medication'][$this->hashCode()] = true;
        $keys = MedicationTableMap::getFieldNames($keyType);
        $result = [
            $keys[0] => $this->getUuid(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getCode(),
            $keys[3] => $this->getActive(),
            $keys[4] => $this->getMedicationDoseFormId(),
            $keys[5] => $this->getMedicationMarketingAuthorizationHolderId(),
            $keys[6] => $this->getStrength(),
            $keys[7] => $this->getCreatedAt(),
            $keys[8] => $this->getUpdatedAt(),
        ];
        if ($result[$keys[7]] instanceof \DateTimeInterface) {
            $result[$keys[7]] = $result[$keys[7]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('Y-m-d H:i:s.u');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aMedicationDoseForm) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'medicationDoseForm';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'medication_dose_form';
                        break;
                    default:
                        $key = 'MedicationDoseForm';
                }

                $result[$key] = $this->aMedicationDoseForm->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMedicationMarketingAuthorizationHolder) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'medicationMarketingAuthorizationHolder';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'medication_marketing_authorization_holder';
                        break;
                    default:
                        $key = 'MedicationMarketingAuthorizationHolder';
                }

                $result[$key] = $this->aMedicationMarketingAuthorizationHolder->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collDewormingHistories) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dewormingHistories';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'deworming_histories';
                        break;
                    default:
                        $key = 'DewormingHistories';
                }

                $result[$key] = $this->collDewormingHistories->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrescriptions) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prescriptions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'prescriptions';
                        break;
                    default:
                        $key = 'Prescriptions';
                }

                $result[$key] = $this->collPrescriptions->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this
     */
    public function setByName(string $name, $value, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MedicationTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        $this->setByPosition($pos, $value);

        return $this;
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return $this
     */
    public function setByPosition(int $pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setUuid($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setCode($value);
                break;
            case 3:
                $this->setActive($value);
                break;
            case 4:
                $this->setMedicationDoseFormId($value);
                break;
            case 5:
                $this->setMedicationMarketingAuthorizationHolderId($value);
                break;
            case 6:
                $this->setStrength($value);
                break;
            case 7:
                $this->setCreatedAt($value);
                break;
            case 8:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param array $arr An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return $this
     */
    public function fromArray(array $arr, string $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = MedicationTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setUuid($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCode($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setActive($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setMedicationDoseFormId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setMedicationMarketingAuthorizationHolderId($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setStrength($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setCreatedAt($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setUpdatedAt($arr[$keys[8]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this The current object, for fluid interface
     */
    public function importFrom($parser, string $data, string $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria(): Criteria
    {
        $criteria = new Criteria(MedicationTableMap::DATABASE_NAME);

        if ($this->isColumnModified(MedicationTableMap::COL_UUID)) {
            $criteria->add(MedicationTableMap::COL_UUID, $this->uuid);
        }
        if ($this->isColumnModified(MedicationTableMap::COL_NAME)) {
            $criteria->add(MedicationTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(MedicationTableMap::COL_CODE)) {
            $criteria->add(MedicationTableMap::COL_CODE, $this->code);
        }
        if ($this->isColumnModified(MedicationTableMap::COL_ACTIVE)) {
            $criteria->add(MedicationTableMap::COL_ACTIVE, $this->active);
        }
        if ($this->isColumnModified(MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID)) {
            $criteria->add(MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID, $this->medication_dose_form_id);
        }
        if ($this->isColumnModified(MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID)) {
            $criteria->add(MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID, $this->medication_marketing_authorization_holder_id);
        }
        if ($this->isColumnModified(MedicationTableMap::COL_STRENGTH)) {
            $criteria->add(MedicationTableMap::COL_STRENGTH, $this->strength);
        }
        if ($this->isColumnModified(MedicationTableMap::COL_CREATED_AT)) {
            $criteria->add(MedicationTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(MedicationTableMap::COL_UPDATED_AT)) {
            $criteria->add(MedicationTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria(): Criteria
    {
        $criteria = ChildMedicationQuery::create();
        $criteria->add(MedicationTableMap::COL_UUID, $this->uuid);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int|string Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getUuid();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getUuid();
    }

    /**
     * Generic method to set the primary key (uuid column).
     *
     * @param string|null $key Primary key.
     * @return void
     */
    public function setPrimaryKey(?string $key = null): void
    {
        $this->setUuid($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     *
     * @return bool
     */
    public function isPrimaryKeyNull(): bool
    {
        return null === $this->getUuid();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of \Medication (or compatible) type.
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param bool $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function copyInto(object $copyObj, bool $deepCopy = false, bool $makeNew = true): void
    {
        $copyObj->setUuid($this->getUuid());
        $copyObj->setName($this->getName());
        $copyObj->setCode($this->getCode());
        $copyObj->setActive($this->getActive());
        $copyObj->setMedicationDoseFormId($this->getMedicationDoseFormId());
        $copyObj->setMedicationMarketingAuthorizationHolderId($this->getMedicationMarketingAuthorizationHolderId());
        $copyObj->setStrength($this->getStrength());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getDewormingHistories() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDewormingHistory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrescriptions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrescription($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Medication Clone of current object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function copy(bool $deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildMedicationDoseForm object.
     *
     * @param ChildMedicationDoseForm $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setMedicationDoseForm(ChildMedicationDoseForm $v = null)
    {
        if ($v === null) {
            $this->setMedicationDoseFormId(NULL);
        } else {
            $this->setMedicationDoseFormId($v->getId());
        }

        $this->aMedicationDoseForm = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMedicationDoseForm object, it will not be re-added.
        if ($v !== null) {
            $v->addMedication($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMedicationDoseForm object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildMedicationDoseForm The associated ChildMedicationDoseForm object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getMedicationDoseForm(?ConnectionInterface $con = null)
    {
        if ($this->aMedicationDoseForm === null && (($this->medication_dose_form_id !== "" && $this->medication_dose_form_id !== null))) {
            $this->aMedicationDoseForm = ChildMedicationDoseFormQuery::create()->findPk($this->medication_dose_form_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMedicationDoseForm->addMedications($this);
             */
        }

        return $this->aMedicationDoseForm;
    }

    /**
     * Declares an association between this object and a ChildMedicationMarketingAuthorizationHolder object.
     *
     * @param ChildMedicationMarketingAuthorizationHolder|null $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setMedicationMarketingAuthorizationHolder(ChildMedicationMarketingAuthorizationHolder $v = null)
    {
        if ($v === null) {
            $this->setMedicationMarketingAuthorizationHolderId(NULL);
        } else {
            $this->setMedicationMarketingAuthorizationHolderId($v->getId());
        }

        $this->aMedicationMarketingAuthorizationHolder = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMedicationMarketingAuthorizationHolder object, it will not be re-added.
        if ($v !== null) {
            $v->addMedication($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMedicationMarketingAuthorizationHolder object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildMedicationMarketingAuthorizationHolder|null The associated ChildMedicationMarketingAuthorizationHolder object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getMedicationMarketingAuthorizationHolder(?ConnectionInterface $con = null)
    {
        if ($this->aMedicationMarketingAuthorizationHolder === null && ($this->medication_marketing_authorization_holder_id != 0)) {
            $this->aMedicationMarketingAuthorizationHolder = ChildMedicationMarketingAuthorizationHolderQuery::create()->findPk($this->medication_marketing_authorization_holder_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMedicationMarketingAuthorizationHolder->addMedications($this);
             */
        }

        return $this->aMedicationMarketingAuthorizationHolder;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName): void
    {
        if ('DewormingHistory' === $relationName) {
            $this->initDewormingHistories();
            return;
        }
        if ('Prescription' === $relationName) {
            $this->initPrescriptions();
            return;
        }
    }

    /**
     * Clears out the collDewormingHistories collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addDewormingHistories()
     */
    public function clearDewormingHistories()
    {
        $this->collDewormingHistories = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collDewormingHistories collection loaded partially.
     *
     * @return void
     */
    public function resetPartialDewormingHistories($v = true): void
    {
        $this->collDewormingHistoriesPartial = $v;
    }

    /**
     * Initializes the collDewormingHistories collection.
     *
     * By default this just sets the collDewormingHistories collection to an empty array (like clearcollDewormingHistories());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDewormingHistories(bool $overrideExisting = true): void
    {
        if (null !== $this->collDewormingHistories && !$overrideExisting) {
            return;
        }

        $collectionClassName = DewormingHistoryTableMap::getTableMap()->getCollectionClassName();

        $this->collDewormingHistories = new $collectionClassName;
        $this->collDewormingHistories->setModel('\DewormingHistory');
    }

    /**
     * Gets an array of ChildDewormingHistory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMedication is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDewormingHistory[] List of ChildDewormingHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildDewormingHistory> List of ChildDewormingHistory objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getDewormingHistories(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collDewormingHistoriesPartial && !$this->isNew();
        if (null === $this->collDewormingHistories || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collDewormingHistories) {
                    $this->initDewormingHistories();
                } else {
                    $collectionClassName = DewormingHistoryTableMap::getTableMap()->getCollectionClassName();

                    $collDewormingHistories = new $collectionClassName;
                    $collDewormingHistories->setModel('\DewormingHistory');

                    return $collDewormingHistories;
                }
            } else {
                $collDewormingHistories = ChildDewormingHistoryQuery::create(null, $criteria)
                    ->filterByMedication($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDewormingHistoriesPartial && count($collDewormingHistories)) {
                        $this->initDewormingHistories(false);

                        foreach ($collDewormingHistories as $obj) {
                            if (false == $this->collDewormingHistories->contains($obj)) {
                                $this->collDewormingHistories->append($obj);
                            }
                        }

                        $this->collDewormingHistoriesPartial = true;
                    }

                    return $collDewormingHistories;
                }

                if ($partial && $this->collDewormingHistories) {
                    foreach ($this->collDewormingHistories as $obj) {
                        if ($obj->isNew()) {
                            $collDewormingHistories[] = $obj;
                        }
                    }
                }

                $this->collDewormingHistories = $collDewormingHistories;
                $this->collDewormingHistoriesPartial = false;
            }
        }

        return $this->collDewormingHistories;
    }

    /**
     * Sets a collection of ChildDewormingHistory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $dewormingHistories A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setDewormingHistories(Collection $dewormingHistories, ?ConnectionInterface $con = null)
    {
        /** @var ChildDewormingHistory[] $dewormingHistoriesToDelete */
        $dewormingHistoriesToDelete = $this->getDewormingHistories(new Criteria(), $con)->diff($dewormingHistories);


        $this->dewormingHistoriesScheduledForDeletion = $dewormingHistoriesToDelete;

        foreach ($dewormingHistoriesToDelete as $dewormingHistoryRemoved) {
            $dewormingHistoryRemoved->setMedication(null);
        }

        $this->collDewormingHistories = null;
        foreach ($dewormingHistories as $dewormingHistory) {
            $this->addDewormingHistory($dewormingHistory);
        }

        $this->collDewormingHistories = $dewormingHistories;
        $this->collDewormingHistoriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DewormingHistory objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related DewormingHistory objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countDewormingHistories(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collDewormingHistoriesPartial && !$this->isNew();
        if (null === $this->collDewormingHistories || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDewormingHistories) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDewormingHistories());
            }

            $query = ChildDewormingHistoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMedication($this)
                ->count($con);
        }

        return count($this->collDewormingHistories);
    }

    /**
     * Method called to associate a ChildDewormingHistory object to this object
     * through the ChildDewormingHistory foreign key attribute.
     *
     * @param ChildDewormingHistory $l ChildDewormingHistory
     * @return $this The current object (for fluent API support)
     */
    public function addDewormingHistory(ChildDewormingHistory $l)
    {
        if ($this->collDewormingHistories === null) {
            $this->initDewormingHistories();
            $this->collDewormingHistoriesPartial = true;
        }

        if (!$this->collDewormingHistories->contains($l)) {
            $this->doAddDewormingHistory($l);

            if ($this->dewormingHistoriesScheduledForDeletion and $this->dewormingHistoriesScheduledForDeletion->contains($l)) {
                $this->dewormingHistoriesScheduledForDeletion->remove($this->dewormingHistoriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDewormingHistory $dewormingHistory The ChildDewormingHistory object to add.
     */
    protected function doAddDewormingHistory(ChildDewormingHistory $dewormingHistory): void
    {
        $this->collDewormingHistories[]= $dewormingHistory;
        $dewormingHistory->setMedication($this);
    }

    /**
     * @param ChildDewormingHistory $dewormingHistory The ChildDewormingHistory object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeDewormingHistory(ChildDewormingHistory $dewormingHistory)
    {
        if ($this->getDewormingHistories()->contains($dewormingHistory)) {
            $pos = $this->collDewormingHistories->search($dewormingHistory);
            $this->collDewormingHistories->remove($pos);
            if (null === $this->dewormingHistoriesScheduledForDeletion) {
                $this->dewormingHistoriesScheduledForDeletion = clone $this->collDewormingHistories;
                $this->dewormingHistoriesScheduledForDeletion->clear();
            }
            $this->dewormingHistoriesScheduledForDeletion[]= $dewormingHistory;
            $dewormingHistory->setMedication(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Medication is new, it will return
     * an empty collection; or if this Medication has previously
     * been saved, it will retrieve related DewormingHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Medication.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDewormingHistory[] List of ChildDewormingHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildDewormingHistory}> List of ChildDewormingHistory objects
     */
    public function getDewormingHistoriesJoinPatient(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDewormingHistoryQuery::create(null, $criteria);
        $query->joinWith('Patient', $joinBehavior);

        return $this->getDewormingHistories($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Medication is new, it will return
     * an empty collection; or if this Medication has previously
     * been saved, it will retrieve related DewormingHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Medication.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDewormingHistory[] List of ChildDewormingHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildDewormingHistory}> List of ChildDewormingHistory objects
     */
    public function getDewormingHistoriesJoinUser(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDewormingHistoryQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getDewormingHistories($query, $con);
    }

    /**
     * Clears out the collPrescriptions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addPrescriptions()
     */
    public function clearPrescriptions()
    {
        $this->collPrescriptions = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collPrescriptions collection loaded partially.
     *
     * @return void
     */
    public function resetPartialPrescriptions($v = true): void
    {
        $this->collPrescriptionsPartial = $v;
    }

    /**
     * Initializes the collPrescriptions collection.
     *
     * By default this just sets the collPrescriptions collection to an empty array (like clearcollPrescriptions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrescriptions(bool $overrideExisting = true): void
    {
        if (null !== $this->collPrescriptions && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrescriptionTableMap::getTableMap()->getCollectionClassName();

        $this->collPrescriptions = new $collectionClassName;
        $this->collPrescriptions->setModel('\Prescription');
    }

    /**
     * Gets an array of ChildPrescription objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMedication is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription> List of ChildPrescription objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getPrescriptions(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collPrescriptionsPartial && !$this->isNew();
        if (null === $this->collPrescriptions || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPrescriptions) {
                    $this->initPrescriptions();
                } else {
                    $collectionClassName = PrescriptionTableMap::getTableMap()->getCollectionClassName();

                    $collPrescriptions = new $collectionClassName;
                    $collPrescriptions->setModel('\Prescription');

                    return $collPrescriptions;
                }
            } else {
                $collPrescriptions = ChildPrescriptionQuery::create(null, $criteria)
                    ->filterByMedication($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrescriptionsPartial && count($collPrescriptions)) {
                        $this->initPrescriptions(false);

                        foreach ($collPrescriptions as $obj) {
                            if (false == $this->collPrescriptions->contains($obj)) {
                                $this->collPrescriptions->append($obj);
                            }
                        }

                        $this->collPrescriptionsPartial = true;
                    }

                    return $collPrescriptions;
                }

                if ($partial && $this->collPrescriptions) {
                    foreach ($this->collPrescriptions as $obj) {
                        if ($obj->isNew()) {
                            $collPrescriptions[] = $obj;
                        }
                    }
                }

                $this->collPrescriptions = $collPrescriptions;
                $this->collPrescriptionsPartial = false;
            }
        }

        return $this->collPrescriptions;
    }

    /**
     * Sets a collection of ChildPrescription objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $prescriptions A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setPrescriptions(Collection $prescriptions, ?ConnectionInterface $con = null)
    {
        /** @var ChildPrescription[] $prescriptionsToDelete */
        $prescriptionsToDelete = $this->getPrescriptions(new Criteria(), $con)->diff($prescriptions);


        $this->prescriptionsScheduledForDeletion = $prescriptionsToDelete;

        foreach ($prescriptionsToDelete as $prescriptionRemoved) {
            $prescriptionRemoved->setMedication(null);
        }

        $this->collPrescriptions = null;
        foreach ($prescriptions as $prescription) {
            $this->addPrescription($prescription);
        }

        $this->collPrescriptions = $prescriptions;
        $this->collPrescriptionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prescription objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related Prescription objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countPrescriptions(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collPrescriptionsPartial && !$this->isNew();
        if (null === $this->collPrescriptions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrescriptions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrescriptions());
            }

            $query = ChildPrescriptionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMedication($this)
                ->count($con);
        }

        return count($this->collPrescriptions);
    }

    /**
     * Method called to associate a ChildPrescription object to this object
     * through the ChildPrescription foreign key attribute.
     *
     * @param ChildPrescription $l ChildPrescription
     * @return $this The current object (for fluent API support)
     */
    public function addPrescription(ChildPrescription $l)
    {
        if ($this->collPrescriptions === null) {
            $this->initPrescriptions();
            $this->collPrescriptionsPartial = true;
        }

        if (!$this->collPrescriptions->contains($l)) {
            $this->doAddPrescription($l);

            if ($this->prescriptionsScheduledForDeletion and $this->prescriptionsScheduledForDeletion->contains($l)) {
                $this->prescriptionsScheduledForDeletion->remove($this->prescriptionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrescription $prescription The ChildPrescription object to add.
     */
    protected function doAddPrescription(ChildPrescription $prescription): void
    {
        $this->collPrescriptions[]= $prescription;
        $prescription->setMedication($this);
    }

    /**
     * @param ChildPrescription $prescription The ChildPrescription object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removePrescription(ChildPrescription $prescription)
    {
        if ($this->getPrescriptions()->contains($prescription)) {
            $pos = $this->collPrescriptions->search($prescription);
            $this->collPrescriptions->remove($pos);
            if (null === $this->prescriptionsScheduledForDeletion) {
                $this->prescriptionsScheduledForDeletion = clone $this->collPrescriptions;
                $this->prescriptionsScheduledForDeletion->clear();
            }
            $this->prescriptionsScheduledForDeletion[]= clone $prescription;
            $prescription->setMedication(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Medication is new, it will return
     * an empty collection; or if this Medication has previously
     * been saved, it will retrieve related Prescriptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Medication.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription}> List of ChildPrescription objects
     */
    public function getPrescriptionsJoinMedicationAdministrationMethod(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrescriptionQuery::create(null, $criteria);
        $query->joinWith('MedicationAdministrationMethod', $joinBehavior);

        return $this->getPrescriptions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Medication is new, it will return
     * an empty collection; or if this Medication has previously
     * been saved, it will retrieve related Prescriptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Medication.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription}> List of ChildPrescription objects
     */
    public function getPrescriptionsJoinUser(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrescriptionQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getPrescriptions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Medication is new, it will return
     * an empty collection; or if this Medication has previously
     * been saved, it will retrieve related Prescriptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Medication.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription}> List of ChildPrescription objects
     */
    public function getPrescriptionsJoinPatient(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrescriptionQuery::create(null, $criteria);
        $query->joinWith('Patient', $joinBehavior);

        return $this->getPrescriptions($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     *
     * @return $this
     */
    public function clear()
    {
        if (null !== $this->aMedicationDoseForm) {
            $this->aMedicationDoseForm->removeMedication($this);
        }
        if (null !== $this->aMedicationMarketingAuthorizationHolder) {
            $this->aMedicationMarketingAuthorizationHolder->removeMedication($this);
        }
        $this->uuid = null;
        $this->name = null;
        $this->code = null;
        $this->active = null;
        $this->medication_dose_form_id = null;
        $this->medication_marketing_authorization_holder_id = null;
        $this->strength = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);

        return $this;
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param bool $deep Whether to also clear the references on all referrer objects.
     * @return $this
     */
    public function clearAllReferences(bool $deep = false)
    {
        if ($deep) {
            if ($this->collDewormingHistories) {
                foreach ($this->collDewormingHistories as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrescriptions) {
                foreach ($this->collPrescriptions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collDewormingHistories = null;
        $this->collPrescriptions = null;
        $this->aMedicationDoseForm = null;
        $this->aMedicationMarketingAuthorizationHolder = null;
        return $this;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MedicationTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return $this The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[MedicationTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preSave(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postSave(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before inserting to database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preInsert(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postInsert(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preUpdate(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postUpdate(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preDelete(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postDelete(?ConnectionInterface $con = null): void
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
