<?php

namespace Base;

use \Prescription as ChildPrescription;
use \PrescriptionQuery as ChildPrescriptionQuery;
use \Exception;
use \PDO;
use Map\PrescriptionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'prescription' table.
 *
 *
 *
 * @method     ChildPrescriptionQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildPrescriptionQuery orderByMedicationUuid($order = Criteria::ASC) Order by the medication_uuid column
 * @method     ChildPrescriptionQuery orderByPatientUuid($order = Criteria::ASC) Order by the patient_uuid column
 * @method     ChildPrescriptionQuery orderByDoseQuantity($order = Criteria::ASC) Order by the dose_quantity column
 * @method     ChildPrescriptionQuery orderByDoseMorning($order = Criteria::ASC) Order by the dose_morning column
 * @method     ChildPrescriptionQuery orderByDoseNoon($order = Criteria::ASC) Order by the dose_noon column
 * @method     ChildPrescriptionQuery orderByDoseEvening($order = Criteria::ASC) Order by the dose_evening column
 * @method     ChildPrescriptionQuery orderByDoseNight($order = Criteria::ASC) Order by the dose_night column
 * @method     ChildPrescriptionQuery orderByMaxDosePeriod($order = Criteria::ASC) Order by the max_dose_period column
 * @method     ChildPrescriptionQuery orderByPatientInstruction($order = Criteria::ASC) Order by the patient_instruction column
 * @method     ChildPrescriptionQuery orderByMedicationAdministrationMethodId($order = Criteria::ASC) Order by the medication_administration_method_id column
 * @method     ChildPrescriptionQuery orderByDoseStart($order = Criteria::ASC) Order by the dose_start column
 * @method     ChildPrescriptionQuery orderByDoseEnd($order = Criteria::ASC) Order by the dose_end column
 * @method     ChildPrescriptionQuery orderByInterval($order = Criteria::ASC) Order by the interval column
 * @method     ChildPrescriptionQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildPrescriptionQuery orderByComment($order = Criteria::ASC) Order by the comment column
 * @method     ChildPrescriptionQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPrescriptionQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPrescriptionQuery groupByUuid() Group by the uuid column
 * @method     ChildPrescriptionQuery groupByMedicationUuid() Group by the medication_uuid column
 * @method     ChildPrescriptionQuery groupByPatientUuid() Group by the patient_uuid column
 * @method     ChildPrescriptionQuery groupByDoseQuantity() Group by the dose_quantity column
 * @method     ChildPrescriptionQuery groupByDoseMorning() Group by the dose_morning column
 * @method     ChildPrescriptionQuery groupByDoseNoon() Group by the dose_noon column
 * @method     ChildPrescriptionQuery groupByDoseEvening() Group by the dose_evening column
 * @method     ChildPrescriptionQuery groupByDoseNight() Group by the dose_night column
 * @method     ChildPrescriptionQuery groupByMaxDosePeriod() Group by the max_dose_period column
 * @method     ChildPrescriptionQuery groupByPatientInstruction() Group by the patient_instruction column
 * @method     ChildPrescriptionQuery groupByMedicationAdministrationMethodId() Group by the medication_administration_method_id column
 * @method     ChildPrescriptionQuery groupByDoseStart() Group by the dose_start column
 * @method     ChildPrescriptionQuery groupByDoseEnd() Group by the dose_end column
 * @method     ChildPrescriptionQuery groupByInterval() Group by the interval column
 * @method     ChildPrescriptionQuery groupByUserId() Group by the user_id column
 * @method     ChildPrescriptionQuery groupByComment() Group by the comment column
 * @method     ChildPrescriptionQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPrescriptionQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPrescriptionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPrescriptionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPrescriptionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPrescriptionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPrescriptionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPrescriptionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPrescriptionQuery leftJoinMedicationAdministrationMethod($relationAlias = null) Adds a LEFT JOIN clause to the query using the MedicationAdministrationMethod relation
 * @method     ChildPrescriptionQuery rightJoinMedicationAdministrationMethod($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MedicationAdministrationMethod relation
 * @method     ChildPrescriptionQuery innerJoinMedicationAdministrationMethod($relationAlias = null) Adds a INNER JOIN clause to the query using the MedicationAdministrationMethod relation
 *
 * @method     ChildPrescriptionQuery joinWithMedicationAdministrationMethod($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MedicationAdministrationMethod relation
 *
 * @method     ChildPrescriptionQuery leftJoinWithMedicationAdministrationMethod() Adds a LEFT JOIN clause and with to the query using the MedicationAdministrationMethod relation
 * @method     ChildPrescriptionQuery rightJoinWithMedicationAdministrationMethod() Adds a RIGHT JOIN clause and with to the query using the MedicationAdministrationMethod relation
 * @method     ChildPrescriptionQuery innerJoinWithMedicationAdministrationMethod() Adds a INNER JOIN clause and with to the query using the MedicationAdministrationMethod relation
 *
 * @method     ChildPrescriptionQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildPrescriptionQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildPrescriptionQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildPrescriptionQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildPrescriptionQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildPrescriptionQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildPrescriptionQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     ChildPrescriptionQuery leftJoinPatient($relationAlias = null) Adds a LEFT JOIN clause to the query using the Patient relation
 * @method     ChildPrescriptionQuery rightJoinPatient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Patient relation
 * @method     ChildPrescriptionQuery innerJoinPatient($relationAlias = null) Adds a INNER JOIN clause to the query using the Patient relation
 *
 * @method     ChildPrescriptionQuery joinWithPatient($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Patient relation
 *
 * @method     ChildPrescriptionQuery leftJoinWithPatient() Adds a LEFT JOIN clause and with to the query using the Patient relation
 * @method     ChildPrescriptionQuery rightJoinWithPatient() Adds a RIGHT JOIN clause and with to the query using the Patient relation
 * @method     ChildPrescriptionQuery innerJoinWithPatient() Adds a INNER JOIN clause and with to the query using the Patient relation
 *
 * @method     ChildPrescriptionQuery leftJoinMedication($relationAlias = null) Adds a LEFT JOIN clause to the query using the Medication relation
 * @method     ChildPrescriptionQuery rightJoinMedication($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Medication relation
 * @method     ChildPrescriptionQuery innerJoinMedication($relationAlias = null) Adds a INNER JOIN clause to the query using the Medication relation
 *
 * @method     ChildPrescriptionQuery joinWithMedication($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Medication relation
 *
 * @method     ChildPrescriptionQuery leftJoinWithMedication() Adds a LEFT JOIN clause and with to the query using the Medication relation
 * @method     ChildPrescriptionQuery rightJoinWithMedication() Adds a RIGHT JOIN clause and with to the query using the Medication relation
 * @method     ChildPrescriptionQuery innerJoinWithMedication() Adds a INNER JOIN clause and with to the query using the Medication relation
 *
 * @method     ChildPrescriptionQuery leftJoinPrescriptionDoseInstruction($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrescriptionDoseInstruction relation
 * @method     ChildPrescriptionQuery rightJoinPrescriptionDoseInstruction($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrescriptionDoseInstruction relation
 * @method     ChildPrescriptionQuery innerJoinPrescriptionDoseInstruction($relationAlias = null) Adds a INNER JOIN clause to the query using the PrescriptionDoseInstruction relation
 *
 * @method     ChildPrescriptionQuery joinWithPrescriptionDoseInstruction($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PrescriptionDoseInstruction relation
 *
 * @method     ChildPrescriptionQuery leftJoinWithPrescriptionDoseInstruction() Adds a LEFT JOIN clause and with to the query using the PrescriptionDoseInstruction relation
 * @method     ChildPrescriptionQuery rightJoinWithPrescriptionDoseInstruction() Adds a RIGHT JOIN clause and with to the query using the PrescriptionDoseInstruction relation
 * @method     ChildPrescriptionQuery innerJoinWithPrescriptionDoseInstruction() Adds a INNER JOIN clause and with to the query using the PrescriptionDoseInstruction relation
 *
 * @method     \MedicationAdministrationMethodQuery|\UserQuery|\PatientQuery|\MedicationQuery|\PrescriptionDoseInstructionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPrescription|null findOne(?ConnectionInterface $con = null) Return the first ChildPrescription matching the query
 * @method     ChildPrescription findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildPrescription matching the query, or a new ChildPrescription object populated from the query conditions when no match is found
 *
 * @method     ChildPrescription|null findOneByUuid(string $uuid) Return the first ChildPrescription filtered by the uuid column
 * @method     ChildPrescription|null findOneByMedicationUuid(string $medication_uuid) Return the first ChildPrescription filtered by the medication_uuid column
 * @method     ChildPrescription|null findOneByPatientUuid(string $patient_uuid) Return the first ChildPrescription filtered by the patient_uuid column
 * @method     ChildPrescription|null findOneByDoseQuantity(string $dose_quantity) Return the first ChildPrescription filtered by the dose_quantity column
 * @method     ChildPrescription|null findOneByDoseMorning(boolean $dose_morning) Return the first ChildPrescription filtered by the dose_morning column
 * @method     ChildPrescription|null findOneByDoseNoon(boolean $dose_noon) Return the first ChildPrescription filtered by the dose_noon column
 * @method     ChildPrescription|null findOneByDoseEvening(boolean $dose_evening) Return the first ChildPrescription filtered by the dose_evening column
 * @method     ChildPrescription|null findOneByDoseNight(boolean $dose_night) Return the first ChildPrescription filtered by the dose_night column
 * @method     ChildPrescription|null findOneByMaxDosePeriod(string $max_dose_period) Return the first ChildPrescription filtered by the max_dose_period column
 * @method     ChildPrescription|null findOneByPatientInstruction(string $patient_instruction) Return the first ChildPrescription filtered by the patient_instruction column
 * @method     ChildPrescription|null findOneByMedicationAdministrationMethodId(string $medication_administration_method_id) Return the first ChildPrescription filtered by the medication_administration_method_id column
 * @method     ChildPrescription|null findOneByDoseStart(string $dose_start) Return the first ChildPrescription filtered by the dose_start column
 * @method     ChildPrescription|null findOneByDoseEnd(string $dose_end) Return the first ChildPrescription filtered by the dose_end column
 * @method     ChildPrescription|null findOneByInterval(int $interval) Return the first ChildPrescription filtered by the interval column
 * @method     ChildPrescription|null findOneByUserId(int $user_id) Return the first ChildPrescription filtered by the user_id column
 * @method     ChildPrescription|null findOneByComment(string $comment) Return the first ChildPrescription filtered by the comment column
 * @method     ChildPrescription|null findOneByCreatedAt(string $created_at) Return the first ChildPrescription filtered by the created_at column
 * @method     ChildPrescription|null findOneByUpdatedAt(string $updated_at) Return the first ChildPrescription filtered by the updated_at column *

 * @method     ChildPrescription requirePk($key, ?ConnectionInterface $con = null) Return the ChildPrescription by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOne(?ConnectionInterface $con = null) Return the first ChildPrescription matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrescription requireOneByUuid(string $uuid) Return the first ChildPrescription filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByMedicationUuid(string $medication_uuid) Return the first ChildPrescription filtered by the medication_uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByPatientUuid(string $patient_uuid) Return the first ChildPrescription filtered by the patient_uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByDoseQuantity(string $dose_quantity) Return the first ChildPrescription filtered by the dose_quantity column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByDoseMorning(boolean $dose_morning) Return the first ChildPrescription filtered by the dose_morning column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByDoseNoon(boolean $dose_noon) Return the first ChildPrescription filtered by the dose_noon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByDoseEvening(boolean $dose_evening) Return the first ChildPrescription filtered by the dose_evening column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByDoseNight(boolean $dose_night) Return the first ChildPrescription filtered by the dose_night column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByMaxDosePeriod(string $max_dose_period) Return the first ChildPrescription filtered by the max_dose_period column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByPatientInstruction(string $patient_instruction) Return the first ChildPrescription filtered by the patient_instruction column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByMedicationAdministrationMethodId(string $medication_administration_method_id) Return the first ChildPrescription filtered by the medication_administration_method_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByDoseStart(string $dose_start) Return the first ChildPrescription filtered by the dose_start column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByDoseEnd(string $dose_end) Return the first ChildPrescription filtered by the dose_end column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByInterval(int $interval) Return the first ChildPrescription filtered by the interval column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByUserId(int $user_id) Return the first ChildPrescription filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByComment(string $comment) Return the first ChildPrescription filtered by the comment column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByCreatedAt(string $created_at) Return the first ChildPrescription filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescription requireOneByUpdatedAt(string $updated_at) Return the first ChildPrescription filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrescription[]|Collection find(?ConnectionInterface $con = null) Return ChildPrescription objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildPrescription> find(?ConnectionInterface $con = null) Return ChildPrescription objects based on current ModelCriteria
 * @method     ChildPrescription[]|Collection findByUuid(string $uuid) Return ChildPrescription objects filtered by the uuid column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByUuid(string $uuid) Return ChildPrescription objects filtered by the uuid column
 * @method     ChildPrescription[]|Collection findByMedicationUuid(string $medication_uuid) Return ChildPrescription objects filtered by the medication_uuid column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByMedicationUuid(string $medication_uuid) Return ChildPrescription objects filtered by the medication_uuid column
 * @method     ChildPrescription[]|Collection findByPatientUuid(string $patient_uuid) Return ChildPrescription objects filtered by the patient_uuid column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByPatientUuid(string $patient_uuid) Return ChildPrescription objects filtered by the patient_uuid column
 * @method     ChildPrescription[]|Collection findByDoseQuantity(string $dose_quantity) Return ChildPrescription objects filtered by the dose_quantity column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByDoseQuantity(string $dose_quantity) Return ChildPrescription objects filtered by the dose_quantity column
 * @method     ChildPrescription[]|Collection findByDoseMorning(boolean $dose_morning) Return ChildPrescription objects filtered by the dose_morning column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByDoseMorning(boolean $dose_morning) Return ChildPrescription objects filtered by the dose_morning column
 * @method     ChildPrescription[]|Collection findByDoseNoon(boolean $dose_noon) Return ChildPrescription objects filtered by the dose_noon column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByDoseNoon(boolean $dose_noon) Return ChildPrescription objects filtered by the dose_noon column
 * @method     ChildPrescription[]|Collection findByDoseEvening(boolean $dose_evening) Return ChildPrescription objects filtered by the dose_evening column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByDoseEvening(boolean $dose_evening) Return ChildPrescription objects filtered by the dose_evening column
 * @method     ChildPrescription[]|Collection findByDoseNight(boolean $dose_night) Return ChildPrescription objects filtered by the dose_night column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByDoseNight(boolean $dose_night) Return ChildPrescription objects filtered by the dose_night column
 * @method     ChildPrescription[]|Collection findByMaxDosePeriod(string $max_dose_period) Return ChildPrescription objects filtered by the max_dose_period column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByMaxDosePeriod(string $max_dose_period) Return ChildPrescription objects filtered by the max_dose_period column
 * @method     ChildPrescription[]|Collection findByPatientInstruction(string $patient_instruction) Return ChildPrescription objects filtered by the patient_instruction column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByPatientInstruction(string $patient_instruction) Return ChildPrescription objects filtered by the patient_instruction column
 * @method     ChildPrescription[]|Collection findByMedicationAdministrationMethodId(string $medication_administration_method_id) Return ChildPrescription objects filtered by the medication_administration_method_id column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByMedicationAdministrationMethodId(string $medication_administration_method_id) Return ChildPrescription objects filtered by the medication_administration_method_id column
 * @method     ChildPrescription[]|Collection findByDoseStart(string $dose_start) Return ChildPrescription objects filtered by the dose_start column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByDoseStart(string $dose_start) Return ChildPrescription objects filtered by the dose_start column
 * @method     ChildPrescription[]|Collection findByDoseEnd(string $dose_end) Return ChildPrescription objects filtered by the dose_end column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByDoseEnd(string $dose_end) Return ChildPrescription objects filtered by the dose_end column
 * @method     ChildPrescription[]|Collection findByInterval(int $interval) Return ChildPrescription objects filtered by the interval column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByInterval(int $interval) Return ChildPrescription objects filtered by the interval column
 * @method     ChildPrescription[]|Collection findByUserId(int $user_id) Return ChildPrescription objects filtered by the user_id column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByUserId(int $user_id) Return ChildPrescription objects filtered by the user_id column
 * @method     ChildPrescription[]|Collection findByComment(string $comment) Return ChildPrescription objects filtered by the comment column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByComment(string $comment) Return ChildPrescription objects filtered by the comment column
 * @method     ChildPrescription[]|Collection findByCreatedAt(string $created_at) Return ChildPrescription objects filtered by the created_at column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByCreatedAt(string $created_at) Return ChildPrescription objects filtered by the created_at column
 * @method     ChildPrescription[]|Collection findByUpdatedAt(string $updated_at) Return ChildPrescription objects filtered by the updated_at column
 * @psalm-method Collection&\Traversable<ChildPrescription> findByUpdatedAt(string $updated_at) Return ChildPrescription objects filtered by the updated_at column
 * @method     ChildPrescription[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildPrescription> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PrescriptionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PrescriptionQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\Prescription', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPrescriptionQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPrescriptionQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildPrescriptionQuery) {
            return $criteria;
        }
        $query = new ChildPrescriptionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPrescription|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PrescriptionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PrescriptionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrescription A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT uuid, medication_uuid, patient_uuid, dose_quantity, dose_morning, dose_noon, dose_evening, dose_night, max_dose_period, patient_instruction, medication_administration_method_id, dose_start, dose_end, interval, user_id, comment, created_at, updated_at FROM prescription WHERE uuid = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPrescription $obj */
            $obj = new ChildPrescription();
            $obj->hydrate($row);
            PrescriptionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildPrescription|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(PrescriptionTableMap::COL_UUID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(PrescriptionTableMap::COL_UUID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * $query->filterByUuid(['foo', 'bar']); // WHERE uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $uuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_UUID, $uuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the medication_uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByMedicationUuid('fooValue');   // WHERE medication_uuid = 'fooValue'
     * $query->filterByMedicationUuid('%fooValue%', Criteria::LIKE); // WHERE medication_uuid LIKE '%fooValue%'
     * $query->filterByMedicationUuid(['foo', 'bar']); // WHERE medication_uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $medicationUuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicationUuid($medicationUuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($medicationUuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_MEDICATION_UUID, $medicationUuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the patient_uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByPatientUuid('fooValue');   // WHERE patient_uuid = 'fooValue'
     * $query->filterByPatientUuid('%fooValue%', Criteria::LIKE); // WHERE patient_uuid LIKE '%fooValue%'
     * $query->filterByPatientUuid(['foo', 'bar']); // WHERE patient_uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $patientUuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPatientUuid($patientUuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($patientUuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_PATIENT_UUID, $patientUuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the dose_quantity column
     *
     * Example usage:
     * <code>
     * $query->filterByDoseQuantity('fooValue');   // WHERE dose_quantity = 'fooValue'
     * $query->filterByDoseQuantity('%fooValue%', Criteria::LIKE); // WHERE dose_quantity LIKE '%fooValue%'
     * $query->filterByDoseQuantity(['foo', 'bar']); // WHERE dose_quantity IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $doseQuantity The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDoseQuantity($doseQuantity = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($doseQuantity)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_DOSE_QUANTITY, $doseQuantity, $comparison);

        return $this;
    }

    /**
     * Filter the query on the dose_morning column
     *
     * Example usage:
     * <code>
     * $query->filterByDoseMorning(true); // WHERE dose_morning = true
     * $query->filterByDoseMorning('yes'); // WHERE dose_morning = true
     * </code>
     *
     * @param bool|string $doseMorning The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDoseMorning($doseMorning = null, ?string $comparison = null)
    {
        if (is_string($doseMorning)) {
            $doseMorning = in_array(strtolower($doseMorning), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_DOSE_MORNING, $doseMorning, $comparison);

        return $this;
    }

    /**
     * Filter the query on the dose_noon column
     *
     * Example usage:
     * <code>
     * $query->filterByDoseNoon(true); // WHERE dose_noon = true
     * $query->filterByDoseNoon('yes'); // WHERE dose_noon = true
     * </code>
     *
     * @param bool|string $doseNoon The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDoseNoon($doseNoon = null, ?string $comparison = null)
    {
        if (is_string($doseNoon)) {
            $doseNoon = in_array(strtolower($doseNoon), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_DOSE_NOON, $doseNoon, $comparison);

        return $this;
    }

    /**
     * Filter the query on the dose_evening column
     *
     * Example usage:
     * <code>
     * $query->filterByDoseEvening(true); // WHERE dose_evening = true
     * $query->filterByDoseEvening('yes'); // WHERE dose_evening = true
     * </code>
     *
     * @param bool|string $doseEvening The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDoseEvening($doseEvening = null, ?string $comparison = null)
    {
        if (is_string($doseEvening)) {
            $doseEvening = in_array(strtolower($doseEvening), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_DOSE_EVENING, $doseEvening, $comparison);

        return $this;
    }

    /**
     * Filter the query on the dose_night column
     *
     * Example usage:
     * <code>
     * $query->filterByDoseNight(true); // WHERE dose_night = true
     * $query->filterByDoseNight('yes'); // WHERE dose_night = true
     * </code>
     *
     * @param bool|string $doseNight The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDoseNight($doseNight = null, ?string $comparison = null)
    {
        if (is_string($doseNight)) {
            $doseNight = in_array(strtolower($doseNight), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_DOSE_NIGHT, $doseNight, $comparison);

        return $this;
    }

    /**
     * Filter the query on the max_dose_period column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxDosePeriod('fooValue');   // WHERE max_dose_period = 'fooValue'
     * $query->filterByMaxDosePeriod('%fooValue%', Criteria::LIKE); // WHERE max_dose_period LIKE '%fooValue%'
     * $query->filterByMaxDosePeriod(['foo', 'bar']); // WHERE max_dose_period IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $maxDosePeriod The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMaxDosePeriod($maxDosePeriod = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($maxDosePeriod)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_MAX_DOSE_PERIOD, $maxDosePeriod, $comparison);

        return $this;
    }

    /**
     * Filter the query on the patient_instruction column
     *
     * Example usage:
     * <code>
     * $query->filterByPatientInstruction('fooValue');   // WHERE patient_instruction = 'fooValue'
     * $query->filterByPatientInstruction('%fooValue%', Criteria::LIKE); // WHERE patient_instruction LIKE '%fooValue%'
     * $query->filterByPatientInstruction(['foo', 'bar']); // WHERE patient_instruction IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $patientInstruction The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPatientInstruction($patientInstruction = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($patientInstruction)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_PATIENT_INSTRUCTION, $patientInstruction, $comparison);

        return $this;
    }

    /**
     * Filter the query on the medication_administration_method_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMedicationAdministrationMethodId(1234); // WHERE medication_administration_method_id = 1234
     * $query->filterByMedicationAdministrationMethodId(array(12, 34)); // WHERE medication_administration_method_id IN (12, 34)
     * $query->filterByMedicationAdministrationMethodId(array('min' => 12)); // WHERE medication_administration_method_id > 12
     * </code>
     *
     * @see       filterByMedicationAdministrationMethod()
     *
     * @param mixed $medicationAdministrationMethodId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicationAdministrationMethodId($medicationAdministrationMethodId = null, ?string $comparison = null)
    {
        if (is_array($medicationAdministrationMethodId)) {
            $useMinMax = false;
            if (isset($medicationAdministrationMethodId['min'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID, $medicationAdministrationMethodId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($medicationAdministrationMethodId['max'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID, $medicationAdministrationMethodId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID, $medicationAdministrationMethodId, $comparison);

        return $this;
    }

    /**
     * Filter the query on the dose_start column
     *
     * Example usage:
     * <code>
     * $query->filterByDoseStart('2011-03-14'); // WHERE dose_start = '2011-03-14'
     * $query->filterByDoseStart('now'); // WHERE dose_start = '2011-03-14'
     * $query->filterByDoseStart(array('max' => 'yesterday')); // WHERE dose_start > '2011-03-13'
     * </code>
     *
     * @param mixed $doseStart The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDoseStart($doseStart = null, ?string $comparison = null)
    {
        if (is_array($doseStart)) {
            $useMinMax = false;
            if (isset($doseStart['min'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_DOSE_START, $doseStart['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($doseStart['max'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_DOSE_START, $doseStart['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_DOSE_START, $doseStart, $comparison);

        return $this;
    }

    /**
     * Filter the query on the dose_end column
     *
     * Example usage:
     * <code>
     * $query->filterByDoseEnd('2011-03-14'); // WHERE dose_end = '2011-03-14'
     * $query->filterByDoseEnd('now'); // WHERE dose_end = '2011-03-14'
     * $query->filterByDoseEnd(array('max' => 'yesterday')); // WHERE dose_end > '2011-03-13'
     * </code>
     *
     * @param mixed $doseEnd The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDoseEnd($doseEnd = null, ?string $comparison = null)
    {
        if (is_array($doseEnd)) {
            $useMinMax = false;
            if (isset($doseEnd['min'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_DOSE_END, $doseEnd['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($doseEnd['max'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_DOSE_END, $doseEnd['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_DOSE_END, $doseEnd, $comparison);

        return $this;
    }

    /**
     * Filter the query on the interval column
     *
     * Example usage:
     * <code>
     * $query->filterByInterval(1234); // WHERE interval = 1234
     * $query->filterByInterval(array(12, 34)); // WHERE interval IN (12, 34)
     * $query->filterByInterval(array('min' => 12)); // WHERE interval > 12
     * </code>
     *
     * @param mixed $interval The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByInterval($interval = null, ?string $comparison = null)
    {
        if (is_array($interval)) {
            $useMinMax = false;
            if (isset($interval['min'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_INTERVAL, $interval['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($interval['max'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_INTERVAL, $interval['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_INTERVAL, $interval, $comparison);

        return $this;
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUserId($userId = null, ?string $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_USER_ID, $userId, $comparison);

        return $this;
    }

    /**
     * Filter the query on the comment column
     *
     * Example usage:
     * <code>
     * $query->filterByComment('fooValue');   // WHERE comment = 'fooValue'
     * $query->filterByComment('%fooValue%', Criteria::LIKE); // WHERE comment LIKE '%fooValue%'
     * $query->filterByComment(['foo', 'bar']); // WHERE comment IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $comment The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByComment($comment = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comment)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_COMMENT, $comment, $comparison);

        return $this;
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, ?string $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_CREATED_AT, $createdAt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, ?string $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PrescriptionTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionTableMap::COL_UPDATED_AT, $updatedAt, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \MedicationAdministrationMethod object
     *
     * @param \MedicationAdministrationMethod|ObjectCollection $medicationAdministrationMethod The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicationAdministrationMethod($medicationAdministrationMethod, ?string $comparison = null)
    {
        if ($medicationAdministrationMethod instanceof \MedicationAdministrationMethod) {
            return $this
                ->addUsingAlias(PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID, $medicationAdministrationMethod->getId(), $comparison);
        } elseif ($medicationAdministrationMethod instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID, $medicationAdministrationMethod->toKeyValue('PrimaryKey', 'Id'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByMedicationAdministrationMethod() only accepts arguments of type \MedicationAdministrationMethod or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MedicationAdministrationMethod relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinMedicationAdministrationMethod(?string $relationAlias = null, ?string $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MedicationAdministrationMethod');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MedicationAdministrationMethod');
        }

        return $this;
    }

    /**
     * Use the MedicationAdministrationMethod relation MedicationAdministrationMethod object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MedicationAdministrationMethodQuery A secondary query class using the current class as primary query
     */
    public function useMedicationAdministrationMethodQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMedicationAdministrationMethod($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MedicationAdministrationMethod', '\MedicationAdministrationMethodQuery');
    }

    /**
     * Use the MedicationAdministrationMethod relation MedicationAdministrationMethod object
     *
     * @param callable(\MedicationAdministrationMethodQuery):\MedicationAdministrationMethodQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withMedicationAdministrationMethodQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useMedicationAdministrationMethodQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to MedicationAdministrationMethod table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \MedicationAdministrationMethodQuery The inner query object of the EXISTS statement
     */
    public function useMedicationAdministrationMethodExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('MedicationAdministrationMethod', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to MedicationAdministrationMethod table for a NOT EXISTS query.
     *
     * @see useMedicationAdministrationMethodExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \MedicationAdministrationMethodQuery The inner query object of the NOT EXISTS statement
     */
    public function useMedicationAdministrationMethodNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('MedicationAdministrationMethod', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \User object
     *
     * @param \User|ObjectCollection $user The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUser($user, ?string $comparison = null)
    {
        if ($user instanceof \User) {
            return $this
                ->addUsingAlias(PrescriptionTableMap::COL_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(PrescriptionTableMap::COL_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinUser(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\UserQuery');
    }

    /**
     * Use the User relation User object
     *
     * @param callable(\UserQuery):\UserQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUserQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUserQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to User table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \UserQuery The inner query object of the EXISTS statement
     */
    public function useUserExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('User', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to User table for a NOT EXISTS query.
     *
     * @see useUserExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \UserQuery The inner query object of the NOT EXISTS statement
     */
    public function useUserNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('User', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \Patient object
     *
     * @param \Patient|ObjectCollection $patient The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPatient($patient, ?string $comparison = null)
    {
        if ($patient instanceof \Patient) {
            return $this
                ->addUsingAlias(PrescriptionTableMap::COL_PATIENT_UUID, $patient->getUuid(), $comparison);
        } elseif ($patient instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(PrescriptionTableMap::COL_PATIENT_UUID, $patient->toKeyValue('PrimaryKey', 'Uuid'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByPatient() only accepts arguments of type \Patient or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Patient relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPatient(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Patient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Patient');
        }

        return $this;
    }

    /**
     * Use the Patient relation Patient object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PatientQuery A secondary query class using the current class as primary query
     */
    public function usePatientQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPatient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Patient', '\PatientQuery');
    }

    /**
     * Use the Patient relation Patient object
     *
     * @param callable(\PatientQuery):\PatientQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPatientQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePatientQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Patient table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PatientQuery The inner query object of the EXISTS statement
     */
    public function usePatientExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Patient', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Patient table for a NOT EXISTS query.
     *
     * @see usePatientExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PatientQuery The inner query object of the NOT EXISTS statement
     */
    public function usePatientNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Patient', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \Medication object
     *
     * @param \Medication|ObjectCollection $medication The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedication($medication, ?string $comparison = null)
    {
        if ($medication instanceof \Medication) {
            return $this
                ->addUsingAlias(PrescriptionTableMap::COL_MEDICATION_UUID, $medication->getUuid(), $comparison);
        } elseif ($medication instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(PrescriptionTableMap::COL_MEDICATION_UUID, $medication->toKeyValue('PrimaryKey', 'Uuid'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByMedication() only accepts arguments of type \Medication or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Medication relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinMedication(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Medication');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Medication');
        }

        return $this;
    }

    /**
     * Use the Medication relation Medication object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MedicationQuery A secondary query class using the current class as primary query
     */
    public function useMedicationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMedication($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Medication', '\MedicationQuery');
    }

    /**
     * Use the Medication relation Medication object
     *
     * @param callable(\MedicationQuery):\MedicationQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withMedicationQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useMedicationQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Medication table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \MedicationQuery The inner query object of the EXISTS statement
     */
    public function useMedicationExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Medication', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Medication table for a NOT EXISTS query.
     *
     * @see useMedicationExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \MedicationQuery The inner query object of the NOT EXISTS statement
     */
    public function useMedicationNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Medication', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \PrescriptionDoseInstruction object
     *
     * @param \PrescriptionDoseInstruction|ObjectCollection $prescriptionDoseInstruction the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrescriptionDoseInstruction($prescriptionDoseInstruction, ?string $comparison = null)
    {
        if ($prescriptionDoseInstruction instanceof \PrescriptionDoseInstruction) {
            $this
                ->addUsingAlias(PrescriptionTableMap::COL_UUID, $prescriptionDoseInstruction->getPrescriptionUuid(), $comparison);

            return $this;
        } elseif ($prescriptionDoseInstruction instanceof ObjectCollection) {
            $this
                ->usePrescriptionDoseInstructionQuery()
                ->filterByPrimaryKeys($prescriptionDoseInstruction->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByPrescriptionDoseInstruction() only accepts arguments of type \PrescriptionDoseInstruction or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrescriptionDoseInstruction relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPrescriptionDoseInstruction(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrescriptionDoseInstruction');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrescriptionDoseInstruction');
        }

        return $this;
    }

    /**
     * Use the PrescriptionDoseInstruction relation PrescriptionDoseInstruction object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrescriptionDoseInstructionQuery A secondary query class using the current class as primary query
     */
    public function usePrescriptionDoseInstructionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrescriptionDoseInstruction($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrescriptionDoseInstruction', '\PrescriptionDoseInstructionQuery');
    }

    /**
     * Use the PrescriptionDoseInstruction relation PrescriptionDoseInstruction object
     *
     * @param callable(\PrescriptionDoseInstructionQuery):\PrescriptionDoseInstructionQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPrescriptionDoseInstructionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePrescriptionDoseInstructionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to PrescriptionDoseInstruction table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PrescriptionDoseInstructionQuery The inner query object of the EXISTS statement
     */
    public function usePrescriptionDoseInstructionExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('PrescriptionDoseInstruction', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to PrescriptionDoseInstruction table for a NOT EXISTS query.
     *
     * @see usePrescriptionDoseInstructionExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PrescriptionDoseInstructionQuery The inner query object of the NOT EXISTS statement
     */
    public function usePrescriptionDoseInstructionNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('PrescriptionDoseInstruction', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related MedicationDoseInstruction object
     * using the prescription_dose_instruction table as cross reference
     *
     * @param MedicationDoseInstruction $medicationDoseInstruction the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicationDoseInstruction($medicationDoseInstruction, string $comparison = Criteria::EQUAL)
    {
        $this
            ->usePrescriptionDoseInstructionQuery()
            ->filterByMedicationDoseInstruction($medicationDoseInstruction, $comparison)
            ->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param ChildPrescription $prescription Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($prescription = null)
    {
        if ($prescription) {
            $this->addUsingAlias(PrescriptionTableMap::COL_UUID, $prescription->getUuid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the prescription table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrescriptionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PrescriptionTableMap::clearInstancePool();
            PrescriptionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrescriptionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PrescriptionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PrescriptionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PrescriptionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param int $nbDays Maximum age of the latest update in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        $this->addUsingAlias(PrescriptionTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by update date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        $this->addDescendingOrderByColumn(PrescriptionTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by update date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        $this->addAscendingOrderByColumn(PrescriptionTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by create date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        $this->addDescendingOrderByColumn(PrescriptionTableMap::COL_CREATED_AT);

        return $this;
    }

    /**
     * Filter by the latest created
     *
     * @param int $nbDays Maximum age of in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        $this->addUsingAlias(PrescriptionTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by create date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        $this->addAscendingOrderByColumn(PrescriptionTableMap::COL_CREATED_AT);

        return $this;
    }

}
