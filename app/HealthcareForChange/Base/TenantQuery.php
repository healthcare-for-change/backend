<?php

namespace Base;

use \Tenant as ChildTenant;
use \TenantQuery as ChildTenantQuery;
use \Exception;
use \PDO;
use Map\TenantTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'tenant' table.
 *
 * Core table for holding tenant information
 *
 * @method     ChildTenantQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildTenantQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildTenantQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildTenantQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildTenantQuery groupByUuid() Group by the uuid column
 * @method     ChildTenantQuery groupByName() Group by the name column
 * @method     ChildTenantQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildTenantQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildTenantQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTenantQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTenantQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTenantQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTenantQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTenantQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTenantQuery leftJoinPatient($relationAlias = null) Adds a LEFT JOIN clause to the query using the Patient relation
 * @method     ChildTenantQuery rightJoinPatient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Patient relation
 * @method     ChildTenantQuery innerJoinPatient($relationAlias = null) Adds a INNER JOIN clause to the query using the Patient relation
 *
 * @method     ChildTenantQuery joinWithPatient($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Patient relation
 *
 * @method     ChildTenantQuery leftJoinWithPatient() Adds a LEFT JOIN clause and with to the query using the Patient relation
 * @method     ChildTenantQuery rightJoinWithPatient() Adds a RIGHT JOIN clause and with to the query using the Patient relation
 * @method     ChildTenantQuery innerJoinWithPatient() Adds a INNER JOIN clause and with to the query using the Patient relation
 *
 * @method     ChildTenantQuery leftJoinVaccinationPlan($relationAlias = null) Adds a LEFT JOIN clause to the query using the VaccinationPlan relation
 * @method     ChildTenantQuery rightJoinVaccinationPlan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VaccinationPlan relation
 * @method     ChildTenantQuery innerJoinVaccinationPlan($relationAlias = null) Adds a INNER JOIN clause to the query using the VaccinationPlan relation
 *
 * @method     ChildTenantQuery joinWithVaccinationPlan($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the VaccinationPlan relation
 *
 * @method     ChildTenantQuery leftJoinWithVaccinationPlan() Adds a LEFT JOIN clause and with to the query using the VaccinationPlan relation
 * @method     ChildTenantQuery rightJoinWithVaccinationPlan() Adds a RIGHT JOIN clause and with to the query using the VaccinationPlan relation
 * @method     ChildTenantQuery innerJoinWithVaccinationPlan() Adds a INNER JOIN clause and with to the query using the VaccinationPlan relation
 *
 * @method     ChildTenantQuery leftJoinUserHasTenant($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserHasTenant relation
 * @method     ChildTenantQuery rightJoinUserHasTenant($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserHasTenant relation
 * @method     ChildTenantQuery innerJoinUserHasTenant($relationAlias = null) Adds a INNER JOIN clause to the query using the UserHasTenant relation
 *
 * @method     ChildTenantQuery joinWithUserHasTenant($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserHasTenant relation
 *
 * @method     ChildTenantQuery leftJoinWithUserHasTenant() Adds a LEFT JOIN clause and with to the query using the UserHasTenant relation
 * @method     ChildTenantQuery rightJoinWithUserHasTenant() Adds a RIGHT JOIN clause and with to the query using the UserHasTenant relation
 * @method     ChildTenantQuery innerJoinWithUserHasTenant() Adds a INNER JOIN clause and with to the query using the UserHasTenant relation
 *
 * @method     \PatientQuery|\VaccinationPlanQuery|\UserHasTenantQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTenant|null findOne(?ConnectionInterface $con = null) Return the first ChildTenant matching the query
 * @method     ChildTenant findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildTenant matching the query, or a new ChildTenant object populated from the query conditions when no match is found
 *
 * @method     ChildTenant|null findOneByUuid(string $uuid) Return the first ChildTenant filtered by the uuid column
 * @method     ChildTenant|null findOneByName(string $name) Return the first ChildTenant filtered by the name column
 * @method     ChildTenant|null findOneByCreatedAt(string $created_at) Return the first ChildTenant filtered by the created_at column
 * @method     ChildTenant|null findOneByUpdatedAt(string $updated_at) Return the first ChildTenant filtered by the updated_at column *

 * @method     ChildTenant requirePk($key, ?ConnectionInterface $con = null) Return the ChildTenant by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOne(?ConnectionInterface $con = null) Return the first ChildTenant matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTenant requireOneByUuid(string $uuid) Return the first ChildTenant filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByName(string $name) Return the first ChildTenant filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByCreatedAt(string $created_at) Return the first ChildTenant filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByUpdatedAt(string $updated_at) Return the first ChildTenant filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTenant[]|Collection find(?ConnectionInterface $con = null) Return ChildTenant objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildTenant> find(?ConnectionInterface $con = null) Return ChildTenant objects based on current ModelCriteria
 * @method     ChildTenant[]|Collection findByUuid(string $uuid) Return ChildTenant objects filtered by the uuid column
 * @psalm-method Collection&\Traversable<ChildTenant> findByUuid(string $uuid) Return ChildTenant objects filtered by the uuid column
 * @method     ChildTenant[]|Collection findByName(string $name) Return ChildTenant objects filtered by the name column
 * @psalm-method Collection&\Traversable<ChildTenant> findByName(string $name) Return ChildTenant objects filtered by the name column
 * @method     ChildTenant[]|Collection findByCreatedAt(string $created_at) Return ChildTenant objects filtered by the created_at column
 * @psalm-method Collection&\Traversable<ChildTenant> findByCreatedAt(string $created_at) Return ChildTenant objects filtered by the created_at column
 * @method     ChildTenant[]|Collection findByUpdatedAt(string $updated_at) Return ChildTenant objects filtered by the updated_at column
 * @psalm-method Collection&\Traversable<ChildTenant> findByUpdatedAt(string $updated_at) Return ChildTenant objects filtered by the updated_at column
 * @method     ChildTenant[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildTenant> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TenantQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TenantQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\Tenant', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTenantQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTenantQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildTenantQuery) {
            return $criteria;
        }
        $query = new ChildTenantQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTenant|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TenantTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TenantTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTenant A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT uuid, name, created_at, updated_at FROM tenant WHERE uuid = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTenant $obj */
            $obj = new ChildTenant();
            $obj->hydrate($row);
            TenantTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildTenant|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(TenantTableMap::COL_UUID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(TenantTableMap::COL_UUID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * $query->filterByUuid(['foo', 'bar']); // WHERE uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $uuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(TenantTableMap::COL_UUID, $uuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * $query->filterByName(['foo', 'bar']); // WHERE name IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $name The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByName($name = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(TenantTableMap::COL_NAME, $name, $comparison);

        return $this;
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, ?string $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(TenantTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(TenantTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(TenantTableMap::COL_CREATED_AT, $createdAt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, ?string $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(TenantTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(TenantTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(TenantTableMap::COL_UPDATED_AT, $updatedAt, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \Patient object
     *
     * @param \Patient|ObjectCollection $patient the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPatient($patient, ?string $comparison = null)
    {
        if ($patient instanceof \Patient) {
            $this
                ->addUsingAlias(TenantTableMap::COL_UUID, $patient->getTenantUuid(), $comparison);

            return $this;
        } elseif ($patient instanceof ObjectCollection) {
            $this
                ->usePatientQuery()
                ->filterByPrimaryKeys($patient->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByPatient() only accepts arguments of type \Patient or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Patient relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPatient(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Patient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Patient');
        }

        return $this;
    }

    /**
     * Use the Patient relation Patient object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PatientQuery A secondary query class using the current class as primary query
     */
    public function usePatientQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPatient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Patient', '\PatientQuery');
    }

    /**
     * Use the Patient relation Patient object
     *
     * @param callable(\PatientQuery):\PatientQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPatientQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePatientQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Patient table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PatientQuery The inner query object of the EXISTS statement
     */
    public function usePatientExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Patient', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Patient table for a NOT EXISTS query.
     *
     * @see usePatientExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PatientQuery The inner query object of the NOT EXISTS statement
     */
    public function usePatientNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Patient', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \VaccinationPlan object
     *
     * @param \VaccinationPlan|ObjectCollection $vaccinationPlan the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVaccinationPlan($vaccinationPlan, ?string $comparison = null)
    {
        if ($vaccinationPlan instanceof \VaccinationPlan) {
            $this
                ->addUsingAlias(TenantTableMap::COL_UUID, $vaccinationPlan->getTenantUuid(), $comparison);

            return $this;
        } elseif ($vaccinationPlan instanceof ObjectCollection) {
            $this
                ->useVaccinationPlanQuery()
                ->filterByPrimaryKeys($vaccinationPlan->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByVaccinationPlan() only accepts arguments of type \VaccinationPlan or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VaccinationPlan relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinVaccinationPlan(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VaccinationPlan');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VaccinationPlan');
        }

        return $this;
    }

    /**
     * Use the VaccinationPlan relation VaccinationPlan object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VaccinationPlanQuery A secondary query class using the current class as primary query
     */
    public function useVaccinationPlanQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVaccinationPlan($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VaccinationPlan', '\VaccinationPlanQuery');
    }

    /**
     * Use the VaccinationPlan relation VaccinationPlan object
     *
     * @param callable(\VaccinationPlanQuery):\VaccinationPlanQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withVaccinationPlanQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useVaccinationPlanQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to VaccinationPlan table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \VaccinationPlanQuery The inner query object of the EXISTS statement
     */
    public function useVaccinationPlanExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('VaccinationPlan', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to VaccinationPlan table for a NOT EXISTS query.
     *
     * @see useVaccinationPlanExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \VaccinationPlanQuery The inner query object of the NOT EXISTS statement
     */
    public function useVaccinationPlanNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('VaccinationPlan', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \UserHasTenant object
     *
     * @param \UserHasTenant|ObjectCollection $userHasTenant the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUserHasTenant($userHasTenant, ?string $comparison = null)
    {
        if ($userHasTenant instanceof \UserHasTenant) {
            $this
                ->addUsingAlias(TenantTableMap::COL_UUID, $userHasTenant->getTenantUuid(), $comparison);

            return $this;
        } elseif ($userHasTenant instanceof ObjectCollection) {
            $this
                ->useUserHasTenantQuery()
                ->filterByPrimaryKeys($userHasTenant->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByUserHasTenant() only accepts arguments of type \UserHasTenant or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserHasTenant relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinUserHasTenant(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserHasTenant');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserHasTenant');
        }

        return $this;
    }

    /**
     * Use the UserHasTenant relation UserHasTenant object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserHasTenantQuery A secondary query class using the current class as primary query
     */
    public function useUserHasTenantQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserHasTenant($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserHasTenant', '\UserHasTenantQuery');
    }

    /**
     * Use the UserHasTenant relation UserHasTenant object
     *
     * @param callable(\UserHasTenantQuery):\UserHasTenantQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUserHasTenantQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUserHasTenantQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to UserHasTenant table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \UserHasTenantQuery The inner query object of the EXISTS statement
     */
    public function useUserHasTenantExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('UserHasTenant', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to UserHasTenant table for a NOT EXISTS query.
     *
     * @see useUserHasTenantExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \UserHasTenantQuery The inner query object of the NOT EXISTS statement
     */
    public function useUserHasTenantNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('UserHasTenant', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related User object
     * using the user_has_tenant table as cross reference
     *
     * @param User $user the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUser($user, string $comparison = Criteria::EQUAL)
    {
        $this
            ->useUserHasTenantQuery()
            ->filterByUser($user, $comparison)
            ->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param ChildTenant $tenant Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($tenant = null)
    {
        if ($tenant) {
            $this->addUsingAlias(TenantTableMap::COL_UUID, $tenant->getUuid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the tenant table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TenantTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TenantTableMap::clearInstancePool();
            TenantTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TenantTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TenantTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TenantTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TenantTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param int $nbDays Maximum age of the latest update in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        $this->addUsingAlias(TenantTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by update date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        $this->addDescendingOrderByColumn(TenantTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by update date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        $this->addAscendingOrderByColumn(TenantTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by create date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        $this->addDescendingOrderByColumn(TenantTableMap::COL_CREATED_AT);

        return $this;
    }

    /**
     * Filter by the latest created
     *
     * @param int $nbDays Maximum age of in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        $this->addUsingAlias(TenantTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by create date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        $this->addAscendingOrderByColumn(TenantTableMap::COL_CREATED_AT);

        return $this;
    }

}
