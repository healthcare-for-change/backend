<?php

namespace Base;

use \MedicalHistory as ChildMedicalHistory;
use \MedicalHistoryQuery as ChildMedicalHistoryQuery;
use \Exception;
use \PDO;
use Map\MedicalHistoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'medical_history' table.
 *
 *
 *
 * @method     ChildMedicalHistoryQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildMedicalHistoryQuery orderByPatientUuid($order = Criteria::ASC) Order by the patient_uuid column
 * @method     ChildMedicalHistoryQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildMedicalHistoryQuery orderByClinicalStatus($order = Criteria::ASC) Order by the clinical_status column
 * @method     ChildMedicalHistoryQuery orderByVerificationStatus($order = Criteria::ASC) Order by the verification_status column
 * @method     ChildMedicalHistoryQuery orderBySeverity($order = Criteria::ASC) Order by the severity column
 * @method     ChildMedicalHistoryQuery orderByNote($order = Criteria::ASC) Order by the note column
 * @method     ChildMedicalHistoryQuery orderByMedicalDiagnosisCodeId($order = Criteria::ASC) Order by the medical_diagnosis_code_id column
 * @method     ChildMedicalHistoryQuery orderByOnsetDate($order = Criteria::ASC) Order by the onset_date column
 * @method     ChildMedicalHistoryQuery orderByOnsetAge($order = Criteria::ASC) Order by the onset_age column
 * @method     ChildMedicalHistoryQuery orderByAbatementDate($order = Criteria::ASC) Order by the abatement_date column
 * @method     ChildMedicalHistoryQuery orderByAbatementAge($order = Criteria::ASC) Order by the abatement_age column
 * @method     ChildMedicalHistoryQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildMedicalHistoryQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildMedicalHistoryQuery groupByUuid() Group by the uuid column
 * @method     ChildMedicalHistoryQuery groupByPatientUuid() Group by the patient_uuid column
 * @method     ChildMedicalHistoryQuery groupByUserId() Group by the user_id column
 * @method     ChildMedicalHistoryQuery groupByClinicalStatus() Group by the clinical_status column
 * @method     ChildMedicalHistoryQuery groupByVerificationStatus() Group by the verification_status column
 * @method     ChildMedicalHistoryQuery groupBySeverity() Group by the severity column
 * @method     ChildMedicalHistoryQuery groupByNote() Group by the note column
 * @method     ChildMedicalHistoryQuery groupByMedicalDiagnosisCodeId() Group by the medical_diagnosis_code_id column
 * @method     ChildMedicalHistoryQuery groupByOnsetDate() Group by the onset_date column
 * @method     ChildMedicalHistoryQuery groupByOnsetAge() Group by the onset_age column
 * @method     ChildMedicalHistoryQuery groupByAbatementDate() Group by the abatement_date column
 * @method     ChildMedicalHistoryQuery groupByAbatementAge() Group by the abatement_age column
 * @method     ChildMedicalHistoryQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildMedicalHistoryQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildMedicalHistoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMedicalHistoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMedicalHistoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMedicalHistoryQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMedicalHistoryQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMedicalHistoryQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMedicalHistoryQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildMedicalHistoryQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildMedicalHistoryQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildMedicalHistoryQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildMedicalHistoryQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildMedicalHistoryQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildMedicalHistoryQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     ChildMedicalHistoryQuery leftJoinPatient($relationAlias = null) Adds a LEFT JOIN clause to the query using the Patient relation
 * @method     ChildMedicalHistoryQuery rightJoinPatient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Patient relation
 * @method     ChildMedicalHistoryQuery innerJoinPatient($relationAlias = null) Adds a INNER JOIN clause to the query using the Patient relation
 *
 * @method     ChildMedicalHistoryQuery joinWithPatient($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Patient relation
 *
 * @method     ChildMedicalHistoryQuery leftJoinWithPatient() Adds a LEFT JOIN clause and with to the query using the Patient relation
 * @method     ChildMedicalHistoryQuery rightJoinWithPatient() Adds a RIGHT JOIN clause and with to the query using the Patient relation
 * @method     ChildMedicalHistoryQuery innerJoinWithPatient() Adds a INNER JOIN clause and with to the query using the Patient relation
 *
 * @method     ChildMedicalHistoryQuery leftJoinMedicalDiagnosisCode($relationAlias = null) Adds a LEFT JOIN clause to the query using the MedicalDiagnosisCode relation
 * @method     ChildMedicalHistoryQuery rightJoinMedicalDiagnosisCode($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MedicalDiagnosisCode relation
 * @method     ChildMedicalHistoryQuery innerJoinMedicalDiagnosisCode($relationAlias = null) Adds a INNER JOIN clause to the query using the MedicalDiagnosisCode relation
 *
 * @method     ChildMedicalHistoryQuery joinWithMedicalDiagnosisCode($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MedicalDiagnosisCode relation
 *
 * @method     ChildMedicalHistoryQuery leftJoinWithMedicalDiagnosisCode() Adds a LEFT JOIN clause and with to the query using the MedicalDiagnosisCode relation
 * @method     ChildMedicalHistoryQuery rightJoinWithMedicalDiagnosisCode() Adds a RIGHT JOIN clause and with to the query using the MedicalDiagnosisCode relation
 * @method     ChildMedicalHistoryQuery innerJoinWithMedicalDiagnosisCode() Adds a INNER JOIN clause and with to the query using the MedicalDiagnosisCode relation
 *
 * @method     \UserQuery|\PatientQuery|\MedicalDiagnosisCodeQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMedicalHistory|null findOne(?ConnectionInterface $con = null) Return the first ChildMedicalHistory matching the query
 * @method     ChildMedicalHistory findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildMedicalHistory matching the query, or a new ChildMedicalHistory object populated from the query conditions when no match is found
 *
 * @method     ChildMedicalHistory|null findOneByUuid(string $uuid) Return the first ChildMedicalHistory filtered by the uuid column
 * @method     ChildMedicalHistory|null findOneByPatientUuid(string $patient_uuid) Return the first ChildMedicalHistory filtered by the patient_uuid column
 * @method     ChildMedicalHistory|null findOneByUserId(int $user_id) Return the first ChildMedicalHistory filtered by the user_id column
 * @method     ChildMedicalHistory|null findOneByClinicalStatus(int $clinical_status) Return the first ChildMedicalHistory filtered by the clinical_status column
 * @method     ChildMedicalHistory|null findOneByVerificationStatus(int $verification_status) Return the first ChildMedicalHistory filtered by the verification_status column
 * @method     ChildMedicalHistory|null findOneBySeverity(int $severity) Return the first ChildMedicalHistory filtered by the severity column
 * @method     ChildMedicalHistory|null findOneByNote(string $note) Return the first ChildMedicalHistory filtered by the note column
 * @method     ChildMedicalHistory|null findOneByMedicalDiagnosisCodeId(string $medical_diagnosis_code_id) Return the first ChildMedicalHistory filtered by the medical_diagnosis_code_id column
 * @method     ChildMedicalHistory|null findOneByOnsetDate(string $onset_date) Return the first ChildMedicalHistory filtered by the onset_date column
 * @method     ChildMedicalHistory|null findOneByOnsetAge(int $onset_age) Return the first ChildMedicalHistory filtered by the onset_age column
 * @method     ChildMedicalHistory|null findOneByAbatementDate(string $abatement_date) Return the first ChildMedicalHistory filtered by the abatement_date column
 * @method     ChildMedicalHistory|null findOneByAbatementAge(int $abatement_age) Return the first ChildMedicalHistory filtered by the abatement_age column
 * @method     ChildMedicalHistory|null findOneByCreatedAt(string $created_at) Return the first ChildMedicalHistory filtered by the created_at column
 * @method     ChildMedicalHistory|null findOneByUpdatedAt(string $updated_at) Return the first ChildMedicalHistory filtered by the updated_at column *

 * @method     ChildMedicalHistory requirePk($key, ?ConnectionInterface $con = null) Return the ChildMedicalHistory by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOne(?ConnectionInterface $con = null) Return the first ChildMedicalHistory matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMedicalHistory requireOneByUuid(string $uuid) Return the first ChildMedicalHistory filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneByPatientUuid(string $patient_uuid) Return the first ChildMedicalHistory filtered by the patient_uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneByUserId(int $user_id) Return the first ChildMedicalHistory filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneByClinicalStatus(int $clinical_status) Return the first ChildMedicalHistory filtered by the clinical_status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneByVerificationStatus(int $verification_status) Return the first ChildMedicalHistory filtered by the verification_status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneBySeverity(int $severity) Return the first ChildMedicalHistory filtered by the severity column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneByNote(string $note) Return the first ChildMedicalHistory filtered by the note column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneByMedicalDiagnosisCodeId(string $medical_diagnosis_code_id) Return the first ChildMedicalHistory filtered by the medical_diagnosis_code_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneByOnsetDate(string $onset_date) Return the first ChildMedicalHistory filtered by the onset_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneByOnsetAge(int $onset_age) Return the first ChildMedicalHistory filtered by the onset_age column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneByAbatementDate(string $abatement_date) Return the first ChildMedicalHistory filtered by the abatement_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneByAbatementAge(int $abatement_age) Return the first ChildMedicalHistory filtered by the abatement_age column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneByCreatedAt(string $created_at) Return the first ChildMedicalHistory filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicalHistory requireOneByUpdatedAt(string $updated_at) Return the first ChildMedicalHistory filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMedicalHistory[]|Collection find(?ConnectionInterface $con = null) Return ChildMedicalHistory objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> find(?ConnectionInterface $con = null) Return ChildMedicalHistory objects based on current ModelCriteria
 * @method     ChildMedicalHistory[]|Collection findByUuid(string $uuid) Return ChildMedicalHistory objects filtered by the uuid column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByUuid(string $uuid) Return ChildMedicalHistory objects filtered by the uuid column
 * @method     ChildMedicalHistory[]|Collection findByPatientUuid(string $patient_uuid) Return ChildMedicalHistory objects filtered by the patient_uuid column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByPatientUuid(string $patient_uuid) Return ChildMedicalHistory objects filtered by the patient_uuid column
 * @method     ChildMedicalHistory[]|Collection findByUserId(int $user_id) Return ChildMedicalHistory objects filtered by the user_id column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByUserId(int $user_id) Return ChildMedicalHistory objects filtered by the user_id column
 * @method     ChildMedicalHistory[]|Collection findByClinicalStatus(int $clinical_status) Return ChildMedicalHistory objects filtered by the clinical_status column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByClinicalStatus(int $clinical_status) Return ChildMedicalHistory objects filtered by the clinical_status column
 * @method     ChildMedicalHistory[]|Collection findByVerificationStatus(int $verification_status) Return ChildMedicalHistory objects filtered by the verification_status column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByVerificationStatus(int $verification_status) Return ChildMedicalHistory objects filtered by the verification_status column
 * @method     ChildMedicalHistory[]|Collection findBySeverity(int $severity) Return ChildMedicalHistory objects filtered by the severity column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findBySeverity(int $severity) Return ChildMedicalHistory objects filtered by the severity column
 * @method     ChildMedicalHistory[]|Collection findByNote(string $note) Return ChildMedicalHistory objects filtered by the note column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByNote(string $note) Return ChildMedicalHistory objects filtered by the note column
 * @method     ChildMedicalHistory[]|Collection findByMedicalDiagnosisCodeId(string $medical_diagnosis_code_id) Return ChildMedicalHistory objects filtered by the medical_diagnosis_code_id column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByMedicalDiagnosisCodeId(string $medical_diagnosis_code_id) Return ChildMedicalHistory objects filtered by the medical_diagnosis_code_id column
 * @method     ChildMedicalHistory[]|Collection findByOnsetDate(string $onset_date) Return ChildMedicalHistory objects filtered by the onset_date column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByOnsetDate(string $onset_date) Return ChildMedicalHistory objects filtered by the onset_date column
 * @method     ChildMedicalHistory[]|Collection findByOnsetAge(int $onset_age) Return ChildMedicalHistory objects filtered by the onset_age column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByOnsetAge(int $onset_age) Return ChildMedicalHistory objects filtered by the onset_age column
 * @method     ChildMedicalHistory[]|Collection findByAbatementDate(string $abatement_date) Return ChildMedicalHistory objects filtered by the abatement_date column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByAbatementDate(string $abatement_date) Return ChildMedicalHistory objects filtered by the abatement_date column
 * @method     ChildMedicalHistory[]|Collection findByAbatementAge(int $abatement_age) Return ChildMedicalHistory objects filtered by the abatement_age column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByAbatementAge(int $abatement_age) Return ChildMedicalHistory objects filtered by the abatement_age column
 * @method     ChildMedicalHistory[]|Collection findByCreatedAt(string $created_at) Return ChildMedicalHistory objects filtered by the created_at column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByCreatedAt(string $created_at) Return ChildMedicalHistory objects filtered by the created_at column
 * @method     ChildMedicalHistory[]|Collection findByUpdatedAt(string $updated_at) Return ChildMedicalHistory objects filtered by the updated_at column
 * @psalm-method Collection&\Traversable<ChildMedicalHistory> findByUpdatedAt(string $updated_at) Return ChildMedicalHistory objects filtered by the updated_at column
 * @method     ChildMedicalHistory[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildMedicalHistory> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MedicalHistoryQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MedicalHistoryQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\MedicalHistory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMedicalHistoryQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMedicalHistoryQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildMedicalHistoryQuery) {
            return $criteria;
        }
        $query = new ChildMedicalHistoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMedicalHistory|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MedicalHistoryTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MedicalHistoryTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMedicalHistory A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT uuid, patient_uuid, user_id, clinical_status, verification_status, severity, note, medical_diagnosis_code_id, onset_date, onset_age, abatement_date, abatement_age, created_at, updated_at FROM medical_history WHERE uuid = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMedicalHistory $obj */
            $obj = new ChildMedicalHistory();
            $obj->hydrate($row);
            MedicalHistoryTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildMedicalHistory|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(MedicalHistoryTableMap::COL_UUID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(MedicalHistoryTableMap::COL_UUID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * $query->filterByUuid(['foo', 'bar']); // WHERE uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $uuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_UUID, $uuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the patient_uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByPatientUuid('fooValue');   // WHERE patient_uuid = 'fooValue'
     * $query->filterByPatientUuid('%fooValue%', Criteria::LIKE); // WHERE patient_uuid LIKE '%fooValue%'
     * $query->filterByPatientUuid(['foo', 'bar']); // WHERE patient_uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $patientUuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPatientUuid($patientUuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($patientUuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_PATIENT_UUID, $patientUuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUserId($userId = null, ?string $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_USER_ID, $userId, $comparison);

        return $this;
    }

    /**
     * Filter the query on the clinical_status column
     *
     * Example usage:
     * <code>
     * $query->filterByClinicalStatus(1234); // WHERE clinical_status = 1234
     * $query->filterByClinicalStatus(array(12, 34)); // WHERE clinical_status IN (12, 34)
     * $query->filterByClinicalStatus(array('min' => 12)); // WHERE clinical_status > 12
     * </code>
     *
     * @param mixed $clinicalStatus The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByClinicalStatus($clinicalStatus = null, ?string $comparison = null)
    {
        if (is_array($clinicalStatus)) {
            $useMinMax = false;
            if (isset($clinicalStatus['min'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_CLINICAL_STATUS, $clinicalStatus['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clinicalStatus['max'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_CLINICAL_STATUS, $clinicalStatus['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_CLINICAL_STATUS, $clinicalStatus, $comparison);

        return $this;
    }

    /**
     * Filter the query on the verification_status column
     *
     * Example usage:
     * <code>
     * $query->filterByVerificationStatus(1234); // WHERE verification_status = 1234
     * $query->filterByVerificationStatus(array(12, 34)); // WHERE verification_status IN (12, 34)
     * $query->filterByVerificationStatus(array('min' => 12)); // WHERE verification_status > 12
     * </code>
     *
     * @param mixed $verificationStatus The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVerificationStatus($verificationStatus = null, ?string $comparison = null)
    {
        if (is_array($verificationStatus)) {
            $useMinMax = false;
            if (isset($verificationStatus['min'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_VERIFICATION_STATUS, $verificationStatus['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($verificationStatus['max'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_VERIFICATION_STATUS, $verificationStatus['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_VERIFICATION_STATUS, $verificationStatus, $comparison);

        return $this;
    }

    /**
     * Filter the query on the severity column
     *
     * Example usage:
     * <code>
     * $query->filterBySeverity(1234); // WHERE severity = 1234
     * $query->filterBySeverity(array(12, 34)); // WHERE severity IN (12, 34)
     * $query->filterBySeverity(array('min' => 12)); // WHERE severity > 12
     * </code>
     *
     * @param mixed $severity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterBySeverity($severity = null, ?string $comparison = null)
    {
        if (is_array($severity)) {
            $useMinMax = false;
            if (isset($severity['min'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_SEVERITY, $severity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($severity['max'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_SEVERITY, $severity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_SEVERITY, $severity, $comparison);

        return $this;
    }

    /**
     * Filter the query on the note column
     *
     * Example usage:
     * <code>
     * $query->filterByNote('fooValue');   // WHERE note = 'fooValue'
     * $query->filterByNote('%fooValue%', Criteria::LIKE); // WHERE note LIKE '%fooValue%'
     * $query->filterByNote(['foo', 'bar']); // WHERE note IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $note The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByNote($note = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($note)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_NOTE, $note, $comparison);

        return $this;
    }

    /**
     * Filter the query on the medical_diagnosis_code_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMedicalDiagnosisCodeId(1234); // WHERE medical_diagnosis_code_id = 1234
     * $query->filterByMedicalDiagnosisCodeId(array(12, 34)); // WHERE medical_diagnosis_code_id IN (12, 34)
     * $query->filterByMedicalDiagnosisCodeId(array('min' => 12)); // WHERE medical_diagnosis_code_id > 12
     * </code>
     *
     * @see       filterByMedicalDiagnosisCode()
     *
     * @param mixed $medicalDiagnosisCodeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicalDiagnosisCodeId($medicalDiagnosisCodeId = null, ?string $comparison = null)
    {
        if (is_array($medicalDiagnosisCodeId)) {
            $useMinMax = false;
            if (isset($medicalDiagnosisCodeId['min'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID, $medicalDiagnosisCodeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($medicalDiagnosisCodeId['max'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID, $medicalDiagnosisCodeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID, $medicalDiagnosisCodeId, $comparison);

        return $this;
    }

    /**
     * Filter the query on the onset_date column
     *
     * Example usage:
     * <code>
     * $query->filterByOnsetDate('2011-03-14'); // WHERE onset_date = '2011-03-14'
     * $query->filterByOnsetDate('now'); // WHERE onset_date = '2011-03-14'
     * $query->filterByOnsetDate(array('max' => 'yesterday')); // WHERE onset_date > '2011-03-13'
     * </code>
     *
     * @param mixed $onsetDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByOnsetDate($onsetDate = null, ?string $comparison = null)
    {
        if (is_array($onsetDate)) {
            $useMinMax = false;
            if (isset($onsetDate['min'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_ONSET_DATE, $onsetDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($onsetDate['max'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_ONSET_DATE, $onsetDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_ONSET_DATE, $onsetDate, $comparison);

        return $this;
    }

    /**
     * Filter the query on the onset_age column
     *
     * Example usage:
     * <code>
     * $query->filterByOnsetAge(1234); // WHERE onset_age = 1234
     * $query->filterByOnsetAge(array(12, 34)); // WHERE onset_age IN (12, 34)
     * $query->filterByOnsetAge(array('min' => 12)); // WHERE onset_age > 12
     * </code>
     *
     * @param mixed $onsetAge The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByOnsetAge($onsetAge = null, ?string $comparison = null)
    {
        if (is_array($onsetAge)) {
            $useMinMax = false;
            if (isset($onsetAge['min'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_ONSET_AGE, $onsetAge['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($onsetAge['max'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_ONSET_AGE, $onsetAge['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_ONSET_AGE, $onsetAge, $comparison);

        return $this;
    }

    /**
     * Filter the query on the abatement_date column
     *
     * Example usage:
     * <code>
     * $query->filterByAbatementDate('2011-03-14'); // WHERE abatement_date = '2011-03-14'
     * $query->filterByAbatementDate('now'); // WHERE abatement_date = '2011-03-14'
     * $query->filterByAbatementDate(array('max' => 'yesterday')); // WHERE abatement_date > '2011-03-13'
     * </code>
     *
     * @param mixed $abatementDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByAbatementDate($abatementDate = null, ?string $comparison = null)
    {
        if (is_array($abatementDate)) {
            $useMinMax = false;
            if (isset($abatementDate['min'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_ABATEMENT_DATE, $abatementDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($abatementDate['max'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_ABATEMENT_DATE, $abatementDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_ABATEMENT_DATE, $abatementDate, $comparison);

        return $this;
    }

    /**
     * Filter the query on the abatement_age column
     *
     * Example usage:
     * <code>
     * $query->filterByAbatementAge(1234); // WHERE abatement_age = 1234
     * $query->filterByAbatementAge(array(12, 34)); // WHERE abatement_age IN (12, 34)
     * $query->filterByAbatementAge(array('min' => 12)); // WHERE abatement_age > 12
     * </code>
     *
     * @param mixed $abatementAge The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByAbatementAge($abatementAge = null, ?string $comparison = null)
    {
        if (is_array($abatementAge)) {
            $useMinMax = false;
            if (isset($abatementAge['min'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_ABATEMENT_AGE, $abatementAge['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($abatementAge['max'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_ABATEMENT_AGE, $abatementAge['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_ABATEMENT_AGE, $abatementAge, $comparison);

        return $this;
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, ?string $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_CREATED_AT, $createdAt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, ?string $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(MedicalHistoryTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicalHistoryTableMap::COL_UPDATED_AT, $updatedAt, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \User object
     *
     * @param \User|ObjectCollection $user The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUser($user, ?string $comparison = null)
    {
        if ($user instanceof \User) {
            return $this
                ->addUsingAlias(MedicalHistoryTableMap::COL_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(MedicalHistoryTableMap::COL_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinUser(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\UserQuery');
    }

    /**
     * Use the User relation User object
     *
     * @param callable(\UserQuery):\UserQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUserQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUserQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to User table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \UserQuery The inner query object of the EXISTS statement
     */
    public function useUserExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('User', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to User table for a NOT EXISTS query.
     *
     * @see useUserExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \UserQuery The inner query object of the NOT EXISTS statement
     */
    public function useUserNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('User', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \Patient object
     *
     * @param \Patient|ObjectCollection $patient The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPatient($patient, ?string $comparison = null)
    {
        if ($patient instanceof \Patient) {
            return $this
                ->addUsingAlias(MedicalHistoryTableMap::COL_PATIENT_UUID, $patient->getUuid(), $comparison);
        } elseif ($patient instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(MedicalHistoryTableMap::COL_PATIENT_UUID, $patient->toKeyValue('PrimaryKey', 'Uuid'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByPatient() only accepts arguments of type \Patient or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Patient relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPatient(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Patient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Patient');
        }

        return $this;
    }

    /**
     * Use the Patient relation Patient object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PatientQuery A secondary query class using the current class as primary query
     */
    public function usePatientQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPatient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Patient', '\PatientQuery');
    }

    /**
     * Use the Patient relation Patient object
     *
     * @param callable(\PatientQuery):\PatientQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPatientQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePatientQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Patient table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PatientQuery The inner query object of the EXISTS statement
     */
    public function usePatientExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Patient', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Patient table for a NOT EXISTS query.
     *
     * @see usePatientExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PatientQuery The inner query object of the NOT EXISTS statement
     */
    public function usePatientNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Patient', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \MedicalDiagnosisCode object
     *
     * @param \MedicalDiagnosisCode|ObjectCollection $medicalDiagnosisCode The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicalDiagnosisCode($medicalDiagnosisCode, ?string $comparison = null)
    {
        if ($medicalDiagnosisCode instanceof \MedicalDiagnosisCode) {
            return $this
                ->addUsingAlias(MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID, $medicalDiagnosisCode->getId(), $comparison);
        } elseif ($medicalDiagnosisCode instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID, $medicalDiagnosisCode->toKeyValue('PrimaryKey', 'Id'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByMedicalDiagnosisCode() only accepts arguments of type \MedicalDiagnosisCode or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MedicalDiagnosisCode relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinMedicalDiagnosisCode(?string $relationAlias = null, ?string $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MedicalDiagnosisCode');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MedicalDiagnosisCode');
        }

        return $this;
    }

    /**
     * Use the MedicalDiagnosisCode relation MedicalDiagnosisCode object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MedicalDiagnosisCodeQuery A secondary query class using the current class as primary query
     */
    public function useMedicalDiagnosisCodeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMedicalDiagnosisCode($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MedicalDiagnosisCode', '\MedicalDiagnosisCodeQuery');
    }

    /**
     * Use the MedicalDiagnosisCode relation MedicalDiagnosisCode object
     *
     * @param callable(\MedicalDiagnosisCodeQuery):\MedicalDiagnosisCodeQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withMedicalDiagnosisCodeQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useMedicalDiagnosisCodeQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to MedicalDiagnosisCode table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \MedicalDiagnosisCodeQuery The inner query object of the EXISTS statement
     */
    public function useMedicalDiagnosisCodeExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('MedicalDiagnosisCode', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to MedicalDiagnosisCode table for a NOT EXISTS query.
     *
     * @see useMedicalDiagnosisCodeExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \MedicalDiagnosisCodeQuery The inner query object of the NOT EXISTS statement
     */
    public function useMedicalDiagnosisCodeNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('MedicalDiagnosisCode', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Exclude object from result
     *
     * @param ChildMedicalHistory $medicalHistory Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($medicalHistory = null)
    {
        if ($medicalHistory) {
            $this->addUsingAlias(MedicalHistoryTableMap::COL_UUID, $medicalHistory->getUuid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the medical_history table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicalHistoryTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MedicalHistoryTableMap::clearInstancePool();
            MedicalHistoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicalHistoryTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MedicalHistoryTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MedicalHistoryTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MedicalHistoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param int $nbDays Maximum age of the latest update in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        $this->addUsingAlias(MedicalHistoryTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by update date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        $this->addDescendingOrderByColumn(MedicalHistoryTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by update date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        $this->addAscendingOrderByColumn(MedicalHistoryTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by create date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        $this->addDescendingOrderByColumn(MedicalHistoryTableMap::COL_CREATED_AT);

        return $this;
    }

    /**
     * Filter by the latest created
     *
     * @param int $nbDays Maximum age of in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        $this->addUsingAlias(MedicalHistoryTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by create date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        $this->addAscendingOrderByColumn(MedicalHistoryTableMap::COL_CREATED_AT);

        return $this;
    }

}
