<?php

namespace Base;

use \DewormingHistory as ChildDewormingHistory;
use \DewormingHistoryQuery as ChildDewormingHistoryQuery;
use \MedicalHistory as ChildMedicalHistory;
use \MedicalHistoryQuery as ChildMedicalHistoryQuery;
use \Patient as ChildPatient;
use \PatientQuery as ChildPatientQuery;
use \PhysicalExamination as ChildPhysicalExamination;
use \PhysicalExaminationQuery as ChildPhysicalExaminationQuery;
use \Prescription as ChildPrescription;
use \PrescriptionQuery as ChildPrescriptionQuery;
use \Religion as ChildReligion;
use \ReligionQuery as ChildReligionQuery;
use \Tenant as ChildTenant;
use \TenantQuery as ChildTenantQuery;
use \VaccinationHistory as ChildVaccinationHistory;
use \VaccinationHistoryQuery as ChildVaccinationHistoryQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\DewormingHistoryTableMap;
use Map\MedicalHistoryTableMap;
use Map\PatientTableMap;
use Map\PhysicalExaminationTableMap;
use Map\PrescriptionTableMap;
use Map\VaccinationHistoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'patient' table.
 *
 * Contains patient information
 *
 * @package    propel.generator..Base
 */
abstract class Patient implements ActiveRecordInterface
{
    /**
     * TableMap class name
     *
     * @var string
     */
    public const TABLE_MAP = '\\Map\\PatientTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var bool
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var bool
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = [];

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = [];

    /**
     * The value for the uuid field.
     * Unique ID
     * @var        string
     */
    protected $uuid;

    /**
     * The value for the first_name field.
     * First name
     * @var        string
     */
    protected $first_name;

    /**
     * The value for the name field.
     * Family name
     * @var        string
     */
    protected $name;

    /**
     * The value for the gender field.
     * Gender defined as male, female or divers
     * @var        string
     */
    protected $gender;

    /**
     * The value for the date_of_birth field.
     * Date of birth
     * @var        DateTime
     */
    protected $date_of_birth;

    /**
     * The value for the date_of_death field.
     * Recording if patient died
     * @var        DateTime|null
     */
    protected $date_of_death;

    /**
     * The value for the unclear_birthdate field.
     * Set to 1 if birthdate is not clear and the suspected year should only be displayed
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $unclear_birthdate;

    /**
     * The value for the mobile_number field.
     * Godians phone number
     * @var        string|null
     */
    protected $mobile_number;

    /**
     * The value for the tenant_uuid field.
     * Reference to tenant
     * @var        string
     */
    protected $tenant_uuid;

    /**
     * The value for the religion_id field.
     *
     * @var        int|null
     */
    protected $religion_id;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        ChildTenant
     */
    protected $aTenant;

    /**
     * @var        ChildReligion
     */
    protected $aReligion;

    /**
     * @var        ObjectCollection|ChildVaccinationHistory[] Collection to store aggregation of ChildVaccinationHistory objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildVaccinationHistory> Collection to store aggregation of ChildVaccinationHistory objects.
     */
    protected $collVaccinationHistories;
    protected $collVaccinationHistoriesPartial;

    /**
     * @var        ObjectCollection|ChildPhysicalExamination[] Collection to store aggregation of ChildPhysicalExamination objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildPhysicalExamination> Collection to store aggregation of ChildPhysicalExamination objects.
     */
    protected $collPhysicalExaminations;
    protected $collPhysicalExaminationsPartial;

    /**
     * @var        ObjectCollection|ChildDewormingHistory[] Collection to store aggregation of ChildDewormingHistory objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildDewormingHistory> Collection to store aggregation of ChildDewormingHistory objects.
     */
    protected $collDewormingHistories;
    protected $collDewormingHistoriesPartial;

    /**
     * @var        ObjectCollection|ChildMedicalHistory[] Collection to store aggregation of ChildMedicalHistory objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildMedicalHistory> Collection to store aggregation of ChildMedicalHistory objects.
     */
    protected $collMedicalHistories;
    protected $collMedicalHistoriesPartial;

    /**
     * @var        ObjectCollection|ChildPrescription[] Collection to store aggregation of ChildPrescription objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildPrescription> Collection to store aggregation of ChildPrescription objects.
     */
    protected $collPrescriptions;
    protected $collPrescriptionsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var bool
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildVaccinationHistory[]
     * @phpstan-var ObjectCollection&\Traversable<ChildVaccinationHistory>
     */
    protected $vaccinationHistoriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPhysicalExamination[]
     * @phpstan-var ObjectCollection&\Traversable<ChildPhysicalExamination>
     */
    protected $physicalExaminationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDewormingHistory[]
     * @phpstan-var ObjectCollection&\Traversable<ChildDewormingHistory>
     */
    protected $dewormingHistoriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMedicalHistory[]
     * @phpstan-var ObjectCollection&\Traversable<ChildMedicalHistory>
     */
    protected $medicalHistoriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrescription[]
     * @phpstan-var ObjectCollection&\Traversable<ChildPrescription>
     */
    protected $prescriptionsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues(): void
    {
        $this->unclear_birthdate = false;
    }

    /**
     * Initializes internal state of Base\Patient object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return bool True if the object has been modified.
     */
    public function isModified(): bool
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param string $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return bool True if $col has been modified.
     */
    public function isColumnModified(string $col): bool
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns(): array
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return bool True, if the object has never been persisted.
     */
    public function isNew(): bool
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param bool $b the state of the object.
     */
    public function setNew(bool $b): void
    {
        $this->new = $b;
    }

    /**
     * Whether this object has been deleted.
     * @return bool The deleted state of this object.
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param bool $b The deleted state of this object.
     * @return void
     */
    public function setDeleted(bool $b): void
    {
        $this->deleted = $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified(?string $col = null): void
    {
        if (null !== $col) {
            unset($this->modifiedColumns[$col]);
        } else {
            $this->modifiedColumns = [];
        }
    }

    /**
     * Compares this with another <code>Patient</code> instance.  If
     * <code>obj</code> is an instance of <code>Patient</code>, delegates to
     * <code>equals(Patient)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param mixed $obj The object to compare to.
     * @return bool Whether equal to the object specified.
     */
    public function equals($obj): bool
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns(): array
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return bool
     */
    public function hasVirtualColumn(string $name): bool
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return mixed
     *
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getVirtualColumn(string $name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of nonexistent virtual column `%s`.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @param mixed $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn(string $name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param string $msg
     * @param int $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log(string $msg, int $priority = Propel::LOG_INFO): void
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param \Propel\Runtime\Parser\AbstractParser|string $parser An AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string The exported data
     */
    public function exportTo($parser, bool $includeLazyLoadColumns = true, string $keyType = TableMap::TYPE_PHPNAME): string
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     *
     * @return array<string>
     */
    public function __sleep(): array
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [uuid] column value.
     * Unique ID
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Get the [first_name] column value.
     * First name
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Get the [name] column value.
     * Family name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [gender] column value.
     * Gender defined as male, female or divers
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Get the [optionally formatted] temporal [date_of_birth] column value.
     * Date of birth
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL).
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getDateOfBirth($format = null)
    {
        if ($format === null) {
            return $this->date_of_birth;
        } else {
            return $this->date_of_birth instanceof \DateTimeInterface ? $this->date_of_birth->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [date_of_death] column value.
     * Recording if patient died
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getDateOfDeath($format = null)
    {
        if ($format === null) {
            return $this->date_of_death;
        } else {
            return $this->date_of_death instanceof \DateTimeInterface ? $this->date_of_death->format($format) : null;
        }
    }

    /**
     * Get the [unclear_birthdate] column value.
     * Set to 1 if birthdate is not clear and the suspected year should only be displayed
     * @return boolean|null
     */
    public function getUnclearBirthdate()
    {
        return $this->unclear_birthdate;
    }

    /**
     * Get the [unclear_birthdate] column value.
     * Set to 1 if birthdate is not clear and the suspected year should only be displayed
     * @return boolean|null
     */
    public function isUnclearBirthdate()
    {
        return $this->getUnclearBirthdate();
    }

    /**
     * Get the [mobile_number] column value.
     * Godians phone number
     * @return string|null
     */
    public function getMobileNumber()
    {
        return $this->mobile_number;
    }

    /**
     * Get the [tenant_uuid] column value.
     * Reference to tenant
     * @return string
     */
    public function getTenantUuid()
    {
        return $this->tenant_uuid;
    }

    /**
     * Get the [religion_id] column value.
     *
     * @return int|null
     */
    public function getReligionId()
    {
        return $this->religion_id;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [uuid] column.
     * Unique ID
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uuid !== $v) {
            $this->uuid = $v;
            $this->modifiedColumns[PatientTableMap::COL_UUID] = true;
        }

        return $this;
    }

    /**
     * Set the value of [first_name] column.
     * First name
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setFirstName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->first_name !== $v) {
            $this->first_name = $v;
            $this->modifiedColumns[PatientTableMap::COL_FIRST_NAME] = true;
        }

        return $this;
    }

    /**
     * Set the value of [name] column.
     * Family name
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[PatientTableMap::COL_NAME] = true;
        }

        return $this;
    }

    /**
     * Set the value of [gender] column.
     * Gender defined as male, female or divers
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setGender($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->gender !== $v) {
            $this->gender = $v;
            $this->modifiedColumns[PatientTableMap::COL_GENDER] = true;
        }

        return $this;
    }

    /**
     * Sets the value of [date_of_birth] column to a normalized version of the date/time value specified.
     * Date of birth
     * @param string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setDateOfBirth($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_of_birth !== null || $dt !== null) {
            if ($this->date_of_birth === null || $dt === null || $dt->format("Y-m-d") !== $this->date_of_birth->format("Y-m-d")) {
                $this->date_of_birth = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PatientTableMap::COL_DATE_OF_BIRTH] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Sets the value of [date_of_death] column to a normalized version of the date/time value specified.
     * Recording if patient died
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setDateOfDeath($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_of_death !== null || $dt !== null) {
            if ($this->date_of_death === null || $dt === null || $dt->format("Y-m-d") !== $this->date_of_death->format("Y-m-d")) {
                $this->date_of_death = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PatientTableMap::COL_DATE_OF_DEATH] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Sets the value of the [unclear_birthdate] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * Set to 1 if birthdate is not clear and the suspected year should only be displayed
     * @param bool|integer|string|null $v The new value
     * @return $this The current object (for fluent API support)
     */
    public function setUnclearBirthdate($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->unclear_birthdate !== $v) {
            $this->unclear_birthdate = $v;
            $this->modifiedColumns[PatientTableMap::COL_UNCLEAR_BIRTHDATE] = true;
        }

        return $this;
    }

    /**
     * Set the value of [mobile_number] column.
     * Godians phone number
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setMobileNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mobile_number !== $v) {
            $this->mobile_number = $v;
            $this->modifiedColumns[PatientTableMap::COL_MOBILE_NUMBER] = true;
        }

        return $this;
    }

    /**
     * Set the value of [tenant_uuid] column.
     * Reference to tenant
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setTenantUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tenant_uuid !== $v) {
            $this->tenant_uuid = $v;
            $this->modifiedColumns[PatientTableMap::COL_TENANT_UUID] = true;
        }

        if ($this->aTenant !== null && $this->aTenant->getUuid() !== $v) {
            $this->aTenant = null;
        }

        return $this;
    }

    /**
     * Set the value of [religion_id] column.
     *
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setReligionId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->religion_id !== $v) {
            $this->religion_id = $v;
            $this->modifiedColumns[PatientTableMap::COL_RELIGION_ID] = true;
        }

        if ($this->aReligion !== null && $this->aReligion->getId() !== $v) {
            $this->aReligion = null;
        }

        return $this;
    }

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PatientTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PatientTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return bool Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues(): bool
    {
            if ($this->unclear_birthdate !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    }

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by DataFetcher->fetch().
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param bool $rehydrate Whether this object is being re-hydrated from the database.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int next starting column
     * @throws \Propel\Runtime\Exception\PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate(array $row, int $startcol = 0, bool $rehydrate = false, string $indexType = TableMap::TYPE_NUM): int
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PatientTableMap::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PatientTableMap::translateFieldName('FirstName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->first_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PatientTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PatientTableMap::translateFieldName('Gender', TableMap::TYPE_PHPNAME, $indexType)];
            $this->gender = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PatientTableMap::translateFieldName('DateOfBirth', TableMap::TYPE_PHPNAME, $indexType)];
            $this->date_of_birth = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PatientTableMap::translateFieldName('DateOfDeath', TableMap::TYPE_PHPNAME, $indexType)];
            $this->date_of_death = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PatientTableMap::translateFieldName('UnclearBirthdate', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unclear_birthdate = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PatientTableMap::translateFieldName('MobileNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mobile_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : PatientTableMap::translateFieldName('TenantUuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tenant_uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : PatientTableMap::translateFieldName('ReligionId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->religion_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : PatientTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : PatientTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = PatientTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Patient'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function ensureConsistency(): void
    {
        if ($this->aTenant !== null && $this->tenant_uuid !== $this->aTenant->getUuid()) {
            $this->aTenant = null;
        }
        if ($this->aReligion !== null && $this->religion_id !== $this->aReligion->getId()) {
            $this->aReligion = null;
        }
    }

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param bool $deep (optional) Whether to also de-associated any related objects.
     * @param ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload(bool $deep = false, ?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PatientTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPatientQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aTenant = null;
            $this->aReligion = null;
            $this->collVaccinationHistories = null;

            $this->collPhysicalExaminations = null;

            $this->collDewormingHistories = null;

            $this->collMedicalHistories = null;

            $this->collPrescriptions = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param ConnectionInterface $con
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     * @see Patient::setDeleted()
     * @see Patient::isDeleted()
     */
    public function delete(?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PatientTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPatientQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    public function save(?ConnectionInterface $con = null): int
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PatientTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(PatientTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(PatientTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(PatientTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PatientTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con): int
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aTenant !== null) {
                if ($this->aTenant->isModified() || $this->aTenant->isNew()) {
                    $affectedRows += $this->aTenant->save($con);
                }
                $this->setTenant($this->aTenant);
            }

            if ($this->aReligion !== null) {
                if ($this->aReligion->isModified() || $this->aReligion->isNew()) {
                    $affectedRows += $this->aReligion->save($con);
                }
                $this->setReligion($this->aReligion);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->vaccinationHistoriesScheduledForDeletion !== null) {
                if (!$this->vaccinationHistoriesScheduledForDeletion->isEmpty()) {
                    \VaccinationHistoryQuery::create()
                        ->filterByPrimaryKeys($this->vaccinationHistoriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vaccinationHistoriesScheduledForDeletion = null;
                }
            }

            if ($this->collVaccinationHistories !== null) {
                foreach ($this->collVaccinationHistories as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->physicalExaminationsScheduledForDeletion !== null) {
                if (!$this->physicalExaminationsScheduledForDeletion->isEmpty()) {
                    \PhysicalExaminationQuery::create()
                        ->filterByPrimaryKeys($this->physicalExaminationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->physicalExaminationsScheduledForDeletion = null;
                }
            }

            if ($this->collPhysicalExaminations !== null) {
                foreach ($this->collPhysicalExaminations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->dewormingHistoriesScheduledForDeletion !== null) {
                if (!$this->dewormingHistoriesScheduledForDeletion->isEmpty()) {
                    \DewormingHistoryQuery::create()
                        ->filterByPrimaryKeys($this->dewormingHistoriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dewormingHistoriesScheduledForDeletion = null;
                }
            }

            if ($this->collDewormingHistories !== null) {
                foreach ($this->collDewormingHistories as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->medicalHistoriesScheduledForDeletion !== null) {
                if (!$this->medicalHistoriesScheduledForDeletion->isEmpty()) {
                    \MedicalHistoryQuery::create()
                        ->filterByPrimaryKeys($this->medicalHistoriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->medicalHistoriesScheduledForDeletion = null;
                }
            }

            if ($this->collMedicalHistories !== null) {
                foreach ($this->collMedicalHistories as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prescriptionsScheduledForDeletion !== null) {
                if (!$this->prescriptionsScheduledForDeletion->isEmpty()) {
                    \PrescriptionQuery::create()
                        ->filterByPrimaryKeys($this->prescriptionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prescriptionsScheduledForDeletion = null;
                }
            }

            if ($this->collPrescriptions !== null) {
                foreach ($this->collPrescriptions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    }

    /**
     * Insert the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con): void
    {
        $modifiedColumns = [];
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PatientTableMap::COL_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'uuid';
        }
        if ($this->isColumnModified(PatientTableMap::COL_FIRST_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'first_name';
        }
        if ($this->isColumnModified(PatientTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(PatientTableMap::COL_GENDER)) {
            $modifiedColumns[':p' . $index++]  = 'gender';
        }
        if ($this->isColumnModified(PatientTableMap::COL_DATE_OF_BIRTH)) {
            $modifiedColumns[':p' . $index++]  = 'date_of_birth';
        }
        if ($this->isColumnModified(PatientTableMap::COL_DATE_OF_DEATH)) {
            $modifiedColumns[':p' . $index++]  = 'date_of_death';
        }
        if ($this->isColumnModified(PatientTableMap::COL_UNCLEAR_BIRTHDATE)) {
            $modifiedColumns[':p' . $index++]  = 'unclear_birthdate';
        }
        if ($this->isColumnModified(PatientTableMap::COL_MOBILE_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'mobile_number';
        }
        if ($this->isColumnModified(PatientTableMap::COL_TENANT_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'tenant_uuid';
        }
        if ($this->isColumnModified(PatientTableMap::COL_RELIGION_ID)) {
            $modifiedColumns[':p' . $index++]  = 'religion_id';
        }
        if ($this->isColumnModified(PatientTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(PatientTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO patient (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'uuid':
                        $stmt->bindValue($identifier, $this->uuid, PDO::PARAM_STR);
                        break;
                    case 'first_name':
                        $stmt->bindValue($identifier, $this->first_name, PDO::PARAM_STR);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'gender':
                        $stmt->bindValue($identifier, $this->gender, PDO::PARAM_STR);
                        break;
                    case 'date_of_birth':
                        $stmt->bindValue($identifier, $this->date_of_birth ? $this->date_of_birth->format("Y-m-d") : null, PDO::PARAM_STR);
                        break;
                    case 'date_of_death':
                        $stmt->bindValue($identifier, $this->date_of_death ? $this->date_of_death->format("Y-m-d") : null, PDO::PARAM_STR);
                        break;
                    case 'unclear_birthdate':
                        $stmt->bindValue($identifier, $this->unclear_birthdate, PDO::PARAM_BOOL);
                        break;
                    case 'mobile_number':
                        $stmt->bindValue($identifier, $this->mobile_number, PDO::PARAM_STR);
                        break;
                    case 'tenant_uuid':
                        $stmt->bindValue($identifier, $this->tenant_uuid, PDO::PARAM_STR);
                        break;
                    case 'religion_id':
                        $stmt->bindValue($identifier, $this->religion_id, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @return int Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con): int
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName(string $name, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PatientTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos Position in XML schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition(int $pos)
    {
        switch ($pos) {
            case 0:
                return $this->getUuid();

            case 1:
                return $this->getFirstName();

            case 2:
                return $this->getName();

            case 3:
                return $this->getGender();

            case 4:
                return $this->getDateOfBirth();

            case 5:
                return $this->getDateOfDeath();

            case 6:
                return $this->getUnclearBirthdate();

            case 7:
                return $this->getMobileNumber();

            case 8:
                return $this->getTenantUuid();

            case 9:
                return $this->getReligionId();

            case 10:
                return $this->getCreatedAt();

            case 11:
                return $this->getUpdatedAt();

            default:
                return null;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param bool $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array An associative array containing the field names (as keys) and field values
     */
    public function toArray(string $keyType = TableMap::TYPE_PHPNAME, bool $includeLazyLoadColumns = true, array $alreadyDumpedObjects = [], bool $includeForeignObjects = false): array
    {
        if (isset($alreadyDumpedObjects['Patient'][$this->hashCode()])) {
            return ['*RECURSION*'];
        }
        $alreadyDumpedObjects['Patient'][$this->hashCode()] = true;
        $keys = PatientTableMap::getFieldNames($keyType);
        $result = [
            $keys[0] => $this->getUuid(),
            $keys[1] => $this->getFirstName(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getGender(),
            $keys[4] => $this->getDateOfBirth(),
            $keys[5] => $this->getDateOfDeath(),
            $keys[6] => $this->getUnclearBirthdate(),
            $keys[7] => $this->getMobileNumber(),
            $keys[8] => $this->getTenantUuid(),
            $keys[9] => $this->getReligionId(),
            $keys[10] => $this->getCreatedAt(),
            $keys[11] => $this->getUpdatedAt(),
        ];
        if ($result[$keys[4]] instanceof \DateTimeInterface) {
            $result[$keys[4]] = $result[$keys[4]]->format('Y-m-d');
        }

        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('Y-m-d');
        }

        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('Y-m-d H:i:s.u');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aTenant) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'tenant';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'tenant';
                        break;
                    default:
                        $key = 'Tenant';
                }

                $result[$key] = $this->aTenant->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aReligion) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'religion';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'religion';
                        break;
                    default:
                        $key = 'Religion';
                }

                $result[$key] = $this->aReligion->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collVaccinationHistories) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'vaccinationHistories';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'vaccination_histories';
                        break;
                    default:
                        $key = 'VaccinationHistories';
                }

                $result[$key] = $this->collVaccinationHistories->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPhysicalExaminations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'physicalExaminations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'physical_examinations';
                        break;
                    default:
                        $key = 'PhysicalExaminations';
                }

                $result[$key] = $this->collPhysicalExaminations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDewormingHistories) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dewormingHistories';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'deworming_histories';
                        break;
                    default:
                        $key = 'DewormingHistories';
                }

                $result[$key] = $this->collDewormingHistories->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMedicalHistories) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'medicalHistories';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'medical_histories';
                        break;
                    default:
                        $key = 'MedicalHistories';
                }

                $result[$key] = $this->collMedicalHistories->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrescriptions) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prescriptions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'prescriptions';
                        break;
                    default:
                        $key = 'Prescriptions';
                }

                $result[$key] = $this->collPrescriptions->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this
     */
    public function setByName(string $name, $value, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PatientTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        $this->setByPosition($pos, $value);

        return $this;
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return $this
     */
    public function setByPosition(int $pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setUuid($value);
                break;
            case 1:
                $this->setFirstName($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setGender($value);
                break;
            case 4:
                $this->setDateOfBirth($value);
                break;
            case 5:
                $this->setDateOfDeath($value);
                break;
            case 6:
                $this->setUnclearBirthdate($value);
                break;
            case 7:
                $this->setMobileNumber($value);
                break;
            case 8:
                $this->setTenantUuid($value);
                break;
            case 9:
                $this->setReligionId($value);
                break;
            case 10:
                $this->setCreatedAt($value);
                break;
            case 11:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param array $arr An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return $this
     */
    public function fromArray(array $arr, string $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PatientTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setUuid($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setFirstName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setGender($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDateOfBirth($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDateOfDeath($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUnclearBirthdate($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setMobileNumber($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTenantUuid($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setReligionId($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCreatedAt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUpdatedAt($arr[$keys[11]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this The current object, for fluid interface
     */
    public function importFrom($parser, string $data, string $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria(): Criteria
    {
        $criteria = new Criteria(PatientTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PatientTableMap::COL_UUID)) {
            $criteria->add(PatientTableMap::COL_UUID, $this->uuid);
        }
        if ($this->isColumnModified(PatientTableMap::COL_FIRST_NAME)) {
            $criteria->add(PatientTableMap::COL_FIRST_NAME, $this->first_name);
        }
        if ($this->isColumnModified(PatientTableMap::COL_NAME)) {
            $criteria->add(PatientTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(PatientTableMap::COL_GENDER)) {
            $criteria->add(PatientTableMap::COL_GENDER, $this->gender);
        }
        if ($this->isColumnModified(PatientTableMap::COL_DATE_OF_BIRTH)) {
            $criteria->add(PatientTableMap::COL_DATE_OF_BIRTH, $this->date_of_birth);
        }
        if ($this->isColumnModified(PatientTableMap::COL_DATE_OF_DEATH)) {
            $criteria->add(PatientTableMap::COL_DATE_OF_DEATH, $this->date_of_death);
        }
        if ($this->isColumnModified(PatientTableMap::COL_UNCLEAR_BIRTHDATE)) {
            $criteria->add(PatientTableMap::COL_UNCLEAR_BIRTHDATE, $this->unclear_birthdate);
        }
        if ($this->isColumnModified(PatientTableMap::COL_MOBILE_NUMBER)) {
            $criteria->add(PatientTableMap::COL_MOBILE_NUMBER, $this->mobile_number);
        }
        if ($this->isColumnModified(PatientTableMap::COL_TENANT_UUID)) {
            $criteria->add(PatientTableMap::COL_TENANT_UUID, $this->tenant_uuid);
        }
        if ($this->isColumnModified(PatientTableMap::COL_RELIGION_ID)) {
            $criteria->add(PatientTableMap::COL_RELIGION_ID, $this->religion_id);
        }
        if ($this->isColumnModified(PatientTableMap::COL_CREATED_AT)) {
            $criteria->add(PatientTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(PatientTableMap::COL_UPDATED_AT)) {
            $criteria->add(PatientTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria(): Criteria
    {
        $criteria = ChildPatientQuery::create();
        $criteria->add(PatientTableMap::COL_UUID, $this->uuid);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int|string Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getUuid();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getUuid();
    }

    /**
     * Generic method to set the primary key (uuid column).
     *
     * @param string|null $key Primary key.
     * @return void
     */
    public function setPrimaryKey(?string $key = null): void
    {
        $this->setUuid($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     *
     * @return bool
     */
    public function isPrimaryKeyNull(): bool
    {
        return null === $this->getUuid();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of \Patient (or compatible) type.
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param bool $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function copyInto(object $copyObj, bool $deepCopy = false, bool $makeNew = true): void
    {
        $copyObj->setUuid($this->getUuid());
        $copyObj->setFirstName($this->getFirstName());
        $copyObj->setName($this->getName());
        $copyObj->setGender($this->getGender());
        $copyObj->setDateOfBirth($this->getDateOfBirth());
        $copyObj->setDateOfDeath($this->getDateOfDeath());
        $copyObj->setUnclearBirthdate($this->getUnclearBirthdate());
        $copyObj->setMobileNumber($this->getMobileNumber());
        $copyObj->setTenantUuid($this->getTenantUuid());
        $copyObj->setReligionId($this->getReligionId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getVaccinationHistories() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVaccinationHistory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPhysicalExaminations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPhysicalExamination($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDewormingHistories() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDewormingHistory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMedicalHistories() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMedicalHistory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrescriptions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrescription($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Patient Clone of current object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function copy(bool $deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildTenant object.
     *
     * @param ChildTenant $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setTenant(ChildTenant $v = null)
    {
        if ($v === null) {
            $this->setTenantUuid(NULL);
        } else {
            $this->setTenantUuid($v->getUuid());
        }

        $this->aTenant = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildTenant object, it will not be re-added.
        if ($v !== null) {
            $v->addPatient($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildTenant object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildTenant The associated ChildTenant object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getTenant(?ConnectionInterface $con = null)
    {
        if ($this->aTenant === null && (($this->tenant_uuid !== "" && $this->tenant_uuid !== null))) {
            $this->aTenant = ChildTenantQuery::create()->findPk($this->tenant_uuid, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTenant->addPatients($this);
             */
        }

        return $this->aTenant;
    }

    /**
     * Declares an association between this object and a ChildReligion object.
     *
     * @param ChildReligion|null $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setReligion(ChildReligion $v = null)
    {
        if ($v === null) {
            $this->setReligionId(NULL);
        } else {
            $this->setReligionId($v->getId());
        }

        $this->aReligion = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildReligion object, it will not be re-added.
        if ($v !== null) {
            $v->addPatient($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildReligion object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildReligion|null The associated ChildReligion object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getReligion(?ConnectionInterface $con = null)
    {
        if ($this->aReligion === null && ($this->religion_id != 0)) {
            $this->aReligion = ChildReligionQuery::create()->findPk($this->religion_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aReligion->addPatients($this);
             */
        }

        return $this->aReligion;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName): void
    {
        if ('VaccinationHistory' === $relationName) {
            $this->initVaccinationHistories();
            return;
        }
        if ('PhysicalExamination' === $relationName) {
            $this->initPhysicalExaminations();
            return;
        }
        if ('DewormingHistory' === $relationName) {
            $this->initDewormingHistories();
            return;
        }
        if ('MedicalHistory' === $relationName) {
            $this->initMedicalHistories();
            return;
        }
        if ('Prescription' === $relationName) {
            $this->initPrescriptions();
            return;
        }
    }

    /**
     * Clears out the collVaccinationHistories collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addVaccinationHistories()
     */
    public function clearVaccinationHistories()
    {
        $this->collVaccinationHistories = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collVaccinationHistories collection loaded partially.
     *
     * @return void
     */
    public function resetPartialVaccinationHistories($v = true): void
    {
        $this->collVaccinationHistoriesPartial = $v;
    }

    /**
     * Initializes the collVaccinationHistories collection.
     *
     * By default this just sets the collVaccinationHistories collection to an empty array (like clearcollVaccinationHistories());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVaccinationHistories(bool $overrideExisting = true): void
    {
        if (null !== $this->collVaccinationHistories && !$overrideExisting) {
            return;
        }

        $collectionClassName = VaccinationHistoryTableMap::getTableMap()->getCollectionClassName();

        $this->collVaccinationHistories = new $collectionClassName;
        $this->collVaccinationHistories->setModel('\VaccinationHistory');
    }

    /**
     * Gets an array of ChildVaccinationHistory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPatient is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildVaccinationHistory[] List of ChildVaccinationHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildVaccinationHistory> List of ChildVaccinationHistory objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getVaccinationHistories(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collVaccinationHistoriesPartial && !$this->isNew();
        if (null === $this->collVaccinationHistories || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collVaccinationHistories) {
                    $this->initVaccinationHistories();
                } else {
                    $collectionClassName = VaccinationHistoryTableMap::getTableMap()->getCollectionClassName();

                    $collVaccinationHistories = new $collectionClassName;
                    $collVaccinationHistories->setModel('\VaccinationHistory');

                    return $collVaccinationHistories;
                }
            } else {
                $collVaccinationHistories = ChildVaccinationHistoryQuery::create(null, $criteria)
                    ->filterByPatient($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collVaccinationHistoriesPartial && count($collVaccinationHistories)) {
                        $this->initVaccinationHistories(false);

                        foreach ($collVaccinationHistories as $obj) {
                            if (false == $this->collVaccinationHistories->contains($obj)) {
                                $this->collVaccinationHistories->append($obj);
                            }
                        }

                        $this->collVaccinationHistoriesPartial = true;
                    }

                    return $collVaccinationHistories;
                }

                if ($partial && $this->collVaccinationHistories) {
                    foreach ($this->collVaccinationHistories as $obj) {
                        if ($obj->isNew()) {
                            $collVaccinationHistories[] = $obj;
                        }
                    }
                }

                $this->collVaccinationHistories = $collVaccinationHistories;
                $this->collVaccinationHistoriesPartial = false;
            }
        }

        return $this->collVaccinationHistories;
    }

    /**
     * Sets a collection of ChildVaccinationHistory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $vaccinationHistories A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setVaccinationHistories(Collection $vaccinationHistories, ?ConnectionInterface $con = null)
    {
        /** @var ChildVaccinationHistory[] $vaccinationHistoriesToDelete */
        $vaccinationHistoriesToDelete = $this->getVaccinationHistories(new Criteria(), $con)->diff($vaccinationHistories);


        $this->vaccinationHistoriesScheduledForDeletion = $vaccinationHistoriesToDelete;

        foreach ($vaccinationHistoriesToDelete as $vaccinationHistoryRemoved) {
            $vaccinationHistoryRemoved->setPatient(null);
        }

        $this->collVaccinationHistories = null;
        foreach ($vaccinationHistories as $vaccinationHistory) {
            $this->addVaccinationHistory($vaccinationHistory);
        }

        $this->collVaccinationHistories = $vaccinationHistories;
        $this->collVaccinationHistoriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VaccinationHistory objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related VaccinationHistory objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countVaccinationHistories(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collVaccinationHistoriesPartial && !$this->isNew();
        if (null === $this->collVaccinationHistories || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVaccinationHistories) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getVaccinationHistories());
            }

            $query = ChildVaccinationHistoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPatient($this)
                ->count($con);
        }

        return count($this->collVaccinationHistories);
    }

    /**
     * Method called to associate a ChildVaccinationHistory object to this object
     * through the ChildVaccinationHistory foreign key attribute.
     *
     * @param ChildVaccinationHistory $l ChildVaccinationHistory
     * @return $this The current object (for fluent API support)
     */
    public function addVaccinationHistory(ChildVaccinationHistory $l)
    {
        if ($this->collVaccinationHistories === null) {
            $this->initVaccinationHistories();
            $this->collVaccinationHistoriesPartial = true;
        }

        if (!$this->collVaccinationHistories->contains($l)) {
            $this->doAddVaccinationHistory($l);

            if ($this->vaccinationHistoriesScheduledForDeletion and $this->vaccinationHistoriesScheduledForDeletion->contains($l)) {
                $this->vaccinationHistoriesScheduledForDeletion->remove($this->vaccinationHistoriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildVaccinationHistory $vaccinationHistory The ChildVaccinationHistory object to add.
     */
    protected function doAddVaccinationHistory(ChildVaccinationHistory $vaccinationHistory): void
    {
        $this->collVaccinationHistories[]= $vaccinationHistory;
        $vaccinationHistory->setPatient($this);
    }

    /**
     * @param ChildVaccinationHistory $vaccinationHistory The ChildVaccinationHistory object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeVaccinationHistory(ChildVaccinationHistory $vaccinationHistory)
    {
        if ($this->getVaccinationHistories()->contains($vaccinationHistory)) {
            $pos = $this->collVaccinationHistories->search($vaccinationHistory);
            $this->collVaccinationHistories->remove($pos);
            if (null === $this->vaccinationHistoriesScheduledForDeletion) {
                $this->vaccinationHistoriesScheduledForDeletion = clone $this->collVaccinationHistories;
                $this->vaccinationHistoriesScheduledForDeletion->clear();
            }
            $this->vaccinationHistoriesScheduledForDeletion[]= clone $vaccinationHistory;
            $vaccinationHistory->setPatient(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Patient is new, it will return
     * an empty collection; or if this Patient has previously
     * been saved, it will retrieve related VaccinationHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Patient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildVaccinationHistory[] List of ChildVaccinationHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildVaccinationHistory}> List of ChildVaccinationHistory objects
     */
    public function getVaccinationHistoriesJoinVaccine(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildVaccinationHistoryQuery::create(null, $criteria);
        $query->joinWith('Vaccine', $joinBehavior);

        return $this->getVaccinationHistories($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Patient is new, it will return
     * an empty collection; or if this Patient has previously
     * been saved, it will retrieve related VaccinationHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Patient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildVaccinationHistory[] List of ChildVaccinationHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildVaccinationHistory}> List of ChildVaccinationHistory objects
     */
    public function getVaccinationHistoriesJoinUser(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildVaccinationHistoryQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getVaccinationHistories($query, $con);
    }

    /**
     * Clears out the collPhysicalExaminations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addPhysicalExaminations()
     */
    public function clearPhysicalExaminations()
    {
        $this->collPhysicalExaminations = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collPhysicalExaminations collection loaded partially.
     *
     * @return void
     */
    public function resetPartialPhysicalExaminations($v = true): void
    {
        $this->collPhysicalExaminationsPartial = $v;
    }

    /**
     * Initializes the collPhysicalExaminations collection.
     *
     * By default this just sets the collPhysicalExaminations collection to an empty array (like clearcollPhysicalExaminations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPhysicalExaminations(bool $overrideExisting = true): void
    {
        if (null !== $this->collPhysicalExaminations && !$overrideExisting) {
            return;
        }

        $collectionClassName = PhysicalExaminationTableMap::getTableMap()->getCollectionClassName();

        $this->collPhysicalExaminations = new $collectionClassName;
        $this->collPhysicalExaminations->setModel('\PhysicalExamination');
    }

    /**
     * Gets an array of ChildPhysicalExamination objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPatient is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPhysicalExamination[] List of ChildPhysicalExamination objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPhysicalExamination> List of ChildPhysicalExamination objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getPhysicalExaminations(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collPhysicalExaminationsPartial && !$this->isNew();
        if (null === $this->collPhysicalExaminations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPhysicalExaminations) {
                    $this->initPhysicalExaminations();
                } else {
                    $collectionClassName = PhysicalExaminationTableMap::getTableMap()->getCollectionClassName();

                    $collPhysicalExaminations = new $collectionClassName;
                    $collPhysicalExaminations->setModel('\PhysicalExamination');

                    return $collPhysicalExaminations;
                }
            } else {
                $collPhysicalExaminations = ChildPhysicalExaminationQuery::create(null, $criteria)
                    ->filterByPatient($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPhysicalExaminationsPartial && count($collPhysicalExaminations)) {
                        $this->initPhysicalExaminations(false);

                        foreach ($collPhysicalExaminations as $obj) {
                            if (false == $this->collPhysicalExaminations->contains($obj)) {
                                $this->collPhysicalExaminations->append($obj);
                            }
                        }

                        $this->collPhysicalExaminationsPartial = true;
                    }

                    return $collPhysicalExaminations;
                }

                if ($partial && $this->collPhysicalExaminations) {
                    foreach ($this->collPhysicalExaminations as $obj) {
                        if ($obj->isNew()) {
                            $collPhysicalExaminations[] = $obj;
                        }
                    }
                }

                $this->collPhysicalExaminations = $collPhysicalExaminations;
                $this->collPhysicalExaminationsPartial = false;
            }
        }

        return $this->collPhysicalExaminations;
    }

    /**
     * Sets a collection of ChildPhysicalExamination objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $physicalExaminations A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setPhysicalExaminations(Collection $physicalExaminations, ?ConnectionInterface $con = null)
    {
        /** @var ChildPhysicalExamination[] $physicalExaminationsToDelete */
        $physicalExaminationsToDelete = $this->getPhysicalExaminations(new Criteria(), $con)->diff($physicalExaminations);


        $this->physicalExaminationsScheduledForDeletion = $physicalExaminationsToDelete;

        foreach ($physicalExaminationsToDelete as $physicalExaminationRemoved) {
            $physicalExaminationRemoved->setPatient(null);
        }

        $this->collPhysicalExaminations = null;
        foreach ($physicalExaminations as $physicalExamination) {
            $this->addPhysicalExamination($physicalExamination);
        }

        $this->collPhysicalExaminations = $physicalExaminations;
        $this->collPhysicalExaminationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PhysicalExamination objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related PhysicalExamination objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countPhysicalExaminations(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collPhysicalExaminationsPartial && !$this->isNew();
        if (null === $this->collPhysicalExaminations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPhysicalExaminations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPhysicalExaminations());
            }

            $query = ChildPhysicalExaminationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPatient($this)
                ->count($con);
        }

        return count($this->collPhysicalExaminations);
    }

    /**
     * Method called to associate a ChildPhysicalExamination object to this object
     * through the ChildPhysicalExamination foreign key attribute.
     *
     * @param ChildPhysicalExamination $l ChildPhysicalExamination
     * @return $this The current object (for fluent API support)
     */
    public function addPhysicalExamination(ChildPhysicalExamination $l)
    {
        if ($this->collPhysicalExaminations === null) {
            $this->initPhysicalExaminations();
            $this->collPhysicalExaminationsPartial = true;
        }

        if (!$this->collPhysicalExaminations->contains($l)) {
            $this->doAddPhysicalExamination($l);

            if ($this->physicalExaminationsScheduledForDeletion and $this->physicalExaminationsScheduledForDeletion->contains($l)) {
                $this->physicalExaminationsScheduledForDeletion->remove($this->physicalExaminationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPhysicalExamination $physicalExamination The ChildPhysicalExamination object to add.
     */
    protected function doAddPhysicalExamination(ChildPhysicalExamination $physicalExamination): void
    {
        $this->collPhysicalExaminations[]= $physicalExamination;
        $physicalExamination->setPatient($this);
    }

    /**
     * @param ChildPhysicalExamination $physicalExamination The ChildPhysicalExamination object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removePhysicalExamination(ChildPhysicalExamination $physicalExamination)
    {
        if ($this->getPhysicalExaminations()->contains($physicalExamination)) {
            $pos = $this->collPhysicalExaminations->search($physicalExamination);
            $this->collPhysicalExaminations->remove($pos);
            if (null === $this->physicalExaminationsScheduledForDeletion) {
                $this->physicalExaminationsScheduledForDeletion = clone $this->collPhysicalExaminations;
                $this->physicalExaminationsScheduledForDeletion->clear();
            }
            $this->physicalExaminationsScheduledForDeletion[]= clone $physicalExamination;
            $physicalExamination->setPatient(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Patient is new, it will return
     * an empty collection; or if this Patient has previously
     * been saved, it will retrieve related PhysicalExaminations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Patient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPhysicalExamination[] List of ChildPhysicalExamination objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPhysicalExamination}> List of ChildPhysicalExamination objects
     */
    public function getPhysicalExaminationsJoinUser(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPhysicalExaminationQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getPhysicalExaminations($query, $con);
    }

    /**
     * Clears out the collDewormingHistories collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addDewormingHistories()
     */
    public function clearDewormingHistories()
    {
        $this->collDewormingHistories = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collDewormingHistories collection loaded partially.
     *
     * @return void
     */
    public function resetPartialDewormingHistories($v = true): void
    {
        $this->collDewormingHistoriesPartial = $v;
    }

    /**
     * Initializes the collDewormingHistories collection.
     *
     * By default this just sets the collDewormingHistories collection to an empty array (like clearcollDewormingHistories());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDewormingHistories(bool $overrideExisting = true): void
    {
        if (null !== $this->collDewormingHistories && !$overrideExisting) {
            return;
        }

        $collectionClassName = DewormingHistoryTableMap::getTableMap()->getCollectionClassName();

        $this->collDewormingHistories = new $collectionClassName;
        $this->collDewormingHistories->setModel('\DewormingHistory');
    }

    /**
     * Gets an array of ChildDewormingHistory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPatient is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDewormingHistory[] List of ChildDewormingHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildDewormingHistory> List of ChildDewormingHistory objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getDewormingHistories(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collDewormingHistoriesPartial && !$this->isNew();
        if (null === $this->collDewormingHistories || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collDewormingHistories) {
                    $this->initDewormingHistories();
                } else {
                    $collectionClassName = DewormingHistoryTableMap::getTableMap()->getCollectionClassName();

                    $collDewormingHistories = new $collectionClassName;
                    $collDewormingHistories->setModel('\DewormingHistory');

                    return $collDewormingHistories;
                }
            } else {
                $collDewormingHistories = ChildDewormingHistoryQuery::create(null, $criteria)
                    ->filterByPatient($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDewormingHistoriesPartial && count($collDewormingHistories)) {
                        $this->initDewormingHistories(false);

                        foreach ($collDewormingHistories as $obj) {
                            if (false == $this->collDewormingHistories->contains($obj)) {
                                $this->collDewormingHistories->append($obj);
                            }
                        }

                        $this->collDewormingHistoriesPartial = true;
                    }

                    return $collDewormingHistories;
                }

                if ($partial && $this->collDewormingHistories) {
                    foreach ($this->collDewormingHistories as $obj) {
                        if ($obj->isNew()) {
                            $collDewormingHistories[] = $obj;
                        }
                    }
                }

                $this->collDewormingHistories = $collDewormingHistories;
                $this->collDewormingHistoriesPartial = false;
            }
        }

        return $this->collDewormingHistories;
    }

    /**
     * Sets a collection of ChildDewormingHistory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $dewormingHistories A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setDewormingHistories(Collection $dewormingHistories, ?ConnectionInterface $con = null)
    {
        /** @var ChildDewormingHistory[] $dewormingHistoriesToDelete */
        $dewormingHistoriesToDelete = $this->getDewormingHistories(new Criteria(), $con)->diff($dewormingHistories);


        $this->dewormingHistoriesScheduledForDeletion = $dewormingHistoriesToDelete;

        foreach ($dewormingHistoriesToDelete as $dewormingHistoryRemoved) {
            $dewormingHistoryRemoved->setPatient(null);
        }

        $this->collDewormingHistories = null;
        foreach ($dewormingHistories as $dewormingHistory) {
            $this->addDewormingHistory($dewormingHistory);
        }

        $this->collDewormingHistories = $dewormingHistories;
        $this->collDewormingHistoriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DewormingHistory objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related DewormingHistory objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countDewormingHistories(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collDewormingHistoriesPartial && !$this->isNew();
        if (null === $this->collDewormingHistories || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDewormingHistories) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDewormingHistories());
            }

            $query = ChildDewormingHistoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPatient($this)
                ->count($con);
        }

        return count($this->collDewormingHistories);
    }

    /**
     * Method called to associate a ChildDewormingHistory object to this object
     * through the ChildDewormingHistory foreign key attribute.
     *
     * @param ChildDewormingHistory $l ChildDewormingHistory
     * @return $this The current object (for fluent API support)
     */
    public function addDewormingHistory(ChildDewormingHistory $l)
    {
        if ($this->collDewormingHistories === null) {
            $this->initDewormingHistories();
            $this->collDewormingHistoriesPartial = true;
        }

        if (!$this->collDewormingHistories->contains($l)) {
            $this->doAddDewormingHistory($l);

            if ($this->dewormingHistoriesScheduledForDeletion and $this->dewormingHistoriesScheduledForDeletion->contains($l)) {
                $this->dewormingHistoriesScheduledForDeletion->remove($this->dewormingHistoriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDewormingHistory $dewormingHistory The ChildDewormingHistory object to add.
     */
    protected function doAddDewormingHistory(ChildDewormingHistory $dewormingHistory): void
    {
        $this->collDewormingHistories[]= $dewormingHistory;
        $dewormingHistory->setPatient($this);
    }

    /**
     * @param ChildDewormingHistory $dewormingHistory The ChildDewormingHistory object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeDewormingHistory(ChildDewormingHistory $dewormingHistory)
    {
        if ($this->getDewormingHistories()->contains($dewormingHistory)) {
            $pos = $this->collDewormingHistories->search($dewormingHistory);
            $this->collDewormingHistories->remove($pos);
            if (null === $this->dewormingHistoriesScheduledForDeletion) {
                $this->dewormingHistoriesScheduledForDeletion = clone $this->collDewormingHistories;
                $this->dewormingHistoriesScheduledForDeletion->clear();
            }
            $this->dewormingHistoriesScheduledForDeletion[]= clone $dewormingHistory;
            $dewormingHistory->setPatient(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Patient is new, it will return
     * an empty collection; or if this Patient has previously
     * been saved, it will retrieve related DewormingHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Patient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDewormingHistory[] List of ChildDewormingHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildDewormingHistory}> List of ChildDewormingHistory objects
     */
    public function getDewormingHistoriesJoinUser(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDewormingHistoryQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getDewormingHistories($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Patient is new, it will return
     * an empty collection; or if this Patient has previously
     * been saved, it will retrieve related DewormingHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Patient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDewormingHistory[] List of ChildDewormingHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildDewormingHistory}> List of ChildDewormingHistory objects
     */
    public function getDewormingHistoriesJoinMedication(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDewormingHistoryQuery::create(null, $criteria);
        $query->joinWith('Medication', $joinBehavior);

        return $this->getDewormingHistories($query, $con);
    }

    /**
     * Clears out the collMedicalHistories collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addMedicalHistories()
     */
    public function clearMedicalHistories()
    {
        $this->collMedicalHistories = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collMedicalHistories collection loaded partially.
     *
     * @return void
     */
    public function resetPartialMedicalHistories($v = true): void
    {
        $this->collMedicalHistoriesPartial = $v;
    }

    /**
     * Initializes the collMedicalHistories collection.
     *
     * By default this just sets the collMedicalHistories collection to an empty array (like clearcollMedicalHistories());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMedicalHistories(bool $overrideExisting = true): void
    {
        if (null !== $this->collMedicalHistories && !$overrideExisting) {
            return;
        }

        $collectionClassName = MedicalHistoryTableMap::getTableMap()->getCollectionClassName();

        $this->collMedicalHistories = new $collectionClassName;
        $this->collMedicalHistories->setModel('\MedicalHistory');
    }

    /**
     * Gets an array of ChildMedicalHistory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPatient is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMedicalHistory[] List of ChildMedicalHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildMedicalHistory> List of ChildMedicalHistory objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getMedicalHistories(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collMedicalHistoriesPartial && !$this->isNew();
        if (null === $this->collMedicalHistories || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collMedicalHistories) {
                    $this->initMedicalHistories();
                } else {
                    $collectionClassName = MedicalHistoryTableMap::getTableMap()->getCollectionClassName();

                    $collMedicalHistories = new $collectionClassName;
                    $collMedicalHistories->setModel('\MedicalHistory');

                    return $collMedicalHistories;
                }
            } else {
                $collMedicalHistories = ChildMedicalHistoryQuery::create(null, $criteria)
                    ->filterByPatient($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMedicalHistoriesPartial && count($collMedicalHistories)) {
                        $this->initMedicalHistories(false);

                        foreach ($collMedicalHistories as $obj) {
                            if (false == $this->collMedicalHistories->contains($obj)) {
                                $this->collMedicalHistories->append($obj);
                            }
                        }

                        $this->collMedicalHistoriesPartial = true;
                    }

                    return $collMedicalHistories;
                }

                if ($partial && $this->collMedicalHistories) {
                    foreach ($this->collMedicalHistories as $obj) {
                        if ($obj->isNew()) {
                            $collMedicalHistories[] = $obj;
                        }
                    }
                }

                $this->collMedicalHistories = $collMedicalHistories;
                $this->collMedicalHistoriesPartial = false;
            }
        }

        return $this->collMedicalHistories;
    }

    /**
     * Sets a collection of ChildMedicalHistory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $medicalHistories A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setMedicalHistories(Collection $medicalHistories, ?ConnectionInterface $con = null)
    {
        /** @var ChildMedicalHistory[] $medicalHistoriesToDelete */
        $medicalHistoriesToDelete = $this->getMedicalHistories(new Criteria(), $con)->diff($medicalHistories);


        $this->medicalHistoriesScheduledForDeletion = $medicalHistoriesToDelete;

        foreach ($medicalHistoriesToDelete as $medicalHistoryRemoved) {
            $medicalHistoryRemoved->setPatient(null);
        }

        $this->collMedicalHistories = null;
        foreach ($medicalHistories as $medicalHistory) {
            $this->addMedicalHistory($medicalHistory);
        }

        $this->collMedicalHistories = $medicalHistories;
        $this->collMedicalHistoriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MedicalHistory objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related MedicalHistory objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countMedicalHistories(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collMedicalHistoriesPartial && !$this->isNew();
        if (null === $this->collMedicalHistories || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMedicalHistories) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMedicalHistories());
            }

            $query = ChildMedicalHistoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPatient($this)
                ->count($con);
        }

        return count($this->collMedicalHistories);
    }

    /**
     * Method called to associate a ChildMedicalHistory object to this object
     * through the ChildMedicalHistory foreign key attribute.
     *
     * @param ChildMedicalHistory $l ChildMedicalHistory
     * @return $this The current object (for fluent API support)
     */
    public function addMedicalHistory(ChildMedicalHistory $l)
    {
        if ($this->collMedicalHistories === null) {
            $this->initMedicalHistories();
            $this->collMedicalHistoriesPartial = true;
        }

        if (!$this->collMedicalHistories->contains($l)) {
            $this->doAddMedicalHistory($l);

            if ($this->medicalHistoriesScheduledForDeletion and $this->medicalHistoriesScheduledForDeletion->contains($l)) {
                $this->medicalHistoriesScheduledForDeletion->remove($this->medicalHistoriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMedicalHistory $medicalHistory The ChildMedicalHistory object to add.
     */
    protected function doAddMedicalHistory(ChildMedicalHistory $medicalHistory): void
    {
        $this->collMedicalHistories[]= $medicalHistory;
        $medicalHistory->setPatient($this);
    }

    /**
     * @param ChildMedicalHistory $medicalHistory The ChildMedicalHistory object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeMedicalHistory(ChildMedicalHistory $medicalHistory)
    {
        if ($this->getMedicalHistories()->contains($medicalHistory)) {
            $pos = $this->collMedicalHistories->search($medicalHistory);
            $this->collMedicalHistories->remove($pos);
            if (null === $this->medicalHistoriesScheduledForDeletion) {
                $this->medicalHistoriesScheduledForDeletion = clone $this->collMedicalHistories;
                $this->medicalHistoriesScheduledForDeletion->clear();
            }
            $this->medicalHistoriesScheduledForDeletion[]= clone $medicalHistory;
            $medicalHistory->setPatient(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Patient is new, it will return
     * an empty collection; or if this Patient has previously
     * been saved, it will retrieve related MedicalHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Patient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMedicalHistory[] List of ChildMedicalHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildMedicalHistory}> List of ChildMedicalHistory objects
     */
    public function getMedicalHistoriesJoinUser(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMedicalHistoryQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getMedicalHistories($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Patient is new, it will return
     * an empty collection; or if this Patient has previously
     * been saved, it will retrieve related MedicalHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Patient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMedicalHistory[] List of ChildMedicalHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildMedicalHistory}> List of ChildMedicalHistory objects
     */
    public function getMedicalHistoriesJoinMedicalDiagnosisCode(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMedicalHistoryQuery::create(null, $criteria);
        $query->joinWith('MedicalDiagnosisCode', $joinBehavior);

        return $this->getMedicalHistories($query, $con);
    }

    /**
     * Clears out the collPrescriptions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addPrescriptions()
     */
    public function clearPrescriptions()
    {
        $this->collPrescriptions = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collPrescriptions collection loaded partially.
     *
     * @return void
     */
    public function resetPartialPrescriptions($v = true): void
    {
        $this->collPrescriptionsPartial = $v;
    }

    /**
     * Initializes the collPrescriptions collection.
     *
     * By default this just sets the collPrescriptions collection to an empty array (like clearcollPrescriptions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrescriptions(bool $overrideExisting = true): void
    {
        if (null !== $this->collPrescriptions && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrescriptionTableMap::getTableMap()->getCollectionClassName();

        $this->collPrescriptions = new $collectionClassName;
        $this->collPrescriptions->setModel('\Prescription');
    }

    /**
     * Gets an array of ChildPrescription objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPatient is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription> List of ChildPrescription objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getPrescriptions(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collPrescriptionsPartial && !$this->isNew();
        if (null === $this->collPrescriptions || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPrescriptions) {
                    $this->initPrescriptions();
                } else {
                    $collectionClassName = PrescriptionTableMap::getTableMap()->getCollectionClassName();

                    $collPrescriptions = new $collectionClassName;
                    $collPrescriptions->setModel('\Prescription');

                    return $collPrescriptions;
                }
            } else {
                $collPrescriptions = ChildPrescriptionQuery::create(null, $criteria)
                    ->filterByPatient($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrescriptionsPartial && count($collPrescriptions)) {
                        $this->initPrescriptions(false);

                        foreach ($collPrescriptions as $obj) {
                            if (false == $this->collPrescriptions->contains($obj)) {
                                $this->collPrescriptions->append($obj);
                            }
                        }

                        $this->collPrescriptionsPartial = true;
                    }

                    return $collPrescriptions;
                }

                if ($partial && $this->collPrescriptions) {
                    foreach ($this->collPrescriptions as $obj) {
                        if ($obj->isNew()) {
                            $collPrescriptions[] = $obj;
                        }
                    }
                }

                $this->collPrescriptions = $collPrescriptions;
                $this->collPrescriptionsPartial = false;
            }
        }

        return $this->collPrescriptions;
    }

    /**
     * Sets a collection of ChildPrescription objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $prescriptions A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setPrescriptions(Collection $prescriptions, ?ConnectionInterface $con = null)
    {
        /** @var ChildPrescription[] $prescriptionsToDelete */
        $prescriptionsToDelete = $this->getPrescriptions(new Criteria(), $con)->diff($prescriptions);


        $this->prescriptionsScheduledForDeletion = $prescriptionsToDelete;

        foreach ($prescriptionsToDelete as $prescriptionRemoved) {
            $prescriptionRemoved->setPatient(null);
        }

        $this->collPrescriptions = null;
        foreach ($prescriptions as $prescription) {
            $this->addPrescription($prescription);
        }

        $this->collPrescriptions = $prescriptions;
        $this->collPrescriptionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prescription objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related Prescription objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countPrescriptions(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collPrescriptionsPartial && !$this->isNew();
        if (null === $this->collPrescriptions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrescriptions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrescriptions());
            }

            $query = ChildPrescriptionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPatient($this)
                ->count($con);
        }

        return count($this->collPrescriptions);
    }

    /**
     * Method called to associate a ChildPrescription object to this object
     * through the ChildPrescription foreign key attribute.
     *
     * @param ChildPrescription $l ChildPrescription
     * @return $this The current object (for fluent API support)
     */
    public function addPrescription(ChildPrescription $l)
    {
        if ($this->collPrescriptions === null) {
            $this->initPrescriptions();
            $this->collPrescriptionsPartial = true;
        }

        if (!$this->collPrescriptions->contains($l)) {
            $this->doAddPrescription($l);

            if ($this->prescriptionsScheduledForDeletion and $this->prescriptionsScheduledForDeletion->contains($l)) {
                $this->prescriptionsScheduledForDeletion->remove($this->prescriptionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrescription $prescription The ChildPrescription object to add.
     */
    protected function doAddPrescription(ChildPrescription $prescription): void
    {
        $this->collPrescriptions[]= $prescription;
        $prescription->setPatient($this);
    }

    /**
     * @param ChildPrescription $prescription The ChildPrescription object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removePrescription(ChildPrescription $prescription)
    {
        if ($this->getPrescriptions()->contains($prescription)) {
            $pos = $this->collPrescriptions->search($prescription);
            $this->collPrescriptions->remove($pos);
            if (null === $this->prescriptionsScheduledForDeletion) {
                $this->prescriptionsScheduledForDeletion = clone $this->collPrescriptions;
                $this->prescriptionsScheduledForDeletion->clear();
            }
            $this->prescriptionsScheduledForDeletion[]= clone $prescription;
            $prescription->setPatient(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Patient is new, it will return
     * an empty collection; or if this Patient has previously
     * been saved, it will retrieve related Prescriptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Patient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription}> List of ChildPrescription objects
     */
    public function getPrescriptionsJoinMedicationAdministrationMethod(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrescriptionQuery::create(null, $criteria);
        $query->joinWith('MedicationAdministrationMethod', $joinBehavior);

        return $this->getPrescriptions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Patient is new, it will return
     * an empty collection; or if this Patient has previously
     * been saved, it will retrieve related Prescriptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Patient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription}> List of ChildPrescription objects
     */
    public function getPrescriptionsJoinUser(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrescriptionQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getPrescriptions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Patient is new, it will return
     * an empty collection; or if this Patient has previously
     * been saved, it will retrieve related Prescriptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Patient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription}> List of ChildPrescription objects
     */
    public function getPrescriptionsJoinMedication(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrescriptionQuery::create(null, $criteria);
        $query->joinWith('Medication', $joinBehavior);

        return $this->getPrescriptions($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     *
     * @return $this
     */
    public function clear()
    {
        if (null !== $this->aTenant) {
            $this->aTenant->removePatient($this);
        }
        if (null !== $this->aReligion) {
            $this->aReligion->removePatient($this);
        }
        $this->uuid = null;
        $this->first_name = null;
        $this->name = null;
        $this->gender = null;
        $this->date_of_birth = null;
        $this->date_of_death = null;
        $this->unclear_birthdate = null;
        $this->mobile_number = null;
        $this->tenant_uuid = null;
        $this->religion_id = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);

        return $this;
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param bool $deep Whether to also clear the references on all referrer objects.
     * @return $this
     */
    public function clearAllReferences(bool $deep = false)
    {
        if ($deep) {
            if ($this->collVaccinationHistories) {
                foreach ($this->collVaccinationHistories as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPhysicalExaminations) {
                foreach ($this->collPhysicalExaminations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDewormingHistories) {
                foreach ($this->collDewormingHistories as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMedicalHistories) {
                foreach ($this->collMedicalHistories as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrescriptions) {
                foreach ($this->collPrescriptions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collVaccinationHistories = null;
        $this->collPhysicalExaminations = null;
        $this->collDewormingHistories = null;
        $this->collMedicalHistories = null;
        $this->collPrescriptions = null;
        $this->aTenant = null;
        $this->aReligion = null;
        return $this;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PatientTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return $this The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[PatientTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preSave(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postSave(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before inserting to database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preInsert(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postInsert(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preUpdate(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postUpdate(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preDelete(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postDelete(?ConnectionInterface $con = null): void
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
