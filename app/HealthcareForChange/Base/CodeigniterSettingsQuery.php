<?php

namespace Base;

use \CodeigniterSettings as ChildCodeigniterSettings;
use \CodeigniterSettingsQuery as ChildCodeigniterSettingsQuery;
use \Exception;
use \PDO;
use Map\CodeigniterSettingsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'settings' table.
 *
 * CodeIgniter Settings Table
 *
 * @method     ChildCodeigniterSettingsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCodeigniterSettingsQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildCodeigniterSettingsQuery orderByKey($order = Criteria::ASC) Order by the key column
 * @method     ChildCodeigniterSettingsQuery orderByValue($order = Criteria::ASC) Order by the value column
 * @method     ChildCodeigniterSettingsQuery orderByContext($order = Criteria::ASC) Order by the context column
 * @method     ChildCodeigniterSettingsQuery orderByClass($order = Criteria::ASC) Order by the class column
 * @method     ChildCodeigniterSettingsQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCodeigniterSettingsQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCodeigniterSettingsQuery groupById() Group by the id column
 * @method     ChildCodeigniterSettingsQuery groupByType() Group by the type column
 * @method     ChildCodeigniterSettingsQuery groupByKey() Group by the key column
 * @method     ChildCodeigniterSettingsQuery groupByValue() Group by the value column
 * @method     ChildCodeigniterSettingsQuery groupByContext() Group by the context column
 * @method     ChildCodeigniterSettingsQuery groupByClass() Group by the class column
 * @method     ChildCodeigniterSettingsQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCodeigniterSettingsQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCodeigniterSettingsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCodeigniterSettingsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCodeigniterSettingsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCodeigniterSettingsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCodeigniterSettingsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCodeigniterSettingsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCodeigniterSettings|null findOne(?ConnectionInterface $con = null) Return the first ChildCodeigniterSettings matching the query
 * @method     ChildCodeigniterSettings findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildCodeigniterSettings matching the query, or a new ChildCodeigniterSettings object populated from the query conditions when no match is found
 *
 * @method     ChildCodeigniterSettings|null findOneById(int $id) Return the first ChildCodeigniterSettings filtered by the id column
 * @method     ChildCodeigniterSettings|null findOneByType(string $type) Return the first ChildCodeigniterSettings filtered by the type column
 * @method     ChildCodeigniterSettings|null findOneByKey(string $key) Return the first ChildCodeigniterSettings filtered by the key column
 * @method     ChildCodeigniterSettings|null findOneByValue(string $value) Return the first ChildCodeigniterSettings filtered by the value column
 * @method     ChildCodeigniterSettings|null findOneByContext(string $context) Return the first ChildCodeigniterSettings filtered by the context column
 * @method     ChildCodeigniterSettings|null findOneByClass(string $class) Return the first ChildCodeigniterSettings filtered by the class column
 * @method     ChildCodeigniterSettings|null findOneByCreatedAt(string $created_at) Return the first ChildCodeigniterSettings filtered by the created_at column
 * @method     ChildCodeigniterSettings|null findOneByUpdatedAt(string $updated_at) Return the first ChildCodeigniterSettings filtered by the updated_at column *

 * @method     ChildCodeigniterSettings requirePk($key, ?ConnectionInterface $con = null) Return the ChildCodeigniterSettings by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodeigniterSettings requireOne(?ConnectionInterface $con = null) Return the first ChildCodeigniterSettings matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCodeigniterSettings requireOneById(int $id) Return the first ChildCodeigniterSettings filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodeigniterSettings requireOneByType(string $type) Return the first ChildCodeigniterSettings filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodeigniterSettings requireOneByKey(string $key) Return the first ChildCodeigniterSettings filtered by the key column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodeigniterSettings requireOneByValue(string $value) Return the first ChildCodeigniterSettings filtered by the value column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodeigniterSettings requireOneByContext(string $context) Return the first ChildCodeigniterSettings filtered by the context column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodeigniterSettings requireOneByClass(string $class) Return the first ChildCodeigniterSettings filtered by the class column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodeigniterSettings requireOneByCreatedAt(string $created_at) Return the first ChildCodeigniterSettings filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCodeigniterSettings requireOneByUpdatedAt(string $updated_at) Return the first ChildCodeigniterSettings filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCodeigniterSettings[]|Collection find(?ConnectionInterface $con = null) Return ChildCodeigniterSettings objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildCodeigniterSettings> find(?ConnectionInterface $con = null) Return ChildCodeigniterSettings objects based on current ModelCriteria
 * @method     ChildCodeigniterSettings[]|Collection findById(int $id) Return ChildCodeigniterSettings objects filtered by the id column
 * @psalm-method Collection&\Traversable<ChildCodeigniterSettings> findById(int $id) Return ChildCodeigniterSettings objects filtered by the id column
 * @method     ChildCodeigniterSettings[]|Collection findByType(string $type) Return ChildCodeigniterSettings objects filtered by the type column
 * @psalm-method Collection&\Traversable<ChildCodeigniterSettings> findByType(string $type) Return ChildCodeigniterSettings objects filtered by the type column
 * @method     ChildCodeigniterSettings[]|Collection findByKey(string $key) Return ChildCodeigniterSettings objects filtered by the key column
 * @psalm-method Collection&\Traversable<ChildCodeigniterSettings> findByKey(string $key) Return ChildCodeigniterSettings objects filtered by the key column
 * @method     ChildCodeigniterSettings[]|Collection findByValue(string $value) Return ChildCodeigniterSettings objects filtered by the value column
 * @psalm-method Collection&\Traversable<ChildCodeigniterSettings> findByValue(string $value) Return ChildCodeigniterSettings objects filtered by the value column
 * @method     ChildCodeigniterSettings[]|Collection findByContext(string $context) Return ChildCodeigniterSettings objects filtered by the context column
 * @psalm-method Collection&\Traversable<ChildCodeigniterSettings> findByContext(string $context) Return ChildCodeigniterSettings objects filtered by the context column
 * @method     ChildCodeigniterSettings[]|Collection findByClass(string $class) Return ChildCodeigniterSettings objects filtered by the class column
 * @psalm-method Collection&\Traversable<ChildCodeigniterSettings> findByClass(string $class) Return ChildCodeigniterSettings objects filtered by the class column
 * @method     ChildCodeigniterSettings[]|Collection findByCreatedAt(string $created_at) Return ChildCodeigniterSettings objects filtered by the created_at column
 * @psalm-method Collection&\Traversable<ChildCodeigniterSettings> findByCreatedAt(string $created_at) Return ChildCodeigniterSettings objects filtered by the created_at column
 * @method     ChildCodeigniterSettings[]|Collection findByUpdatedAt(string $updated_at) Return ChildCodeigniterSettings objects filtered by the updated_at column
 * @psalm-method Collection&\Traversable<ChildCodeigniterSettings> findByUpdatedAt(string $updated_at) Return ChildCodeigniterSettings objects filtered by the updated_at column
 * @method     ChildCodeigniterSettings[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildCodeigniterSettings> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CodeigniterSettingsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CodeigniterSettingsQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\CodeigniterSettings', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCodeigniterSettingsQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCodeigniterSettingsQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildCodeigniterSettingsQuery) {
            return $criteria;
        }
        $query = new ChildCodeigniterSettingsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCodeigniterSettings|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CodeigniterSettingsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CodeigniterSettingsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCodeigniterSettings A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, type, key, value, context, class, created_at, updated_at FROM settings WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCodeigniterSettings $obj */
            $obj = new ChildCodeigniterSettings();
            $obj->hydrate($row);
            CodeigniterSettingsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildCodeigniterSettings|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(CodeigniterSettingsTableMap::COL_ID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(CodeigniterSettingsTableMap::COL_ID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterById($id = null, ?string $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CodeigniterSettingsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CodeigniterSettingsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(CodeigniterSettingsTableMap::COL_ID, $id, $comparison);

        return $this;
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * $query->filterByType(['foo', 'bar']); // WHERE type IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $type The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByType($type = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(CodeigniterSettingsTableMap::COL_TYPE, $type, $comparison);

        return $this;
    }

    /**
     * Filter the query on the key column
     *
     * Example usage:
     * <code>
     * $query->filterByKey('fooValue');   // WHERE key = 'fooValue'
     * $query->filterByKey('%fooValue%', Criteria::LIKE); // WHERE key LIKE '%fooValue%'
     * $query->filterByKey(['foo', 'bar']); // WHERE key IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $key The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByKey($key = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($key)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(CodeigniterSettingsTableMap::COL_KEY, $key, $comparison);

        return $this;
    }

    /**
     * Filter the query on the value column
     *
     * Example usage:
     * <code>
     * $query->filterByValue('fooValue');   // WHERE value = 'fooValue'
     * $query->filterByValue('%fooValue%', Criteria::LIKE); // WHERE value LIKE '%fooValue%'
     * $query->filterByValue(['foo', 'bar']); // WHERE value IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $value The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByValue($value = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($value)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(CodeigniterSettingsTableMap::COL_VALUE, $value, $comparison);

        return $this;
    }

    /**
     * Filter the query on the context column
     *
     * Example usage:
     * <code>
     * $query->filterByContext('fooValue');   // WHERE context = 'fooValue'
     * $query->filterByContext('%fooValue%', Criteria::LIKE); // WHERE context LIKE '%fooValue%'
     * $query->filterByContext(['foo', 'bar']); // WHERE context IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $context The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByContext($context = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($context)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(CodeigniterSettingsTableMap::COL_CONTEXT, $context, $comparison);

        return $this;
    }

    /**
     * Filter the query on the class column
     *
     * Example usage:
     * <code>
     * $query->filterByClass('fooValue');   // WHERE class = 'fooValue'
     * $query->filterByClass('%fooValue%', Criteria::LIKE); // WHERE class LIKE '%fooValue%'
     * $query->filterByClass(['foo', 'bar']); // WHERE class IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $class The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByClass($class = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($class)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(CodeigniterSettingsTableMap::COL_CLASS, $class, $comparison);

        return $this;
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, ?string $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CodeigniterSettingsTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CodeigniterSettingsTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(CodeigniterSettingsTableMap::COL_CREATED_AT, $createdAt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, ?string $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CodeigniterSettingsTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CodeigniterSettingsTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(CodeigniterSettingsTableMap::COL_UPDATED_AT, $updatedAt, $comparison);

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param ChildCodeigniterSettings $codeigniterSettings Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($codeigniterSettings = null)
    {
        if ($codeigniterSettings) {
            $this->addUsingAlias(CodeigniterSettingsTableMap::COL_ID, $codeigniterSettings->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the settings table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CodeigniterSettingsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CodeigniterSettingsTableMap::clearInstancePool();
            CodeigniterSettingsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CodeigniterSettingsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CodeigniterSettingsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CodeigniterSettingsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CodeigniterSettingsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param int $nbDays Maximum age of the latest update in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        $this->addUsingAlias(CodeigniterSettingsTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by update date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        $this->addDescendingOrderByColumn(CodeigniterSettingsTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by update date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        $this->addAscendingOrderByColumn(CodeigniterSettingsTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by create date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        $this->addDescendingOrderByColumn(CodeigniterSettingsTableMap::COL_CREATED_AT);

        return $this;
    }

    /**
     * Filter by the latest created
     *
     * @param int $nbDays Maximum age of in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        $this->addUsingAlias(CodeigniterSettingsTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by create date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        $this->addAscendingOrderByColumn(CodeigniterSettingsTableMap::COL_CREATED_AT);

        return $this;
    }

}
