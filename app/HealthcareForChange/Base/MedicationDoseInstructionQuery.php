<?php

namespace Base;

use \MedicationDoseInstruction as ChildMedicationDoseInstruction;
use \MedicationDoseInstructionQuery as ChildMedicationDoseInstructionQuery;
use \Exception;
use \PDO;
use Map\MedicationDoseInstructionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'medication_dose_instruction' table.
 *
 *
 *
 * @method     ChildMedicationDoseInstructionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildMedicationDoseInstructionQuery orderByDescription($order = Criteria::ASC) Order by the description column
 *
 * @method     ChildMedicationDoseInstructionQuery groupById() Group by the id column
 * @method     ChildMedicationDoseInstructionQuery groupByDescription() Group by the description column
 *
 * @method     ChildMedicationDoseInstructionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMedicationDoseInstructionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMedicationDoseInstructionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMedicationDoseInstructionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMedicationDoseInstructionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMedicationDoseInstructionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMedicationDoseInstructionQuery leftJoinPrescriptionDoseInstruction($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrescriptionDoseInstruction relation
 * @method     ChildMedicationDoseInstructionQuery rightJoinPrescriptionDoseInstruction($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrescriptionDoseInstruction relation
 * @method     ChildMedicationDoseInstructionQuery innerJoinPrescriptionDoseInstruction($relationAlias = null) Adds a INNER JOIN clause to the query using the PrescriptionDoseInstruction relation
 *
 * @method     ChildMedicationDoseInstructionQuery joinWithPrescriptionDoseInstruction($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PrescriptionDoseInstruction relation
 *
 * @method     ChildMedicationDoseInstructionQuery leftJoinWithPrescriptionDoseInstruction() Adds a LEFT JOIN clause and with to the query using the PrescriptionDoseInstruction relation
 * @method     ChildMedicationDoseInstructionQuery rightJoinWithPrescriptionDoseInstruction() Adds a RIGHT JOIN clause and with to the query using the PrescriptionDoseInstruction relation
 * @method     ChildMedicationDoseInstructionQuery innerJoinWithPrescriptionDoseInstruction() Adds a INNER JOIN clause and with to the query using the PrescriptionDoseInstruction relation
 *
 * @method     \PrescriptionDoseInstructionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMedicationDoseInstruction|null findOne(?ConnectionInterface $con = null) Return the first ChildMedicationDoseInstruction matching the query
 * @method     ChildMedicationDoseInstruction findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildMedicationDoseInstruction matching the query, or a new ChildMedicationDoseInstruction object populated from the query conditions when no match is found
 *
 * @method     ChildMedicationDoseInstruction|null findOneById(string $id) Return the first ChildMedicationDoseInstruction filtered by the id column
 * @method     ChildMedicationDoseInstruction|null findOneByDescription(string $description) Return the first ChildMedicationDoseInstruction filtered by the description column *

 * @method     ChildMedicationDoseInstruction requirePk($key, ?ConnectionInterface $con = null) Return the ChildMedicationDoseInstruction by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicationDoseInstruction requireOne(?ConnectionInterface $con = null) Return the first ChildMedicationDoseInstruction matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMedicationDoseInstruction requireOneById(string $id) Return the first ChildMedicationDoseInstruction filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedicationDoseInstruction requireOneByDescription(string $description) Return the first ChildMedicationDoseInstruction filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMedicationDoseInstruction[]|Collection find(?ConnectionInterface $con = null) Return ChildMedicationDoseInstruction objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildMedicationDoseInstruction> find(?ConnectionInterface $con = null) Return ChildMedicationDoseInstruction objects based on current ModelCriteria
 * @method     ChildMedicationDoseInstruction[]|Collection findById(string $id) Return ChildMedicationDoseInstruction objects filtered by the id column
 * @psalm-method Collection&\Traversable<ChildMedicationDoseInstruction> findById(string $id) Return ChildMedicationDoseInstruction objects filtered by the id column
 * @method     ChildMedicationDoseInstruction[]|Collection findByDescription(string $description) Return ChildMedicationDoseInstruction objects filtered by the description column
 * @psalm-method Collection&\Traversable<ChildMedicationDoseInstruction> findByDescription(string $description) Return ChildMedicationDoseInstruction objects filtered by the description column
 * @method     ChildMedicationDoseInstruction[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildMedicationDoseInstruction> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MedicationDoseInstructionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MedicationDoseInstructionQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\MedicationDoseInstruction', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMedicationDoseInstructionQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMedicationDoseInstructionQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildMedicationDoseInstructionQuery) {
            return $criteria;
        }
        $query = new ChildMedicationDoseInstructionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMedicationDoseInstruction|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MedicationDoseInstructionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MedicationDoseInstructionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMedicationDoseInstruction A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, description FROM medication_dose_instruction WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMedicationDoseInstruction $obj */
            $obj = new ChildMedicationDoseInstruction();
            $obj->hydrate($row);
            MedicationDoseInstructionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildMedicationDoseInstruction|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(MedicationDoseInstructionTableMap::COL_ID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(MedicationDoseInstructionTableMap::COL_ID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterById($id = null, ?string $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MedicationDoseInstructionTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MedicationDoseInstructionTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicationDoseInstructionTableMap::COL_ID, $id, $comparison);

        return $this;
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * $query->filterByDescription(['foo', 'bar']); // WHERE description IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $description The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDescription($description = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicationDoseInstructionTableMap::COL_DESCRIPTION, $description, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \PrescriptionDoseInstruction object
     *
     * @param \PrescriptionDoseInstruction|ObjectCollection $prescriptionDoseInstruction the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrescriptionDoseInstruction($prescriptionDoseInstruction, ?string $comparison = null)
    {
        if ($prescriptionDoseInstruction instanceof \PrescriptionDoseInstruction) {
            $this
                ->addUsingAlias(MedicationDoseInstructionTableMap::COL_ID, $prescriptionDoseInstruction->getMedicationDoseInstructionId(), $comparison);

            return $this;
        } elseif ($prescriptionDoseInstruction instanceof ObjectCollection) {
            $this
                ->usePrescriptionDoseInstructionQuery()
                ->filterByPrimaryKeys($prescriptionDoseInstruction->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByPrescriptionDoseInstruction() only accepts arguments of type \PrescriptionDoseInstruction or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrescriptionDoseInstruction relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPrescriptionDoseInstruction(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrescriptionDoseInstruction');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrescriptionDoseInstruction');
        }

        return $this;
    }

    /**
     * Use the PrescriptionDoseInstruction relation PrescriptionDoseInstruction object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrescriptionDoseInstructionQuery A secondary query class using the current class as primary query
     */
    public function usePrescriptionDoseInstructionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrescriptionDoseInstruction($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrescriptionDoseInstruction', '\PrescriptionDoseInstructionQuery');
    }

    /**
     * Use the PrescriptionDoseInstruction relation PrescriptionDoseInstruction object
     *
     * @param callable(\PrescriptionDoseInstructionQuery):\PrescriptionDoseInstructionQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPrescriptionDoseInstructionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePrescriptionDoseInstructionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to PrescriptionDoseInstruction table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PrescriptionDoseInstructionQuery The inner query object of the EXISTS statement
     */
    public function usePrescriptionDoseInstructionExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('PrescriptionDoseInstruction', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to PrescriptionDoseInstruction table for a NOT EXISTS query.
     *
     * @see usePrescriptionDoseInstructionExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PrescriptionDoseInstructionQuery The inner query object of the NOT EXISTS statement
     */
    public function usePrescriptionDoseInstructionNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('PrescriptionDoseInstruction', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related Prescription object
     * using the prescription_dose_instruction table as cross reference
     *
     * @param Prescription $prescription the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrescription($prescription, string $comparison = Criteria::EQUAL)
    {
        $this
            ->usePrescriptionDoseInstructionQuery()
            ->filterByPrescription($prescription, $comparison)
            ->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param ChildMedicationDoseInstruction $medicationDoseInstruction Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($medicationDoseInstruction = null)
    {
        if ($medicationDoseInstruction) {
            $this->addUsingAlias(MedicationDoseInstructionTableMap::COL_ID, $medicationDoseInstruction->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the medication_dose_instruction table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicationDoseInstructionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MedicationDoseInstructionTableMap::clearInstancePool();
            MedicationDoseInstructionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicationDoseInstructionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MedicationDoseInstructionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MedicationDoseInstructionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MedicationDoseInstructionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

}
