<?php

namespace Base;

use \MedicationDoseInstruction as ChildMedicationDoseInstruction;
use \MedicationDoseInstructionQuery as ChildMedicationDoseInstructionQuery;
use \Prescription as ChildPrescription;
use \PrescriptionDoseInstruction as ChildPrescriptionDoseInstruction;
use \PrescriptionDoseInstructionQuery as ChildPrescriptionDoseInstructionQuery;
use \PrescriptionQuery as ChildPrescriptionQuery;
use \Exception;
use \PDO;
use Map\MedicationDoseInstructionTableMap;
use Map\PrescriptionDoseInstructionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'medication_dose_instruction' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class MedicationDoseInstruction implements ActiveRecordInterface
{
    /**
     * TableMap class name
     *
     * @var string
     */
    public const TABLE_MAP = '\\Map\\MedicationDoseInstructionTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var bool
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var bool
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = [];

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = [];

    /**
     * The value for the id field.
     *
     * @var        string
     */
    protected $id;

    /**
     * The value for the description field.
     * SNOMED code as of http://www.hl7.org/fhir/valueset-additional-instruction-codes.html
     * @var        string
     */
    protected $description;

    /**
     * @var        ObjectCollection|ChildPrescriptionDoseInstruction[] Collection to store aggregation of ChildPrescriptionDoseInstruction objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildPrescriptionDoseInstruction> Collection to store aggregation of ChildPrescriptionDoseInstruction objects.
     */
    protected $collPrescriptionDoseInstructions;
    protected $collPrescriptionDoseInstructionsPartial;

    /**
     * @var        ObjectCollection|ChildPrescription[] Cross Collection to store aggregation of ChildPrescription objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildPrescription> Cross Collection to store aggregation of ChildPrescription objects.
     */
    protected $collPrescriptions;

    /**
     * @var bool
     */
    protected $collPrescriptionsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var bool
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrescription[]
     * @phpstan-var ObjectCollection&\Traversable<ChildPrescription>
     */
    protected $prescriptionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrescriptionDoseInstruction[]
     * @phpstan-var ObjectCollection&\Traversable<ChildPrescriptionDoseInstruction>
     */
    protected $prescriptionDoseInstructionsScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\MedicationDoseInstruction object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return bool True if the object has been modified.
     */
    public function isModified(): bool
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param string $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return bool True if $col has been modified.
     */
    public function isColumnModified(string $col): bool
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns(): array
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return bool True, if the object has never been persisted.
     */
    public function isNew(): bool
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param bool $b the state of the object.
     */
    public function setNew(bool $b): void
    {
        $this->new = $b;
    }

    /**
     * Whether this object has been deleted.
     * @return bool The deleted state of this object.
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param bool $b The deleted state of this object.
     * @return void
     */
    public function setDeleted(bool $b): void
    {
        $this->deleted = $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified(?string $col = null): void
    {
        if (null !== $col) {
            unset($this->modifiedColumns[$col]);
        } else {
            $this->modifiedColumns = [];
        }
    }

    /**
     * Compares this with another <code>MedicationDoseInstruction</code> instance.  If
     * <code>obj</code> is an instance of <code>MedicationDoseInstruction</code>, delegates to
     * <code>equals(MedicationDoseInstruction)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param mixed $obj The object to compare to.
     * @return bool Whether equal to the object specified.
     */
    public function equals($obj): bool
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns(): array
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return bool
     */
    public function hasVirtualColumn(string $name): bool
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return mixed
     *
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getVirtualColumn(string $name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of nonexistent virtual column `%s`.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @param mixed $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn(string $name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param string $msg
     * @param int $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log(string $msg, int $priority = Propel::LOG_INFO): void
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param \Propel\Runtime\Parser\AbstractParser|string $parser An AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string The exported data
     */
    public function exportTo($parser, bool $includeLazyLoadColumns = true, string $keyType = TableMap::TYPE_PHPNAME): string
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     *
     * @return array<string>
     */
    public function __sleep(): array
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [description] column value.
     * SNOMED code as of http://www.hl7.org/fhir/valueset-additional-instruction-codes.html
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of [id] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[MedicationDoseInstructionTableMap::COL_ID] = true;
        }

        return $this;
    }

    /**
     * Set the value of [description] column.
     * SNOMED code as of http://www.hl7.org/fhir/valueset-additional-instruction-codes.html
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[MedicationDoseInstructionTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    }

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return bool Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues(): bool
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    }

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by DataFetcher->fetch().
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param bool $rehydrate Whether this object is being re-hydrated from the database.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int next starting column
     * @throws \Propel\Runtime\Exception\PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate(array $row, int $startcol = 0, bool $rehydrate = false, string $indexType = TableMap::TYPE_NUM): int
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : MedicationDoseInstructionTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : MedicationDoseInstructionTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 2; // 2 = MedicationDoseInstructionTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\MedicationDoseInstruction'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function ensureConsistency(): void
    {
    }

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param bool $deep (optional) Whether to also de-associated any related objects.
     * @param ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload(bool $deep = false, ?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MedicationDoseInstructionTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildMedicationDoseInstructionQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collPrescriptionDoseInstructions = null;

            $this->collPrescriptions = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param ConnectionInterface $con
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     * @see MedicationDoseInstruction::setDeleted()
     * @see MedicationDoseInstruction::isDeleted()
     */
    public function delete(?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicationDoseInstructionTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildMedicationDoseInstructionQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    public function save(?ConnectionInterface $con = null): int
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicationDoseInstructionTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MedicationDoseInstructionTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con): int
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->prescriptionsScheduledForDeletion !== null) {
                if (!$this->prescriptionsScheduledForDeletion->isEmpty()) {
                    $pks = [];
                    foreach ($this->prescriptionsScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[0] = $this->getId();
                        $entryPk[1] = $entry->getUuid();
                        $pks[] = $entryPk;
                    }

                    \PrescriptionDoseInstructionQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->prescriptionsScheduledForDeletion = null;
                }

            }

            if ($this->collPrescriptions) {
                foreach ($this->collPrescriptions as $prescription) {
                    if (!$prescription->isDeleted() && ($prescription->isNew() || $prescription->isModified())) {
                        $prescription->save($con);
                    }
                }
            }


            if ($this->prescriptionDoseInstructionsScheduledForDeletion !== null) {
                if (!$this->prescriptionDoseInstructionsScheduledForDeletion->isEmpty()) {
                    \PrescriptionDoseInstructionQuery::create()
                        ->filterByPrimaryKeys($this->prescriptionDoseInstructionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prescriptionDoseInstructionsScheduledForDeletion = null;
                }
            }

            if ($this->collPrescriptionDoseInstructions !== null) {
                foreach ($this->collPrescriptionDoseInstructions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    }

    /**
     * Insert the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con): void
    {
        $modifiedColumns = [];
        $index = 0;

        $this->modifiedColumns[MedicationDoseInstructionTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . MedicationDoseInstructionTableMap::COL_ID . ')');
        }
        if (null === $this->id) {
            try {
                $dataFetcher = $con->query("SELECT nextval('medication_dose_instruction_id_seq')");
                $this->id = (string) $dataFetcher->fetchColumn();
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', 0, $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(MedicationDoseInstructionTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(MedicationDoseInstructionTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }

        $sql = sprintf(
            'INSERT INTO medication_dose_instruction (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @return int Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con): int
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName(string $name, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MedicationDoseInstructionTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos Position in XML schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition(int $pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();

            case 1:
                return $this->getDescription();

            default:
                return null;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param bool $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array An associative array containing the field names (as keys) and field values
     */
    public function toArray(string $keyType = TableMap::TYPE_PHPNAME, bool $includeLazyLoadColumns = true, array $alreadyDumpedObjects = [], bool $includeForeignObjects = false): array
    {
        if (isset($alreadyDumpedObjects['MedicationDoseInstruction'][$this->hashCode()])) {
            return ['*RECURSION*'];
        }
        $alreadyDumpedObjects['MedicationDoseInstruction'][$this->hashCode()] = true;
        $keys = MedicationDoseInstructionTableMap::getFieldNames($keyType);
        $result = [
            $keys[0] => $this->getId(),
            $keys[1] => $this->getDescription(),
        ];
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collPrescriptionDoseInstructions) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prescriptionDoseInstructions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'prescription_dose_instructions';
                        break;
                    default:
                        $key = 'PrescriptionDoseInstructions';
                }

                $result[$key] = $this->collPrescriptionDoseInstructions->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this
     */
    public function setByName(string $name, $value, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MedicationDoseInstructionTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        $this->setByPosition($pos, $value);

        return $this;
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return $this
     */
    public function setByPosition(int $pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setDescription($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param array $arr An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return $this
     */
    public function fromArray(array $arr, string $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = MedicationDoseInstructionTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setDescription($arr[$keys[1]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this The current object, for fluid interface
     */
    public function importFrom($parser, string $data, string $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria(): Criteria
    {
        $criteria = new Criteria(MedicationDoseInstructionTableMap::DATABASE_NAME);

        if ($this->isColumnModified(MedicationDoseInstructionTableMap::COL_ID)) {
            $criteria->add(MedicationDoseInstructionTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(MedicationDoseInstructionTableMap::COL_DESCRIPTION)) {
            $criteria->add(MedicationDoseInstructionTableMap::COL_DESCRIPTION, $this->description);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria(): Criteria
    {
        $criteria = ChildMedicationDoseInstructionQuery::create();
        $criteria->add(MedicationDoseInstructionTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int|string Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param string|null $key Primary key.
     * @return void
     */
    public function setPrimaryKey(?string $key = null): void
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     *
     * @return bool
     */
    public function isPrimaryKeyNull(): bool
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of \MedicationDoseInstruction (or compatible) type.
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param bool $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function copyInto(object $copyObj, bool $deepCopy = false, bool $makeNew = true): void
    {
        $copyObj->setDescription($this->getDescription());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getPrescriptionDoseInstructions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrescriptionDoseInstruction($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \MedicationDoseInstruction Clone of current object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function copy(bool $deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName): void
    {
        if ('PrescriptionDoseInstruction' === $relationName) {
            $this->initPrescriptionDoseInstructions();
            return;
        }
    }

    /**
     * Clears out the collPrescriptionDoseInstructions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addPrescriptionDoseInstructions()
     */
    public function clearPrescriptionDoseInstructions()
    {
        $this->collPrescriptionDoseInstructions = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collPrescriptionDoseInstructions collection loaded partially.
     *
     * @return void
     */
    public function resetPartialPrescriptionDoseInstructions($v = true): void
    {
        $this->collPrescriptionDoseInstructionsPartial = $v;
    }

    /**
     * Initializes the collPrescriptionDoseInstructions collection.
     *
     * By default this just sets the collPrescriptionDoseInstructions collection to an empty array (like clearcollPrescriptionDoseInstructions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrescriptionDoseInstructions(bool $overrideExisting = true): void
    {
        if (null !== $this->collPrescriptionDoseInstructions && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrescriptionDoseInstructionTableMap::getTableMap()->getCollectionClassName();

        $this->collPrescriptionDoseInstructions = new $collectionClassName;
        $this->collPrescriptionDoseInstructions->setModel('\PrescriptionDoseInstruction');
    }

    /**
     * Gets an array of ChildPrescriptionDoseInstruction objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMedicationDoseInstruction is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrescriptionDoseInstruction[] List of ChildPrescriptionDoseInstruction objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescriptionDoseInstruction> List of ChildPrescriptionDoseInstruction objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getPrescriptionDoseInstructions(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collPrescriptionDoseInstructionsPartial && !$this->isNew();
        if (null === $this->collPrescriptionDoseInstructions || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPrescriptionDoseInstructions) {
                    $this->initPrescriptionDoseInstructions();
                } else {
                    $collectionClassName = PrescriptionDoseInstructionTableMap::getTableMap()->getCollectionClassName();

                    $collPrescriptionDoseInstructions = new $collectionClassName;
                    $collPrescriptionDoseInstructions->setModel('\PrescriptionDoseInstruction');

                    return $collPrescriptionDoseInstructions;
                }
            } else {
                $collPrescriptionDoseInstructions = ChildPrescriptionDoseInstructionQuery::create(null, $criteria)
                    ->filterByMedicationDoseInstruction($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrescriptionDoseInstructionsPartial && count($collPrescriptionDoseInstructions)) {
                        $this->initPrescriptionDoseInstructions(false);

                        foreach ($collPrescriptionDoseInstructions as $obj) {
                            if (false == $this->collPrescriptionDoseInstructions->contains($obj)) {
                                $this->collPrescriptionDoseInstructions->append($obj);
                            }
                        }

                        $this->collPrescriptionDoseInstructionsPartial = true;
                    }

                    return $collPrescriptionDoseInstructions;
                }

                if ($partial && $this->collPrescriptionDoseInstructions) {
                    foreach ($this->collPrescriptionDoseInstructions as $obj) {
                        if ($obj->isNew()) {
                            $collPrescriptionDoseInstructions[] = $obj;
                        }
                    }
                }

                $this->collPrescriptionDoseInstructions = $collPrescriptionDoseInstructions;
                $this->collPrescriptionDoseInstructionsPartial = false;
            }
        }

        return $this->collPrescriptionDoseInstructions;
    }

    /**
     * Sets a collection of ChildPrescriptionDoseInstruction objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $prescriptionDoseInstructions A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setPrescriptionDoseInstructions(Collection $prescriptionDoseInstructions, ?ConnectionInterface $con = null)
    {
        /** @var ChildPrescriptionDoseInstruction[] $prescriptionDoseInstructionsToDelete */
        $prescriptionDoseInstructionsToDelete = $this->getPrescriptionDoseInstructions(new Criteria(), $con)->diff($prescriptionDoseInstructions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->prescriptionDoseInstructionsScheduledForDeletion = clone $prescriptionDoseInstructionsToDelete;

        foreach ($prescriptionDoseInstructionsToDelete as $prescriptionDoseInstructionRemoved) {
            $prescriptionDoseInstructionRemoved->setMedicationDoseInstruction(null);
        }

        $this->collPrescriptionDoseInstructions = null;
        foreach ($prescriptionDoseInstructions as $prescriptionDoseInstruction) {
            $this->addPrescriptionDoseInstruction($prescriptionDoseInstruction);
        }

        $this->collPrescriptionDoseInstructions = $prescriptionDoseInstructions;
        $this->collPrescriptionDoseInstructionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PrescriptionDoseInstruction objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related PrescriptionDoseInstruction objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countPrescriptionDoseInstructions(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collPrescriptionDoseInstructionsPartial && !$this->isNew();
        if (null === $this->collPrescriptionDoseInstructions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrescriptionDoseInstructions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrescriptionDoseInstructions());
            }

            $query = ChildPrescriptionDoseInstructionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMedicationDoseInstruction($this)
                ->count($con);
        }

        return count($this->collPrescriptionDoseInstructions);
    }

    /**
     * Method called to associate a ChildPrescriptionDoseInstruction object to this object
     * through the ChildPrescriptionDoseInstruction foreign key attribute.
     *
     * @param ChildPrescriptionDoseInstruction $l ChildPrescriptionDoseInstruction
     * @return $this The current object (for fluent API support)
     */
    public function addPrescriptionDoseInstruction(ChildPrescriptionDoseInstruction $l)
    {
        if ($this->collPrescriptionDoseInstructions === null) {
            $this->initPrescriptionDoseInstructions();
            $this->collPrescriptionDoseInstructionsPartial = true;
        }

        if (!$this->collPrescriptionDoseInstructions->contains($l)) {
            $this->doAddPrescriptionDoseInstruction($l);

            if ($this->prescriptionDoseInstructionsScheduledForDeletion and $this->prescriptionDoseInstructionsScheduledForDeletion->contains($l)) {
                $this->prescriptionDoseInstructionsScheduledForDeletion->remove($this->prescriptionDoseInstructionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrescriptionDoseInstruction $prescriptionDoseInstruction The ChildPrescriptionDoseInstruction object to add.
     */
    protected function doAddPrescriptionDoseInstruction(ChildPrescriptionDoseInstruction $prescriptionDoseInstruction): void
    {
        $this->collPrescriptionDoseInstructions[]= $prescriptionDoseInstruction;
        $prescriptionDoseInstruction->setMedicationDoseInstruction($this);
    }

    /**
     * @param ChildPrescriptionDoseInstruction $prescriptionDoseInstruction The ChildPrescriptionDoseInstruction object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removePrescriptionDoseInstruction(ChildPrescriptionDoseInstruction $prescriptionDoseInstruction)
    {
        if ($this->getPrescriptionDoseInstructions()->contains($prescriptionDoseInstruction)) {
            $pos = $this->collPrescriptionDoseInstructions->search($prescriptionDoseInstruction);
            $this->collPrescriptionDoseInstructions->remove($pos);
            if (null === $this->prescriptionDoseInstructionsScheduledForDeletion) {
                $this->prescriptionDoseInstructionsScheduledForDeletion = clone $this->collPrescriptionDoseInstructions;
                $this->prescriptionDoseInstructionsScheduledForDeletion->clear();
            }
            $this->prescriptionDoseInstructionsScheduledForDeletion[]= clone $prescriptionDoseInstruction;
            $prescriptionDoseInstruction->setMedicationDoseInstruction(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MedicationDoseInstruction is new, it will return
     * an empty collection; or if this MedicationDoseInstruction has previously
     * been saved, it will retrieve related PrescriptionDoseInstructions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MedicationDoseInstruction.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrescriptionDoseInstruction[] List of ChildPrescriptionDoseInstruction objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescriptionDoseInstruction}> List of ChildPrescriptionDoseInstruction objects
     */
    public function getPrescriptionDoseInstructionsJoinPrescription(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrescriptionDoseInstructionQuery::create(null, $criteria);
        $query->joinWith('Prescription', $joinBehavior);

        return $this->getPrescriptionDoseInstructions($query, $con);
    }

    /**
     * Clears out the collPrescriptions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPrescriptions()
     */
    public function clearPrescriptions()
    {
        $this->collPrescriptions = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collPrescriptions crossRef collection.
     *
     * By default this just sets the collPrescriptions collection to an empty collection (like clearPrescriptions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initPrescriptions()
    {
        $collectionClassName = PrescriptionDoseInstructionTableMap::getTableMap()->getCollectionClassName();

        $this->collPrescriptions = new $collectionClassName;
        $this->collPrescriptionsPartial = true;
        $this->collPrescriptions->setModel('\Prescription');
    }

    /**
     * Checks if the collPrescriptions collection is loaded.
     *
     * @return bool
     */
    public function isPrescriptionsLoaded(): bool
    {
        return null !== $this->collPrescriptions;
    }

    /**
     * Gets a collection of ChildPrescription objects related by a many-to-many relationship
     * to the current object by way of the prescription_dose_instruction cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMedicationDoseInstruction is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription> List of ChildPrescription objects
     */
    public function getPrescriptions(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collPrescriptionsPartial && !$this->isNew();
        if (null === $this->collPrescriptions || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPrescriptions) {
                    $this->initPrescriptions();
                }
            } else {

                $query = ChildPrescriptionQuery::create(null, $criteria)
                    ->filterByMedicationDoseInstruction($this);
                $collPrescriptions = $query->find($con);
                if (null !== $criteria) {
                    return $collPrescriptions;
                }

                if ($partial && $this->collPrescriptions) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collPrescriptions as $obj) {
                        if (!$collPrescriptions->contains($obj)) {
                            $collPrescriptions[] = $obj;
                        }
                    }
                }

                $this->collPrescriptions = $collPrescriptions;
                $this->collPrescriptionsPartial = false;
            }
        }

        return $this->collPrescriptions;
    }

    /**
     * Sets a collection of Prescription objects related by a many-to-many relationship
     * to the current object by way of the prescription_dose_instruction cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $prescriptions A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setPrescriptions(Collection $prescriptions, ?ConnectionInterface $con = null)
    {
        $this->clearPrescriptions();
        $currentPrescriptions = $this->getPrescriptions();

        $prescriptionsScheduledForDeletion = $currentPrescriptions->diff($prescriptions);

        foreach ($prescriptionsScheduledForDeletion as $toDelete) {
            $this->removePrescription($toDelete);
        }

        foreach ($prescriptions as $prescription) {
            if (!$currentPrescriptions->contains($prescription)) {
                $this->doAddPrescription($prescription);
            }
        }

        $this->collPrescriptionsPartial = false;
        $this->collPrescriptions = $prescriptions;

        return $this;
    }

    /**
     * Gets the number of Prescription objects related by a many-to-many relationship
     * to the current object by way of the prescription_dose_instruction cross-reference table.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param bool $distinct Set to true to force count distinct
     * @param ConnectionInterface $con Optional connection object
     *
     * @return int The number of related Prescription objects
     */
    public function countPrescriptions(?Criteria $criteria = null, $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collPrescriptionsPartial && !$this->isNew();
        if (null === $this->collPrescriptions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrescriptions) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getPrescriptions());
                }

                $query = ChildPrescriptionQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByMedicationDoseInstruction($this)
                    ->count($con);
            }
        } else {
            return count($this->collPrescriptions);
        }
    }

    /**
     * Associate a ChildPrescription to this object
     * through the prescription_dose_instruction cross reference table.
     *
     * @param ChildPrescription $prescription
     * @return ChildMedicationDoseInstruction The current object (for fluent API support)
     */
    public function addPrescription(ChildPrescription $prescription)
    {
        if ($this->collPrescriptions === null) {
            $this->initPrescriptions();
        }

        if (!$this->getPrescriptions()->contains($prescription)) {
            // only add it if the **same** object is not already associated
            $this->collPrescriptions->push($prescription);
            $this->doAddPrescription($prescription);
        }

        return $this;
    }

    /**
     *
     * @param ChildPrescription $prescription
     */
    protected function doAddPrescription(ChildPrescription $prescription)
    {
        $prescriptionDoseInstruction = new ChildPrescriptionDoseInstruction();

        $prescriptionDoseInstruction->setPrescription($prescription);

        $prescriptionDoseInstruction->setMedicationDoseInstruction($this);

        $this->addPrescriptionDoseInstruction($prescriptionDoseInstruction);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$prescription->isMedicationDoseInstructionsLoaded()) {
            $prescription->initMedicationDoseInstructions();
            $prescription->getMedicationDoseInstructions()->push($this);
        } elseif (!$prescription->getMedicationDoseInstructions()->contains($this)) {
            $prescription->getMedicationDoseInstructions()->push($this);
        }

    }

    /**
     * Remove prescription of this object
     * through the prescription_dose_instruction cross reference table.
     *
     * @param ChildPrescription $prescription
     * @return ChildMedicationDoseInstruction The current object (for fluent API support)
     */
    public function removePrescription(ChildPrescription $prescription)
    {
        if ($this->getPrescriptions()->contains($prescription)) {
            $prescriptionDoseInstruction = new ChildPrescriptionDoseInstruction();
            $prescriptionDoseInstruction->setPrescription($prescription);
            if ($prescription->isMedicationDoseInstructionsLoaded()) {
                //remove the back reference if available
                $prescription->getMedicationDoseInstructions()->removeObject($this);
            }

            $prescriptionDoseInstruction->setMedicationDoseInstruction($this);
            $this->removePrescriptionDoseInstruction(clone $prescriptionDoseInstruction);
            $prescriptionDoseInstruction->clear();

            $this->collPrescriptions->remove($this->collPrescriptions->search($prescription));

            if (null === $this->prescriptionsScheduledForDeletion) {
                $this->prescriptionsScheduledForDeletion = clone $this->collPrescriptions;
                $this->prescriptionsScheduledForDeletion->clear();
            }

            $this->prescriptionsScheduledForDeletion->push($prescription);
        }


        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     *
     * @return $this
     */
    public function clear()
    {
        $this->id = null;
        $this->description = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);

        return $this;
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param bool $deep Whether to also clear the references on all referrer objects.
     * @return $this
     */
    public function clearAllReferences(bool $deep = false)
    {
        if ($deep) {
            if ($this->collPrescriptionDoseInstructions) {
                foreach ($this->collPrescriptionDoseInstructions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrescriptions) {
                foreach ($this->collPrescriptions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collPrescriptionDoseInstructions = null;
        $this->collPrescriptions = null;
        return $this;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MedicationDoseInstructionTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preSave(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postSave(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before inserting to database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preInsert(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postInsert(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preUpdate(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postUpdate(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preDelete(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postDelete(?ConnectionInterface $con = null): void
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
