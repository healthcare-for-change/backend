<?php

namespace Base;

use \Vaccine as ChildVaccine;
use \VaccineQuery as ChildVaccineQuery;
use \Exception;
use \PDO;
use Map\VaccineTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'vaccine' table.
 *
 * Table containing the available vaccine types as reference for further usage in the medical and vaccine history
 *
 * @method     ChildVaccineQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildVaccineQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildVaccineQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildVaccineQuery orderByFirstVaccination($order = Criteria::ASC) Order by the first_vaccination column
 * @method     ChildVaccineQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildVaccineQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildVaccineQuery groupByUuid() Group by the uuid column
 * @method     ChildVaccineQuery groupByName() Group by the name column
 * @method     ChildVaccineQuery groupByDescription() Group by the description column
 * @method     ChildVaccineQuery groupByFirstVaccination() Group by the first_vaccination column
 * @method     ChildVaccineQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildVaccineQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildVaccineQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildVaccineQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildVaccineQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildVaccineQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildVaccineQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildVaccineQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildVaccineQuery leftJoinVaccineDoseSchedule($relationAlias = null) Adds a LEFT JOIN clause to the query using the VaccineDoseSchedule relation
 * @method     ChildVaccineQuery rightJoinVaccineDoseSchedule($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VaccineDoseSchedule relation
 * @method     ChildVaccineQuery innerJoinVaccineDoseSchedule($relationAlias = null) Adds a INNER JOIN clause to the query using the VaccineDoseSchedule relation
 *
 * @method     ChildVaccineQuery joinWithVaccineDoseSchedule($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the VaccineDoseSchedule relation
 *
 * @method     ChildVaccineQuery leftJoinWithVaccineDoseSchedule() Adds a LEFT JOIN clause and with to the query using the VaccineDoseSchedule relation
 * @method     ChildVaccineQuery rightJoinWithVaccineDoseSchedule() Adds a RIGHT JOIN clause and with to the query using the VaccineDoseSchedule relation
 * @method     ChildVaccineQuery innerJoinWithVaccineDoseSchedule() Adds a INNER JOIN clause and with to the query using the VaccineDoseSchedule relation
 *
 * @method     ChildVaccineQuery leftJoinVaccinationHistory($relationAlias = null) Adds a LEFT JOIN clause to the query using the VaccinationHistory relation
 * @method     ChildVaccineQuery rightJoinVaccinationHistory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VaccinationHistory relation
 * @method     ChildVaccineQuery innerJoinVaccinationHistory($relationAlias = null) Adds a INNER JOIN clause to the query using the VaccinationHistory relation
 *
 * @method     ChildVaccineQuery joinWithVaccinationHistory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the VaccinationHistory relation
 *
 * @method     ChildVaccineQuery leftJoinWithVaccinationHistory() Adds a LEFT JOIN clause and with to the query using the VaccinationHistory relation
 * @method     ChildVaccineQuery rightJoinWithVaccinationHistory() Adds a RIGHT JOIN clause and with to the query using the VaccinationHistory relation
 * @method     ChildVaccineQuery innerJoinWithVaccinationHistory() Adds a INNER JOIN clause and with to the query using the VaccinationHistory relation
 *
 * @method     ChildVaccineQuery leftJoinVaccinationPlan($relationAlias = null) Adds a LEFT JOIN clause to the query using the VaccinationPlan relation
 * @method     ChildVaccineQuery rightJoinVaccinationPlan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VaccinationPlan relation
 * @method     ChildVaccineQuery innerJoinVaccinationPlan($relationAlias = null) Adds a INNER JOIN clause to the query using the VaccinationPlan relation
 *
 * @method     ChildVaccineQuery joinWithVaccinationPlan($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the VaccinationPlan relation
 *
 * @method     ChildVaccineQuery leftJoinWithVaccinationPlan() Adds a LEFT JOIN clause and with to the query using the VaccinationPlan relation
 * @method     ChildVaccineQuery rightJoinWithVaccinationPlan() Adds a RIGHT JOIN clause and with to the query using the VaccinationPlan relation
 * @method     ChildVaccineQuery innerJoinWithVaccinationPlan() Adds a INNER JOIN clause and with to the query using the VaccinationPlan relation
 *
 * @method     \VaccineDoseScheduleQuery|\VaccinationHistoryQuery|\VaccinationPlanQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildVaccine|null findOne(?ConnectionInterface $con = null) Return the first ChildVaccine matching the query
 * @method     ChildVaccine findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildVaccine matching the query, or a new ChildVaccine object populated from the query conditions when no match is found
 *
 * @method     ChildVaccine|null findOneByUuid(string $uuid) Return the first ChildVaccine filtered by the uuid column
 * @method     ChildVaccine|null findOneByName(string $name) Return the first ChildVaccine filtered by the name column
 * @method     ChildVaccine|null findOneByDescription(string $description) Return the first ChildVaccine filtered by the description column
 * @method     ChildVaccine|null findOneByFirstVaccination(int $first_vaccination) Return the first ChildVaccine filtered by the first_vaccination column
 * @method     ChildVaccine|null findOneByCreatedAt(string $created_at) Return the first ChildVaccine filtered by the created_at column
 * @method     ChildVaccine|null findOneByUpdatedAt(string $updated_at) Return the first ChildVaccine filtered by the updated_at column *

 * @method     ChildVaccine requirePk($key, ?ConnectionInterface $con = null) Return the ChildVaccine by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccine requireOne(?ConnectionInterface $con = null) Return the first ChildVaccine matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVaccine requireOneByUuid(string $uuid) Return the first ChildVaccine filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccine requireOneByName(string $name) Return the first ChildVaccine filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccine requireOneByDescription(string $description) Return the first ChildVaccine filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccine requireOneByFirstVaccination(int $first_vaccination) Return the first ChildVaccine filtered by the first_vaccination column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccine requireOneByCreatedAt(string $created_at) Return the first ChildVaccine filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccine requireOneByUpdatedAt(string $updated_at) Return the first ChildVaccine filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVaccine[]|Collection find(?ConnectionInterface $con = null) Return ChildVaccine objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildVaccine> find(?ConnectionInterface $con = null) Return ChildVaccine objects based on current ModelCriteria
 * @method     ChildVaccine[]|Collection findByUuid(string $uuid) Return ChildVaccine objects filtered by the uuid column
 * @psalm-method Collection&\Traversable<ChildVaccine> findByUuid(string $uuid) Return ChildVaccine objects filtered by the uuid column
 * @method     ChildVaccine[]|Collection findByName(string $name) Return ChildVaccine objects filtered by the name column
 * @psalm-method Collection&\Traversable<ChildVaccine> findByName(string $name) Return ChildVaccine objects filtered by the name column
 * @method     ChildVaccine[]|Collection findByDescription(string $description) Return ChildVaccine objects filtered by the description column
 * @psalm-method Collection&\Traversable<ChildVaccine> findByDescription(string $description) Return ChildVaccine objects filtered by the description column
 * @method     ChildVaccine[]|Collection findByFirstVaccination(int $first_vaccination) Return ChildVaccine objects filtered by the first_vaccination column
 * @psalm-method Collection&\Traversable<ChildVaccine> findByFirstVaccination(int $first_vaccination) Return ChildVaccine objects filtered by the first_vaccination column
 * @method     ChildVaccine[]|Collection findByCreatedAt(string $created_at) Return ChildVaccine objects filtered by the created_at column
 * @psalm-method Collection&\Traversable<ChildVaccine> findByCreatedAt(string $created_at) Return ChildVaccine objects filtered by the created_at column
 * @method     ChildVaccine[]|Collection findByUpdatedAt(string $updated_at) Return ChildVaccine objects filtered by the updated_at column
 * @psalm-method Collection&\Traversable<ChildVaccine> findByUpdatedAt(string $updated_at) Return ChildVaccine objects filtered by the updated_at column
 * @method     ChildVaccine[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildVaccine> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class VaccineQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\VaccineQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\Vaccine', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildVaccineQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildVaccineQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildVaccineQuery) {
            return $criteria;
        }
        $query = new ChildVaccineQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildVaccine|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VaccineTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = VaccineTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVaccine A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT uuid, name, description, first_vaccination, created_at, updated_at FROM vaccine WHERE uuid = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildVaccine $obj */
            $obj = new ChildVaccine();
            $obj->hydrate($row);
            VaccineTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildVaccine|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(VaccineTableMap::COL_UUID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(VaccineTableMap::COL_UUID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * $query->filterByUuid(['foo', 'bar']); // WHERE uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $uuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccineTableMap::COL_UUID, $uuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * $query->filterByName(['foo', 'bar']); // WHERE name IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $name The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByName($name = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccineTableMap::COL_NAME, $name, $comparison);

        return $this;
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * $query->filterByDescription(['foo', 'bar']); // WHERE description IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $description The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDescription($description = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccineTableMap::COL_DESCRIPTION, $description, $comparison);

        return $this;
    }

    /**
     * Filter the query on the first_vaccination column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstVaccination(1234); // WHERE first_vaccination = 1234
     * $query->filterByFirstVaccination(array(12, 34)); // WHERE first_vaccination IN (12, 34)
     * $query->filterByFirstVaccination(array('min' => 12)); // WHERE first_vaccination > 12
     * </code>
     *
     * @param mixed $firstVaccination The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByFirstVaccination($firstVaccination = null, ?string $comparison = null)
    {
        if (is_array($firstVaccination)) {
            $useMinMax = false;
            if (isset($firstVaccination['min'])) {
                $this->addUsingAlias(VaccineTableMap::COL_FIRST_VACCINATION, $firstVaccination['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($firstVaccination['max'])) {
                $this->addUsingAlias(VaccineTableMap::COL_FIRST_VACCINATION, $firstVaccination['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccineTableMap::COL_FIRST_VACCINATION, $firstVaccination, $comparison);

        return $this;
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, ?string $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(VaccineTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(VaccineTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccineTableMap::COL_CREATED_AT, $createdAt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, ?string $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(VaccineTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(VaccineTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccineTableMap::COL_UPDATED_AT, $updatedAt, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \VaccineDoseSchedule object
     *
     * @param \VaccineDoseSchedule|ObjectCollection $vaccineDoseSchedule the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVaccineDoseSchedule($vaccineDoseSchedule, ?string $comparison = null)
    {
        if ($vaccineDoseSchedule instanceof \VaccineDoseSchedule) {
            $this
                ->addUsingAlias(VaccineTableMap::COL_UUID, $vaccineDoseSchedule->getVaccineUuid(), $comparison);

            return $this;
        } elseif ($vaccineDoseSchedule instanceof ObjectCollection) {
            $this
                ->useVaccineDoseScheduleQuery()
                ->filterByPrimaryKeys($vaccineDoseSchedule->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByVaccineDoseSchedule() only accepts arguments of type \VaccineDoseSchedule or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VaccineDoseSchedule relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinVaccineDoseSchedule(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VaccineDoseSchedule');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VaccineDoseSchedule');
        }

        return $this;
    }

    /**
     * Use the VaccineDoseSchedule relation VaccineDoseSchedule object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VaccineDoseScheduleQuery A secondary query class using the current class as primary query
     */
    public function useVaccineDoseScheduleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVaccineDoseSchedule($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VaccineDoseSchedule', '\VaccineDoseScheduleQuery');
    }

    /**
     * Use the VaccineDoseSchedule relation VaccineDoseSchedule object
     *
     * @param callable(\VaccineDoseScheduleQuery):\VaccineDoseScheduleQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withVaccineDoseScheduleQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useVaccineDoseScheduleQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to VaccineDoseSchedule table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \VaccineDoseScheduleQuery The inner query object of the EXISTS statement
     */
    public function useVaccineDoseScheduleExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('VaccineDoseSchedule', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to VaccineDoseSchedule table for a NOT EXISTS query.
     *
     * @see useVaccineDoseScheduleExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \VaccineDoseScheduleQuery The inner query object of the NOT EXISTS statement
     */
    public function useVaccineDoseScheduleNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('VaccineDoseSchedule', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \VaccinationHistory object
     *
     * @param \VaccinationHistory|ObjectCollection $vaccinationHistory the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVaccinationHistory($vaccinationHistory, ?string $comparison = null)
    {
        if ($vaccinationHistory instanceof \VaccinationHistory) {
            $this
                ->addUsingAlias(VaccineTableMap::COL_UUID, $vaccinationHistory->getVaccineUuid(), $comparison);

            return $this;
        } elseif ($vaccinationHistory instanceof ObjectCollection) {
            $this
                ->useVaccinationHistoryQuery()
                ->filterByPrimaryKeys($vaccinationHistory->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByVaccinationHistory() only accepts arguments of type \VaccinationHistory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VaccinationHistory relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinVaccinationHistory(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VaccinationHistory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VaccinationHistory');
        }

        return $this;
    }

    /**
     * Use the VaccinationHistory relation VaccinationHistory object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VaccinationHistoryQuery A secondary query class using the current class as primary query
     */
    public function useVaccinationHistoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVaccinationHistory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VaccinationHistory', '\VaccinationHistoryQuery');
    }

    /**
     * Use the VaccinationHistory relation VaccinationHistory object
     *
     * @param callable(\VaccinationHistoryQuery):\VaccinationHistoryQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withVaccinationHistoryQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useVaccinationHistoryQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to VaccinationHistory table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \VaccinationHistoryQuery The inner query object of the EXISTS statement
     */
    public function useVaccinationHistoryExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('VaccinationHistory', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to VaccinationHistory table for a NOT EXISTS query.
     *
     * @see useVaccinationHistoryExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \VaccinationHistoryQuery The inner query object of the NOT EXISTS statement
     */
    public function useVaccinationHistoryNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('VaccinationHistory', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \VaccinationPlan object
     *
     * @param \VaccinationPlan|ObjectCollection $vaccinationPlan the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVaccinationPlan($vaccinationPlan, ?string $comparison = null)
    {
        if ($vaccinationPlan instanceof \VaccinationPlan) {
            $this
                ->addUsingAlias(VaccineTableMap::COL_UUID, $vaccinationPlan->getVaccineUuid(), $comparison);

            return $this;
        } elseif ($vaccinationPlan instanceof ObjectCollection) {
            $this
                ->useVaccinationPlanQuery()
                ->filterByPrimaryKeys($vaccinationPlan->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByVaccinationPlan() only accepts arguments of type \VaccinationPlan or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VaccinationPlan relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinVaccinationPlan(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VaccinationPlan');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VaccinationPlan');
        }

        return $this;
    }

    /**
     * Use the VaccinationPlan relation VaccinationPlan object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VaccinationPlanQuery A secondary query class using the current class as primary query
     */
    public function useVaccinationPlanQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVaccinationPlan($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VaccinationPlan', '\VaccinationPlanQuery');
    }

    /**
     * Use the VaccinationPlan relation VaccinationPlan object
     *
     * @param callable(\VaccinationPlanQuery):\VaccinationPlanQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withVaccinationPlanQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useVaccinationPlanQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to VaccinationPlan table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \VaccinationPlanQuery The inner query object of the EXISTS statement
     */
    public function useVaccinationPlanExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('VaccinationPlan', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to VaccinationPlan table for a NOT EXISTS query.
     *
     * @see useVaccinationPlanExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \VaccinationPlanQuery The inner query object of the NOT EXISTS statement
     */
    public function useVaccinationPlanNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('VaccinationPlan', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Exclude object from result
     *
     * @param ChildVaccine $vaccine Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($vaccine = null)
    {
        if ($vaccine) {
            $this->addUsingAlias(VaccineTableMap::COL_UUID, $vaccine->getUuid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the vaccine table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VaccineTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VaccineTableMap::clearInstancePool();
            VaccineTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VaccineTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(VaccineTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            VaccineTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            VaccineTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param int $nbDays Maximum age of the latest update in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        $this->addUsingAlias(VaccineTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by update date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        $this->addDescendingOrderByColumn(VaccineTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by update date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        $this->addAscendingOrderByColumn(VaccineTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by create date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        $this->addDescendingOrderByColumn(VaccineTableMap::COL_CREATED_AT);

        return $this;
    }

    /**
     * Filter by the latest created
     *
     * @param int $nbDays Maximum age of in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        $this->addUsingAlias(VaccineTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by create date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        $this->addAscendingOrderByColumn(VaccineTableMap::COL_CREATED_AT);

        return $this;
    }

}
