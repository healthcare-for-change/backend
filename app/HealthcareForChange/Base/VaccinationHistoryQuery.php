<?php

namespace Base;

use \VaccinationHistory as ChildVaccinationHistory;
use \VaccinationHistoryQuery as ChildVaccinationHistoryQuery;
use \Exception;
use \PDO;
use Map\VaccinationHistoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'vaccination_history' table.
 *
 *
 *
 * @method     ChildVaccinationHistoryQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildVaccinationHistoryQuery orderByTimestamp($order = Criteria::ASC) Order by the timestamp column
 * @method     ChildVaccinationHistoryQuery orderByComment($order = Criteria::ASC) Order by the comment column
 * @method     ChildVaccinationHistoryQuery orderByPatientUuid($order = Criteria::ASC) Order by the patient_uuid column
 * @method     ChildVaccinationHistoryQuery orderByVaccineUuid($order = Criteria::ASC) Order by the vaccine_uuid column
 * @method     ChildVaccinationHistoryQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildVaccinationHistoryQuery orderByLot($order = Criteria::ASC) Order by the lot column
 *
 * @method     ChildVaccinationHistoryQuery groupByUuid() Group by the uuid column
 * @method     ChildVaccinationHistoryQuery groupByTimestamp() Group by the timestamp column
 * @method     ChildVaccinationHistoryQuery groupByComment() Group by the comment column
 * @method     ChildVaccinationHistoryQuery groupByPatientUuid() Group by the patient_uuid column
 * @method     ChildVaccinationHistoryQuery groupByVaccineUuid() Group by the vaccine_uuid column
 * @method     ChildVaccinationHistoryQuery groupByUserId() Group by the user_id column
 * @method     ChildVaccinationHistoryQuery groupByLot() Group by the lot column
 *
 * @method     ChildVaccinationHistoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildVaccinationHistoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildVaccinationHistoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildVaccinationHistoryQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildVaccinationHistoryQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildVaccinationHistoryQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildVaccinationHistoryQuery leftJoinPatient($relationAlias = null) Adds a LEFT JOIN clause to the query using the Patient relation
 * @method     ChildVaccinationHistoryQuery rightJoinPatient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Patient relation
 * @method     ChildVaccinationHistoryQuery innerJoinPatient($relationAlias = null) Adds a INNER JOIN clause to the query using the Patient relation
 *
 * @method     ChildVaccinationHistoryQuery joinWithPatient($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Patient relation
 *
 * @method     ChildVaccinationHistoryQuery leftJoinWithPatient() Adds a LEFT JOIN clause and with to the query using the Patient relation
 * @method     ChildVaccinationHistoryQuery rightJoinWithPatient() Adds a RIGHT JOIN clause and with to the query using the Patient relation
 * @method     ChildVaccinationHistoryQuery innerJoinWithPatient() Adds a INNER JOIN clause and with to the query using the Patient relation
 *
 * @method     ChildVaccinationHistoryQuery leftJoinVaccine($relationAlias = null) Adds a LEFT JOIN clause to the query using the Vaccine relation
 * @method     ChildVaccinationHistoryQuery rightJoinVaccine($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Vaccine relation
 * @method     ChildVaccinationHistoryQuery innerJoinVaccine($relationAlias = null) Adds a INNER JOIN clause to the query using the Vaccine relation
 *
 * @method     ChildVaccinationHistoryQuery joinWithVaccine($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Vaccine relation
 *
 * @method     ChildVaccinationHistoryQuery leftJoinWithVaccine() Adds a LEFT JOIN clause and with to the query using the Vaccine relation
 * @method     ChildVaccinationHistoryQuery rightJoinWithVaccine() Adds a RIGHT JOIN clause and with to the query using the Vaccine relation
 * @method     ChildVaccinationHistoryQuery innerJoinWithVaccine() Adds a INNER JOIN clause and with to the query using the Vaccine relation
 *
 * @method     ChildVaccinationHistoryQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildVaccinationHistoryQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildVaccinationHistoryQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildVaccinationHistoryQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildVaccinationHistoryQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildVaccinationHistoryQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildVaccinationHistoryQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     \PatientQuery|\VaccineQuery|\UserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildVaccinationHistory|null findOne(?ConnectionInterface $con = null) Return the first ChildVaccinationHistory matching the query
 * @method     ChildVaccinationHistory findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildVaccinationHistory matching the query, or a new ChildVaccinationHistory object populated from the query conditions when no match is found
 *
 * @method     ChildVaccinationHistory|null findOneByUuid(string $uuid) Return the first ChildVaccinationHistory filtered by the uuid column
 * @method     ChildVaccinationHistory|null findOneByTimestamp(string $timestamp) Return the first ChildVaccinationHistory filtered by the timestamp column
 * @method     ChildVaccinationHistory|null findOneByComment(string $comment) Return the first ChildVaccinationHistory filtered by the comment column
 * @method     ChildVaccinationHistory|null findOneByPatientUuid(string $patient_uuid) Return the first ChildVaccinationHistory filtered by the patient_uuid column
 * @method     ChildVaccinationHistory|null findOneByVaccineUuid(string $vaccine_uuid) Return the first ChildVaccinationHistory filtered by the vaccine_uuid column
 * @method     ChildVaccinationHistory|null findOneByUserId(int $user_id) Return the first ChildVaccinationHistory filtered by the user_id column
 * @method     ChildVaccinationHistory|null findOneByLot(string $lot) Return the first ChildVaccinationHistory filtered by the lot column *

 * @method     ChildVaccinationHistory requirePk($key, ?ConnectionInterface $con = null) Return the ChildVaccinationHistory by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccinationHistory requireOne(?ConnectionInterface $con = null) Return the first ChildVaccinationHistory matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVaccinationHistory requireOneByUuid(string $uuid) Return the first ChildVaccinationHistory filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccinationHistory requireOneByTimestamp(string $timestamp) Return the first ChildVaccinationHistory filtered by the timestamp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccinationHistory requireOneByComment(string $comment) Return the first ChildVaccinationHistory filtered by the comment column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccinationHistory requireOneByPatientUuid(string $patient_uuid) Return the first ChildVaccinationHistory filtered by the patient_uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccinationHistory requireOneByVaccineUuid(string $vaccine_uuid) Return the first ChildVaccinationHistory filtered by the vaccine_uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccinationHistory requireOneByUserId(int $user_id) Return the first ChildVaccinationHistory filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccinationHistory requireOneByLot(string $lot) Return the first ChildVaccinationHistory filtered by the lot column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVaccinationHistory[]|Collection find(?ConnectionInterface $con = null) Return ChildVaccinationHistory objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildVaccinationHistory> find(?ConnectionInterface $con = null) Return ChildVaccinationHistory objects based on current ModelCriteria
 * @method     ChildVaccinationHistory[]|Collection findByUuid(string $uuid) Return ChildVaccinationHistory objects filtered by the uuid column
 * @psalm-method Collection&\Traversable<ChildVaccinationHistory> findByUuid(string $uuid) Return ChildVaccinationHistory objects filtered by the uuid column
 * @method     ChildVaccinationHistory[]|Collection findByTimestamp(string $timestamp) Return ChildVaccinationHistory objects filtered by the timestamp column
 * @psalm-method Collection&\Traversable<ChildVaccinationHistory> findByTimestamp(string $timestamp) Return ChildVaccinationHistory objects filtered by the timestamp column
 * @method     ChildVaccinationHistory[]|Collection findByComment(string $comment) Return ChildVaccinationHistory objects filtered by the comment column
 * @psalm-method Collection&\Traversable<ChildVaccinationHistory> findByComment(string $comment) Return ChildVaccinationHistory objects filtered by the comment column
 * @method     ChildVaccinationHistory[]|Collection findByPatientUuid(string $patient_uuid) Return ChildVaccinationHistory objects filtered by the patient_uuid column
 * @psalm-method Collection&\Traversable<ChildVaccinationHistory> findByPatientUuid(string $patient_uuid) Return ChildVaccinationHistory objects filtered by the patient_uuid column
 * @method     ChildVaccinationHistory[]|Collection findByVaccineUuid(string $vaccine_uuid) Return ChildVaccinationHistory objects filtered by the vaccine_uuid column
 * @psalm-method Collection&\Traversable<ChildVaccinationHistory> findByVaccineUuid(string $vaccine_uuid) Return ChildVaccinationHistory objects filtered by the vaccine_uuid column
 * @method     ChildVaccinationHistory[]|Collection findByUserId(int $user_id) Return ChildVaccinationHistory objects filtered by the user_id column
 * @psalm-method Collection&\Traversable<ChildVaccinationHistory> findByUserId(int $user_id) Return ChildVaccinationHistory objects filtered by the user_id column
 * @method     ChildVaccinationHistory[]|Collection findByLot(string $lot) Return ChildVaccinationHistory objects filtered by the lot column
 * @psalm-method Collection&\Traversable<ChildVaccinationHistory> findByLot(string $lot) Return ChildVaccinationHistory objects filtered by the lot column
 * @method     ChildVaccinationHistory[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildVaccinationHistory> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class VaccinationHistoryQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\VaccinationHistoryQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\VaccinationHistory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildVaccinationHistoryQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildVaccinationHistoryQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildVaccinationHistoryQuery) {
            return $criteria;
        }
        $query = new ChildVaccinationHistoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildVaccinationHistory|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VaccinationHistoryTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = VaccinationHistoryTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVaccinationHistory A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT uuid, timestamp, comment, patient_uuid, vaccine_uuid, user_id, lot FROM vaccination_history WHERE uuid = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildVaccinationHistory $obj */
            $obj = new ChildVaccinationHistory();
            $obj->hydrate($row);
            VaccinationHistoryTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildVaccinationHistory|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(VaccinationHistoryTableMap::COL_UUID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(VaccinationHistoryTableMap::COL_UUID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * $query->filterByUuid(['foo', 'bar']); // WHERE uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $uuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccinationHistoryTableMap::COL_UUID, $uuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the timestamp column
     *
     * Example usage:
     * <code>
     * $query->filterByTimestamp('2011-03-14'); // WHERE timestamp = '2011-03-14'
     * $query->filterByTimestamp('now'); // WHERE timestamp = '2011-03-14'
     * $query->filterByTimestamp(array('max' => 'yesterday')); // WHERE timestamp > '2011-03-13'
     * </code>
     *
     * @param mixed $timestamp The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByTimestamp($timestamp = null, ?string $comparison = null)
    {
        if (is_array($timestamp)) {
            $useMinMax = false;
            if (isset($timestamp['min'])) {
                $this->addUsingAlias(VaccinationHistoryTableMap::COL_TIMESTAMP, $timestamp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($timestamp['max'])) {
                $this->addUsingAlias(VaccinationHistoryTableMap::COL_TIMESTAMP, $timestamp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccinationHistoryTableMap::COL_TIMESTAMP, $timestamp, $comparison);

        return $this;
    }

    /**
     * Filter the query on the comment column
     *
     * Example usage:
     * <code>
     * $query->filterByComment('fooValue');   // WHERE comment = 'fooValue'
     * $query->filterByComment('%fooValue%', Criteria::LIKE); // WHERE comment LIKE '%fooValue%'
     * $query->filterByComment(['foo', 'bar']); // WHERE comment IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $comment The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByComment($comment = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comment)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccinationHistoryTableMap::COL_COMMENT, $comment, $comparison);

        return $this;
    }

    /**
     * Filter the query on the patient_uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByPatientUuid('fooValue');   // WHERE patient_uuid = 'fooValue'
     * $query->filterByPatientUuid('%fooValue%', Criteria::LIKE); // WHERE patient_uuid LIKE '%fooValue%'
     * $query->filterByPatientUuid(['foo', 'bar']); // WHERE patient_uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $patientUuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPatientUuid($patientUuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($patientUuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccinationHistoryTableMap::COL_PATIENT_UUID, $patientUuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the vaccine_uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByVaccineUuid('fooValue');   // WHERE vaccine_uuid = 'fooValue'
     * $query->filterByVaccineUuid('%fooValue%', Criteria::LIKE); // WHERE vaccine_uuid LIKE '%fooValue%'
     * $query->filterByVaccineUuid(['foo', 'bar']); // WHERE vaccine_uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $vaccineUuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVaccineUuid($vaccineUuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($vaccineUuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccinationHistoryTableMap::COL_VACCINE_UUID, $vaccineUuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUserId($userId = null, ?string $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(VaccinationHistoryTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(VaccinationHistoryTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccinationHistoryTableMap::COL_USER_ID, $userId, $comparison);

        return $this;
    }

    /**
     * Filter the query on the lot column
     *
     * Example usage:
     * <code>
     * $query->filterByLot('fooValue');   // WHERE lot = 'fooValue'
     * $query->filterByLot('%fooValue%', Criteria::LIKE); // WHERE lot LIKE '%fooValue%'
     * $query->filterByLot(['foo', 'bar']); // WHERE lot IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $lot The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByLot($lot = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lot)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccinationHistoryTableMap::COL_LOT, $lot, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \Patient object
     *
     * @param \Patient|ObjectCollection $patient The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPatient($patient, ?string $comparison = null)
    {
        if ($patient instanceof \Patient) {
            return $this
                ->addUsingAlias(VaccinationHistoryTableMap::COL_PATIENT_UUID, $patient->getUuid(), $comparison);
        } elseif ($patient instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(VaccinationHistoryTableMap::COL_PATIENT_UUID, $patient->toKeyValue('PrimaryKey', 'Uuid'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByPatient() only accepts arguments of type \Patient or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Patient relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPatient(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Patient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Patient');
        }

        return $this;
    }

    /**
     * Use the Patient relation Patient object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PatientQuery A secondary query class using the current class as primary query
     */
    public function usePatientQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPatient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Patient', '\PatientQuery');
    }

    /**
     * Use the Patient relation Patient object
     *
     * @param callable(\PatientQuery):\PatientQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPatientQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePatientQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Patient table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PatientQuery The inner query object of the EXISTS statement
     */
    public function usePatientExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Patient', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Patient table for a NOT EXISTS query.
     *
     * @see usePatientExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PatientQuery The inner query object of the NOT EXISTS statement
     */
    public function usePatientNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Patient', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \Vaccine object
     *
     * @param \Vaccine|ObjectCollection $vaccine The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVaccine($vaccine, ?string $comparison = null)
    {
        if ($vaccine instanceof \Vaccine) {
            return $this
                ->addUsingAlias(VaccinationHistoryTableMap::COL_VACCINE_UUID, $vaccine->getUuid(), $comparison);
        } elseif ($vaccine instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(VaccinationHistoryTableMap::COL_VACCINE_UUID, $vaccine->toKeyValue('PrimaryKey', 'Uuid'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByVaccine() only accepts arguments of type \Vaccine or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Vaccine relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinVaccine(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Vaccine');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Vaccine');
        }

        return $this;
    }

    /**
     * Use the Vaccine relation Vaccine object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VaccineQuery A secondary query class using the current class as primary query
     */
    public function useVaccineQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVaccine($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Vaccine', '\VaccineQuery');
    }

    /**
     * Use the Vaccine relation Vaccine object
     *
     * @param callable(\VaccineQuery):\VaccineQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withVaccineQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useVaccineQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Vaccine table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \VaccineQuery The inner query object of the EXISTS statement
     */
    public function useVaccineExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Vaccine', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Vaccine table for a NOT EXISTS query.
     *
     * @see useVaccineExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \VaccineQuery The inner query object of the NOT EXISTS statement
     */
    public function useVaccineNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Vaccine', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \User object
     *
     * @param \User|ObjectCollection $user The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUser($user, ?string $comparison = null)
    {
        if ($user instanceof \User) {
            return $this
                ->addUsingAlias(VaccinationHistoryTableMap::COL_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(VaccinationHistoryTableMap::COL_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinUser(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\UserQuery');
    }

    /**
     * Use the User relation User object
     *
     * @param callable(\UserQuery):\UserQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUserQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUserQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to User table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \UserQuery The inner query object of the EXISTS statement
     */
    public function useUserExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('User', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to User table for a NOT EXISTS query.
     *
     * @see useUserExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \UserQuery The inner query object of the NOT EXISTS statement
     */
    public function useUserNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('User', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Exclude object from result
     *
     * @param ChildVaccinationHistory $vaccinationHistory Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($vaccinationHistory = null)
    {
        if ($vaccinationHistory) {
            $this->addUsingAlias(VaccinationHistoryTableMap::COL_UUID, $vaccinationHistory->getUuid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the vaccination_history table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VaccinationHistoryTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VaccinationHistoryTableMap::clearInstancePool();
            VaccinationHistoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VaccinationHistoryTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(VaccinationHistoryTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            VaccinationHistoryTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            VaccinationHistoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

}
