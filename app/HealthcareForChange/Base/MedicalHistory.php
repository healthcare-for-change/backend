<?php

namespace Base;

use \MedicalDiagnosisCode as ChildMedicalDiagnosisCode;
use \MedicalDiagnosisCodeQuery as ChildMedicalDiagnosisCodeQuery;
use \MedicalHistoryQuery as ChildMedicalHistoryQuery;
use \Patient as ChildPatient;
use \PatientQuery as ChildPatientQuery;
use \User as ChildUser;
use \UserQuery as ChildUserQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\MedicalHistoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'medical_history' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class MedicalHistory implements ActiveRecordInterface
{
    /**
     * TableMap class name
     *
     * @var string
     */
    public const TABLE_MAP = '\\Map\\MedicalHistoryTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var bool
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var bool
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = [];

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = [];

    /**
     * The value for the uuid field.
     *
     * @var        string
     */
    protected $uuid;

    /**
     * The value for the patient_uuid field.
     *
     * @var        string
     */
    protected $patient_uuid;

    /**
     * The value for the user_id field.
     *
     * @var        int
     */
    protected $user_id;

    /**
     * The value for the clinical_status field.
     * 1 active | 2 inactive | 3 resolved
     * @var        int|null
     */
    protected $clinical_status;

    /**
     * The value for the verification_status field.
     * 1 unconfirmed | 2 provisional | 3 refuted
     * @var        int|null
     */
    protected $verification_status;

    /**
     * The value for the severity field.
     * 1 severe | 2 moderate | 3 mild
     * @var        int|null
     */
    protected $severity;

    /**
     * The value for the note field.
     *
     * @var        string|null
     */
    protected $note;

    /**
     * The value for the medical_diagnosis_code_id field.
     *
     * @var        string|null
     */
    protected $medical_diagnosis_code_id;

    /**
     * The value for the onset_date field.
     * Estimated or actual date
     * @var        DateTime|null
     */
    protected $onset_date;

    /**
     * The value for the onset_age field.
     * Estimated or actual age
     * @var        int|null
     */
    protected $onset_age;

    /**
     * The value for the abatement_date field.
     * When in resolution/remission
     * @var        DateTime|null
     */
    protected $abatement_date;

    /**
     * The value for the abatement_age field.
     * When in resolution/remission
     * @var        int|null
     */
    protected $abatement_age;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        ChildUser
     */
    protected $aUser;

    /**
     * @var        ChildPatient
     */
    protected $aPatient;

    /**
     * @var        ChildMedicalDiagnosisCode
     */
    protected $aMedicalDiagnosisCode;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var bool
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Base\MedicalHistory object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return bool True if the object has been modified.
     */
    public function isModified(): bool
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param string $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return bool True if $col has been modified.
     */
    public function isColumnModified(string $col): bool
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns(): array
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return bool True, if the object has never been persisted.
     */
    public function isNew(): bool
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param bool $b the state of the object.
     */
    public function setNew(bool $b): void
    {
        $this->new = $b;
    }

    /**
     * Whether this object has been deleted.
     * @return bool The deleted state of this object.
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param bool $b The deleted state of this object.
     * @return void
     */
    public function setDeleted(bool $b): void
    {
        $this->deleted = $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified(?string $col = null): void
    {
        if (null !== $col) {
            unset($this->modifiedColumns[$col]);
        } else {
            $this->modifiedColumns = [];
        }
    }

    /**
     * Compares this with another <code>MedicalHistory</code> instance.  If
     * <code>obj</code> is an instance of <code>MedicalHistory</code>, delegates to
     * <code>equals(MedicalHistory)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param mixed $obj The object to compare to.
     * @return bool Whether equal to the object specified.
     */
    public function equals($obj): bool
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns(): array
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return bool
     */
    public function hasVirtualColumn(string $name): bool
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return mixed
     *
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getVirtualColumn(string $name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of nonexistent virtual column `%s`.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @param mixed $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn(string $name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param string $msg
     * @param int $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log(string $msg, int $priority = Propel::LOG_INFO): void
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param \Propel\Runtime\Parser\AbstractParser|string $parser An AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string The exported data
     */
    public function exportTo($parser, bool $includeLazyLoadColumns = true, string $keyType = TableMap::TYPE_PHPNAME): string
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     *
     * @return array<string>
     */
    public function __sleep(): array
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [uuid] column value.
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Get the [patient_uuid] column value.
     *
     * @return string
     */
    public function getPatientUuid()
    {
        return $this->patient_uuid;
    }

    /**
     * Get the [user_id] column value.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Get the [clinical_status] column value.
     * 1 active | 2 inactive | 3 resolved
     * @return int|null
     */
    public function getClinicalStatus()
    {
        return $this->clinical_status;
    }

    /**
     * Get the [verification_status] column value.
     * 1 unconfirmed | 2 provisional | 3 refuted
     * @return int|null
     */
    public function getVerificationStatus()
    {
        return $this->verification_status;
    }

    /**
     * Get the [severity] column value.
     * 1 severe | 2 moderate | 3 mild
     * @return int|null
     */
    public function getSeverity()
    {
        return $this->severity;
    }

    /**
     * Get the [note] column value.
     *
     * @return string|null
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Get the [medical_diagnosis_code_id] column value.
     *
     * @return string|null
     */
    public function getMedicalDiagnosisCodeId()
    {
        return $this->medical_diagnosis_code_id;
    }

    /**
     * Get the [optionally formatted] temporal [onset_date] column value.
     * Estimated or actual date
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getOnsetDate($format = null)
    {
        if ($format === null) {
            return $this->onset_date;
        } else {
            return $this->onset_date instanceof \DateTimeInterface ? $this->onset_date->format($format) : null;
        }
    }

    /**
     * Get the [onset_age] column value.
     * Estimated or actual age
     * @return int|null
     */
    public function getOnsetAge()
    {
        return $this->onset_age;
    }

    /**
     * Get the [optionally formatted] temporal [abatement_date] column value.
     * When in resolution/remission
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getAbatementDate($format = null)
    {
        if ($format === null) {
            return $this->abatement_date;
        } else {
            return $this->abatement_date instanceof \DateTimeInterface ? $this->abatement_date->format($format) : null;
        }
    }

    /**
     * Get the [abatement_age] column value.
     * When in resolution/remission
     * @return int|null
     */
    public function getAbatementAge()
    {
        return $this->abatement_age;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [uuid] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uuid !== $v) {
            $this->uuid = $v;
            $this->modifiedColumns[MedicalHistoryTableMap::COL_UUID] = true;
        }

        return $this;
    }

    /**
     * Set the value of [patient_uuid] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setPatientUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->patient_uuid !== $v) {
            $this->patient_uuid = $v;
            $this->modifiedColumns[MedicalHistoryTableMap::COL_PATIENT_UUID] = true;
        }

        if ($this->aPatient !== null && $this->aPatient->getUuid() !== $v) {
            $this->aPatient = null;
        }

        return $this;
    }

    /**
     * Set the value of [user_id] column.
     *
     * @param int $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->user_id !== $v) {
            $this->user_id = $v;
            $this->modifiedColumns[MedicalHistoryTableMap::COL_USER_ID] = true;
        }

        if ($this->aUser !== null && $this->aUser->getId() !== $v) {
            $this->aUser = null;
        }

        return $this;
    }

    /**
     * Set the value of [clinical_status] column.
     * 1 active | 2 inactive | 3 resolved
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setClinicalStatus($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->clinical_status !== $v) {
            $this->clinical_status = $v;
            $this->modifiedColumns[MedicalHistoryTableMap::COL_CLINICAL_STATUS] = true;
        }

        return $this;
    }

    /**
     * Set the value of [verification_status] column.
     * 1 unconfirmed | 2 provisional | 3 refuted
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setVerificationStatus($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->verification_status !== $v) {
            $this->verification_status = $v;
            $this->modifiedColumns[MedicalHistoryTableMap::COL_VERIFICATION_STATUS] = true;
        }

        return $this;
    }

    /**
     * Set the value of [severity] column.
     * 1 severe | 2 moderate | 3 mild
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setSeverity($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->severity !== $v) {
            $this->severity = $v;
            $this->modifiedColumns[MedicalHistoryTableMap::COL_SEVERITY] = true;
        }

        return $this;
    }

    /**
     * Set the value of [note] column.
     *
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setNote($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->note !== $v) {
            $this->note = $v;
            $this->modifiedColumns[MedicalHistoryTableMap::COL_NOTE] = true;
        }

        return $this;
    }

    /**
     * Set the value of [medical_diagnosis_code_id] column.
     *
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setMedicalDiagnosisCodeId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->medical_diagnosis_code_id !== $v) {
            $this->medical_diagnosis_code_id = $v;
            $this->modifiedColumns[MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID] = true;
        }

        if ($this->aMedicalDiagnosisCode !== null && $this->aMedicalDiagnosisCode->getId() !== $v) {
            $this->aMedicalDiagnosisCode = null;
        }

        return $this;
    }

    /**
     * Sets the value of [onset_date] column to a normalized version of the date/time value specified.
     * Estimated or actual date
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setOnsetDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->onset_date !== null || $dt !== null) {
            if ($this->onset_date === null || $dt === null || $dt->format("Y-m-d") !== $this->onset_date->format("Y-m-d")) {
                $this->onset_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MedicalHistoryTableMap::COL_ONSET_DATE] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Set the value of [onset_age] column.
     * Estimated or actual age
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setOnsetAge($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->onset_age !== $v) {
            $this->onset_age = $v;
            $this->modifiedColumns[MedicalHistoryTableMap::COL_ONSET_AGE] = true;
        }

        return $this;
    }

    /**
     * Sets the value of [abatement_date] column to a normalized version of the date/time value specified.
     * When in resolution/remission
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setAbatementDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->abatement_date !== null || $dt !== null) {
            if ($this->abatement_date === null || $dt === null || $dt->format("Y-m-d") !== $this->abatement_date->format("Y-m-d")) {
                $this->abatement_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MedicalHistoryTableMap::COL_ABATEMENT_DATE] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Set the value of [abatement_age] column.
     * When in resolution/remission
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setAbatementAge($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->abatement_age !== $v) {
            $this->abatement_age = $v;
            $this->modifiedColumns[MedicalHistoryTableMap::COL_ABATEMENT_AGE] = true;
        }

        return $this;
    }

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MedicalHistoryTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MedicalHistoryTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return bool Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues(): bool
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    }

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by DataFetcher->fetch().
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param bool $rehydrate Whether this object is being re-hydrated from the database.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int next starting column
     * @throws \Propel\Runtime\Exception\PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate(array $row, int $startcol = 0, bool $rehydrate = false, string $indexType = TableMap::TYPE_NUM): int
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : MedicalHistoryTableMap::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : MedicalHistoryTableMap::translateFieldName('PatientUuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->patient_uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : MedicalHistoryTableMap::translateFieldName('UserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : MedicalHistoryTableMap::translateFieldName('ClinicalStatus', TableMap::TYPE_PHPNAME, $indexType)];
            $this->clinical_status = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : MedicalHistoryTableMap::translateFieldName('VerificationStatus', TableMap::TYPE_PHPNAME, $indexType)];
            $this->verification_status = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : MedicalHistoryTableMap::translateFieldName('Severity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->severity = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : MedicalHistoryTableMap::translateFieldName('Note', TableMap::TYPE_PHPNAME, $indexType)];
            $this->note = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : MedicalHistoryTableMap::translateFieldName('MedicalDiagnosisCodeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->medical_diagnosis_code_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : MedicalHistoryTableMap::translateFieldName('OnsetDate', TableMap::TYPE_PHPNAME, $indexType)];
            $this->onset_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : MedicalHistoryTableMap::translateFieldName('OnsetAge', TableMap::TYPE_PHPNAME, $indexType)];
            $this->onset_age = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : MedicalHistoryTableMap::translateFieldName('AbatementDate', TableMap::TYPE_PHPNAME, $indexType)];
            $this->abatement_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : MedicalHistoryTableMap::translateFieldName('AbatementAge', TableMap::TYPE_PHPNAME, $indexType)];
            $this->abatement_age = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : MedicalHistoryTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : MedicalHistoryTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = MedicalHistoryTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\MedicalHistory'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function ensureConsistency(): void
    {
        if ($this->aPatient !== null && $this->patient_uuid !== $this->aPatient->getUuid()) {
            $this->aPatient = null;
        }
        if ($this->aUser !== null && $this->user_id !== $this->aUser->getId()) {
            $this->aUser = null;
        }
        if ($this->aMedicalDiagnosisCode !== null && $this->medical_diagnosis_code_id !== $this->aMedicalDiagnosisCode->getId()) {
            $this->aMedicalDiagnosisCode = null;
        }
    }

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param bool $deep (optional) Whether to also de-associated any related objects.
     * @param ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload(bool $deep = false, ?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MedicalHistoryTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildMedicalHistoryQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aUser = null;
            $this->aPatient = null;
            $this->aMedicalDiagnosisCode = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param ConnectionInterface $con
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     * @see MedicalHistory::setDeleted()
     * @see MedicalHistory::isDeleted()
     */
    public function delete(?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicalHistoryTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildMedicalHistoryQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    public function save(?ConnectionInterface $con = null): int
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicalHistoryTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(MedicalHistoryTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(MedicalHistoryTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(MedicalHistoryTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MedicalHistoryTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con): int
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->aPatient !== null) {
                if ($this->aPatient->isModified() || $this->aPatient->isNew()) {
                    $affectedRows += $this->aPatient->save($con);
                }
                $this->setPatient($this->aPatient);
            }

            if ($this->aMedicalDiagnosisCode !== null) {
                if ($this->aMedicalDiagnosisCode->isModified() || $this->aMedicalDiagnosisCode->isNew()) {
                    $affectedRows += $this->aMedicalDiagnosisCode->save($con);
                }
                $this->setMedicalDiagnosisCode($this->aMedicalDiagnosisCode);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    }

    /**
     * Insert the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con): void
    {
        $modifiedColumns = [];
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'uuid';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_PATIENT_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'patient_uuid';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'user_id';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_CLINICAL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'clinical_status';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_VERIFICATION_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'verification_status';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_SEVERITY)) {
            $modifiedColumns[':p' . $index++]  = 'severity';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_NOTE)) {
            $modifiedColumns[':p' . $index++]  = 'note';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'medical_diagnosis_code_id';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_ONSET_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'onset_date';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_ONSET_AGE)) {
            $modifiedColumns[':p' . $index++]  = 'onset_age';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_ABATEMENT_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'abatement_date';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_ABATEMENT_AGE)) {
            $modifiedColumns[':p' . $index++]  = 'abatement_age';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO medical_history (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'uuid':
                        $stmt->bindValue($identifier, $this->uuid, PDO::PARAM_STR);
                        break;
                    case 'patient_uuid':
                        $stmt->bindValue($identifier, $this->patient_uuid, PDO::PARAM_STR);
                        break;
                    case 'user_id':
                        $stmt->bindValue($identifier, $this->user_id, PDO::PARAM_INT);
                        break;
                    case 'clinical_status':
                        $stmt->bindValue($identifier, $this->clinical_status, PDO::PARAM_INT);
                        break;
                    case 'verification_status':
                        $stmt->bindValue($identifier, $this->verification_status, PDO::PARAM_INT);
                        break;
                    case 'severity':
                        $stmt->bindValue($identifier, $this->severity, PDO::PARAM_INT);
                        break;
                    case 'note':
                        $stmt->bindValue($identifier, $this->note, PDO::PARAM_STR);
                        break;
                    case 'medical_diagnosis_code_id':
                        $stmt->bindValue($identifier, $this->medical_diagnosis_code_id, PDO::PARAM_INT);
                        break;
                    case 'onset_date':
                        $stmt->bindValue($identifier, $this->onset_date ? $this->onset_date->format("Y-m-d") : null, PDO::PARAM_STR);
                        break;
                    case 'onset_age':
                        $stmt->bindValue($identifier, $this->onset_age, PDO::PARAM_INT);
                        break;
                    case 'abatement_date':
                        $stmt->bindValue($identifier, $this->abatement_date ? $this->abatement_date->format("Y-m-d") : null, PDO::PARAM_STR);
                        break;
                    case 'abatement_age':
                        $stmt->bindValue($identifier, $this->abatement_age, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @return int Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con): int
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName(string $name, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MedicalHistoryTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos Position in XML schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition(int $pos)
    {
        switch ($pos) {
            case 0:
                return $this->getUuid();

            case 1:
                return $this->getPatientUuid();

            case 2:
                return $this->getUserId();

            case 3:
                return $this->getClinicalStatus();

            case 4:
                return $this->getVerificationStatus();

            case 5:
                return $this->getSeverity();

            case 6:
                return $this->getNote();

            case 7:
                return $this->getMedicalDiagnosisCodeId();

            case 8:
                return $this->getOnsetDate();

            case 9:
                return $this->getOnsetAge();

            case 10:
                return $this->getAbatementDate();

            case 11:
                return $this->getAbatementAge();

            case 12:
                return $this->getCreatedAt();

            case 13:
                return $this->getUpdatedAt();

            default:
                return null;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param bool $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array An associative array containing the field names (as keys) and field values
     */
    public function toArray(string $keyType = TableMap::TYPE_PHPNAME, bool $includeLazyLoadColumns = true, array $alreadyDumpedObjects = [], bool $includeForeignObjects = false): array
    {
        if (isset($alreadyDumpedObjects['MedicalHistory'][$this->hashCode()])) {
            return ['*RECURSION*'];
        }
        $alreadyDumpedObjects['MedicalHistory'][$this->hashCode()] = true;
        $keys = MedicalHistoryTableMap::getFieldNames($keyType);
        $result = [
            $keys[0] => $this->getUuid(),
            $keys[1] => $this->getPatientUuid(),
            $keys[2] => $this->getUserId(),
            $keys[3] => $this->getClinicalStatus(),
            $keys[4] => $this->getVerificationStatus(),
            $keys[5] => $this->getSeverity(),
            $keys[6] => $this->getNote(),
            $keys[7] => $this->getMedicalDiagnosisCodeId(),
            $keys[8] => $this->getOnsetDate(),
            $keys[9] => $this->getOnsetAge(),
            $keys[10] => $this->getAbatementDate(),
            $keys[11] => $this->getAbatementAge(),
            $keys[12] => $this->getCreatedAt(),
            $keys[13] => $this->getUpdatedAt(),
        ];
        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('Y-m-d');
        }

        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('Y-m-d');
        }

        if ($result[$keys[12]] instanceof \DateTimeInterface) {
            $result[$keys[12]] = $result[$keys[12]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[13]] instanceof \DateTimeInterface) {
            $result[$keys[13]] = $result[$keys[13]]->format('Y-m-d H:i:s.u');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'users';
                        break;
                    default:
                        $key = 'User';
                }

                $result[$key] = $this->aUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPatient) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'patient';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'patient';
                        break;
                    default:
                        $key = 'Patient';
                }

                $result[$key] = $this->aPatient->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMedicalDiagnosisCode) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'medicalDiagnosisCode';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'medical_diagnosis_code';
                        break;
                    default:
                        $key = 'MedicalDiagnosisCode';
                }

                $result[$key] = $this->aMedicalDiagnosisCode->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this
     */
    public function setByName(string $name, $value, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MedicalHistoryTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        $this->setByPosition($pos, $value);

        return $this;
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return $this
     */
    public function setByPosition(int $pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setUuid($value);
                break;
            case 1:
                $this->setPatientUuid($value);
                break;
            case 2:
                $this->setUserId($value);
                break;
            case 3:
                $this->setClinicalStatus($value);
                break;
            case 4:
                $this->setVerificationStatus($value);
                break;
            case 5:
                $this->setSeverity($value);
                break;
            case 6:
                $this->setNote($value);
                break;
            case 7:
                $this->setMedicalDiagnosisCodeId($value);
                break;
            case 8:
                $this->setOnsetDate($value);
                break;
            case 9:
                $this->setOnsetAge($value);
                break;
            case 10:
                $this->setAbatementDate($value);
                break;
            case 11:
                $this->setAbatementAge($value);
                break;
            case 12:
                $this->setCreatedAt($value);
                break;
            case 13:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param array $arr An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return $this
     */
    public function fromArray(array $arr, string $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = MedicalHistoryTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setUuid($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setPatientUuid($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setUserId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setClinicalStatus($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setVerificationStatus($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setSeverity($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setNote($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setMedicalDiagnosisCodeId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setOnsetDate($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setOnsetAge($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setAbatementDate($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setAbatementAge($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setCreatedAt($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setUpdatedAt($arr[$keys[13]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this The current object, for fluid interface
     */
    public function importFrom($parser, string $data, string $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria(): Criteria
    {
        $criteria = new Criteria(MedicalHistoryTableMap::DATABASE_NAME);

        if ($this->isColumnModified(MedicalHistoryTableMap::COL_UUID)) {
            $criteria->add(MedicalHistoryTableMap::COL_UUID, $this->uuid);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_PATIENT_UUID)) {
            $criteria->add(MedicalHistoryTableMap::COL_PATIENT_UUID, $this->patient_uuid);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_USER_ID)) {
            $criteria->add(MedicalHistoryTableMap::COL_USER_ID, $this->user_id);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_CLINICAL_STATUS)) {
            $criteria->add(MedicalHistoryTableMap::COL_CLINICAL_STATUS, $this->clinical_status);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_VERIFICATION_STATUS)) {
            $criteria->add(MedicalHistoryTableMap::COL_VERIFICATION_STATUS, $this->verification_status);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_SEVERITY)) {
            $criteria->add(MedicalHistoryTableMap::COL_SEVERITY, $this->severity);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_NOTE)) {
            $criteria->add(MedicalHistoryTableMap::COL_NOTE, $this->note);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID)) {
            $criteria->add(MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID, $this->medical_diagnosis_code_id);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_ONSET_DATE)) {
            $criteria->add(MedicalHistoryTableMap::COL_ONSET_DATE, $this->onset_date);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_ONSET_AGE)) {
            $criteria->add(MedicalHistoryTableMap::COL_ONSET_AGE, $this->onset_age);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_ABATEMENT_DATE)) {
            $criteria->add(MedicalHistoryTableMap::COL_ABATEMENT_DATE, $this->abatement_date);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_ABATEMENT_AGE)) {
            $criteria->add(MedicalHistoryTableMap::COL_ABATEMENT_AGE, $this->abatement_age);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_CREATED_AT)) {
            $criteria->add(MedicalHistoryTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(MedicalHistoryTableMap::COL_UPDATED_AT)) {
            $criteria->add(MedicalHistoryTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria(): Criteria
    {
        $criteria = ChildMedicalHistoryQuery::create();
        $criteria->add(MedicalHistoryTableMap::COL_UUID, $this->uuid);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int|string Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getUuid();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getUuid();
    }

    /**
     * Generic method to set the primary key (uuid column).
     *
     * @param string|null $key Primary key.
     * @return void
     */
    public function setPrimaryKey(?string $key = null): void
    {
        $this->setUuid($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     *
     * @return bool
     */
    public function isPrimaryKeyNull(): bool
    {
        return null === $this->getUuid();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of \MedicalHistory (or compatible) type.
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param bool $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function copyInto(object $copyObj, bool $deepCopy = false, bool $makeNew = true): void
    {
        $copyObj->setUuid($this->getUuid());
        $copyObj->setPatientUuid($this->getPatientUuid());
        $copyObj->setUserId($this->getUserId());
        $copyObj->setClinicalStatus($this->getClinicalStatus());
        $copyObj->setVerificationStatus($this->getVerificationStatus());
        $copyObj->setSeverity($this->getSeverity());
        $copyObj->setNote($this->getNote());
        $copyObj->setMedicalDiagnosisCodeId($this->getMedicalDiagnosisCodeId());
        $copyObj->setOnsetDate($this->getOnsetDate());
        $copyObj->setOnsetAge($this->getOnsetAge());
        $copyObj->setAbatementDate($this->getAbatementDate());
        $copyObj->setAbatementAge($this->getAbatementAge());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \MedicalHistory Clone of current object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function copy(bool $deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildUser object.
     *
     * @param ChildUser $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setUser(ChildUser $v = null)
    {
        if ($v === null) {
            $this->setUserId(NULL);
        } else {
            $this->setUserId($v->getId());
        }

        $this->aUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUser object, it will not be re-added.
        if ($v !== null) {
            $v->addMedicalHistory($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUser object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildUser The associated ChildUser object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getUser(?ConnectionInterface $con = null)
    {
        if ($this->aUser === null && ($this->user_id != 0)) {
            $this->aUser = ChildUserQuery::create()->findPk($this->user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUser->addMedicalHistories($this);
             */
        }

        return $this->aUser;
    }

    /**
     * Declares an association between this object and a ChildPatient object.
     *
     * @param ChildPatient $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setPatient(ChildPatient $v = null)
    {
        if ($v === null) {
            $this->setPatientUuid(NULL);
        } else {
            $this->setPatientUuid($v->getUuid());
        }

        $this->aPatient = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPatient object, it will not be re-added.
        if ($v !== null) {
            $v->addMedicalHistory($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPatient object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildPatient The associated ChildPatient object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getPatient(?ConnectionInterface $con = null)
    {
        if ($this->aPatient === null && (($this->patient_uuid !== "" && $this->patient_uuid !== null))) {
            $this->aPatient = ChildPatientQuery::create()->findPk($this->patient_uuid, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPatient->addMedicalHistories($this);
             */
        }

        return $this->aPatient;
    }

    /**
     * Declares an association between this object and a ChildMedicalDiagnosisCode object.
     *
     * @param ChildMedicalDiagnosisCode|null $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setMedicalDiagnosisCode(ChildMedicalDiagnosisCode $v = null)
    {
        if ($v === null) {
            $this->setMedicalDiagnosisCodeId(NULL);
        } else {
            $this->setMedicalDiagnosisCodeId($v->getId());
        }

        $this->aMedicalDiagnosisCode = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMedicalDiagnosisCode object, it will not be re-added.
        if ($v !== null) {
            $v->addMedicalHistory($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMedicalDiagnosisCode object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildMedicalDiagnosisCode|null The associated ChildMedicalDiagnosisCode object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getMedicalDiagnosisCode(?ConnectionInterface $con = null)
    {
        if ($this->aMedicalDiagnosisCode === null && (($this->medical_diagnosis_code_id !== "" && $this->medical_diagnosis_code_id !== null))) {
            $this->aMedicalDiagnosisCode = ChildMedicalDiagnosisCodeQuery::create()->findPk($this->medical_diagnosis_code_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMedicalDiagnosisCode->addMedicalHistories($this);
             */
        }

        return $this->aMedicalDiagnosisCode;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     *
     * @return $this
     */
    public function clear()
    {
        if (null !== $this->aUser) {
            $this->aUser->removeMedicalHistory($this);
        }
        if (null !== $this->aPatient) {
            $this->aPatient->removeMedicalHistory($this);
        }
        if (null !== $this->aMedicalDiagnosisCode) {
            $this->aMedicalDiagnosisCode->removeMedicalHistory($this);
        }
        $this->uuid = null;
        $this->patient_uuid = null;
        $this->user_id = null;
        $this->clinical_status = null;
        $this->verification_status = null;
        $this->severity = null;
        $this->note = null;
        $this->medical_diagnosis_code_id = null;
        $this->onset_date = null;
        $this->onset_age = null;
        $this->abatement_date = null;
        $this->abatement_age = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);

        return $this;
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param bool $deep Whether to also clear the references on all referrer objects.
     * @return $this
     */
    public function clearAllReferences(bool $deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aUser = null;
        $this->aPatient = null;
        $this->aMedicalDiagnosisCode = null;
        return $this;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MedicalHistoryTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return $this The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[MedicalHistoryTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preSave(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postSave(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before inserting to database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preInsert(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postInsert(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preUpdate(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postUpdate(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preDelete(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postDelete(?ConnectionInterface $con = null): void
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
