<?php

namespace Base;

use \PhysicalExamination as ChildPhysicalExamination;
use \PhysicalExaminationQuery as ChildPhysicalExaminationQuery;
use \Exception;
use \PDO;
use Map\PhysicalExaminationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'physical_examination' table.
 *
 * Contains physical examination history of the respective patient
 *
 * @method     ChildPhysicalExaminationQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildPhysicalExaminationQuery orderByTimestamp($order = Criteria::ASC) Order by the timestamp column
 * @method     ChildPhysicalExaminationQuery orderByBloodPressureSystolic($order = Criteria::ASC) Order by the blood_pressure_systolic column
 * @method     ChildPhysicalExaminationQuery orderByBloodPressureDiastolic($order = Criteria::ASC) Order by the blood_pressure_diastolic column
 * @method     ChildPhysicalExaminationQuery orderByHeartRate($order = Criteria::ASC) Order by the heart_rate column
 * @method     ChildPhysicalExaminationQuery orderByWeight($order = Criteria::ASC) Order by the weight column
 * @method     ChildPhysicalExaminationQuery orderByHeight($order = Criteria::ASC) Order by the height column
 * @method     ChildPhysicalExaminationQuery orderByMuac($order = Criteria::ASC) Order by the muac column
 * @method     ChildPhysicalExaminationQuery orderByTemperature($order = Criteria::ASC) Order by the temperature column
 * @method     ChildPhysicalExaminationQuery orderBySkinExamination($order = Criteria::ASC) Order by the skin_examination column
 * @method     ChildPhysicalExaminationQuery orderByDetailsEnt($order = Criteria::ASC) Order by the details_ent column
 * @method     ChildPhysicalExaminationQuery orderByDetailsEyes($order = Criteria::ASC) Order by the details_eyes column
 * @method     ChildPhysicalExaminationQuery orderByDetailsHead($order = Criteria::ASC) Order by the details_head column
 * @method     ChildPhysicalExaminationQuery orderByDetailsMusclesBones($order = Criteria::ASC) Order by the details_muscles_bones column
 * @method     ChildPhysicalExaminationQuery orderByDetailsHeart($order = Criteria::ASC) Order by the details_heart column
 * @method     ChildPhysicalExaminationQuery orderByDetailsLung($order = Criteria::ASC) Order by the details_lung column
 * @method     ChildPhysicalExaminationQuery orderByDetailsGastrointestinal($order = Criteria::ASC) Order by the details_gastrointestinal column
 * @method     ChildPhysicalExaminationQuery orderByDetailsUrinaryTract($order = Criteria::ASC) Order by the details_urinary_tract column
 * @method     ChildPhysicalExaminationQuery orderByDetailsReproductiveSystem($order = Criteria::ASC) Order by the details_reproductive_system column
 * @method     ChildPhysicalExaminationQuery orderByDetailsOther($order = Criteria::ASC) Order by the details_other column
 * @method     ChildPhysicalExaminationQuery orderByGeneralImpression($order = Criteria::ASC) Order by the general_impression column
 * @method     ChildPhysicalExaminationQuery orderByPatientUuid($order = Criteria::ASC) Order by the patient_uuid column
 * @method     ChildPhysicalExaminationQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildPhysicalExaminationQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPhysicalExaminationQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPhysicalExaminationQuery groupByUuid() Group by the uuid column
 * @method     ChildPhysicalExaminationQuery groupByTimestamp() Group by the timestamp column
 * @method     ChildPhysicalExaminationQuery groupByBloodPressureSystolic() Group by the blood_pressure_systolic column
 * @method     ChildPhysicalExaminationQuery groupByBloodPressureDiastolic() Group by the blood_pressure_diastolic column
 * @method     ChildPhysicalExaminationQuery groupByHeartRate() Group by the heart_rate column
 * @method     ChildPhysicalExaminationQuery groupByWeight() Group by the weight column
 * @method     ChildPhysicalExaminationQuery groupByHeight() Group by the height column
 * @method     ChildPhysicalExaminationQuery groupByMuac() Group by the muac column
 * @method     ChildPhysicalExaminationQuery groupByTemperature() Group by the temperature column
 * @method     ChildPhysicalExaminationQuery groupBySkinExamination() Group by the skin_examination column
 * @method     ChildPhysicalExaminationQuery groupByDetailsEnt() Group by the details_ent column
 * @method     ChildPhysicalExaminationQuery groupByDetailsEyes() Group by the details_eyes column
 * @method     ChildPhysicalExaminationQuery groupByDetailsHead() Group by the details_head column
 * @method     ChildPhysicalExaminationQuery groupByDetailsMusclesBones() Group by the details_muscles_bones column
 * @method     ChildPhysicalExaminationQuery groupByDetailsHeart() Group by the details_heart column
 * @method     ChildPhysicalExaminationQuery groupByDetailsLung() Group by the details_lung column
 * @method     ChildPhysicalExaminationQuery groupByDetailsGastrointestinal() Group by the details_gastrointestinal column
 * @method     ChildPhysicalExaminationQuery groupByDetailsUrinaryTract() Group by the details_urinary_tract column
 * @method     ChildPhysicalExaminationQuery groupByDetailsReproductiveSystem() Group by the details_reproductive_system column
 * @method     ChildPhysicalExaminationQuery groupByDetailsOther() Group by the details_other column
 * @method     ChildPhysicalExaminationQuery groupByGeneralImpression() Group by the general_impression column
 * @method     ChildPhysicalExaminationQuery groupByPatientUuid() Group by the patient_uuid column
 * @method     ChildPhysicalExaminationQuery groupByUserId() Group by the user_id column
 * @method     ChildPhysicalExaminationQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPhysicalExaminationQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPhysicalExaminationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPhysicalExaminationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPhysicalExaminationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPhysicalExaminationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPhysicalExaminationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPhysicalExaminationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPhysicalExaminationQuery leftJoinPatient($relationAlias = null) Adds a LEFT JOIN clause to the query using the Patient relation
 * @method     ChildPhysicalExaminationQuery rightJoinPatient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Patient relation
 * @method     ChildPhysicalExaminationQuery innerJoinPatient($relationAlias = null) Adds a INNER JOIN clause to the query using the Patient relation
 *
 * @method     ChildPhysicalExaminationQuery joinWithPatient($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Patient relation
 *
 * @method     ChildPhysicalExaminationQuery leftJoinWithPatient() Adds a LEFT JOIN clause and with to the query using the Patient relation
 * @method     ChildPhysicalExaminationQuery rightJoinWithPatient() Adds a RIGHT JOIN clause and with to the query using the Patient relation
 * @method     ChildPhysicalExaminationQuery innerJoinWithPatient() Adds a INNER JOIN clause and with to the query using the Patient relation
 *
 * @method     ChildPhysicalExaminationQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildPhysicalExaminationQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildPhysicalExaminationQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildPhysicalExaminationQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildPhysicalExaminationQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildPhysicalExaminationQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildPhysicalExaminationQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     \PatientQuery|\UserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPhysicalExamination|null findOne(?ConnectionInterface $con = null) Return the first ChildPhysicalExamination matching the query
 * @method     ChildPhysicalExamination findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildPhysicalExamination matching the query, or a new ChildPhysicalExamination object populated from the query conditions when no match is found
 *
 * @method     ChildPhysicalExamination|null findOneByUuid(string $uuid) Return the first ChildPhysicalExamination filtered by the uuid column
 * @method     ChildPhysicalExamination|null findOneByTimestamp(string $timestamp) Return the first ChildPhysicalExamination filtered by the timestamp column
 * @method     ChildPhysicalExamination|null findOneByBloodPressureSystolic(int $blood_pressure_systolic) Return the first ChildPhysicalExamination filtered by the blood_pressure_systolic column
 * @method     ChildPhysicalExamination|null findOneByBloodPressureDiastolic(int $blood_pressure_diastolic) Return the first ChildPhysicalExamination filtered by the blood_pressure_diastolic column
 * @method     ChildPhysicalExamination|null findOneByHeartRate(int $heart_rate) Return the first ChildPhysicalExamination filtered by the heart_rate column
 * @method     ChildPhysicalExamination|null findOneByWeight(string $weight) Return the first ChildPhysicalExamination filtered by the weight column
 * @method     ChildPhysicalExamination|null findOneByHeight(int $height) Return the first ChildPhysicalExamination filtered by the height column
 * @method     ChildPhysicalExamination|null findOneByMuac(string $muac) Return the first ChildPhysicalExamination filtered by the muac column
 * @method     ChildPhysicalExamination|null findOneByTemperature(string $temperature) Return the first ChildPhysicalExamination filtered by the temperature column
 * @method     ChildPhysicalExamination|null findOneBySkinExamination(string $skin_examination) Return the first ChildPhysicalExamination filtered by the skin_examination column
 * @method     ChildPhysicalExamination|null findOneByDetailsEnt(string $details_ent) Return the first ChildPhysicalExamination filtered by the details_ent column
 * @method     ChildPhysicalExamination|null findOneByDetailsEyes(string $details_eyes) Return the first ChildPhysicalExamination filtered by the details_eyes column
 * @method     ChildPhysicalExamination|null findOneByDetailsHead(string $details_head) Return the first ChildPhysicalExamination filtered by the details_head column
 * @method     ChildPhysicalExamination|null findOneByDetailsMusclesBones(string $details_muscles_bones) Return the first ChildPhysicalExamination filtered by the details_muscles_bones column
 * @method     ChildPhysicalExamination|null findOneByDetailsHeart(string $details_heart) Return the first ChildPhysicalExamination filtered by the details_heart column
 * @method     ChildPhysicalExamination|null findOneByDetailsLung(string $details_lung) Return the first ChildPhysicalExamination filtered by the details_lung column
 * @method     ChildPhysicalExamination|null findOneByDetailsGastrointestinal(string $details_gastrointestinal) Return the first ChildPhysicalExamination filtered by the details_gastrointestinal column
 * @method     ChildPhysicalExamination|null findOneByDetailsUrinaryTract(string $details_urinary_tract) Return the first ChildPhysicalExamination filtered by the details_urinary_tract column
 * @method     ChildPhysicalExamination|null findOneByDetailsReproductiveSystem(string $details_reproductive_system) Return the first ChildPhysicalExamination filtered by the details_reproductive_system column
 * @method     ChildPhysicalExamination|null findOneByDetailsOther(string $details_other) Return the first ChildPhysicalExamination filtered by the details_other column
 * @method     ChildPhysicalExamination|null findOneByGeneralImpression(int $general_impression) Return the first ChildPhysicalExamination filtered by the general_impression column
 * @method     ChildPhysicalExamination|null findOneByPatientUuid(string $patient_uuid) Return the first ChildPhysicalExamination filtered by the patient_uuid column
 * @method     ChildPhysicalExamination|null findOneByUserId(int $user_id) Return the first ChildPhysicalExamination filtered by the user_id column
 * @method     ChildPhysicalExamination|null findOneByCreatedAt(string $created_at) Return the first ChildPhysicalExamination filtered by the created_at column
 * @method     ChildPhysicalExamination|null findOneByUpdatedAt(string $updated_at) Return the first ChildPhysicalExamination filtered by the updated_at column *

 * @method     ChildPhysicalExamination requirePk($key, ?ConnectionInterface $con = null) Return the ChildPhysicalExamination by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOne(?ConnectionInterface $con = null) Return the first ChildPhysicalExamination matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPhysicalExamination requireOneByUuid(string $uuid) Return the first ChildPhysicalExamination filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByTimestamp(string $timestamp) Return the first ChildPhysicalExamination filtered by the timestamp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByBloodPressureSystolic(int $blood_pressure_systolic) Return the first ChildPhysicalExamination filtered by the blood_pressure_systolic column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByBloodPressureDiastolic(int $blood_pressure_diastolic) Return the first ChildPhysicalExamination filtered by the blood_pressure_diastolic column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByHeartRate(int $heart_rate) Return the first ChildPhysicalExamination filtered by the heart_rate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByWeight(string $weight) Return the first ChildPhysicalExamination filtered by the weight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByHeight(int $height) Return the first ChildPhysicalExamination filtered by the height column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByMuac(string $muac) Return the first ChildPhysicalExamination filtered by the muac column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByTemperature(string $temperature) Return the first ChildPhysicalExamination filtered by the temperature column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneBySkinExamination(string $skin_examination) Return the first ChildPhysicalExamination filtered by the skin_examination column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByDetailsEnt(string $details_ent) Return the first ChildPhysicalExamination filtered by the details_ent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByDetailsEyes(string $details_eyes) Return the first ChildPhysicalExamination filtered by the details_eyes column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByDetailsHead(string $details_head) Return the first ChildPhysicalExamination filtered by the details_head column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByDetailsMusclesBones(string $details_muscles_bones) Return the first ChildPhysicalExamination filtered by the details_muscles_bones column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByDetailsHeart(string $details_heart) Return the first ChildPhysicalExamination filtered by the details_heart column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByDetailsLung(string $details_lung) Return the first ChildPhysicalExamination filtered by the details_lung column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByDetailsGastrointestinal(string $details_gastrointestinal) Return the first ChildPhysicalExamination filtered by the details_gastrointestinal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByDetailsUrinaryTract(string $details_urinary_tract) Return the first ChildPhysicalExamination filtered by the details_urinary_tract column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByDetailsReproductiveSystem(string $details_reproductive_system) Return the first ChildPhysicalExamination filtered by the details_reproductive_system column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByDetailsOther(string $details_other) Return the first ChildPhysicalExamination filtered by the details_other column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByGeneralImpression(int $general_impression) Return the first ChildPhysicalExamination filtered by the general_impression column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByPatientUuid(string $patient_uuid) Return the first ChildPhysicalExamination filtered by the patient_uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByUserId(int $user_id) Return the first ChildPhysicalExamination filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByCreatedAt(string $created_at) Return the first ChildPhysicalExamination filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPhysicalExamination requireOneByUpdatedAt(string $updated_at) Return the first ChildPhysicalExamination filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPhysicalExamination[]|Collection find(?ConnectionInterface $con = null) Return ChildPhysicalExamination objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> find(?ConnectionInterface $con = null) Return ChildPhysicalExamination objects based on current ModelCriteria
 * @method     ChildPhysicalExamination[]|Collection findByUuid(string $uuid) Return ChildPhysicalExamination objects filtered by the uuid column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByUuid(string $uuid) Return ChildPhysicalExamination objects filtered by the uuid column
 * @method     ChildPhysicalExamination[]|Collection findByTimestamp(string $timestamp) Return ChildPhysicalExamination objects filtered by the timestamp column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByTimestamp(string $timestamp) Return ChildPhysicalExamination objects filtered by the timestamp column
 * @method     ChildPhysicalExamination[]|Collection findByBloodPressureSystolic(int $blood_pressure_systolic) Return ChildPhysicalExamination objects filtered by the blood_pressure_systolic column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByBloodPressureSystolic(int $blood_pressure_systolic) Return ChildPhysicalExamination objects filtered by the blood_pressure_systolic column
 * @method     ChildPhysicalExamination[]|Collection findByBloodPressureDiastolic(int $blood_pressure_diastolic) Return ChildPhysicalExamination objects filtered by the blood_pressure_diastolic column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByBloodPressureDiastolic(int $blood_pressure_diastolic) Return ChildPhysicalExamination objects filtered by the blood_pressure_diastolic column
 * @method     ChildPhysicalExamination[]|Collection findByHeartRate(int $heart_rate) Return ChildPhysicalExamination objects filtered by the heart_rate column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByHeartRate(int $heart_rate) Return ChildPhysicalExamination objects filtered by the heart_rate column
 * @method     ChildPhysicalExamination[]|Collection findByWeight(string $weight) Return ChildPhysicalExamination objects filtered by the weight column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByWeight(string $weight) Return ChildPhysicalExamination objects filtered by the weight column
 * @method     ChildPhysicalExamination[]|Collection findByHeight(int $height) Return ChildPhysicalExamination objects filtered by the height column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByHeight(int $height) Return ChildPhysicalExamination objects filtered by the height column
 * @method     ChildPhysicalExamination[]|Collection findByMuac(string $muac) Return ChildPhysicalExamination objects filtered by the muac column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByMuac(string $muac) Return ChildPhysicalExamination objects filtered by the muac column
 * @method     ChildPhysicalExamination[]|Collection findByTemperature(string $temperature) Return ChildPhysicalExamination objects filtered by the temperature column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByTemperature(string $temperature) Return ChildPhysicalExamination objects filtered by the temperature column
 * @method     ChildPhysicalExamination[]|Collection findBySkinExamination(string $skin_examination) Return ChildPhysicalExamination objects filtered by the skin_examination column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findBySkinExamination(string $skin_examination) Return ChildPhysicalExamination objects filtered by the skin_examination column
 * @method     ChildPhysicalExamination[]|Collection findByDetailsEnt(string $details_ent) Return ChildPhysicalExamination objects filtered by the details_ent column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByDetailsEnt(string $details_ent) Return ChildPhysicalExamination objects filtered by the details_ent column
 * @method     ChildPhysicalExamination[]|Collection findByDetailsEyes(string $details_eyes) Return ChildPhysicalExamination objects filtered by the details_eyes column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByDetailsEyes(string $details_eyes) Return ChildPhysicalExamination objects filtered by the details_eyes column
 * @method     ChildPhysicalExamination[]|Collection findByDetailsHead(string $details_head) Return ChildPhysicalExamination objects filtered by the details_head column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByDetailsHead(string $details_head) Return ChildPhysicalExamination objects filtered by the details_head column
 * @method     ChildPhysicalExamination[]|Collection findByDetailsMusclesBones(string $details_muscles_bones) Return ChildPhysicalExamination objects filtered by the details_muscles_bones column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByDetailsMusclesBones(string $details_muscles_bones) Return ChildPhysicalExamination objects filtered by the details_muscles_bones column
 * @method     ChildPhysicalExamination[]|Collection findByDetailsHeart(string $details_heart) Return ChildPhysicalExamination objects filtered by the details_heart column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByDetailsHeart(string $details_heart) Return ChildPhysicalExamination objects filtered by the details_heart column
 * @method     ChildPhysicalExamination[]|Collection findByDetailsLung(string $details_lung) Return ChildPhysicalExamination objects filtered by the details_lung column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByDetailsLung(string $details_lung) Return ChildPhysicalExamination objects filtered by the details_lung column
 * @method     ChildPhysicalExamination[]|Collection findByDetailsGastrointestinal(string $details_gastrointestinal) Return ChildPhysicalExamination objects filtered by the details_gastrointestinal column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByDetailsGastrointestinal(string $details_gastrointestinal) Return ChildPhysicalExamination objects filtered by the details_gastrointestinal column
 * @method     ChildPhysicalExamination[]|Collection findByDetailsUrinaryTract(string $details_urinary_tract) Return ChildPhysicalExamination objects filtered by the details_urinary_tract column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByDetailsUrinaryTract(string $details_urinary_tract) Return ChildPhysicalExamination objects filtered by the details_urinary_tract column
 * @method     ChildPhysicalExamination[]|Collection findByDetailsReproductiveSystem(string $details_reproductive_system) Return ChildPhysicalExamination objects filtered by the details_reproductive_system column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByDetailsReproductiveSystem(string $details_reproductive_system) Return ChildPhysicalExamination objects filtered by the details_reproductive_system column
 * @method     ChildPhysicalExamination[]|Collection findByDetailsOther(string $details_other) Return ChildPhysicalExamination objects filtered by the details_other column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByDetailsOther(string $details_other) Return ChildPhysicalExamination objects filtered by the details_other column
 * @method     ChildPhysicalExamination[]|Collection findByGeneralImpression(int $general_impression) Return ChildPhysicalExamination objects filtered by the general_impression column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByGeneralImpression(int $general_impression) Return ChildPhysicalExamination objects filtered by the general_impression column
 * @method     ChildPhysicalExamination[]|Collection findByPatientUuid(string $patient_uuid) Return ChildPhysicalExamination objects filtered by the patient_uuid column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByPatientUuid(string $patient_uuid) Return ChildPhysicalExamination objects filtered by the patient_uuid column
 * @method     ChildPhysicalExamination[]|Collection findByUserId(int $user_id) Return ChildPhysicalExamination objects filtered by the user_id column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByUserId(int $user_id) Return ChildPhysicalExamination objects filtered by the user_id column
 * @method     ChildPhysicalExamination[]|Collection findByCreatedAt(string $created_at) Return ChildPhysicalExamination objects filtered by the created_at column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByCreatedAt(string $created_at) Return ChildPhysicalExamination objects filtered by the created_at column
 * @method     ChildPhysicalExamination[]|Collection findByUpdatedAt(string $updated_at) Return ChildPhysicalExamination objects filtered by the updated_at column
 * @psalm-method Collection&\Traversable<ChildPhysicalExamination> findByUpdatedAt(string $updated_at) Return ChildPhysicalExamination objects filtered by the updated_at column
 * @method     ChildPhysicalExamination[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildPhysicalExamination> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PhysicalExaminationQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PhysicalExaminationQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\PhysicalExamination', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPhysicalExaminationQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPhysicalExaminationQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildPhysicalExaminationQuery) {
            return $criteria;
        }
        $query = new ChildPhysicalExaminationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPhysicalExamination|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PhysicalExaminationTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PhysicalExaminationTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPhysicalExamination A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT uuid, timestamp, blood_pressure_systolic, blood_pressure_diastolic, heart_rate, weight, height, muac, temperature, skin_examination, details_ent, details_eyes, details_head, details_muscles_bones, details_heart, details_lung, details_gastrointestinal, details_urinary_tract, details_reproductive_system, details_other, general_impression, patient_uuid, user_id, created_at, updated_at FROM physical_examination WHERE uuid = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPhysicalExamination $obj */
            $obj = new ChildPhysicalExamination();
            $obj->hydrate($row);
            PhysicalExaminationTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildPhysicalExamination|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_UUID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_UUID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * $query->filterByUuid(['foo', 'bar']); // WHERE uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $uuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_UUID, $uuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the timestamp column
     *
     * Example usage:
     * <code>
     * $query->filterByTimestamp('2011-03-14'); // WHERE timestamp = '2011-03-14'
     * $query->filterByTimestamp('now'); // WHERE timestamp = '2011-03-14'
     * $query->filterByTimestamp(array('max' => 'yesterday')); // WHERE timestamp > '2011-03-13'
     * </code>
     *
     * @param mixed $timestamp The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByTimestamp($timestamp = null, ?string $comparison = null)
    {
        if (is_array($timestamp)) {
            $useMinMax = false;
            if (isset($timestamp['min'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_TIMESTAMP, $timestamp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($timestamp['max'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_TIMESTAMP, $timestamp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_TIMESTAMP, $timestamp, $comparison);

        return $this;
    }

    /**
     * Filter the query on the blood_pressure_systolic column
     *
     * Example usage:
     * <code>
     * $query->filterByBloodPressureSystolic(1234); // WHERE blood_pressure_systolic = 1234
     * $query->filterByBloodPressureSystolic(array(12, 34)); // WHERE blood_pressure_systolic IN (12, 34)
     * $query->filterByBloodPressureSystolic(array('min' => 12)); // WHERE blood_pressure_systolic > 12
     * </code>
     *
     * @param mixed $bloodPressureSystolic The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByBloodPressureSystolic($bloodPressureSystolic = null, ?string $comparison = null)
    {
        if (is_array($bloodPressureSystolic)) {
            $useMinMax = false;
            if (isset($bloodPressureSystolic['min'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_SYSTOLIC, $bloodPressureSystolic['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bloodPressureSystolic['max'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_SYSTOLIC, $bloodPressureSystolic['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_SYSTOLIC, $bloodPressureSystolic, $comparison);

        return $this;
    }

    /**
     * Filter the query on the blood_pressure_diastolic column
     *
     * Example usage:
     * <code>
     * $query->filterByBloodPressureDiastolic(1234); // WHERE blood_pressure_diastolic = 1234
     * $query->filterByBloodPressureDiastolic(array(12, 34)); // WHERE blood_pressure_diastolic IN (12, 34)
     * $query->filterByBloodPressureDiastolic(array('min' => 12)); // WHERE blood_pressure_diastolic > 12
     * </code>
     *
     * @param mixed $bloodPressureDiastolic The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByBloodPressureDiastolic($bloodPressureDiastolic = null, ?string $comparison = null)
    {
        if (is_array($bloodPressureDiastolic)) {
            $useMinMax = false;
            if (isset($bloodPressureDiastolic['min'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_DIASTOLIC, $bloodPressureDiastolic['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bloodPressureDiastolic['max'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_DIASTOLIC, $bloodPressureDiastolic['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_DIASTOLIC, $bloodPressureDiastolic, $comparison);

        return $this;
    }

    /**
     * Filter the query on the heart_rate column
     *
     * Example usage:
     * <code>
     * $query->filterByHeartRate(1234); // WHERE heart_rate = 1234
     * $query->filterByHeartRate(array(12, 34)); // WHERE heart_rate IN (12, 34)
     * $query->filterByHeartRate(array('min' => 12)); // WHERE heart_rate > 12
     * </code>
     *
     * @param mixed $heartRate The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByHeartRate($heartRate = null, ?string $comparison = null)
    {
        if (is_array($heartRate)) {
            $useMinMax = false;
            if (isset($heartRate['min'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_HEART_RATE, $heartRate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($heartRate['max'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_HEART_RATE, $heartRate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_HEART_RATE, $heartRate, $comparison);

        return $this;
    }

    /**
     * Filter the query on the weight column
     *
     * Example usage:
     * <code>
     * $query->filterByWeight(1234); // WHERE weight = 1234
     * $query->filterByWeight(array(12, 34)); // WHERE weight IN (12, 34)
     * $query->filterByWeight(array('min' => 12)); // WHERE weight > 12
     * </code>
     *
     * @param mixed $weight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByWeight($weight = null, ?string $comparison = null)
    {
        if (is_array($weight)) {
            $useMinMax = false;
            if (isset($weight['min'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_WEIGHT, $weight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($weight['max'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_WEIGHT, $weight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_WEIGHT, $weight, $comparison);

        return $this;
    }

    /**
     * Filter the query on the height column
     *
     * Example usage:
     * <code>
     * $query->filterByHeight(1234); // WHERE height = 1234
     * $query->filterByHeight(array(12, 34)); // WHERE height IN (12, 34)
     * $query->filterByHeight(array('min' => 12)); // WHERE height > 12
     * </code>
     *
     * @param mixed $height The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByHeight($height = null, ?string $comparison = null)
    {
        if (is_array($height)) {
            $useMinMax = false;
            if (isset($height['min'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_HEIGHT, $height['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($height['max'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_HEIGHT, $height['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_HEIGHT, $height, $comparison);

        return $this;
    }

    /**
     * Filter the query on the muac column
     *
     * Example usage:
     * <code>
     * $query->filterByMuac(1234); // WHERE muac = 1234
     * $query->filterByMuac(array(12, 34)); // WHERE muac IN (12, 34)
     * $query->filterByMuac(array('min' => 12)); // WHERE muac > 12
     * </code>
     *
     * @param mixed $muac The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMuac($muac = null, ?string $comparison = null)
    {
        if (is_array($muac)) {
            $useMinMax = false;
            if (isset($muac['min'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_MUAC, $muac['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($muac['max'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_MUAC, $muac['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_MUAC, $muac, $comparison);

        return $this;
    }

    /**
     * Filter the query on the temperature column
     *
     * Example usage:
     * <code>
     * $query->filterByTemperature(1234); // WHERE temperature = 1234
     * $query->filterByTemperature(array(12, 34)); // WHERE temperature IN (12, 34)
     * $query->filterByTemperature(array('min' => 12)); // WHERE temperature > 12
     * </code>
     *
     * @param mixed $temperature The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByTemperature($temperature = null, ?string $comparison = null)
    {
        if (is_array($temperature)) {
            $useMinMax = false;
            if (isset($temperature['min'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_TEMPERATURE, $temperature['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($temperature['max'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_TEMPERATURE, $temperature['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_TEMPERATURE, $temperature, $comparison);

        return $this;
    }

    /**
     * Filter the query on the skin_examination column
     *
     * Example usage:
     * <code>
     * $query->filterBySkinExamination('fooValue');   // WHERE skin_examination = 'fooValue'
     * $query->filterBySkinExamination('%fooValue%', Criteria::LIKE); // WHERE skin_examination LIKE '%fooValue%'
     * $query->filterBySkinExamination(['foo', 'bar']); // WHERE skin_examination IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $skinExamination The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterBySkinExamination($skinExamination = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($skinExamination)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_SKIN_EXAMINATION, $skinExamination, $comparison);

        return $this;
    }

    /**
     * Filter the query on the details_ent column
     *
     * Example usage:
     * <code>
     * $query->filterByDetailsEnt('fooValue');   // WHERE details_ent = 'fooValue'
     * $query->filterByDetailsEnt('%fooValue%', Criteria::LIKE); // WHERE details_ent LIKE '%fooValue%'
     * $query->filterByDetailsEnt(['foo', 'bar']); // WHERE details_ent IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $detailsEnt The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDetailsEnt($detailsEnt = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($detailsEnt)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_DETAILS_ENT, $detailsEnt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the details_eyes column
     *
     * Example usage:
     * <code>
     * $query->filterByDetailsEyes('fooValue');   // WHERE details_eyes = 'fooValue'
     * $query->filterByDetailsEyes('%fooValue%', Criteria::LIKE); // WHERE details_eyes LIKE '%fooValue%'
     * $query->filterByDetailsEyes(['foo', 'bar']); // WHERE details_eyes IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $detailsEyes The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDetailsEyes($detailsEyes = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($detailsEyes)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_DETAILS_EYES, $detailsEyes, $comparison);

        return $this;
    }

    /**
     * Filter the query on the details_head column
     *
     * Example usage:
     * <code>
     * $query->filterByDetailsHead('fooValue');   // WHERE details_head = 'fooValue'
     * $query->filterByDetailsHead('%fooValue%', Criteria::LIKE); // WHERE details_head LIKE '%fooValue%'
     * $query->filterByDetailsHead(['foo', 'bar']); // WHERE details_head IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $detailsHead The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDetailsHead($detailsHead = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($detailsHead)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_DETAILS_HEAD, $detailsHead, $comparison);

        return $this;
    }

    /**
     * Filter the query on the details_muscles_bones column
     *
     * Example usage:
     * <code>
     * $query->filterByDetailsMusclesBones('fooValue');   // WHERE details_muscles_bones = 'fooValue'
     * $query->filterByDetailsMusclesBones('%fooValue%', Criteria::LIKE); // WHERE details_muscles_bones LIKE '%fooValue%'
     * $query->filterByDetailsMusclesBones(['foo', 'bar']); // WHERE details_muscles_bones IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $detailsMusclesBones The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDetailsMusclesBones($detailsMusclesBones = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($detailsMusclesBones)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_DETAILS_MUSCLES_BONES, $detailsMusclesBones, $comparison);

        return $this;
    }

    /**
     * Filter the query on the details_heart column
     *
     * Example usage:
     * <code>
     * $query->filterByDetailsHeart('fooValue');   // WHERE details_heart = 'fooValue'
     * $query->filterByDetailsHeart('%fooValue%', Criteria::LIKE); // WHERE details_heart LIKE '%fooValue%'
     * $query->filterByDetailsHeart(['foo', 'bar']); // WHERE details_heart IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $detailsHeart The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDetailsHeart($detailsHeart = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($detailsHeart)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_DETAILS_HEART, $detailsHeart, $comparison);

        return $this;
    }

    /**
     * Filter the query on the details_lung column
     *
     * Example usage:
     * <code>
     * $query->filterByDetailsLung('fooValue');   // WHERE details_lung = 'fooValue'
     * $query->filterByDetailsLung('%fooValue%', Criteria::LIKE); // WHERE details_lung LIKE '%fooValue%'
     * $query->filterByDetailsLung(['foo', 'bar']); // WHERE details_lung IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $detailsLung The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDetailsLung($detailsLung = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($detailsLung)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_DETAILS_LUNG, $detailsLung, $comparison);

        return $this;
    }

    /**
     * Filter the query on the details_gastrointestinal column
     *
     * Example usage:
     * <code>
     * $query->filterByDetailsGastrointestinal('fooValue');   // WHERE details_gastrointestinal = 'fooValue'
     * $query->filterByDetailsGastrointestinal('%fooValue%', Criteria::LIKE); // WHERE details_gastrointestinal LIKE '%fooValue%'
     * $query->filterByDetailsGastrointestinal(['foo', 'bar']); // WHERE details_gastrointestinal IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $detailsGastrointestinal The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDetailsGastrointestinal($detailsGastrointestinal = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($detailsGastrointestinal)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_DETAILS_GASTROINTESTINAL, $detailsGastrointestinal, $comparison);

        return $this;
    }

    /**
     * Filter the query on the details_urinary_tract column
     *
     * Example usage:
     * <code>
     * $query->filterByDetailsUrinaryTract('fooValue');   // WHERE details_urinary_tract = 'fooValue'
     * $query->filterByDetailsUrinaryTract('%fooValue%', Criteria::LIKE); // WHERE details_urinary_tract LIKE '%fooValue%'
     * $query->filterByDetailsUrinaryTract(['foo', 'bar']); // WHERE details_urinary_tract IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $detailsUrinaryTract The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDetailsUrinaryTract($detailsUrinaryTract = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($detailsUrinaryTract)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_DETAILS_URINARY_TRACT, $detailsUrinaryTract, $comparison);

        return $this;
    }

    /**
     * Filter the query on the details_reproductive_system column
     *
     * Example usage:
     * <code>
     * $query->filterByDetailsReproductiveSystem('fooValue');   // WHERE details_reproductive_system = 'fooValue'
     * $query->filterByDetailsReproductiveSystem('%fooValue%', Criteria::LIKE); // WHERE details_reproductive_system LIKE '%fooValue%'
     * $query->filterByDetailsReproductiveSystem(['foo', 'bar']); // WHERE details_reproductive_system IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $detailsReproductiveSystem The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDetailsReproductiveSystem($detailsReproductiveSystem = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($detailsReproductiveSystem)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_DETAILS_REPRODUCTIVE_SYSTEM, $detailsReproductiveSystem, $comparison);

        return $this;
    }

    /**
     * Filter the query on the details_other column
     *
     * Example usage:
     * <code>
     * $query->filterByDetailsOther('fooValue');   // WHERE details_other = 'fooValue'
     * $query->filterByDetailsOther('%fooValue%', Criteria::LIKE); // WHERE details_other LIKE '%fooValue%'
     * $query->filterByDetailsOther(['foo', 'bar']); // WHERE details_other IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $detailsOther The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDetailsOther($detailsOther = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($detailsOther)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_DETAILS_OTHER, $detailsOther, $comparison);

        return $this;
    }

    /**
     * Filter the query on the general_impression column
     *
     * Example usage:
     * <code>
     * $query->filterByGeneralImpression(1234); // WHERE general_impression = 1234
     * $query->filterByGeneralImpression(array(12, 34)); // WHERE general_impression IN (12, 34)
     * $query->filterByGeneralImpression(array('min' => 12)); // WHERE general_impression > 12
     * </code>
     *
     * @param mixed $generalImpression The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByGeneralImpression($generalImpression = null, ?string $comparison = null)
    {
        if (is_array($generalImpression)) {
            $useMinMax = false;
            if (isset($generalImpression['min'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_GENERAL_IMPRESSION, $generalImpression['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($generalImpression['max'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_GENERAL_IMPRESSION, $generalImpression['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_GENERAL_IMPRESSION, $generalImpression, $comparison);

        return $this;
    }

    /**
     * Filter the query on the patient_uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByPatientUuid('fooValue');   // WHERE patient_uuid = 'fooValue'
     * $query->filterByPatientUuid('%fooValue%', Criteria::LIKE); // WHERE patient_uuid LIKE '%fooValue%'
     * $query->filterByPatientUuid(['foo', 'bar']); // WHERE patient_uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $patientUuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPatientUuid($patientUuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($patientUuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_PATIENT_UUID, $patientUuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUserId($userId = null, ?string $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_USER_ID, $userId, $comparison);

        return $this;
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, ?string $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_CREATED_AT, $createdAt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, ?string $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PhysicalExaminationTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PhysicalExaminationTableMap::COL_UPDATED_AT, $updatedAt, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \Patient object
     *
     * @param \Patient|ObjectCollection $patient The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPatient($patient, ?string $comparison = null)
    {
        if ($patient instanceof \Patient) {
            return $this
                ->addUsingAlias(PhysicalExaminationTableMap::COL_PATIENT_UUID, $patient->getUuid(), $comparison);
        } elseif ($patient instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(PhysicalExaminationTableMap::COL_PATIENT_UUID, $patient->toKeyValue('PrimaryKey', 'Uuid'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByPatient() only accepts arguments of type \Patient or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Patient relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPatient(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Patient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Patient');
        }

        return $this;
    }

    /**
     * Use the Patient relation Patient object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PatientQuery A secondary query class using the current class as primary query
     */
    public function usePatientQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPatient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Patient', '\PatientQuery');
    }

    /**
     * Use the Patient relation Patient object
     *
     * @param callable(\PatientQuery):\PatientQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPatientQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePatientQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Patient table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PatientQuery The inner query object of the EXISTS statement
     */
    public function usePatientExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Patient', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Patient table for a NOT EXISTS query.
     *
     * @see usePatientExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PatientQuery The inner query object of the NOT EXISTS statement
     */
    public function usePatientNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Patient', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \User object
     *
     * @param \User|ObjectCollection $user The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUser($user, ?string $comparison = null)
    {
        if ($user instanceof \User) {
            return $this
                ->addUsingAlias(PhysicalExaminationTableMap::COL_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(PhysicalExaminationTableMap::COL_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinUser(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\UserQuery');
    }

    /**
     * Use the User relation User object
     *
     * @param callable(\UserQuery):\UserQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUserQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUserQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to User table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \UserQuery The inner query object of the EXISTS statement
     */
    public function useUserExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('User', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to User table for a NOT EXISTS query.
     *
     * @see useUserExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \UserQuery The inner query object of the NOT EXISTS statement
     */
    public function useUserNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('User', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Exclude object from result
     *
     * @param ChildPhysicalExamination $physicalExamination Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($physicalExamination = null)
    {
        if ($physicalExamination) {
            $this->addUsingAlias(PhysicalExaminationTableMap::COL_UUID, $physicalExamination->getUuid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the physical_examination table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PhysicalExaminationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PhysicalExaminationTableMap::clearInstancePool();
            PhysicalExaminationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PhysicalExaminationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PhysicalExaminationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PhysicalExaminationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PhysicalExaminationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param int $nbDays Maximum age of the latest update in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        $this->addUsingAlias(PhysicalExaminationTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by update date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        $this->addDescendingOrderByColumn(PhysicalExaminationTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by update date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        $this->addAscendingOrderByColumn(PhysicalExaminationTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by create date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        $this->addDescendingOrderByColumn(PhysicalExaminationTableMap::COL_CREATED_AT);

        return $this;
    }

    /**
     * Filter by the latest created
     *
     * @param int $nbDays Maximum age of in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        $this->addUsingAlias(PhysicalExaminationTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by create date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        $this->addAscendingOrderByColumn(PhysicalExaminationTableMap::COL_CREATED_AT);

        return $this;
    }

}
