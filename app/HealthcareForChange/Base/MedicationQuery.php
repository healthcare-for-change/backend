<?php

namespace Base;

use \Medication as ChildMedication;
use \MedicationQuery as ChildMedicationQuery;
use \Exception;
use \PDO;
use Map\MedicationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'medication' table.
 *
 *
 *
 * @method     ChildMedicationQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildMedicationQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildMedicationQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method     ChildMedicationQuery orderByActive($order = Criteria::ASC) Order by the active column
 * @method     ChildMedicationQuery orderByMedicationDoseFormId($order = Criteria::ASC) Order by the medication_dose_form_id column
 * @method     ChildMedicationQuery orderByMedicationMarketingAuthorizationHolderId($order = Criteria::ASC) Order by the medication_marketing_authorization_holder_id column
 * @method     ChildMedicationQuery orderByStrength($order = Criteria::ASC) Order by the strength column
 * @method     ChildMedicationQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildMedicationQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildMedicationQuery groupByUuid() Group by the uuid column
 * @method     ChildMedicationQuery groupByName() Group by the name column
 * @method     ChildMedicationQuery groupByCode() Group by the code column
 * @method     ChildMedicationQuery groupByActive() Group by the active column
 * @method     ChildMedicationQuery groupByMedicationDoseFormId() Group by the medication_dose_form_id column
 * @method     ChildMedicationQuery groupByMedicationMarketingAuthorizationHolderId() Group by the medication_marketing_authorization_holder_id column
 * @method     ChildMedicationQuery groupByStrength() Group by the strength column
 * @method     ChildMedicationQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildMedicationQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildMedicationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMedicationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMedicationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMedicationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMedicationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMedicationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMedicationQuery leftJoinMedicationDoseForm($relationAlias = null) Adds a LEFT JOIN clause to the query using the MedicationDoseForm relation
 * @method     ChildMedicationQuery rightJoinMedicationDoseForm($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MedicationDoseForm relation
 * @method     ChildMedicationQuery innerJoinMedicationDoseForm($relationAlias = null) Adds a INNER JOIN clause to the query using the MedicationDoseForm relation
 *
 * @method     ChildMedicationQuery joinWithMedicationDoseForm($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MedicationDoseForm relation
 *
 * @method     ChildMedicationQuery leftJoinWithMedicationDoseForm() Adds a LEFT JOIN clause and with to the query using the MedicationDoseForm relation
 * @method     ChildMedicationQuery rightJoinWithMedicationDoseForm() Adds a RIGHT JOIN clause and with to the query using the MedicationDoseForm relation
 * @method     ChildMedicationQuery innerJoinWithMedicationDoseForm() Adds a INNER JOIN clause and with to the query using the MedicationDoseForm relation
 *
 * @method     ChildMedicationQuery leftJoinMedicationMarketingAuthorizationHolder($relationAlias = null) Adds a LEFT JOIN clause to the query using the MedicationMarketingAuthorizationHolder relation
 * @method     ChildMedicationQuery rightJoinMedicationMarketingAuthorizationHolder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MedicationMarketingAuthorizationHolder relation
 * @method     ChildMedicationQuery innerJoinMedicationMarketingAuthorizationHolder($relationAlias = null) Adds a INNER JOIN clause to the query using the MedicationMarketingAuthorizationHolder relation
 *
 * @method     ChildMedicationQuery joinWithMedicationMarketingAuthorizationHolder($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MedicationMarketingAuthorizationHolder relation
 *
 * @method     ChildMedicationQuery leftJoinWithMedicationMarketingAuthorizationHolder() Adds a LEFT JOIN clause and with to the query using the MedicationMarketingAuthorizationHolder relation
 * @method     ChildMedicationQuery rightJoinWithMedicationMarketingAuthorizationHolder() Adds a RIGHT JOIN clause and with to the query using the MedicationMarketingAuthorizationHolder relation
 * @method     ChildMedicationQuery innerJoinWithMedicationMarketingAuthorizationHolder() Adds a INNER JOIN clause and with to the query using the MedicationMarketingAuthorizationHolder relation
 *
 * @method     ChildMedicationQuery leftJoinDewormingHistory($relationAlias = null) Adds a LEFT JOIN clause to the query using the DewormingHistory relation
 * @method     ChildMedicationQuery rightJoinDewormingHistory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DewormingHistory relation
 * @method     ChildMedicationQuery innerJoinDewormingHistory($relationAlias = null) Adds a INNER JOIN clause to the query using the DewormingHistory relation
 *
 * @method     ChildMedicationQuery joinWithDewormingHistory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DewormingHistory relation
 *
 * @method     ChildMedicationQuery leftJoinWithDewormingHistory() Adds a LEFT JOIN clause and with to the query using the DewormingHistory relation
 * @method     ChildMedicationQuery rightJoinWithDewormingHistory() Adds a RIGHT JOIN clause and with to the query using the DewormingHistory relation
 * @method     ChildMedicationQuery innerJoinWithDewormingHistory() Adds a INNER JOIN clause and with to the query using the DewormingHistory relation
 *
 * @method     ChildMedicationQuery leftJoinPrescription($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prescription relation
 * @method     ChildMedicationQuery rightJoinPrescription($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prescription relation
 * @method     ChildMedicationQuery innerJoinPrescription($relationAlias = null) Adds a INNER JOIN clause to the query using the Prescription relation
 *
 * @method     ChildMedicationQuery joinWithPrescription($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prescription relation
 *
 * @method     ChildMedicationQuery leftJoinWithPrescription() Adds a LEFT JOIN clause and with to the query using the Prescription relation
 * @method     ChildMedicationQuery rightJoinWithPrescription() Adds a RIGHT JOIN clause and with to the query using the Prescription relation
 * @method     ChildMedicationQuery innerJoinWithPrescription() Adds a INNER JOIN clause and with to the query using the Prescription relation
 *
 * @method     \MedicationDoseFormQuery|\MedicationMarketingAuthorizationHolderQuery|\DewormingHistoryQuery|\PrescriptionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMedication|null findOne(?ConnectionInterface $con = null) Return the first ChildMedication matching the query
 * @method     ChildMedication findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildMedication matching the query, or a new ChildMedication object populated from the query conditions when no match is found
 *
 * @method     ChildMedication|null findOneByUuid(string $uuid) Return the first ChildMedication filtered by the uuid column
 * @method     ChildMedication|null findOneByName(string $name) Return the first ChildMedication filtered by the name column
 * @method     ChildMedication|null findOneByCode(string $code) Return the first ChildMedication filtered by the code column
 * @method     ChildMedication|null findOneByActive(boolean $active) Return the first ChildMedication filtered by the active column
 * @method     ChildMedication|null findOneByMedicationDoseFormId(string $medication_dose_form_id) Return the first ChildMedication filtered by the medication_dose_form_id column
 * @method     ChildMedication|null findOneByMedicationMarketingAuthorizationHolderId(int $medication_marketing_authorization_holder_id) Return the first ChildMedication filtered by the medication_marketing_authorization_holder_id column
 * @method     ChildMedication|null findOneByStrength(string $strength) Return the first ChildMedication filtered by the strength column
 * @method     ChildMedication|null findOneByCreatedAt(string $created_at) Return the first ChildMedication filtered by the created_at column
 * @method     ChildMedication|null findOneByUpdatedAt(string $updated_at) Return the first ChildMedication filtered by the updated_at column *

 * @method     ChildMedication requirePk($key, ?ConnectionInterface $con = null) Return the ChildMedication by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedication requireOne(?ConnectionInterface $con = null) Return the first ChildMedication matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMedication requireOneByUuid(string $uuid) Return the first ChildMedication filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedication requireOneByName(string $name) Return the first ChildMedication filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedication requireOneByCode(string $code) Return the first ChildMedication filtered by the code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedication requireOneByActive(boolean $active) Return the first ChildMedication filtered by the active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedication requireOneByMedicationDoseFormId(string $medication_dose_form_id) Return the first ChildMedication filtered by the medication_dose_form_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedication requireOneByMedicationMarketingAuthorizationHolderId(int $medication_marketing_authorization_holder_id) Return the first ChildMedication filtered by the medication_marketing_authorization_holder_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedication requireOneByStrength(string $strength) Return the first ChildMedication filtered by the strength column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedication requireOneByCreatedAt(string $created_at) Return the first ChildMedication filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedication requireOneByUpdatedAt(string $updated_at) Return the first ChildMedication filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMedication[]|Collection find(?ConnectionInterface $con = null) Return ChildMedication objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildMedication> find(?ConnectionInterface $con = null) Return ChildMedication objects based on current ModelCriteria
 * @method     ChildMedication[]|Collection findByUuid(string $uuid) Return ChildMedication objects filtered by the uuid column
 * @psalm-method Collection&\Traversable<ChildMedication> findByUuid(string $uuid) Return ChildMedication objects filtered by the uuid column
 * @method     ChildMedication[]|Collection findByName(string $name) Return ChildMedication objects filtered by the name column
 * @psalm-method Collection&\Traversable<ChildMedication> findByName(string $name) Return ChildMedication objects filtered by the name column
 * @method     ChildMedication[]|Collection findByCode(string $code) Return ChildMedication objects filtered by the code column
 * @psalm-method Collection&\Traversable<ChildMedication> findByCode(string $code) Return ChildMedication objects filtered by the code column
 * @method     ChildMedication[]|Collection findByActive(boolean $active) Return ChildMedication objects filtered by the active column
 * @psalm-method Collection&\Traversable<ChildMedication> findByActive(boolean $active) Return ChildMedication objects filtered by the active column
 * @method     ChildMedication[]|Collection findByMedicationDoseFormId(string $medication_dose_form_id) Return ChildMedication objects filtered by the medication_dose_form_id column
 * @psalm-method Collection&\Traversable<ChildMedication> findByMedicationDoseFormId(string $medication_dose_form_id) Return ChildMedication objects filtered by the medication_dose_form_id column
 * @method     ChildMedication[]|Collection findByMedicationMarketingAuthorizationHolderId(int $medication_marketing_authorization_holder_id) Return ChildMedication objects filtered by the medication_marketing_authorization_holder_id column
 * @psalm-method Collection&\Traversable<ChildMedication> findByMedicationMarketingAuthorizationHolderId(int $medication_marketing_authorization_holder_id) Return ChildMedication objects filtered by the medication_marketing_authorization_holder_id column
 * @method     ChildMedication[]|Collection findByStrength(string $strength) Return ChildMedication objects filtered by the strength column
 * @psalm-method Collection&\Traversable<ChildMedication> findByStrength(string $strength) Return ChildMedication objects filtered by the strength column
 * @method     ChildMedication[]|Collection findByCreatedAt(string $created_at) Return ChildMedication objects filtered by the created_at column
 * @psalm-method Collection&\Traversable<ChildMedication> findByCreatedAt(string $created_at) Return ChildMedication objects filtered by the created_at column
 * @method     ChildMedication[]|Collection findByUpdatedAt(string $updated_at) Return ChildMedication objects filtered by the updated_at column
 * @psalm-method Collection&\Traversable<ChildMedication> findByUpdatedAt(string $updated_at) Return ChildMedication objects filtered by the updated_at column
 * @method     ChildMedication[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildMedication> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MedicationQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MedicationQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\Medication', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMedicationQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMedicationQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildMedicationQuery) {
            return $criteria;
        }
        $query = new ChildMedicationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMedication|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MedicationTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MedicationTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMedication A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT uuid, name, code, active, medication_dose_form_id, medication_marketing_authorization_holder_id, strength, created_at, updated_at FROM medication WHERE uuid = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMedication $obj */
            $obj = new ChildMedication();
            $obj->hydrate($row);
            MedicationTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildMedication|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(MedicationTableMap::COL_UUID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(MedicationTableMap::COL_UUID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * $query->filterByUuid(['foo', 'bar']); // WHERE uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $uuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicationTableMap::COL_UUID, $uuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * $query->filterByName(['foo', 'bar']); // WHERE name IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $name The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByName($name = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicationTableMap::COL_NAME, $name, $comparison);

        return $this;
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE code LIKE '%fooValue%'
     * $query->filterByCode(['foo', 'bar']); // WHERE code IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $code The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByCode($code = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicationTableMap::COL_CODE, $code, $comparison);

        return $this;
    }

    /**
     * Filter the query on the active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(true); // WHERE active = true
     * $query->filterByActive('yes'); // WHERE active = true
     * </code>
     *
     * @param bool|string $active The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByActive($active = null, ?string $comparison = null)
    {
        if (is_string($active)) {
            $active = in_array(strtolower($active), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        $this->addUsingAlias(MedicationTableMap::COL_ACTIVE, $active, $comparison);

        return $this;
    }

    /**
     * Filter the query on the medication_dose_form_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMedicationDoseFormId(1234); // WHERE medication_dose_form_id = 1234
     * $query->filterByMedicationDoseFormId(array(12, 34)); // WHERE medication_dose_form_id IN (12, 34)
     * $query->filterByMedicationDoseFormId(array('min' => 12)); // WHERE medication_dose_form_id > 12
     * </code>
     *
     * @see       filterByMedicationDoseForm()
     *
     * @param mixed $medicationDoseFormId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicationDoseFormId($medicationDoseFormId = null, ?string $comparison = null)
    {
        if (is_array($medicationDoseFormId)) {
            $useMinMax = false;
            if (isset($medicationDoseFormId['min'])) {
                $this->addUsingAlias(MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID, $medicationDoseFormId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($medicationDoseFormId['max'])) {
                $this->addUsingAlias(MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID, $medicationDoseFormId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID, $medicationDoseFormId, $comparison);

        return $this;
    }

    /**
     * Filter the query on the medication_marketing_authorization_holder_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMedicationMarketingAuthorizationHolderId(1234); // WHERE medication_marketing_authorization_holder_id = 1234
     * $query->filterByMedicationMarketingAuthorizationHolderId(array(12, 34)); // WHERE medication_marketing_authorization_holder_id IN (12, 34)
     * $query->filterByMedicationMarketingAuthorizationHolderId(array('min' => 12)); // WHERE medication_marketing_authorization_holder_id > 12
     * </code>
     *
     * @see       filterByMedicationMarketingAuthorizationHolder()
     *
     * @param mixed $medicationMarketingAuthorizationHolderId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicationMarketingAuthorizationHolderId($medicationMarketingAuthorizationHolderId = null, ?string $comparison = null)
    {
        if (is_array($medicationMarketingAuthorizationHolderId)) {
            $useMinMax = false;
            if (isset($medicationMarketingAuthorizationHolderId['min'])) {
                $this->addUsingAlias(MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID, $medicationMarketingAuthorizationHolderId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($medicationMarketingAuthorizationHolderId['max'])) {
                $this->addUsingAlias(MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID, $medicationMarketingAuthorizationHolderId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID, $medicationMarketingAuthorizationHolderId, $comparison);

        return $this;
    }

    /**
     * Filter the query on the strength column
     *
     * Example usage:
     * <code>
     * $query->filterByStrength('fooValue');   // WHERE strength = 'fooValue'
     * $query->filterByStrength('%fooValue%', Criteria::LIKE); // WHERE strength LIKE '%fooValue%'
     * $query->filterByStrength(['foo', 'bar']); // WHERE strength IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $strength The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByStrength($strength = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($strength)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicationTableMap::COL_STRENGTH, $strength, $comparison);

        return $this;
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, ?string $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(MedicationTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(MedicationTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicationTableMap::COL_CREATED_AT, $createdAt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, ?string $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(MedicationTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(MedicationTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(MedicationTableMap::COL_UPDATED_AT, $updatedAt, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \MedicationDoseForm object
     *
     * @param \MedicationDoseForm|ObjectCollection $medicationDoseForm The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicationDoseForm($medicationDoseForm, ?string $comparison = null)
    {
        if ($medicationDoseForm instanceof \MedicationDoseForm) {
            return $this
                ->addUsingAlias(MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID, $medicationDoseForm->getId(), $comparison);
        } elseif ($medicationDoseForm instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID, $medicationDoseForm->toKeyValue('PrimaryKey', 'Id'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByMedicationDoseForm() only accepts arguments of type \MedicationDoseForm or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MedicationDoseForm relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinMedicationDoseForm(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MedicationDoseForm');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MedicationDoseForm');
        }

        return $this;
    }

    /**
     * Use the MedicationDoseForm relation MedicationDoseForm object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MedicationDoseFormQuery A secondary query class using the current class as primary query
     */
    public function useMedicationDoseFormQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMedicationDoseForm($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MedicationDoseForm', '\MedicationDoseFormQuery');
    }

    /**
     * Use the MedicationDoseForm relation MedicationDoseForm object
     *
     * @param callable(\MedicationDoseFormQuery):\MedicationDoseFormQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withMedicationDoseFormQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useMedicationDoseFormQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to MedicationDoseForm table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \MedicationDoseFormQuery The inner query object of the EXISTS statement
     */
    public function useMedicationDoseFormExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('MedicationDoseForm', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to MedicationDoseForm table for a NOT EXISTS query.
     *
     * @see useMedicationDoseFormExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \MedicationDoseFormQuery The inner query object of the NOT EXISTS statement
     */
    public function useMedicationDoseFormNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('MedicationDoseForm', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \MedicationMarketingAuthorizationHolder object
     *
     * @param \MedicationMarketingAuthorizationHolder|ObjectCollection $medicationMarketingAuthorizationHolder The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicationMarketingAuthorizationHolder($medicationMarketingAuthorizationHolder, ?string $comparison = null)
    {
        if ($medicationMarketingAuthorizationHolder instanceof \MedicationMarketingAuthorizationHolder) {
            return $this
                ->addUsingAlias(MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID, $medicationMarketingAuthorizationHolder->getId(), $comparison);
        } elseif ($medicationMarketingAuthorizationHolder instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID, $medicationMarketingAuthorizationHolder->toKeyValue('PrimaryKey', 'Id'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByMedicationMarketingAuthorizationHolder() only accepts arguments of type \MedicationMarketingAuthorizationHolder or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MedicationMarketingAuthorizationHolder relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinMedicationMarketingAuthorizationHolder(?string $relationAlias = null, ?string $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MedicationMarketingAuthorizationHolder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MedicationMarketingAuthorizationHolder');
        }

        return $this;
    }

    /**
     * Use the MedicationMarketingAuthorizationHolder relation MedicationMarketingAuthorizationHolder object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MedicationMarketingAuthorizationHolderQuery A secondary query class using the current class as primary query
     */
    public function useMedicationMarketingAuthorizationHolderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMedicationMarketingAuthorizationHolder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MedicationMarketingAuthorizationHolder', '\MedicationMarketingAuthorizationHolderQuery');
    }

    /**
     * Use the MedicationMarketingAuthorizationHolder relation MedicationMarketingAuthorizationHolder object
     *
     * @param callable(\MedicationMarketingAuthorizationHolderQuery):\MedicationMarketingAuthorizationHolderQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withMedicationMarketingAuthorizationHolderQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useMedicationMarketingAuthorizationHolderQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to MedicationMarketingAuthorizationHolder table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \MedicationMarketingAuthorizationHolderQuery The inner query object of the EXISTS statement
     */
    public function useMedicationMarketingAuthorizationHolderExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('MedicationMarketingAuthorizationHolder', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to MedicationMarketingAuthorizationHolder table for a NOT EXISTS query.
     *
     * @see useMedicationMarketingAuthorizationHolderExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \MedicationMarketingAuthorizationHolderQuery The inner query object of the NOT EXISTS statement
     */
    public function useMedicationMarketingAuthorizationHolderNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('MedicationMarketingAuthorizationHolder', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \DewormingHistory object
     *
     * @param \DewormingHistory|ObjectCollection $dewormingHistory the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDewormingHistory($dewormingHistory, ?string $comparison = null)
    {
        if ($dewormingHistory instanceof \DewormingHistory) {
            $this
                ->addUsingAlias(MedicationTableMap::COL_UUID, $dewormingHistory->getMedicationId(), $comparison);

            return $this;
        } elseif ($dewormingHistory instanceof ObjectCollection) {
            $this
                ->useDewormingHistoryQuery()
                ->filterByPrimaryKeys($dewormingHistory->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByDewormingHistory() only accepts arguments of type \DewormingHistory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DewormingHistory relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinDewormingHistory(?string $relationAlias = null, ?string $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DewormingHistory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DewormingHistory');
        }

        return $this;
    }

    /**
     * Use the DewormingHistory relation DewormingHistory object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \DewormingHistoryQuery A secondary query class using the current class as primary query
     */
    public function useDewormingHistoryQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinDewormingHistory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DewormingHistory', '\DewormingHistoryQuery');
    }

    /**
     * Use the DewormingHistory relation DewormingHistory object
     *
     * @param callable(\DewormingHistoryQuery):\DewormingHistoryQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withDewormingHistoryQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useDewormingHistoryQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to DewormingHistory table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \DewormingHistoryQuery The inner query object of the EXISTS statement
     */
    public function useDewormingHistoryExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('DewormingHistory', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to DewormingHistory table for a NOT EXISTS query.
     *
     * @see useDewormingHistoryExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \DewormingHistoryQuery The inner query object of the NOT EXISTS statement
     */
    public function useDewormingHistoryNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('DewormingHistory', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \Prescription object
     *
     * @param \Prescription|ObjectCollection $prescription the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrescription($prescription, ?string $comparison = null)
    {
        if ($prescription instanceof \Prescription) {
            $this
                ->addUsingAlias(MedicationTableMap::COL_UUID, $prescription->getMedicationUuid(), $comparison);

            return $this;
        } elseif ($prescription instanceof ObjectCollection) {
            $this
                ->usePrescriptionQuery()
                ->filterByPrimaryKeys($prescription->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByPrescription() only accepts arguments of type \Prescription or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prescription relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPrescription(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prescription');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prescription');
        }

        return $this;
    }

    /**
     * Use the Prescription relation Prescription object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrescriptionQuery A secondary query class using the current class as primary query
     */
    public function usePrescriptionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrescription($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prescription', '\PrescriptionQuery');
    }

    /**
     * Use the Prescription relation Prescription object
     *
     * @param callable(\PrescriptionQuery):\PrescriptionQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPrescriptionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePrescriptionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Prescription table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PrescriptionQuery The inner query object of the EXISTS statement
     */
    public function usePrescriptionExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Prescription', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Prescription table for a NOT EXISTS query.
     *
     * @see usePrescriptionExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PrescriptionQuery The inner query object of the NOT EXISTS statement
     */
    public function usePrescriptionNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Prescription', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Exclude object from result
     *
     * @param ChildMedication $medication Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($medication = null)
    {
        if ($medication) {
            $this->addUsingAlias(MedicationTableMap::COL_UUID, $medication->getUuid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the medication table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MedicationTableMap::clearInstancePool();
            MedicationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MedicationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MedicationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MedicationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param int $nbDays Maximum age of the latest update in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        $this->addUsingAlias(MedicationTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by update date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        $this->addDescendingOrderByColumn(MedicationTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by update date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        $this->addAscendingOrderByColumn(MedicationTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by create date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        $this->addDescendingOrderByColumn(MedicationTableMap::COL_CREATED_AT);

        return $this;
    }

    /**
     * Filter by the latest created
     *
     * @param int $nbDays Maximum age of in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        $this->addUsingAlias(MedicationTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by create date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        $this->addAscendingOrderByColumn(MedicationTableMap::COL_CREATED_AT);

        return $this;
    }

}
