<?php

namespace Base;

use \PrescriptionDoseInstruction as ChildPrescriptionDoseInstruction;
use \PrescriptionDoseInstructionQuery as ChildPrescriptionDoseInstructionQuery;
use \Exception;
use \PDO;
use Map\PrescriptionDoseInstructionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'prescription_dose_instruction' table.
 *
 *
 *
 * @method     ChildPrescriptionDoseInstructionQuery orderByMedicationDoseInstructionId($order = Criteria::ASC) Order by the medication_dose_instruction_id column
 * @method     ChildPrescriptionDoseInstructionQuery orderByPrescriptionUuid($order = Criteria::ASC) Order by the prescription_uuid column
 * @method     ChildPrescriptionDoseInstructionQuery orderByQuantifier1($order = Criteria::ASC) Order by the quantifier_1 column
 * @method     ChildPrescriptionDoseInstructionQuery orderByQuantifier2($order = Criteria::ASC) Order by the quantifier_2 column
 *
 * @method     ChildPrescriptionDoseInstructionQuery groupByMedicationDoseInstructionId() Group by the medication_dose_instruction_id column
 * @method     ChildPrescriptionDoseInstructionQuery groupByPrescriptionUuid() Group by the prescription_uuid column
 * @method     ChildPrescriptionDoseInstructionQuery groupByQuantifier1() Group by the quantifier_1 column
 * @method     ChildPrescriptionDoseInstructionQuery groupByQuantifier2() Group by the quantifier_2 column
 *
 * @method     ChildPrescriptionDoseInstructionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPrescriptionDoseInstructionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPrescriptionDoseInstructionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPrescriptionDoseInstructionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPrescriptionDoseInstructionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPrescriptionDoseInstructionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPrescriptionDoseInstructionQuery leftJoinMedicationDoseInstruction($relationAlias = null) Adds a LEFT JOIN clause to the query using the MedicationDoseInstruction relation
 * @method     ChildPrescriptionDoseInstructionQuery rightJoinMedicationDoseInstruction($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MedicationDoseInstruction relation
 * @method     ChildPrescriptionDoseInstructionQuery innerJoinMedicationDoseInstruction($relationAlias = null) Adds a INNER JOIN clause to the query using the MedicationDoseInstruction relation
 *
 * @method     ChildPrescriptionDoseInstructionQuery joinWithMedicationDoseInstruction($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MedicationDoseInstruction relation
 *
 * @method     ChildPrescriptionDoseInstructionQuery leftJoinWithMedicationDoseInstruction() Adds a LEFT JOIN clause and with to the query using the MedicationDoseInstruction relation
 * @method     ChildPrescriptionDoseInstructionQuery rightJoinWithMedicationDoseInstruction() Adds a RIGHT JOIN clause and with to the query using the MedicationDoseInstruction relation
 * @method     ChildPrescriptionDoseInstructionQuery innerJoinWithMedicationDoseInstruction() Adds a INNER JOIN clause and with to the query using the MedicationDoseInstruction relation
 *
 * @method     ChildPrescriptionDoseInstructionQuery leftJoinPrescription($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prescription relation
 * @method     ChildPrescriptionDoseInstructionQuery rightJoinPrescription($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prescription relation
 * @method     ChildPrescriptionDoseInstructionQuery innerJoinPrescription($relationAlias = null) Adds a INNER JOIN clause to the query using the Prescription relation
 *
 * @method     ChildPrescriptionDoseInstructionQuery joinWithPrescription($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prescription relation
 *
 * @method     ChildPrescriptionDoseInstructionQuery leftJoinWithPrescription() Adds a LEFT JOIN clause and with to the query using the Prescription relation
 * @method     ChildPrescriptionDoseInstructionQuery rightJoinWithPrescription() Adds a RIGHT JOIN clause and with to the query using the Prescription relation
 * @method     ChildPrescriptionDoseInstructionQuery innerJoinWithPrescription() Adds a INNER JOIN clause and with to the query using the Prescription relation
 *
 * @method     \MedicationDoseInstructionQuery|\PrescriptionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPrescriptionDoseInstruction|null findOne(?ConnectionInterface $con = null) Return the first ChildPrescriptionDoseInstruction matching the query
 * @method     ChildPrescriptionDoseInstruction findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildPrescriptionDoseInstruction matching the query, or a new ChildPrescriptionDoseInstruction object populated from the query conditions when no match is found
 *
 * @method     ChildPrescriptionDoseInstruction|null findOneByMedicationDoseInstructionId(string $medication_dose_instruction_id) Return the first ChildPrescriptionDoseInstruction filtered by the medication_dose_instruction_id column
 * @method     ChildPrescriptionDoseInstruction|null findOneByPrescriptionUuid(string $prescription_uuid) Return the first ChildPrescriptionDoseInstruction filtered by the prescription_uuid column
 * @method     ChildPrescriptionDoseInstruction|null findOneByQuantifier1(string $quantifier_1) Return the first ChildPrescriptionDoseInstruction filtered by the quantifier_1 column
 * @method     ChildPrescriptionDoseInstruction|null findOneByQuantifier2(string $quantifier_2) Return the first ChildPrescriptionDoseInstruction filtered by the quantifier_2 column *

 * @method     ChildPrescriptionDoseInstruction requirePk($key, ?ConnectionInterface $con = null) Return the ChildPrescriptionDoseInstruction by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescriptionDoseInstruction requireOne(?ConnectionInterface $con = null) Return the first ChildPrescriptionDoseInstruction matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrescriptionDoseInstruction requireOneByMedicationDoseInstructionId(string $medication_dose_instruction_id) Return the first ChildPrescriptionDoseInstruction filtered by the medication_dose_instruction_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescriptionDoseInstruction requireOneByPrescriptionUuid(string $prescription_uuid) Return the first ChildPrescriptionDoseInstruction filtered by the prescription_uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescriptionDoseInstruction requireOneByQuantifier1(string $quantifier_1) Return the first ChildPrescriptionDoseInstruction filtered by the quantifier_1 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrescriptionDoseInstruction requireOneByQuantifier2(string $quantifier_2) Return the first ChildPrescriptionDoseInstruction filtered by the quantifier_2 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrescriptionDoseInstruction[]|Collection find(?ConnectionInterface $con = null) Return ChildPrescriptionDoseInstruction objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildPrescriptionDoseInstruction> find(?ConnectionInterface $con = null) Return ChildPrescriptionDoseInstruction objects based on current ModelCriteria
 * @method     ChildPrescriptionDoseInstruction[]|Collection findByMedicationDoseInstructionId(string $medication_dose_instruction_id) Return ChildPrescriptionDoseInstruction objects filtered by the medication_dose_instruction_id column
 * @psalm-method Collection&\Traversable<ChildPrescriptionDoseInstruction> findByMedicationDoseInstructionId(string $medication_dose_instruction_id) Return ChildPrescriptionDoseInstruction objects filtered by the medication_dose_instruction_id column
 * @method     ChildPrescriptionDoseInstruction[]|Collection findByPrescriptionUuid(string $prescription_uuid) Return ChildPrescriptionDoseInstruction objects filtered by the prescription_uuid column
 * @psalm-method Collection&\Traversable<ChildPrescriptionDoseInstruction> findByPrescriptionUuid(string $prescription_uuid) Return ChildPrescriptionDoseInstruction objects filtered by the prescription_uuid column
 * @method     ChildPrescriptionDoseInstruction[]|Collection findByQuantifier1(string $quantifier_1) Return ChildPrescriptionDoseInstruction objects filtered by the quantifier_1 column
 * @psalm-method Collection&\Traversable<ChildPrescriptionDoseInstruction> findByQuantifier1(string $quantifier_1) Return ChildPrescriptionDoseInstruction objects filtered by the quantifier_1 column
 * @method     ChildPrescriptionDoseInstruction[]|Collection findByQuantifier2(string $quantifier_2) Return ChildPrescriptionDoseInstruction objects filtered by the quantifier_2 column
 * @psalm-method Collection&\Traversable<ChildPrescriptionDoseInstruction> findByQuantifier2(string $quantifier_2) Return ChildPrescriptionDoseInstruction objects filtered by the quantifier_2 column
 * @method     ChildPrescriptionDoseInstruction[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildPrescriptionDoseInstruction> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PrescriptionDoseInstructionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PrescriptionDoseInstructionQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\PrescriptionDoseInstruction', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPrescriptionDoseInstructionQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPrescriptionDoseInstructionQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildPrescriptionDoseInstructionQuery) {
            return $criteria;
        }
        $query = new ChildPrescriptionDoseInstructionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$medication_dose_instruction_id, $prescription_uuid] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPrescriptionDoseInstruction|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PrescriptionDoseInstructionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PrescriptionDoseInstructionTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrescriptionDoseInstruction A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT medication_dose_instruction_id, prescription_uuid, quantifier_1, quantifier_2 FROM prescription_dose_instruction WHERE medication_dose_instruction_id = :p0 AND prescription_uuid = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPrescriptionDoseInstruction $obj */
            $obj = new ChildPrescriptionDoseInstruction();
            $obj->hydrate($row);
            PrescriptionDoseInstructionTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildPrescriptionDoseInstruction|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(PrescriptionDoseInstructionTableMap::COL_MEDICATION_DOSE_INSTRUCTION_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(PrescriptionDoseInstructionTableMap::COL_PRESCRIPTION_UUID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            $this->add(null, '1<>1', Criteria::CUSTOM);

            return $this;
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(PrescriptionDoseInstructionTableMap::COL_MEDICATION_DOSE_INSTRUCTION_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(PrescriptionDoseInstructionTableMap::COL_PRESCRIPTION_UUID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the medication_dose_instruction_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMedicationDoseInstructionId(1234); // WHERE medication_dose_instruction_id = 1234
     * $query->filterByMedicationDoseInstructionId(array(12, 34)); // WHERE medication_dose_instruction_id IN (12, 34)
     * $query->filterByMedicationDoseInstructionId(array('min' => 12)); // WHERE medication_dose_instruction_id > 12
     * </code>
     *
     * @see       filterByMedicationDoseInstruction()
     *
     * @param mixed $medicationDoseInstructionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicationDoseInstructionId($medicationDoseInstructionId = null, ?string $comparison = null)
    {
        if (is_array($medicationDoseInstructionId)) {
            $useMinMax = false;
            if (isset($medicationDoseInstructionId['min'])) {
                $this->addUsingAlias(PrescriptionDoseInstructionTableMap::COL_MEDICATION_DOSE_INSTRUCTION_ID, $medicationDoseInstructionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($medicationDoseInstructionId['max'])) {
                $this->addUsingAlias(PrescriptionDoseInstructionTableMap::COL_MEDICATION_DOSE_INSTRUCTION_ID, $medicationDoseInstructionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionDoseInstructionTableMap::COL_MEDICATION_DOSE_INSTRUCTION_ID, $medicationDoseInstructionId, $comparison);

        return $this;
    }

    /**
     * Filter the query on the prescription_uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByPrescriptionUuid('fooValue');   // WHERE prescription_uuid = 'fooValue'
     * $query->filterByPrescriptionUuid('%fooValue%', Criteria::LIKE); // WHERE prescription_uuid LIKE '%fooValue%'
     * $query->filterByPrescriptionUuid(['foo', 'bar']); // WHERE prescription_uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $prescriptionUuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrescriptionUuid($prescriptionUuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prescriptionUuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionDoseInstructionTableMap::COL_PRESCRIPTION_UUID, $prescriptionUuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the quantifier_1 column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantifier1('fooValue');   // WHERE quantifier_1 = 'fooValue'
     * $query->filterByQuantifier1('%fooValue%', Criteria::LIKE); // WHERE quantifier_1 LIKE '%fooValue%'
     * $query->filterByQuantifier1(['foo', 'bar']); // WHERE quantifier_1 IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $quantifier1 The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByQuantifier1($quantifier1 = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($quantifier1)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionDoseInstructionTableMap::COL_QUANTIFIER_1, $quantifier1, $comparison);

        return $this;
    }

    /**
     * Filter the query on the quantifier_2 column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantifier2('fooValue');   // WHERE quantifier_2 = 'fooValue'
     * $query->filterByQuantifier2('%fooValue%', Criteria::LIKE); // WHERE quantifier_2 LIKE '%fooValue%'
     * $query->filterByQuantifier2(['foo', 'bar']); // WHERE quantifier_2 IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $quantifier2 The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByQuantifier2($quantifier2 = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($quantifier2)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PrescriptionDoseInstructionTableMap::COL_QUANTIFIER_2, $quantifier2, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \MedicationDoseInstruction object
     *
     * @param \MedicationDoseInstruction|ObjectCollection $medicationDoseInstruction The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicationDoseInstruction($medicationDoseInstruction, ?string $comparison = null)
    {
        if ($medicationDoseInstruction instanceof \MedicationDoseInstruction) {
            return $this
                ->addUsingAlias(PrescriptionDoseInstructionTableMap::COL_MEDICATION_DOSE_INSTRUCTION_ID, $medicationDoseInstruction->getId(), $comparison);
        } elseif ($medicationDoseInstruction instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(PrescriptionDoseInstructionTableMap::COL_MEDICATION_DOSE_INSTRUCTION_ID, $medicationDoseInstruction->toKeyValue('PrimaryKey', 'Id'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByMedicationDoseInstruction() only accepts arguments of type \MedicationDoseInstruction or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MedicationDoseInstruction relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinMedicationDoseInstruction(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MedicationDoseInstruction');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MedicationDoseInstruction');
        }

        return $this;
    }

    /**
     * Use the MedicationDoseInstruction relation MedicationDoseInstruction object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MedicationDoseInstructionQuery A secondary query class using the current class as primary query
     */
    public function useMedicationDoseInstructionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMedicationDoseInstruction($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MedicationDoseInstruction', '\MedicationDoseInstructionQuery');
    }

    /**
     * Use the MedicationDoseInstruction relation MedicationDoseInstruction object
     *
     * @param callable(\MedicationDoseInstructionQuery):\MedicationDoseInstructionQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withMedicationDoseInstructionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useMedicationDoseInstructionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to MedicationDoseInstruction table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \MedicationDoseInstructionQuery The inner query object of the EXISTS statement
     */
    public function useMedicationDoseInstructionExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('MedicationDoseInstruction', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to MedicationDoseInstruction table for a NOT EXISTS query.
     *
     * @see useMedicationDoseInstructionExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \MedicationDoseInstructionQuery The inner query object of the NOT EXISTS statement
     */
    public function useMedicationDoseInstructionNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('MedicationDoseInstruction', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \Prescription object
     *
     * @param \Prescription|ObjectCollection $prescription The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrescription($prescription, ?string $comparison = null)
    {
        if ($prescription instanceof \Prescription) {
            return $this
                ->addUsingAlias(PrescriptionDoseInstructionTableMap::COL_PRESCRIPTION_UUID, $prescription->getUuid(), $comparison);
        } elseif ($prescription instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(PrescriptionDoseInstructionTableMap::COL_PRESCRIPTION_UUID, $prescription->toKeyValue('PrimaryKey', 'Uuid'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByPrescription() only accepts arguments of type \Prescription or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prescription relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPrescription(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prescription');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prescription');
        }

        return $this;
    }

    /**
     * Use the Prescription relation Prescription object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrescriptionQuery A secondary query class using the current class as primary query
     */
    public function usePrescriptionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrescription($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prescription', '\PrescriptionQuery');
    }

    /**
     * Use the Prescription relation Prescription object
     *
     * @param callable(\PrescriptionQuery):\PrescriptionQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPrescriptionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePrescriptionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Prescription table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PrescriptionQuery The inner query object of the EXISTS statement
     */
    public function usePrescriptionExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Prescription', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Prescription table for a NOT EXISTS query.
     *
     * @see usePrescriptionExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PrescriptionQuery The inner query object of the NOT EXISTS statement
     */
    public function usePrescriptionNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Prescription', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Exclude object from result
     *
     * @param ChildPrescriptionDoseInstruction $prescriptionDoseInstruction Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($prescriptionDoseInstruction = null)
    {
        if ($prescriptionDoseInstruction) {
            $this->addCond('pruneCond0', $this->getAliasedColName(PrescriptionDoseInstructionTableMap::COL_MEDICATION_DOSE_INSTRUCTION_ID), $prescriptionDoseInstruction->getMedicationDoseInstructionId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(PrescriptionDoseInstructionTableMap::COL_PRESCRIPTION_UUID), $prescriptionDoseInstruction->getPrescriptionUuid(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the prescription_dose_instruction table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrescriptionDoseInstructionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PrescriptionDoseInstructionTableMap::clearInstancePool();
            PrescriptionDoseInstructionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrescriptionDoseInstructionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PrescriptionDoseInstructionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PrescriptionDoseInstructionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PrescriptionDoseInstructionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

}
