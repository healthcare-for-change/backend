<?php

namespace Base;

use \AuthTokenLogin as ChildAuthTokenLogin;
use \AuthTokenLoginQuery as ChildAuthTokenLoginQuery;
use \Exception;
use \PDO;
use Map\AuthTokenLoginTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'auth_token_logins' table.
 *
 *
 *
 * @method     ChildAuthTokenLoginQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildAuthTokenLoginQuery orderByIpAddress($order = Criteria::ASC) Order by the ip_address column
 * @method     ChildAuthTokenLoginQuery orderByUserAgent($order = Criteria::ASC) Order by the user_agent column
 * @method     ChildAuthTokenLoginQuery orderByIdType($order = Criteria::ASC) Order by the id_type column
 * @method     ChildAuthTokenLoginQuery orderByIdentifier($order = Criteria::ASC) Order by the identifier column
 * @method     ChildAuthTokenLoginQuery orderByDate($order = Criteria::ASC) Order by the date column
 * @method     ChildAuthTokenLoginQuery orderBySuccess($order = Criteria::ASC) Order by the success column
 * @method     ChildAuthTokenLoginQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 *
 * @method     ChildAuthTokenLoginQuery groupById() Group by the id column
 * @method     ChildAuthTokenLoginQuery groupByIpAddress() Group by the ip_address column
 * @method     ChildAuthTokenLoginQuery groupByUserAgent() Group by the user_agent column
 * @method     ChildAuthTokenLoginQuery groupByIdType() Group by the id_type column
 * @method     ChildAuthTokenLoginQuery groupByIdentifier() Group by the identifier column
 * @method     ChildAuthTokenLoginQuery groupByDate() Group by the date column
 * @method     ChildAuthTokenLoginQuery groupBySuccess() Group by the success column
 * @method     ChildAuthTokenLoginQuery groupByUserId() Group by the user_id column
 *
 * @method     ChildAuthTokenLoginQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAuthTokenLoginQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAuthTokenLoginQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAuthTokenLoginQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAuthTokenLoginQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAuthTokenLoginQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAuthTokenLoginQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildAuthTokenLoginQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildAuthTokenLoginQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildAuthTokenLoginQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildAuthTokenLoginQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildAuthTokenLoginQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildAuthTokenLoginQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     \UserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildAuthTokenLogin|null findOne(?ConnectionInterface $con = null) Return the first ChildAuthTokenLogin matching the query
 * @method     ChildAuthTokenLogin findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildAuthTokenLogin matching the query, or a new ChildAuthTokenLogin object populated from the query conditions when no match is found
 *
 * @method     ChildAuthTokenLogin|null findOneById(int $id) Return the first ChildAuthTokenLogin filtered by the id column
 * @method     ChildAuthTokenLogin|null findOneByIpAddress(string $ip_address) Return the first ChildAuthTokenLogin filtered by the ip_address column
 * @method     ChildAuthTokenLogin|null findOneByUserAgent(string $user_agent) Return the first ChildAuthTokenLogin filtered by the user_agent column
 * @method     ChildAuthTokenLogin|null findOneByIdType(string $id_type) Return the first ChildAuthTokenLogin filtered by the id_type column
 * @method     ChildAuthTokenLogin|null findOneByIdentifier(string $identifier) Return the first ChildAuthTokenLogin filtered by the identifier column
 * @method     ChildAuthTokenLogin|null findOneByDate(string $date) Return the first ChildAuthTokenLogin filtered by the date column
 * @method     ChildAuthTokenLogin|null findOneBySuccess(int $success) Return the first ChildAuthTokenLogin filtered by the success column
 * @method     ChildAuthTokenLogin|null findOneByUserId(int $user_id) Return the first ChildAuthTokenLogin filtered by the user_id column *

 * @method     ChildAuthTokenLogin requirePk($key, ?ConnectionInterface $con = null) Return the ChildAuthTokenLogin by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthTokenLogin requireOne(?ConnectionInterface $con = null) Return the first ChildAuthTokenLogin matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAuthTokenLogin requireOneById(int $id) Return the first ChildAuthTokenLogin filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthTokenLogin requireOneByIpAddress(string $ip_address) Return the first ChildAuthTokenLogin filtered by the ip_address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthTokenLogin requireOneByUserAgent(string $user_agent) Return the first ChildAuthTokenLogin filtered by the user_agent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthTokenLogin requireOneByIdType(string $id_type) Return the first ChildAuthTokenLogin filtered by the id_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthTokenLogin requireOneByIdentifier(string $identifier) Return the first ChildAuthTokenLogin filtered by the identifier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthTokenLogin requireOneByDate(string $date) Return the first ChildAuthTokenLogin filtered by the date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthTokenLogin requireOneBySuccess(int $success) Return the first ChildAuthTokenLogin filtered by the success column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAuthTokenLogin requireOneByUserId(int $user_id) Return the first ChildAuthTokenLogin filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAuthTokenLogin[]|Collection find(?ConnectionInterface $con = null) Return ChildAuthTokenLogin objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildAuthTokenLogin> find(?ConnectionInterface $con = null) Return ChildAuthTokenLogin objects based on current ModelCriteria
 * @method     ChildAuthTokenLogin[]|Collection findById(int $id) Return ChildAuthTokenLogin objects filtered by the id column
 * @psalm-method Collection&\Traversable<ChildAuthTokenLogin> findById(int $id) Return ChildAuthTokenLogin objects filtered by the id column
 * @method     ChildAuthTokenLogin[]|Collection findByIpAddress(string $ip_address) Return ChildAuthTokenLogin objects filtered by the ip_address column
 * @psalm-method Collection&\Traversable<ChildAuthTokenLogin> findByIpAddress(string $ip_address) Return ChildAuthTokenLogin objects filtered by the ip_address column
 * @method     ChildAuthTokenLogin[]|Collection findByUserAgent(string $user_agent) Return ChildAuthTokenLogin objects filtered by the user_agent column
 * @psalm-method Collection&\Traversable<ChildAuthTokenLogin> findByUserAgent(string $user_agent) Return ChildAuthTokenLogin objects filtered by the user_agent column
 * @method     ChildAuthTokenLogin[]|Collection findByIdType(string $id_type) Return ChildAuthTokenLogin objects filtered by the id_type column
 * @psalm-method Collection&\Traversable<ChildAuthTokenLogin> findByIdType(string $id_type) Return ChildAuthTokenLogin objects filtered by the id_type column
 * @method     ChildAuthTokenLogin[]|Collection findByIdentifier(string $identifier) Return ChildAuthTokenLogin objects filtered by the identifier column
 * @psalm-method Collection&\Traversable<ChildAuthTokenLogin> findByIdentifier(string $identifier) Return ChildAuthTokenLogin objects filtered by the identifier column
 * @method     ChildAuthTokenLogin[]|Collection findByDate(string $date) Return ChildAuthTokenLogin objects filtered by the date column
 * @psalm-method Collection&\Traversable<ChildAuthTokenLogin> findByDate(string $date) Return ChildAuthTokenLogin objects filtered by the date column
 * @method     ChildAuthTokenLogin[]|Collection findBySuccess(int $success) Return ChildAuthTokenLogin objects filtered by the success column
 * @psalm-method Collection&\Traversable<ChildAuthTokenLogin> findBySuccess(int $success) Return ChildAuthTokenLogin objects filtered by the success column
 * @method     ChildAuthTokenLogin[]|Collection findByUserId(int $user_id) Return ChildAuthTokenLogin objects filtered by the user_id column
 * @psalm-method Collection&\Traversable<ChildAuthTokenLogin> findByUserId(int $user_id) Return ChildAuthTokenLogin objects filtered by the user_id column
 * @method     ChildAuthTokenLogin[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildAuthTokenLogin> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AuthTokenLoginQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AuthTokenLoginQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\AuthTokenLogin', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAuthTokenLoginQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAuthTokenLoginQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildAuthTokenLoginQuery) {
            return $criteria;
        }
        $query = new ChildAuthTokenLoginQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAuthTokenLogin|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AuthTokenLoginTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AuthTokenLoginTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAuthTokenLogin A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, ip_address, user_agent, id_type, identifier, date, success, user_id FROM auth_token_logins WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAuthTokenLogin $obj */
            $obj = new ChildAuthTokenLogin();
            $obj->hydrate($row);
            AuthTokenLoginTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildAuthTokenLogin|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(AuthTokenLoginTableMap::COL_ID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(AuthTokenLoginTableMap::COL_ID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterById($id = null, ?string $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(AuthTokenLoginTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(AuthTokenLoginTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthTokenLoginTableMap::COL_ID, $id, $comparison);

        return $this;
    }

    /**
     * Filter the query on the ip_address column
     *
     * Example usage:
     * <code>
     * $query->filterByIpAddress('fooValue');   // WHERE ip_address = 'fooValue'
     * $query->filterByIpAddress('%fooValue%', Criteria::LIKE); // WHERE ip_address LIKE '%fooValue%'
     * $query->filterByIpAddress(['foo', 'bar']); // WHERE ip_address IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $ipAddress The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByIpAddress($ipAddress = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ipAddress)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthTokenLoginTableMap::COL_IP_ADDRESS, $ipAddress, $comparison);

        return $this;
    }

    /**
     * Filter the query on the user_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByUserAgent('fooValue');   // WHERE user_agent = 'fooValue'
     * $query->filterByUserAgent('%fooValue%', Criteria::LIKE); // WHERE user_agent LIKE '%fooValue%'
     * $query->filterByUserAgent(['foo', 'bar']); // WHERE user_agent IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $userAgent The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUserAgent($userAgent = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userAgent)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthTokenLoginTableMap::COL_USER_AGENT, $userAgent, $comparison);

        return $this;
    }

    /**
     * Filter the query on the id_type column
     *
     * Example usage:
     * <code>
     * $query->filterByIdType('fooValue');   // WHERE id_type = 'fooValue'
     * $query->filterByIdType('%fooValue%', Criteria::LIKE); // WHERE id_type LIKE '%fooValue%'
     * $query->filterByIdType(['foo', 'bar']); // WHERE id_type IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $idType The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByIdType($idType = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idType)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthTokenLoginTableMap::COL_ID_TYPE, $idType, $comparison);

        return $this;
    }

    /**
     * Filter the query on the identifier column
     *
     * Example usage:
     * <code>
     * $query->filterByIdentifier('fooValue');   // WHERE identifier = 'fooValue'
     * $query->filterByIdentifier('%fooValue%', Criteria::LIKE); // WHERE identifier LIKE '%fooValue%'
     * $query->filterByIdentifier(['foo', 'bar']); // WHERE identifier IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $identifier The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByIdentifier($identifier = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($identifier)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthTokenLoginTableMap::COL_IDENTIFIER, $identifier, $comparison);

        return $this;
    }

    /**
     * Filter the query on the date column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('2011-03-14'); // WHERE date = '2011-03-14'
     * $query->filterByDate('now'); // WHERE date = '2011-03-14'
     * $query->filterByDate(array('max' => 'yesterday')); // WHERE date > '2011-03-13'
     * </code>
     *
     * @param mixed $date The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDate($date = null, ?string $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(AuthTokenLoginTableMap::COL_DATE, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(AuthTokenLoginTableMap::COL_DATE, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthTokenLoginTableMap::COL_DATE, $date, $comparison);

        return $this;
    }

    /**
     * Filter the query on the success column
     *
     * Example usage:
     * <code>
     * $query->filterBySuccess(1234); // WHERE success = 1234
     * $query->filterBySuccess(array(12, 34)); // WHERE success IN (12, 34)
     * $query->filterBySuccess(array('min' => 12)); // WHERE success > 12
     * </code>
     *
     * @param mixed $success The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterBySuccess($success = null, ?string $comparison = null)
    {
        if (is_array($success)) {
            $useMinMax = false;
            if (isset($success['min'])) {
                $this->addUsingAlias(AuthTokenLoginTableMap::COL_SUCCESS, $success['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($success['max'])) {
                $this->addUsingAlias(AuthTokenLoginTableMap::COL_SUCCESS, $success['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthTokenLoginTableMap::COL_SUCCESS, $success, $comparison);

        return $this;
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUserId($userId = null, ?string $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(AuthTokenLoginTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(AuthTokenLoginTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(AuthTokenLoginTableMap::COL_USER_ID, $userId, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \User object
     *
     * @param \User|ObjectCollection $user The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUser($user, ?string $comparison = null)
    {
        if ($user instanceof \User) {
            return $this
                ->addUsingAlias(AuthTokenLoginTableMap::COL_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(AuthTokenLoginTableMap::COL_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinUser(?string $relationAlias = null, ?string $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\UserQuery');
    }

    /**
     * Use the User relation User object
     *
     * @param callable(\UserQuery):\UserQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUserQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useUserQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to User table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \UserQuery The inner query object of the EXISTS statement
     */
    public function useUserExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('User', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to User table for a NOT EXISTS query.
     *
     * @see useUserExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \UserQuery The inner query object of the NOT EXISTS statement
     */
    public function useUserNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('User', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Exclude object from result
     *
     * @param ChildAuthTokenLogin $authTokenLogin Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($authTokenLogin = null)
    {
        if ($authTokenLogin) {
            $this->addUsingAlias(AuthTokenLoginTableMap::COL_ID, $authTokenLogin->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the auth_token_logins table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AuthTokenLoginTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AuthTokenLoginTableMap::clearInstancePool();
            AuthTokenLoginTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AuthTokenLoginTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AuthTokenLoginTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AuthTokenLoginTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AuthTokenLoginTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

}
