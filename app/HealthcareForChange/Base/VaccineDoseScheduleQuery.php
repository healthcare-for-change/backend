<?php

namespace Base;

use \VaccineDoseSchedule as ChildVaccineDoseSchedule;
use \VaccineDoseScheduleQuery as ChildVaccineDoseScheduleQuery;
use \Exception;
use \PDO;
use Map\VaccineDoseScheduleTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'vaccine_dose_schedule' table.
 *
 * Contains the required dose schedule of this vaccine in order to be able to display recommendation on further vaccinations
 *
 * @method     ChildVaccineDoseScheduleQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildVaccineDoseScheduleQuery orderByMinMonth($order = Criteria::ASC) Order by the min_month column
 * @method     ChildVaccineDoseScheduleQuery orderByMaxMonth($order = Criteria::ASC) Order by the max_month column
 * @method     ChildVaccineDoseScheduleQuery orderByComment($order = Criteria::ASC) Order by the comment column
 * @method     ChildVaccineDoseScheduleQuery orderByVaccineUuid($order = Criteria::ASC) Order by the vaccine_uuid column
 *
 * @method     ChildVaccineDoseScheduleQuery groupByUuid() Group by the uuid column
 * @method     ChildVaccineDoseScheduleQuery groupByMinMonth() Group by the min_month column
 * @method     ChildVaccineDoseScheduleQuery groupByMaxMonth() Group by the max_month column
 * @method     ChildVaccineDoseScheduleQuery groupByComment() Group by the comment column
 * @method     ChildVaccineDoseScheduleQuery groupByVaccineUuid() Group by the vaccine_uuid column
 *
 * @method     ChildVaccineDoseScheduleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildVaccineDoseScheduleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildVaccineDoseScheduleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildVaccineDoseScheduleQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildVaccineDoseScheduleQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildVaccineDoseScheduleQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildVaccineDoseScheduleQuery leftJoinVaccine($relationAlias = null) Adds a LEFT JOIN clause to the query using the Vaccine relation
 * @method     ChildVaccineDoseScheduleQuery rightJoinVaccine($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Vaccine relation
 * @method     ChildVaccineDoseScheduleQuery innerJoinVaccine($relationAlias = null) Adds a INNER JOIN clause to the query using the Vaccine relation
 *
 * @method     ChildVaccineDoseScheduleQuery joinWithVaccine($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Vaccine relation
 *
 * @method     ChildVaccineDoseScheduleQuery leftJoinWithVaccine() Adds a LEFT JOIN clause and with to the query using the Vaccine relation
 * @method     ChildVaccineDoseScheduleQuery rightJoinWithVaccine() Adds a RIGHT JOIN clause and with to the query using the Vaccine relation
 * @method     ChildVaccineDoseScheduleQuery innerJoinWithVaccine() Adds a INNER JOIN clause and with to the query using the Vaccine relation
 *
 * @method     \VaccineQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildVaccineDoseSchedule|null findOne(?ConnectionInterface $con = null) Return the first ChildVaccineDoseSchedule matching the query
 * @method     ChildVaccineDoseSchedule findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildVaccineDoseSchedule matching the query, or a new ChildVaccineDoseSchedule object populated from the query conditions when no match is found
 *
 * @method     ChildVaccineDoseSchedule|null findOneByUuid(string $uuid) Return the first ChildVaccineDoseSchedule filtered by the uuid column
 * @method     ChildVaccineDoseSchedule|null findOneByMinMonth(int $min_month) Return the first ChildVaccineDoseSchedule filtered by the min_month column
 * @method     ChildVaccineDoseSchedule|null findOneByMaxMonth(int $max_month) Return the first ChildVaccineDoseSchedule filtered by the max_month column
 * @method     ChildVaccineDoseSchedule|null findOneByComment(string $comment) Return the first ChildVaccineDoseSchedule filtered by the comment column
 * @method     ChildVaccineDoseSchedule|null findOneByVaccineUuid(string $vaccine_uuid) Return the first ChildVaccineDoseSchedule filtered by the vaccine_uuid column *

 * @method     ChildVaccineDoseSchedule requirePk($key, ?ConnectionInterface $con = null) Return the ChildVaccineDoseSchedule by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccineDoseSchedule requireOne(?ConnectionInterface $con = null) Return the first ChildVaccineDoseSchedule matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVaccineDoseSchedule requireOneByUuid(string $uuid) Return the first ChildVaccineDoseSchedule filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccineDoseSchedule requireOneByMinMonth(int $min_month) Return the first ChildVaccineDoseSchedule filtered by the min_month column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccineDoseSchedule requireOneByMaxMonth(int $max_month) Return the first ChildVaccineDoseSchedule filtered by the max_month column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccineDoseSchedule requireOneByComment(string $comment) Return the first ChildVaccineDoseSchedule filtered by the comment column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVaccineDoseSchedule requireOneByVaccineUuid(string $vaccine_uuid) Return the first ChildVaccineDoseSchedule filtered by the vaccine_uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVaccineDoseSchedule[]|Collection find(?ConnectionInterface $con = null) Return ChildVaccineDoseSchedule objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildVaccineDoseSchedule> find(?ConnectionInterface $con = null) Return ChildVaccineDoseSchedule objects based on current ModelCriteria
 * @method     ChildVaccineDoseSchedule[]|Collection findByUuid(string $uuid) Return ChildVaccineDoseSchedule objects filtered by the uuid column
 * @psalm-method Collection&\Traversable<ChildVaccineDoseSchedule> findByUuid(string $uuid) Return ChildVaccineDoseSchedule objects filtered by the uuid column
 * @method     ChildVaccineDoseSchedule[]|Collection findByMinMonth(int $min_month) Return ChildVaccineDoseSchedule objects filtered by the min_month column
 * @psalm-method Collection&\Traversable<ChildVaccineDoseSchedule> findByMinMonth(int $min_month) Return ChildVaccineDoseSchedule objects filtered by the min_month column
 * @method     ChildVaccineDoseSchedule[]|Collection findByMaxMonth(int $max_month) Return ChildVaccineDoseSchedule objects filtered by the max_month column
 * @psalm-method Collection&\Traversable<ChildVaccineDoseSchedule> findByMaxMonth(int $max_month) Return ChildVaccineDoseSchedule objects filtered by the max_month column
 * @method     ChildVaccineDoseSchedule[]|Collection findByComment(string $comment) Return ChildVaccineDoseSchedule objects filtered by the comment column
 * @psalm-method Collection&\Traversable<ChildVaccineDoseSchedule> findByComment(string $comment) Return ChildVaccineDoseSchedule objects filtered by the comment column
 * @method     ChildVaccineDoseSchedule[]|Collection findByVaccineUuid(string $vaccine_uuid) Return ChildVaccineDoseSchedule objects filtered by the vaccine_uuid column
 * @psalm-method Collection&\Traversable<ChildVaccineDoseSchedule> findByVaccineUuid(string $vaccine_uuid) Return ChildVaccineDoseSchedule objects filtered by the vaccine_uuid column
 * @method     ChildVaccineDoseSchedule[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildVaccineDoseSchedule> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class VaccineDoseScheduleQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\VaccineDoseScheduleQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\VaccineDoseSchedule', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildVaccineDoseScheduleQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildVaccineDoseScheduleQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildVaccineDoseScheduleQuery) {
            return $criteria;
        }
        $query = new ChildVaccineDoseScheduleQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildVaccineDoseSchedule|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VaccineDoseScheduleTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = VaccineDoseScheduleTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVaccineDoseSchedule A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT uuid, min_month, max_month, comment, vaccine_uuid FROM vaccine_dose_schedule WHERE uuid = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildVaccineDoseSchedule $obj */
            $obj = new ChildVaccineDoseSchedule();
            $obj->hydrate($row);
            VaccineDoseScheduleTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildVaccineDoseSchedule|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(VaccineDoseScheduleTableMap::COL_UUID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(VaccineDoseScheduleTableMap::COL_UUID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * $query->filterByUuid(['foo', 'bar']); // WHERE uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $uuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccineDoseScheduleTableMap::COL_UUID, $uuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the min_month column
     *
     * Example usage:
     * <code>
     * $query->filterByMinMonth(1234); // WHERE min_month = 1234
     * $query->filterByMinMonth(array(12, 34)); // WHERE min_month IN (12, 34)
     * $query->filterByMinMonth(array('min' => 12)); // WHERE min_month > 12
     * </code>
     *
     * @param mixed $minMonth The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMinMonth($minMonth = null, ?string $comparison = null)
    {
        if (is_array($minMonth)) {
            $useMinMax = false;
            if (isset($minMonth['min'])) {
                $this->addUsingAlias(VaccineDoseScheduleTableMap::COL_MIN_MONTH, $minMonth['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($minMonth['max'])) {
                $this->addUsingAlias(VaccineDoseScheduleTableMap::COL_MIN_MONTH, $minMonth['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccineDoseScheduleTableMap::COL_MIN_MONTH, $minMonth, $comparison);

        return $this;
    }

    /**
     * Filter the query on the max_month column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxMonth(1234); // WHERE max_month = 1234
     * $query->filterByMaxMonth(array(12, 34)); // WHERE max_month IN (12, 34)
     * $query->filterByMaxMonth(array('min' => 12)); // WHERE max_month > 12
     * </code>
     *
     * @param mixed $maxMonth The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMaxMonth($maxMonth = null, ?string $comparison = null)
    {
        if (is_array($maxMonth)) {
            $useMinMax = false;
            if (isset($maxMonth['min'])) {
                $this->addUsingAlias(VaccineDoseScheduleTableMap::COL_MAX_MONTH, $maxMonth['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxMonth['max'])) {
                $this->addUsingAlias(VaccineDoseScheduleTableMap::COL_MAX_MONTH, $maxMonth['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccineDoseScheduleTableMap::COL_MAX_MONTH, $maxMonth, $comparison);

        return $this;
    }

    /**
     * Filter the query on the comment column
     *
     * Example usage:
     * <code>
     * $query->filterByComment('fooValue');   // WHERE comment = 'fooValue'
     * $query->filterByComment('%fooValue%', Criteria::LIKE); // WHERE comment LIKE '%fooValue%'
     * $query->filterByComment(['foo', 'bar']); // WHERE comment IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $comment The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByComment($comment = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comment)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccineDoseScheduleTableMap::COL_COMMENT, $comment, $comparison);

        return $this;
    }

    /**
     * Filter the query on the vaccine_uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByVaccineUuid('fooValue');   // WHERE vaccine_uuid = 'fooValue'
     * $query->filterByVaccineUuid('%fooValue%', Criteria::LIKE); // WHERE vaccine_uuid LIKE '%fooValue%'
     * $query->filterByVaccineUuid(['foo', 'bar']); // WHERE vaccine_uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $vaccineUuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVaccineUuid($vaccineUuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($vaccineUuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(VaccineDoseScheduleTableMap::COL_VACCINE_UUID, $vaccineUuid, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \Vaccine object
     *
     * @param \Vaccine|ObjectCollection $vaccine The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVaccine($vaccine, ?string $comparison = null)
    {
        if ($vaccine instanceof \Vaccine) {
            return $this
                ->addUsingAlias(VaccineDoseScheduleTableMap::COL_VACCINE_UUID, $vaccine->getUuid(), $comparison);
        } elseif ($vaccine instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(VaccineDoseScheduleTableMap::COL_VACCINE_UUID, $vaccine->toKeyValue('PrimaryKey', 'Uuid'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByVaccine() only accepts arguments of type \Vaccine or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Vaccine relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinVaccine(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Vaccine');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Vaccine');
        }

        return $this;
    }

    /**
     * Use the Vaccine relation Vaccine object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VaccineQuery A secondary query class using the current class as primary query
     */
    public function useVaccineQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVaccine($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Vaccine', '\VaccineQuery');
    }

    /**
     * Use the Vaccine relation Vaccine object
     *
     * @param callable(\VaccineQuery):\VaccineQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withVaccineQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useVaccineQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Vaccine table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \VaccineQuery The inner query object of the EXISTS statement
     */
    public function useVaccineExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Vaccine', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Vaccine table for a NOT EXISTS query.
     *
     * @see useVaccineExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \VaccineQuery The inner query object of the NOT EXISTS statement
     */
    public function useVaccineNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Vaccine', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Exclude object from result
     *
     * @param ChildVaccineDoseSchedule $vaccineDoseSchedule Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($vaccineDoseSchedule = null)
    {
        if ($vaccineDoseSchedule) {
            $this->addUsingAlias(VaccineDoseScheduleTableMap::COL_UUID, $vaccineDoseSchedule->getUuid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the vaccine_dose_schedule table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VaccineDoseScheduleTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VaccineDoseScheduleTableMap::clearInstancePool();
            VaccineDoseScheduleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VaccineDoseScheduleTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(VaccineDoseScheduleTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            VaccineDoseScheduleTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            VaccineDoseScheduleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

}
