<?php

namespace Base;

use \Patient as ChildPatient;
use \PatientQuery as ChildPatientQuery;
use \Exception;
use \PDO;
use Map\PatientTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'patient' table.
 *
 * Contains patient information
 *
 * @method     ChildPatientQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildPatientQuery orderByFirstName($order = Criteria::ASC) Order by the first_name column
 * @method     ChildPatientQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildPatientQuery orderByGender($order = Criteria::ASC) Order by the gender column
 * @method     ChildPatientQuery orderByDateOfBirth($order = Criteria::ASC) Order by the date_of_birth column
 * @method     ChildPatientQuery orderByDateOfDeath($order = Criteria::ASC) Order by the date_of_death column
 * @method     ChildPatientQuery orderByUnclearBirthdate($order = Criteria::ASC) Order by the unclear_birthdate column
 * @method     ChildPatientQuery orderByMobileNumber($order = Criteria::ASC) Order by the mobile_number column
 * @method     ChildPatientQuery orderByTenantUuid($order = Criteria::ASC) Order by the tenant_uuid column
 * @method     ChildPatientQuery orderByReligionId($order = Criteria::ASC) Order by the religion_id column
 * @method     ChildPatientQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPatientQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPatientQuery groupByUuid() Group by the uuid column
 * @method     ChildPatientQuery groupByFirstName() Group by the first_name column
 * @method     ChildPatientQuery groupByName() Group by the name column
 * @method     ChildPatientQuery groupByGender() Group by the gender column
 * @method     ChildPatientQuery groupByDateOfBirth() Group by the date_of_birth column
 * @method     ChildPatientQuery groupByDateOfDeath() Group by the date_of_death column
 * @method     ChildPatientQuery groupByUnclearBirthdate() Group by the unclear_birthdate column
 * @method     ChildPatientQuery groupByMobileNumber() Group by the mobile_number column
 * @method     ChildPatientQuery groupByTenantUuid() Group by the tenant_uuid column
 * @method     ChildPatientQuery groupByReligionId() Group by the religion_id column
 * @method     ChildPatientQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPatientQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPatientQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPatientQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPatientQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPatientQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPatientQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPatientQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPatientQuery leftJoinTenant($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tenant relation
 * @method     ChildPatientQuery rightJoinTenant($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tenant relation
 * @method     ChildPatientQuery innerJoinTenant($relationAlias = null) Adds a INNER JOIN clause to the query using the Tenant relation
 *
 * @method     ChildPatientQuery joinWithTenant($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Tenant relation
 *
 * @method     ChildPatientQuery leftJoinWithTenant() Adds a LEFT JOIN clause and with to the query using the Tenant relation
 * @method     ChildPatientQuery rightJoinWithTenant() Adds a RIGHT JOIN clause and with to the query using the Tenant relation
 * @method     ChildPatientQuery innerJoinWithTenant() Adds a INNER JOIN clause and with to the query using the Tenant relation
 *
 * @method     ChildPatientQuery leftJoinReligion($relationAlias = null) Adds a LEFT JOIN clause to the query using the Religion relation
 * @method     ChildPatientQuery rightJoinReligion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Religion relation
 * @method     ChildPatientQuery innerJoinReligion($relationAlias = null) Adds a INNER JOIN clause to the query using the Religion relation
 *
 * @method     ChildPatientQuery joinWithReligion($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Religion relation
 *
 * @method     ChildPatientQuery leftJoinWithReligion() Adds a LEFT JOIN clause and with to the query using the Religion relation
 * @method     ChildPatientQuery rightJoinWithReligion() Adds a RIGHT JOIN clause and with to the query using the Religion relation
 * @method     ChildPatientQuery innerJoinWithReligion() Adds a INNER JOIN clause and with to the query using the Religion relation
 *
 * @method     ChildPatientQuery leftJoinVaccinationHistory($relationAlias = null) Adds a LEFT JOIN clause to the query using the VaccinationHistory relation
 * @method     ChildPatientQuery rightJoinVaccinationHistory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VaccinationHistory relation
 * @method     ChildPatientQuery innerJoinVaccinationHistory($relationAlias = null) Adds a INNER JOIN clause to the query using the VaccinationHistory relation
 *
 * @method     ChildPatientQuery joinWithVaccinationHistory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the VaccinationHistory relation
 *
 * @method     ChildPatientQuery leftJoinWithVaccinationHistory() Adds a LEFT JOIN clause and with to the query using the VaccinationHistory relation
 * @method     ChildPatientQuery rightJoinWithVaccinationHistory() Adds a RIGHT JOIN clause and with to the query using the VaccinationHistory relation
 * @method     ChildPatientQuery innerJoinWithVaccinationHistory() Adds a INNER JOIN clause and with to the query using the VaccinationHistory relation
 *
 * @method     ChildPatientQuery leftJoinPhysicalExamination($relationAlias = null) Adds a LEFT JOIN clause to the query using the PhysicalExamination relation
 * @method     ChildPatientQuery rightJoinPhysicalExamination($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PhysicalExamination relation
 * @method     ChildPatientQuery innerJoinPhysicalExamination($relationAlias = null) Adds a INNER JOIN clause to the query using the PhysicalExamination relation
 *
 * @method     ChildPatientQuery joinWithPhysicalExamination($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PhysicalExamination relation
 *
 * @method     ChildPatientQuery leftJoinWithPhysicalExamination() Adds a LEFT JOIN clause and with to the query using the PhysicalExamination relation
 * @method     ChildPatientQuery rightJoinWithPhysicalExamination() Adds a RIGHT JOIN clause and with to the query using the PhysicalExamination relation
 * @method     ChildPatientQuery innerJoinWithPhysicalExamination() Adds a INNER JOIN clause and with to the query using the PhysicalExamination relation
 *
 * @method     ChildPatientQuery leftJoinDewormingHistory($relationAlias = null) Adds a LEFT JOIN clause to the query using the DewormingHistory relation
 * @method     ChildPatientQuery rightJoinDewormingHistory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DewormingHistory relation
 * @method     ChildPatientQuery innerJoinDewormingHistory($relationAlias = null) Adds a INNER JOIN clause to the query using the DewormingHistory relation
 *
 * @method     ChildPatientQuery joinWithDewormingHistory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DewormingHistory relation
 *
 * @method     ChildPatientQuery leftJoinWithDewormingHistory() Adds a LEFT JOIN clause and with to the query using the DewormingHistory relation
 * @method     ChildPatientQuery rightJoinWithDewormingHistory() Adds a RIGHT JOIN clause and with to the query using the DewormingHistory relation
 * @method     ChildPatientQuery innerJoinWithDewormingHistory() Adds a INNER JOIN clause and with to the query using the DewormingHistory relation
 *
 * @method     ChildPatientQuery leftJoinMedicalHistory($relationAlias = null) Adds a LEFT JOIN clause to the query using the MedicalHistory relation
 * @method     ChildPatientQuery rightJoinMedicalHistory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MedicalHistory relation
 * @method     ChildPatientQuery innerJoinMedicalHistory($relationAlias = null) Adds a INNER JOIN clause to the query using the MedicalHistory relation
 *
 * @method     ChildPatientQuery joinWithMedicalHistory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MedicalHistory relation
 *
 * @method     ChildPatientQuery leftJoinWithMedicalHistory() Adds a LEFT JOIN clause and with to the query using the MedicalHistory relation
 * @method     ChildPatientQuery rightJoinWithMedicalHistory() Adds a RIGHT JOIN clause and with to the query using the MedicalHistory relation
 * @method     ChildPatientQuery innerJoinWithMedicalHistory() Adds a INNER JOIN clause and with to the query using the MedicalHistory relation
 *
 * @method     ChildPatientQuery leftJoinPrescription($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prescription relation
 * @method     ChildPatientQuery rightJoinPrescription($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prescription relation
 * @method     ChildPatientQuery innerJoinPrescription($relationAlias = null) Adds a INNER JOIN clause to the query using the Prescription relation
 *
 * @method     ChildPatientQuery joinWithPrescription($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prescription relation
 *
 * @method     ChildPatientQuery leftJoinWithPrescription() Adds a LEFT JOIN clause and with to the query using the Prescription relation
 * @method     ChildPatientQuery rightJoinWithPrescription() Adds a RIGHT JOIN clause and with to the query using the Prescription relation
 * @method     ChildPatientQuery innerJoinWithPrescription() Adds a INNER JOIN clause and with to the query using the Prescription relation
 *
 * @method     \TenantQuery|\ReligionQuery|\VaccinationHistoryQuery|\PhysicalExaminationQuery|\DewormingHistoryQuery|\MedicalHistoryQuery|\PrescriptionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPatient|null findOne(?ConnectionInterface $con = null) Return the first ChildPatient matching the query
 * @method     ChildPatient findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildPatient matching the query, or a new ChildPatient object populated from the query conditions when no match is found
 *
 * @method     ChildPatient|null findOneByUuid(string $uuid) Return the first ChildPatient filtered by the uuid column
 * @method     ChildPatient|null findOneByFirstName(string $first_name) Return the first ChildPatient filtered by the first_name column
 * @method     ChildPatient|null findOneByName(string $name) Return the first ChildPatient filtered by the name column
 * @method     ChildPatient|null findOneByGender(string $gender) Return the first ChildPatient filtered by the gender column
 * @method     ChildPatient|null findOneByDateOfBirth(string $date_of_birth) Return the first ChildPatient filtered by the date_of_birth column
 * @method     ChildPatient|null findOneByDateOfDeath(string $date_of_death) Return the first ChildPatient filtered by the date_of_death column
 * @method     ChildPatient|null findOneByUnclearBirthdate(boolean $unclear_birthdate) Return the first ChildPatient filtered by the unclear_birthdate column
 * @method     ChildPatient|null findOneByMobileNumber(string $mobile_number) Return the first ChildPatient filtered by the mobile_number column
 * @method     ChildPatient|null findOneByTenantUuid(string $tenant_uuid) Return the first ChildPatient filtered by the tenant_uuid column
 * @method     ChildPatient|null findOneByReligionId(int $religion_id) Return the first ChildPatient filtered by the religion_id column
 * @method     ChildPatient|null findOneByCreatedAt(string $created_at) Return the first ChildPatient filtered by the created_at column
 * @method     ChildPatient|null findOneByUpdatedAt(string $updated_at) Return the first ChildPatient filtered by the updated_at column *

 * @method     ChildPatient requirePk($key, ?ConnectionInterface $con = null) Return the ChildPatient by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPatient requireOne(?ConnectionInterface $con = null) Return the first ChildPatient matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPatient requireOneByUuid(string $uuid) Return the first ChildPatient filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPatient requireOneByFirstName(string $first_name) Return the first ChildPatient filtered by the first_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPatient requireOneByName(string $name) Return the first ChildPatient filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPatient requireOneByGender(string $gender) Return the first ChildPatient filtered by the gender column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPatient requireOneByDateOfBirth(string $date_of_birth) Return the first ChildPatient filtered by the date_of_birth column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPatient requireOneByDateOfDeath(string $date_of_death) Return the first ChildPatient filtered by the date_of_death column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPatient requireOneByUnclearBirthdate(boolean $unclear_birthdate) Return the first ChildPatient filtered by the unclear_birthdate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPatient requireOneByMobileNumber(string $mobile_number) Return the first ChildPatient filtered by the mobile_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPatient requireOneByTenantUuid(string $tenant_uuid) Return the first ChildPatient filtered by the tenant_uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPatient requireOneByReligionId(int $religion_id) Return the first ChildPatient filtered by the religion_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPatient requireOneByCreatedAt(string $created_at) Return the first ChildPatient filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPatient requireOneByUpdatedAt(string $updated_at) Return the first ChildPatient filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPatient[]|Collection find(?ConnectionInterface $con = null) Return ChildPatient objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildPatient> find(?ConnectionInterface $con = null) Return ChildPatient objects based on current ModelCriteria
 * @method     ChildPatient[]|Collection findByUuid(string $uuid) Return ChildPatient objects filtered by the uuid column
 * @psalm-method Collection&\Traversable<ChildPatient> findByUuid(string $uuid) Return ChildPatient objects filtered by the uuid column
 * @method     ChildPatient[]|Collection findByFirstName(string $first_name) Return ChildPatient objects filtered by the first_name column
 * @psalm-method Collection&\Traversable<ChildPatient> findByFirstName(string $first_name) Return ChildPatient objects filtered by the first_name column
 * @method     ChildPatient[]|Collection findByName(string $name) Return ChildPatient objects filtered by the name column
 * @psalm-method Collection&\Traversable<ChildPatient> findByName(string $name) Return ChildPatient objects filtered by the name column
 * @method     ChildPatient[]|Collection findByGender(string $gender) Return ChildPatient objects filtered by the gender column
 * @psalm-method Collection&\Traversable<ChildPatient> findByGender(string $gender) Return ChildPatient objects filtered by the gender column
 * @method     ChildPatient[]|Collection findByDateOfBirth(string $date_of_birth) Return ChildPatient objects filtered by the date_of_birth column
 * @psalm-method Collection&\Traversable<ChildPatient> findByDateOfBirth(string $date_of_birth) Return ChildPatient objects filtered by the date_of_birth column
 * @method     ChildPatient[]|Collection findByDateOfDeath(string $date_of_death) Return ChildPatient objects filtered by the date_of_death column
 * @psalm-method Collection&\Traversable<ChildPatient> findByDateOfDeath(string $date_of_death) Return ChildPatient objects filtered by the date_of_death column
 * @method     ChildPatient[]|Collection findByUnclearBirthdate(boolean $unclear_birthdate) Return ChildPatient objects filtered by the unclear_birthdate column
 * @psalm-method Collection&\Traversable<ChildPatient> findByUnclearBirthdate(boolean $unclear_birthdate) Return ChildPatient objects filtered by the unclear_birthdate column
 * @method     ChildPatient[]|Collection findByMobileNumber(string $mobile_number) Return ChildPatient objects filtered by the mobile_number column
 * @psalm-method Collection&\Traversable<ChildPatient> findByMobileNumber(string $mobile_number) Return ChildPatient objects filtered by the mobile_number column
 * @method     ChildPatient[]|Collection findByTenantUuid(string $tenant_uuid) Return ChildPatient objects filtered by the tenant_uuid column
 * @psalm-method Collection&\Traversable<ChildPatient> findByTenantUuid(string $tenant_uuid) Return ChildPatient objects filtered by the tenant_uuid column
 * @method     ChildPatient[]|Collection findByReligionId(int $religion_id) Return ChildPatient objects filtered by the religion_id column
 * @psalm-method Collection&\Traversable<ChildPatient> findByReligionId(int $religion_id) Return ChildPatient objects filtered by the religion_id column
 * @method     ChildPatient[]|Collection findByCreatedAt(string $created_at) Return ChildPatient objects filtered by the created_at column
 * @psalm-method Collection&\Traversable<ChildPatient> findByCreatedAt(string $created_at) Return ChildPatient objects filtered by the created_at column
 * @method     ChildPatient[]|Collection findByUpdatedAt(string $updated_at) Return ChildPatient objects filtered by the updated_at column
 * @psalm-method Collection&\Traversable<ChildPatient> findByUpdatedAt(string $updated_at) Return ChildPatient objects filtered by the updated_at column
 * @method     ChildPatient[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildPatient> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PatientQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PatientQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'HealtchareForChange', $modelName = '\\Patient', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPatientQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPatientQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildPatientQuery) {
            return $criteria;
        }
        $query = new ChildPatientQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPatient|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PatientTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PatientTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPatient A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT uuid, first_name, name, gender, date_of_birth, date_of_death, unclear_birthdate, mobile_number, tenant_uuid, religion_id, created_at, updated_at FROM patient WHERE uuid = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPatient $obj */
            $obj = new ChildPatient();
            $obj->hydrate($row);
            PatientTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildPatient|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(PatientTableMap::COL_UUID, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(PatientTableMap::COL_UUID, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * $query->filterByUuid(['foo', 'bar']); // WHERE uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $uuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PatientTableMap::COL_UUID, $uuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the first_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstName('fooValue');   // WHERE first_name = 'fooValue'
     * $query->filterByFirstName('%fooValue%', Criteria::LIKE); // WHERE first_name LIKE '%fooValue%'
     * $query->filterByFirstName(['foo', 'bar']); // WHERE first_name IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $firstName The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByFirstName($firstName = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstName)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PatientTableMap::COL_FIRST_NAME, $firstName, $comparison);

        return $this;
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * $query->filterByName(['foo', 'bar']); // WHERE name IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $name The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByName($name = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PatientTableMap::COL_NAME, $name, $comparison);

        return $this;
    }

    /**
     * Filter the query on the gender column
     *
     * Example usage:
     * <code>
     * $query->filterByGender('fooValue');   // WHERE gender = 'fooValue'
     * $query->filterByGender('%fooValue%', Criteria::LIKE); // WHERE gender LIKE '%fooValue%'
     * $query->filterByGender(['foo', 'bar']); // WHERE gender IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $gender The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByGender($gender = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gender)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PatientTableMap::COL_GENDER, $gender, $comparison);

        return $this;
    }

    /**
     * Filter the query on the date_of_birth column
     *
     * Example usage:
     * <code>
     * $query->filterByDateOfBirth('2011-03-14'); // WHERE date_of_birth = '2011-03-14'
     * $query->filterByDateOfBirth('now'); // WHERE date_of_birth = '2011-03-14'
     * $query->filterByDateOfBirth(array('max' => 'yesterday')); // WHERE date_of_birth > '2011-03-13'
     * </code>
     *
     * @param mixed $dateOfBirth The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDateOfBirth($dateOfBirth = null, ?string $comparison = null)
    {
        if (is_array($dateOfBirth)) {
            $useMinMax = false;
            if (isset($dateOfBirth['min'])) {
                $this->addUsingAlias(PatientTableMap::COL_DATE_OF_BIRTH, $dateOfBirth['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateOfBirth['max'])) {
                $this->addUsingAlias(PatientTableMap::COL_DATE_OF_BIRTH, $dateOfBirth['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PatientTableMap::COL_DATE_OF_BIRTH, $dateOfBirth, $comparison);

        return $this;
    }

    /**
     * Filter the query on the date_of_death column
     *
     * Example usage:
     * <code>
     * $query->filterByDateOfDeath('2011-03-14'); // WHERE date_of_death = '2011-03-14'
     * $query->filterByDateOfDeath('now'); // WHERE date_of_death = '2011-03-14'
     * $query->filterByDateOfDeath(array('max' => 'yesterday')); // WHERE date_of_death > '2011-03-13'
     * </code>
     *
     * @param mixed $dateOfDeath The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDateOfDeath($dateOfDeath = null, ?string $comparison = null)
    {
        if (is_array($dateOfDeath)) {
            $useMinMax = false;
            if (isset($dateOfDeath['min'])) {
                $this->addUsingAlias(PatientTableMap::COL_DATE_OF_DEATH, $dateOfDeath['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateOfDeath['max'])) {
                $this->addUsingAlias(PatientTableMap::COL_DATE_OF_DEATH, $dateOfDeath['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PatientTableMap::COL_DATE_OF_DEATH, $dateOfDeath, $comparison);

        return $this;
    }

    /**
     * Filter the query on the unclear_birthdate column
     *
     * Example usage:
     * <code>
     * $query->filterByUnclearBirthdate(true); // WHERE unclear_birthdate = true
     * $query->filterByUnclearBirthdate('yes'); // WHERE unclear_birthdate = true
     * </code>
     *
     * @param bool|string $unclearBirthdate The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUnclearBirthdate($unclearBirthdate = null, ?string $comparison = null)
    {
        if (is_string($unclearBirthdate)) {
            $unclearBirthdate = in_array(strtolower($unclearBirthdate), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        $this->addUsingAlias(PatientTableMap::COL_UNCLEAR_BIRTHDATE, $unclearBirthdate, $comparison);

        return $this;
    }

    /**
     * Filter the query on the mobile_number column
     *
     * Example usage:
     * <code>
     * $query->filterByMobileNumber('fooValue');   // WHERE mobile_number = 'fooValue'
     * $query->filterByMobileNumber('%fooValue%', Criteria::LIKE); // WHERE mobile_number LIKE '%fooValue%'
     * $query->filterByMobileNumber(['foo', 'bar']); // WHERE mobile_number IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $mobileNumber The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMobileNumber($mobileNumber = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mobileNumber)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PatientTableMap::COL_MOBILE_NUMBER, $mobileNumber, $comparison);

        return $this;
    }

    /**
     * Filter the query on the tenant_uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByTenantUuid('fooValue');   // WHERE tenant_uuid = 'fooValue'
     * $query->filterByTenantUuid('%fooValue%', Criteria::LIKE); // WHERE tenant_uuid LIKE '%fooValue%'
     * $query->filterByTenantUuid(['foo', 'bar']); // WHERE tenant_uuid IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $tenantUuid The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByTenantUuid($tenantUuid = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tenantUuid)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PatientTableMap::COL_TENANT_UUID, $tenantUuid, $comparison);

        return $this;
    }

    /**
     * Filter the query on the religion_id column
     *
     * Example usage:
     * <code>
     * $query->filterByReligionId(1234); // WHERE religion_id = 1234
     * $query->filterByReligionId(array(12, 34)); // WHERE religion_id IN (12, 34)
     * $query->filterByReligionId(array('min' => 12)); // WHERE religion_id > 12
     * </code>
     *
     * @see       filterByReligion()
     *
     * @param mixed $religionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByReligionId($religionId = null, ?string $comparison = null)
    {
        if (is_array($religionId)) {
            $useMinMax = false;
            if (isset($religionId['min'])) {
                $this->addUsingAlias(PatientTableMap::COL_RELIGION_ID, $religionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($religionId['max'])) {
                $this->addUsingAlias(PatientTableMap::COL_RELIGION_ID, $religionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PatientTableMap::COL_RELIGION_ID, $religionId, $comparison);

        return $this;
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, ?string $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PatientTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PatientTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PatientTableMap::COL_CREATED_AT, $createdAt, $comparison);

        return $this;
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, ?string $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PatientTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PatientTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(PatientTableMap::COL_UPDATED_AT, $updatedAt, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \Tenant object
     *
     * @param \Tenant|ObjectCollection $tenant The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByTenant($tenant, ?string $comparison = null)
    {
        if ($tenant instanceof \Tenant) {
            return $this
                ->addUsingAlias(PatientTableMap::COL_TENANT_UUID, $tenant->getUuid(), $comparison);
        } elseif ($tenant instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(PatientTableMap::COL_TENANT_UUID, $tenant->toKeyValue('PrimaryKey', 'Uuid'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByTenant() only accepts arguments of type \Tenant or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tenant relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinTenant(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tenant');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tenant');
        }

        return $this;
    }

    /**
     * Use the Tenant relation Tenant object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TenantQuery A secondary query class using the current class as primary query
     */
    public function useTenantQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTenant($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tenant', '\TenantQuery');
    }

    /**
     * Use the Tenant relation Tenant object
     *
     * @param callable(\TenantQuery):\TenantQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withTenantQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useTenantQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Tenant table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \TenantQuery The inner query object of the EXISTS statement
     */
    public function useTenantExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Tenant', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Tenant table for a NOT EXISTS query.
     *
     * @see useTenantExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \TenantQuery The inner query object of the NOT EXISTS statement
     */
    public function useTenantNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Tenant', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \Religion object
     *
     * @param \Religion|ObjectCollection $religion The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByReligion($religion, ?string $comparison = null)
    {
        if ($religion instanceof \Religion) {
            return $this
                ->addUsingAlias(PatientTableMap::COL_RELIGION_ID, $religion->getId(), $comparison);
        } elseif ($religion instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(PatientTableMap::COL_RELIGION_ID, $religion->toKeyValue('PrimaryKey', 'Id'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByReligion() only accepts arguments of type \Religion or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Religion relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinReligion(?string $relationAlias = null, ?string $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Religion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Religion');
        }

        return $this;
    }

    /**
     * Use the Religion relation Religion object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ReligionQuery A secondary query class using the current class as primary query
     */
    public function useReligionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinReligion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Religion', '\ReligionQuery');
    }

    /**
     * Use the Religion relation Religion object
     *
     * @param callable(\ReligionQuery):\ReligionQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withReligionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useReligionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Religion table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \ReligionQuery The inner query object of the EXISTS statement
     */
    public function useReligionExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Religion', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Religion table for a NOT EXISTS query.
     *
     * @see useReligionExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \ReligionQuery The inner query object of the NOT EXISTS statement
     */
    public function useReligionNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Religion', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \VaccinationHistory object
     *
     * @param \VaccinationHistory|ObjectCollection $vaccinationHistory the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVaccinationHistory($vaccinationHistory, ?string $comparison = null)
    {
        if ($vaccinationHistory instanceof \VaccinationHistory) {
            $this
                ->addUsingAlias(PatientTableMap::COL_UUID, $vaccinationHistory->getPatientUuid(), $comparison);

            return $this;
        } elseif ($vaccinationHistory instanceof ObjectCollection) {
            $this
                ->useVaccinationHistoryQuery()
                ->filterByPrimaryKeys($vaccinationHistory->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByVaccinationHistory() only accepts arguments of type \VaccinationHistory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VaccinationHistory relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinVaccinationHistory(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VaccinationHistory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VaccinationHistory');
        }

        return $this;
    }

    /**
     * Use the VaccinationHistory relation VaccinationHistory object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VaccinationHistoryQuery A secondary query class using the current class as primary query
     */
    public function useVaccinationHistoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVaccinationHistory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VaccinationHistory', '\VaccinationHistoryQuery');
    }

    /**
     * Use the VaccinationHistory relation VaccinationHistory object
     *
     * @param callable(\VaccinationHistoryQuery):\VaccinationHistoryQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withVaccinationHistoryQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useVaccinationHistoryQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to VaccinationHistory table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \VaccinationHistoryQuery The inner query object of the EXISTS statement
     */
    public function useVaccinationHistoryExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('VaccinationHistory', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to VaccinationHistory table for a NOT EXISTS query.
     *
     * @see useVaccinationHistoryExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \VaccinationHistoryQuery The inner query object of the NOT EXISTS statement
     */
    public function useVaccinationHistoryNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('VaccinationHistory', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \PhysicalExamination object
     *
     * @param \PhysicalExamination|ObjectCollection $physicalExamination the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPhysicalExamination($physicalExamination, ?string $comparison = null)
    {
        if ($physicalExamination instanceof \PhysicalExamination) {
            $this
                ->addUsingAlias(PatientTableMap::COL_UUID, $physicalExamination->getPatientUuid(), $comparison);

            return $this;
        } elseif ($physicalExamination instanceof ObjectCollection) {
            $this
                ->usePhysicalExaminationQuery()
                ->filterByPrimaryKeys($physicalExamination->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByPhysicalExamination() only accepts arguments of type \PhysicalExamination or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PhysicalExamination relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPhysicalExamination(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PhysicalExamination');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PhysicalExamination');
        }

        return $this;
    }

    /**
     * Use the PhysicalExamination relation PhysicalExamination object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PhysicalExaminationQuery A secondary query class using the current class as primary query
     */
    public function usePhysicalExaminationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPhysicalExamination($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PhysicalExamination', '\PhysicalExaminationQuery');
    }

    /**
     * Use the PhysicalExamination relation PhysicalExamination object
     *
     * @param callable(\PhysicalExaminationQuery):\PhysicalExaminationQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPhysicalExaminationQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePhysicalExaminationQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to PhysicalExamination table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PhysicalExaminationQuery The inner query object of the EXISTS statement
     */
    public function usePhysicalExaminationExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('PhysicalExamination', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to PhysicalExamination table for a NOT EXISTS query.
     *
     * @see usePhysicalExaminationExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PhysicalExaminationQuery The inner query object of the NOT EXISTS statement
     */
    public function usePhysicalExaminationNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('PhysicalExamination', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \DewormingHistory object
     *
     * @param \DewormingHistory|ObjectCollection $dewormingHistory the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDewormingHistory($dewormingHistory, ?string $comparison = null)
    {
        if ($dewormingHistory instanceof \DewormingHistory) {
            $this
                ->addUsingAlias(PatientTableMap::COL_UUID, $dewormingHistory->getPatientUuid(), $comparison);

            return $this;
        } elseif ($dewormingHistory instanceof ObjectCollection) {
            $this
                ->useDewormingHistoryQuery()
                ->filterByPrimaryKeys($dewormingHistory->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByDewormingHistory() only accepts arguments of type \DewormingHistory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DewormingHistory relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinDewormingHistory(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DewormingHistory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DewormingHistory');
        }

        return $this;
    }

    /**
     * Use the DewormingHistory relation DewormingHistory object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \DewormingHistoryQuery A secondary query class using the current class as primary query
     */
    public function useDewormingHistoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDewormingHistory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DewormingHistory', '\DewormingHistoryQuery');
    }

    /**
     * Use the DewormingHistory relation DewormingHistory object
     *
     * @param callable(\DewormingHistoryQuery):\DewormingHistoryQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withDewormingHistoryQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useDewormingHistoryQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to DewormingHistory table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \DewormingHistoryQuery The inner query object of the EXISTS statement
     */
    public function useDewormingHistoryExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('DewormingHistory', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to DewormingHistory table for a NOT EXISTS query.
     *
     * @see useDewormingHistoryExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \DewormingHistoryQuery The inner query object of the NOT EXISTS statement
     */
    public function useDewormingHistoryNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('DewormingHistory', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \MedicalHistory object
     *
     * @param \MedicalHistory|ObjectCollection $medicalHistory the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMedicalHistory($medicalHistory, ?string $comparison = null)
    {
        if ($medicalHistory instanceof \MedicalHistory) {
            $this
                ->addUsingAlias(PatientTableMap::COL_UUID, $medicalHistory->getPatientUuid(), $comparison);

            return $this;
        } elseif ($medicalHistory instanceof ObjectCollection) {
            $this
                ->useMedicalHistoryQuery()
                ->filterByPrimaryKeys($medicalHistory->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByMedicalHistory() only accepts arguments of type \MedicalHistory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MedicalHistory relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinMedicalHistory(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MedicalHistory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MedicalHistory');
        }

        return $this;
    }

    /**
     * Use the MedicalHistory relation MedicalHistory object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MedicalHistoryQuery A secondary query class using the current class as primary query
     */
    public function useMedicalHistoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMedicalHistory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MedicalHistory', '\MedicalHistoryQuery');
    }

    /**
     * Use the MedicalHistory relation MedicalHistory object
     *
     * @param callable(\MedicalHistoryQuery):\MedicalHistoryQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withMedicalHistoryQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useMedicalHistoryQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to MedicalHistory table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \MedicalHistoryQuery The inner query object of the EXISTS statement
     */
    public function useMedicalHistoryExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('MedicalHistory', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to MedicalHistory table for a NOT EXISTS query.
     *
     * @see useMedicalHistoryExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \MedicalHistoryQuery The inner query object of the NOT EXISTS statement
     */
    public function useMedicalHistoryNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('MedicalHistory', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Filter the query by a related \Prescription object
     *
     * @param \Prescription|ObjectCollection $prescription the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrescription($prescription, ?string $comparison = null)
    {
        if ($prescription instanceof \Prescription) {
            $this
                ->addUsingAlias(PatientTableMap::COL_UUID, $prescription->getPatientUuid(), $comparison);

            return $this;
        } elseif ($prescription instanceof ObjectCollection) {
            $this
                ->usePrescriptionQuery()
                ->filterByPrimaryKeys($prescription->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByPrescription() only accepts arguments of type \Prescription or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prescription relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinPrescription(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prescription');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prescription');
        }

        return $this;
    }

    /**
     * Use the Prescription relation Prescription object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrescriptionQuery A secondary query class using the current class as primary query
     */
    public function usePrescriptionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrescription($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prescription', '\PrescriptionQuery');
    }

    /**
     * Use the Prescription relation Prescription object
     *
     * @param callable(\PrescriptionQuery):\PrescriptionQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPrescriptionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePrescriptionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }
    /**
     * Use the relation to Prescription table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string $typeOfExists Either ExistsCriterion::TYPE_EXISTS or ExistsCriterion::TYPE_NOT_EXISTS
     *
     * @return \PrescriptionQuery The inner query object of the EXISTS statement
     */
    public function usePrescriptionExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        return $this->useExistsQuery('Prescription', $modelAlias, $queryClass, $typeOfExists);
    }

    /**
     * Use the relation to Prescription table for a NOT EXISTS query.
     *
     * @see usePrescriptionExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \PrescriptionQuery The inner query object of the NOT EXISTS statement
     */
    public function usePrescriptionNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        return $this->useExistsQuery('Prescription', $modelAlias, $queryClass, 'NOT EXISTS');
    }
    /**
     * Exclude object from result
     *
     * @param ChildPatient $patient Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($patient = null)
    {
        if ($patient) {
            $this->addUsingAlias(PatientTableMap::COL_UUID, $patient->getUuid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the patient table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PatientTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PatientTableMap::clearInstancePool();
            PatientTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PatientTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PatientTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PatientTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PatientTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param int $nbDays Maximum age of the latest update in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        $this->addUsingAlias(PatientTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by update date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        $this->addDescendingOrderByColumn(PatientTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by update date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        $this->addAscendingOrderByColumn(PatientTableMap::COL_UPDATED_AT);

        return $this;
    }

    /**
     * Order by create date desc
     *
     * @return $this The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        $this->addDescendingOrderByColumn(PatientTableMap::COL_CREATED_AT);

        return $this;
    }

    /**
     * Filter by the latest created
     *
     * @param int $nbDays Maximum age of in days
     *
     * @return $this The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        $this->addUsingAlias(PatientTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);

        return $this;
    }

    /**
     * Order by create date asc
     *
     * @return $this The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        $this->addAscendingOrderByColumn(PatientTableMap::COL_CREATED_AT);

        return $this;
    }

}
