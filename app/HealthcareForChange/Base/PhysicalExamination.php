<?php

namespace Base;

use \Patient as ChildPatient;
use \PatientQuery as ChildPatientQuery;
use \PhysicalExaminationQuery as ChildPhysicalExaminationQuery;
use \User as ChildUser;
use \UserQuery as ChildUserQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\PhysicalExaminationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'physical_examination' table.
 *
 * Contains physical examination history of the respective patient
 *
 * @package    propel.generator..Base
 */
abstract class PhysicalExamination implements ActiveRecordInterface
{
    /**
     * TableMap class name
     *
     * @var string
     */
    public const TABLE_MAP = '\\Map\\PhysicalExaminationTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var bool
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var bool
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = [];

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = [];

    /**
     * The value for the uuid field.
     * PK
     * @var        string
     */
    protected $uuid;

    /**
     * The value for the timestamp field.
     * Timestamp of examination
     * @var        DateTime
     */
    protected $timestamp;

    /**
     * The value for the blood_pressure_systolic field.
     * Blood pressure Systolic value
     * @var        int|null
     */
    protected $blood_pressure_systolic;

    /**
     * The value for the blood_pressure_diastolic field.
     * Blood pressure Diastolic value
     * @var        int|null
     */
    protected $blood_pressure_diastolic;

    /**
     * The value for the heart_rate field.
     * Pulse rate of the patients heart
     * @var        int|null
     */
    protected $heart_rate;

    /**
     * The value for the weight field.
     * Weight of patient in kg
     * @var        string|null
     */
    protected $weight;

    /**
     * The value for the height field.
     * Height of patient in cm
     * @var        int|null
     */
    protected $height;

    /**
     * The value for the muac field.
     * Patients Mid upper arm circumference
     * @var        string|null
     */
    protected $muac;

    /**
     * The value for the temperature field.
     * Temperature (fever)
     * @var        string|null
     */
    protected $temperature;

    /**
     * The value for the skin_examination field.
     * Free text for skin examination
     * @var        string|null
     */
    protected $skin_examination;

    /**
     * The value for the details_ent field.
     * Optional details on E.N.T.
     * @var        string|null
     */
    protected $details_ent;

    /**
     * The value for the details_eyes field.
     * Optional details on eyes
     * @var        string|null
     */
    protected $details_eyes;

    /**
     * The value for the details_head field.
     * Optional details on head and brain
     * @var        string|null
     */
    protected $details_head;

    /**
     * The value for the details_muscles_bones field.
     * Optional details on muscles and bones
     * @var        string|null
     */
    protected $details_muscles_bones;

    /**
     * The value for the details_heart field.
     * Optional details on heart
     * @var        string|null
     */
    protected $details_heart;

    /**
     * The value for the details_lung field.
     * Optional details on lung
     * @var        string|null
     */
    protected $details_lung;

    /**
     * The value for the details_gastrointestinal field.
     * Optional details on gastrointestinal
     * @var        string|null
     */
    protected $details_gastrointestinal;

    /**
     * The value for the details_urinary_tract field.
     * Optional details on urinary tract
     * @var        string|null
     */
    protected $details_urinary_tract;

    /**
     * The value for the details_reproductive_system field.
     * Optional details on reproductive system
     * @var        string|null
     */
    protected $details_reproductive_system;

    /**
     * The value for the details_other field.
     * Optional details not fitting to other group
     * @var        string|null
     */
    protected $details_other;

    /**
     * The value for the general_impression field.
     * General impression on a scale from 1-5 where 5 ist best
     * @var        int
     */
    protected $general_impression;

    /**
     * The value for the patient_uuid field.
     *
     * @var        string
     */
    protected $patient_uuid;

    /**
     * The value for the user_id field.
     *
     * @var        int
     */
    protected $user_id;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        ChildPatient
     */
    protected $aPatient;

    /**
     * @var        ChildUser
     */
    protected $aUser;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var bool
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Base\PhysicalExamination object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return bool True if the object has been modified.
     */
    public function isModified(): bool
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param string $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return bool True if $col has been modified.
     */
    public function isColumnModified(string $col): bool
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns(): array
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return bool True, if the object has never been persisted.
     */
    public function isNew(): bool
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param bool $b the state of the object.
     */
    public function setNew(bool $b): void
    {
        $this->new = $b;
    }

    /**
     * Whether this object has been deleted.
     * @return bool The deleted state of this object.
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param bool $b The deleted state of this object.
     * @return void
     */
    public function setDeleted(bool $b): void
    {
        $this->deleted = $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified(?string $col = null): void
    {
        if (null !== $col) {
            unset($this->modifiedColumns[$col]);
        } else {
            $this->modifiedColumns = [];
        }
    }

    /**
     * Compares this with another <code>PhysicalExamination</code> instance.  If
     * <code>obj</code> is an instance of <code>PhysicalExamination</code>, delegates to
     * <code>equals(PhysicalExamination)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param mixed $obj The object to compare to.
     * @return bool Whether equal to the object specified.
     */
    public function equals($obj): bool
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns(): array
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return bool
     */
    public function hasVirtualColumn(string $name): bool
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return mixed
     *
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getVirtualColumn(string $name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of nonexistent virtual column `%s`.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @param mixed $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn(string $name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param string $msg
     * @param int $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log(string $msg, int $priority = Propel::LOG_INFO): void
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param \Propel\Runtime\Parser\AbstractParser|string $parser An AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string The exported data
     */
    public function exportTo($parser, bool $includeLazyLoadColumns = true, string $keyType = TableMap::TYPE_PHPNAME): string
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     *
     * @return array<string>
     */
    public function __sleep(): array
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [uuid] column value.
     * PK
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Get the [optionally formatted] temporal [timestamp] column value.
     * Timestamp of examination
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL).
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getTimestamp($format = null)
    {
        if ($format === null) {
            return $this->timestamp;
        } else {
            return $this->timestamp instanceof \DateTimeInterface ? $this->timestamp->format($format) : null;
        }
    }

    /**
     * Get the [blood_pressure_systolic] column value.
     * Blood pressure Systolic value
     * @return int|null
     */
    public function getBloodPressureSystolic()
    {
        return $this->blood_pressure_systolic;
    }

    /**
     * Get the [blood_pressure_diastolic] column value.
     * Blood pressure Diastolic value
     * @return int|null
     */
    public function getBloodPressureDiastolic()
    {
        return $this->blood_pressure_diastolic;
    }

    /**
     * Get the [heart_rate] column value.
     * Pulse rate of the patients heart
     * @return int|null
     */
    public function getHeartRate()
    {
        return $this->heart_rate;
    }

    /**
     * Get the [weight] column value.
     * Weight of patient in kg
     * @return string|null
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Get the [height] column value.
     * Height of patient in cm
     * @return int|null
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Get the [muac] column value.
     * Patients Mid upper arm circumference
     * @return string|null
     */
    public function getMuac()
    {
        return $this->muac;
    }

    /**
     * Get the [temperature] column value.
     * Temperature (fever)
     * @return string|null
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * Get the [skin_examination] column value.
     * Free text for skin examination
     * @return string|null
     */
    public function getSkinExamination()
    {
        return $this->skin_examination;
    }

    /**
     * Get the [details_ent] column value.
     * Optional details on E.N.T.
     * @return string|null
     */
    public function getDetailsEnt()
    {
        return $this->details_ent;
    }

    /**
     * Get the [details_eyes] column value.
     * Optional details on eyes
     * @return string|null
     */
    public function getDetailsEyes()
    {
        return $this->details_eyes;
    }

    /**
     * Get the [details_head] column value.
     * Optional details on head and brain
     * @return string|null
     */
    public function getDetailsHead()
    {
        return $this->details_head;
    }

    /**
     * Get the [details_muscles_bones] column value.
     * Optional details on muscles and bones
     * @return string|null
     */
    public function getDetailsMusclesBones()
    {
        return $this->details_muscles_bones;
    }

    /**
     * Get the [details_heart] column value.
     * Optional details on heart
     * @return string|null
     */
    public function getDetailsHeart()
    {
        return $this->details_heart;
    }

    /**
     * Get the [details_lung] column value.
     * Optional details on lung
     * @return string|null
     */
    public function getDetailsLung()
    {
        return $this->details_lung;
    }

    /**
     * Get the [details_gastrointestinal] column value.
     * Optional details on gastrointestinal
     * @return string|null
     */
    public function getDetailsGastrointestinal()
    {
        return $this->details_gastrointestinal;
    }

    /**
     * Get the [details_urinary_tract] column value.
     * Optional details on urinary tract
     * @return string|null
     */
    public function getDetailsUrinaryTract()
    {
        return $this->details_urinary_tract;
    }

    /**
     * Get the [details_reproductive_system] column value.
     * Optional details on reproductive system
     * @return string|null
     */
    public function getDetailsReproductiveSystem()
    {
        return $this->details_reproductive_system;
    }

    /**
     * Get the [details_other] column value.
     * Optional details not fitting to other group
     * @return string|null
     */
    public function getDetailsOther()
    {
        return $this->details_other;
    }

    /**
     * Get the [general_impression] column value.
     * General impression on a scale from 1-5 where 5 ist best
     * @return int
     */
    public function getGeneralImpression()
    {
        return $this->general_impression;
    }

    /**
     * Get the [patient_uuid] column value.
     *
     * @return string
     */
    public function getPatientUuid()
    {
        return $this->patient_uuid;
    }

    /**
     * Get the [user_id] column value.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [uuid] column.
     * PK
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uuid !== $v) {
            $this->uuid = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_UUID] = true;
        }

        return $this;
    }

    /**
     * Sets the value of [timestamp] column to a normalized version of the date/time value specified.
     * Timestamp of examination
     * @param string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setTimestamp($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->timestamp !== null || $dt !== null) {
            if ($this->timestamp === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->timestamp->format("Y-m-d H:i:s.u")) {
                $this->timestamp = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PhysicalExaminationTableMap::COL_TIMESTAMP] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Set the value of [blood_pressure_systolic] column.
     * Blood pressure Systolic value
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setBloodPressureSystolic($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->blood_pressure_systolic !== $v) {
            $this->blood_pressure_systolic = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_SYSTOLIC] = true;
        }

        return $this;
    }

    /**
     * Set the value of [blood_pressure_diastolic] column.
     * Blood pressure Diastolic value
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setBloodPressureDiastolic($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->blood_pressure_diastolic !== $v) {
            $this->blood_pressure_diastolic = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_DIASTOLIC] = true;
        }

        return $this;
    }

    /**
     * Set the value of [heart_rate] column.
     * Pulse rate of the patients heart
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setHeartRate($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->heart_rate !== $v) {
            $this->heart_rate = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_HEART_RATE] = true;
        }

        return $this;
    }

    /**
     * Set the value of [weight] column.
     * Weight of patient in kg
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setWeight($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->weight !== $v) {
            $this->weight = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_WEIGHT] = true;
        }

        return $this;
    }

    /**
     * Set the value of [height] column.
     * Height of patient in cm
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setHeight($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->height !== $v) {
            $this->height = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_HEIGHT] = true;
        }

        return $this;
    }

    /**
     * Set the value of [muac] column.
     * Patients Mid upper arm circumference
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setMuac($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->muac !== $v) {
            $this->muac = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_MUAC] = true;
        }

        return $this;
    }

    /**
     * Set the value of [temperature] column.
     * Temperature (fever)
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setTemperature($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->temperature !== $v) {
            $this->temperature = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_TEMPERATURE] = true;
        }

        return $this;
    }

    /**
     * Set the value of [skin_examination] column.
     * Free text for skin examination
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setSkinExamination($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->skin_examination !== $v) {
            $this->skin_examination = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_SKIN_EXAMINATION] = true;
        }

        return $this;
    }

    /**
     * Set the value of [details_ent] column.
     * Optional details on E.N.T.
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDetailsEnt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->details_ent !== $v) {
            $this->details_ent = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_DETAILS_ENT] = true;
        }

        return $this;
    }

    /**
     * Set the value of [details_eyes] column.
     * Optional details on eyes
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDetailsEyes($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->details_eyes !== $v) {
            $this->details_eyes = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_DETAILS_EYES] = true;
        }

        return $this;
    }

    /**
     * Set the value of [details_head] column.
     * Optional details on head and brain
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDetailsHead($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->details_head !== $v) {
            $this->details_head = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_DETAILS_HEAD] = true;
        }

        return $this;
    }

    /**
     * Set the value of [details_muscles_bones] column.
     * Optional details on muscles and bones
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDetailsMusclesBones($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->details_muscles_bones !== $v) {
            $this->details_muscles_bones = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_DETAILS_MUSCLES_BONES] = true;
        }

        return $this;
    }

    /**
     * Set the value of [details_heart] column.
     * Optional details on heart
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDetailsHeart($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->details_heart !== $v) {
            $this->details_heart = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_DETAILS_HEART] = true;
        }

        return $this;
    }

    /**
     * Set the value of [details_lung] column.
     * Optional details on lung
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDetailsLung($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->details_lung !== $v) {
            $this->details_lung = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_DETAILS_LUNG] = true;
        }

        return $this;
    }

    /**
     * Set the value of [details_gastrointestinal] column.
     * Optional details on gastrointestinal
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDetailsGastrointestinal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->details_gastrointestinal !== $v) {
            $this->details_gastrointestinal = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_DETAILS_GASTROINTESTINAL] = true;
        }

        return $this;
    }

    /**
     * Set the value of [details_urinary_tract] column.
     * Optional details on urinary tract
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDetailsUrinaryTract($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->details_urinary_tract !== $v) {
            $this->details_urinary_tract = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_DETAILS_URINARY_TRACT] = true;
        }

        return $this;
    }

    /**
     * Set the value of [details_reproductive_system] column.
     * Optional details on reproductive system
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDetailsReproductiveSystem($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->details_reproductive_system !== $v) {
            $this->details_reproductive_system = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_DETAILS_REPRODUCTIVE_SYSTEM] = true;
        }

        return $this;
    }

    /**
     * Set the value of [details_other] column.
     * Optional details not fitting to other group
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDetailsOther($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->details_other !== $v) {
            $this->details_other = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_DETAILS_OTHER] = true;
        }

        return $this;
    }

    /**
     * Set the value of [general_impression] column.
     * General impression on a scale from 1-5 where 5 ist best
     * @param int $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setGeneralImpression($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->general_impression !== $v) {
            $this->general_impression = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_GENERAL_IMPRESSION] = true;
        }

        return $this;
    }

    /**
     * Set the value of [patient_uuid] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setPatientUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->patient_uuid !== $v) {
            $this->patient_uuid = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_PATIENT_UUID] = true;
        }

        if ($this->aPatient !== null && $this->aPatient->getUuid() !== $v) {
            $this->aPatient = null;
        }

        return $this;
    }

    /**
     * Set the value of [user_id] column.
     *
     * @param int $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->user_id !== $v) {
            $this->user_id = $v;
            $this->modifiedColumns[PhysicalExaminationTableMap::COL_USER_ID] = true;
        }

        if ($this->aUser !== null && $this->aUser->getId() !== $v) {
            $this->aUser = null;
        }

        return $this;
    }

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PhysicalExaminationTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PhysicalExaminationTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return bool Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues(): bool
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    }

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by DataFetcher->fetch().
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param bool $rehydrate Whether this object is being re-hydrated from the database.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int next starting column
     * @throws \Propel\Runtime\Exception\PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate(array $row, int $startcol = 0, bool $rehydrate = false, string $indexType = TableMap::TYPE_NUM): int
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PhysicalExaminationTableMap::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PhysicalExaminationTableMap::translateFieldName('Timestamp', TableMap::TYPE_PHPNAME, $indexType)];
            $this->timestamp = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PhysicalExaminationTableMap::translateFieldName('BloodPressureSystolic', TableMap::TYPE_PHPNAME, $indexType)];
            $this->blood_pressure_systolic = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PhysicalExaminationTableMap::translateFieldName('BloodPressureDiastolic', TableMap::TYPE_PHPNAME, $indexType)];
            $this->blood_pressure_diastolic = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PhysicalExaminationTableMap::translateFieldName('HeartRate', TableMap::TYPE_PHPNAME, $indexType)];
            $this->heart_rate = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PhysicalExaminationTableMap::translateFieldName('Weight', TableMap::TYPE_PHPNAME, $indexType)];
            $this->weight = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PhysicalExaminationTableMap::translateFieldName('Height', TableMap::TYPE_PHPNAME, $indexType)];
            $this->height = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PhysicalExaminationTableMap::translateFieldName('Muac', TableMap::TYPE_PHPNAME, $indexType)];
            $this->muac = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : PhysicalExaminationTableMap::translateFieldName('Temperature', TableMap::TYPE_PHPNAME, $indexType)];
            $this->temperature = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : PhysicalExaminationTableMap::translateFieldName('SkinExamination', TableMap::TYPE_PHPNAME, $indexType)];
            $this->skin_examination = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : PhysicalExaminationTableMap::translateFieldName('DetailsEnt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->details_ent = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : PhysicalExaminationTableMap::translateFieldName('DetailsEyes', TableMap::TYPE_PHPNAME, $indexType)];
            $this->details_eyes = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : PhysicalExaminationTableMap::translateFieldName('DetailsHead', TableMap::TYPE_PHPNAME, $indexType)];
            $this->details_head = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : PhysicalExaminationTableMap::translateFieldName('DetailsMusclesBones', TableMap::TYPE_PHPNAME, $indexType)];
            $this->details_muscles_bones = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : PhysicalExaminationTableMap::translateFieldName('DetailsHeart', TableMap::TYPE_PHPNAME, $indexType)];
            $this->details_heart = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : PhysicalExaminationTableMap::translateFieldName('DetailsLung', TableMap::TYPE_PHPNAME, $indexType)];
            $this->details_lung = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : PhysicalExaminationTableMap::translateFieldName('DetailsGastrointestinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->details_gastrointestinal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : PhysicalExaminationTableMap::translateFieldName('DetailsUrinaryTract', TableMap::TYPE_PHPNAME, $indexType)];
            $this->details_urinary_tract = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : PhysicalExaminationTableMap::translateFieldName('DetailsReproductiveSystem', TableMap::TYPE_PHPNAME, $indexType)];
            $this->details_reproductive_system = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : PhysicalExaminationTableMap::translateFieldName('DetailsOther', TableMap::TYPE_PHPNAME, $indexType)];
            $this->details_other = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : PhysicalExaminationTableMap::translateFieldName('GeneralImpression', TableMap::TYPE_PHPNAME, $indexType)];
            $this->general_impression = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : PhysicalExaminationTableMap::translateFieldName('PatientUuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->patient_uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : PhysicalExaminationTableMap::translateFieldName('UserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : PhysicalExaminationTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : PhysicalExaminationTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 25; // 25 = PhysicalExaminationTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\PhysicalExamination'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function ensureConsistency(): void
    {
        if ($this->aPatient !== null && $this->patient_uuid !== $this->aPatient->getUuid()) {
            $this->aPatient = null;
        }
        if ($this->aUser !== null && $this->user_id !== $this->aUser->getId()) {
            $this->aUser = null;
        }
    }

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param bool $deep (optional) Whether to also de-associated any related objects.
     * @param ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload(bool $deep = false, ?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PhysicalExaminationTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPhysicalExaminationQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPatient = null;
            $this->aUser = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param ConnectionInterface $con
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     * @see PhysicalExamination::setDeleted()
     * @see PhysicalExamination::isDeleted()
     */
    public function delete(?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PhysicalExaminationTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPhysicalExaminationQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    public function save(?ConnectionInterface $con = null): int
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PhysicalExaminationTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(PhysicalExaminationTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(PhysicalExaminationTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(PhysicalExaminationTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PhysicalExaminationTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con): int
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPatient !== null) {
                if ($this->aPatient->isModified() || $this->aPatient->isNew()) {
                    $affectedRows += $this->aPatient->save($con);
                }
                $this->setPatient($this->aPatient);
            }

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    }

    /**
     * Insert the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con): void
    {
        $modifiedColumns = [];
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'uuid';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_TIMESTAMP)) {
            $modifiedColumns[':p' . $index++]  = 'timestamp';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_SYSTOLIC)) {
            $modifiedColumns[':p' . $index++]  = 'blood_pressure_systolic';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_DIASTOLIC)) {
            $modifiedColumns[':p' . $index++]  = 'blood_pressure_diastolic';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_HEART_RATE)) {
            $modifiedColumns[':p' . $index++]  = 'heart_rate';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_WEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'weight';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_HEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'height';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_MUAC)) {
            $modifiedColumns[':p' . $index++]  = 'muac';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_TEMPERATURE)) {
            $modifiedColumns[':p' . $index++]  = 'temperature';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_SKIN_EXAMINATION)) {
            $modifiedColumns[':p' . $index++]  = 'skin_examination';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_ENT)) {
            $modifiedColumns[':p' . $index++]  = 'details_ent';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_EYES)) {
            $modifiedColumns[':p' . $index++]  = 'details_eyes';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_HEAD)) {
            $modifiedColumns[':p' . $index++]  = 'details_head';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_MUSCLES_BONES)) {
            $modifiedColumns[':p' . $index++]  = 'details_muscles_bones';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_HEART)) {
            $modifiedColumns[':p' . $index++]  = 'details_heart';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_LUNG)) {
            $modifiedColumns[':p' . $index++]  = 'details_lung';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_GASTROINTESTINAL)) {
            $modifiedColumns[':p' . $index++]  = 'details_gastrointestinal';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_URINARY_TRACT)) {
            $modifiedColumns[':p' . $index++]  = 'details_urinary_tract';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_REPRODUCTIVE_SYSTEM)) {
            $modifiedColumns[':p' . $index++]  = 'details_reproductive_system';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_OTHER)) {
            $modifiedColumns[':p' . $index++]  = 'details_other';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_GENERAL_IMPRESSION)) {
            $modifiedColumns[':p' . $index++]  = 'general_impression';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_PATIENT_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'patient_uuid';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'user_id';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO physical_examination (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'uuid':
                        $stmt->bindValue($identifier, $this->uuid, PDO::PARAM_STR);
                        break;
                    case 'timestamp':
                        $stmt->bindValue($identifier, $this->timestamp ? $this->timestamp->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'blood_pressure_systolic':
                        $stmt->bindValue($identifier, $this->blood_pressure_systolic, PDO::PARAM_INT);
                        break;
                    case 'blood_pressure_diastolic':
                        $stmt->bindValue($identifier, $this->blood_pressure_diastolic, PDO::PARAM_INT);
                        break;
                    case 'heart_rate':
                        $stmt->bindValue($identifier, $this->heart_rate, PDO::PARAM_INT);
                        break;
                    case 'weight':
                        $stmt->bindValue($identifier, $this->weight, PDO::PARAM_STR);
                        break;
                    case 'height':
                        $stmt->bindValue($identifier, $this->height, PDO::PARAM_INT);
                        break;
                    case 'muac':
                        $stmt->bindValue($identifier, $this->muac, PDO::PARAM_STR);
                        break;
                    case 'temperature':
                        $stmt->bindValue($identifier, $this->temperature, PDO::PARAM_STR);
                        break;
                    case 'skin_examination':
                        $stmt->bindValue($identifier, $this->skin_examination, PDO::PARAM_STR);
                        break;
                    case 'details_ent':
                        $stmt->bindValue($identifier, $this->details_ent, PDO::PARAM_STR);
                        break;
                    case 'details_eyes':
                        $stmt->bindValue($identifier, $this->details_eyes, PDO::PARAM_STR);
                        break;
                    case 'details_head':
                        $stmt->bindValue($identifier, $this->details_head, PDO::PARAM_STR);
                        break;
                    case 'details_muscles_bones':
                        $stmt->bindValue($identifier, $this->details_muscles_bones, PDO::PARAM_STR);
                        break;
                    case 'details_heart':
                        $stmt->bindValue($identifier, $this->details_heart, PDO::PARAM_STR);
                        break;
                    case 'details_lung':
                        $stmt->bindValue($identifier, $this->details_lung, PDO::PARAM_STR);
                        break;
                    case 'details_gastrointestinal':
                        $stmt->bindValue($identifier, $this->details_gastrointestinal, PDO::PARAM_STR);
                        break;
                    case 'details_urinary_tract':
                        $stmt->bindValue($identifier, $this->details_urinary_tract, PDO::PARAM_STR);
                        break;
                    case 'details_reproductive_system':
                        $stmt->bindValue($identifier, $this->details_reproductive_system, PDO::PARAM_STR);
                        break;
                    case 'details_other':
                        $stmt->bindValue($identifier, $this->details_other, PDO::PARAM_STR);
                        break;
                    case 'general_impression':
                        $stmt->bindValue($identifier, $this->general_impression, PDO::PARAM_INT);
                        break;
                    case 'patient_uuid':
                        $stmt->bindValue($identifier, $this->patient_uuid, PDO::PARAM_STR);
                        break;
                    case 'user_id':
                        $stmt->bindValue($identifier, $this->user_id, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @return int Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con): int
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName(string $name, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PhysicalExaminationTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos Position in XML schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition(int $pos)
    {
        switch ($pos) {
            case 0:
                return $this->getUuid();

            case 1:
                return $this->getTimestamp();

            case 2:
                return $this->getBloodPressureSystolic();

            case 3:
                return $this->getBloodPressureDiastolic();

            case 4:
                return $this->getHeartRate();

            case 5:
                return $this->getWeight();

            case 6:
                return $this->getHeight();

            case 7:
                return $this->getMuac();

            case 8:
                return $this->getTemperature();

            case 9:
                return $this->getSkinExamination();

            case 10:
                return $this->getDetailsEnt();

            case 11:
                return $this->getDetailsEyes();

            case 12:
                return $this->getDetailsHead();

            case 13:
                return $this->getDetailsMusclesBones();

            case 14:
                return $this->getDetailsHeart();

            case 15:
                return $this->getDetailsLung();

            case 16:
                return $this->getDetailsGastrointestinal();

            case 17:
                return $this->getDetailsUrinaryTract();

            case 18:
                return $this->getDetailsReproductiveSystem();

            case 19:
                return $this->getDetailsOther();

            case 20:
                return $this->getGeneralImpression();

            case 21:
                return $this->getPatientUuid();

            case 22:
                return $this->getUserId();

            case 23:
                return $this->getCreatedAt();

            case 24:
                return $this->getUpdatedAt();

            default:
                return null;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param bool $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array An associative array containing the field names (as keys) and field values
     */
    public function toArray(string $keyType = TableMap::TYPE_PHPNAME, bool $includeLazyLoadColumns = true, array $alreadyDumpedObjects = [], bool $includeForeignObjects = false): array
    {
        if (isset($alreadyDumpedObjects['PhysicalExamination'][$this->hashCode()])) {
            return ['*RECURSION*'];
        }
        $alreadyDumpedObjects['PhysicalExamination'][$this->hashCode()] = true;
        $keys = PhysicalExaminationTableMap::getFieldNames($keyType);
        $result = [
            $keys[0] => $this->getUuid(),
            $keys[1] => $this->getTimestamp(),
            $keys[2] => $this->getBloodPressureSystolic(),
            $keys[3] => $this->getBloodPressureDiastolic(),
            $keys[4] => $this->getHeartRate(),
            $keys[5] => $this->getWeight(),
            $keys[6] => $this->getHeight(),
            $keys[7] => $this->getMuac(),
            $keys[8] => $this->getTemperature(),
            $keys[9] => $this->getSkinExamination(),
            $keys[10] => $this->getDetailsEnt(),
            $keys[11] => $this->getDetailsEyes(),
            $keys[12] => $this->getDetailsHead(),
            $keys[13] => $this->getDetailsMusclesBones(),
            $keys[14] => $this->getDetailsHeart(),
            $keys[15] => $this->getDetailsLung(),
            $keys[16] => $this->getDetailsGastrointestinal(),
            $keys[17] => $this->getDetailsUrinaryTract(),
            $keys[18] => $this->getDetailsReproductiveSystem(),
            $keys[19] => $this->getDetailsOther(),
            $keys[20] => $this->getGeneralImpression(),
            $keys[21] => $this->getPatientUuid(),
            $keys[22] => $this->getUserId(),
            $keys[23] => $this->getCreatedAt(),
            $keys[24] => $this->getUpdatedAt(),
        ];
        if ($result[$keys[1]] instanceof \DateTimeInterface) {
            $result[$keys[1]] = $result[$keys[1]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[23]] instanceof \DateTimeInterface) {
            $result[$keys[23]] = $result[$keys[23]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[24]] instanceof \DateTimeInterface) {
            $result[$keys[24]] = $result[$keys[24]]->format('Y-m-d H:i:s.u');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aPatient) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'patient';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'patient';
                        break;
                    default:
                        $key = 'Patient';
                }

                $result[$key] = $this->aPatient->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'users';
                        break;
                    default:
                        $key = 'User';
                }

                $result[$key] = $this->aUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this
     */
    public function setByName(string $name, $value, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PhysicalExaminationTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        $this->setByPosition($pos, $value);

        return $this;
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return $this
     */
    public function setByPosition(int $pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setUuid($value);
                break;
            case 1:
                $this->setTimestamp($value);
                break;
            case 2:
                $this->setBloodPressureSystolic($value);
                break;
            case 3:
                $this->setBloodPressureDiastolic($value);
                break;
            case 4:
                $this->setHeartRate($value);
                break;
            case 5:
                $this->setWeight($value);
                break;
            case 6:
                $this->setHeight($value);
                break;
            case 7:
                $this->setMuac($value);
                break;
            case 8:
                $this->setTemperature($value);
                break;
            case 9:
                $this->setSkinExamination($value);
                break;
            case 10:
                $this->setDetailsEnt($value);
                break;
            case 11:
                $this->setDetailsEyes($value);
                break;
            case 12:
                $this->setDetailsHead($value);
                break;
            case 13:
                $this->setDetailsMusclesBones($value);
                break;
            case 14:
                $this->setDetailsHeart($value);
                break;
            case 15:
                $this->setDetailsLung($value);
                break;
            case 16:
                $this->setDetailsGastrointestinal($value);
                break;
            case 17:
                $this->setDetailsUrinaryTract($value);
                break;
            case 18:
                $this->setDetailsReproductiveSystem($value);
                break;
            case 19:
                $this->setDetailsOther($value);
                break;
            case 20:
                $this->setGeneralImpression($value);
                break;
            case 21:
                $this->setPatientUuid($value);
                break;
            case 22:
                $this->setUserId($value);
                break;
            case 23:
                $this->setCreatedAt($value);
                break;
            case 24:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param array $arr An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return $this
     */
    public function fromArray(array $arr, string $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PhysicalExaminationTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setUuid($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTimestamp($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setBloodPressureSystolic($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setBloodPressureDiastolic($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setHeartRate($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setWeight($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setHeight($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setMuac($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTemperature($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setSkinExamination($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setDetailsEnt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setDetailsEyes($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setDetailsHead($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setDetailsMusclesBones($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setDetailsHeart($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setDetailsLung($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setDetailsGastrointestinal($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setDetailsUrinaryTract($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setDetailsReproductiveSystem($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setDetailsOther($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setGeneralImpression($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setPatientUuid($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setUserId($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setCreatedAt($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setUpdatedAt($arr[$keys[24]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this The current object, for fluid interface
     */
    public function importFrom($parser, string $data, string $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria(): Criteria
    {
        $criteria = new Criteria(PhysicalExaminationTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_UUID)) {
            $criteria->add(PhysicalExaminationTableMap::COL_UUID, $this->uuid);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_TIMESTAMP)) {
            $criteria->add(PhysicalExaminationTableMap::COL_TIMESTAMP, $this->timestamp);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_SYSTOLIC)) {
            $criteria->add(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_SYSTOLIC, $this->blood_pressure_systolic);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_DIASTOLIC)) {
            $criteria->add(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_DIASTOLIC, $this->blood_pressure_diastolic);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_HEART_RATE)) {
            $criteria->add(PhysicalExaminationTableMap::COL_HEART_RATE, $this->heart_rate);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_WEIGHT)) {
            $criteria->add(PhysicalExaminationTableMap::COL_WEIGHT, $this->weight);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_HEIGHT)) {
            $criteria->add(PhysicalExaminationTableMap::COL_HEIGHT, $this->height);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_MUAC)) {
            $criteria->add(PhysicalExaminationTableMap::COL_MUAC, $this->muac);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_TEMPERATURE)) {
            $criteria->add(PhysicalExaminationTableMap::COL_TEMPERATURE, $this->temperature);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_SKIN_EXAMINATION)) {
            $criteria->add(PhysicalExaminationTableMap::COL_SKIN_EXAMINATION, $this->skin_examination);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_ENT)) {
            $criteria->add(PhysicalExaminationTableMap::COL_DETAILS_ENT, $this->details_ent);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_EYES)) {
            $criteria->add(PhysicalExaminationTableMap::COL_DETAILS_EYES, $this->details_eyes);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_HEAD)) {
            $criteria->add(PhysicalExaminationTableMap::COL_DETAILS_HEAD, $this->details_head);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_MUSCLES_BONES)) {
            $criteria->add(PhysicalExaminationTableMap::COL_DETAILS_MUSCLES_BONES, $this->details_muscles_bones);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_HEART)) {
            $criteria->add(PhysicalExaminationTableMap::COL_DETAILS_HEART, $this->details_heart);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_LUNG)) {
            $criteria->add(PhysicalExaminationTableMap::COL_DETAILS_LUNG, $this->details_lung);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_GASTROINTESTINAL)) {
            $criteria->add(PhysicalExaminationTableMap::COL_DETAILS_GASTROINTESTINAL, $this->details_gastrointestinal);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_URINARY_TRACT)) {
            $criteria->add(PhysicalExaminationTableMap::COL_DETAILS_URINARY_TRACT, $this->details_urinary_tract);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_REPRODUCTIVE_SYSTEM)) {
            $criteria->add(PhysicalExaminationTableMap::COL_DETAILS_REPRODUCTIVE_SYSTEM, $this->details_reproductive_system);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_DETAILS_OTHER)) {
            $criteria->add(PhysicalExaminationTableMap::COL_DETAILS_OTHER, $this->details_other);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_GENERAL_IMPRESSION)) {
            $criteria->add(PhysicalExaminationTableMap::COL_GENERAL_IMPRESSION, $this->general_impression);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_PATIENT_UUID)) {
            $criteria->add(PhysicalExaminationTableMap::COL_PATIENT_UUID, $this->patient_uuid);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_USER_ID)) {
            $criteria->add(PhysicalExaminationTableMap::COL_USER_ID, $this->user_id);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_CREATED_AT)) {
            $criteria->add(PhysicalExaminationTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(PhysicalExaminationTableMap::COL_UPDATED_AT)) {
            $criteria->add(PhysicalExaminationTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria(): Criteria
    {
        $criteria = ChildPhysicalExaminationQuery::create();
        $criteria->add(PhysicalExaminationTableMap::COL_UUID, $this->uuid);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int|string Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getUuid();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getUuid();
    }

    /**
     * Generic method to set the primary key (uuid column).
     *
     * @param string|null $key Primary key.
     * @return void
     */
    public function setPrimaryKey(?string $key = null): void
    {
        $this->setUuid($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     *
     * @return bool
     */
    public function isPrimaryKeyNull(): bool
    {
        return null === $this->getUuid();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of \PhysicalExamination (or compatible) type.
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param bool $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function copyInto(object $copyObj, bool $deepCopy = false, bool $makeNew = true): void
    {
        $copyObj->setUuid($this->getUuid());
        $copyObj->setTimestamp($this->getTimestamp());
        $copyObj->setBloodPressureSystolic($this->getBloodPressureSystolic());
        $copyObj->setBloodPressureDiastolic($this->getBloodPressureDiastolic());
        $copyObj->setHeartRate($this->getHeartRate());
        $copyObj->setWeight($this->getWeight());
        $copyObj->setHeight($this->getHeight());
        $copyObj->setMuac($this->getMuac());
        $copyObj->setTemperature($this->getTemperature());
        $copyObj->setSkinExamination($this->getSkinExamination());
        $copyObj->setDetailsEnt($this->getDetailsEnt());
        $copyObj->setDetailsEyes($this->getDetailsEyes());
        $copyObj->setDetailsHead($this->getDetailsHead());
        $copyObj->setDetailsMusclesBones($this->getDetailsMusclesBones());
        $copyObj->setDetailsHeart($this->getDetailsHeart());
        $copyObj->setDetailsLung($this->getDetailsLung());
        $copyObj->setDetailsGastrointestinal($this->getDetailsGastrointestinal());
        $copyObj->setDetailsUrinaryTract($this->getDetailsUrinaryTract());
        $copyObj->setDetailsReproductiveSystem($this->getDetailsReproductiveSystem());
        $copyObj->setDetailsOther($this->getDetailsOther());
        $copyObj->setGeneralImpression($this->getGeneralImpression());
        $copyObj->setPatientUuid($this->getPatientUuid());
        $copyObj->setUserId($this->getUserId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \PhysicalExamination Clone of current object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function copy(bool $deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildPatient object.
     *
     * @param ChildPatient $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setPatient(ChildPatient $v = null)
    {
        if ($v === null) {
            $this->setPatientUuid(NULL);
        } else {
            $this->setPatientUuid($v->getUuid());
        }

        $this->aPatient = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPatient object, it will not be re-added.
        if ($v !== null) {
            $v->addPhysicalExamination($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPatient object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildPatient The associated ChildPatient object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getPatient(?ConnectionInterface $con = null)
    {
        if ($this->aPatient === null && (($this->patient_uuid !== "" && $this->patient_uuid !== null))) {
            $this->aPatient = ChildPatientQuery::create()->findPk($this->patient_uuid, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPatient->addPhysicalExaminations($this);
             */
        }

        return $this->aPatient;
    }

    /**
     * Declares an association between this object and a ChildUser object.
     *
     * @param ChildUser $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setUser(ChildUser $v = null)
    {
        if ($v === null) {
            $this->setUserId(NULL);
        } else {
            $this->setUserId($v->getId());
        }

        $this->aUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUser object, it will not be re-added.
        if ($v !== null) {
            $v->addPhysicalExamination($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUser object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildUser The associated ChildUser object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getUser(?ConnectionInterface $con = null)
    {
        if ($this->aUser === null && ($this->user_id != 0)) {
            $this->aUser = ChildUserQuery::create()->findPk($this->user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUser->addPhysicalExaminations($this);
             */
        }

        return $this->aUser;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     *
     * @return $this
     */
    public function clear()
    {
        if (null !== $this->aPatient) {
            $this->aPatient->removePhysicalExamination($this);
        }
        if (null !== $this->aUser) {
            $this->aUser->removePhysicalExamination($this);
        }
        $this->uuid = null;
        $this->timestamp = null;
        $this->blood_pressure_systolic = null;
        $this->blood_pressure_diastolic = null;
        $this->heart_rate = null;
        $this->weight = null;
        $this->height = null;
        $this->muac = null;
        $this->temperature = null;
        $this->skin_examination = null;
        $this->details_ent = null;
        $this->details_eyes = null;
        $this->details_head = null;
        $this->details_muscles_bones = null;
        $this->details_heart = null;
        $this->details_lung = null;
        $this->details_gastrointestinal = null;
        $this->details_urinary_tract = null;
        $this->details_reproductive_system = null;
        $this->details_other = null;
        $this->general_impression = null;
        $this->patient_uuid = null;
        $this->user_id = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);

        return $this;
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param bool $deep Whether to also clear the references on all referrer objects.
     * @return $this
     */
    public function clearAllReferences(bool $deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aPatient = null;
        $this->aUser = null;
        return $this;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PhysicalExaminationTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return $this The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[PhysicalExaminationTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preSave(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postSave(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before inserting to database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preInsert(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postInsert(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preUpdate(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postUpdate(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preDelete(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postDelete(?ConnectionInterface $con = null): void
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
