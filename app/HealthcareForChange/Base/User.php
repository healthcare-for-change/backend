<?php

namespace Base;

use \AuthGroupsUser as ChildAuthGroupsUser;
use \AuthGroupsUserQuery as ChildAuthGroupsUserQuery;
use \AuthIdentity as ChildAuthIdentity;
use \AuthIdentityQuery as ChildAuthIdentityQuery;
use \AuthLogin as ChildAuthLogin;
use \AuthLoginQuery as ChildAuthLoginQuery;
use \AuthPermissionsUser as ChildAuthPermissionsUser;
use \AuthPermissionsUserQuery as ChildAuthPermissionsUserQuery;
use \AuthRememberToken as ChildAuthRememberToken;
use \AuthRememberTokenQuery as ChildAuthRememberTokenQuery;
use \AuthTokenLogin as ChildAuthTokenLogin;
use \AuthTokenLoginQuery as ChildAuthTokenLoginQuery;
use \DewormingHistory as ChildDewormingHistory;
use \DewormingHistoryQuery as ChildDewormingHistoryQuery;
use \MedicalHistory as ChildMedicalHistory;
use \MedicalHistoryQuery as ChildMedicalHistoryQuery;
use \PhysicalExamination as ChildPhysicalExamination;
use \PhysicalExaminationQuery as ChildPhysicalExaminationQuery;
use \Prescription as ChildPrescription;
use \PrescriptionQuery as ChildPrescriptionQuery;
use \Tenant as ChildTenant;
use \TenantQuery as ChildTenantQuery;
use \User as ChildUser;
use \UserHasTenant as ChildUserHasTenant;
use \UserHasTenantQuery as ChildUserHasTenantQuery;
use \UserQuery as ChildUserQuery;
use \VaccinationHistory as ChildVaccinationHistory;
use \VaccinationHistoryQuery as ChildVaccinationHistoryQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\AuthGroupsUserTableMap;
use Map\AuthIdentityTableMap;
use Map\AuthLoginTableMap;
use Map\AuthPermissionsUserTableMap;
use Map\AuthRememberTokenTableMap;
use Map\AuthTokenLoginTableMap;
use Map\DewormingHistoryTableMap;
use Map\MedicalHistoryTableMap;
use Map\PhysicalExaminationTableMap;
use Map\PrescriptionTableMap;
use Map\UserHasTenantTableMap;
use Map\UserTableMap;
use Map\VaccinationHistoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'users' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class User implements ActiveRecordInterface
{
    /**
     * TableMap class name
     *
     * @var string
     */
    public const TABLE_MAP = '\\Map\\UserTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var bool
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var bool
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = [];

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = [];

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the username field.
     *
     * @var        string|null
     */
    protected $username;

    /**
     * The value for the display_name field.
     *
     * @var        string|null
     */
    protected $display_name;

    /**
     * The value for the status field.
     *
     * @var        string|null
     */
    protected $status;

    /**
     * The value for the status_message field.
     *
     * @var        string|null
     */
    protected $status_message;

    /**
     * The value for the active field.
     *
     * Note: this column has a database default value of: 0
     * @var        int|null
     */
    protected $active;

    /**
     * The value for the last_active field.
     *
     * @var        DateTime|null
     */
    protected $last_active;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime|null
     */
    protected $deleted_at;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        ObjectCollection|ChildVaccinationHistory[] Collection to store aggregation of ChildVaccinationHistory objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildVaccinationHistory> Collection to store aggregation of ChildVaccinationHistory objects.
     */
    protected $collVaccinationHistories;
    protected $collVaccinationHistoriesPartial;

    /**
     * @var        ObjectCollection|ChildPhysicalExamination[] Collection to store aggregation of ChildPhysicalExamination objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildPhysicalExamination> Collection to store aggregation of ChildPhysicalExamination objects.
     */
    protected $collPhysicalExaminations;
    protected $collPhysicalExaminationsPartial;

    /**
     * @var        ObjectCollection|ChildDewormingHistory[] Collection to store aggregation of ChildDewormingHistory objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildDewormingHistory> Collection to store aggregation of ChildDewormingHistory objects.
     */
    protected $collDewormingHistories;
    protected $collDewormingHistoriesPartial;

    /**
     * @var        ObjectCollection|ChildMedicalHistory[] Collection to store aggregation of ChildMedicalHistory objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildMedicalHistory> Collection to store aggregation of ChildMedicalHistory objects.
     */
    protected $collMedicalHistories;
    protected $collMedicalHistoriesPartial;

    /**
     * @var        ObjectCollection|ChildPrescription[] Collection to store aggregation of ChildPrescription objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildPrescription> Collection to store aggregation of ChildPrescription objects.
     */
    protected $collPrescriptions;
    protected $collPrescriptionsPartial;

    /**
     * @var        ObjectCollection|ChildUserHasTenant[] Collection to store aggregation of ChildUserHasTenant objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildUserHasTenant> Collection to store aggregation of ChildUserHasTenant objects.
     */
    protected $collUserHasTenants;
    protected $collUserHasTenantsPartial;

    /**
     * @var        ObjectCollection|ChildAuthIdentity[] Collection to store aggregation of ChildAuthIdentity objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildAuthIdentity> Collection to store aggregation of ChildAuthIdentity objects.
     */
    protected $collAuthIdentities;
    protected $collAuthIdentitiesPartial;

    /**
     * @var        ObjectCollection|ChildAuthLogin[] Collection to store aggregation of ChildAuthLogin objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildAuthLogin> Collection to store aggregation of ChildAuthLogin objects.
     */
    protected $collAuthLogins;
    protected $collAuthLoginsPartial;

    /**
     * @var        ObjectCollection|ChildAuthTokenLogin[] Collection to store aggregation of ChildAuthTokenLogin objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildAuthTokenLogin> Collection to store aggregation of ChildAuthTokenLogin objects.
     */
    protected $collAuthTokenLogins;
    protected $collAuthTokenLoginsPartial;

    /**
     * @var        ObjectCollection|ChildAuthRememberToken[] Collection to store aggregation of ChildAuthRememberToken objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildAuthRememberToken> Collection to store aggregation of ChildAuthRememberToken objects.
     */
    protected $collAuthRememberTokens;
    protected $collAuthRememberTokensPartial;

    /**
     * @var        ObjectCollection|ChildAuthGroupsUser[] Collection to store aggregation of ChildAuthGroupsUser objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildAuthGroupsUser> Collection to store aggregation of ChildAuthGroupsUser objects.
     */
    protected $collAuthGroupsUsers;
    protected $collAuthGroupsUsersPartial;

    /**
     * @var        ObjectCollection|ChildAuthPermissionsUser[] Collection to store aggregation of ChildAuthPermissionsUser objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildAuthPermissionsUser> Collection to store aggregation of ChildAuthPermissionsUser objects.
     */
    protected $collAuthPermissionsUsers;
    protected $collAuthPermissionsUsersPartial;

    /**
     * @var        ObjectCollection|ChildTenant[] Cross Collection to store aggregation of ChildTenant objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildTenant> Cross Collection to store aggregation of ChildTenant objects.
     */
    protected $collTenants;

    /**
     * @var bool
     */
    protected $collTenantsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var bool
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTenant[]
     * @phpstan-var ObjectCollection&\Traversable<ChildTenant>
     */
    protected $tenantsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildVaccinationHistory[]
     * @phpstan-var ObjectCollection&\Traversable<ChildVaccinationHistory>
     */
    protected $vaccinationHistoriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPhysicalExamination[]
     * @phpstan-var ObjectCollection&\Traversable<ChildPhysicalExamination>
     */
    protected $physicalExaminationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDewormingHistory[]
     * @phpstan-var ObjectCollection&\Traversable<ChildDewormingHistory>
     */
    protected $dewormingHistoriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMedicalHistory[]
     * @phpstan-var ObjectCollection&\Traversable<ChildMedicalHistory>
     */
    protected $medicalHistoriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrescription[]
     * @phpstan-var ObjectCollection&\Traversable<ChildPrescription>
     */
    protected $prescriptionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUserHasTenant[]
     * @phpstan-var ObjectCollection&\Traversable<ChildUserHasTenant>
     */
    protected $userHasTenantsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildAuthIdentity[]
     * @phpstan-var ObjectCollection&\Traversable<ChildAuthIdentity>
     */
    protected $authIdentitiesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildAuthLogin[]
     * @phpstan-var ObjectCollection&\Traversable<ChildAuthLogin>
     */
    protected $authLoginsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildAuthTokenLogin[]
     * @phpstan-var ObjectCollection&\Traversable<ChildAuthTokenLogin>
     */
    protected $authTokenLoginsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildAuthRememberToken[]
     * @phpstan-var ObjectCollection&\Traversable<ChildAuthRememberToken>
     */
    protected $authRememberTokensScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildAuthGroupsUser[]
     * @phpstan-var ObjectCollection&\Traversable<ChildAuthGroupsUser>
     */
    protected $authGroupsUsersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildAuthPermissionsUser[]
     * @phpstan-var ObjectCollection&\Traversable<ChildAuthPermissionsUser>
     */
    protected $authPermissionsUsersScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues(): void
    {
        $this->active = 0;
    }

    /**
     * Initializes internal state of Base\User object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return bool True if the object has been modified.
     */
    public function isModified(): bool
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param string $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return bool True if $col has been modified.
     */
    public function isColumnModified(string $col): bool
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns(): array
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return bool True, if the object has never been persisted.
     */
    public function isNew(): bool
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param bool $b the state of the object.
     */
    public function setNew(bool $b): void
    {
        $this->new = $b;
    }

    /**
     * Whether this object has been deleted.
     * @return bool The deleted state of this object.
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param bool $b The deleted state of this object.
     * @return void
     */
    public function setDeleted(bool $b): void
    {
        $this->deleted = $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified(?string $col = null): void
    {
        if (null !== $col) {
            unset($this->modifiedColumns[$col]);
        } else {
            $this->modifiedColumns = [];
        }
    }

    /**
     * Compares this with another <code>User</code> instance.  If
     * <code>obj</code> is an instance of <code>User</code>, delegates to
     * <code>equals(User)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param mixed $obj The object to compare to.
     * @return bool Whether equal to the object specified.
     */
    public function equals($obj): bool
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns(): array
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return bool
     */
    public function hasVirtualColumn(string $name): bool
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return mixed
     *
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getVirtualColumn(string $name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of nonexistent virtual column `%s`.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @param mixed $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn(string $name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param string $msg
     * @param int $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log(string $msg, int $priority = Propel::LOG_INFO): void
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param \Propel\Runtime\Parser\AbstractParser|string $parser An AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string The exported data
     */
    public function exportTo($parser, bool $includeLazyLoadColumns = true, string $keyType = TableMap::TYPE_PHPNAME): string
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     *
     * @return array<string>
     */
    public function __sleep(): array
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [username] column value.
     *
     * @return string|null
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the [display_name] column value.
     *
     * @return string|null
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * Get the [status] column value.
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [status_message] column value.
     *
     * @return string|null
     */
    public function getStatusMessage()
    {
        return $this->status_message;
    }

    /**
     * Get the [active] column value.
     *
     * @return int|null
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get the [optionally formatted] temporal [last_active] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getLastActive($format = null)
    {
        if ($format === null) {
            return $this->last_active;
        } else {
            return $this->last_active instanceof \DateTimeInterface ? $this->last_active->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getDeletedAt($format = null)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UserTableMap::COL_ID] = true;
        }

        return $this;
    }

    /**
     * Set the value of [username] column.
     *
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[UserTableMap::COL_USERNAME] = true;
        }

        return $this;
    }

    /**
     * Set the value of [display_name] column.
     *
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDisplayName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->display_name !== $v) {
            $this->display_name = $v;
            $this->modifiedColumns[UserTableMap::COL_DISPLAY_NAME] = true;
        }

        return $this;
    }

    /**
     * Set the value of [status] column.
     *
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[UserTableMap::COL_STATUS] = true;
        }

        return $this;
    }

    /**
     * Set the value of [status_message] column.
     *
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setStatusMessage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->status_message !== $v) {
            $this->status_message = $v;
            $this->modifiedColumns[UserTableMap::COL_STATUS_MESSAGE] = true;
        }

        return $this;
    }

    /**
     * Set the value of [active] column.
     *
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setActive($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->active !== $v) {
            $this->active = $v;
            $this->modifiedColumns[UserTableMap::COL_ACTIVE] = true;
        }

        return $this;
    }

    /**
     * Sets the value of [last_active] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setLastActive($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_active !== null || $dt !== null) {
            if ($this->last_active === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->last_active->format("Y-m-d H:i:s.u")) {
                $this->last_active = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserTableMap::COL_LAST_ACTIVE] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return bool Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues(): bool
    {
            if ($this->active !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    }

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by DataFetcher->fetch().
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param bool $rehydrate Whether this object is being re-hydrated from the database.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int next starting column
     * @throws \Propel\Runtime\Exception\PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate(array $row, int $startcol = 0, bool $rehydrate = false, string $indexType = TableMap::TYPE_NUM): int
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UserTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UserTableMap::translateFieldName('Username', TableMap::TYPE_PHPNAME, $indexType)];
            $this->username = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UserTableMap::translateFieldName('DisplayName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->display_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UserTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UserTableMap::translateFieldName('StatusMessage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status_message = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UserTableMap::translateFieldName('Active', TableMap::TYPE_PHPNAME, $indexType)];
            $this->active = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UserTableMap::translateFieldName('LastActive', TableMap::TYPE_PHPNAME, $indexType)];
            $this->last_active = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UserTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UserTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : UserTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = UserTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\User'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function ensureConsistency(): void
    {
    }

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param bool $deep (optional) Whether to also de-associated any related objects.
     * @param ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload(bool $deep = false, ?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUserQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collVaccinationHistories = null;

            $this->collPhysicalExaminations = null;

            $this->collDewormingHistories = null;

            $this->collMedicalHistories = null;

            $this->collPrescriptions = null;

            $this->collUserHasTenants = null;

            $this->collAuthIdentities = null;

            $this->collAuthLogins = null;

            $this->collAuthTokenLogins = null;

            $this->collAuthRememberTokens = null;

            $this->collAuthGroupsUsers = null;

            $this->collAuthPermissionsUsers = null;

            $this->collTenants = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param ConnectionInterface $con
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     * @see User::setDeleted()
     * @see User::isDeleted()
     */
    public function delete(?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUserQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    public function save(?ConnectionInterface $con = null): int
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(UserTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(UserTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(UserTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UserTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con): int
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->tenantsScheduledForDeletion !== null) {
                if (!$this->tenantsScheduledForDeletion->isEmpty()) {
                    $pks = [];
                    foreach ($this->tenantsScheduledForDeletion as $entry) {
                        $entryPk = [];

                        $entryPk[1] = $this->getId();
                        $entryPk[0] = $entry->getUuid();
                        $pks[] = $entryPk;
                    }

                    \UserHasTenantQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);

                    $this->tenantsScheduledForDeletion = null;
                }

            }

            if ($this->collTenants) {
                foreach ($this->collTenants as $tenant) {
                    if (!$tenant->isDeleted() && ($tenant->isNew() || $tenant->isModified())) {
                        $tenant->save($con);
                    }
                }
            }


            if ($this->vaccinationHistoriesScheduledForDeletion !== null) {
                if (!$this->vaccinationHistoriesScheduledForDeletion->isEmpty()) {
                    \VaccinationHistoryQuery::create()
                        ->filterByPrimaryKeys($this->vaccinationHistoriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vaccinationHistoriesScheduledForDeletion = null;
                }
            }

            if ($this->collVaccinationHistories !== null) {
                foreach ($this->collVaccinationHistories as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->physicalExaminationsScheduledForDeletion !== null) {
                if (!$this->physicalExaminationsScheduledForDeletion->isEmpty()) {
                    \PhysicalExaminationQuery::create()
                        ->filterByPrimaryKeys($this->physicalExaminationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->physicalExaminationsScheduledForDeletion = null;
                }
            }

            if ($this->collPhysicalExaminations !== null) {
                foreach ($this->collPhysicalExaminations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->dewormingHistoriesScheduledForDeletion !== null) {
                if (!$this->dewormingHistoriesScheduledForDeletion->isEmpty()) {
                    \DewormingHistoryQuery::create()
                        ->filterByPrimaryKeys($this->dewormingHistoriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dewormingHistoriesScheduledForDeletion = null;
                }
            }

            if ($this->collDewormingHistories !== null) {
                foreach ($this->collDewormingHistories as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->medicalHistoriesScheduledForDeletion !== null) {
                if (!$this->medicalHistoriesScheduledForDeletion->isEmpty()) {
                    \MedicalHistoryQuery::create()
                        ->filterByPrimaryKeys($this->medicalHistoriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->medicalHistoriesScheduledForDeletion = null;
                }
            }

            if ($this->collMedicalHistories !== null) {
                foreach ($this->collMedicalHistories as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prescriptionsScheduledForDeletion !== null) {
                if (!$this->prescriptionsScheduledForDeletion->isEmpty()) {
                    \PrescriptionQuery::create()
                        ->filterByPrimaryKeys($this->prescriptionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prescriptionsScheduledForDeletion = null;
                }
            }

            if ($this->collPrescriptions !== null) {
                foreach ($this->collPrescriptions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->userHasTenantsScheduledForDeletion !== null) {
                if (!$this->userHasTenantsScheduledForDeletion->isEmpty()) {
                    \UserHasTenantQuery::create()
                        ->filterByPrimaryKeys($this->userHasTenantsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userHasTenantsScheduledForDeletion = null;
                }
            }

            if ($this->collUserHasTenants !== null) {
                foreach ($this->collUserHasTenants as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->authIdentitiesScheduledForDeletion !== null) {
                if (!$this->authIdentitiesScheduledForDeletion->isEmpty()) {
                    \AuthIdentityQuery::create()
                        ->filterByPrimaryKeys($this->authIdentitiesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->authIdentitiesScheduledForDeletion = null;
                }
            }

            if ($this->collAuthIdentities !== null) {
                foreach ($this->collAuthIdentities as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->authLoginsScheduledForDeletion !== null) {
                if (!$this->authLoginsScheduledForDeletion->isEmpty()) {
                    \AuthLoginQuery::create()
                        ->filterByPrimaryKeys($this->authLoginsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->authLoginsScheduledForDeletion = null;
                }
            }

            if ($this->collAuthLogins !== null) {
                foreach ($this->collAuthLogins as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->authTokenLoginsScheduledForDeletion !== null) {
                if (!$this->authTokenLoginsScheduledForDeletion->isEmpty()) {
                    \AuthTokenLoginQuery::create()
                        ->filterByPrimaryKeys($this->authTokenLoginsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->authTokenLoginsScheduledForDeletion = null;
                }
            }

            if ($this->collAuthTokenLogins !== null) {
                foreach ($this->collAuthTokenLogins as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->authRememberTokensScheduledForDeletion !== null) {
                if (!$this->authRememberTokensScheduledForDeletion->isEmpty()) {
                    \AuthRememberTokenQuery::create()
                        ->filterByPrimaryKeys($this->authRememberTokensScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->authRememberTokensScheduledForDeletion = null;
                }
            }

            if ($this->collAuthRememberTokens !== null) {
                foreach ($this->collAuthRememberTokens as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->authGroupsUsersScheduledForDeletion !== null) {
                if (!$this->authGroupsUsersScheduledForDeletion->isEmpty()) {
                    \AuthGroupsUserQuery::create()
                        ->filterByPrimaryKeys($this->authGroupsUsersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->authGroupsUsersScheduledForDeletion = null;
                }
            }

            if ($this->collAuthGroupsUsers !== null) {
                foreach ($this->collAuthGroupsUsers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->authPermissionsUsersScheduledForDeletion !== null) {
                if (!$this->authPermissionsUsersScheduledForDeletion->isEmpty()) {
                    \AuthPermissionsUserQuery::create()
                        ->filterByPrimaryKeys($this->authPermissionsUsersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->authPermissionsUsersScheduledForDeletion = null;
                }
            }

            if ($this->collAuthPermissionsUsers !== null) {
                foreach ($this->collAuthPermissionsUsers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    }

    /**
     * Insert the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con): void
    {
        $modifiedColumns = [];
        $index = 0;

        $this->modifiedColumns[UserTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UserTableMap::COL_ID . ')');
        }
        if (null === $this->id) {
            try {
                $dataFetcher = $con->query("SELECT nextval('users_id_seq')");
                $this->id = (int) $dataFetcher->fetchColumn();
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', 0, $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UserTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(UserTableMap::COL_USERNAME)) {
            $modifiedColumns[':p' . $index++]  = 'username';
        }
        if ($this->isColumnModified(UserTableMap::COL_DISPLAY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'display_name';
        }
        if ($this->isColumnModified(UserTableMap::COL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'status';
        }
        if ($this->isColumnModified(UserTableMap::COL_STATUS_MESSAGE)) {
            $modifiedColumns[':p' . $index++]  = 'status_message';
        }
        if ($this->isColumnModified(UserTableMap::COL_ACTIVE)) {
            $modifiedColumns[':p' . $index++]  = 'active';
        }
        if ($this->isColumnModified(UserTableMap::COL_LAST_ACTIVE)) {
            $modifiedColumns[':p' . $index++]  = 'last_active';
        }
        if ($this->isColumnModified(UserTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }
        if ($this->isColumnModified(UserTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(UserTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO users (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'username':
                        $stmt->bindValue($identifier, $this->username, PDO::PARAM_STR);
                        break;
                    case 'display_name':
                        $stmt->bindValue($identifier, $this->display_name, PDO::PARAM_STR);
                        break;
                    case 'status':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_STR);
                        break;
                    case 'status_message':
                        $stmt->bindValue($identifier, $this->status_message, PDO::PARAM_STR);
                        break;
                    case 'active':
                        $stmt->bindValue($identifier, $this->active, PDO::PARAM_INT);
                        break;
                    case 'last_active':
                        $stmt->bindValue($identifier, $this->last_active ? $this->last_active->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @return int Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con): int
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName(string $name, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos Position in XML schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition(int $pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();

            case 1:
                return $this->getUsername();

            case 2:
                return $this->getDisplayName();

            case 3:
                return $this->getStatus();

            case 4:
                return $this->getStatusMessage();

            case 5:
                return $this->getActive();

            case 6:
                return $this->getLastActive();

            case 7:
                return $this->getDeletedAt();

            case 8:
                return $this->getCreatedAt();

            case 9:
                return $this->getUpdatedAt();

            default:
                return null;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param bool $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array An associative array containing the field names (as keys) and field values
     */
    public function toArray(string $keyType = TableMap::TYPE_PHPNAME, bool $includeLazyLoadColumns = true, array $alreadyDumpedObjects = [], bool $includeForeignObjects = false): array
    {
        if (isset($alreadyDumpedObjects['User'][$this->hashCode()])) {
            return ['*RECURSION*'];
        }
        $alreadyDumpedObjects['User'][$this->hashCode()] = true;
        $keys = UserTableMap::getFieldNames($keyType);
        $result = [
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUsername(),
            $keys[2] => $this->getDisplayName(),
            $keys[3] => $this->getStatus(),
            $keys[4] => $this->getStatusMessage(),
            $keys[5] => $this->getActive(),
            $keys[6] => $this->getLastActive(),
            $keys[7] => $this->getDeletedAt(),
            $keys[8] => $this->getCreatedAt(),
            $keys[9] => $this->getUpdatedAt(),
        ];
        if ($result[$keys[6]] instanceof \DateTimeInterface) {
            $result[$keys[6]] = $result[$keys[6]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[7]] instanceof \DateTimeInterface) {
            $result[$keys[7]] = $result[$keys[7]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[9]] instanceof \DateTimeInterface) {
            $result[$keys[9]] = $result[$keys[9]]->format('Y-m-d H:i:s.u');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collVaccinationHistories) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'vaccinationHistories';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'vaccination_histories';
                        break;
                    default:
                        $key = 'VaccinationHistories';
                }

                $result[$key] = $this->collVaccinationHistories->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPhysicalExaminations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'physicalExaminations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'physical_examinations';
                        break;
                    default:
                        $key = 'PhysicalExaminations';
                }

                $result[$key] = $this->collPhysicalExaminations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDewormingHistories) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dewormingHistories';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'deworming_histories';
                        break;
                    default:
                        $key = 'DewormingHistories';
                }

                $result[$key] = $this->collDewormingHistories->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMedicalHistories) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'medicalHistories';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'medical_histories';
                        break;
                    default:
                        $key = 'MedicalHistories';
                }

                $result[$key] = $this->collMedicalHistories->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrescriptions) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prescriptions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'prescriptions';
                        break;
                    default:
                        $key = 'Prescriptions';
                }

                $result[$key] = $this->collPrescriptions->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUserHasTenants) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userHasTenants';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_has_tenants';
                        break;
                    default:
                        $key = 'UserHasTenants';
                }

                $result[$key] = $this->collUserHasTenants->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAuthIdentities) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'authIdentities';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'auth_identitiess';
                        break;
                    default:
                        $key = 'AuthIdentities';
                }

                $result[$key] = $this->collAuthIdentities->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAuthLogins) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'authLogins';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'auth_loginss';
                        break;
                    default:
                        $key = 'AuthLogins';
                }

                $result[$key] = $this->collAuthLogins->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAuthTokenLogins) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'authTokenLogins';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'auth_token_loginss';
                        break;
                    default:
                        $key = 'AuthTokenLogins';
                }

                $result[$key] = $this->collAuthTokenLogins->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAuthRememberTokens) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'authRememberTokens';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'auth_remember_tokenss';
                        break;
                    default:
                        $key = 'AuthRememberTokens';
                }

                $result[$key] = $this->collAuthRememberTokens->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAuthGroupsUsers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'authGroupsUsers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'auth_groups_userss';
                        break;
                    default:
                        $key = 'AuthGroupsUsers';
                }

                $result[$key] = $this->collAuthGroupsUsers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAuthPermissionsUsers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'authPermissionsUsers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'auth_permissions_userss';
                        break;
                    default:
                        $key = 'AuthPermissionsUsers';
                }

                $result[$key] = $this->collAuthPermissionsUsers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this
     */
    public function setByName(string $name, $value, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        $this->setByPosition($pos, $value);

        return $this;
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return $this
     */
    public function setByPosition(int $pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUsername($value);
                break;
            case 2:
                $this->setDisplayName($value);
                break;
            case 3:
                $this->setStatus($value);
                break;
            case 4:
                $this->setStatusMessage($value);
                break;
            case 5:
                $this->setActive($value);
                break;
            case 6:
                $this->setLastActive($value);
                break;
            case 7:
                $this->setDeletedAt($value);
                break;
            case 8:
                $this->setCreatedAt($value);
                break;
            case 9:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param array $arr An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return $this
     */
    public function fromArray(array $arr, string $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UserTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUsername($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setDisplayName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setStatus($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setStatusMessage($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setActive($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setLastActive($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDeletedAt($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setCreatedAt($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setUpdatedAt($arr[$keys[9]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this The current object, for fluid interface
     */
    public function importFrom($parser, string $data, string $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria(): Criteria
    {
        $criteria = new Criteria(UserTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UserTableMap::COL_ID)) {
            $criteria->add(UserTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UserTableMap::COL_USERNAME)) {
            $criteria->add(UserTableMap::COL_USERNAME, $this->username);
        }
        if ($this->isColumnModified(UserTableMap::COL_DISPLAY_NAME)) {
            $criteria->add(UserTableMap::COL_DISPLAY_NAME, $this->display_name);
        }
        if ($this->isColumnModified(UserTableMap::COL_STATUS)) {
            $criteria->add(UserTableMap::COL_STATUS, $this->status);
        }
        if ($this->isColumnModified(UserTableMap::COL_STATUS_MESSAGE)) {
            $criteria->add(UserTableMap::COL_STATUS_MESSAGE, $this->status_message);
        }
        if ($this->isColumnModified(UserTableMap::COL_ACTIVE)) {
            $criteria->add(UserTableMap::COL_ACTIVE, $this->active);
        }
        if ($this->isColumnModified(UserTableMap::COL_LAST_ACTIVE)) {
            $criteria->add(UserTableMap::COL_LAST_ACTIVE, $this->last_active);
        }
        if ($this->isColumnModified(UserTableMap::COL_DELETED_AT)) {
            $criteria->add(UserTableMap::COL_DELETED_AT, $this->deleted_at);
        }
        if ($this->isColumnModified(UserTableMap::COL_CREATED_AT)) {
            $criteria->add(UserTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(UserTableMap::COL_UPDATED_AT)) {
            $criteria->add(UserTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria(): Criteria
    {
        $criteria = ChildUserQuery::create();
        $criteria->add(UserTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int|string Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param int|null $key Primary key.
     * @return void
     */
    public function setPrimaryKey(?int $key = null): void
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     *
     * @return bool
     */
    public function isPrimaryKeyNull(): bool
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of \User (or compatible) type.
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param bool $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function copyInto(object $copyObj, bool $deepCopy = false, bool $makeNew = true): void
    {
        $copyObj->setUsername($this->getUsername());
        $copyObj->setDisplayName($this->getDisplayName());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setStatusMessage($this->getStatusMessage());
        $copyObj->setActive($this->getActive());
        $copyObj->setLastActive($this->getLastActive());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getVaccinationHistories() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVaccinationHistory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPhysicalExaminations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPhysicalExamination($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDewormingHistories() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDewormingHistory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMedicalHistories() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMedicalHistory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrescriptions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrescription($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUserHasTenants() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserHasTenant($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAuthIdentities() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAuthIdentity($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAuthLogins() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAuthLogin($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAuthTokenLogins() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAuthTokenLogin($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAuthRememberTokens() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAuthRememberToken($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAuthGroupsUsers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAuthGroupsUser($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAuthPermissionsUsers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAuthPermissionsUser($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \User Clone of current object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function copy(bool $deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName): void
    {
        if ('VaccinationHistory' === $relationName) {
            $this->initVaccinationHistories();
            return;
        }
        if ('PhysicalExamination' === $relationName) {
            $this->initPhysicalExaminations();
            return;
        }
        if ('DewormingHistory' === $relationName) {
            $this->initDewormingHistories();
            return;
        }
        if ('MedicalHistory' === $relationName) {
            $this->initMedicalHistories();
            return;
        }
        if ('Prescription' === $relationName) {
            $this->initPrescriptions();
            return;
        }
        if ('UserHasTenant' === $relationName) {
            $this->initUserHasTenants();
            return;
        }
        if ('AuthIdentity' === $relationName) {
            $this->initAuthIdentities();
            return;
        }
        if ('AuthLogin' === $relationName) {
            $this->initAuthLogins();
            return;
        }
        if ('AuthTokenLogin' === $relationName) {
            $this->initAuthTokenLogins();
            return;
        }
        if ('AuthRememberToken' === $relationName) {
            $this->initAuthRememberTokens();
            return;
        }
        if ('AuthGroupsUser' === $relationName) {
            $this->initAuthGroupsUsers();
            return;
        }
        if ('AuthPermissionsUser' === $relationName) {
            $this->initAuthPermissionsUsers();
            return;
        }
    }

    /**
     * Clears out the collVaccinationHistories collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addVaccinationHistories()
     */
    public function clearVaccinationHistories()
    {
        $this->collVaccinationHistories = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collVaccinationHistories collection loaded partially.
     *
     * @return void
     */
    public function resetPartialVaccinationHistories($v = true): void
    {
        $this->collVaccinationHistoriesPartial = $v;
    }

    /**
     * Initializes the collVaccinationHistories collection.
     *
     * By default this just sets the collVaccinationHistories collection to an empty array (like clearcollVaccinationHistories());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVaccinationHistories(bool $overrideExisting = true): void
    {
        if (null !== $this->collVaccinationHistories && !$overrideExisting) {
            return;
        }

        $collectionClassName = VaccinationHistoryTableMap::getTableMap()->getCollectionClassName();

        $this->collVaccinationHistories = new $collectionClassName;
        $this->collVaccinationHistories->setModel('\VaccinationHistory');
    }

    /**
     * Gets an array of ChildVaccinationHistory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildVaccinationHistory[] List of ChildVaccinationHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildVaccinationHistory> List of ChildVaccinationHistory objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getVaccinationHistories(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collVaccinationHistoriesPartial && !$this->isNew();
        if (null === $this->collVaccinationHistories || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collVaccinationHistories) {
                    $this->initVaccinationHistories();
                } else {
                    $collectionClassName = VaccinationHistoryTableMap::getTableMap()->getCollectionClassName();

                    $collVaccinationHistories = new $collectionClassName;
                    $collVaccinationHistories->setModel('\VaccinationHistory');

                    return $collVaccinationHistories;
                }
            } else {
                $collVaccinationHistories = ChildVaccinationHistoryQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collVaccinationHistoriesPartial && count($collVaccinationHistories)) {
                        $this->initVaccinationHistories(false);

                        foreach ($collVaccinationHistories as $obj) {
                            if (false == $this->collVaccinationHistories->contains($obj)) {
                                $this->collVaccinationHistories->append($obj);
                            }
                        }

                        $this->collVaccinationHistoriesPartial = true;
                    }

                    return $collVaccinationHistories;
                }

                if ($partial && $this->collVaccinationHistories) {
                    foreach ($this->collVaccinationHistories as $obj) {
                        if ($obj->isNew()) {
                            $collVaccinationHistories[] = $obj;
                        }
                    }
                }

                $this->collVaccinationHistories = $collVaccinationHistories;
                $this->collVaccinationHistoriesPartial = false;
            }
        }

        return $this->collVaccinationHistories;
    }

    /**
     * Sets a collection of ChildVaccinationHistory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $vaccinationHistories A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setVaccinationHistories(Collection $vaccinationHistories, ?ConnectionInterface $con = null)
    {
        /** @var ChildVaccinationHistory[] $vaccinationHistoriesToDelete */
        $vaccinationHistoriesToDelete = $this->getVaccinationHistories(new Criteria(), $con)->diff($vaccinationHistories);


        $this->vaccinationHistoriesScheduledForDeletion = $vaccinationHistoriesToDelete;

        foreach ($vaccinationHistoriesToDelete as $vaccinationHistoryRemoved) {
            $vaccinationHistoryRemoved->setUser(null);
        }

        $this->collVaccinationHistories = null;
        foreach ($vaccinationHistories as $vaccinationHistory) {
            $this->addVaccinationHistory($vaccinationHistory);
        }

        $this->collVaccinationHistories = $vaccinationHistories;
        $this->collVaccinationHistoriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VaccinationHistory objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related VaccinationHistory objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countVaccinationHistories(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collVaccinationHistoriesPartial && !$this->isNew();
        if (null === $this->collVaccinationHistories || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVaccinationHistories) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getVaccinationHistories());
            }

            $query = ChildVaccinationHistoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collVaccinationHistories);
    }

    /**
     * Method called to associate a ChildVaccinationHistory object to this object
     * through the ChildVaccinationHistory foreign key attribute.
     *
     * @param ChildVaccinationHistory $l ChildVaccinationHistory
     * @return $this The current object (for fluent API support)
     */
    public function addVaccinationHistory(ChildVaccinationHistory $l)
    {
        if ($this->collVaccinationHistories === null) {
            $this->initVaccinationHistories();
            $this->collVaccinationHistoriesPartial = true;
        }

        if (!$this->collVaccinationHistories->contains($l)) {
            $this->doAddVaccinationHistory($l);

            if ($this->vaccinationHistoriesScheduledForDeletion and $this->vaccinationHistoriesScheduledForDeletion->contains($l)) {
                $this->vaccinationHistoriesScheduledForDeletion->remove($this->vaccinationHistoriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildVaccinationHistory $vaccinationHistory The ChildVaccinationHistory object to add.
     */
    protected function doAddVaccinationHistory(ChildVaccinationHistory $vaccinationHistory): void
    {
        $this->collVaccinationHistories[]= $vaccinationHistory;
        $vaccinationHistory->setUser($this);
    }

    /**
     * @param ChildVaccinationHistory $vaccinationHistory The ChildVaccinationHistory object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeVaccinationHistory(ChildVaccinationHistory $vaccinationHistory)
    {
        if ($this->getVaccinationHistories()->contains($vaccinationHistory)) {
            $pos = $this->collVaccinationHistories->search($vaccinationHistory);
            $this->collVaccinationHistories->remove($pos);
            if (null === $this->vaccinationHistoriesScheduledForDeletion) {
                $this->vaccinationHistoriesScheduledForDeletion = clone $this->collVaccinationHistories;
                $this->vaccinationHistoriesScheduledForDeletion->clear();
            }
            $this->vaccinationHistoriesScheduledForDeletion[]= clone $vaccinationHistory;
            $vaccinationHistory->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related VaccinationHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildVaccinationHistory[] List of ChildVaccinationHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildVaccinationHistory}> List of ChildVaccinationHistory objects
     */
    public function getVaccinationHistoriesJoinPatient(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildVaccinationHistoryQuery::create(null, $criteria);
        $query->joinWith('Patient', $joinBehavior);

        return $this->getVaccinationHistories($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related VaccinationHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildVaccinationHistory[] List of ChildVaccinationHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildVaccinationHistory}> List of ChildVaccinationHistory objects
     */
    public function getVaccinationHistoriesJoinVaccine(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildVaccinationHistoryQuery::create(null, $criteria);
        $query->joinWith('Vaccine', $joinBehavior);

        return $this->getVaccinationHistories($query, $con);
    }

    /**
     * Clears out the collPhysicalExaminations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addPhysicalExaminations()
     */
    public function clearPhysicalExaminations()
    {
        $this->collPhysicalExaminations = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collPhysicalExaminations collection loaded partially.
     *
     * @return void
     */
    public function resetPartialPhysicalExaminations($v = true): void
    {
        $this->collPhysicalExaminationsPartial = $v;
    }

    /**
     * Initializes the collPhysicalExaminations collection.
     *
     * By default this just sets the collPhysicalExaminations collection to an empty array (like clearcollPhysicalExaminations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPhysicalExaminations(bool $overrideExisting = true): void
    {
        if (null !== $this->collPhysicalExaminations && !$overrideExisting) {
            return;
        }

        $collectionClassName = PhysicalExaminationTableMap::getTableMap()->getCollectionClassName();

        $this->collPhysicalExaminations = new $collectionClassName;
        $this->collPhysicalExaminations->setModel('\PhysicalExamination');
    }

    /**
     * Gets an array of ChildPhysicalExamination objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPhysicalExamination[] List of ChildPhysicalExamination objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPhysicalExamination> List of ChildPhysicalExamination objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getPhysicalExaminations(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collPhysicalExaminationsPartial && !$this->isNew();
        if (null === $this->collPhysicalExaminations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPhysicalExaminations) {
                    $this->initPhysicalExaminations();
                } else {
                    $collectionClassName = PhysicalExaminationTableMap::getTableMap()->getCollectionClassName();

                    $collPhysicalExaminations = new $collectionClassName;
                    $collPhysicalExaminations->setModel('\PhysicalExamination');

                    return $collPhysicalExaminations;
                }
            } else {
                $collPhysicalExaminations = ChildPhysicalExaminationQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPhysicalExaminationsPartial && count($collPhysicalExaminations)) {
                        $this->initPhysicalExaminations(false);

                        foreach ($collPhysicalExaminations as $obj) {
                            if (false == $this->collPhysicalExaminations->contains($obj)) {
                                $this->collPhysicalExaminations->append($obj);
                            }
                        }

                        $this->collPhysicalExaminationsPartial = true;
                    }

                    return $collPhysicalExaminations;
                }

                if ($partial && $this->collPhysicalExaminations) {
                    foreach ($this->collPhysicalExaminations as $obj) {
                        if ($obj->isNew()) {
                            $collPhysicalExaminations[] = $obj;
                        }
                    }
                }

                $this->collPhysicalExaminations = $collPhysicalExaminations;
                $this->collPhysicalExaminationsPartial = false;
            }
        }

        return $this->collPhysicalExaminations;
    }

    /**
     * Sets a collection of ChildPhysicalExamination objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $physicalExaminations A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setPhysicalExaminations(Collection $physicalExaminations, ?ConnectionInterface $con = null)
    {
        /** @var ChildPhysicalExamination[] $physicalExaminationsToDelete */
        $physicalExaminationsToDelete = $this->getPhysicalExaminations(new Criteria(), $con)->diff($physicalExaminations);


        $this->physicalExaminationsScheduledForDeletion = $physicalExaminationsToDelete;

        foreach ($physicalExaminationsToDelete as $physicalExaminationRemoved) {
            $physicalExaminationRemoved->setUser(null);
        }

        $this->collPhysicalExaminations = null;
        foreach ($physicalExaminations as $physicalExamination) {
            $this->addPhysicalExamination($physicalExamination);
        }

        $this->collPhysicalExaminations = $physicalExaminations;
        $this->collPhysicalExaminationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PhysicalExamination objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related PhysicalExamination objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countPhysicalExaminations(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collPhysicalExaminationsPartial && !$this->isNew();
        if (null === $this->collPhysicalExaminations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPhysicalExaminations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPhysicalExaminations());
            }

            $query = ChildPhysicalExaminationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collPhysicalExaminations);
    }

    /**
     * Method called to associate a ChildPhysicalExamination object to this object
     * through the ChildPhysicalExamination foreign key attribute.
     *
     * @param ChildPhysicalExamination $l ChildPhysicalExamination
     * @return $this The current object (for fluent API support)
     */
    public function addPhysicalExamination(ChildPhysicalExamination $l)
    {
        if ($this->collPhysicalExaminations === null) {
            $this->initPhysicalExaminations();
            $this->collPhysicalExaminationsPartial = true;
        }

        if (!$this->collPhysicalExaminations->contains($l)) {
            $this->doAddPhysicalExamination($l);

            if ($this->physicalExaminationsScheduledForDeletion and $this->physicalExaminationsScheduledForDeletion->contains($l)) {
                $this->physicalExaminationsScheduledForDeletion->remove($this->physicalExaminationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPhysicalExamination $physicalExamination The ChildPhysicalExamination object to add.
     */
    protected function doAddPhysicalExamination(ChildPhysicalExamination $physicalExamination): void
    {
        $this->collPhysicalExaminations[]= $physicalExamination;
        $physicalExamination->setUser($this);
    }

    /**
     * @param ChildPhysicalExamination $physicalExamination The ChildPhysicalExamination object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removePhysicalExamination(ChildPhysicalExamination $physicalExamination)
    {
        if ($this->getPhysicalExaminations()->contains($physicalExamination)) {
            $pos = $this->collPhysicalExaminations->search($physicalExamination);
            $this->collPhysicalExaminations->remove($pos);
            if (null === $this->physicalExaminationsScheduledForDeletion) {
                $this->physicalExaminationsScheduledForDeletion = clone $this->collPhysicalExaminations;
                $this->physicalExaminationsScheduledForDeletion->clear();
            }
            $this->physicalExaminationsScheduledForDeletion[]= clone $physicalExamination;
            $physicalExamination->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related PhysicalExaminations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPhysicalExamination[] List of ChildPhysicalExamination objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPhysicalExamination}> List of ChildPhysicalExamination objects
     */
    public function getPhysicalExaminationsJoinPatient(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPhysicalExaminationQuery::create(null, $criteria);
        $query->joinWith('Patient', $joinBehavior);

        return $this->getPhysicalExaminations($query, $con);
    }

    /**
     * Clears out the collDewormingHistories collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addDewormingHistories()
     */
    public function clearDewormingHistories()
    {
        $this->collDewormingHistories = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collDewormingHistories collection loaded partially.
     *
     * @return void
     */
    public function resetPartialDewormingHistories($v = true): void
    {
        $this->collDewormingHistoriesPartial = $v;
    }

    /**
     * Initializes the collDewormingHistories collection.
     *
     * By default this just sets the collDewormingHistories collection to an empty array (like clearcollDewormingHistories());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDewormingHistories(bool $overrideExisting = true): void
    {
        if (null !== $this->collDewormingHistories && !$overrideExisting) {
            return;
        }

        $collectionClassName = DewormingHistoryTableMap::getTableMap()->getCollectionClassName();

        $this->collDewormingHistories = new $collectionClassName;
        $this->collDewormingHistories->setModel('\DewormingHistory');
    }

    /**
     * Gets an array of ChildDewormingHistory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDewormingHistory[] List of ChildDewormingHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildDewormingHistory> List of ChildDewormingHistory objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getDewormingHistories(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collDewormingHistoriesPartial && !$this->isNew();
        if (null === $this->collDewormingHistories || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collDewormingHistories) {
                    $this->initDewormingHistories();
                } else {
                    $collectionClassName = DewormingHistoryTableMap::getTableMap()->getCollectionClassName();

                    $collDewormingHistories = new $collectionClassName;
                    $collDewormingHistories->setModel('\DewormingHistory');

                    return $collDewormingHistories;
                }
            } else {
                $collDewormingHistories = ChildDewormingHistoryQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDewormingHistoriesPartial && count($collDewormingHistories)) {
                        $this->initDewormingHistories(false);

                        foreach ($collDewormingHistories as $obj) {
                            if (false == $this->collDewormingHistories->contains($obj)) {
                                $this->collDewormingHistories->append($obj);
                            }
                        }

                        $this->collDewormingHistoriesPartial = true;
                    }

                    return $collDewormingHistories;
                }

                if ($partial && $this->collDewormingHistories) {
                    foreach ($this->collDewormingHistories as $obj) {
                        if ($obj->isNew()) {
                            $collDewormingHistories[] = $obj;
                        }
                    }
                }

                $this->collDewormingHistories = $collDewormingHistories;
                $this->collDewormingHistoriesPartial = false;
            }
        }

        return $this->collDewormingHistories;
    }

    /**
     * Sets a collection of ChildDewormingHistory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $dewormingHistories A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setDewormingHistories(Collection $dewormingHistories, ?ConnectionInterface $con = null)
    {
        /** @var ChildDewormingHistory[] $dewormingHistoriesToDelete */
        $dewormingHistoriesToDelete = $this->getDewormingHistories(new Criteria(), $con)->diff($dewormingHistories);


        $this->dewormingHistoriesScheduledForDeletion = $dewormingHistoriesToDelete;

        foreach ($dewormingHistoriesToDelete as $dewormingHistoryRemoved) {
            $dewormingHistoryRemoved->setUser(null);
        }

        $this->collDewormingHistories = null;
        foreach ($dewormingHistories as $dewormingHistory) {
            $this->addDewormingHistory($dewormingHistory);
        }

        $this->collDewormingHistories = $dewormingHistories;
        $this->collDewormingHistoriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DewormingHistory objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related DewormingHistory objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countDewormingHistories(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collDewormingHistoriesPartial && !$this->isNew();
        if (null === $this->collDewormingHistories || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDewormingHistories) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDewormingHistories());
            }

            $query = ChildDewormingHistoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collDewormingHistories);
    }

    /**
     * Method called to associate a ChildDewormingHistory object to this object
     * through the ChildDewormingHistory foreign key attribute.
     *
     * @param ChildDewormingHistory $l ChildDewormingHistory
     * @return $this The current object (for fluent API support)
     */
    public function addDewormingHistory(ChildDewormingHistory $l)
    {
        if ($this->collDewormingHistories === null) {
            $this->initDewormingHistories();
            $this->collDewormingHistoriesPartial = true;
        }

        if (!$this->collDewormingHistories->contains($l)) {
            $this->doAddDewormingHistory($l);

            if ($this->dewormingHistoriesScheduledForDeletion and $this->dewormingHistoriesScheduledForDeletion->contains($l)) {
                $this->dewormingHistoriesScheduledForDeletion->remove($this->dewormingHistoriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDewormingHistory $dewormingHistory The ChildDewormingHistory object to add.
     */
    protected function doAddDewormingHistory(ChildDewormingHistory $dewormingHistory): void
    {
        $this->collDewormingHistories[]= $dewormingHistory;
        $dewormingHistory->setUser($this);
    }

    /**
     * @param ChildDewormingHistory $dewormingHistory The ChildDewormingHistory object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeDewormingHistory(ChildDewormingHistory $dewormingHistory)
    {
        if ($this->getDewormingHistories()->contains($dewormingHistory)) {
            $pos = $this->collDewormingHistories->search($dewormingHistory);
            $this->collDewormingHistories->remove($pos);
            if (null === $this->dewormingHistoriesScheduledForDeletion) {
                $this->dewormingHistoriesScheduledForDeletion = clone $this->collDewormingHistories;
                $this->dewormingHistoriesScheduledForDeletion->clear();
            }
            $this->dewormingHistoriesScheduledForDeletion[]= clone $dewormingHistory;
            $dewormingHistory->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related DewormingHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDewormingHistory[] List of ChildDewormingHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildDewormingHistory}> List of ChildDewormingHistory objects
     */
    public function getDewormingHistoriesJoinPatient(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDewormingHistoryQuery::create(null, $criteria);
        $query->joinWith('Patient', $joinBehavior);

        return $this->getDewormingHistories($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related DewormingHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDewormingHistory[] List of ChildDewormingHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildDewormingHistory}> List of ChildDewormingHistory objects
     */
    public function getDewormingHistoriesJoinMedication(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDewormingHistoryQuery::create(null, $criteria);
        $query->joinWith('Medication', $joinBehavior);

        return $this->getDewormingHistories($query, $con);
    }

    /**
     * Clears out the collMedicalHistories collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addMedicalHistories()
     */
    public function clearMedicalHistories()
    {
        $this->collMedicalHistories = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collMedicalHistories collection loaded partially.
     *
     * @return void
     */
    public function resetPartialMedicalHistories($v = true): void
    {
        $this->collMedicalHistoriesPartial = $v;
    }

    /**
     * Initializes the collMedicalHistories collection.
     *
     * By default this just sets the collMedicalHistories collection to an empty array (like clearcollMedicalHistories());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMedicalHistories(bool $overrideExisting = true): void
    {
        if (null !== $this->collMedicalHistories && !$overrideExisting) {
            return;
        }

        $collectionClassName = MedicalHistoryTableMap::getTableMap()->getCollectionClassName();

        $this->collMedicalHistories = new $collectionClassName;
        $this->collMedicalHistories->setModel('\MedicalHistory');
    }

    /**
     * Gets an array of ChildMedicalHistory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMedicalHistory[] List of ChildMedicalHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildMedicalHistory> List of ChildMedicalHistory objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getMedicalHistories(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collMedicalHistoriesPartial && !$this->isNew();
        if (null === $this->collMedicalHistories || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collMedicalHistories) {
                    $this->initMedicalHistories();
                } else {
                    $collectionClassName = MedicalHistoryTableMap::getTableMap()->getCollectionClassName();

                    $collMedicalHistories = new $collectionClassName;
                    $collMedicalHistories->setModel('\MedicalHistory');

                    return $collMedicalHistories;
                }
            } else {
                $collMedicalHistories = ChildMedicalHistoryQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMedicalHistoriesPartial && count($collMedicalHistories)) {
                        $this->initMedicalHistories(false);

                        foreach ($collMedicalHistories as $obj) {
                            if (false == $this->collMedicalHistories->contains($obj)) {
                                $this->collMedicalHistories->append($obj);
                            }
                        }

                        $this->collMedicalHistoriesPartial = true;
                    }

                    return $collMedicalHistories;
                }

                if ($partial && $this->collMedicalHistories) {
                    foreach ($this->collMedicalHistories as $obj) {
                        if ($obj->isNew()) {
                            $collMedicalHistories[] = $obj;
                        }
                    }
                }

                $this->collMedicalHistories = $collMedicalHistories;
                $this->collMedicalHistoriesPartial = false;
            }
        }

        return $this->collMedicalHistories;
    }

    /**
     * Sets a collection of ChildMedicalHistory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $medicalHistories A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setMedicalHistories(Collection $medicalHistories, ?ConnectionInterface $con = null)
    {
        /** @var ChildMedicalHistory[] $medicalHistoriesToDelete */
        $medicalHistoriesToDelete = $this->getMedicalHistories(new Criteria(), $con)->diff($medicalHistories);


        $this->medicalHistoriesScheduledForDeletion = $medicalHistoriesToDelete;

        foreach ($medicalHistoriesToDelete as $medicalHistoryRemoved) {
            $medicalHistoryRemoved->setUser(null);
        }

        $this->collMedicalHistories = null;
        foreach ($medicalHistories as $medicalHistory) {
            $this->addMedicalHistory($medicalHistory);
        }

        $this->collMedicalHistories = $medicalHistories;
        $this->collMedicalHistoriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MedicalHistory objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related MedicalHistory objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countMedicalHistories(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collMedicalHistoriesPartial && !$this->isNew();
        if (null === $this->collMedicalHistories || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMedicalHistories) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMedicalHistories());
            }

            $query = ChildMedicalHistoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collMedicalHistories);
    }

    /**
     * Method called to associate a ChildMedicalHistory object to this object
     * through the ChildMedicalHistory foreign key attribute.
     *
     * @param ChildMedicalHistory $l ChildMedicalHistory
     * @return $this The current object (for fluent API support)
     */
    public function addMedicalHistory(ChildMedicalHistory $l)
    {
        if ($this->collMedicalHistories === null) {
            $this->initMedicalHistories();
            $this->collMedicalHistoriesPartial = true;
        }

        if (!$this->collMedicalHistories->contains($l)) {
            $this->doAddMedicalHistory($l);

            if ($this->medicalHistoriesScheduledForDeletion and $this->medicalHistoriesScheduledForDeletion->contains($l)) {
                $this->medicalHistoriesScheduledForDeletion->remove($this->medicalHistoriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMedicalHistory $medicalHistory The ChildMedicalHistory object to add.
     */
    protected function doAddMedicalHistory(ChildMedicalHistory $medicalHistory): void
    {
        $this->collMedicalHistories[]= $medicalHistory;
        $medicalHistory->setUser($this);
    }

    /**
     * @param ChildMedicalHistory $medicalHistory The ChildMedicalHistory object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeMedicalHistory(ChildMedicalHistory $medicalHistory)
    {
        if ($this->getMedicalHistories()->contains($medicalHistory)) {
            $pos = $this->collMedicalHistories->search($medicalHistory);
            $this->collMedicalHistories->remove($pos);
            if (null === $this->medicalHistoriesScheduledForDeletion) {
                $this->medicalHistoriesScheduledForDeletion = clone $this->collMedicalHistories;
                $this->medicalHistoriesScheduledForDeletion->clear();
            }
            $this->medicalHistoriesScheduledForDeletion[]= clone $medicalHistory;
            $medicalHistory->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related MedicalHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMedicalHistory[] List of ChildMedicalHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildMedicalHistory}> List of ChildMedicalHistory objects
     */
    public function getMedicalHistoriesJoinPatient(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMedicalHistoryQuery::create(null, $criteria);
        $query->joinWith('Patient', $joinBehavior);

        return $this->getMedicalHistories($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related MedicalHistories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMedicalHistory[] List of ChildMedicalHistory objects
     * @phpstan-return ObjectCollection&\Traversable<ChildMedicalHistory}> List of ChildMedicalHistory objects
     */
    public function getMedicalHistoriesJoinMedicalDiagnosisCode(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMedicalHistoryQuery::create(null, $criteria);
        $query->joinWith('MedicalDiagnosisCode', $joinBehavior);

        return $this->getMedicalHistories($query, $con);
    }

    /**
     * Clears out the collPrescriptions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addPrescriptions()
     */
    public function clearPrescriptions()
    {
        $this->collPrescriptions = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collPrescriptions collection loaded partially.
     *
     * @return void
     */
    public function resetPartialPrescriptions($v = true): void
    {
        $this->collPrescriptionsPartial = $v;
    }

    /**
     * Initializes the collPrescriptions collection.
     *
     * By default this just sets the collPrescriptions collection to an empty array (like clearcollPrescriptions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrescriptions(bool $overrideExisting = true): void
    {
        if (null !== $this->collPrescriptions && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrescriptionTableMap::getTableMap()->getCollectionClassName();

        $this->collPrescriptions = new $collectionClassName;
        $this->collPrescriptions->setModel('\Prescription');
    }

    /**
     * Gets an array of ChildPrescription objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription> List of ChildPrescription objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getPrescriptions(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collPrescriptionsPartial && !$this->isNew();
        if (null === $this->collPrescriptions || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPrescriptions) {
                    $this->initPrescriptions();
                } else {
                    $collectionClassName = PrescriptionTableMap::getTableMap()->getCollectionClassName();

                    $collPrescriptions = new $collectionClassName;
                    $collPrescriptions->setModel('\Prescription');

                    return $collPrescriptions;
                }
            } else {
                $collPrescriptions = ChildPrescriptionQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrescriptionsPartial && count($collPrescriptions)) {
                        $this->initPrescriptions(false);

                        foreach ($collPrescriptions as $obj) {
                            if (false == $this->collPrescriptions->contains($obj)) {
                                $this->collPrescriptions->append($obj);
                            }
                        }

                        $this->collPrescriptionsPartial = true;
                    }

                    return $collPrescriptions;
                }

                if ($partial && $this->collPrescriptions) {
                    foreach ($this->collPrescriptions as $obj) {
                        if ($obj->isNew()) {
                            $collPrescriptions[] = $obj;
                        }
                    }
                }

                $this->collPrescriptions = $collPrescriptions;
                $this->collPrescriptionsPartial = false;
            }
        }

        return $this->collPrescriptions;
    }

    /**
     * Sets a collection of ChildPrescription objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $prescriptions A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setPrescriptions(Collection $prescriptions, ?ConnectionInterface $con = null)
    {
        /** @var ChildPrescription[] $prescriptionsToDelete */
        $prescriptionsToDelete = $this->getPrescriptions(new Criteria(), $con)->diff($prescriptions);


        $this->prescriptionsScheduledForDeletion = $prescriptionsToDelete;

        foreach ($prescriptionsToDelete as $prescriptionRemoved) {
            $prescriptionRemoved->setUser(null);
        }

        $this->collPrescriptions = null;
        foreach ($prescriptions as $prescription) {
            $this->addPrescription($prescription);
        }

        $this->collPrescriptions = $prescriptions;
        $this->collPrescriptionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prescription objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related Prescription objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countPrescriptions(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collPrescriptionsPartial && !$this->isNew();
        if (null === $this->collPrescriptions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrescriptions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrescriptions());
            }

            $query = ChildPrescriptionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collPrescriptions);
    }

    /**
     * Method called to associate a ChildPrescription object to this object
     * through the ChildPrescription foreign key attribute.
     *
     * @param ChildPrescription $l ChildPrescription
     * @return $this The current object (for fluent API support)
     */
    public function addPrescription(ChildPrescription $l)
    {
        if ($this->collPrescriptions === null) {
            $this->initPrescriptions();
            $this->collPrescriptionsPartial = true;
        }

        if (!$this->collPrescriptions->contains($l)) {
            $this->doAddPrescription($l);

            if ($this->prescriptionsScheduledForDeletion and $this->prescriptionsScheduledForDeletion->contains($l)) {
                $this->prescriptionsScheduledForDeletion->remove($this->prescriptionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrescription $prescription The ChildPrescription object to add.
     */
    protected function doAddPrescription(ChildPrescription $prescription): void
    {
        $this->collPrescriptions[]= $prescription;
        $prescription->setUser($this);
    }

    /**
     * @param ChildPrescription $prescription The ChildPrescription object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removePrescription(ChildPrescription $prescription)
    {
        if ($this->getPrescriptions()->contains($prescription)) {
            $pos = $this->collPrescriptions->search($prescription);
            $this->collPrescriptions->remove($pos);
            if (null === $this->prescriptionsScheduledForDeletion) {
                $this->prescriptionsScheduledForDeletion = clone $this->collPrescriptions;
                $this->prescriptionsScheduledForDeletion->clear();
            }
            $this->prescriptionsScheduledForDeletion[]= clone $prescription;
            $prescription->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related Prescriptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription}> List of ChildPrescription objects
     */
    public function getPrescriptionsJoinMedicationAdministrationMethod(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrescriptionQuery::create(null, $criteria);
        $query->joinWith('MedicationAdministrationMethod', $joinBehavior);

        return $this->getPrescriptions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related Prescriptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription}> List of ChildPrescription objects
     */
    public function getPrescriptionsJoinPatient(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrescriptionQuery::create(null, $criteria);
        $query->joinWith('Patient', $joinBehavior);

        return $this->getPrescriptions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related Prescriptions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrescription[] List of ChildPrescription objects
     * @phpstan-return ObjectCollection&\Traversable<ChildPrescription}> List of ChildPrescription objects
     */
    public function getPrescriptionsJoinMedication(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrescriptionQuery::create(null, $criteria);
        $query->joinWith('Medication', $joinBehavior);

        return $this->getPrescriptions($query, $con);
    }

    /**
     * Clears out the collUserHasTenants collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addUserHasTenants()
     */
    public function clearUserHasTenants()
    {
        $this->collUserHasTenants = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collUserHasTenants collection loaded partially.
     *
     * @return void
     */
    public function resetPartialUserHasTenants($v = true): void
    {
        $this->collUserHasTenantsPartial = $v;
    }

    /**
     * Initializes the collUserHasTenants collection.
     *
     * By default this just sets the collUserHasTenants collection to an empty array (like clearcollUserHasTenants());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserHasTenants(bool $overrideExisting = true): void
    {
        if (null !== $this->collUserHasTenants && !$overrideExisting) {
            return;
        }

        $collectionClassName = UserHasTenantTableMap::getTableMap()->getCollectionClassName();

        $this->collUserHasTenants = new $collectionClassName;
        $this->collUserHasTenants->setModel('\UserHasTenant');
    }

    /**
     * Gets an array of ChildUserHasTenant objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUserHasTenant[] List of ChildUserHasTenant objects
     * @phpstan-return ObjectCollection&\Traversable<ChildUserHasTenant> List of ChildUserHasTenant objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getUserHasTenants(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collUserHasTenantsPartial && !$this->isNew();
        if (null === $this->collUserHasTenants || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUserHasTenants) {
                    $this->initUserHasTenants();
                } else {
                    $collectionClassName = UserHasTenantTableMap::getTableMap()->getCollectionClassName();

                    $collUserHasTenants = new $collectionClassName;
                    $collUserHasTenants->setModel('\UserHasTenant');

                    return $collUserHasTenants;
                }
            } else {
                $collUserHasTenants = ChildUserHasTenantQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUserHasTenantsPartial && count($collUserHasTenants)) {
                        $this->initUserHasTenants(false);

                        foreach ($collUserHasTenants as $obj) {
                            if (false == $this->collUserHasTenants->contains($obj)) {
                                $this->collUserHasTenants->append($obj);
                            }
                        }

                        $this->collUserHasTenantsPartial = true;
                    }

                    return $collUserHasTenants;
                }

                if ($partial && $this->collUserHasTenants) {
                    foreach ($this->collUserHasTenants as $obj) {
                        if ($obj->isNew()) {
                            $collUserHasTenants[] = $obj;
                        }
                    }
                }

                $this->collUserHasTenants = $collUserHasTenants;
                $this->collUserHasTenantsPartial = false;
            }
        }

        return $this->collUserHasTenants;
    }

    /**
     * Sets a collection of ChildUserHasTenant objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $userHasTenants A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setUserHasTenants(Collection $userHasTenants, ?ConnectionInterface $con = null)
    {
        /** @var ChildUserHasTenant[] $userHasTenantsToDelete */
        $userHasTenantsToDelete = $this->getUserHasTenants(new Criteria(), $con)->diff($userHasTenants);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->userHasTenantsScheduledForDeletion = clone $userHasTenantsToDelete;

        foreach ($userHasTenantsToDelete as $userHasTenantRemoved) {
            $userHasTenantRemoved->setUser(null);
        }

        $this->collUserHasTenants = null;
        foreach ($userHasTenants as $userHasTenant) {
            $this->addUserHasTenant($userHasTenant);
        }

        $this->collUserHasTenants = $userHasTenants;
        $this->collUserHasTenantsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserHasTenant objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related UserHasTenant objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countUserHasTenants(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collUserHasTenantsPartial && !$this->isNew();
        if (null === $this->collUserHasTenants || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserHasTenants) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserHasTenants());
            }

            $query = ChildUserHasTenantQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collUserHasTenants);
    }

    /**
     * Method called to associate a ChildUserHasTenant object to this object
     * through the ChildUserHasTenant foreign key attribute.
     *
     * @param ChildUserHasTenant $l ChildUserHasTenant
     * @return $this The current object (for fluent API support)
     */
    public function addUserHasTenant(ChildUserHasTenant $l)
    {
        if ($this->collUserHasTenants === null) {
            $this->initUserHasTenants();
            $this->collUserHasTenantsPartial = true;
        }

        if (!$this->collUserHasTenants->contains($l)) {
            $this->doAddUserHasTenant($l);

            if ($this->userHasTenantsScheduledForDeletion and $this->userHasTenantsScheduledForDeletion->contains($l)) {
                $this->userHasTenantsScheduledForDeletion->remove($this->userHasTenantsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUserHasTenant $userHasTenant The ChildUserHasTenant object to add.
     */
    protected function doAddUserHasTenant(ChildUserHasTenant $userHasTenant): void
    {
        $this->collUserHasTenants[]= $userHasTenant;
        $userHasTenant->setUser($this);
    }

    /**
     * @param ChildUserHasTenant $userHasTenant The ChildUserHasTenant object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeUserHasTenant(ChildUserHasTenant $userHasTenant)
    {
        if ($this->getUserHasTenants()->contains($userHasTenant)) {
            $pos = $this->collUserHasTenants->search($userHasTenant);
            $this->collUserHasTenants->remove($pos);
            if (null === $this->userHasTenantsScheduledForDeletion) {
                $this->userHasTenantsScheduledForDeletion = clone $this->collUserHasTenants;
                $this->userHasTenantsScheduledForDeletion->clear();
            }
            $this->userHasTenantsScheduledForDeletion[]= clone $userHasTenant;
            $userHasTenant->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UserHasTenants from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserHasTenant[] List of ChildUserHasTenant objects
     * @phpstan-return ObjectCollection&\Traversable<ChildUserHasTenant}> List of ChildUserHasTenant objects
     */
    public function getUserHasTenantsJoinTenant(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserHasTenantQuery::create(null, $criteria);
        $query->joinWith('Tenant', $joinBehavior);

        return $this->getUserHasTenants($query, $con);
    }

    /**
     * Clears out the collAuthIdentities collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addAuthIdentities()
     */
    public function clearAuthIdentities()
    {
        $this->collAuthIdentities = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collAuthIdentities collection loaded partially.
     *
     * @return void
     */
    public function resetPartialAuthIdentities($v = true): void
    {
        $this->collAuthIdentitiesPartial = $v;
    }

    /**
     * Initializes the collAuthIdentities collection.
     *
     * By default this just sets the collAuthIdentities collection to an empty array (like clearcollAuthIdentities());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAuthIdentities(bool $overrideExisting = true): void
    {
        if (null !== $this->collAuthIdentities && !$overrideExisting) {
            return;
        }

        $collectionClassName = AuthIdentityTableMap::getTableMap()->getCollectionClassName();

        $this->collAuthIdentities = new $collectionClassName;
        $this->collAuthIdentities->setModel('\AuthIdentity');
    }

    /**
     * Gets an array of ChildAuthIdentity objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildAuthIdentity[] List of ChildAuthIdentity objects
     * @phpstan-return ObjectCollection&\Traversable<ChildAuthIdentity> List of ChildAuthIdentity objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getAuthIdentities(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collAuthIdentitiesPartial && !$this->isNew();
        if (null === $this->collAuthIdentities || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collAuthIdentities) {
                    $this->initAuthIdentities();
                } else {
                    $collectionClassName = AuthIdentityTableMap::getTableMap()->getCollectionClassName();

                    $collAuthIdentities = new $collectionClassName;
                    $collAuthIdentities->setModel('\AuthIdentity');

                    return $collAuthIdentities;
                }
            } else {
                $collAuthIdentities = ChildAuthIdentityQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAuthIdentitiesPartial && count($collAuthIdentities)) {
                        $this->initAuthIdentities(false);

                        foreach ($collAuthIdentities as $obj) {
                            if (false == $this->collAuthIdentities->contains($obj)) {
                                $this->collAuthIdentities->append($obj);
                            }
                        }

                        $this->collAuthIdentitiesPartial = true;
                    }

                    return $collAuthIdentities;
                }

                if ($partial && $this->collAuthIdentities) {
                    foreach ($this->collAuthIdentities as $obj) {
                        if ($obj->isNew()) {
                            $collAuthIdentities[] = $obj;
                        }
                    }
                }

                $this->collAuthIdentities = $collAuthIdentities;
                $this->collAuthIdentitiesPartial = false;
            }
        }

        return $this->collAuthIdentities;
    }

    /**
     * Sets a collection of ChildAuthIdentity objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $authIdentities A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setAuthIdentities(Collection $authIdentities, ?ConnectionInterface $con = null)
    {
        /** @var ChildAuthIdentity[] $authIdentitiesToDelete */
        $authIdentitiesToDelete = $this->getAuthIdentities(new Criteria(), $con)->diff($authIdentities);


        $this->authIdentitiesScheduledForDeletion = $authIdentitiesToDelete;

        foreach ($authIdentitiesToDelete as $authIdentityRemoved) {
            $authIdentityRemoved->setUser(null);
        }

        $this->collAuthIdentities = null;
        foreach ($authIdentities as $authIdentity) {
            $this->addAuthIdentity($authIdentity);
        }

        $this->collAuthIdentities = $authIdentities;
        $this->collAuthIdentitiesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AuthIdentity objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related AuthIdentity objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countAuthIdentities(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collAuthIdentitiesPartial && !$this->isNew();
        if (null === $this->collAuthIdentities || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAuthIdentities) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAuthIdentities());
            }

            $query = ChildAuthIdentityQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collAuthIdentities);
    }

    /**
     * Method called to associate a ChildAuthIdentity object to this object
     * through the ChildAuthIdentity foreign key attribute.
     *
     * @param ChildAuthIdentity $l ChildAuthIdentity
     * @return $this The current object (for fluent API support)
     */
    public function addAuthIdentity(ChildAuthIdentity $l)
    {
        if ($this->collAuthIdentities === null) {
            $this->initAuthIdentities();
            $this->collAuthIdentitiesPartial = true;
        }

        if (!$this->collAuthIdentities->contains($l)) {
            $this->doAddAuthIdentity($l);

            if ($this->authIdentitiesScheduledForDeletion and $this->authIdentitiesScheduledForDeletion->contains($l)) {
                $this->authIdentitiesScheduledForDeletion->remove($this->authIdentitiesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildAuthIdentity $authIdentity The ChildAuthIdentity object to add.
     */
    protected function doAddAuthIdentity(ChildAuthIdentity $authIdentity): void
    {
        $this->collAuthIdentities[]= $authIdentity;
        $authIdentity->setUser($this);
    }

    /**
     * @param ChildAuthIdentity $authIdentity The ChildAuthIdentity object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeAuthIdentity(ChildAuthIdentity $authIdentity)
    {
        if ($this->getAuthIdentities()->contains($authIdentity)) {
            $pos = $this->collAuthIdentities->search($authIdentity);
            $this->collAuthIdentities->remove($pos);
            if (null === $this->authIdentitiesScheduledForDeletion) {
                $this->authIdentitiesScheduledForDeletion = clone $this->collAuthIdentities;
                $this->authIdentitiesScheduledForDeletion->clear();
            }
            $this->authIdentitiesScheduledForDeletion[]= clone $authIdentity;
            $authIdentity->setUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collAuthLogins collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addAuthLogins()
     */
    public function clearAuthLogins()
    {
        $this->collAuthLogins = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collAuthLogins collection loaded partially.
     *
     * @return void
     */
    public function resetPartialAuthLogins($v = true): void
    {
        $this->collAuthLoginsPartial = $v;
    }

    /**
     * Initializes the collAuthLogins collection.
     *
     * By default this just sets the collAuthLogins collection to an empty array (like clearcollAuthLogins());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAuthLogins(bool $overrideExisting = true): void
    {
        if (null !== $this->collAuthLogins && !$overrideExisting) {
            return;
        }

        $collectionClassName = AuthLoginTableMap::getTableMap()->getCollectionClassName();

        $this->collAuthLogins = new $collectionClassName;
        $this->collAuthLogins->setModel('\AuthLogin');
    }

    /**
     * Gets an array of ChildAuthLogin objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildAuthLogin[] List of ChildAuthLogin objects
     * @phpstan-return ObjectCollection&\Traversable<ChildAuthLogin> List of ChildAuthLogin objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getAuthLogins(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collAuthLoginsPartial && !$this->isNew();
        if (null === $this->collAuthLogins || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collAuthLogins) {
                    $this->initAuthLogins();
                } else {
                    $collectionClassName = AuthLoginTableMap::getTableMap()->getCollectionClassName();

                    $collAuthLogins = new $collectionClassName;
                    $collAuthLogins->setModel('\AuthLogin');

                    return $collAuthLogins;
                }
            } else {
                $collAuthLogins = ChildAuthLoginQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAuthLoginsPartial && count($collAuthLogins)) {
                        $this->initAuthLogins(false);

                        foreach ($collAuthLogins as $obj) {
                            if (false == $this->collAuthLogins->contains($obj)) {
                                $this->collAuthLogins->append($obj);
                            }
                        }

                        $this->collAuthLoginsPartial = true;
                    }

                    return $collAuthLogins;
                }

                if ($partial && $this->collAuthLogins) {
                    foreach ($this->collAuthLogins as $obj) {
                        if ($obj->isNew()) {
                            $collAuthLogins[] = $obj;
                        }
                    }
                }

                $this->collAuthLogins = $collAuthLogins;
                $this->collAuthLoginsPartial = false;
            }
        }

        return $this->collAuthLogins;
    }

    /**
     * Sets a collection of ChildAuthLogin objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $authLogins A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setAuthLogins(Collection $authLogins, ?ConnectionInterface $con = null)
    {
        /** @var ChildAuthLogin[] $authLoginsToDelete */
        $authLoginsToDelete = $this->getAuthLogins(new Criteria(), $con)->diff($authLogins);


        $this->authLoginsScheduledForDeletion = $authLoginsToDelete;

        foreach ($authLoginsToDelete as $authLoginRemoved) {
            $authLoginRemoved->setUser(null);
        }

        $this->collAuthLogins = null;
        foreach ($authLogins as $authLogin) {
            $this->addAuthLogin($authLogin);
        }

        $this->collAuthLogins = $authLogins;
        $this->collAuthLoginsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AuthLogin objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related AuthLogin objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countAuthLogins(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collAuthLoginsPartial && !$this->isNew();
        if (null === $this->collAuthLogins || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAuthLogins) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAuthLogins());
            }

            $query = ChildAuthLoginQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collAuthLogins);
    }

    /**
     * Method called to associate a ChildAuthLogin object to this object
     * through the ChildAuthLogin foreign key attribute.
     *
     * @param ChildAuthLogin $l ChildAuthLogin
     * @return $this The current object (for fluent API support)
     */
    public function addAuthLogin(ChildAuthLogin $l)
    {
        if ($this->collAuthLogins === null) {
            $this->initAuthLogins();
            $this->collAuthLoginsPartial = true;
        }

        if (!$this->collAuthLogins->contains($l)) {
            $this->doAddAuthLogin($l);

            if ($this->authLoginsScheduledForDeletion and $this->authLoginsScheduledForDeletion->contains($l)) {
                $this->authLoginsScheduledForDeletion->remove($this->authLoginsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildAuthLogin $authLogin The ChildAuthLogin object to add.
     */
    protected function doAddAuthLogin(ChildAuthLogin $authLogin): void
    {
        $this->collAuthLogins[]= $authLogin;
        $authLogin->setUser($this);
    }

    /**
     * @param ChildAuthLogin $authLogin The ChildAuthLogin object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeAuthLogin(ChildAuthLogin $authLogin)
    {
        if ($this->getAuthLogins()->contains($authLogin)) {
            $pos = $this->collAuthLogins->search($authLogin);
            $this->collAuthLogins->remove($pos);
            if (null === $this->authLoginsScheduledForDeletion) {
                $this->authLoginsScheduledForDeletion = clone $this->collAuthLogins;
                $this->authLoginsScheduledForDeletion->clear();
            }
            $this->authLoginsScheduledForDeletion[]= $authLogin;
            $authLogin->setUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collAuthTokenLogins collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addAuthTokenLogins()
     */
    public function clearAuthTokenLogins()
    {
        $this->collAuthTokenLogins = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collAuthTokenLogins collection loaded partially.
     *
     * @return void
     */
    public function resetPartialAuthTokenLogins($v = true): void
    {
        $this->collAuthTokenLoginsPartial = $v;
    }

    /**
     * Initializes the collAuthTokenLogins collection.
     *
     * By default this just sets the collAuthTokenLogins collection to an empty array (like clearcollAuthTokenLogins());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAuthTokenLogins(bool $overrideExisting = true): void
    {
        if (null !== $this->collAuthTokenLogins && !$overrideExisting) {
            return;
        }

        $collectionClassName = AuthTokenLoginTableMap::getTableMap()->getCollectionClassName();

        $this->collAuthTokenLogins = new $collectionClassName;
        $this->collAuthTokenLogins->setModel('\AuthTokenLogin');
    }

    /**
     * Gets an array of ChildAuthTokenLogin objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildAuthTokenLogin[] List of ChildAuthTokenLogin objects
     * @phpstan-return ObjectCollection&\Traversable<ChildAuthTokenLogin> List of ChildAuthTokenLogin objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getAuthTokenLogins(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collAuthTokenLoginsPartial && !$this->isNew();
        if (null === $this->collAuthTokenLogins || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collAuthTokenLogins) {
                    $this->initAuthTokenLogins();
                } else {
                    $collectionClassName = AuthTokenLoginTableMap::getTableMap()->getCollectionClassName();

                    $collAuthTokenLogins = new $collectionClassName;
                    $collAuthTokenLogins->setModel('\AuthTokenLogin');

                    return $collAuthTokenLogins;
                }
            } else {
                $collAuthTokenLogins = ChildAuthTokenLoginQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAuthTokenLoginsPartial && count($collAuthTokenLogins)) {
                        $this->initAuthTokenLogins(false);

                        foreach ($collAuthTokenLogins as $obj) {
                            if (false == $this->collAuthTokenLogins->contains($obj)) {
                                $this->collAuthTokenLogins->append($obj);
                            }
                        }

                        $this->collAuthTokenLoginsPartial = true;
                    }

                    return $collAuthTokenLogins;
                }

                if ($partial && $this->collAuthTokenLogins) {
                    foreach ($this->collAuthTokenLogins as $obj) {
                        if ($obj->isNew()) {
                            $collAuthTokenLogins[] = $obj;
                        }
                    }
                }

                $this->collAuthTokenLogins = $collAuthTokenLogins;
                $this->collAuthTokenLoginsPartial = false;
            }
        }

        return $this->collAuthTokenLogins;
    }

    /**
     * Sets a collection of ChildAuthTokenLogin objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $authTokenLogins A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setAuthTokenLogins(Collection $authTokenLogins, ?ConnectionInterface $con = null)
    {
        /** @var ChildAuthTokenLogin[] $authTokenLoginsToDelete */
        $authTokenLoginsToDelete = $this->getAuthTokenLogins(new Criteria(), $con)->diff($authTokenLogins);


        $this->authTokenLoginsScheduledForDeletion = $authTokenLoginsToDelete;

        foreach ($authTokenLoginsToDelete as $authTokenLoginRemoved) {
            $authTokenLoginRemoved->setUser(null);
        }

        $this->collAuthTokenLogins = null;
        foreach ($authTokenLogins as $authTokenLogin) {
            $this->addAuthTokenLogin($authTokenLogin);
        }

        $this->collAuthTokenLogins = $authTokenLogins;
        $this->collAuthTokenLoginsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AuthTokenLogin objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related AuthTokenLogin objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countAuthTokenLogins(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collAuthTokenLoginsPartial && !$this->isNew();
        if (null === $this->collAuthTokenLogins || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAuthTokenLogins) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAuthTokenLogins());
            }

            $query = ChildAuthTokenLoginQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collAuthTokenLogins);
    }

    /**
     * Method called to associate a ChildAuthTokenLogin object to this object
     * through the ChildAuthTokenLogin foreign key attribute.
     *
     * @param ChildAuthTokenLogin $l ChildAuthTokenLogin
     * @return $this The current object (for fluent API support)
     */
    public function addAuthTokenLogin(ChildAuthTokenLogin $l)
    {
        if ($this->collAuthTokenLogins === null) {
            $this->initAuthTokenLogins();
            $this->collAuthTokenLoginsPartial = true;
        }

        if (!$this->collAuthTokenLogins->contains($l)) {
            $this->doAddAuthTokenLogin($l);

            if ($this->authTokenLoginsScheduledForDeletion and $this->authTokenLoginsScheduledForDeletion->contains($l)) {
                $this->authTokenLoginsScheduledForDeletion->remove($this->authTokenLoginsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildAuthTokenLogin $authTokenLogin The ChildAuthTokenLogin object to add.
     */
    protected function doAddAuthTokenLogin(ChildAuthTokenLogin $authTokenLogin): void
    {
        $this->collAuthTokenLogins[]= $authTokenLogin;
        $authTokenLogin->setUser($this);
    }

    /**
     * @param ChildAuthTokenLogin $authTokenLogin The ChildAuthTokenLogin object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeAuthTokenLogin(ChildAuthTokenLogin $authTokenLogin)
    {
        if ($this->getAuthTokenLogins()->contains($authTokenLogin)) {
            $pos = $this->collAuthTokenLogins->search($authTokenLogin);
            $this->collAuthTokenLogins->remove($pos);
            if (null === $this->authTokenLoginsScheduledForDeletion) {
                $this->authTokenLoginsScheduledForDeletion = clone $this->collAuthTokenLogins;
                $this->authTokenLoginsScheduledForDeletion->clear();
            }
            $this->authTokenLoginsScheduledForDeletion[]= $authTokenLogin;
            $authTokenLogin->setUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collAuthRememberTokens collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addAuthRememberTokens()
     */
    public function clearAuthRememberTokens()
    {
        $this->collAuthRememberTokens = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collAuthRememberTokens collection loaded partially.
     *
     * @return void
     */
    public function resetPartialAuthRememberTokens($v = true): void
    {
        $this->collAuthRememberTokensPartial = $v;
    }

    /**
     * Initializes the collAuthRememberTokens collection.
     *
     * By default this just sets the collAuthRememberTokens collection to an empty array (like clearcollAuthRememberTokens());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAuthRememberTokens(bool $overrideExisting = true): void
    {
        if (null !== $this->collAuthRememberTokens && !$overrideExisting) {
            return;
        }

        $collectionClassName = AuthRememberTokenTableMap::getTableMap()->getCollectionClassName();

        $this->collAuthRememberTokens = new $collectionClassName;
        $this->collAuthRememberTokens->setModel('\AuthRememberToken');
    }

    /**
     * Gets an array of ChildAuthRememberToken objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildAuthRememberToken[] List of ChildAuthRememberToken objects
     * @phpstan-return ObjectCollection&\Traversable<ChildAuthRememberToken> List of ChildAuthRememberToken objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getAuthRememberTokens(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collAuthRememberTokensPartial && !$this->isNew();
        if (null === $this->collAuthRememberTokens || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collAuthRememberTokens) {
                    $this->initAuthRememberTokens();
                } else {
                    $collectionClassName = AuthRememberTokenTableMap::getTableMap()->getCollectionClassName();

                    $collAuthRememberTokens = new $collectionClassName;
                    $collAuthRememberTokens->setModel('\AuthRememberToken');

                    return $collAuthRememberTokens;
                }
            } else {
                $collAuthRememberTokens = ChildAuthRememberTokenQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAuthRememberTokensPartial && count($collAuthRememberTokens)) {
                        $this->initAuthRememberTokens(false);

                        foreach ($collAuthRememberTokens as $obj) {
                            if (false == $this->collAuthRememberTokens->contains($obj)) {
                                $this->collAuthRememberTokens->append($obj);
                            }
                        }

                        $this->collAuthRememberTokensPartial = true;
                    }

                    return $collAuthRememberTokens;
                }

                if ($partial && $this->collAuthRememberTokens) {
                    foreach ($this->collAuthRememberTokens as $obj) {
                        if ($obj->isNew()) {
                            $collAuthRememberTokens[] = $obj;
                        }
                    }
                }

                $this->collAuthRememberTokens = $collAuthRememberTokens;
                $this->collAuthRememberTokensPartial = false;
            }
        }

        return $this->collAuthRememberTokens;
    }

    /**
     * Sets a collection of ChildAuthRememberToken objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $authRememberTokens A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setAuthRememberTokens(Collection $authRememberTokens, ?ConnectionInterface $con = null)
    {
        /** @var ChildAuthRememberToken[] $authRememberTokensToDelete */
        $authRememberTokensToDelete = $this->getAuthRememberTokens(new Criteria(), $con)->diff($authRememberTokens);


        $this->authRememberTokensScheduledForDeletion = $authRememberTokensToDelete;

        foreach ($authRememberTokensToDelete as $authRememberTokenRemoved) {
            $authRememberTokenRemoved->setUser(null);
        }

        $this->collAuthRememberTokens = null;
        foreach ($authRememberTokens as $authRememberToken) {
            $this->addAuthRememberToken($authRememberToken);
        }

        $this->collAuthRememberTokens = $authRememberTokens;
        $this->collAuthRememberTokensPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AuthRememberToken objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related AuthRememberToken objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countAuthRememberTokens(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collAuthRememberTokensPartial && !$this->isNew();
        if (null === $this->collAuthRememberTokens || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAuthRememberTokens) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAuthRememberTokens());
            }

            $query = ChildAuthRememberTokenQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collAuthRememberTokens);
    }

    /**
     * Method called to associate a ChildAuthRememberToken object to this object
     * through the ChildAuthRememberToken foreign key attribute.
     *
     * @param ChildAuthRememberToken $l ChildAuthRememberToken
     * @return $this The current object (for fluent API support)
     */
    public function addAuthRememberToken(ChildAuthRememberToken $l)
    {
        if ($this->collAuthRememberTokens === null) {
            $this->initAuthRememberTokens();
            $this->collAuthRememberTokensPartial = true;
        }

        if (!$this->collAuthRememberTokens->contains($l)) {
            $this->doAddAuthRememberToken($l);

            if ($this->authRememberTokensScheduledForDeletion and $this->authRememberTokensScheduledForDeletion->contains($l)) {
                $this->authRememberTokensScheduledForDeletion->remove($this->authRememberTokensScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildAuthRememberToken $authRememberToken The ChildAuthRememberToken object to add.
     */
    protected function doAddAuthRememberToken(ChildAuthRememberToken $authRememberToken): void
    {
        $this->collAuthRememberTokens[]= $authRememberToken;
        $authRememberToken->setUser($this);
    }

    /**
     * @param ChildAuthRememberToken $authRememberToken The ChildAuthRememberToken object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeAuthRememberToken(ChildAuthRememberToken $authRememberToken)
    {
        if ($this->getAuthRememberTokens()->contains($authRememberToken)) {
            $pos = $this->collAuthRememberTokens->search($authRememberToken);
            $this->collAuthRememberTokens->remove($pos);
            if (null === $this->authRememberTokensScheduledForDeletion) {
                $this->authRememberTokensScheduledForDeletion = clone $this->collAuthRememberTokens;
                $this->authRememberTokensScheduledForDeletion->clear();
            }
            $this->authRememberTokensScheduledForDeletion[]= clone $authRememberToken;
            $authRememberToken->setUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collAuthGroupsUsers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addAuthGroupsUsers()
     */
    public function clearAuthGroupsUsers()
    {
        $this->collAuthGroupsUsers = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collAuthGroupsUsers collection loaded partially.
     *
     * @return void
     */
    public function resetPartialAuthGroupsUsers($v = true): void
    {
        $this->collAuthGroupsUsersPartial = $v;
    }

    /**
     * Initializes the collAuthGroupsUsers collection.
     *
     * By default this just sets the collAuthGroupsUsers collection to an empty array (like clearcollAuthGroupsUsers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAuthGroupsUsers(bool $overrideExisting = true): void
    {
        if (null !== $this->collAuthGroupsUsers && !$overrideExisting) {
            return;
        }

        $collectionClassName = AuthGroupsUserTableMap::getTableMap()->getCollectionClassName();

        $this->collAuthGroupsUsers = new $collectionClassName;
        $this->collAuthGroupsUsers->setModel('\AuthGroupsUser');
    }

    /**
     * Gets an array of ChildAuthGroupsUser objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildAuthGroupsUser[] List of ChildAuthGroupsUser objects
     * @phpstan-return ObjectCollection&\Traversable<ChildAuthGroupsUser> List of ChildAuthGroupsUser objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getAuthGroupsUsers(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collAuthGroupsUsersPartial && !$this->isNew();
        if (null === $this->collAuthGroupsUsers || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collAuthGroupsUsers) {
                    $this->initAuthGroupsUsers();
                } else {
                    $collectionClassName = AuthGroupsUserTableMap::getTableMap()->getCollectionClassName();

                    $collAuthGroupsUsers = new $collectionClassName;
                    $collAuthGroupsUsers->setModel('\AuthGroupsUser');

                    return $collAuthGroupsUsers;
                }
            } else {
                $collAuthGroupsUsers = ChildAuthGroupsUserQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAuthGroupsUsersPartial && count($collAuthGroupsUsers)) {
                        $this->initAuthGroupsUsers(false);

                        foreach ($collAuthGroupsUsers as $obj) {
                            if (false == $this->collAuthGroupsUsers->contains($obj)) {
                                $this->collAuthGroupsUsers->append($obj);
                            }
                        }

                        $this->collAuthGroupsUsersPartial = true;
                    }

                    return $collAuthGroupsUsers;
                }

                if ($partial && $this->collAuthGroupsUsers) {
                    foreach ($this->collAuthGroupsUsers as $obj) {
                        if ($obj->isNew()) {
                            $collAuthGroupsUsers[] = $obj;
                        }
                    }
                }

                $this->collAuthGroupsUsers = $collAuthGroupsUsers;
                $this->collAuthGroupsUsersPartial = false;
            }
        }

        return $this->collAuthGroupsUsers;
    }

    /**
     * Sets a collection of ChildAuthGroupsUser objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $authGroupsUsers A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setAuthGroupsUsers(Collection $authGroupsUsers, ?ConnectionInterface $con = null)
    {
        /** @var ChildAuthGroupsUser[] $authGroupsUsersToDelete */
        $authGroupsUsersToDelete = $this->getAuthGroupsUsers(new Criteria(), $con)->diff($authGroupsUsers);


        $this->authGroupsUsersScheduledForDeletion = $authGroupsUsersToDelete;

        foreach ($authGroupsUsersToDelete as $authGroupsUserRemoved) {
            $authGroupsUserRemoved->setUser(null);
        }

        $this->collAuthGroupsUsers = null;
        foreach ($authGroupsUsers as $authGroupsUser) {
            $this->addAuthGroupsUser($authGroupsUser);
        }

        $this->collAuthGroupsUsers = $authGroupsUsers;
        $this->collAuthGroupsUsersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AuthGroupsUser objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related AuthGroupsUser objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countAuthGroupsUsers(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collAuthGroupsUsersPartial && !$this->isNew();
        if (null === $this->collAuthGroupsUsers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAuthGroupsUsers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAuthGroupsUsers());
            }

            $query = ChildAuthGroupsUserQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collAuthGroupsUsers);
    }

    /**
     * Method called to associate a ChildAuthGroupsUser object to this object
     * through the ChildAuthGroupsUser foreign key attribute.
     *
     * @param ChildAuthGroupsUser $l ChildAuthGroupsUser
     * @return $this The current object (for fluent API support)
     */
    public function addAuthGroupsUser(ChildAuthGroupsUser $l)
    {
        if ($this->collAuthGroupsUsers === null) {
            $this->initAuthGroupsUsers();
            $this->collAuthGroupsUsersPartial = true;
        }

        if (!$this->collAuthGroupsUsers->contains($l)) {
            $this->doAddAuthGroupsUser($l);

            if ($this->authGroupsUsersScheduledForDeletion and $this->authGroupsUsersScheduledForDeletion->contains($l)) {
                $this->authGroupsUsersScheduledForDeletion->remove($this->authGroupsUsersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildAuthGroupsUser $authGroupsUser The ChildAuthGroupsUser object to add.
     */
    protected function doAddAuthGroupsUser(ChildAuthGroupsUser $authGroupsUser): void
    {
        $this->collAuthGroupsUsers[]= $authGroupsUser;
        $authGroupsUser->setUser($this);
    }

    /**
     * @param ChildAuthGroupsUser $authGroupsUser The ChildAuthGroupsUser object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeAuthGroupsUser(ChildAuthGroupsUser $authGroupsUser)
    {
        if ($this->getAuthGroupsUsers()->contains($authGroupsUser)) {
            $pos = $this->collAuthGroupsUsers->search($authGroupsUser);
            $this->collAuthGroupsUsers->remove($pos);
            if (null === $this->authGroupsUsersScheduledForDeletion) {
                $this->authGroupsUsersScheduledForDeletion = clone $this->collAuthGroupsUsers;
                $this->authGroupsUsersScheduledForDeletion->clear();
            }
            $this->authGroupsUsersScheduledForDeletion[]= clone $authGroupsUser;
            $authGroupsUser->setUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collAuthPermissionsUsers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addAuthPermissionsUsers()
     */
    public function clearAuthPermissionsUsers()
    {
        $this->collAuthPermissionsUsers = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collAuthPermissionsUsers collection loaded partially.
     *
     * @return void
     */
    public function resetPartialAuthPermissionsUsers($v = true): void
    {
        $this->collAuthPermissionsUsersPartial = $v;
    }

    /**
     * Initializes the collAuthPermissionsUsers collection.
     *
     * By default this just sets the collAuthPermissionsUsers collection to an empty array (like clearcollAuthPermissionsUsers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAuthPermissionsUsers(bool $overrideExisting = true): void
    {
        if (null !== $this->collAuthPermissionsUsers && !$overrideExisting) {
            return;
        }

        $collectionClassName = AuthPermissionsUserTableMap::getTableMap()->getCollectionClassName();

        $this->collAuthPermissionsUsers = new $collectionClassName;
        $this->collAuthPermissionsUsers->setModel('\AuthPermissionsUser');
    }

    /**
     * Gets an array of ChildAuthPermissionsUser objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildAuthPermissionsUser[] List of ChildAuthPermissionsUser objects
     * @phpstan-return ObjectCollection&\Traversable<ChildAuthPermissionsUser> List of ChildAuthPermissionsUser objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getAuthPermissionsUsers(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collAuthPermissionsUsersPartial && !$this->isNew();
        if (null === $this->collAuthPermissionsUsers || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collAuthPermissionsUsers) {
                    $this->initAuthPermissionsUsers();
                } else {
                    $collectionClassName = AuthPermissionsUserTableMap::getTableMap()->getCollectionClassName();

                    $collAuthPermissionsUsers = new $collectionClassName;
                    $collAuthPermissionsUsers->setModel('\AuthPermissionsUser');

                    return $collAuthPermissionsUsers;
                }
            } else {
                $collAuthPermissionsUsers = ChildAuthPermissionsUserQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAuthPermissionsUsersPartial && count($collAuthPermissionsUsers)) {
                        $this->initAuthPermissionsUsers(false);

                        foreach ($collAuthPermissionsUsers as $obj) {
                            if (false == $this->collAuthPermissionsUsers->contains($obj)) {
                                $this->collAuthPermissionsUsers->append($obj);
                            }
                        }

                        $this->collAuthPermissionsUsersPartial = true;
                    }

                    return $collAuthPermissionsUsers;
                }

                if ($partial && $this->collAuthPermissionsUsers) {
                    foreach ($this->collAuthPermissionsUsers as $obj) {
                        if ($obj->isNew()) {
                            $collAuthPermissionsUsers[] = $obj;
                        }
                    }
                }

                $this->collAuthPermissionsUsers = $collAuthPermissionsUsers;
                $this->collAuthPermissionsUsersPartial = false;
            }
        }

        return $this->collAuthPermissionsUsers;
    }

    /**
     * Sets a collection of ChildAuthPermissionsUser objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $authPermissionsUsers A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setAuthPermissionsUsers(Collection $authPermissionsUsers, ?ConnectionInterface $con = null)
    {
        /** @var ChildAuthPermissionsUser[] $authPermissionsUsersToDelete */
        $authPermissionsUsersToDelete = $this->getAuthPermissionsUsers(new Criteria(), $con)->diff($authPermissionsUsers);


        $this->authPermissionsUsersScheduledForDeletion = $authPermissionsUsersToDelete;

        foreach ($authPermissionsUsersToDelete as $authPermissionsUserRemoved) {
            $authPermissionsUserRemoved->setUser(null);
        }

        $this->collAuthPermissionsUsers = null;
        foreach ($authPermissionsUsers as $authPermissionsUser) {
            $this->addAuthPermissionsUser($authPermissionsUser);
        }

        $this->collAuthPermissionsUsers = $authPermissionsUsers;
        $this->collAuthPermissionsUsersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related AuthPermissionsUser objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related AuthPermissionsUser objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countAuthPermissionsUsers(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collAuthPermissionsUsersPartial && !$this->isNew();
        if (null === $this->collAuthPermissionsUsers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAuthPermissionsUsers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAuthPermissionsUsers());
            }

            $query = ChildAuthPermissionsUserQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collAuthPermissionsUsers);
    }

    /**
     * Method called to associate a ChildAuthPermissionsUser object to this object
     * through the ChildAuthPermissionsUser foreign key attribute.
     *
     * @param ChildAuthPermissionsUser $l ChildAuthPermissionsUser
     * @return $this The current object (for fluent API support)
     */
    public function addAuthPermissionsUser(ChildAuthPermissionsUser $l)
    {
        if ($this->collAuthPermissionsUsers === null) {
            $this->initAuthPermissionsUsers();
            $this->collAuthPermissionsUsersPartial = true;
        }

        if (!$this->collAuthPermissionsUsers->contains($l)) {
            $this->doAddAuthPermissionsUser($l);

            if ($this->authPermissionsUsersScheduledForDeletion and $this->authPermissionsUsersScheduledForDeletion->contains($l)) {
                $this->authPermissionsUsersScheduledForDeletion->remove($this->authPermissionsUsersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildAuthPermissionsUser $authPermissionsUser The ChildAuthPermissionsUser object to add.
     */
    protected function doAddAuthPermissionsUser(ChildAuthPermissionsUser $authPermissionsUser): void
    {
        $this->collAuthPermissionsUsers[]= $authPermissionsUser;
        $authPermissionsUser->setUser($this);
    }

    /**
     * @param ChildAuthPermissionsUser $authPermissionsUser The ChildAuthPermissionsUser object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeAuthPermissionsUser(ChildAuthPermissionsUser $authPermissionsUser)
    {
        if ($this->getAuthPermissionsUsers()->contains($authPermissionsUser)) {
            $pos = $this->collAuthPermissionsUsers->search($authPermissionsUser);
            $this->collAuthPermissionsUsers->remove($pos);
            if (null === $this->authPermissionsUsersScheduledForDeletion) {
                $this->authPermissionsUsersScheduledForDeletion = clone $this->collAuthPermissionsUsers;
                $this->authPermissionsUsersScheduledForDeletion->clear();
            }
            $this->authPermissionsUsersScheduledForDeletion[]= clone $authPermissionsUser;
            $authPermissionsUser->setUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collTenants collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTenants()
     */
    public function clearTenants()
    {
        $this->collTenants = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Initializes the collTenants crossRef collection.
     *
     * By default this just sets the collTenants collection to an empty collection (like clearTenants());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initTenants()
    {
        $collectionClassName = UserHasTenantTableMap::getTableMap()->getCollectionClassName();

        $this->collTenants = new $collectionClassName;
        $this->collTenantsPartial = true;
        $this->collTenants->setModel('\Tenant');
    }

    /**
     * Checks if the collTenants collection is loaded.
     *
     * @return bool
     */
    public function isTenantsLoaded(): bool
    {
        return null !== $this->collTenants;
    }

    /**
     * Gets a collection of ChildTenant objects related by a many-to-many relationship
     * to the current object by way of the user_has_tenant cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param ConnectionInterface $con Optional connection object
     *
     * @return ObjectCollection|ChildTenant[] List of ChildTenant objects
     * @phpstan-return ObjectCollection&\Traversable<ChildTenant> List of ChildTenant objects
     */
    public function getTenants(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collTenantsPartial && !$this->isNew();
        if (null === $this->collTenants || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collTenants) {
                    $this->initTenants();
                }
            } else {

                $query = ChildTenantQuery::create(null, $criteria)
                    ->filterByUser($this);
                $collTenants = $query->find($con);
                if (null !== $criteria) {
                    return $collTenants;
                }

                if ($partial && $this->collTenants) {
                    //make sure that already added objects gets added to the list of the database.
                    foreach ($this->collTenants as $obj) {
                        if (!$collTenants->contains($obj)) {
                            $collTenants[] = $obj;
                        }
                    }
                }

                $this->collTenants = $collTenants;
                $this->collTenantsPartial = false;
            }
        }

        return $this->collTenants;
    }

    /**
     * Sets a collection of Tenant objects related by a many-to-many relationship
     * to the current object by way of the user_has_tenant cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $tenants A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setTenants(Collection $tenants, ?ConnectionInterface $con = null)
    {
        $this->clearTenants();
        $currentTenants = $this->getTenants();

        $tenantsScheduledForDeletion = $currentTenants->diff($tenants);

        foreach ($tenantsScheduledForDeletion as $toDelete) {
            $this->removeTenant($toDelete);
        }

        foreach ($tenants as $tenant) {
            if (!$currentTenants->contains($tenant)) {
                $this->doAddTenant($tenant);
            }
        }

        $this->collTenantsPartial = false;
        $this->collTenants = $tenants;

        return $this;
    }

    /**
     * Gets the number of Tenant objects related by a many-to-many relationship
     * to the current object by way of the user_has_tenant cross-reference table.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param bool $distinct Set to true to force count distinct
     * @param ConnectionInterface $con Optional connection object
     *
     * @return int The number of related Tenant objects
     */
    public function countTenants(?Criteria $criteria = null, $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collTenantsPartial && !$this->isNew();
        if (null === $this->collTenants || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTenants) {
                return 0;
            } else {

                if ($partial && !$criteria) {
                    return count($this->getTenants());
                }

                $query = ChildTenantQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByUser($this)
                    ->count($con);
            }
        } else {
            return count($this->collTenants);
        }
    }

    /**
     * Associate a ChildTenant to this object
     * through the user_has_tenant cross reference table.
     *
     * @param ChildTenant $tenant
     * @return ChildUser The current object (for fluent API support)
     */
    public function addTenant(ChildTenant $tenant)
    {
        if ($this->collTenants === null) {
            $this->initTenants();
        }

        if (!$this->getTenants()->contains($tenant)) {
            // only add it if the **same** object is not already associated
            $this->collTenants->push($tenant);
            $this->doAddTenant($tenant);
        }

        return $this;
    }

    /**
     *
     * @param ChildTenant $tenant
     */
    protected function doAddTenant(ChildTenant $tenant)
    {
        $userHasTenant = new ChildUserHasTenant();

        $userHasTenant->setTenant($tenant);

        $userHasTenant->setUser($this);

        $this->addUserHasTenant($userHasTenant);

        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$tenant->isUsersLoaded()) {
            $tenant->initUsers();
            $tenant->getUsers()->push($this);
        } elseif (!$tenant->getUsers()->contains($this)) {
            $tenant->getUsers()->push($this);
        }

    }

    /**
     * Remove tenant of this object
     * through the user_has_tenant cross reference table.
     *
     * @param ChildTenant $tenant
     * @return ChildUser The current object (for fluent API support)
     */
    public function removeTenant(ChildTenant $tenant)
    {
        if ($this->getTenants()->contains($tenant)) {
            $userHasTenant = new ChildUserHasTenant();
            $userHasTenant->setTenant($tenant);
            if ($tenant->isUsersLoaded()) {
                //remove the back reference if available
                $tenant->getUsers()->removeObject($this);
            }

            $userHasTenant->setUser($this);
            $this->removeUserHasTenant(clone $userHasTenant);
            $userHasTenant->clear();

            $this->collTenants->remove($this->collTenants->search($tenant));

            if (null === $this->tenantsScheduledForDeletion) {
                $this->tenantsScheduledForDeletion = clone $this->collTenants;
                $this->tenantsScheduledForDeletion->clear();
            }

            $this->tenantsScheduledForDeletion->push($tenant);
        }


        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     *
     * @return $this
     */
    public function clear()
    {
        $this->id = null;
        $this->username = null;
        $this->display_name = null;
        $this->status = null;
        $this->status_message = null;
        $this->active = null;
        $this->last_active = null;
        $this->deleted_at = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);

        return $this;
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param bool $deep Whether to also clear the references on all referrer objects.
     * @return $this
     */
    public function clearAllReferences(bool $deep = false)
    {
        if ($deep) {
            if ($this->collVaccinationHistories) {
                foreach ($this->collVaccinationHistories as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPhysicalExaminations) {
                foreach ($this->collPhysicalExaminations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDewormingHistories) {
                foreach ($this->collDewormingHistories as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMedicalHistories) {
                foreach ($this->collMedicalHistories as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrescriptions) {
                foreach ($this->collPrescriptions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUserHasTenants) {
                foreach ($this->collUserHasTenants as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAuthIdentities) {
                foreach ($this->collAuthIdentities as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAuthLogins) {
                foreach ($this->collAuthLogins as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAuthTokenLogins) {
                foreach ($this->collAuthTokenLogins as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAuthRememberTokens) {
                foreach ($this->collAuthRememberTokens as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAuthGroupsUsers) {
                foreach ($this->collAuthGroupsUsers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAuthPermissionsUsers) {
                foreach ($this->collAuthPermissionsUsers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTenants) {
                foreach ($this->collTenants as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collVaccinationHistories = null;
        $this->collPhysicalExaminations = null;
        $this->collDewormingHistories = null;
        $this->collMedicalHistories = null;
        $this->collPrescriptions = null;
        $this->collUserHasTenants = null;
        $this->collAuthIdentities = null;
        $this->collAuthLogins = null;
        $this->collAuthTokenLogins = null;
        $this->collAuthRememberTokens = null;
        $this->collAuthGroupsUsers = null;
        $this->collAuthPermissionsUsers = null;
        $this->collTenants = null;
        return $this;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UserTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return $this The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[UserTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preSave(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postSave(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before inserting to database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preInsert(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postInsert(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preUpdate(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postUpdate(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preDelete(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postDelete(?ConnectionInterface $con = null): void
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
