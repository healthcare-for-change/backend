<?php

use Base\MedicalDiagnosisCode as BaseMedicalDiagnosisCode;

/**
 * Skeleton subclass for representing a row from the 'medical_diagnosis_code' table.
 *
 * Contains the diagnosis code as of http://www.hl7.org/fhir/valueset-condition-code.html
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class MedicalDiagnosisCode extends BaseMedicalDiagnosisCode
{

}
