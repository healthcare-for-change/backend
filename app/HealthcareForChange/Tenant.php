<?php

use Base\Tenant as BaseTenant;

/**
 * Skeleton subclass for representing a row from the 'tenant' table.
 *
 * Core table for holding tenant information
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class Tenant extends BaseTenant
{

}
