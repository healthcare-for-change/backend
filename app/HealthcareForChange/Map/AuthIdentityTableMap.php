<?php

namespace Map;

use \AuthIdentity;
use \AuthIdentityQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'auth_identities' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class AuthIdentityTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.AuthIdentityTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'HealtchareForChange';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'auth_identities';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\AuthIdentity';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'AuthIdentity';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    public const COL_ID = 'auth_identities.id';

    /**
     * the column name for the user_id field
     */
    public const COL_USER_ID = 'auth_identities.user_id';

    /**
     * the column name for the type field
     */
    public const COL_TYPE = 'auth_identities.type';

    /**
     * the column name for the name field
     */
    public const COL_NAME = 'auth_identities.name';

    /**
     * the column name for the secret field
     */
    public const COL_SECRET = 'auth_identities.secret';

    /**
     * the column name for the secret2 field
     */
    public const COL_SECRET2 = 'auth_identities.secret2';

    /**
     * the column name for the expires field
     */
    public const COL_EXPIRES = 'auth_identities.expires';

    /**
     * the column name for the extra field
     */
    public const COL_EXTRA = 'auth_identities.extra';

    /**
     * the column name for the force_reset field
     */
    public const COL_FORCE_RESET = 'auth_identities.force_reset';

    /**
     * the column name for the last_used_at field
     */
    public const COL_LAST_USED_AT = 'auth_identities.last_used_at';

    /**
     * the column name for the created_at field
     */
    public const COL_CREATED_AT = 'auth_identities.created_at';

    /**
     * the column name for the updated_at field
     */
    public const COL_UPDATED_AT = 'auth_identities.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Id', 'UserId', 'Type', 'Name', 'Secret', 'Secret2', 'Expires', 'Extra', 'ForceReset', 'LastUsedAt', 'CreatedAt', 'UpdatedAt', ],
        self::TYPE_CAMELNAME     => ['id', 'userId', 'type', 'name', 'secret', 'secret2', 'expires', 'extra', 'forceReset', 'lastUsedAt', 'createdAt', 'updatedAt', ],
        self::TYPE_COLNAME       => [AuthIdentityTableMap::COL_ID, AuthIdentityTableMap::COL_USER_ID, AuthIdentityTableMap::COL_TYPE, AuthIdentityTableMap::COL_NAME, AuthIdentityTableMap::COL_SECRET, AuthIdentityTableMap::COL_SECRET2, AuthIdentityTableMap::COL_EXPIRES, AuthIdentityTableMap::COL_EXTRA, AuthIdentityTableMap::COL_FORCE_RESET, AuthIdentityTableMap::COL_LAST_USED_AT, AuthIdentityTableMap::COL_CREATED_AT, AuthIdentityTableMap::COL_UPDATED_AT, ],
        self::TYPE_FIELDNAME     => ['id', 'user_id', 'type', 'name', 'secret', 'secret2', 'expires', 'extra', 'force_reset', 'last_used_at', 'created_at', 'updated_at', ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Id' => 0, 'UserId' => 1, 'Type' => 2, 'Name' => 3, 'Secret' => 4, 'Secret2' => 5, 'Expires' => 6, 'Extra' => 7, 'ForceReset' => 8, 'LastUsedAt' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, ],
        self::TYPE_CAMELNAME     => ['id' => 0, 'userId' => 1, 'type' => 2, 'name' => 3, 'secret' => 4, 'secret2' => 5, 'expires' => 6, 'extra' => 7, 'forceReset' => 8, 'lastUsedAt' => 9, 'createdAt' => 10, 'updatedAt' => 11, ],
        self::TYPE_COLNAME       => [AuthIdentityTableMap::COL_ID => 0, AuthIdentityTableMap::COL_USER_ID => 1, AuthIdentityTableMap::COL_TYPE => 2, AuthIdentityTableMap::COL_NAME => 3, AuthIdentityTableMap::COL_SECRET => 4, AuthIdentityTableMap::COL_SECRET2 => 5, AuthIdentityTableMap::COL_EXPIRES => 6, AuthIdentityTableMap::COL_EXTRA => 7, AuthIdentityTableMap::COL_FORCE_RESET => 8, AuthIdentityTableMap::COL_LAST_USED_AT => 9, AuthIdentityTableMap::COL_CREATED_AT => 10, AuthIdentityTableMap::COL_UPDATED_AT => 11, ],
        self::TYPE_FIELDNAME     => ['id' => 0, 'user_id' => 1, 'type' => 2, 'name' => 3, 'secret' => 4, 'secret2' => 5, 'expires' => 6, 'extra' => 7, 'force_reset' => 8, 'last_used_at' => 9, 'created_at' => 10, 'updated_at' => 11, ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Id' => 'ID',
        'AuthIdentity.Id' => 'ID',
        'id' => 'ID',
        'authIdentity.id' => 'ID',
        'AuthIdentityTableMap::COL_ID' => 'ID',
        'COL_ID' => 'ID',
        'auth_identities.id' => 'ID',
        'UserId' => 'USER_ID',
        'AuthIdentity.UserId' => 'USER_ID',
        'userId' => 'USER_ID',
        'authIdentity.userId' => 'USER_ID',
        'AuthIdentityTableMap::COL_USER_ID' => 'USER_ID',
        'COL_USER_ID' => 'USER_ID',
        'user_id' => 'USER_ID',
        'auth_identities.user_id' => 'USER_ID',
        'Type' => 'TYPE',
        'AuthIdentity.Type' => 'TYPE',
        'type' => 'TYPE',
        'authIdentity.type' => 'TYPE',
        'AuthIdentityTableMap::COL_TYPE' => 'TYPE',
        'COL_TYPE' => 'TYPE',
        'auth_identities.type' => 'TYPE',
        'Name' => 'NAME',
        'AuthIdentity.Name' => 'NAME',
        'name' => 'NAME',
        'authIdentity.name' => 'NAME',
        'AuthIdentityTableMap::COL_NAME' => 'NAME',
        'COL_NAME' => 'NAME',
        'auth_identities.name' => 'NAME',
        'Secret' => 'SECRET',
        'AuthIdentity.Secret' => 'SECRET',
        'secret' => 'SECRET',
        'authIdentity.secret' => 'SECRET',
        'AuthIdentityTableMap::COL_SECRET' => 'SECRET',
        'COL_SECRET' => 'SECRET',
        'auth_identities.secret' => 'SECRET',
        'Secret2' => 'SECRET2',
        'AuthIdentity.Secret2' => 'SECRET2',
        'secret2' => 'SECRET2',
        'authIdentity.secret2' => 'SECRET2',
        'AuthIdentityTableMap::COL_SECRET2' => 'SECRET2',
        'COL_SECRET2' => 'SECRET2',
        'auth_identities.secret2' => 'SECRET2',
        'Expires' => 'EXPIRES',
        'AuthIdentity.Expires' => 'EXPIRES',
        'expires' => 'EXPIRES',
        'authIdentity.expires' => 'EXPIRES',
        'AuthIdentityTableMap::COL_EXPIRES' => 'EXPIRES',
        'COL_EXPIRES' => 'EXPIRES',
        'auth_identities.expires' => 'EXPIRES',
        'Extra' => 'EXTRA',
        'AuthIdentity.Extra' => 'EXTRA',
        'extra' => 'EXTRA',
        'authIdentity.extra' => 'EXTRA',
        'AuthIdentityTableMap::COL_EXTRA' => 'EXTRA',
        'COL_EXTRA' => 'EXTRA',
        'auth_identities.extra' => 'EXTRA',
        'ForceReset' => 'FORCE_RESET',
        'AuthIdentity.ForceReset' => 'FORCE_RESET',
        'forceReset' => 'FORCE_RESET',
        'authIdentity.forceReset' => 'FORCE_RESET',
        'AuthIdentityTableMap::COL_FORCE_RESET' => 'FORCE_RESET',
        'COL_FORCE_RESET' => 'FORCE_RESET',
        'force_reset' => 'FORCE_RESET',
        'auth_identities.force_reset' => 'FORCE_RESET',
        'LastUsedAt' => 'LAST_USED_AT',
        'AuthIdentity.LastUsedAt' => 'LAST_USED_AT',
        'lastUsedAt' => 'LAST_USED_AT',
        'authIdentity.lastUsedAt' => 'LAST_USED_AT',
        'AuthIdentityTableMap::COL_LAST_USED_AT' => 'LAST_USED_AT',
        'COL_LAST_USED_AT' => 'LAST_USED_AT',
        'last_used_at' => 'LAST_USED_AT',
        'auth_identities.last_used_at' => 'LAST_USED_AT',
        'CreatedAt' => 'CREATED_AT',
        'AuthIdentity.CreatedAt' => 'CREATED_AT',
        'createdAt' => 'CREATED_AT',
        'authIdentity.createdAt' => 'CREATED_AT',
        'AuthIdentityTableMap::COL_CREATED_AT' => 'CREATED_AT',
        'COL_CREATED_AT' => 'CREATED_AT',
        'created_at' => 'CREATED_AT',
        'auth_identities.created_at' => 'CREATED_AT',
        'UpdatedAt' => 'UPDATED_AT',
        'AuthIdentity.UpdatedAt' => 'UPDATED_AT',
        'updatedAt' => 'UPDATED_AT',
        'authIdentity.updatedAt' => 'UPDATED_AT',
        'AuthIdentityTableMap::COL_UPDATED_AT' => 'UPDATED_AT',
        'COL_UPDATED_AT' => 'UPDATED_AT',
        'updated_at' => 'UPDATED_AT',
        'auth_identities.updated_at' => 'UPDATED_AT',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('auth_identities');
        $this->setPhpName('AuthIdentity');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\AuthIdentity');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('auth_identities_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'users', 'id', true, null, null);
        $this->addColumn('type', 'Type', 'VARCHAR', true, 255, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('secret', 'Secret', 'VARCHAR', true, 255, null);
        $this->addColumn('secret2', 'Secret2', 'VARCHAR', false, 255, null);
        $this->addColumn('expires', 'Expires', 'TIMESTAMP', false, null, null);
        $this->addColumn('extra', 'Extra', 'LONGVARCHAR', false, null, null);
        $this->addColumn('force_reset', 'ForceReset', 'TINYINT', false, null, 0);
        $this->addColumn('last_used_at', 'LastUsedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
        $this->addRelation('User', '\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    }

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array<string, array> Associative array (name => parameters) of behaviors
     */
    public function getBehaviors(): array
    {
        return [
            'timestampable' => ['create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false'],
        ];
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? AuthIdentityTableMap::CLASS_DEFAULT : AuthIdentityTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (AuthIdentity object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = AuthIdentityTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AuthIdentityTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AuthIdentityTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AuthIdentityTableMap::OM_CLASS;
            /** @var AuthIdentity $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AuthIdentityTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AuthIdentityTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AuthIdentityTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AuthIdentity $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AuthIdentityTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AuthIdentityTableMap::COL_ID);
            $criteria->addSelectColumn(AuthIdentityTableMap::COL_USER_ID);
            $criteria->addSelectColumn(AuthIdentityTableMap::COL_TYPE);
            $criteria->addSelectColumn(AuthIdentityTableMap::COL_NAME);
            $criteria->addSelectColumn(AuthIdentityTableMap::COL_SECRET);
            $criteria->addSelectColumn(AuthIdentityTableMap::COL_SECRET2);
            $criteria->addSelectColumn(AuthIdentityTableMap::COL_EXPIRES);
            $criteria->addSelectColumn(AuthIdentityTableMap::COL_EXTRA);
            $criteria->addSelectColumn(AuthIdentityTableMap::COL_FORCE_RESET);
            $criteria->addSelectColumn(AuthIdentityTableMap::COL_LAST_USED_AT);
            $criteria->addSelectColumn(AuthIdentityTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(AuthIdentityTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.secret');
            $criteria->addSelectColumn($alias . '.secret2');
            $criteria->addSelectColumn($alias . '.expires');
            $criteria->addSelectColumn($alias . '.extra');
            $criteria->addSelectColumn($alias . '.force_reset');
            $criteria->addSelectColumn($alias . '.last_used_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(AuthIdentityTableMap::COL_ID);
            $criteria->removeSelectColumn(AuthIdentityTableMap::COL_USER_ID);
            $criteria->removeSelectColumn(AuthIdentityTableMap::COL_TYPE);
            $criteria->removeSelectColumn(AuthIdentityTableMap::COL_NAME);
            $criteria->removeSelectColumn(AuthIdentityTableMap::COL_SECRET);
            $criteria->removeSelectColumn(AuthIdentityTableMap::COL_SECRET2);
            $criteria->removeSelectColumn(AuthIdentityTableMap::COL_EXPIRES);
            $criteria->removeSelectColumn(AuthIdentityTableMap::COL_EXTRA);
            $criteria->removeSelectColumn(AuthIdentityTableMap::COL_FORCE_RESET);
            $criteria->removeSelectColumn(AuthIdentityTableMap::COL_LAST_USED_AT);
            $criteria->removeSelectColumn(AuthIdentityTableMap::COL_CREATED_AT);
            $criteria->removeSelectColumn(AuthIdentityTableMap::COL_UPDATED_AT);
        } else {
            $criteria->removeSelectColumn($alias . '.id');
            $criteria->removeSelectColumn($alias . '.user_id');
            $criteria->removeSelectColumn($alias . '.type');
            $criteria->removeSelectColumn($alias . '.name');
            $criteria->removeSelectColumn($alias . '.secret');
            $criteria->removeSelectColumn($alias . '.secret2');
            $criteria->removeSelectColumn($alias . '.expires');
            $criteria->removeSelectColumn($alias . '.extra');
            $criteria->removeSelectColumn($alias . '.force_reset');
            $criteria->removeSelectColumn($alias . '.last_used_at');
            $criteria->removeSelectColumn($alias . '.created_at');
            $criteria->removeSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(AuthIdentityTableMap::DATABASE_NAME)->getTable(AuthIdentityTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a AuthIdentity or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or AuthIdentity object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AuthIdentityTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AuthIdentity) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AuthIdentityTableMap::DATABASE_NAME);
            $criteria->add(AuthIdentityTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = AuthIdentityQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AuthIdentityTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AuthIdentityTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the auth_identities table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return AuthIdentityQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AuthIdentity or Criteria object.
     *
     * @param mixed $criteria Criteria or AuthIdentity object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AuthIdentityTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AuthIdentity object
        }

        if ($criteria->containsKey(AuthIdentityTableMap::COL_ID) && $criteria->keyContainsValue(AuthIdentityTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.AuthIdentityTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = AuthIdentityQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
