<?php

namespace Map;

use \DewormingHistory;
use \DewormingHistoryQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'deworming_history' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class DewormingHistoryTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.DewormingHistoryTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'HealtchareForChange';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'deworming_history';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\DewormingHistory';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'DewormingHistory';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 6;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 6;

    /**
     * the column name for the uuid field
     */
    public const COL_UUID = 'deworming_history.uuid';

    /**
     * the column name for the timestamp field
     */
    public const COL_TIMESTAMP = 'deworming_history.timestamp';

    /**
     * the column name for the patient_uuid field
     */
    public const COL_PATIENT_UUID = 'deworming_history.patient_uuid';

    /**
     * the column name for the user_id field
     */
    public const COL_USER_ID = 'deworming_history.user_id';

    /**
     * the column name for the medication_id field
     */
    public const COL_MEDICATION_ID = 'deworming_history.medication_id';

    /**
     * the column name for the comment field
     */
    public const COL_COMMENT = 'deworming_history.comment';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Uuid', 'Timestamp', 'PatientUuid', 'UserId', 'MedicationId', 'Comment', ],
        self::TYPE_CAMELNAME     => ['uuid', 'timestamp', 'patientUuid', 'userId', 'medicationId', 'comment', ],
        self::TYPE_COLNAME       => [DewormingHistoryTableMap::COL_UUID, DewormingHistoryTableMap::COL_TIMESTAMP, DewormingHistoryTableMap::COL_PATIENT_UUID, DewormingHistoryTableMap::COL_USER_ID, DewormingHistoryTableMap::COL_MEDICATION_ID, DewormingHistoryTableMap::COL_COMMENT, ],
        self::TYPE_FIELDNAME     => ['uuid', 'timestamp', 'patient_uuid', 'user_id', 'medication_id', 'comment', ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Uuid' => 0, 'Timestamp' => 1, 'PatientUuid' => 2, 'UserId' => 3, 'MedicationId' => 4, 'Comment' => 5, ],
        self::TYPE_CAMELNAME     => ['uuid' => 0, 'timestamp' => 1, 'patientUuid' => 2, 'userId' => 3, 'medicationId' => 4, 'comment' => 5, ],
        self::TYPE_COLNAME       => [DewormingHistoryTableMap::COL_UUID => 0, DewormingHistoryTableMap::COL_TIMESTAMP => 1, DewormingHistoryTableMap::COL_PATIENT_UUID => 2, DewormingHistoryTableMap::COL_USER_ID => 3, DewormingHistoryTableMap::COL_MEDICATION_ID => 4, DewormingHistoryTableMap::COL_COMMENT => 5, ],
        self::TYPE_FIELDNAME     => ['uuid' => 0, 'timestamp' => 1, 'patient_uuid' => 2, 'user_id' => 3, 'medication_id' => 4, 'comment' => 5, ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Uuid' => 'UUID',
        'DewormingHistory.Uuid' => 'UUID',
        'uuid' => 'UUID',
        'dewormingHistory.uuid' => 'UUID',
        'DewormingHistoryTableMap::COL_UUID' => 'UUID',
        'COL_UUID' => 'UUID',
        'deworming_history.uuid' => 'UUID',
        'Timestamp' => 'TIMESTAMP',
        'DewormingHistory.Timestamp' => 'TIMESTAMP',
        'timestamp' => 'TIMESTAMP',
        'dewormingHistory.timestamp' => 'TIMESTAMP',
        'DewormingHistoryTableMap::COL_TIMESTAMP' => 'TIMESTAMP',
        'COL_TIMESTAMP' => 'TIMESTAMP',
        'deworming_history.timestamp' => 'TIMESTAMP',
        'PatientUuid' => 'PATIENT_UUID',
        'DewormingHistory.PatientUuid' => 'PATIENT_UUID',
        'patientUuid' => 'PATIENT_UUID',
        'dewormingHistory.patientUuid' => 'PATIENT_UUID',
        'DewormingHistoryTableMap::COL_PATIENT_UUID' => 'PATIENT_UUID',
        'COL_PATIENT_UUID' => 'PATIENT_UUID',
        'patient_uuid' => 'PATIENT_UUID',
        'deworming_history.patient_uuid' => 'PATIENT_UUID',
        'UserId' => 'USER_ID',
        'DewormingHistory.UserId' => 'USER_ID',
        'userId' => 'USER_ID',
        'dewormingHistory.userId' => 'USER_ID',
        'DewormingHistoryTableMap::COL_USER_ID' => 'USER_ID',
        'COL_USER_ID' => 'USER_ID',
        'user_id' => 'USER_ID',
        'deworming_history.user_id' => 'USER_ID',
        'MedicationId' => 'MEDICATION_ID',
        'DewormingHistory.MedicationId' => 'MEDICATION_ID',
        'medicationId' => 'MEDICATION_ID',
        'dewormingHistory.medicationId' => 'MEDICATION_ID',
        'DewormingHistoryTableMap::COL_MEDICATION_ID' => 'MEDICATION_ID',
        'COL_MEDICATION_ID' => 'MEDICATION_ID',
        'medication_id' => 'MEDICATION_ID',
        'deworming_history.medication_id' => 'MEDICATION_ID',
        'Comment' => 'COMMENT',
        'DewormingHistory.Comment' => 'COMMENT',
        'comment' => 'COMMENT',
        'dewormingHistory.comment' => 'COMMENT',
        'DewormingHistoryTableMap::COL_COMMENT' => 'COMMENT',
        'COL_COMMENT' => 'COMMENT',
        'deworming_history.comment' => 'COMMENT',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('deworming_history');
        $this->setPhpName('DewormingHistory');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\DewormingHistory');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('uuid', 'Uuid', 'VARCHAR', true, 36, null);
        $this->addColumn('timestamp', 'Timestamp', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('patient_uuid', 'PatientUuid', 'VARCHAR', 'patient', 'uuid', true, 36, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'users', 'id', true, null, null);
        $this->addForeignKey('medication_id', 'MedicationId', 'VARCHAR', 'medication', 'uuid', false, 36, null);
        $this->addColumn('comment', 'Comment', 'VARCHAR', false, 255, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
        $this->addRelation('Patient', '\\Patient', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':patient_uuid',
    1 => ':uuid',
  ),
), null, null, null, false);
        $this->addRelation('User', '\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Medication', '\\Medication', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':medication_id',
    1 => ':uuid',
  ),
), null, null, null, false);
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? DewormingHistoryTableMap::CLASS_DEFAULT : DewormingHistoryTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (DewormingHistory object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = DewormingHistoryTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = DewormingHistoryTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + DewormingHistoryTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = DewormingHistoryTableMap::OM_CLASS;
            /** @var DewormingHistory $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            DewormingHistoryTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = DewormingHistoryTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = DewormingHistoryTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var DewormingHistory $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                DewormingHistoryTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(DewormingHistoryTableMap::COL_UUID);
            $criteria->addSelectColumn(DewormingHistoryTableMap::COL_TIMESTAMP);
            $criteria->addSelectColumn(DewormingHistoryTableMap::COL_PATIENT_UUID);
            $criteria->addSelectColumn(DewormingHistoryTableMap::COL_USER_ID);
            $criteria->addSelectColumn(DewormingHistoryTableMap::COL_MEDICATION_ID);
            $criteria->addSelectColumn(DewormingHistoryTableMap::COL_COMMENT);
        } else {
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.timestamp');
            $criteria->addSelectColumn($alias . '.patient_uuid');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.medication_id');
            $criteria->addSelectColumn($alias . '.comment');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(DewormingHistoryTableMap::COL_UUID);
            $criteria->removeSelectColumn(DewormingHistoryTableMap::COL_TIMESTAMP);
            $criteria->removeSelectColumn(DewormingHistoryTableMap::COL_PATIENT_UUID);
            $criteria->removeSelectColumn(DewormingHistoryTableMap::COL_USER_ID);
            $criteria->removeSelectColumn(DewormingHistoryTableMap::COL_MEDICATION_ID);
            $criteria->removeSelectColumn(DewormingHistoryTableMap::COL_COMMENT);
        } else {
            $criteria->removeSelectColumn($alias . '.uuid');
            $criteria->removeSelectColumn($alias . '.timestamp');
            $criteria->removeSelectColumn($alias . '.patient_uuid');
            $criteria->removeSelectColumn($alias . '.user_id');
            $criteria->removeSelectColumn($alias . '.medication_id');
            $criteria->removeSelectColumn($alias . '.comment');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(DewormingHistoryTableMap::DATABASE_NAME)->getTable(DewormingHistoryTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a DewormingHistory or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or DewormingHistory object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DewormingHistoryTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \DewormingHistory) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(DewormingHistoryTableMap::DATABASE_NAME);
            $criteria->add(DewormingHistoryTableMap::COL_UUID, (array) $values, Criteria::IN);
        }

        $query = DewormingHistoryQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            DewormingHistoryTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                DewormingHistoryTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the deworming_history table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return DewormingHistoryQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a DewormingHistory or Criteria object.
     *
     * @param mixed $criteria Criteria or DewormingHistory object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DewormingHistoryTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from DewormingHistory object
        }


        // Set the correct dbName
        $query = DewormingHistoryQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
