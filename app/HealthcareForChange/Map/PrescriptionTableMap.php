<?php

namespace Map;

use \Prescription;
use \PrescriptionQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'prescription' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class PrescriptionTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.PrescriptionTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'HealtchareForChange';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'prescription';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\Prescription';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'Prescription';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 18;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 18;

    /**
     * the column name for the uuid field
     */
    public const COL_UUID = 'prescription.uuid';

    /**
     * the column name for the medication_uuid field
     */
    public const COL_MEDICATION_UUID = 'prescription.medication_uuid';

    /**
     * the column name for the patient_uuid field
     */
    public const COL_PATIENT_UUID = 'prescription.patient_uuid';

    /**
     * the column name for the dose_quantity field
     */
    public const COL_DOSE_QUANTITY = 'prescription.dose_quantity';

    /**
     * the column name for the dose_morning field
     */
    public const COL_DOSE_MORNING = 'prescription.dose_morning';

    /**
     * the column name for the dose_noon field
     */
    public const COL_DOSE_NOON = 'prescription.dose_noon';

    /**
     * the column name for the dose_evening field
     */
    public const COL_DOSE_EVENING = 'prescription.dose_evening';

    /**
     * the column name for the dose_night field
     */
    public const COL_DOSE_NIGHT = 'prescription.dose_night';

    /**
     * the column name for the max_dose_period field
     */
    public const COL_MAX_DOSE_PERIOD = 'prescription.max_dose_period';

    /**
     * the column name for the patient_instruction field
     */
    public const COL_PATIENT_INSTRUCTION = 'prescription.patient_instruction';

    /**
     * the column name for the medication_administration_method_id field
     */
    public const COL_MEDICATION_ADMINISTRATION_METHOD_ID = 'prescription.medication_administration_method_id';

    /**
     * the column name for the dose_start field
     */
    public const COL_DOSE_START = 'prescription.dose_start';

    /**
     * the column name for the dose_end field
     */
    public const COL_DOSE_END = 'prescription.dose_end';

    /**
     * the column name for the interval field
     */
    public const COL_INTERVAL = 'prescription.interval';

    /**
     * the column name for the user_id field
     */
    public const COL_USER_ID = 'prescription.user_id';

    /**
     * the column name for the comment field
     */
    public const COL_COMMENT = 'prescription.comment';

    /**
     * the column name for the created_at field
     */
    public const COL_CREATED_AT = 'prescription.created_at';

    /**
     * the column name for the updated_at field
     */
    public const COL_UPDATED_AT = 'prescription.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Uuid', 'MedicationUuid', 'PatientUuid', 'DoseQuantity', 'DoseMorning', 'DoseNoon', 'DoseEvening', 'DoseNight', 'MaxDosePeriod', 'PatientInstruction', 'MedicationAdministrationMethodId', 'DoseStart', 'DoseEnd', 'Interval', 'UserId', 'Comment', 'CreatedAt', 'UpdatedAt', ],
        self::TYPE_CAMELNAME     => ['uuid', 'medicationUuid', 'patientUuid', 'doseQuantity', 'doseMorning', 'doseNoon', 'doseEvening', 'doseNight', 'maxDosePeriod', 'patientInstruction', 'medicationAdministrationMethodId', 'doseStart', 'doseEnd', 'interval', 'userId', 'comment', 'createdAt', 'updatedAt', ],
        self::TYPE_COLNAME       => [PrescriptionTableMap::COL_UUID, PrescriptionTableMap::COL_MEDICATION_UUID, PrescriptionTableMap::COL_PATIENT_UUID, PrescriptionTableMap::COL_DOSE_QUANTITY, PrescriptionTableMap::COL_DOSE_MORNING, PrescriptionTableMap::COL_DOSE_NOON, PrescriptionTableMap::COL_DOSE_EVENING, PrescriptionTableMap::COL_DOSE_NIGHT, PrescriptionTableMap::COL_MAX_DOSE_PERIOD, PrescriptionTableMap::COL_PATIENT_INSTRUCTION, PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID, PrescriptionTableMap::COL_DOSE_START, PrescriptionTableMap::COL_DOSE_END, PrescriptionTableMap::COL_INTERVAL, PrescriptionTableMap::COL_USER_ID, PrescriptionTableMap::COL_COMMENT, PrescriptionTableMap::COL_CREATED_AT, PrescriptionTableMap::COL_UPDATED_AT, ],
        self::TYPE_FIELDNAME     => ['uuid', 'medication_uuid', 'patient_uuid', 'dose_quantity', 'dose_morning', 'dose_noon', 'dose_evening', 'dose_night', 'max_dose_period', 'patient_instruction', 'medication_administration_method_id', 'dose_start', 'dose_end', 'interval', 'user_id', 'comment', 'created_at', 'updated_at', ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Uuid' => 0, 'MedicationUuid' => 1, 'PatientUuid' => 2, 'DoseQuantity' => 3, 'DoseMorning' => 4, 'DoseNoon' => 5, 'DoseEvening' => 6, 'DoseNight' => 7, 'MaxDosePeriod' => 8, 'PatientInstruction' => 9, 'MedicationAdministrationMethodId' => 10, 'DoseStart' => 11, 'DoseEnd' => 12, 'Interval' => 13, 'UserId' => 14, 'Comment' => 15, 'CreatedAt' => 16, 'UpdatedAt' => 17, ],
        self::TYPE_CAMELNAME     => ['uuid' => 0, 'medicationUuid' => 1, 'patientUuid' => 2, 'doseQuantity' => 3, 'doseMorning' => 4, 'doseNoon' => 5, 'doseEvening' => 6, 'doseNight' => 7, 'maxDosePeriod' => 8, 'patientInstruction' => 9, 'medicationAdministrationMethodId' => 10, 'doseStart' => 11, 'doseEnd' => 12, 'interval' => 13, 'userId' => 14, 'comment' => 15, 'createdAt' => 16, 'updatedAt' => 17, ],
        self::TYPE_COLNAME       => [PrescriptionTableMap::COL_UUID => 0, PrescriptionTableMap::COL_MEDICATION_UUID => 1, PrescriptionTableMap::COL_PATIENT_UUID => 2, PrescriptionTableMap::COL_DOSE_QUANTITY => 3, PrescriptionTableMap::COL_DOSE_MORNING => 4, PrescriptionTableMap::COL_DOSE_NOON => 5, PrescriptionTableMap::COL_DOSE_EVENING => 6, PrescriptionTableMap::COL_DOSE_NIGHT => 7, PrescriptionTableMap::COL_MAX_DOSE_PERIOD => 8, PrescriptionTableMap::COL_PATIENT_INSTRUCTION => 9, PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID => 10, PrescriptionTableMap::COL_DOSE_START => 11, PrescriptionTableMap::COL_DOSE_END => 12, PrescriptionTableMap::COL_INTERVAL => 13, PrescriptionTableMap::COL_USER_ID => 14, PrescriptionTableMap::COL_COMMENT => 15, PrescriptionTableMap::COL_CREATED_AT => 16, PrescriptionTableMap::COL_UPDATED_AT => 17, ],
        self::TYPE_FIELDNAME     => ['uuid' => 0, 'medication_uuid' => 1, 'patient_uuid' => 2, 'dose_quantity' => 3, 'dose_morning' => 4, 'dose_noon' => 5, 'dose_evening' => 6, 'dose_night' => 7, 'max_dose_period' => 8, 'patient_instruction' => 9, 'medication_administration_method_id' => 10, 'dose_start' => 11, 'dose_end' => 12, 'interval' => 13, 'user_id' => 14, 'comment' => 15, 'created_at' => 16, 'updated_at' => 17, ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Uuid' => 'UUID',
        'Prescription.Uuid' => 'UUID',
        'uuid' => 'UUID',
        'prescription.uuid' => 'UUID',
        'PrescriptionTableMap::COL_UUID' => 'UUID',
        'COL_UUID' => 'UUID',
        'MedicationUuid' => 'MEDICATION_UUID',
        'Prescription.MedicationUuid' => 'MEDICATION_UUID',
        'medicationUuid' => 'MEDICATION_UUID',
        'prescription.medicationUuid' => 'MEDICATION_UUID',
        'PrescriptionTableMap::COL_MEDICATION_UUID' => 'MEDICATION_UUID',
        'COL_MEDICATION_UUID' => 'MEDICATION_UUID',
        'medication_uuid' => 'MEDICATION_UUID',
        'prescription.medication_uuid' => 'MEDICATION_UUID',
        'PatientUuid' => 'PATIENT_UUID',
        'Prescription.PatientUuid' => 'PATIENT_UUID',
        'patientUuid' => 'PATIENT_UUID',
        'prescription.patientUuid' => 'PATIENT_UUID',
        'PrescriptionTableMap::COL_PATIENT_UUID' => 'PATIENT_UUID',
        'COL_PATIENT_UUID' => 'PATIENT_UUID',
        'patient_uuid' => 'PATIENT_UUID',
        'prescription.patient_uuid' => 'PATIENT_UUID',
        'DoseQuantity' => 'DOSE_QUANTITY',
        'Prescription.DoseQuantity' => 'DOSE_QUANTITY',
        'doseQuantity' => 'DOSE_QUANTITY',
        'prescription.doseQuantity' => 'DOSE_QUANTITY',
        'PrescriptionTableMap::COL_DOSE_QUANTITY' => 'DOSE_QUANTITY',
        'COL_DOSE_QUANTITY' => 'DOSE_QUANTITY',
        'dose_quantity' => 'DOSE_QUANTITY',
        'prescription.dose_quantity' => 'DOSE_QUANTITY',
        'DoseMorning' => 'DOSE_MORNING',
        'Prescription.DoseMorning' => 'DOSE_MORNING',
        'doseMorning' => 'DOSE_MORNING',
        'prescription.doseMorning' => 'DOSE_MORNING',
        'PrescriptionTableMap::COL_DOSE_MORNING' => 'DOSE_MORNING',
        'COL_DOSE_MORNING' => 'DOSE_MORNING',
        'dose_morning' => 'DOSE_MORNING',
        'prescription.dose_morning' => 'DOSE_MORNING',
        'DoseNoon' => 'DOSE_NOON',
        'Prescription.DoseNoon' => 'DOSE_NOON',
        'doseNoon' => 'DOSE_NOON',
        'prescription.doseNoon' => 'DOSE_NOON',
        'PrescriptionTableMap::COL_DOSE_NOON' => 'DOSE_NOON',
        'COL_DOSE_NOON' => 'DOSE_NOON',
        'dose_noon' => 'DOSE_NOON',
        'prescription.dose_noon' => 'DOSE_NOON',
        'DoseEvening' => 'DOSE_EVENING',
        'Prescription.DoseEvening' => 'DOSE_EVENING',
        'doseEvening' => 'DOSE_EVENING',
        'prescription.doseEvening' => 'DOSE_EVENING',
        'PrescriptionTableMap::COL_DOSE_EVENING' => 'DOSE_EVENING',
        'COL_DOSE_EVENING' => 'DOSE_EVENING',
        'dose_evening' => 'DOSE_EVENING',
        'prescription.dose_evening' => 'DOSE_EVENING',
        'DoseNight' => 'DOSE_NIGHT',
        'Prescription.DoseNight' => 'DOSE_NIGHT',
        'doseNight' => 'DOSE_NIGHT',
        'prescription.doseNight' => 'DOSE_NIGHT',
        'PrescriptionTableMap::COL_DOSE_NIGHT' => 'DOSE_NIGHT',
        'COL_DOSE_NIGHT' => 'DOSE_NIGHT',
        'dose_night' => 'DOSE_NIGHT',
        'prescription.dose_night' => 'DOSE_NIGHT',
        'MaxDosePeriod' => 'MAX_DOSE_PERIOD',
        'Prescription.MaxDosePeriod' => 'MAX_DOSE_PERIOD',
        'maxDosePeriod' => 'MAX_DOSE_PERIOD',
        'prescription.maxDosePeriod' => 'MAX_DOSE_PERIOD',
        'PrescriptionTableMap::COL_MAX_DOSE_PERIOD' => 'MAX_DOSE_PERIOD',
        'COL_MAX_DOSE_PERIOD' => 'MAX_DOSE_PERIOD',
        'max_dose_period' => 'MAX_DOSE_PERIOD',
        'prescription.max_dose_period' => 'MAX_DOSE_PERIOD',
        'PatientInstruction' => 'PATIENT_INSTRUCTION',
        'Prescription.PatientInstruction' => 'PATIENT_INSTRUCTION',
        'patientInstruction' => 'PATIENT_INSTRUCTION',
        'prescription.patientInstruction' => 'PATIENT_INSTRUCTION',
        'PrescriptionTableMap::COL_PATIENT_INSTRUCTION' => 'PATIENT_INSTRUCTION',
        'COL_PATIENT_INSTRUCTION' => 'PATIENT_INSTRUCTION',
        'patient_instruction' => 'PATIENT_INSTRUCTION',
        'prescription.patient_instruction' => 'PATIENT_INSTRUCTION',
        'MedicationAdministrationMethodId' => 'MEDICATION_ADMINISTRATION_METHOD_ID',
        'Prescription.MedicationAdministrationMethodId' => 'MEDICATION_ADMINISTRATION_METHOD_ID',
        'medicationAdministrationMethodId' => 'MEDICATION_ADMINISTRATION_METHOD_ID',
        'prescription.medicationAdministrationMethodId' => 'MEDICATION_ADMINISTRATION_METHOD_ID',
        'PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID' => 'MEDICATION_ADMINISTRATION_METHOD_ID',
        'COL_MEDICATION_ADMINISTRATION_METHOD_ID' => 'MEDICATION_ADMINISTRATION_METHOD_ID',
        'medication_administration_method_id' => 'MEDICATION_ADMINISTRATION_METHOD_ID',
        'prescription.medication_administration_method_id' => 'MEDICATION_ADMINISTRATION_METHOD_ID',
        'DoseStart' => 'DOSE_START',
        'Prescription.DoseStart' => 'DOSE_START',
        'doseStart' => 'DOSE_START',
        'prescription.doseStart' => 'DOSE_START',
        'PrescriptionTableMap::COL_DOSE_START' => 'DOSE_START',
        'COL_DOSE_START' => 'DOSE_START',
        'dose_start' => 'DOSE_START',
        'prescription.dose_start' => 'DOSE_START',
        'DoseEnd' => 'DOSE_END',
        'Prescription.DoseEnd' => 'DOSE_END',
        'doseEnd' => 'DOSE_END',
        'prescription.doseEnd' => 'DOSE_END',
        'PrescriptionTableMap::COL_DOSE_END' => 'DOSE_END',
        'COL_DOSE_END' => 'DOSE_END',
        'dose_end' => 'DOSE_END',
        'prescription.dose_end' => 'DOSE_END',
        'Interval' => 'INTERVAL',
        'Prescription.Interval' => 'INTERVAL',
        'interval' => 'INTERVAL',
        'prescription.interval' => 'INTERVAL',
        'PrescriptionTableMap::COL_INTERVAL' => 'INTERVAL',
        'COL_INTERVAL' => 'INTERVAL',
        'UserId' => 'USER_ID',
        'Prescription.UserId' => 'USER_ID',
        'userId' => 'USER_ID',
        'prescription.userId' => 'USER_ID',
        'PrescriptionTableMap::COL_USER_ID' => 'USER_ID',
        'COL_USER_ID' => 'USER_ID',
        'user_id' => 'USER_ID',
        'prescription.user_id' => 'USER_ID',
        'Comment' => 'COMMENT',
        'Prescription.Comment' => 'COMMENT',
        'comment' => 'COMMENT',
        'prescription.comment' => 'COMMENT',
        'PrescriptionTableMap::COL_COMMENT' => 'COMMENT',
        'COL_COMMENT' => 'COMMENT',
        'CreatedAt' => 'CREATED_AT',
        'Prescription.CreatedAt' => 'CREATED_AT',
        'createdAt' => 'CREATED_AT',
        'prescription.createdAt' => 'CREATED_AT',
        'PrescriptionTableMap::COL_CREATED_AT' => 'CREATED_AT',
        'COL_CREATED_AT' => 'CREATED_AT',
        'created_at' => 'CREATED_AT',
        'prescription.created_at' => 'CREATED_AT',
        'UpdatedAt' => 'UPDATED_AT',
        'Prescription.UpdatedAt' => 'UPDATED_AT',
        'updatedAt' => 'UPDATED_AT',
        'prescription.updatedAt' => 'UPDATED_AT',
        'PrescriptionTableMap::COL_UPDATED_AT' => 'UPDATED_AT',
        'COL_UPDATED_AT' => 'UPDATED_AT',
        'updated_at' => 'UPDATED_AT',
        'prescription.updated_at' => 'UPDATED_AT',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('prescription');
        $this->setPhpName('Prescription');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Prescription');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('uuid', 'Uuid', 'VARCHAR', true, 36, null);
        $this->addForeignKey('medication_uuid', 'MedicationUuid', 'VARCHAR', 'medication', 'uuid', true, 36, null);
        $this->addForeignKey('patient_uuid', 'PatientUuid', 'VARCHAR', 'patient', 'uuid', true, 36, null);
        $this->addColumn('dose_quantity', 'DoseQuantity', 'VARCHAR', false, 50, null);
        $this->addColumn('dose_morning', 'DoseMorning', 'BOOLEAN', false, null, false);
        $this->addColumn('dose_noon', 'DoseNoon', 'BOOLEAN', false, null, false);
        $this->addColumn('dose_evening', 'DoseEvening', 'BOOLEAN', false, null, false);
        $this->addColumn('dose_night', 'DoseNight', 'BOOLEAN', false, null, false);
        $this->addColumn('max_dose_period', 'MaxDosePeriod', 'VARCHAR', false, 50, null);
        $this->addColumn('patient_instruction', 'PatientInstruction', 'VARCHAR', false, 255, null);
        $this->addForeignKey('medication_administration_method_id', 'MedicationAdministrationMethodId', 'BIGINT', 'medication_administration_method', 'id', false, null, null);
        $this->addColumn('dose_start', 'DoseStart', 'DATE', false, null, null);
        $this->addColumn('dose_end', 'DoseEnd', 'DATE', false, null, null);
        $this->addColumn('interval', 'Interval', 'TINYINT', false, null, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'users', 'id', true, null, null);
        $this->addColumn('comment', 'Comment', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
        $this->addRelation('MedicationAdministrationMethod', '\\MedicationAdministrationMethod', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':medication_administration_method_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('User', '\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Patient', '\\Patient', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':patient_uuid',
    1 => ':uuid',
  ),
), null, null, null, false);
        $this->addRelation('Medication', '\\Medication', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':medication_uuid',
    1 => ':uuid',
  ),
), null, null, null, false);
        $this->addRelation('PrescriptionDoseInstruction', '\\PrescriptionDoseInstruction', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':prescription_uuid',
    1 => ':uuid',
  ),
), null, null, 'PrescriptionDoseInstructions', false);
        $this->addRelation('MedicationDoseInstruction', '\\MedicationDoseInstruction', RelationMap::MANY_TO_MANY, array(), null, null, 'MedicationDoseInstructions');
    }

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array<string, array> Associative array (name => parameters) of behaviors
     */
    public function getBehaviors(): array
    {
        return [
            'timestampable' => ['create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false'],
        ];
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? PrescriptionTableMap::CLASS_DEFAULT : PrescriptionTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (Prescription object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = PrescriptionTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PrescriptionTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PrescriptionTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PrescriptionTableMap::OM_CLASS;
            /** @var Prescription $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PrescriptionTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PrescriptionTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PrescriptionTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Prescription $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PrescriptionTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PrescriptionTableMap::COL_UUID);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_MEDICATION_UUID);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_PATIENT_UUID);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_DOSE_QUANTITY);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_DOSE_MORNING);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_DOSE_NOON);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_DOSE_EVENING);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_DOSE_NIGHT);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_MAX_DOSE_PERIOD);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_PATIENT_INSTRUCTION);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_DOSE_START);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_DOSE_END);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_INTERVAL);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_USER_ID);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_COMMENT);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PrescriptionTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.medication_uuid');
            $criteria->addSelectColumn($alias . '.patient_uuid');
            $criteria->addSelectColumn($alias . '.dose_quantity');
            $criteria->addSelectColumn($alias . '.dose_morning');
            $criteria->addSelectColumn($alias . '.dose_noon');
            $criteria->addSelectColumn($alias . '.dose_evening');
            $criteria->addSelectColumn($alias . '.dose_night');
            $criteria->addSelectColumn($alias . '.max_dose_period');
            $criteria->addSelectColumn($alias . '.patient_instruction');
            $criteria->addSelectColumn($alias . '.medication_administration_method_id');
            $criteria->addSelectColumn($alias . '.dose_start');
            $criteria->addSelectColumn($alias . '.dose_end');
            $criteria->addSelectColumn($alias . '.interval');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.comment');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_UUID);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_MEDICATION_UUID);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_PATIENT_UUID);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_DOSE_QUANTITY);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_DOSE_MORNING);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_DOSE_NOON);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_DOSE_EVENING);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_DOSE_NIGHT);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_MAX_DOSE_PERIOD);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_PATIENT_INSTRUCTION);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_MEDICATION_ADMINISTRATION_METHOD_ID);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_DOSE_START);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_DOSE_END);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_INTERVAL);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_USER_ID);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_COMMENT);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_CREATED_AT);
            $criteria->removeSelectColumn(PrescriptionTableMap::COL_UPDATED_AT);
        } else {
            $criteria->removeSelectColumn($alias . '.uuid');
            $criteria->removeSelectColumn($alias . '.medication_uuid');
            $criteria->removeSelectColumn($alias . '.patient_uuid');
            $criteria->removeSelectColumn($alias . '.dose_quantity');
            $criteria->removeSelectColumn($alias . '.dose_morning');
            $criteria->removeSelectColumn($alias . '.dose_noon');
            $criteria->removeSelectColumn($alias . '.dose_evening');
            $criteria->removeSelectColumn($alias . '.dose_night');
            $criteria->removeSelectColumn($alias . '.max_dose_period');
            $criteria->removeSelectColumn($alias . '.patient_instruction');
            $criteria->removeSelectColumn($alias . '.medication_administration_method_id');
            $criteria->removeSelectColumn($alias . '.dose_start');
            $criteria->removeSelectColumn($alias . '.dose_end');
            $criteria->removeSelectColumn($alias . '.interval');
            $criteria->removeSelectColumn($alias . '.user_id');
            $criteria->removeSelectColumn($alias . '.comment');
            $criteria->removeSelectColumn($alias . '.created_at');
            $criteria->removeSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(PrescriptionTableMap::DATABASE_NAME)->getTable(PrescriptionTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a Prescription or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or Prescription object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrescriptionTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Prescription) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PrescriptionTableMap::DATABASE_NAME);
            $criteria->add(PrescriptionTableMap::COL_UUID, (array) $values, Criteria::IN);
        }

        $query = PrescriptionQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PrescriptionTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PrescriptionTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the prescription table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return PrescriptionQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Prescription or Criteria object.
     *
     * @param mixed $criteria Criteria or Prescription object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrescriptionTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Prescription object
        }


        // Set the correct dbName
        $query = PrescriptionQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
