<?php

namespace Map;

use \CodeigniterSettings;
use \CodeigniterSettingsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'settings' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CodeigniterSettingsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.CodeigniterSettingsTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'HealtchareForChange';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'settings';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\CodeigniterSettings';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'CodeigniterSettings';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    public const COL_ID = 'settings.id';

    /**
     * the column name for the type field
     */
    public const COL_TYPE = 'settings.type';

    /**
     * the column name for the key field
     */
    public const COL_KEY = 'settings.key';

    /**
     * the column name for the value field
     */
    public const COL_VALUE = 'settings.value';

    /**
     * the column name for the context field
     */
    public const COL_CONTEXT = 'settings.context';

    /**
     * the column name for the class field
     */
    public const COL_CLASS = 'settings.class';

    /**
     * the column name for the created_at field
     */
    public const COL_CREATED_AT = 'settings.created_at';

    /**
     * the column name for the updated_at field
     */
    public const COL_UPDATED_AT = 'settings.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Id', 'Type', 'Key', 'Value', 'Context', 'Class', 'CreatedAt', 'UpdatedAt', ],
        self::TYPE_CAMELNAME     => ['id', 'type', 'key', 'value', 'context', 'class', 'createdAt', 'updatedAt', ],
        self::TYPE_COLNAME       => [CodeigniterSettingsTableMap::COL_ID, CodeigniterSettingsTableMap::COL_TYPE, CodeigniterSettingsTableMap::COL_KEY, CodeigniterSettingsTableMap::COL_VALUE, CodeigniterSettingsTableMap::COL_CONTEXT, CodeigniterSettingsTableMap::COL_CLASS, CodeigniterSettingsTableMap::COL_CREATED_AT, CodeigniterSettingsTableMap::COL_UPDATED_AT, ],
        self::TYPE_FIELDNAME     => ['id', 'type', 'key', 'value', 'context', 'class', 'created_at', 'updated_at', ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Id' => 0, 'Type' => 1, 'Key' => 2, 'Value' => 3, 'Context' => 4, 'Class' => 5, 'CreatedAt' => 6, 'UpdatedAt' => 7, ],
        self::TYPE_CAMELNAME     => ['id' => 0, 'type' => 1, 'key' => 2, 'value' => 3, 'context' => 4, 'class' => 5, 'createdAt' => 6, 'updatedAt' => 7, ],
        self::TYPE_COLNAME       => [CodeigniterSettingsTableMap::COL_ID => 0, CodeigniterSettingsTableMap::COL_TYPE => 1, CodeigniterSettingsTableMap::COL_KEY => 2, CodeigniterSettingsTableMap::COL_VALUE => 3, CodeigniterSettingsTableMap::COL_CONTEXT => 4, CodeigniterSettingsTableMap::COL_CLASS => 5, CodeigniterSettingsTableMap::COL_CREATED_AT => 6, CodeigniterSettingsTableMap::COL_UPDATED_AT => 7, ],
        self::TYPE_FIELDNAME     => ['id' => 0, 'type' => 1, 'key' => 2, 'value' => 3, 'context' => 4, 'class' => 5, 'created_at' => 6, 'updated_at' => 7, ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Id' => 'ID',
        'CodeigniterSettings.Id' => 'ID',
        'id' => 'ID',
        'codeigniterSettings.id' => 'ID',
        'CodeigniterSettingsTableMap::COL_ID' => 'ID',
        'COL_ID' => 'ID',
        'settings.id' => 'ID',
        'Type' => 'TYPE',
        'CodeigniterSettings.Type' => 'TYPE',
        'type' => 'TYPE',
        'codeigniterSettings.type' => 'TYPE',
        'CodeigniterSettingsTableMap::COL_TYPE' => 'TYPE',
        'COL_TYPE' => 'TYPE',
        'settings.type' => 'TYPE',
        'Key' => 'KEY',
        'CodeigniterSettings.Key' => 'KEY',
        'key' => 'KEY',
        'codeigniterSettings.key' => 'KEY',
        'CodeigniterSettingsTableMap::COL_KEY' => 'KEY',
        'COL_KEY' => 'KEY',
        'settings.key' => 'KEY',
        'Value' => 'VALUE',
        'CodeigniterSettings.Value' => 'VALUE',
        'value' => 'VALUE',
        'codeigniterSettings.value' => 'VALUE',
        'CodeigniterSettingsTableMap::COL_VALUE' => 'VALUE',
        'COL_VALUE' => 'VALUE',
        'settings.value' => 'VALUE',
        'Context' => 'CONTEXT',
        'CodeigniterSettings.Context' => 'CONTEXT',
        'context' => 'CONTEXT',
        'codeigniterSettings.context' => 'CONTEXT',
        'CodeigniterSettingsTableMap::COL_CONTEXT' => 'CONTEXT',
        'COL_CONTEXT' => 'CONTEXT',
        'settings.context' => 'CONTEXT',
        'Class' => 'CLASS',
        'CodeigniterSettings.Class' => 'CLASS',
        'class' => 'CLASS',
        'codeigniterSettings.class' => 'CLASS',
        'CodeigniterSettingsTableMap::COL_CLASS' => 'CLASS',
        'COL_CLASS' => 'CLASS',
        'settings.class' => 'CLASS',
        'CreatedAt' => 'CREATED_AT',
        'CodeigniterSettings.CreatedAt' => 'CREATED_AT',
        'createdAt' => 'CREATED_AT',
        'codeigniterSettings.createdAt' => 'CREATED_AT',
        'CodeigniterSettingsTableMap::COL_CREATED_AT' => 'CREATED_AT',
        'COL_CREATED_AT' => 'CREATED_AT',
        'created_at' => 'CREATED_AT',
        'settings.created_at' => 'CREATED_AT',
        'UpdatedAt' => 'UPDATED_AT',
        'CodeigniterSettings.UpdatedAt' => 'UPDATED_AT',
        'updatedAt' => 'UPDATED_AT',
        'codeigniterSettings.updatedAt' => 'UPDATED_AT',
        'CodeigniterSettingsTableMap::COL_UPDATED_AT' => 'UPDATED_AT',
        'COL_UPDATED_AT' => 'UPDATED_AT',
        'updated_at' => 'UPDATED_AT',
        'settings.updated_at' => 'UPDATED_AT',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('settings');
        $this->setPhpName('CodeigniterSettings');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CodeigniterSettings');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('settings_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('type', 'Type', 'VARCHAR', true, 255, null);
        $this->addColumn('key', 'Key', 'VARCHAR', true, 255, null);
        $this->addColumn('value', 'Value', 'LONGVARCHAR', false, null, null);
        $this->addColumn('context', 'Context', 'VARCHAR', false, 255, null);
        $this->addColumn('class', 'Class', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
    }

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array<string, array> Associative array (name => parameters) of behaviors
     */
    public function getBehaviors(): array
    {
        return [
            'timestampable' => ['create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false'],
        ];
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? CodeigniterSettingsTableMap::CLASS_DEFAULT : CodeigniterSettingsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (CodeigniterSettings object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = CodeigniterSettingsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CodeigniterSettingsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CodeigniterSettingsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CodeigniterSettingsTableMap::OM_CLASS;
            /** @var CodeigniterSettings $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CodeigniterSettingsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CodeigniterSettingsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CodeigniterSettingsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CodeigniterSettings $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CodeigniterSettingsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CodeigniterSettingsTableMap::COL_ID);
            $criteria->addSelectColumn(CodeigniterSettingsTableMap::COL_TYPE);
            $criteria->addSelectColumn(CodeigniterSettingsTableMap::COL_KEY);
            $criteria->addSelectColumn(CodeigniterSettingsTableMap::COL_VALUE);
            $criteria->addSelectColumn(CodeigniterSettingsTableMap::COL_CONTEXT);
            $criteria->addSelectColumn(CodeigniterSettingsTableMap::COL_CLASS);
            $criteria->addSelectColumn(CodeigniterSettingsTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(CodeigniterSettingsTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.key');
            $criteria->addSelectColumn($alias . '.value');
            $criteria->addSelectColumn($alias . '.context');
            $criteria->addSelectColumn($alias . '.class');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(CodeigniterSettingsTableMap::COL_ID);
            $criteria->removeSelectColumn(CodeigniterSettingsTableMap::COL_TYPE);
            $criteria->removeSelectColumn(CodeigniterSettingsTableMap::COL_KEY);
            $criteria->removeSelectColumn(CodeigniterSettingsTableMap::COL_VALUE);
            $criteria->removeSelectColumn(CodeigniterSettingsTableMap::COL_CONTEXT);
            $criteria->removeSelectColumn(CodeigniterSettingsTableMap::COL_CLASS);
            $criteria->removeSelectColumn(CodeigniterSettingsTableMap::COL_CREATED_AT);
            $criteria->removeSelectColumn(CodeigniterSettingsTableMap::COL_UPDATED_AT);
        } else {
            $criteria->removeSelectColumn($alias . '.id');
            $criteria->removeSelectColumn($alias . '.type');
            $criteria->removeSelectColumn($alias . '.key');
            $criteria->removeSelectColumn($alias . '.value');
            $criteria->removeSelectColumn($alias . '.context');
            $criteria->removeSelectColumn($alias . '.class');
            $criteria->removeSelectColumn($alias . '.created_at');
            $criteria->removeSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(CodeigniterSettingsTableMap::DATABASE_NAME)->getTable(CodeigniterSettingsTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a CodeigniterSettings or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or CodeigniterSettings object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CodeigniterSettingsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CodeigniterSettings) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CodeigniterSettingsTableMap::DATABASE_NAME);
            $criteria->add(CodeigniterSettingsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CodeigniterSettingsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CodeigniterSettingsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CodeigniterSettingsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the settings table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return CodeigniterSettingsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CodeigniterSettings or Criteria object.
     *
     * @param mixed $criteria Criteria or CodeigniterSettings object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CodeigniterSettingsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CodeigniterSettings object
        }

        if ($criteria->containsKey(CodeigniterSettingsTableMap::COL_ID) && $criteria->keyContainsValue(CodeigniterSettingsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CodeigniterSettingsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CodeigniterSettingsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
