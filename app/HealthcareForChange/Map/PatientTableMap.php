<?php

namespace Map;

use \Patient;
use \PatientQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'patient' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class PatientTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.PatientTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'HealtchareForChange';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'patient';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\Patient';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'Patient';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the uuid field
     */
    public const COL_UUID = 'patient.uuid';

    /**
     * the column name for the first_name field
     */
    public const COL_FIRST_NAME = 'patient.first_name';

    /**
     * the column name for the name field
     */
    public const COL_NAME = 'patient.name';

    /**
     * the column name for the gender field
     */
    public const COL_GENDER = 'patient.gender';

    /**
     * the column name for the date_of_birth field
     */
    public const COL_DATE_OF_BIRTH = 'patient.date_of_birth';

    /**
     * the column name for the date_of_death field
     */
    public const COL_DATE_OF_DEATH = 'patient.date_of_death';

    /**
     * the column name for the unclear_birthdate field
     */
    public const COL_UNCLEAR_BIRTHDATE = 'patient.unclear_birthdate';

    /**
     * the column name for the mobile_number field
     */
    public const COL_MOBILE_NUMBER = 'patient.mobile_number';

    /**
     * the column name for the tenant_uuid field
     */
    public const COL_TENANT_UUID = 'patient.tenant_uuid';

    /**
     * the column name for the religion_id field
     */
    public const COL_RELIGION_ID = 'patient.religion_id';

    /**
     * the column name for the created_at field
     */
    public const COL_CREATED_AT = 'patient.created_at';

    /**
     * the column name for the updated_at field
     */
    public const COL_UPDATED_AT = 'patient.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Uuid', 'FirstName', 'Name', 'Gender', 'DateOfBirth', 'DateOfDeath', 'UnclearBirthdate', 'MobileNumber', 'TenantUuid', 'ReligionId', 'CreatedAt', 'UpdatedAt', ],
        self::TYPE_CAMELNAME     => ['uuid', 'firstName', 'name', 'gender', 'dateOfBirth', 'dateOfDeath', 'unclearBirthdate', 'mobileNumber', 'tenantUuid', 'religionId', 'createdAt', 'updatedAt', ],
        self::TYPE_COLNAME       => [PatientTableMap::COL_UUID, PatientTableMap::COL_FIRST_NAME, PatientTableMap::COL_NAME, PatientTableMap::COL_GENDER, PatientTableMap::COL_DATE_OF_BIRTH, PatientTableMap::COL_DATE_OF_DEATH, PatientTableMap::COL_UNCLEAR_BIRTHDATE, PatientTableMap::COL_MOBILE_NUMBER, PatientTableMap::COL_TENANT_UUID, PatientTableMap::COL_RELIGION_ID, PatientTableMap::COL_CREATED_AT, PatientTableMap::COL_UPDATED_AT, ],
        self::TYPE_FIELDNAME     => ['uuid', 'first_name', 'name', 'gender', 'date_of_birth', 'date_of_death', 'unclear_birthdate', 'mobile_number', 'tenant_uuid', 'religion_id', 'created_at', 'updated_at', ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Uuid' => 0, 'FirstName' => 1, 'Name' => 2, 'Gender' => 3, 'DateOfBirth' => 4, 'DateOfDeath' => 5, 'UnclearBirthdate' => 6, 'MobileNumber' => 7, 'TenantUuid' => 8, 'ReligionId' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, ],
        self::TYPE_CAMELNAME     => ['uuid' => 0, 'firstName' => 1, 'name' => 2, 'gender' => 3, 'dateOfBirth' => 4, 'dateOfDeath' => 5, 'unclearBirthdate' => 6, 'mobileNumber' => 7, 'tenantUuid' => 8, 'religionId' => 9, 'createdAt' => 10, 'updatedAt' => 11, ],
        self::TYPE_COLNAME       => [PatientTableMap::COL_UUID => 0, PatientTableMap::COL_FIRST_NAME => 1, PatientTableMap::COL_NAME => 2, PatientTableMap::COL_GENDER => 3, PatientTableMap::COL_DATE_OF_BIRTH => 4, PatientTableMap::COL_DATE_OF_DEATH => 5, PatientTableMap::COL_UNCLEAR_BIRTHDATE => 6, PatientTableMap::COL_MOBILE_NUMBER => 7, PatientTableMap::COL_TENANT_UUID => 8, PatientTableMap::COL_RELIGION_ID => 9, PatientTableMap::COL_CREATED_AT => 10, PatientTableMap::COL_UPDATED_AT => 11, ],
        self::TYPE_FIELDNAME     => ['uuid' => 0, 'first_name' => 1, 'name' => 2, 'gender' => 3, 'date_of_birth' => 4, 'date_of_death' => 5, 'unclear_birthdate' => 6, 'mobile_number' => 7, 'tenant_uuid' => 8, 'religion_id' => 9, 'created_at' => 10, 'updated_at' => 11, ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Uuid' => 'UUID',
        'Patient.Uuid' => 'UUID',
        'uuid' => 'UUID',
        'patient.uuid' => 'UUID',
        'PatientTableMap::COL_UUID' => 'UUID',
        'COL_UUID' => 'UUID',
        'FirstName' => 'FIRST_NAME',
        'Patient.FirstName' => 'FIRST_NAME',
        'firstName' => 'FIRST_NAME',
        'patient.firstName' => 'FIRST_NAME',
        'PatientTableMap::COL_FIRST_NAME' => 'FIRST_NAME',
        'COL_FIRST_NAME' => 'FIRST_NAME',
        'first_name' => 'FIRST_NAME',
        'patient.first_name' => 'FIRST_NAME',
        'Name' => 'NAME',
        'Patient.Name' => 'NAME',
        'name' => 'NAME',
        'patient.name' => 'NAME',
        'PatientTableMap::COL_NAME' => 'NAME',
        'COL_NAME' => 'NAME',
        'Gender' => 'GENDER',
        'Patient.Gender' => 'GENDER',
        'gender' => 'GENDER',
        'patient.gender' => 'GENDER',
        'PatientTableMap::COL_GENDER' => 'GENDER',
        'COL_GENDER' => 'GENDER',
        'DateOfBirth' => 'DATE_OF_BIRTH',
        'Patient.DateOfBirth' => 'DATE_OF_BIRTH',
        'dateOfBirth' => 'DATE_OF_BIRTH',
        'patient.dateOfBirth' => 'DATE_OF_BIRTH',
        'PatientTableMap::COL_DATE_OF_BIRTH' => 'DATE_OF_BIRTH',
        'COL_DATE_OF_BIRTH' => 'DATE_OF_BIRTH',
        'date_of_birth' => 'DATE_OF_BIRTH',
        'patient.date_of_birth' => 'DATE_OF_BIRTH',
        'DateOfDeath' => 'DATE_OF_DEATH',
        'Patient.DateOfDeath' => 'DATE_OF_DEATH',
        'dateOfDeath' => 'DATE_OF_DEATH',
        'patient.dateOfDeath' => 'DATE_OF_DEATH',
        'PatientTableMap::COL_DATE_OF_DEATH' => 'DATE_OF_DEATH',
        'COL_DATE_OF_DEATH' => 'DATE_OF_DEATH',
        'date_of_death' => 'DATE_OF_DEATH',
        'patient.date_of_death' => 'DATE_OF_DEATH',
        'UnclearBirthdate' => 'UNCLEAR_BIRTHDATE',
        'Patient.UnclearBirthdate' => 'UNCLEAR_BIRTHDATE',
        'unclearBirthdate' => 'UNCLEAR_BIRTHDATE',
        'patient.unclearBirthdate' => 'UNCLEAR_BIRTHDATE',
        'PatientTableMap::COL_UNCLEAR_BIRTHDATE' => 'UNCLEAR_BIRTHDATE',
        'COL_UNCLEAR_BIRTHDATE' => 'UNCLEAR_BIRTHDATE',
        'unclear_birthdate' => 'UNCLEAR_BIRTHDATE',
        'patient.unclear_birthdate' => 'UNCLEAR_BIRTHDATE',
        'MobileNumber' => 'MOBILE_NUMBER',
        'Patient.MobileNumber' => 'MOBILE_NUMBER',
        'mobileNumber' => 'MOBILE_NUMBER',
        'patient.mobileNumber' => 'MOBILE_NUMBER',
        'PatientTableMap::COL_MOBILE_NUMBER' => 'MOBILE_NUMBER',
        'COL_MOBILE_NUMBER' => 'MOBILE_NUMBER',
        'mobile_number' => 'MOBILE_NUMBER',
        'patient.mobile_number' => 'MOBILE_NUMBER',
        'TenantUuid' => 'TENANT_UUID',
        'Patient.TenantUuid' => 'TENANT_UUID',
        'tenantUuid' => 'TENANT_UUID',
        'patient.tenantUuid' => 'TENANT_UUID',
        'PatientTableMap::COL_TENANT_UUID' => 'TENANT_UUID',
        'COL_TENANT_UUID' => 'TENANT_UUID',
        'tenant_uuid' => 'TENANT_UUID',
        'patient.tenant_uuid' => 'TENANT_UUID',
        'ReligionId' => 'RELIGION_ID',
        'Patient.ReligionId' => 'RELIGION_ID',
        'religionId' => 'RELIGION_ID',
        'patient.religionId' => 'RELIGION_ID',
        'PatientTableMap::COL_RELIGION_ID' => 'RELIGION_ID',
        'COL_RELIGION_ID' => 'RELIGION_ID',
        'religion_id' => 'RELIGION_ID',
        'patient.religion_id' => 'RELIGION_ID',
        'CreatedAt' => 'CREATED_AT',
        'Patient.CreatedAt' => 'CREATED_AT',
        'createdAt' => 'CREATED_AT',
        'patient.createdAt' => 'CREATED_AT',
        'PatientTableMap::COL_CREATED_AT' => 'CREATED_AT',
        'COL_CREATED_AT' => 'CREATED_AT',
        'created_at' => 'CREATED_AT',
        'patient.created_at' => 'CREATED_AT',
        'UpdatedAt' => 'UPDATED_AT',
        'Patient.UpdatedAt' => 'UPDATED_AT',
        'updatedAt' => 'UPDATED_AT',
        'patient.updatedAt' => 'UPDATED_AT',
        'PatientTableMap::COL_UPDATED_AT' => 'UPDATED_AT',
        'COL_UPDATED_AT' => 'UPDATED_AT',
        'updated_at' => 'UPDATED_AT',
        'patient.updated_at' => 'UPDATED_AT',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('patient');
        $this->setPhpName('Patient');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Patient');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('uuid', 'Uuid', 'VARCHAR', true, 36, null);
        $this->addColumn('first_name', 'FirstName', 'VARCHAR', true, 255, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('gender', 'Gender', 'VARCHAR', true, 1, null);
        $this->addColumn('date_of_birth', 'DateOfBirth', 'DATE', true, null, null);
        $this->addColumn('date_of_death', 'DateOfDeath', 'DATE', false, null, null);
        $this->addColumn('unclear_birthdate', 'UnclearBirthdate', 'BOOLEAN', false, null, false);
        $this->addColumn('mobile_number', 'MobileNumber', 'VARCHAR', false, 255, null);
        $this->addForeignKey('tenant_uuid', 'TenantUuid', 'VARCHAR', 'tenant', 'uuid', true, 36, null);
        $this->addForeignKey('religion_id', 'ReligionId', 'INTEGER', 'religion', 'id', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
        $this->addRelation('Tenant', '\\Tenant', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':tenant_uuid',
    1 => ':uuid',
  ),
), null, null, null, false);
        $this->addRelation('Religion', '\\Religion', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':religion_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('VaccinationHistory', '\\VaccinationHistory', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':patient_uuid',
    1 => ':uuid',
  ),
), null, null, 'VaccinationHistories', false);
        $this->addRelation('PhysicalExamination', '\\PhysicalExamination', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':patient_uuid',
    1 => ':uuid',
  ),
), null, null, 'PhysicalExaminations', false);
        $this->addRelation('DewormingHistory', '\\DewormingHistory', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':patient_uuid',
    1 => ':uuid',
  ),
), null, null, 'DewormingHistories', false);
        $this->addRelation('MedicalHistory', '\\MedicalHistory', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':patient_uuid',
    1 => ':uuid',
  ),
), null, null, 'MedicalHistories', false);
        $this->addRelation('Prescription', '\\Prescription', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':patient_uuid',
    1 => ':uuid',
  ),
), null, null, 'Prescriptions', false);
    }

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array<string, array> Associative array (name => parameters) of behaviors
     */
    public function getBehaviors(): array
    {
        return [
            'timestampable' => ['create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false'],
        ];
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? PatientTableMap::CLASS_DEFAULT : PatientTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (Patient object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = PatientTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PatientTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PatientTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PatientTableMap::OM_CLASS;
            /** @var Patient $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PatientTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PatientTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PatientTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Patient $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PatientTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PatientTableMap::COL_UUID);
            $criteria->addSelectColumn(PatientTableMap::COL_FIRST_NAME);
            $criteria->addSelectColumn(PatientTableMap::COL_NAME);
            $criteria->addSelectColumn(PatientTableMap::COL_GENDER);
            $criteria->addSelectColumn(PatientTableMap::COL_DATE_OF_BIRTH);
            $criteria->addSelectColumn(PatientTableMap::COL_DATE_OF_DEATH);
            $criteria->addSelectColumn(PatientTableMap::COL_UNCLEAR_BIRTHDATE);
            $criteria->addSelectColumn(PatientTableMap::COL_MOBILE_NUMBER);
            $criteria->addSelectColumn(PatientTableMap::COL_TENANT_UUID);
            $criteria->addSelectColumn(PatientTableMap::COL_RELIGION_ID);
            $criteria->addSelectColumn(PatientTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PatientTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.first_name');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.gender');
            $criteria->addSelectColumn($alias . '.date_of_birth');
            $criteria->addSelectColumn($alias . '.date_of_death');
            $criteria->addSelectColumn($alias . '.unclear_birthdate');
            $criteria->addSelectColumn($alias . '.mobile_number');
            $criteria->addSelectColumn($alias . '.tenant_uuid');
            $criteria->addSelectColumn($alias . '.religion_id');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(PatientTableMap::COL_UUID);
            $criteria->removeSelectColumn(PatientTableMap::COL_FIRST_NAME);
            $criteria->removeSelectColumn(PatientTableMap::COL_NAME);
            $criteria->removeSelectColumn(PatientTableMap::COL_GENDER);
            $criteria->removeSelectColumn(PatientTableMap::COL_DATE_OF_BIRTH);
            $criteria->removeSelectColumn(PatientTableMap::COL_DATE_OF_DEATH);
            $criteria->removeSelectColumn(PatientTableMap::COL_UNCLEAR_BIRTHDATE);
            $criteria->removeSelectColumn(PatientTableMap::COL_MOBILE_NUMBER);
            $criteria->removeSelectColumn(PatientTableMap::COL_TENANT_UUID);
            $criteria->removeSelectColumn(PatientTableMap::COL_RELIGION_ID);
            $criteria->removeSelectColumn(PatientTableMap::COL_CREATED_AT);
            $criteria->removeSelectColumn(PatientTableMap::COL_UPDATED_AT);
        } else {
            $criteria->removeSelectColumn($alias . '.uuid');
            $criteria->removeSelectColumn($alias . '.first_name');
            $criteria->removeSelectColumn($alias . '.name');
            $criteria->removeSelectColumn($alias . '.gender');
            $criteria->removeSelectColumn($alias . '.date_of_birth');
            $criteria->removeSelectColumn($alias . '.date_of_death');
            $criteria->removeSelectColumn($alias . '.unclear_birthdate');
            $criteria->removeSelectColumn($alias . '.mobile_number');
            $criteria->removeSelectColumn($alias . '.tenant_uuid');
            $criteria->removeSelectColumn($alias . '.religion_id');
            $criteria->removeSelectColumn($alias . '.created_at');
            $criteria->removeSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(PatientTableMap::DATABASE_NAME)->getTable(PatientTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a Patient or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or Patient object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PatientTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Patient) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PatientTableMap::DATABASE_NAME);
            $criteria->add(PatientTableMap::COL_UUID, (array) $values, Criteria::IN);
        }

        $query = PatientQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PatientTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PatientTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the patient table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return PatientQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Patient or Criteria object.
     *
     * @param mixed $criteria Criteria or Patient object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PatientTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Patient object
        }


        // Set the correct dbName
        $query = PatientQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
