<?php

namespace Map;

use \VaccinationPlan;
use \VaccinationPlanQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'vaccination_plan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class VaccinationPlanTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.VaccinationPlanTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'HealtchareForChange';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'vaccination_plan';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\VaccinationPlan';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'VaccinationPlan';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 3;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 3;

    /**
     * the column name for the id field
     */
    public const COL_ID = 'vaccination_plan.id';

    /**
     * the column name for the tenant_uuid field
     */
    public const COL_TENANT_UUID = 'vaccination_plan.tenant_uuid';

    /**
     * the column name for the vaccine_uuid field
     */
    public const COL_VACCINE_UUID = 'vaccination_plan.vaccine_uuid';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Id', 'TenantUuid', 'VaccineUuid', ],
        self::TYPE_CAMELNAME     => ['id', 'tenantUuid', 'vaccineUuid', ],
        self::TYPE_COLNAME       => [VaccinationPlanTableMap::COL_ID, VaccinationPlanTableMap::COL_TENANT_UUID, VaccinationPlanTableMap::COL_VACCINE_UUID, ],
        self::TYPE_FIELDNAME     => ['id', 'tenant_uuid', 'vaccine_uuid', ],
        self::TYPE_NUM           => [0, 1, 2, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Id' => 0, 'TenantUuid' => 1, 'VaccineUuid' => 2, ],
        self::TYPE_CAMELNAME     => ['id' => 0, 'tenantUuid' => 1, 'vaccineUuid' => 2, ],
        self::TYPE_COLNAME       => [VaccinationPlanTableMap::COL_ID => 0, VaccinationPlanTableMap::COL_TENANT_UUID => 1, VaccinationPlanTableMap::COL_VACCINE_UUID => 2, ],
        self::TYPE_FIELDNAME     => ['id' => 0, 'tenant_uuid' => 1, 'vaccine_uuid' => 2, ],
        self::TYPE_NUM           => [0, 1, 2, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Id' => 'ID',
        'VaccinationPlan.Id' => 'ID',
        'id' => 'ID',
        'vaccinationPlan.id' => 'ID',
        'VaccinationPlanTableMap::COL_ID' => 'ID',
        'COL_ID' => 'ID',
        'vaccination_plan.id' => 'ID',
        'TenantUuid' => 'TENANT_UUID',
        'VaccinationPlan.TenantUuid' => 'TENANT_UUID',
        'tenantUuid' => 'TENANT_UUID',
        'vaccinationPlan.tenantUuid' => 'TENANT_UUID',
        'VaccinationPlanTableMap::COL_TENANT_UUID' => 'TENANT_UUID',
        'COL_TENANT_UUID' => 'TENANT_UUID',
        'tenant_uuid' => 'TENANT_UUID',
        'vaccination_plan.tenant_uuid' => 'TENANT_UUID',
        'VaccineUuid' => 'VACCINE_UUID',
        'VaccinationPlan.VaccineUuid' => 'VACCINE_UUID',
        'vaccineUuid' => 'VACCINE_UUID',
        'vaccinationPlan.vaccineUuid' => 'VACCINE_UUID',
        'VaccinationPlanTableMap::COL_VACCINE_UUID' => 'VACCINE_UUID',
        'COL_VACCINE_UUID' => 'VACCINE_UUID',
        'vaccine_uuid' => 'VACCINE_UUID',
        'vaccination_plan.vaccine_uuid' => 'VACCINE_UUID',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('vaccination_plan');
        $this->setPhpName('VaccinationPlan');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\VaccinationPlan');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 36, null);
        $this->addForeignKey('tenant_uuid', 'TenantUuid', 'VARCHAR', 'tenant', 'uuid', true, 36, null);
        $this->addForeignKey('vaccine_uuid', 'VaccineUuid', 'VARCHAR', 'vaccine', 'uuid', true, 36, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
        $this->addRelation('Tenant', '\\Tenant', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':tenant_uuid',
    1 => ':uuid',
  ),
), null, null, null, false);
        $this->addRelation('Vaccine', '\\Vaccine', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vaccine_uuid',
    1 => ':uuid',
  ),
), null, null, null, false);
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? VaccinationPlanTableMap::CLASS_DEFAULT : VaccinationPlanTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (VaccinationPlan object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = VaccinationPlanTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = VaccinationPlanTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + VaccinationPlanTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = VaccinationPlanTableMap::OM_CLASS;
            /** @var VaccinationPlan $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            VaccinationPlanTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = VaccinationPlanTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = VaccinationPlanTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var VaccinationPlan $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                VaccinationPlanTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(VaccinationPlanTableMap::COL_ID);
            $criteria->addSelectColumn(VaccinationPlanTableMap::COL_TENANT_UUID);
            $criteria->addSelectColumn(VaccinationPlanTableMap::COL_VACCINE_UUID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.tenant_uuid');
            $criteria->addSelectColumn($alias . '.vaccine_uuid');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(VaccinationPlanTableMap::COL_ID);
            $criteria->removeSelectColumn(VaccinationPlanTableMap::COL_TENANT_UUID);
            $criteria->removeSelectColumn(VaccinationPlanTableMap::COL_VACCINE_UUID);
        } else {
            $criteria->removeSelectColumn($alias . '.id');
            $criteria->removeSelectColumn($alias . '.tenant_uuid');
            $criteria->removeSelectColumn($alias . '.vaccine_uuid');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(VaccinationPlanTableMap::DATABASE_NAME)->getTable(VaccinationPlanTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a VaccinationPlan or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or VaccinationPlan object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VaccinationPlanTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \VaccinationPlan) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(VaccinationPlanTableMap::DATABASE_NAME);
            $criteria->add(VaccinationPlanTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = VaccinationPlanQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            VaccinationPlanTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                VaccinationPlanTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the vaccination_plan table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return VaccinationPlanQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a VaccinationPlan or Criteria object.
     *
     * @param mixed $criteria Criteria or VaccinationPlan object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VaccinationPlanTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from VaccinationPlan object
        }


        // Set the correct dbName
        $query = VaccinationPlanQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
