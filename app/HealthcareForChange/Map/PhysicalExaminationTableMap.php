<?php

namespace Map;

use \PhysicalExamination;
use \PhysicalExaminationQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'physical_examination' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class PhysicalExaminationTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.PhysicalExaminationTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'HealtchareForChange';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'physical_examination';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\PhysicalExamination';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'PhysicalExamination';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 25;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 25;

    /**
     * the column name for the uuid field
     */
    public const COL_UUID = 'physical_examination.uuid';

    /**
     * the column name for the timestamp field
     */
    public const COL_TIMESTAMP = 'physical_examination.timestamp';

    /**
     * the column name for the blood_pressure_systolic field
     */
    public const COL_BLOOD_PRESSURE_SYSTOLIC = 'physical_examination.blood_pressure_systolic';

    /**
     * the column name for the blood_pressure_diastolic field
     */
    public const COL_BLOOD_PRESSURE_DIASTOLIC = 'physical_examination.blood_pressure_diastolic';

    /**
     * the column name for the heart_rate field
     */
    public const COL_HEART_RATE = 'physical_examination.heart_rate';

    /**
     * the column name for the weight field
     */
    public const COL_WEIGHT = 'physical_examination.weight';

    /**
     * the column name for the height field
     */
    public const COL_HEIGHT = 'physical_examination.height';

    /**
     * the column name for the muac field
     */
    public const COL_MUAC = 'physical_examination.muac';

    /**
     * the column name for the temperature field
     */
    public const COL_TEMPERATURE = 'physical_examination.temperature';

    /**
     * the column name for the skin_examination field
     */
    public const COL_SKIN_EXAMINATION = 'physical_examination.skin_examination';

    /**
     * the column name for the details_ent field
     */
    public const COL_DETAILS_ENT = 'physical_examination.details_ent';

    /**
     * the column name for the details_eyes field
     */
    public const COL_DETAILS_EYES = 'physical_examination.details_eyes';

    /**
     * the column name for the details_head field
     */
    public const COL_DETAILS_HEAD = 'physical_examination.details_head';

    /**
     * the column name for the details_muscles_bones field
     */
    public const COL_DETAILS_MUSCLES_BONES = 'physical_examination.details_muscles_bones';

    /**
     * the column name for the details_heart field
     */
    public const COL_DETAILS_HEART = 'physical_examination.details_heart';

    /**
     * the column name for the details_lung field
     */
    public const COL_DETAILS_LUNG = 'physical_examination.details_lung';

    /**
     * the column name for the details_gastrointestinal field
     */
    public const COL_DETAILS_GASTROINTESTINAL = 'physical_examination.details_gastrointestinal';

    /**
     * the column name for the details_urinary_tract field
     */
    public const COL_DETAILS_URINARY_TRACT = 'physical_examination.details_urinary_tract';

    /**
     * the column name for the details_reproductive_system field
     */
    public const COL_DETAILS_REPRODUCTIVE_SYSTEM = 'physical_examination.details_reproductive_system';

    /**
     * the column name for the details_other field
     */
    public const COL_DETAILS_OTHER = 'physical_examination.details_other';

    /**
     * the column name for the general_impression field
     */
    public const COL_GENERAL_IMPRESSION = 'physical_examination.general_impression';

    /**
     * the column name for the patient_uuid field
     */
    public const COL_PATIENT_UUID = 'physical_examination.patient_uuid';

    /**
     * the column name for the user_id field
     */
    public const COL_USER_ID = 'physical_examination.user_id';

    /**
     * the column name for the created_at field
     */
    public const COL_CREATED_AT = 'physical_examination.created_at';

    /**
     * the column name for the updated_at field
     */
    public const COL_UPDATED_AT = 'physical_examination.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Uuid', 'Timestamp', 'BloodPressureSystolic', 'BloodPressureDiastolic', 'HeartRate', 'Weight', 'Height', 'Muac', 'Temperature', 'SkinExamination', 'DetailsEnt', 'DetailsEyes', 'DetailsHead', 'DetailsMusclesBones', 'DetailsHeart', 'DetailsLung', 'DetailsGastrointestinal', 'DetailsUrinaryTract', 'DetailsReproductiveSystem', 'DetailsOther', 'GeneralImpression', 'PatientUuid', 'UserId', 'CreatedAt', 'UpdatedAt', ],
        self::TYPE_CAMELNAME     => ['uuid', 'timestamp', 'bloodPressureSystolic', 'bloodPressureDiastolic', 'heartRate', 'weight', 'height', 'muac', 'temperature', 'skinExamination', 'detailsEnt', 'detailsEyes', 'detailsHead', 'detailsMusclesBones', 'detailsHeart', 'detailsLung', 'detailsGastrointestinal', 'detailsUrinaryTract', 'detailsReproductiveSystem', 'detailsOther', 'generalImpression', 'patientUuid', 'userId', 'createdAt', 'updatedAt', ],
        self::TYPE_COLNAME       => [PhysicalExaminationTableMap::COL_UUID, PhysicalExaminationTableMap::COL_TIMESTAMP, PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_SYSTOLIC, PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_DIASTOLIC, PhysicalExaminationTableMap::COL_HEART_RATE, PhysicalExaminationTableMap::COL_WEIGHT, PhysicalExaminationTableMap::COL_HEIGHT, PhysicalExaminationTableMap::COL_MUAC, PhysicalExaminationTableMap::COL_TEMPERATURE, PhysicalExaminationTableMap::COL_SKIN_EXAMINATION, PhysicalExaminationTableMap::COL_DETAILS_ENT, PhysicalExaminationTableMap::COL_DETAILS_EYES, PhysicalExaminationTableMap::COL_DETAILS_HEAD, PhysicalExaminationTableMap::COL_DETAILS_MUSCLES_BONES, PhysicalExaminationTableMap::COL_DETAILS_HEART, PhysicalExaminationTableMap::COL_DETAILS_LUNG, PhysicalExaminationTableMap::COL_DETAILS_GASTROINTESTINAL, PhysicalExaminationTableMap::COL_DETAILS_URINARY_TRACT, PhysicalExaminationTableMap::COL_DETAILS_REPRODUCTIVE_SYSTEM, PhysicalExaminationTableMap::COL_DETAILS_OTHER, PhysicalExaminationTableMap::COL_GENERAL_IMPRESSION, PhysicalExaminationTableMap::COL_PATIENT_UUID, PhysicalExaminationTableMap::COL_USER_ID, PhysicalExaminationTableMap::COL_CREATED_AT, PhysicalExaminationTableMap::COL_UPDATED_AT, ],
        self::TYPE_FIELDNAME     => ['uuid', 'timestamp', 'blood_pressure_systolic', 'blood_pressure_diastolic', 'heart_rate', 'weight', 'height', 'muac', 'temperature', 'skin_examination', 'details_ent', 'details_eyes', 'details_head', 'details_muscles_bones', 'details_heart', 'details_lung', 'details_gastrointestinal', 'details_urinary_tract', 'details_reproductive_system', 'details_other', 'general_impression', 'patient_uuid', 'user_id', 'created_at', 'updated_at', ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Uuid' => 0, 'Timestamp' => 1, 'BloodPressureSystolic' => 2, 'BloodPressureDiastolic' => 3, 'HeartRate' => 4, 'Weight' => 5, 'Height' => 6, 'Muac' => 7, 'Temperature' => 8, 'SkinExamination' => 9, 'DetailsEnt' => 10, 'DetailsEyes' => 11, 'DetailsHead' => 12, 'DetailsMusclesBones' => 13, 'DetailsHeart' => 14, 'DetailsLung' => 15, 'DetailsGastrointestinal' => 16, 'DetailsUrinaryTract' => 17, 'DetailsReproductiveSystem' => 18, 'DetailsOther' => 19, 'GeneralImpression' => 20, 'PatientUuid' => 21, 'UserId' => 22, 'CreatedAt' => 23, 'UpdatedAt' => 24, ],
        self::TYPE_CAMELNAME     => ['uuid' => 0, 'timestamp' => 1, 'bloodPressureSystolic' => 2, 'bloodPressureDiastolic' => 3, 'heartRate' => 4, 'weight' => 5, 'height' => 6, 'muac' => 7, 'temperature' => 8, 'skinExamination' => 9, 'detailsEnt' => 10, 'detailsEyes' => 11, 'detailsHead' => 12, 'detailsMusclesBones' => 13, 'detailsHeart' => 14, 'detailsLung' => 15, 'detailsGastrointestinal' => 16, 'detailsUrinaryTract' => 17, 'detailsReproductiveSystem' => 18, 'detailsOther' => 19, 'generalImpression' => 20, 'patientUuid' => 21, 'userId' => 22, 'createdAt' => 23, 'updatedAt' => 24, ],
        self::TYPE_COLNAME       => [PhysicalExaminationTableMap::COL_UUID => 0, PhysicalExaminationTableMap::COL_TIMESTAMP => 1, PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_SYSTOLIC => 2, PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_DIASTOLIC => 3, PhysicalExaminationTableMap::COL_HEART_RATE => 4, PhysicalExaminationTableMap::COL_WEIGHT => 5, PhysicalExaminationTableMap::COL_HEIGHT => 6, PhysicalExaminationTableMap::COL_MUAC => 7, PhysicalExaminationTableMap::COL_TEMPERATURE => 8, PhysicalExaminationTableMap::COL_SKIN_EXAMINATION => 9, PhysicalExaminationTableMap::COL_DETAILS_ENT => 10, PhysicalExaminationTableMap::COL_DETAILS_EYES => 11, PhysicalExaminationTableMap::COL_DETAILS_HEAD => 12, PhysicalExaminationTableMap::COL_DETAILS_MUSCLES_BONES => 13, PhysicalExaminationTableMap::COL_DETAILS_HEART => 14, PhysicalExaminationTableMap::COL_DETAILS_LUNG => 15, PhysicalExaminationTableMap::COL_DETAILS_GASTROINTESTINAL => 16, PhysicalExaminationTableMap::COL_DETAILS_URINARY_TRACT => 17, PhysicalExaminationTableMap::COL_DETAILS_REPRODUCTIVE_SYSTEM => 18, PhysicalExaminationTableMap::COL_DETAILS_OTHER => 19, PhysicalExaminationTableMap::COL_GENERAL_IMPRESSION => 20, PhysicalExaminationTableMap::COL_PATIENT_UUID => 21, PhysicalExaminationTableMap::COL_USER_ID => 22, PhysicalExaminationTableMap::COL_CREATED_AT => 23, PhysicalExaminationTableMap::COL_UPDATED_AT => 24, ],
        self::TYPE_FIELDNAME     => ['uuid' => 0, 'timestamp' => 1, 'blood_pressure_systolic' => 2, 'blood_pressure_diastolic' => 3, 'heart_rate' => 4, 'weight' => 5, 'height' => 6, 'muac' => 7, 'temperature' => 8, 'skin_examination' => 9, 'details_ent' => 10, 'details_eyes' => 11, 'details_head' => 12, 'details_muscles_bones' => 13, 'details_heart' => 14, 'details_lung' => 15, 'details_gastrointestinal' => 16, 'details_urinary_tract' => 17, 'details_reproductive_system' => 18, 'details_other' => 19, 'general_impression' => 20, 'patient_uuid' => 21, 'user_id' => 22, 'created_at' => 23, 'updated_at' => 24, ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Uuid' => 'UUID',
        'PhysicalExamination.Uuid' => 'UUID',
        'uuid' => 'UUID',
        'physicalExamination.uuid' => 'UUID',
        'PhysicalExaminationTableMap::COL_UUID' => 'UUID',
        'COL_UUID' => 'UUID',
        'physical_examination.uuid' => 'UUID',
        'Timestamp' => 'TIMESTAMP',
        'PhysicalExamination.Timestamp' => 'TIMESTAMP',
        'timestamp' => 'TIMESTAMP',
        'physicalExamination.timestamp' => 'TIMESTAMP',
        'PhysicalExaminationTableMap::COL_TIMESTAMP' => 'TIMESTAMP',
        'COL_TIMESTAMP' => 'TIMESTAMP',
        'physical_examination.timestamp' => 'TIMESTAMP',
        'BloodPressureSystolic' => 'BLOOD_PRESSURE_SYSTOLIC',
        'PhysicalExamination.BloodPressureSystolic' => 'BLOOD_PRESSURE_SYSTOLIC',
        'bloodPressureSystolic' => 'BLOOD_PRESSURE_SYSTOLIC',
        'physicalExamination.bloodPressureSystolic' => 'BLOOD_PRESSURE_SYSTOLIC',
        'PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_SYSTOLIC' => 'BLOOD_PRESSURE_SYSTOLIC',
        'COL_BLOOD_PRESSURE_SYSTOLIC' => 'BLOOD_PRESSURE_SYSTOLIC',
        'blood_pressure_systolic' => 'BLOOD_PRESSURE_SYSTOLIC',
        'physical_examination.blood_pressure_systolic' => 'BLOOD_PRESSURE_SYSTOLIC',
        'BloodPressureDiastolic' => 'BLOOD_PRESSURE_DIASTOLIC',
        'PhysicalExamination.BloodPressureDiastolic' => 'BLOOD_PRESSURE_DIASTOLIC',
        'bloodPressureDiastolic' => 'BLOOD_PRESSURE_DIASTOLIC',
        'physicalExamination.bloodPressureDiastolic' => 'BLOOD_PRESSURE_DIASTOLIC',
        'PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_DIASTOLIC' => 'BLOOD_PRESSURE_DIASTOLIC',
        'COL_BLOOD_PRESSURE_DIASTOLIC' => 'BLOOD_PRESSURE_DIASTOLIC',
        'blood_pressure_diastolic' => 'BLOOD_PRESSURE_DIASTOLIC',
        'physical_examination.blood_pressure_diastolic' => 'BLOOD_PRESSURE_DIASTOLIC',
        'HeartRate' => 'HEART_RATE',
        'PhysicalExamination.HeartRate' => 'HEART_RATE',
        'heartRate' => 'HEART_RATE',
        'physicalExamination.heartRate' => 'HEART_RATE',
        'PhysicalExaminationTableMap::COL_HEART_RATE' => 'HEART_RATE',
        'COL_HEART_RATE' => 'HEART_RATE',
        'heart_rate' => 'HEART_RATE',
        'physical_examination.heart_rate' => 'HEART_RATE',
        'Weight' => 'WEIGHT',
        'PhysicalExamination.Weight' => 'WEIGHT',
        'weight' => 'WEIGHT',
        'physicalExamination.weight' => 'WEIGHT',
        'PhysicalExaminationTableMap::COL_WEIGHT' => 'WEIGHT',
        'COL_WEIGHT' => 'WEIGHT',
        'physical_examination.weight' => 'WEIGHT',
        'Height' => 'HEIGHT',
        'PhysicalExamination.Height' => 'HEIGHT',
        'height' => 'HEIGHT',
        'physicalExamination.height' => 'HEIGHT',
        'PhysicalExaminationTableMap::COL_HEIGHT' => 'HEIGHT',
        'COL_HEIGHT' => 'HEIGHT',
        'physical_examination.height' => 'HEIGHT',
        'Muac' => 'MUAC',
        'PhysicalExamination.Muac' => 'MUAC',
        'muac' => 'MUAC',
        'physicalExamination.muac' => 'MUAC',
        'PhysicalExaminationTableMap::COL_MUAC' => 'MUAC',
        'COL_MUAC' => 'MUAC',
        'physical_examination.muac' => 'MUAC',
        'Temperature' => 'TEMPERATURE',
        'PhysicalExamination.Temperature' => 'TEMPERATURE',
        'temperature' => 'TEMPERATURE',
        'physicalExamination.temperature' => 'TEMPERATURE',
        'PhysicalExaminationTableMap::COL_TEMPERATURE' => 'TEMPERATURE',
        'COL_TEMPERATURE' => 'TEMPERATURE',
        'physical_examination.temperature' => 'TEMPERATURE',
        'SkinExamination' => 'SKIN_EXAMINATION',
        'PhysicalExamination.SkinExamination' => 'SKIN_EXAMINATION',
        'skinExamination' => 'SKIN_EXAMINATION',
        'physicalExamination.skinExamination' => 'SKIN_EXAMINATION',
        'PhysicalExaminationTableMap::COL_SKIN_EXAMINATION' => 'SKIN_EXAMINATION',
        'COL_SKIN_EXAMINATION' => 'SKIN_EXAMINATION',
        'skin_examination' => 'SKIN_EXAMINATION',
        'physical_examination.skin_examination' => 'SKIN_EXAMINATION',
        'DetailsEnt' => 'DETAILS_ENT',
        'PhysicalExamination.DetailsEnt' => 'DETAILS_ENT',
        'detailsEnt' => 'DETAILS_ENT',
        'physicalExamination.detailsEnt' => 'DETAILS_ENT',
        'PhysicalExaminationTableMap::COL_DETAILS_ENT' => 'DETAILS_ENT',
        'COL_DETAILS_ENT' => 'DETAILS_ENT',
        'details_ent' => 'DETAILS_ENT',
        'physical_examination.details_ent' => 'DETAILS_ENT',
        'DetailsEyes' => 'DETAILS_EYES',
        'PhysicalExamination.DetailsEyes' => 'DETAILS_EYES',
        'detailsEyes' => 'DETAILS_EYES',
        'physicalExamination.detailsEyes' => 'DETAILS_EYES',
        'PhysicalExaminationTableMap::COL_DETAILS_EYES' => 'DETAILS_EYES',
        'COL_DETAILS_EYES' => 'DETAILS_EYES',
        'details_eyes' => 'DETAILS_EYES',
        'physical_examination.details_eyes' => 'DETAILS_EYES',
        'DetailsHead' => 'DETAILS_HEAD',
        'PhysicalExamination.DetailsHead' => 'DETAILS_HEAD',
        'detailsHead' => 'DETAILS_HEAD',
        'physicalExamination.detailsHead' => 'DETAILS_HEAD',
        'PhysicalExaminationTableMap::COL_DETAILS_HEAD' => 'DETAILS_HEAD',
        'COL_DETAILS_HEAD' => 'DETAILS_HEAD',
        'details_head' => 'DETAILS_HEAD',
        'physical_examination.details_head' => 'DETAILS_HEAD',
        'DetailsMusclesBones' => 'DETAILS_MUSCLES_BONES',
        'PhysicalExamination.DetailsMusclesBones' => 'DETAILS_MUSCLES_BONES',
        'detailsMusclesBones' => 'DETAILS_MUSCLES_BONES',
        'physicalExamination.detailsMusclesBones' => 'DETAILS_MUSCLES_BONES',
        'PhysicalExaminationTableMap::COL_DETAILS_MUSCLES_BONES' => 'DETAILS_MUSCLES_BONES',
        'COL_DETAILS_MUSCLES_BONES' => 'DETAILS_MUSCLES_BONES',
        'details_muscles_bones' => 'DETAILS_MUSCLES_BONES',
        'physical_examination.details_muscles_bones' => 'DETAILS_MUSCLES_BONES',
        'DetailsHeart' => 'DETAILS_HEART',
        'PhysicalExamination.DetailsHeart' => 'DETAILS_HEART',
        'detailsHeart' => 'DETAILS_HEART',
        'physicalExamination.detailsHeart' => 'DETAILS_HEART',
        'PhysicalExaminationTableMap::COL_DETAILS_HEART' => 'DETAILS_HEART',
        'COL_DETAILS_HEART' => 'DETAILS_HEART',
        'details_heart' => 'DETAILS_HEART',
        'physical_examination.details_heart' => 'DETAILS_HEART',
        'DetailsLung' => 'DETAILS_LUNG',
        'PhysicalExamination.DetailsLung' => 'DETAILS_LUNG',
        'detailsLung' => 'DETAILS_LUNG',
        'physicalExamination.detailsLung' => 'DETAILS_LUNG',
        'PhysicalExaminationTableMap::COL_DETAILS_LUNG' => 'DETAILS_LUNG',
        'COL_DETAILS_LUNG' => 'DETAILS_LUNG',
        'details_lung' => 'DETAILS_LUNG',
        'physical_examination.details_lung' => 'DETAILS_LUNG',
        'DetailsGastrointestinal' => 'DETAILS_GASTROINTESTINAL',
        'PhysicalExamination.DetailsGastrointestinal' => 'DETAILS_GASTROINTESTINAL',
        'detailsGastrointestinal' => 'DETAILS_GASTROINTESTINAL',
        'physicalExamination.detailsGastrointestinal' => 'DETAILS_GASTROINTESTINAL',
        'PhysicalExaminationTableMap::COL_DETAILS_GASTROINTESTINAL' => 'DETAILS_GASTROINTESTINAL',
        'COL_DETAILS_GASTROINTESTINAL' => 'DETAILS_GASTROINTESTINAL',
        'details_gastrointestinal' => 'DETAILS_GASTROINTESTINAL',
        'physical_examination.details_gastrointestinal' => 'DETAILS_GASTROINTESTINAL',
        'DetailsUrinaryTract' => 'DETAILS_URINARY_TRACT',
        'PhysicalExamination.DetailsUrinaryTract' => 'DETAILS_URINARY_TRACT',
        'detailsUrinaryTract' => 'DETAILS_URINARY_TRACT',
        'physicalExamination.detailsUrinaryTract' => 'DETAILS_URINARY_TRACT',
        'PhysicalExaminationTableMap::COL_DETAILS_URINARY_TRACT' => 'DETAILS_URINARY_TRACT',
        'COL_DETAILS_URINARY_TRACT' => 'DETAILS_URINARY_TRACT',
        'details_urinary_tract' => 'DETAILS_URINARY_TRACT',
        'physical_examination.details_urinary_tract' => 'DETAILS_URINARY_TRACT',
        'DetailsReproductiveSystem' => 'DETAILS_REPRODUCTIVE_SYSTEM',
        'PhysicalExamination.DetailsReproductiveSystem' => 'DETAILS_REPRODUCTIVE_SYSTEM',
        'detailsReproductiveSystem' => 'DETAILS_REPRODUCTIVE_SYSTEM',
        'physicalExamination.detailsReproductiveSystem' => 'DETAILS_REPRODUCTIVE_SYSTEM',
        'PhysicalExaminationTableMap::COL_DETAILS_REPRODUCTIVE_SYSTEM' => 'DETAILS_REPRODUCTIVE_SYSTEM',
        'COL_DETAILS_REPRODUCTIVE_SYSTEM' => 'DETAILS_REPRODUCTIVE_SYSTEM',
        'details_reproductive_system' => 'DETAILS_REPRODUCTIVE_SYSTEM',
        'physical_examination.details_reproductive_system' => 'DETAILS_REPRODUCTIVE_SYSTEM',
        'DetailsOther' => 'DETAILS_OTHER',
        'PhysicalExamination.DetailsOther' => 'DETAILS_OTHER',
        'detailsOther' => 'DETAILS_OTHER',
        'physicalExamination.detailsOther' => 'DETAILS_OTHER',
        'PhysicalExaminationTableMap::COL_DETAILS_OTHER' => 'DETAILS_OTHER',
        'COL_DETAILS_OTHER' => 'DETAILS_OTHER',
        'details_other' => 'DETAILS_OTHER',
        'physical_examination.details_other' => 'DETAILS_OTHER',
        'GeneralImpression' => 'GENERAL_IMPRESSION',
        'PhysicalExamination.GeneralImpression' => 'GENERAL_IMPRESSION',
        'generalImpression' => 'GENERAL_IMPRESSION',
        'physicalExamination.generalImpression' => 'GENERAL_IMPRESSION',
        'PhysicalExaminationTableMap::COL_GENERAL_IMPRESSION' => 'GENERAL_IMPRESSION',
        'COL_GENERAL_IMPRESSION' => 'GENERAL_IMPRESSION',
        'general_impression' => 'GENERAL_IMPRESSION',
        'physical_examination.general_impression' => 'GENERAL_IMPRESSION',
        'PatientUuid' => 'PATIENT_UUID',
        'PhysicalExamination.PatientUuid' => 'PATIENT_UUID',
        'patientUuid' => 'PATIENT_UUID',
        'physicalExamination.patientUuid' => 'PATIENT_UUID',
        'PhysicalExaminationTableMap::COL_PATIENT_UUID' => 'PATIENT_UUID',
        'COL_PATIENT_UUID' => 'PATIENT_UUID',
        'patient_uuid' => 'PATIENT_UUID',
        'physical_examination.patient_uuid' => 'PATIENT_UUID',
        'UserId' => 'USER_ID',
        'PhysicalExamination.UserId' => 'USER_ID',
        'userId' => 'USER_ID',
        'physicalExamination.userId' => 'USER_ID',
        'PhysicalExaminationTableMap::COL_USER_ID' => 'USER_ID',
        'COL_USER_ID' => 'USER_ID',
        'user_id' => 'USER_ID',
        'physical_examination.user_id' => 'USER_ID',
        'CreatedAt' => 'CREATED_AT',
        'PhysicalExamination.CreatedAt' => 'CREATED_AT',
        'createdAt' => 'CREATED_AT',
        'physicalExamination.createdAt' => 'CREATED_AT',
        'PhysicalExaminationTableMap::COL_CREATED_AT' => 'CREATED_AT',
        'COL_CREATED_AT' => 'CREATED_AT',
        'created_at' => 'CREATED_AT',
        'physical_examination.created_at' => 'CREATED_AT',
        'UpdatedAt' => 'UPDATED_AT',
        'PhysicalExamination.UpdatedAt' => 'UPDATED_AT',
        'updatedAt' => 'UPDATED_AT',
        'physicalExamination.updatedAt' => 'UPDATED_AT',
        'PhysicalExaminationTableMap::COL_UPDATED_AT' => 'UPDATED_AT',
        'COL_UPDATED_AT' => 'UPDATED_AT',
        'updated_at' => 'UPDATED_AT',
        'physical_examination.updated_at' => 'UPDATED_AT',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('physical_examination');
        $this->setPhpName('PhysicalExamination');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\PhysicalExamination');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('uuid', 'Uuid', 'VARCHAR', true, 36, null);
        $this->addColumn('timestamp', 'Timestamp', 'TIMESTAMP', true, null, null);
        $this->addColumn('blood_pressure_systolic', 'BloodPressureSystolic', 'TINYINT', false, null, null);
        $this->addColumn('blood_pressure_diastolic', 'BloodPressureDiastolic', 'TINYINT', false, null, null);
        $this->addColumn('heart_rate', 'HeartRate', 'TINYINT', false, null, null);
        $this->addColumn('weight', 'Weight', 'DECIMAL', false, 5, null);
        $this->addColumn('height', 'Height', 'TINYINT', false, null, null);
        $this->addColumn('muac', 'Muac', 'DECIMAL', false, 5, null);
        $this->addColumn('temperature', 'Temperature', 'DECIMAL', false, 5, null);
        $this->addColumn('skin_examination', 'SkinExamination', 'LONGVARCHAR', false, null, null);
        $this->addColumn('details_ent', 'DetailsEnt', 'LONGVARCHAR', false, null, null);
        $this->addColumn('details_eyes', 'DetailsEyes', 'LONGVARCHAR', false, null, null);
        $this->addColumn('details_head', 'DetailsHead', 'LONGVARCHAR', false, null, null);
        $this->addColumn('details_muscles_bones', 'DetailsMusclesBones', 'LONGVARCHAR', false, null, null);
        $this->addColumn('details_heart', 'DetailsHeart', 'LONGVARCHAR', false, null, null);
        $this->addColumn('details_lung', 'DetailsLung', 'LONGVARCHAR', false, null, null);
        $this->addColumn('details_gastrointestinal', 'DetailsGastrointestinal', 'LONGVARCHAR', false, null, null);
        $this->addColumn('details_urinary_tract', 'DetailsUrinaryTract', 'LONGVARCHAR', false, null, null);
        $this->addColumn('details_reproductive_system', 'DetailsReproductiveSystem', 'LONGVARCHAR', false, null, null);
        $this->addColumn('details_other', 'DetailsOther', 'LONGVARCHAR', false, null, null);
        $this->addColumn('general_impression', 'GeneralImpression', 'TINYINT', true, null, null);
        $this->addForeignKey('patient_uuid', 'PatientUuid', 'VARCHAR', 'patient', 'uuid', true, 36, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'users', 'id', true, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
        $this->addRelation('Patient', '\\Patient', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':patient_uuid',
    1 => ':uuid',
  ),
), null, null, null, false);
        $this->addRelation('User', '\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, null, false);
    }

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array<string, array> Associative array (name => parameters) of behaviors
     */
    public function getBehaviors(): array
    {
        return [
            'timestampable' => ['create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false'],
        ];
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? PhysicalExaminationTableMap::CLASS_DEFAULT : PhysicalExaminationTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (PhysicalExamination object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = PhysicalExaminationTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PhysicalExaminationTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PhysicalExaminationTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PhysicalExaminationTableMap::OM_CLASS;
            /** @var PhysicalExamination $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PhysicalExaminationTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PhysicalExaminationTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PhysicalExaminationTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PhysicalExamination $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PhysicalExaminationTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_UUID);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_TIMESTAMP);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_SYSTOLIC);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_DIASTOLIC);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_HEART_RATE);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_WEIGHT);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_HEIGHT);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_MUAC);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_TEMPERATURE);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_SKIN_EXAMINATION);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_ENT);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_EYES);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_HEAD);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_MUSCLES_BONES);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_HEART);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_LUNG);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_GASTROINTESTINAL);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_URINARY_TRACT);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_REPRODUCTIVE_SYSTEM);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_OTHER);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_GENERAL_IMPRESSION);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_PATIENT_UUID);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_USER_ID);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PhysicalExaminationTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.timestamp');
            $criteria->addSelectColumn($alias . '.blood_pressure_systolic');
            $criteria->addSelectColumn($alias . '.blood_pressure_diastolic');
            $criteria->addSelectColumn($alias . '.heart_rate');
            $criteria->addSelectColumn($alias . '.weight');
            $criteria->addSelectColumn($alias . '.height');
            $criteria->addSelectColumn($alias . '.muac');
            $criteria->addSelectColumn($alias . '.temperature');
            $criteria->addSelectColumn($alias . '.skin_examination');
            $criteria->addSelectColumn($alias . '.details_ent');
            $criteria->addSelectColumn($alias . '.details_eyes');
            $criteria->addSelectColumn($alias . '.details_head');
            $criteria->addSelectColumn($alias . '.details_muscles_bones');
            $criteria->addSelectColumn($alias . '.details_heart');
            $criteria->addSelectColumn($alias . '.details_lung');
            $criteria->addSelectColumn($alias . '.details_gastrointestinal');
            $criteria->addSelectColumn($alias . '.details_urinary_tract');
            $criteria->addSelectColumn($alias . '.details_reproductive_system');
            $criteria->addSelectColumn($alias . '.details_other');
            $criteria->addSelectColumn($alias . '.general_impression');
            $criteria->addSelectColumn($alias . '.patient_uuid');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_UUID);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_TIMESTAMP);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_SYSTOLIC);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_BLOOD_PRESSURE_DIASTOLIC);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_HEART_RATE);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_WEIGHT);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_HEIGHT);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_MUAC);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_TEMPERATURE);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_SKIN_EXAMINATION);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_ENT);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_EYES);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_HEAD);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_MUSCLES_BONES);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_HEART);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_LUNG);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_GASTROINTESTINAL);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_URINARY_TRACT);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_REPRODUCTIVE_SYSTEM);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_DETAILS_OTHER);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_GENERAL_IMPRESSION);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_PATIENT_UUID);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_USER_ID);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_CREATED_AT);
            $criteria->removeSelectColumn(PhysicalExaminationTableMap::COL_UPDATED_AT);
        } else {
            $criteria->removeSelectColumn($alias . '.uuid');
            $criteria->removeSelectColumn($alias . '.timestamp');
            $criteria->removeSelectColumn($alias . '.blood_pressure_systolic');
            $criteria->removeSelectColumn($alias . '.blood_pressure_diastolic');
            $criteria->removeSelectColumn($alias . '.heart_rate');
            $criteria->removeSelectColumn($alias . '.weight');
            $criteria->removeSelectColumn($alias . '.height');
            $criteria->removeSelectColumn($alias . '.muac');
            $criteria->removeSelectColumn($alias . '.temperature');
            $criteria->removeSelectColumn($alias . '.skin_examination');
            $criteria->removeSelectColumn($alias . '.details_ent');
            $criteria->removeSelectColumn($alias . '.details_eyes');
            $criteria->removeSelectColumn($alias . '.details_head');
            $criteria->removeSelectColumn($alias . '.details_muscles_bones');
            $criteria->removeSelectColumn($alias . '.details_heart');
            $criteria->removeSelectColumn($alias . '.details_lung');
            $criteria->removeSelectColumn($alias . '.details_gastrointestinal');
            $criteria->removeSelectColumn($alias . '.details_urinary_tract');
            $criteria->removeSelectColumn($alias . '.details_reproductive_system');
            $criteria->removeSelectColumn($alias . '.details_other');
            $criteria->removeSelectColumn($alias . '.general_impression');
            $criteria->removeSelectColumn($alias . '.patient_uuid');
            $criteria->removeSelectColumn($alias . '.user_id');
            $criteria->removeSelectColumn($alias . '.created_at');
            $criteria->removeSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(PhysicalExaminationTableMap::DATABASE_NAME)->getTable(PhysicalExaminationTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a PhysicalExamination or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or PhysicalExamination object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PhysicalExaminationTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \PhysicalExamination) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PhysicalExaminationTableMap::DATABASE_NAME);
            $criteria->add(PhysicalExaminationTableMap::COL_UUID, (array) $values, Criteria::IN);
        }

        $query = PhysicalExaminationQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PhysicalExaminationTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PhysicalExaminationTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the physical_examination table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return PhysicalExaminationQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PhysicalExamination or Criteria object.
     *
     * @param mixed $criteria Criteria or PhysicalExamination object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PhysicalExaminationTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PhysicalExamination object
        }


        // Set the correct dbName
        $query = PhysicalExaminationQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
