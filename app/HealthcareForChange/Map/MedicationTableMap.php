<?php

namespace Map;

use \Medication;
use \MedicationQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'medication' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class MedicationTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.MedicationTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'HealtchareForChange';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'medication';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\Medication';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'Medication';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the uuid field
     */
    public const COL_UUID = 'medication.uuid';

    /**
     * the column name for the name field
     */
    public const COL_NAME = 'medication.name';

    /**
     * the column name for the code field
     */
    public const COL_CODE = 'medication.code';

    /**
     * the column name for the active field
     */
    public const COL_ACTIVE = 'medication.active';

    /**
     * the column name for the medication_dose_form_id field
     */
    public const COL_MEDICATION_DOSE_FORM_ID = 'medication.medication_dose_form_id';

    /**
     * the column name for the medication_marketing_authorization_holder_id field
     */
    public const COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID = 'medication.medication_marketing_authorization_holder_id';

    /**
     * the column name for the strength field
     */
    public const COL_STRENGTH = 'medication.strength';

    /**
     * the column name for the created_at field
     */
    public const COL_CREATED_AT = 'medication.created_at';

    /**
     * the column name for the updated_at field
     */
    public const COL_UPDATED_AT = 'medication.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Uuid', 'Name', 'Code', 'Active', 'MedicationDoseFormId', 'MedicationMarketingAuthorizationHolderId', 'Strength', 'CreatedAt', 'UpdatedAt', ],
        self::TYPE_CAMELNAME     => ['uuid', 'name', 'code', 'active', 'medicationDoseFormId', 'medicationMarketingAuthorizationHolderId', 'strength', 'createdAt', 'updatedAt', ],
        self::TYPE_COLNAME       => [MedicationTableMap::COL_UUID, MedicationTableMap::COL_NAME, MedicationTableMap::COL_CODE, MedicationTableMap::COL_ACTIVE, MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID, MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID, MedicationTableMap::COL_STRENGTH, MedicationTableMap::COL_CREATED_AT, MedicationTableMap::COL_UPDATED_AT, ],
        self::TYPE_FIELDNAME     => ['uuid', 'name', 'code', 'active', 'medication_dose_form_id', 'medication_marketing_authorization_holder_id', 'strength', 'created_at', 'updated_at', ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Uuid' => 0, 'Name' => 1, 'Code' => 2, 'Active' => 3, 'MedicationDoseFormId' => 4, 'MedicationMarketingAuthorizationHolderId' => 5, 'Strength' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, ],
        self::TYPE_CAMELNAME     => ['uuid' => 0, 'name' => 1, 'code' => 2, 'active' => 3, 'medicationDoseFormId' => 4, 'medicationMarketingAuthorizationHolderId' => 5, 'strength' => 6, 'createdAt' => 7, 'updatedAt' => 8, ],
        self::TYPE_COLNAME       => [MedicationTableMap::COL_UUID => 0, MedicationTableMap::COL_NAME => 1, MedicationTableMap::COL_CODE => 2, MedicationTableMap::COL_ACTIVE => 3, MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID => 4, MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID => 5, MedicationTableMap::COL_STRENGTH => 6, MedicationTableMap::COL_CREATED_AT => 7, MedicationTableMap::COL_UPDATED_AT => 8, ],
        self::TYPE_FIELDNAME     => ['uuid' => 0, 'name' => 1, 'code' => 2, 'active' => 3, 'medication_dose_form_id' => 4, 'medication_marketing_authorization_holder_id' => 5, 'strength' => 6, 'created_at' => 7, 'updated_at' => 8, ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Uuid' => 'UUID',
        'Medication.Uuid' => 'UUID',
        'uuid' => 'UUID',
        'medication.uuid' => 'UUID',
        'MedicationTableMap::COL_UUID' => 'UUID',
        'COL_UUID' => 'UUID',
        'Name' => 'NAME',
        'Medication.Name' => 'NAME',
        'name' => 'NAME',
        'medication.name' => 'NAME',
        'MedicationTableMap::COL_NAME' => 'NAME',
        'COL_NAME' => 'NAME',
        'Code' => 'CODE',
        'Medication.Code' => 'CODE',
        'code' => 'CODE',
        'medication.code' => 'CODE',
        'MedicationTableMap::COL_CODE' => 'CODE',
        'COL_CODE' => 'CODE',
        'Active' => 'ACTIVE',
        'Medication.Active' => 'ACTIVE',
        'active' => 'ACTIVE',
        'medication.active' => 'ACTIVE',
        'MedicationTableMap::COL_ACTIVE' => 'ACTIVE',
        'COL_ACTIVE' => 'ACTIVE',
        'MedicationDoseFormId' => 'MEDICATION_DOSE_FORM_ID',
        'Medication.MedicationDoseFormId' => 'MEDICATION_DOSE_FORM_ID',
        'medicationDoseFormId' => 'MEDICATION_DOSE_FORM_ID',
        'medication.medicationDoseFormId' => 'MEDICATION_DOSE_FORM_ID',
        'MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID' => 'MEDICATION_DOSE_FORM_ID',
        'COL_MEDICATION_DOSE_FORM_ID' => 'MEDICATION_DOSE_FORM_ID',
        'medication_dose_form_id' => 'MEDICATION_DOSE_FORM_ID',
        'medication.medication_dose_form_id' => 'MEDICATION_DOSE_FORM_ID',
        'MedicationMarketingAuthorizationHolderId' => 'MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID',
        'Medication.MedicationMarketingAuthorizationHolderId' => 'MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID',
        'medicationMarketingAuthorizationHolderId' => 'MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID',
        'medication.medicationMarketingAuthorizationHolderId' => 'MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID',
        'MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID' => 'MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID',
        'COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID' => 'MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID',
        'medication_marketing_authorization_holder_id' => 'MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID',
        'medication.medication_marketing_authorization_holder_id' => 'MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID',
        'Strength' => 'STRENGTH',
        'Medication.Strength' => 'STRENGTH',
        'strength' => 'STRENGTH',
        'medication.strength' => 'STRENGTH',
        'MedicationTableMap::COL_STRENGTH' => 'STRENGTH',
        'COL_STRENGTH' => 'STRENGTH',
        'CreatedAt' => 'CREATED_AT',
        'Medication.CreatedAt' => 'CREATED_AT',
        'createdAt' => 'CREATED_AT',
        'medication.createdAt' => 'CREATED_AT',
        'MedicationTableMap::COL_CREATED_AT' => 'CREATED_AT',
        'COL_CREATED_AT' => 'CREATED_AT',
        'created_at' => 'CREATED_AT',
        'medication.created_at' => 'CREATED_AT',
        'UpdatedAt' => 'UPDATED_AT',
        'Medication.UpdatedAt' => 'UPDATED_AT',
        'updatedAt' => 'UPDATED_AT',
        'medication.updatedAt' => 'UPDATED_AT',
        'MedicationTableMap::COL_UPDATED_AT' => 'UPDATED_AT',
        'COL_UPDATED_AT' => 'UPDATED_AT',
        'updated_at' => 'UPDATED_AT',
        'medication.updated_at' => 'UPDATED_AT',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('medication');
        $this->setPhpName('Medication');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Medication');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('uuid', 'Uuid', 'VARCHAR', true, 36, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('code', 'Code', 'VARCHAR', false, 255, null);
        $this->addColumn('active', 'Active', 'BOOLEAN', false, null, false);
        $this->addForeignKey('medication_dose_form_id', 'MedicationDoseFormId', 'BIGINT', 'medication_dose_form', 'id', true, null, null);
        $this->addForeignKey('medication_marketing_authorization_holder_id', 'MedicationMarketingAuthorizationHolderId', 'INTEGER', 'medication_marketing_authorization_holder', 'id', false, null, null);
        $this->addColumn('strength', 'Strength', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
        $this->addRelation('MedicationDoseForm', '\\MedicationDoseForm', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':medication_dose_form_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('MedicationMarketingAuthorizationHolder', '\\MedicationMarketingAuthorizationHolder', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':medication_marketing_authorization_holder_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('DewormingHistory', '\\DewormingHistory', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':medication_id',
    1 => ':uuid',
  ),
), null, null, 'DewormingHistories', false);
        $this->addRelation('Prescription', '\\Prescription', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':medication_uuid',
    1 => ':uuid',
  ),
), null, null, 'Prescriptions', false);
    }

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array<string, array> Associative array (name => parameters) of behaviors
     */
    public function getBehaviors(): array
    {
        return [
            'timestampable' => ['create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false'],
        ];
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? MedicationTableMap::CLASS_DEFAULT : MedicationTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (Medication object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = MedicationTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = MedicationTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + MedicationTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = MedicationTableMap::OM_CLASS;
            /** @var Medication $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            MedicationTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = MedicationTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = MedicationTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Medication $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                MedicationTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(MedicationTableMap::COL_UUID);
            $criteria->addSelectColumn(MedicationTableMap::COL_NAME);
            $criteria->addSelectColumn(MedicationTableMap::COL_CODE);
            $criteria->addSelectColumn(MedicationTableMap::COL_ACTIVE);
            $criteria->addSelectColumn(MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID);
            $criteria->addSelectColumn(MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID);
            $criteria->addSelectColumn(MedicationTableMap::COL_STRENGTH);
            $criteria->addSelectColumn(MedicationTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(MedicationTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.code');
            $criteria->addSelectColumn($alias . '.active');
            $criteria->addSelectColumn($alias . '.medication_dose_form_id');
            $criteria->addSelectColumn($alias . '.medication_marketing_authorization_holder_id');
            $criteria->addSelectColumn($alias . '.strength');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(MedicationTableMap::COL_UUID);
            $criteria->removeSelectColumn(MedicationTableMap::COL_NAME);
            $criteria->removeSelectColumn(MedicationTableMap::COL_CODE);
            $criteria->removeSelectColumn(MedicationTableMap::COL_ACTIVE);
            $criteria->removeSelectColumn(MedicationTableMap::COL_MEDICATION_DOSE_FORM_ID);
            $criteria->removeSelectColumn(MedicationTableMap::COL_MEDICATION_MARKETING_AUTHORIZATION_HOLDER_ID);
            $criteria->removeSelectColumn(MedicationTableMap::COL_STRENGTH);
            $criteria->removeSelectColumn(MedicationTableMap::COL_CREATED_AT);
            $criteria->removeSelectColumn(MedicationTableMap::COL_UPDATED_AT);
        } else {
            $criteria->removeSelectColumn($alias . '.uuid');
            $criteria->removeSelectColumn($alias . '.name');
            $criteria->removeSelectColumn($alias . '.code');
            $criteria->removeSelectColumn($alias . '.active');
            $criteria->removeSelectColumn($alias . '.medication_dose_form_id');
            $criteria->removeSelectColumn($alias . '.medication_marketing_authorization_holder_id');
            $criteria->removeSelectColumn($alias . '.strength');
            $criteria->removeSelectColumn($alias . '.created_at');
            $criteria->removeSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(MedicationTableMap::DATABASE_NAME)->getTable(MedicationTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a Medication or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or Medication object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicationTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Medication) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(MedicationTableMap::DATABASE_NAME);
            $criteria->add(MedicationTableMap::COL_UUID, (array) $values, Criteria::IN);
        }

        $query = MedicationQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            MedicationTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                MedicationTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the medication table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return MedicationQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Medication or Criteria object.
     *
     * @param mixed $criteria Criteria or Medication object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicationTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Medication object
        }


        // Set the correct dbName
        $query = MedicationQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
