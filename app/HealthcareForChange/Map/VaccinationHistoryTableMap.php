<?php

namespace Map;

use \VaccinationHistory;
use \VaccinationHistoryQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'vaccination_history' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class VaccinationHistoryTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.VaccinationHistoryTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'HealtchareForChange';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'vaccination_history';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\VaccinationHistory';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'VaccinationHistory';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the uuid field
     */
    public const COL_UUID = 'vaccination_history.uuid';

    /**
     * the column name for the timestamp field
     */
    public const COL_TIMESTAMP = 'vaccination_history.timestamp';

    /**
     * the column name for the comment field
     */
    public const COL_COMMENT = 'vaccination_history.comment';

    /**
     * the column name for the patient_uuid field
     */
    public const COL_PATIENT_UUID = 'vaccination_history.patient_uuid';

    /**
     * the column name for the vaccine_uuid field
     */
    public const COL_VACCINE_UUID = 'vaccination_history.vaccine_uuid';

    /**
     * the column name for the user_id field
     */
    public const COL_USER_ID = 'vaccination_history.user_id';

    /**
     * the column name for the lot field
     */
    public const COL_LOT = 'vaccination_history.lot';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Uuid', 'Timestamp', 'Comment', 'PatientUuid', 'VaccineUuid', 'UserId', 'Lot', ],
        self::TYPE_CAMELNAME     => ['uuid', 'timestamp', 'comment', 'patientUuid', 'vaccineUuid', 'userId', 'lot', ],
        self::TYPE_COLNAME       => [VaccinationHistoryTableMap::COL_UUID, VaccinationHistoryTableMap::COL_TIMESTAMP, VaccinationHistoryTableMap::COL_COMMENT, VaccinationHistoryTableMap::COL_PATIENT_UUID, VaccinationHistoryTableMap::COL_VACCINE_UUID, VaccinationHistoryTableMap::COL_USER_ID, VaccinationHistoryTableMap::COL_LOT, ],
        self::TYPE_FIELDNAME     => ['uuid', 'timestamp', 'comment', 'patient_uuid', 'vaccine_uuid', 'user_id', 'lot', ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Uuid' => 0, 'Timestamp' => 1, 'Comment' => 2, 'PatientUuid' => 3, 'VaccineUuid' => 4, 'UserId' => 5, 'Lot' => 6, ],
        self::TYPE_CAMELNAME     => ['uuid' => 0, 'timestamp' => 1, 'comment' => 2, 'patientUuid' => 3, 'vaccineUuid' => 4, 'userId' => 5, 'lot' => 6, ],
        self::TYPE_COLNAME       => [VaccinationHistoryTableMap::COL_UUID => 0, VaccinationHistoryTableMap::COL_TIMESTAMP => 1, VaccinationHistoryTableMap::COL_COMMENT => 2, VaccinationHistoryTableMap::COL_PATIENT_UUID => 3, VaccinationHistoryTableMap::COL_VACCINE_UUID => 4, VaccinationHistoryTableMap::COL_USER_ID => 5, VaccinationHistoryTableMap::COL_LOT => 6, ],
        self::TYPE_FIELDNAME     => ['uuid' => 0, 'timestamp' => 1, 'comment' => 2, 'patient_uuid' => 3, 'vaccine_uuid' => 4, 'user_id' => 5, 'lot' => 6, ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Uuid' => 'UUID',
        'VaccinationHistory.Uuid' => 'UUID',
        'uuid' => 'UUID',
        'vaccinationHistory.uuid' => 'UUID',
        'VaccinationHistoryTableMap::COL_UUID' => 'UUID',
        'COL_UUID' => 'UUID',
        'vaccination_history.uuid' => 'UUID',
        'Timestamp' => 'TIMESTAMP',
        'VaccinationHistory.Timestamp' => 'TIMESTAMP',
        'timestamp' => 'TIMESTAMP',
        'vaccinationHistory.timestamp' => 'TIMESTAMP',
        'VaccinationHistoryTableMap::COL_TIMESTAMP' => 'TIMESTAMP',
        'COL_TIMESTAMP' => 'TIMESTAMP',
        'vaccination_history.timestamp' => 'TIMESTAMP',
        'Comment' => 'COMMENT',
        'VaccinationHistory.Comment' => 'COMMENT',
        'comment' => 'COMMENT',
        'vaccinationHistory.comment' => 'COMMENT',
        'VaccinationHistoryTableMap::COL_COMMENT' => 'COMMENT',
        'COL_COMMENT' => 'COMMENT',
        'vaccination_history.comment' => 'COMMENT',
        'PatientUuid' => 'PATIENT_UUID',
        'VaccinationHistory.PatientUuid' => 'PATIENT_UUID',
        'patientUuid' => 'PATIENT_UUID',
        'vaccinationHistory.patientUuid' => 'PATIENT_UUID',
        'VaccinationHistoryTableMap::COL_PATIENT_UUID' => 'PATIENT_UUID',
        'COL_PATIENT_UUID' => 'PATIENT_UUID',
        'patient_uuid' => 'PATIENT_UUID',
        'vaccination_history.patient_uuid' => 'PATIENT_UUID',
        'VaccineUuid' => 'VACCINE_UUID',
        'VaccinationHistory.VaccineUuid' => 'VACCINE_UUID',
        'vaccineUuid' => 'VACCINE_UUID',
        'vaccinationHistory.vaccineUuid' => 'VACCINE_UUID',
        'VaccinationHistoryTableMap::COL_VACCINE_UUID' => 'VACCINE_UUID',
        'COL_VACCINE_UUID' => 'VACCINE_UUID',
        'vaccine_uuid' => 'VACCINE_UUID',
        'vaccination_history.vaccine_uuid' => 'VACCINE_UUID',
        'UserId' => 'USER_ID',
        'VaccinationHistory.UserId' => 'USER_ID',
        'userId' => 'USER_ID',
        'vaccinationHistory.userId' => 'USER_ID',
        'VaccinationHistoryTableMap::COL_USER_ID' => 'USER_ID',
        'COL_USER_ID' => 'USER_ID',
        'user_id' => 'USER_ID',
        'vaccination_history.user_id' => 'USER_ID',
        'Lot' => 'LOT',
        'VaccinationHistory.Lot' => 'LOT',
        'lot' => 'LOT',
        'vaccinationHistory.lot' => 'LOT',
        'VaccinationHistoryTableMap::COL_LOT' => 'LOT',
        'COL_LOT' => 'LOT',
        'vaccination_history.lot' => 'LOT',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('vaccination_history');
        $this->setPhpName('VaccinationHistory');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\VaccinationHistory');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('uuid', 'Uuid', 'VARCHAR', true, 36, null);
        $this->addColumn('timestamp', 'Timestamp', 'TIMESTAMP', true, null, null);
        $this->addColumn('comment', 'Comment', 'VARCHAR', false, 255, null);
        $this->addForeignKey('patient_uuid', 'PatientUuid', 'VARCHAR', 'patient', 'uuid', true, 36, null);
        $this->addForeignKey('vaccine_uuid', 'VaccineUuid', 'VARCHAR', 'vaccine', 'uuid', true, 36, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'users', 'id', true, null, null);
        $this->addColumn('lot', 'Lot', 'VARCHAR', false, 255, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
        $this->addRelation('Patient', '\\Patient', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':patient_uuid',
    1 => ':uuid',
  ),
), null, null, null, false);
        $this->addRelation('Vaccine', '\\Vaccine', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vaccine_uuid',
    1 => ':uuid',
  ),
), null, null, null, false);
        $this->addRelation('User', '\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, null, false);
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? VaccinationHistoryTableMap::CLASS_DEFAULT : VaccinationHistoryTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (VaccinationHistory object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = VaccinationHistoryTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = VaccinationHistoryTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + VaccinationHistoryTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = VaccinationHistoryTableMap::OM_CLASS;
            /** @var VaccinationHistory $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            VaccinationHistoryTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = VaccinationHistoryTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = VaccinationHistoryTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var VaccinationHistory $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                VaccinationHistoryTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(VaccinationHistoryTableMap::COL_UUID);
            $criteria->addSelectColumn(VaccinationHistoryTableMap::COL_TIMESTAMP);
            $criteria->addSelectColumn(VaccinationHistoryTableMap::COL_COMMENT);
            $criteria->addSelectColumn(VaccinationHistoryTableMap::COL_PATIENT_UUID);
            $criteria->addSelectColumn(VaccinationHistoryTableMap::COL_VACCINE_UUID);
            $criteria->addSelectColumn(VaccinationHistoryTableMap::COL_USER_ID);
            $criteria->addSelectColumn(VaccinationHistoryTableMap::COL_LOT);
        } else {
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.timestamp');
            $criteria->addSelectColumn($alias . '.comment');
            $criteria->addSelectColumn($alias . '.patient_uuid');
            $criteria->addSelectColumn($alias . '.vaccine_uuid');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.lot');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(VaccinationHistoryTableMap::COL_UUID);
            $criteria->removeSelectColumn(VaccinationHistoryTableMap::COL_TIMESTAMP);
            $criteria->removeSelectColumn(VaccinationHistoryTableMap::COL_COMMENT);
            $criteria->removeSelectColumn(VaccinationHistoryTableMap::COL_PATIENT_UUID);
            $criteria->removeSelectColumn(VaccinationHistoryTableMap::COL_VACCINE_UUID);
            $criteria->removeSelectColumn(VaccinationHistoryTableMap::COL_USER_ID);
            $criteria->removeSelectColumn(VaccinationHistoryTableMap::COL_LOT);
        } else {
            $criteria->removeSelectColumn($alias . '.uuid');
            $criteria->removeSelectColumn($alias . '.timestamp');
            $criteria->removeSelectColumn($alias . '.comment');
            $criteria->removeSelectColumn($alias . '.patient_uuid');
            $criteria->removeSelectColumn($alias . '.vaccine_uuid');
            $criteria->removeSelectColumn($alias . '.user_id');
            $criteria->removeSelectColumn($alias . '.lot');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(VaccinationHistoryTableMap::DATABASE_NAME)->getTable(VaccinationHistoryTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a VaccinationHistory or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or VaccinationHistory object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VaccinationHistoryTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \VaccinationHistory) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(VaccinationHistoryTableMap::DATABASE_NAME);
            $criteria->add(VaccinationHistoryTableMap::COL_UUID, (array) $values, Criteria::IN);
        }

        $query = VaccinationHistoryQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            VaccinationHistoryTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                VaccinationHistoryTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the vaccination_history table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return VaccinationHistoryQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a VaccinationHistory or Criteria object.
     *
     * @param mixed $criteria Criteria or VaccinationHistory object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VaccinationHistoryTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from VaccinationHistory object
        }


        // Set the correct dbName
        $query = VaccinationHistoryQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
