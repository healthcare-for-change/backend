<?php

namespace Map;

use \AuthLogin;
use \AuthLoginQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'auth_logins' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class AuthLoginTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.AuthLoginTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'HealtchareForChange';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'auth_logins';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\AuthLogin';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'AuthLogin';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    public const COL_ID = 'auth_logins.id';

    /**
     * the column name for the user_id field
     */
    public const COL_USER_ID = 'auth_logins.user_id';

    /**
     * the column name for the ip_address field
     */
    public const COL_IP_ADDRESS = 'auth_logins.ip_address';

    /**
     * the column name for the user_agent field
     */
    public const COL_USER_AGENT = 'auth_logins.user_agent';

    /**
     * the column name for the id_type field
     */
    public const COL_ID_TYPE = 'auth_logins.id_type';

    /**
     * the column name for the identifier field
     */
    public const COL_IDENTIFIER = 'auth_logins.identifier';

    /**
     * the column name for the date field
     */
    public const COL_DATE = 'auth_logins.date';

    /**
     * the column name for the success field
     */
    public const COL_SUCCESS = 'auth_logins.success';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Id', 'UserId', 'IpAddress', 'UserAgent', 'IdType', 'Identifier', 'Date', 'Success', ],
        self::TYPE_CAMELNAME     => ['id', 'userId', 'ipAddress', 'userAgent', 'idType', 'identifier', 'date', 'success', ],
        self::TYPE_COLNAME       => [AuthLoginTableMap::COL_ID, AuthLoginTableMap::COL_USER_ID, AuthLoginTableMap::COL_IP_ADDRESS, AuthLoginTableMap::COL_USER_AGENT, AuthLoginTableMap::COL_ID_TYPE, AuthLoginTableMap::COL_IDENTIFIER, AuthLoginTableMap::COL_DATE, AuthLoginTableMap::COL_SUCCESS, ],
        self::TYPE_FIELDNAME     => ['id', 'user_id', 'ip_address', 'user_agent', 'id_type', 'identifier', 'date', 'success', ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Id' => 0, 'UserId' => 1, 'IpAddress' => 2, 'UserAgent' => 3, 'IdType' => 4, 'Identifier' => 5, 'Date' => 6, 'Success' => 7, ],
        self::TYPE_CAMELNAME     => ['id' => 0, 'userId' => 1, 'ipAddress' => 2, 'userAgent' => 3, 'idType' => 4, 'identifier' => 5, 'date' => 6, 'success' => 7, ],
        self::TYPE_COLNAME       => [AuthLoginTableMap::COL_ID => 0, AuthLoginTableMap::COL_USER_ID => 1, AuthLoginTableMap::COL_IP_ADDRESS => 2, AuthLoginTableMap::COL_USER_AGENT => 3, AuthLoginTableMap::COL_ID_TYPE => 4, AuthLoginTableMap::COL_IDENTIFIER => 5, AuthLoginTableMap::COL_DATE => 6, AuthLoginTableMap::COL_SUCCESS => 7, ],
        self::TYPE_FIELDNAME     => ['id' => 0, 'user_id' => 1, 'ip_address' => 2, 'user_agent' => 3, 'id_type' => 4, 'identifier' => 5, 'date' => 6, 'success' => 7, ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Id' => 'ID',
        'AuthLogin.Id' => 'ID',
        'id' => 'ID',
        'authLogin.id' => 'ID',
        'AuthLoginTableMap::COL_ID' => 'ID',
        'COL_ID' => 'ID',
        'auth_logins.id' => 'ID',
        'UserId' => 'USER_ID',
        'AuthLogin.UserId' => 'USER_ID',
        'userId' => 'USER_ID',
        'authLogin.userId' => 'USER_ID',
        'AuthLoginTableMap::COL_USER_ID' => 'USER_ID',
        'COL_USER_ID' => 'USER_ID',
        'user_id' => 'USER_ID',
        'auth_logins.user_id' => 'USER_ID',
        'IpAddress' => 'IP_ADDRESS',
        'AuthLogin.IpAddress' => 'IP_ADDRESS',
        'ipAddress' => 'IP_ADDRESS',
        'authLogin.ipAddress' => 'IP_ADDRESS',
        'AuthLoginTableMap::COL_IP_ADDRESS' => 'IP_ADDRESS',
        'COL_IP_ADDRESS' => 'IP_ADDRESS',
        'ip_address' => 'IP_ADDRESS',
        'auth_logins.ip_address' => 'IP_ADDRESS',
        'UserAgent' => 'USER_AGENT',
        'AuthLogin.UserAgent' => 'USER_AGENT',
        'userAgent' => 'USER_AGENT',
        'authLogin.userAgent' => 'USER_AGENT',
        'AuthLoginTableMap::COL_USER_AGENT' => 'USER_AGENT',
        'COL_USER_AGENT' => 'USER_AGENT',
        'user_agent' => 'USER_AGENT',
        'auth_logins.user_agent' => 'USER_AGENT',
        'IdType' => 'ID_TYPE',
        'AuthLogin.IdType' => 'ID_TYPE',
        'idType' => 'ID_TYPE',
        'authLogin.idType' => 'ID_TYPE',
        'AuthLoginTableMap::COL_ID_TYPE' => 'ID_TYPE',
        'COL_ID_TYPE' => 'ID_TYPE',
        'id_type' => 'ID_TYPE',
        'auth_logins.id_type' => 'ID_TYPE',
        'Identifier' => 'IDENTIFIER',
        'AuthLogin.Identifier' => 'IDENTIFIER',
        'identifier' => 'IDENTIFIER',
        'authLogin.identifier' => 'IDENTIFIER',
        'AuthLoginTableMap::COL_IDENTIFIER' => 'IDENTIFIER',
        'COL_IDENTIFIER' => 'IDENTIFIER',
        'auth_logins.identifier' => 'IDENTIFIER',
        'Date' => 'DATE',
        'AuthLogin.Date' => 'DATE',
        'date' => 'DATE',
        'authLogin.date' => 'DATE',
        'AuthLoginTableMap::COL_DATE' => 'DATE',
        'COL_DATE' => 'DATE',
        'auth_logins.date' => 'DATE',
        'Success' => 'SUCCESS',
        'AuthLogin.Success' => 'SUCCESS',
        'success' => 'SUCCESS',
        'authLogin.success' => 'SUCCESS',
        'AuthLoginTableMap::COL_SUCCESS' => 'SUCCESS',
        'COL_SUCCESS' => 'SUCCESS',
        'auth_logins.success' => 'SUCCESS',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('auth_logins');
        $this->setPhpName('AuthLogin');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\AuthLogin');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('auth_logins_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'users', 'id', false, null, null);
        $this->addColumn('ip_address', 'IpAddress', 'VARCHAR', false, 255, null);
        $this->addColumn('user_agent', 'UserAgent', 'VARCHAR', false, 255, null);
        $this->addColumn('id_type', 'IdType', 'VARCHAR', false, 255, null);
        $this->addColumn('identifier', 'Identifier', 'VARCHAR', false, 255, null);
        $this->addColumn('date', 'Date', 'TIMESTAMP', true, null, null);
        $this->addColumn('success', 'Success', 'TINYINT', false, null, 0);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
        $this->addRelation('User', '\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? AuthLoginTableMap::CLASS_DEFAULT : AuthLoginTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (AuthLogin object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = AuthLoginTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AuthLoginTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AuthLoginTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AuthLoginTableMap::OM_CLASS;
            /** @var AuthLogin $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AuthLoginTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AuthLoginTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AuthLoginTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AuthLogin $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AuthLoginTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AuthLoginTableMap::COL_ID);
            $criteria->addSelectColumn(AuthLoginTableMap::COL_USER_ID);
            $criteria->addSelectColumn(AuthLoginTableMap::COL_IP_ADDRESS);
            $criteria->addSelectColumn(AuthLoginTableMap::COL_USER_AGENT);
            $criteria->addSelectColumn(AuthLoginTableMap::COL_ID_TYPE);
            $criteria->addSelectColumn(AuthLoginTableMap::COL_IDENTIFIER);
            $criteria->addSelectColumn(AuthLoginTableMap::COL_DATE);
            $criteria->addSelectColumn(AuthLoginTableMap::COL_SUCCESS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.ip_address');
            $criteria->addSelectColumn($alias . '.user_agent');
            $criteria->addSelectColumn($alias . '.id_type');
            $criteria->addSelectColumn($alias . '.identifier');
            $criteria->addSelectColumn($alias . '.date');
            $criteria->addSelectColumn($alias . '.success');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(AuthLoginTableMap::COL_ID);
            $criteria->removeSelectColumn(AuthLoginTableMap::COL_USER_ID);
            $criteria->removeSelectColumn(AuthLoginTableMap::COL_IP_ADDRESS);
            $criteria->removeSelectColumn(AuthLoginTableMap::COL_USER_AGENT);
            $criteria->removeSelectColumn(AuthLoginTableMap::COL_ID_TYPE);
            $criteria->removeSelectColumn(AuthLoginTableMap::COL_IDENTIFIER);
            $criteria->removeSelectColumn(AuthLoginTableMap::COL_DATE);
            $criteria->removeSelectColumn(AuthLoginTableMap::COL_SUCCESS);
        } else {
            $criteria->removeSelectColumn($alias . '.id');
            $criteria->removeSelectColumn($alias . '.user_id');
            $criteria->removeSelectColumn($alias . '.ip_address');
            $criteria->removeSelectColumn($alias . '.user_agent');
            $criteria->removeSelectColumn($alias . '.id_type');
            $criteria->removeSelectColumn($alias . '.identifier');
            $criteria->removeSelectColumn($alias . '.date');
            $criteria->removeSelectColumn($alias . '.success');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(AuthLoginTableMap::DATABASE_NAME)->getTable(AuthLoginTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a AuthLogin or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or AuthLogin object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AuthLoginTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AuthLogin) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AuthLoginTableMap::DATABASE_NAME);
            $criteria->add(AuthLoginTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = AuthLoginQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AuthLoginTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AuthLoginTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the auth_logins table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return AuthLoginQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AuthLogin or Criteria object.
     *
     * @param mixed $criteria Criteria or AuthLogin object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AuthLoginTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AuthLogin object
        }

        if ($criteria->containsKey(AuthLoginTableMap::COL_ID) && $criteria->keyContainsValue(AuthLoginTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.AuthLoginTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = AuthLoginQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
