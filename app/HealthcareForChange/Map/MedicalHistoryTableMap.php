<?php

namespace Map;

use \MedicalHistory;
use \MedicalHistoryQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'medical_history' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class MedicalHistoryTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.MedicalHistoryTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'HealtchareForChange';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'medical_history';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\MedicalHistory';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'MedicalHistory';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the uuid field
     */
    public const COL_UUID = 'medical_history.uuid';

    /**
     * the column name for the patient_uuid field
     */
    public const COL_PATIENT_UUID = 'medical_history.patient_uuid';

    /**
     * the column name for the user_id field
     */
    public const COL_USER_ID = 'medical_history.user_id';

    /**
     * the column name for the clinical_status field
     */
    public const COL_CLINICAL_STATUS = 'medical_history.clinical_status';

    /**
     * the column name for the verification_status field
     */
    public const COL_VERIFICATION_STATUS = 'medical_history.verification_status';

    /**
     * the column name for the severity field
     */
    public const COL_SEVERITY = 'medical_history.severity';

    /**
     * the column name for the note field
     */
    public const COL_NOTE = 'medical_history.note';

    /**
     * the column name for the medical_diagnosis_code_id field
     */
    public const COL_MEDICAL_DIAGNOSIS_CODE_ID = 'medical_history.medical_diagnosis_code_id';

    /**
     * the column name for the onset_date field
     */
    public const COL_ONSET_DATE = 'medical_history.onset_date';

    /**
     * the column name for the onset_age field
     */
    public const COL_ONSET_AGE = 'medical_history.onset_age';

    /**
     * the column name for the abatement_date field
     */
    public const COL_ABATEMENT_DATE = 'medical_history.abatement_date';

    /**
     * the column name for the abatement_age field
     */
    public const COL_ABATEMENT_AGE = 'medical_history.abatement_age';

    /**
     * the column name for the created_at field
     */
    public const COL_CREATED_AT = 'medical_history.created_at';

    /**
     * the column name for the updated_at field
     */
    public const COL_UPDATED_AT = 'medical_history.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Uuid', 'PatientUuid', 'UserId', 'ClinicalStatus', 'VerificationStatus', 'Severity', 'Note', 'MedicalDiagnosisCodeId', 'OnsetDate', 'OnsetAge', 'AbatementDate', 'AbatementAge', 'CreatedAt', 'UpdatedAt', ],
        self::TYPE_CAMELNAME     => ['uuid', 'patientUuid', 'userId', 'clinicalStatus', 'verificationStatus', 'severity', 'note', 'medicalDiagnosisCodeId', 'onsetDate', 'onsetAge', 'abatementDate', 'abatementAge', 'createdAt', 'updatedAt', ],
        self::TYPE_COLNAME       => [MedicalHistoryTableMap::COL_UUID, MedicalHistoryTableMap::COL_PATIENT_UUID, MedicalHistoryTableMap::COL_USER_ID, MedicalHistoryTableMap::COL_CLINICAL_STATUS, MedicalHistoryTableMap::COL_VERIFICATION_STATUS, MedicalHistoryTableMap::COL_SEVERITY, MedicalHistoryTableMap::COL_NOTE, MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID, MedicalHistoryTableMap::COL_ONSET_DATE, MedicalHistoryTableMap::COL_ONSET_AGE, MedicalHistoryTableMap::COL_ABATEMENT_DATE, MedicalHistoryTableMap::COL_ABATEMENT_AGE, MedicalHistoryTableMap::COL_CREATED_AT, MedicalHistoryTableMap::COL_UPDATED_AT, ],
        self::TYPE_FIELDNAME     => ['uuid', 'patient_uuid', 'user_id', 'clinical_status', 'verification_status', 'severity', 'note', 'medical_diagnosis_code_id', 'onset_date', 'onset_age', 'abatement_date', 'abatement_age', 'created_at', 'updated_at', ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Uuid' => 0, 'PatientUuid' => 1, 'UserId' => 2, 'ClinicalStatus' => 3, 'VerificationStatus' => 4, 'Severity' => 5, 'Note' => 6, 'MedicalDiagnosisCodeId' => 7, 'OnsetDate' => 8, 'OnsetAge' => 9, 'AbatementDate' => 10, 'AbatementAge' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, ],
        self::TYPE_CAMELNAME     => ['uuid' => 0, 'patientUuid' => 1, 'userId' => 2, 'clinicalStatus' => 3, 'verificationStatus' => 4, 'severity' => 5, 'note' => 6, 'medicalDiagnosisCodeId' => 7, 'onsetDate' => 8, 'onsetAge' => 9, 'abatementDate' => 10, 'abatementAge' => 11, 'createdAt' => 12, 'updatedAt' => 13, ],
        self::TYPE_COLNAME       => [MedicalHistoryTableMap::COL_UUID => 0, MedicalHistoryTableMap::COL_PATIENT_UUID => 1, MedicalHistoryTableMap::COL_USER_ID => 2, MedicalHistoryTableMap::COL_CLINICAL_STATUS => 3, MedicalHistoryTableMap::COL_VERIFICATION_STATUS => 4, MedicalHistoryTableMap::COL_SEVERITY => 5, MedicalHistoryTableMap::COL_NOTE => 6, MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID => 7, MedicalHistoryTableMap::COL_ONSET_DATE => 8, MedicalHistoryTableMap::COL_ONSET_AGE => 9, MedicalHistoryTableMap::COL_ABATEMENT_DATE => 10, MedicalHistoryTableMap::COL_ABATEMENT_AGE => 11, MedicalHistoryTableMap::COL_CREATED_AT => 12, MedicalHistoryTableMap::COL_UPDATED_AT => 13, ],
        self::TYPE_FIELDNAME     => ['uuid' => 0, 'patient_uuid' => 1, 'user_id' => 2, 'clinical_status' => 3, 'verification_status' => 4, 'severity' => 5, 'note' => 6, 'medical_diagnosis_code_id' => 7, 'onset_date' => 8, 'onset_age' => 9, 'abatement_date' => 10, 'abatement_age' => 11, 'created_at' => 12, 'updated_at' => 13, ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Uuid' => 'UUID',
        'MedicalHistory.Uuid' => 'UUID',
        'uuid' => 'UUID',
        'medicalHistory.uuid' => 'UUID',
        'MedicalHistoryTableMap::COL_UUID' => 'UUID',
        'COL_UUID' => 'UUID',
        'medical_history.uuid' => 'UUID',
        'PatientUuid' => 'PATIENT_UUID',
        'MedicalHistory.PatientUuid' => 'PATIENT_UUID',
        'patientUuid' => 'PATIENT_UUID',
        'medicalHistory.patientUuid' => 'PATIENT_UUID',
        'MedicalHistoryTableMap::COL_PATIENT_UUID' => 'PATIENT_UUID',
        'COL_PATIENT_UUID' => 'PATIENT_UUID',
        'patient_uuid' => 'PATIENT_UUID',
        'medical_history.patient_uuid' => 'PATIENT_UUID',
        'UserId' => 'USER_ID',
        'MedicalHistory.UserId' => 'USER_ID',
        'userId' => 'USER_ID',
        'medicalHistory.userId' => 'USER_ID',
        'MedicalHistoryTableMap::COL_USER_ID' => 'USER_ID',
        'COL_USER_ID' => 'USER_ID',
        'user_id' => 'USER_ID',
        'medical_history.user_id' => 'USER_ID',
        'ClinicalStatus' => 'CLINICAL_STATUS',
        'MedicalHistory.ClinicalStatus' => 'CLINICAL_STATUS',
        'clinicalStatus' => 'CLINICAL_STATUS',
        'medicalHistory.clinicalStatus' => 'CLINICAL_STATUS',
        'MedicalHistoryTableMap::COL_CLINICAL_STATUS' => 'CLINICAL_STATUS',
        'COL_CLINICAL_STATUS' => 'CLINICAL_STATUS',
        'clinical_status' => 'CLINICAL_STATUS',
        'medical_history.clinical_status' => 'CLINICAL_STATUS',
        'VerificationStatus' => 'VERIFICATION_STATUS',
        'MedicalHistory.VerificationStatus' => 'VERIFICATION_STATUS',
        'verificationStatus' => 'VERIFICATION_STATUS',
        'medicalHistory.verificationStatus' => 'VERIFICATION_STATUS',
        'MedicalHistoryTableMap::COL_VERIFICATION_STATUS' => 'VERIFICATION_STATUS',
        'COL_VERIFICATION_STATUS' => 'VERIFICATION_STATUS',
        'verification_status' => 'VERIFICATION_STATUS',
        'medical_history.verification_status' => 'VERIFICATION_STATUS',
        'Severity' => 'SEVERITY',
        'MedicalHistory.Severity' => 'SEVERITY',
        'severity' => 'SEVERITY',
        'medicalHistory.severity' => 'SEVERITY',
        'MedicalHistoryTableMap::COL_SEVERITY' => 'SEVERITY',
        'COL_SEVERITY' => 'SEVERITY',
        'medical_history.severity' => 'SEVERITY',
        'Note' => 'NOTE',
        'MedicalHistory.Note' => 'NOTE',
        'note' => 'NOTE',
        'medicalHistory.note' => 'NOTE',
        'MedicalHistoryTableMap::COL_NOTE' => 'NOTE',
        'COL_NOTE' => 'NOTE',
        'medical_history.note' => 'NOTE',
        'MedicalDiagnosisCodeId' => 'MEDICAL_DIAGNOSIS_CODE_ID',
        'MedicalHistory.MedicalDiagnosisCodeId' => 'MEDICAL_DIAGNOSIS_CODE_ID',
        'medicalDiagnosisCodeId' => 'MEDICAL_DIAGNOSIS_CODE_ID',
        'medicalHistory.medicalDiagnosisCodeId' => 'MEDICAL_DIAGNOSIS_CODE_ID',
        'MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID' => 'MEDICAL_DIAGNOSIS_CODE_ID',
        'COL_MEDICAL_DIAGNOSIS_CODE_ID' => 'MEDICAL_DIAGNOSIS_CODE_ID',
        'medical_diagnosis_code_id' => 'MEDICAL_DIAGNOSIS_CODE_ID',
        'medical_history.medical_diagnosis_code_id' => 'MEDICAL_DIAGNOSIS_CODE_ID',
        'OnsetDate' => 'ONSET_DATE',
        'MedicalHistory.OnsetDate' => 'ONSET_DATE',
        'onsetDate' => 'ONSET_DATE',
        'medicalHistory.onsetDate' => 'ONSET_DATE',
        'MedicalHistoryTableMap::COL_ONSET_DATE' => 'ONSET_DATE',
        'COL_ONSET_DATE' => 'ONSET_DATE',
        'onset_date' => 'ONSET_DATE',
        'medical_history.onset_date' => 'ONSET_DATE',
        'OnsetAge' => 'ONSET_AGE',
        'MedicalHistory.OnsetAge' => 'ONSET_AGE',
        'onsetAge' => 'ONSET_AGE',
        'medicalHistory.onsetAge' => 'ONSET_AGE',
        'MedicalHistoryTableMap::COL_ONSET_AGE' => 'ONSET_AGE',
        'COL_ONSET_AGE' => 'ONSET_AGE',
        'onset_age' => 'ONSET_AGE',
        'medical_history.onset_age' => 'ONSET_AGE',
        'AbatementDate' => 'ABATEMENT_DATE',
        'MedicalHistory.AbatementDate' => 'ABATEMENT_DATE',
        'abatementDate' => 'ABATEMENT_DATE',
        'medicalHistory.abatementDate' => 'ABATEMENT_DATE',
        'MedicalHistoryTableMap::COL_ABATEMENT_DATE' => 'ABATEMENT_DATE',
        'COL_ABATEMENT_DATE' => 'ABATEMENT_DATE',
        'abatement_date' => 'ABATEMENT_DATE',
        'medical_history.abatement_date' => 'ABATEMENT_DATE',
        'AbatementAge' => 'ABATEMENT_AGE',
        'MedicalHistory.AbatementAge' => 'ABATEMENT_AGE',
        'abatementAge' => 'ABATEMENT_AGE',
        'medicalHistory.abatementAge' => 'ABATEMENT_AGE',
        'MedicalHistoryTableMap::COL_ABATEMENT_AGE' => 'ABATEMENT_AGE',
        'COL_ABATEMENT_AGE' => 'ABATEMENT_AGE',
        'abatement_age' => 'ABATEMENT_AGE',
        'medical_history.abatement_age' => 'ABATEMENT_AGE',
        'CreatedAt' => 'CREATED_AT',
        'MedicalHistory.CreatedAt' => 'CREATED_AT',
        'createdAt' => 'CREATED_AT',
        'medicalHistory.createdAt' => 'CREATED_AT',
        'MedicalHistoryTableMap::COL_CREATED_AT' => 'CREATED_AT',
        'COL_CREATED_AT' => 'CREATED_AT',
        'created_at' => 'CREATED_AT',
        'medical_history.created_at' => 'CREATED_AT',
        'UpdatedAt' => 'UPDATED_AT',
        'MedicalHistory.UpdatedAt' => 'UPDATED_AT',
        'updatedAt' => 'UPDATED_AT',
        'medicalHistory.updatedAt' => 'UPDATED_AT',
        'MedicalHistoryTableMap::COL_UPDATED_AT' => 'UPDATED_AT',
        'COL_UPDATED_AT' => 'UPDATED_AT',
        'updated_at' => 'UPDATED_AT',
        'medical_history.updated_at' => 'UPDATED_AT',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('medical_history');
        $this->setPhpName('MedicalHistory');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\MedicalHistory');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('uuid', 'Uuid', 'VARCHAR', true, 36, null);
        $this->addForeignKey('patient_uuid', 'PatientUuid', 'VARCHAR', 'patient', 'uuid', true, 36, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'users', 'id', true, null, null);
        $this->addColumn('clinical_status', 'ClinicalStatus', 'TINYINT', false, null, null);
        $this->addColumn('verification_status', 'VerificationStatus', 'TINYINT', false, null, null);
        $this->addColumn('severity', 'Severity', 'TINYINT', false, null, null);
        $this->addColumn('note', 'Note', 'CLOB', false, null, null);
        $this->addForeignKey('medical_diagnosis_code_id', 'MedicalDiagnosisCodeId', 'BIGINT', 'medical_diagnosis_code', 'id', false, null, null);
        $this->addColumn('onset_date', 'OnsetDate', 'DATE', false, null, null);
        $this->addColumn('onset_age', 'OnsetAge', 'INTEGER', false, null, null);
        $this->addColumn('abatement_date', 'AbatementDate', 'DATE', false, null, null);
        $this->addColumn('abatement_age', 'AbatementAge', 'INTEGER', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
        $this->addRelation('User', '\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Patient', '\\Patient', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':patient_uuid',
    1 => ':uuid',
  ),
), null, null, null, false);
        $this->addRelation('MedicalDiagnosisCode', '\\MedicalDiagnosisCode', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':medical_diagnosis_code_id',
    1 => ':id',
  ),
), null, null, null, false);
    }

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array<string, array> Associative array (name => parameters) of behaviors
     */
    public function getBehaviors(): array
    {
        return [
            'timestampable' => ['create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false'],
        ];
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? MedicalHistoryTableMap::CLASS_DEFAULT : MedicalHistoryTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (MedicalHistory object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = MedicalHistoryTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = MedicalHistoryTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + MedicalHistoryTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = MedicalHistoryTableMap::OM_CLASS;
            /** @var MedicalHistory $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            MedicalHistoryTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = MedicalHistoryTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = MedicalHistoryTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var MedicalHistory $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                MedicalHistoryTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_UUID);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_PATIENT_UUID);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_USER_ID);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_CLINICAL_STATUS);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_VERIFICATION_STATUS);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_SEVERITY);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_NOTE);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_ONSET_DATE);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_ONSET_AGE);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_ABATEMENT_DATE);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_ABATEMENT_AGE);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(MedicalHistoryTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.patient_uuid');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.clinical_status');
            $criteria->addSelectColumn($alias . '.verification_status');
            $criteria->addSelectColumn($alias . '.severity');
            $criteria->addSelectColumn($alias . '.note');
            $criteria->addSelectColumn($alias . '.medical_diagnosis_code_id');
            $criteria->addSelectColumn($alias . '.onset_date');
            $criteria->addSelectColumn($alias . '.onset_age');
            $criteria->addSelectColumn($alias . '.abatement_date');
            $criteria->addSelectColumn($alias . '.abatement_age');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_UUID);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_PATIENT_UUID);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_USER_ID);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_CLINICAL_STATUS);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_VERIFICATION_STATUS);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_SEVERITY);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_NOTE);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_MEDICAL_DIAGNOSIS_CODE_ID);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_ONSET_DATE);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_ONSET_AGE);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_ABATEMENT_DATE);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_ABATEMENT_AGE);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_CREATED_AT);
            $criteria->removeSelectColumn(MedicalHistoryTableMap::COL_UPDATED_AT);
        } else {
            $criteria->removeSelectColumn($alias . '.uuid');
            $criteria->removeSelectColumn($alias . '.patient_uuid');
            $criteria->removeSelectColumn($alias . '.user_id');
            $criteria->removeSelectColumn($alias . '.clinical_status');
            $criteria->removeSelectColumn($alias . '.verification_status');
            $criteria->removeSelectColumn($alias . '.severity');
            $criteria->removeSelectColumn($alias . '.note');
            $criteria->removeSelectColumn($alias . '.medical_diagnosis_code_id');
            $criteria->removeSelectColumn($alias . '.onset_date');
            $criteria->removeSelectColumn($alias . '.onset_age');
            $criteria->removeSelectColumn($alias . '.abatement_date');
            $criteria->removeSelectColumn($alias . '.abatement_age');
            $criteria->removeSelectColumn($alias . '.created_at');
            $criteria->removeSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(MedicalHistoryTableMap::DATABASE_NAME)->getTable(MedicalHistoryTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a MedicalHistory or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or MedicalHistory object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicalHistoryTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \MedicalHistory) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(MedicalHistoryTableMap::DATABASE_NAME);
            $criteria->add(MedicalHistoryTableMap::COL_UUID, (array) $values, Criteria::IN);
        }

        $query = MedicalHistoryQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            MedicalHistoryTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                MedicalHistoryTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the medical_history table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return MedicalHistoryQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a MedicalHistory or Criteria object.
     *
     * @param mixed $criteria Criteria or MedicalHistory object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MedicalHistoryTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from MedicalHistory object
        }


        // Set the correct dbName
        $query = MedicalHistoryQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
