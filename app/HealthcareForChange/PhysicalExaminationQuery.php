<?php

use Base\PhysicalExaminationQuery as BasePhysicalExaminationQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'physical_examination' table.
 *
 * Contains physical examination history of the respective patient
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class PhysicalExaminationQuery extends BasePhysicalExaminationQuery
{

}
