<?php

use Base\VaccineQuery as BaseVaccineQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'vaccine' table.
 *
 * Table containing the available vaccine types as reference for further usage in the medical and vaccine history
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class VaccineQuery extends BaseVaccineQuery
{

}
