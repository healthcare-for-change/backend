<?php

use Base\VaccineDoseScheduleQuery as BaseVaccineDoseScheduleQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'vaccine_dose_schedule' table.
 *
 * Contains the required dose schedule of this vaccine in order to be able to display recommendation on further vaccinations
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class VaccineDoseScheduleQuery extends BaseVaccineDoseScheduleQuery
{

}
