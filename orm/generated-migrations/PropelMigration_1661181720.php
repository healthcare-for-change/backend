<?php
use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1661181720.
 * Generated on 2022-08-22 15:22:00  
 */
class PropelMigration_1661181720 
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

ALTER TABLE "vaccination_history" DROP CONSTRAINT "vaccination_history_fk_87a1d6";
ALTER TABLE "vaccination_history" RENAME COLUMN "vaccine_name" TO "lot";
ALTER TABLE "vaccination_history"
  ADD "user_id" INTEGER NOT NULL,
  DROP COLUMN "vaccine_dose_schedule_uuid";

ALTER TABLE "vaccination_history" ADD CONSTRAINT "vaccination_history_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id");

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

ALTER TABLE "vaccination_history" DROP CONSTRAINT "vaccination_history_fk_69bd79";
ALTER TABLE "vaccination_history" RENAME COLUMN "lot" TO "vaccine_name";
ALTER TABLE "vaccination_history"
  ADD "vaccine_dose_schedule_uuid" VARCHAR,
  DROP COLUMN "user_id";

ALTER TABLE "vaccination_history" ADD CONSTRAINT "vaccination_history_fk_87a1d6"
    FOREIGN KEY ("vaccine_dose_schedule_uuid")
    REFERENCES "vaccine_dose_schedule" ("uuid");

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

}