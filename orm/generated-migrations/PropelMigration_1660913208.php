<?php
use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1660913208.
 * Generated on 2022-08-19 12:46:48  
 */
class PropelMigration_1660913208 
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

ALTER TABLE "deworming_history"

  ADD "comment" VARCHAR(255);

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

ALTER TABLE "deworming_history"

  DROP COLUMN "comment";

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

}