<?php
use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1660728747.
 * Generated on 2022-08-17 09:32:27  
 */
class PropelMigration_1660728747 
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

CREATE TABLE "patient"
(
    "uuid" uuid NOT NULL,
    "first_name" VARCHAR(255) NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "gender" INT2 NOT NULL,
    "date_of_birth" DATE NOT NULL,
    "date_of_death" DATE,
    "unclear_birthdate" BOOLEAN DEFAULT 'f',
    "mobile_number" VARCHAR(255),
    "tenant_uuid" uuid NOT NULL,
    "religion_id" INTEGER,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("uuid")
);

COMMENT ON TABLE "patient" IS 'Contains patient information';

COMMENT ON COLUMN "patient"."uuid" IS 'Unique ID';

COMMENT ON COLUMN "patient"."first_name" IS 'First name';

COMMENT ON COLUMN "patient"."name" IS 'Family name';

COMMENT ON COLUMN "patient"."gender" IS 'Gender defined as male, female or divers';

COMMENT ON COLUMN "patient"."date_of_birth" IS 'Date of birth';

COMMENT ON COLUMN "patient"."date_of_death" IS 'Recording if patient died';

COMMENT ON COLUMN "patient"."unclear_birthdate" IS 'Set to 1 if birthdate is not clear and the suspected year should only be displayed';

COMMENT ON COLUMN "patient"."mobile_number" IS 'Godians phone number';

COMMENT ON COLUMN "patient"."tenant_uuid" IS 'Reference to tenant';

CREATE INDEX "patient_search" ON "patient" ("first_name","name");

CREATE TABLE "tenant"
(
    "uuid" uuid NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("uuid")
);

COMMENT ON TABLE "tenant" IS 'Core table for holding tenant information';

COMMENT ON COLUMN "tenant"."uuid" IS 'Unique identification';

COMMENT ON COLUMN "tenant"."name" IS 'Visible descriptive mane';

CREATE TABLE "vaccine"
(
    "uuid" uuid NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "description" TEXT,
    "first_vaccination" INTEGER,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("uuid")
);

COMMENT ON TABLE "vaccine" IS 'Table containing the available vaccine types as reference for further usage in the medical and vaccine history';

COMMENT ON COLUMN "vaccine"."uuid" IS 'PK as UUID';

COMMENT ON COLUMN "vaccine"."name" IS 'Publicly visible name of vaccine type';

COMMENT ON COLUMN "vaccine"."description" IS 'Optional long description of the vaccine type';

COMMENT ON COLUMN "vaccine"."first_vaccination" IS 'Recommended first vaccination in month starting from date of birth';

CREATE TABLE "vaccine_dose_schedule"
(
    "uuid" uuid NOT NULL,
    "min_month" INTEGER NOT NULL,
    "max_month" INTEGER,
    "comment" VARCHAR(255),
    "vaccine_uuid" uuid NOT NULL,
    PRIMARY KEY ("uuid")
);

COMMENT ON TABLE "vaccine_dose_schedule" IS 'Contains the required dose schedule of this vaccine in order to be able to display recommendation on further vaccinations';

COMMENT ON COLUMN "vaccine_dose_schedule"."uuid" IS 'PK as UUID';

COMMENT ON COLUMN "vaccine_dose_schedule"."min_month" IS 'Minimum month in which the additional dose shall be vaccinated';

COMMENT ON COLUMN "vaccine_dose_schedule"."max_month" IS 'Maximum month in which the dose shall be vaccinated';

COMMENT ON COLUMN "vaccine_dose_schedule"."comment" IS 'Additional visible comment to vaccination strategy';

CREATE TABLE "vaccination_history"
(
    "uuid" uuid NOT NULL,
    "timestamp" TIMESTAMP NOT NULL,
    "vaccine_name" VARCHAR(255),
    "comment" VARCHAR(255),
    "patient_uuid" uuid NOT NULL,
    "vaccine_uuid" uuid NOT NULL,
    "vaccine_dose_schedule_uuid" uuid,
    PRIMARY KEY ("uuid")
);

COMMENT ON COLUMN "vaccination_history"."uuid" IS 'PK as UUID';

COMMENT ON COLUMN "vaccination_history"."timestamp" IS 'Timestamp of vaccination';

COMMENT ON COLUMN "vaccination_history"."vaccine_name" IS 'Optional name of vaccine';

COMMENT ON COLUMN "vaccination_history"."comment" IS 'Optional comment to be recorded by doctor';

CREATE TABLE "vaccination_plan"
(
    "id" uuid NOT NULL,
    "tenant_uuid" uuid NOT NULL,
    "vaccine_uuid" uuid NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "vaccination_plan" IS 'Vaccination plan based on the tenant configuration. This shall enable the tenent to provide an individual vaccination plan that is shown in the patient datasheet as upcoming vaccinations';

COMMENT ON COLUMN "vaccination_plan"."id" IS 'PK';

CREATE TABLE "pysical_examination"
(
    "uuid" VARCHAR(36) NOT NULL,
    "timestamp" TIMESTAMP NOT NULL,
    "blood_pressure_systolic" INT2,
    "blood_pressure_diastolic" INT2,
    "heart_rate" INT2,
    "weight" NUMERIC(5,2),
    "height" INT2,
    "muac" NUMERIC(5,2),
    "temperature" NUMERIC(5,2),
    "skin_examination" TEXT,
    "details_ent" TEXT,
    "details_eyes" TEXT,
    "details_head" TEXT,
    "details_muscles_bones" TEXT,
    "details_heart" TEXT,
    "details_lung" TEXT,
    "details_gastrointestinal" TEXT,
    "details_urinary_tract" TEXT,
    "details_reproductive_system" TEXT,
    "details_other" TEXT,
    "general_impression" INT2 NOT NULL,
    "patient_uuid" uuid NOT NULL,
    "user_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("uuid")
);

COMMENT ON TABLE "pysical_examination" IS 'Contains physical examination history of the respective patient';

COMMENT ON COLUMN "pysical_examination"."uuid" IS 'PK';

COMMENT ON COLUMN "pysical_examination"."timestamp" IS 'Timestamp of examination';

COMMENT ON COLUMN "pysical_examination"."blood_pressure_systolic" IS 'Blood pressure Systolic value';

COMMENT ON COLUMN "pysical_examination"."blood_pressure_diastolic" IS 'Blood pressure Diastolic value';

COMMENT ON COLUMN "pysical_examination"."heart_rate" IS 'Pulse rate of the patients heart';

COMMENT ON COLUMN "pysical_examination"."weight" IS 'Weight of patient in kg';

COMMENT ON COLUMN "pysical_examination"."height" IS 'Height of patient in cm';

COMMENT ON COLUMN "pysical_examination"."muac" IS 'Patients Mid upper arm circumference';

COMMENT ON COLUMN "pysical_examination"."temperature" IS 'Temperature (fever)';

COMMENT ON COLUMN "pysical_examination"."skin_examination" IS 'Free text for skin examination';

COMMENT ON COLUMN "pysical_examination"."details_ent" IS 'Optional details on E.N.T.';

COMMENT ON COLUMN "pysical_examination"."details_eyes" IS 'Optional details on eyes';

COMMENT ON COLUMN "pysical_examination"."details_head" IS 'Optional details on head and brain';

COMMENT ON COLUMN "pysical_examination"."details_muscles_bones" IS 'Optional details on muscles and bones';

COMMENT ON COLUMN "pysical_examination"."details_heart" IS 'Optional details on heart';

COMMENT ON COLUMN "pysical_examination"."details_lung" IS 'Optional details on lung';

COMMENT ON COLUMN "pysical_examination"."details_gastrointestinal" IS 'Optional details on gastrointestinal';

COMMENT ON COLUMN "pysical_examination"."details_urinary_tract" IS 'Optional details on urinary tract';

COMMENT ON COLUMN "pysical_examination"."details_reproductive_system" IS 'Optional details on reproductive system';

COMMENT ON COLUMN "pysical_examination"."details_other" IS 'Optional details not fitting to other group';

COMMENT ON COLUMN "pysical_examination"."general_impression" IS 'General impression on a scale from 1-5 where 5 ist best';

CREATE TABLE "deworming_history"
(
    "uuid" uuid NOT NULL,
    "timestamp" TIMESTAMP NOT NULL,
    "patient_uuid" uuid NOT NULL,
    "user_id" INTEGER NOT NULL,
    "medication_id" uuid,
    PRIMARY KEY ("uuid")
);

COMMENT ON TABLE "deworming_history" IS 'History of deworming';

COMMENT ON COLUMN "deworming_history"."uuid" IS 'PK';

COMMENT ON COLUMN "deworming_history"."timestamp" IS 'Timestamp of deworming';

COMMENT ON COLUMN "deworming_history"."patient_uuid" IS 'Patient reference';

CREATE TABLE "prescription"
(
    "uuid" uuid NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "description" TEXT,
    "patient_uuid" uuid NOT NULL,
    "user_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("uuid")
);

COMMENT ON TABLE "prescription" IS 'History of prescription';

COMMENT ON COLUMN "prescription"."uuid" IS 'PK';

COMMENT ON COLUMN "prescription"."name" IS 'Name of prescription';

COMMENT ON COLUMN "prescription"."description" IS 'Additional information, eg schedule of dosis etc.';

COMMENT ON COLUMN "prescription"."patient_uuid" IS 'Reference to patient';

CREATE TABLE "religion"
(
    "id" serial NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "impact" TEXT,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "religion" IS 'Table containing religions to be assigned to patients';

COMMENT ON COLUMN "religion"."name" IS 'Name of religion';

COMMENT ON COLUMN "religion"."impact" IS 'Impact on medical therapy or use of vaccines';

CREATE TABLE "medication"
(
    "uuid" uuid NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "code" VARCHAR(255),
    "active" BOOLEAN DEFAULT 'f',
    "medication_dose_form_id" INT8 NOT NULL,
    "medication_marketing_authorization_holder_id" INTEGER,
    "strength" VARCHAR(255),
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("uuid")
);

COMMENT ON COLUMN "medication"."code" IS 'Codes that identify this medication e.g. SNOMED CT Medication Codes';

COMMENT ON COLUMN "medication"."active" IS 'Identifies if this record is currently active';

COMMENT ON COLUMN "medication"."strength" IS 'Optional information to strength';

CREATE TABLE "medication_dose_form"
(
    "id" INT8 NOT NULL,
    "description" VARCHAR(255) NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "medication_dose_form"."id" IS 'SNOMED dose code as of https://build.fhir.org/valueset-medication-form-codes.html';

COMMENT ON COLUMN "medication_dose_form"."description" IS 'Plain text description';

CREATE TABLE "medication_marketing_authorization_holder"
(
    "id" serial NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "medication_marketing_authorization_holder" IS 'Contains the manufacturer information of a medication';

CREATE TABLE "medication_administration_method"
(
    "id" bigserial NOT NULL,
    "description" VARCHAR(255) NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "medication_administration_method"."description" IS 'SNOMED administration method code as of http://www.hl7.org/fhir/valueset-administration-method-codes.html';

CREATE TABLE "medication_dose_instruction"
(
    "id" bigserial NOT NULL,
    "description" VARCHAR(255) NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "medication_dose_instruction"."description" IS 'SNOMED code as of http://www.hl7.org/fhir/valueset-additional-instruction-codes.html';

CREATE TABLE "medical_history"
(
    "uuid" uuid NOT NULL,
    "patient_uuid" uuid NOT NULL,
    "users_id" INTEGER NOT NULL,
    "clinical_status" INT2,
    "verification_status" INT2,
    "severity" INT2,
    "note" TEXT,
    "medical_diagnosis_code_id" INT8,
    "onset_date" DATE,
    "onset_age" INTEGER,
    "abatement_date" DATE,
    "abatement_age" INTEGER,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("uuid")
);

COMMENT ON COLUMN "medical_history"."clinical_status" IS '1 active | 2 inactive | 3 resolved';

COMMENT ON COLUMN "medical_history"."verification_status" IS '1 unconfirmed | 2 provisional | 3 refuted';

COMMENT ON COLUMN "medical_history"."severity" IS '1 severe | 2 moderate | 3 mild';

COMMENT ON COLUMN "medical_history"."onset_date" IS 'Estimated or actual date';

COMMENT ON COLUMN "medical_history"."onset_age" IS 'Estimated or actual age';

COMMENT ON COLUMN "medical_history"."abatement_date" IS 'When in resolution/remission';

COMMENT ON COLUMN "medical_history"."abatement_age" IS 'When in resolution/remission';

CREATE TABLE "prescription_has_medication"
(
    "uuid" uuid NOT NULL,
    "medication_uuid" uuid NOT NULL,
    "prescription_uuid" uuid NOT NULL,
    "dose_quantity" VARCHAR(50) NOT NULL,
    "dose_morning" BOOLEAN DEFAULT 'f',
    "dose_noon" BOOLEAN DEFAULT 'f',
    "dose_evening" BOOLEAN DEFAULT 'f',
    "dose_night" BOOLEAN DEFAULT 'f',
    "max_dose_period" VARCHAR(50),
    "patient_instruction" VARCHAR(255),
    "medication_administration_method_id" INT8,
    "start" DATE,
    "end" DATE,
    "interval" INT2,
    PRIMARY KEY ("uuid")
);

COMMENT ON COLUMN "prescription_has_medication"."max_dose_period" IS 'Defines the max dosis per period';

COMMENT ON COLUMN "prescription_has_medication"."patient_instruction" IS 'Clear text instruction for patient';

COMMENT ON COLUMN "prescription_has_medication"."start" IS 'Startdate of medication';

COMMENT ON COLUMN "prescription_has_medication"."end" IS 'Enddate of medication';

COMMENT ON COLUMN "prescription_has_medication"."interval" IS 'Interval in days';

CREATE TABLE "medical_diagnosis_code"
(
    "id" bigserial NOT NULL,
    "description" VARCHAR(255) NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "medical_diagnosis_code" IS 'Contains the diagnosis code as of http://www.hl7.org/fhir/valueset-condition-code.html';

CREATE TABLE "prescription_dose_instruction"
(
    "medication_dose_instruction_id" INT8 NOT NULL,
    "prescription_has_medication_uuid" uuid NOT NULL,
    "quantifier_1" VARCHAR(255),
    "quantifier_2" VARCHAR(255),
    PRIMARY KEY ("medication_dose_instruction_id","prescription_has_medication_uuid")
);

CREATE TABLE "user_has_tenant"
(
    "tenant_uuid" uuid NOT NULL,
    "user_id" INTEGER NOT NULL,
    PRIMARY KEY ("tenant_uuid","user_id")
);

CREATE TABLE "users"
(
    "id" serial NOT NULL,
    "username" VARCHAR(255),
    "display_name" VARCHAR(255),
    "status" VARCHAR(255),
    "status_message" VARCHAR(255),
    "active" INT2 DEFAULT 0,
    "last_active" TIMESTAMP,
    "deleted_at" TIMESTAMP,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_username" UNIQUE ("username")
);

CREATE TABLE "auth_identities"
(
    "id" serial NOT NULL,
    "user_id" INTEGER NOT NULL,
    "type" VARCHAR(255) NOT NULL,
    "name" VARCHAR(255),
    "secret" VARCHAR(255) NOT NULL,
    "secret2" VARCHAR(255),
    "expires" TIMESTAMP,
    "extra" TEXT,
    "force_reset" INT2 DEFAULT 0,
    "last_used_at" TIMESTAMP,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("id")
);

CREATE INDEX "type_secret" ON "auth_identities" ("type","secret");

CREATE TABLE "auth_logins"
(
    "id" serial NOT NULL,
    "user_id" INTEGER,
    "ip_address" VARCHAR(255),
    "user_agent" VARCHAR(255),
    "id_type" VARCHAR(255),
    "identifier" VARCHAR(255),
    "date" TIMESTAMP NOT NULL,
    "success" INT2 DEFAULT 0,
    PRIMARY KEY ("id")
);

CREATE INDEX "type_identifier" ON "auth_logins" ("id_type","identifier");

CREATE TABLE "auth_token_logins"
(
    "id" serial NOT NULL,
    "ip_address" VARCHAR(255),
    "user_agent" VARCHAR(255),
    "id_type" VARCHAR(255),
    "identifier" VARCHAR(255),
    "date" TIMESTAMP NOT NULL,
    "success" INT2 DEFAULT 0,
    "user_id" INTEGER,
    PRIMARY KEY ("id")
);

CREATE TABLE "auth_remember_tokens"
(
    "id" serial NOT NULL,
    "selector" VARCHAR(255) NOT NULL,
    "hashedValidator" VARCHAR(255) NOT NULL,
    "expires" TIMESTAMP NOT NULL,
    "user_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("id"),
    CONSTRAINT "selector" UNIQUE ("selector")
);

CREATE TABLE "auth_groups_users"
(
    "id" serial NOT NULL,
    "user_id" INTEGER NOT NULL,
    "group" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("id")
);

CREATE TABLE "auth_permissions_users"
(
    "id" serial NOT NULL,
    "permissions" VARCHAR(255) NOT NULL,
    "user_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("id")
);

CREATE TABLE "settings"
(
    "id" serial NOT NULL,
    "type" VARCHAR(255) NOT NULL,
    "key" VARCHAR(255) NOT NULL,
    "value" TEXT,
    "context" VARCHAR(255),
    "class" VARCHAR(255),
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "settings" IS 'CodeIgniter Settings Table';

CREATE INDEX "search" ON "settings" ("type","key");

ALTER TABLE "patient" ADD CONSTRAINT "patient_fk_43bb84"
    FOREIGN KEY ("tenant_uuid")
    REFERENCES "tenant" ("uuid");

ALTER TABLE "patient" ADD CONSTRAINT "patient_fk_d6a601"
    FOREIGN KEY ("religion_id")
    REFERENCES "religion" ("id");

ALTER TABLE "vaccine_dose_schedule" ADD CONSTRAINT "vaccine_dose_schedule_fk_e22a39"
    FOREIGN KEY ("vaccine_uuid")
    REFERENCES "vaccine" ("uuid");

ALTER TABLE "vaccination_history" ADD CONSTRAINT "vaccination_history_fk_466e46"
    FOREIGN KEY ("patient_uuid")
    REFERENCES "patient" ("uuid");

ALTER TABLE "vaccination_history" ADD CONSTRAINT "vaccination_history_fk_e22a39"
    FOREIGN KEY ("vaccine_uuid")
    REFERENCES "vaccine" ("uuid");

ALTER TABLE "vaccination_history" ADD CONSTRAINT "vaccination_history_fk_87a1d6"
    FOREIGN KEY ("vaccine_dose_schedule_uuid")
    REFERENCES "vaccine_dose_schedule" ("uuid");

ALTER TABLE "vaccination_plan" ADD CONSTRAINT "vaccination_plan_fk_43bb84"
    FOREIGN KEY ("tenant_uuid")
    REFERENCES "tenant" ("uuid");

ALTER TABLE "vaccination_plan" ADD CONSTRAINT "vaccination_plan_fk_e22a39"
    FOREIGN KEY ("vaccine_uuid")
    REFERENCES "vaccine" ("uuid");

ALTER TABLE "pysical_examination" ADD CONSTRAINT "pysical_examination_fk_466e46"
    FOREIGN KEY ("patient_uuid")
    REFERENCES "patient" ("uuid");

ALTER TABLE "pysical_examination" ADD CONSTRAINT "pysical_examination_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id");

ALTER TABLE "deworming_history" ADD CONSTRAINT "deworming_history_fk_466e46"
    FOREIGN KEY ("patient_uuid")
    REFERENCES "patient" ("uuid");

ALTER TABLE "deworming_history" ADD CONSTRAINT "deworming_history_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id");

ALTER TABLE "deworming_history" ADD CONSTRAINT "deworming_history_fk_fa1780"
    FOREIGN KEY ("medication_id")
    REFERENCES "medication" ("uuid");

ALTER TABLE "prescription" ADD CONSTRAINT "prescription_fk_466e46"
    FOREIGN KEY ("patient_uuid")
    REFERENCES "patient" ("uuid");

ALTER TABLE "prescription" ADD CONSTRAINT "prescription_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id");

ALTER TABLE "medication" ADD CONSTRAINT "medication_fk_4e481b"
    FOREIGN KEY ("medication_dose_form_id")
    REFERENCES "medication_dose_form" ("id");

ALTER TABLE "medication" ADD CONSTRAINT "medication_fk_10a6de"
    FOREIGN KEY ("medication_marketing_authorization_holder_id")
    REFERENCES "medication_marketing_authorization_holder" ("id");

ALTER TABLE "medical_history" ADD CONSTRAINT "medical_history_fk_935655"
    FOREIGN KEY ("users_id")
    REFERENCES "users" ("id");

ALTER TABLE "medical_history" ADD CONSTRAINT "medical_history_fk_466e46"
    FOREIGN KEY ("patient_uuid")
    REFERENCES "patient" ("uuid");

ALTER TABLE "medical_history" ADD CONSTRAINT "medical_history_fk_8f1766"
    FOREIGN KEY ("medical_diagnosis_code_id")
    REFERENCES "medical_diagnosis_code" ("id");

ALTER TABLE "prescription_has_medication" ADD CONSTRAINT "prescription_has_medication_fk_1df5b9"
    FOREIGN KEY ("medication_uuid")
    REFERENCES "medication" ("uuid");

ALTER TABLE "prescription_has_medication" ADD CONSTRAINT "prescription_has_medication_fk_924420"
    FOREIGN KEY ("prescription_uuid")
    REFERENCES "prescription" ("uuid")
    ON DELETE CASCADE;

ALTER TABLE "prescription_has_medication" ADD CONSTRAINT "prescription_has_medication_fk_5c05b1"
    FOREIGN KEY ("medication_administration_method_id")
    REFERENCES "medication_administration_method" ("id");

ALTER TABLE "prescription_dose_instruction" ADD CONSTRAINT "prescription_dose_instruction_fk_0e49bc"
    FOREIGN KEY ("medication_dose_instruction_id")
    REFERENCES "medication_dose_instruction" ("id");

ALTER TABLE "prescription_dose_instruction" ADD CONSTRAINT "prescription_dose_instruction_fk_04958c"
    FOREIGN KEY ("prescription_has_medication_uuid")
    REFERENCES "prescription_has_medication" ("uuid");

ALTER TABLE "user_has_tenant" ADD CONSTRAINT "user_has_tenant_fk_43bb84"
    FOREIGN KEY ("tenant_uuid")
    REFERENCES "tenant" ("uuid");

ALTER TABLE "user_has_tenant" ADD CONSTRAINT "user_has_tenant_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id");

ALTER TABLE "auth_identities" ADD CONSTRAINT "auth_identities_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id")
    ON DELETE CASCADE;

ALTER TABLE "auth_logins" ADD CONSTRAINT "auth_logins_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id")
    ON DELETE CASCADE;

ALTER TABLE "auth_token_logins" ADD CONSTRAINT "auth_token_logins_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id")
    ON DELETE CASCADE;

ALTER TABLE "auth_remember_tokens" ADD CONSTRAINT "auth_remember_tokens_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id")
    ON DELETE CASCADE;

ALTER TABLE "auth_groups_users" ADD CONSTRAINT "auth_groups_users_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id")
    ON DELETE CASCADE;

ALTER TABLE "auth_permissions_users" ADD CONSTRAINT "auth_permissions_users_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id")
    ON DELETE CASCADE;

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

DROP TABLE IF EXISTS "patient" CASCADE;

DROP TABLE IF EXISTS "tenant" CASCADE;

DROP TABLE IF EXISTS "vaccine" CASCADE;

DROP TABLE IF EXISTS "vaccine_dose_schedule" CASCADE;

DROP TABLE IF EXISTS "vaccination_history" CASCADE;

DROP TABLE IF EXISTS "vaccination_plan" CASCADE;

DROP TABLE IF EXISTS "pysical_examination" CASCADE;

DROP TABLE IF EXISTS "deworming_history" CASCADE;

DROP TABLE IF EXISTS "prescription" CASCADE;

DROP TABLE IF EXISTS "religion" CASCADE;

DROP TABLE IF EXISTS "medication" CASCADE;

DROP TABLE IF EXISTS "medication_dose_form" CASCADE;

DROP TABLE IF EXISTS "medication_marketing_authorization_holder" CASCADE;

DROP TABLE IF EXISTS "medication_administration_method" CASCADE;

DROP TABLE IF EXISTS "medication_dose_instruction" CASCADE;

DROP TABLE IF EXISTS "medical_history" CASCADE;

DROP TABLE IF EXISTS "prescription_has_medication" CASCADE;

DROP TABLE IF EXISTS "medical_diagnosis_code" CASCADE;

DROP TABLE IF EXISTS "prescription_dose_instruction" CASCADE;

DROP TABLE IF EXISTS "user_has_tenant" CASCADE;

DROP TABLE IF EXISTS "users" CASCADE;

DROP TABLE IF EXISTS "auth_identities" CASCADE;

DROP TABLE IF EXISTS "auth_logins" CASCADE;

DROP TABLE IF EXISTS "auth_token_logins" CASCADE;

DROP TABLE IF EXISTS "auth_remember_tokens" CASCADE;

DROP TABLE IF EXISTS "auth_groups_users" CASCADE;

DROP TABLE IF EXISTS "auth_permissions_users" CASCADE;

DROP TABLE IF EXISTS "settings" CASCADE;

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

}