<?php
use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1662401699.
 * Generated on 2022-09-05 18:14:59  
 */
class PropelMigration_1662401699 
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

ALTER TABLE "prescription" RENAME COLUMN "start" TO "dose_start";
ALTER TABLE "prescription" RENAME COLUMN "end" TO "dose_end";

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

ALTER TABLE "prescription" RENAME COLUMN "dose_start" TO "start";
ALTER TABLE "prescription" RENAME COLUMN "dose_end" TO "end";

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

}