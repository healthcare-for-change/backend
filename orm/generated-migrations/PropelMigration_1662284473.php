<?php
use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1662284473.
 * Generated on 2022-09-04 09:41:13  
 */
class PropelMigration_1662284473 
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

DROP TABLE IF EXISTS "pysical_examination" CASCADE;

CREATE TABLE "physical_examination"
(
    "uuid" VARCHAR(36) NOT NULL,
    "timestamp" TIMESTAMP NOT NULL,
    "blood_pressure_systolic" INT2,
    "blood_pressure_diastolic" INT2,
    "heart_rate" INT2,
    "weight" NUMERIC(5,2),
    "height" INT2,
    "muac" NUMERIC(5,2),
    "temperature" NUMERIC(5,2),
    "skin_examination" TEXT,
    "details_ent" TEXT,
    "details_eyes" TEXT,
    "details_head" TEXT,
    "details_muscles_bones" TEXT,
    "details_heart" TEXT,
    "details_lung" TEXT,
    "details_gastrointestinal" TEXT,
    "details_urinary_tract" TEXT,
    "details_reproductive_system" TEXT,
    "details_other" TEXT,
    "general_impression" INT2 NOT NULL,
    "patient_uuid" uuid NOT NULL,
    "user_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("uuid")
);

COMMENT ON TABLE "physical_examination" IS 'Contains physical examination history of the respective patient';

COMMENT ON COLUMN "physical_examination"."uuid" IS 'PK';

COMMENT ON COLUMN "physical_examination"."timestamp" IS 'Timestamp of examination';

COMMENT ON COLUMN "physical_examination"."blood_pressure_systolic" IS 'Blood pressure Systolic value';

COMMENT ON COLUMN "physical_examination"."blood_pressure_diastolic" IS 'Blood pressure Diastolic value';

COMMENT ON COLUMN "physical_examination"."heart_rate" IS 'Pulse rate of the patients heart';

COMMENT ON COLUMN "physical_examination"."weight" IS 'Weight of patient in kg';

COMMENT ON COLUMN "physical_examination"."height" IS 'Height of patient in cm';

COMMENT ON COLUMN "physical_examination"."muac" IS 'Patients Mid upper arm circumference';

COMMENT ON COLUMN "physical_examination"."temperature" IS 'Temperature (fever)';

COMMENT ON COLUMN "physical_examination"."skin_examination" IS 'Free text for skin examination';

COMMENT ON COLUMN "physical_examination"."details_ent" IS 'Optional details on E.N.T.';

COMMENT ON COLUMN "physical_examination"."details_eyes" IS 'Optional details on eyes';

COMMENT ON COLUMN "physical_examination"."details_head" IS 'Optional details on head and brain';

COMMENT ON COLUMN "physical_examination"."details_muscles_bones" IS 'Optional details on muscles and bones';

COMMENT ON COLUMN "physical_examination"."details_heart" IS 'Optional details on heart';

COMMENT ON COLUMN "physical_examination"."details_lung" IS 'Optional details on lung';

COMMENT ON COLUMN "physical_examination"."details_gastrointestinal" IS 'Optional details on gastrointestinal';

COMMENT ON COLUMN "physical_examination"."details_urinary_tract" IS 'Optional details on urinary tract';

COMMENT ON COLUMN "physical_examination"."details_reproductive_system" IS 'Optional details on reproductive system';

COMMENT ON COLUMN "physical_examination"."details_other" IS 'Optional details not fitting to other group';

COMMENT ON COLUMN "physical_examination"."general_impression" IS 'General impression on a scale from 1-5 where 5 ist best';

ALTER TABLE "physical_examination" ADD CONSTRAINT "physical_examination_fk_466e46"
    FOREIGN KEY ("patient_uuid")
    REFERENCES "patient" ("uuid");

ALTER TABLE "physical_examination" ADD CONSTRAINT "physical_examination_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id");

INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('463827a6-c21e-4a6b-9cef-a780398eab01', 'Tuberculosis', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('6ce6d6f9-42aa-421c-90e6-ae33e9d7ecf3', 'Polio', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('aa16e487-4edb-420b-ae46-2fd930ca02bf', 'Diphtherie', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('9eb1c318-ca45-4b99-b348-0e859a9ee15c', 'Pertussis', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('b6626caa-fb54-4487-9441-677a891b768e', 'Tetanus', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('561ed67e-a502-4f4c-ad1e-18a2bdd77636', 'Hepatitis B', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('f45080d5-306f-46e7-85ce-aa1b24fa15a3', 'H. influenza', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('5f8a11f0-20e2-4c5d-9f87-4350c6582179', 'Pneumoccal disease ', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('548169af-eb19-499c-b21d-df2faefa3e4d', 'Meningococcal disease', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('347069e6-0037-478e-a715-6f4e4a7d4ec2', 'Rota Virus', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('3dfcb894-235d-4eac-b3bc-bd7f58c0c6d3', 'Measles, Mumps, Rubella', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('1e0cd4f5-eb75-4d2b-928c-7c52f1ff71f9', 'Yellow Fever', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('700d2708-c709-4f6b-9caa-c189111448a3', 'Shingles', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('9ea7aad8-9ae9-4e3c-9797-44f1a46fdf79', 'Cholera', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('2f97b992-0bb9-4efe-9972-4e2d25123205', 'Rabies', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');
INSERT INTO public.vaccine (uuid, name, description, first_vaccination, created_at, updated_at) VALUES ('cb95e5db-88e0-42a0-b6bd-9c9efb5474a0', 'Typhoid', null, null, '2022-09-05 11:20:45.000000', '2022-09-05 11:20:45.000000');


COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

DROP TABLE IF EXISTS "physical_examination" CASCADE;

CREATE TABLE "pysical_examination"
(
    "uuid" VARCHAR(36) NOT NULL,
    "timestamp" TIMESTAMP NOT NULL,
    "blood_pressure_systolic" INT2,
    "blood_pressure_diastolic" INT2,
    "heart_rate" INT2,
    "weight" NUMERIC(5,2),
    "height" INT2,
    "muac" NUMERIC(5,2),
    "temperature" NUMERIC(5,2),
    "skin_examination" TEXT,
    "details_ent" TEXT,
    "details_eyes" TEXT,
    "details_head" TEXT,
    "details_muscles_bones" TEXT,
    "details_heart" TEXT,
    "details_lung" TEXT,
    "details_gastrointestinal" TEXT,
    "details_urinary_tract" TEXT,
    "details_reproductive_system" TEXT,
    "details_other" TEXT,
    "general_impression" INT2 NOT NULL,
    "patient_uuid" VARCHAR NOT NULL,
    "user_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    PRIMARY KEY ("uuid")
);

ALTER TABLE "pysical_examination" ADD CONSTRAINT "pysical_examination_fk_466e46"
    FOREIGN KEY ("patient_uuid")
    REFERENCES "patient" ("uuid");

ALTER TABLE "pysical_examination" ADD CONSTRAINT "pysical_examination_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id");

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

}