<?php
use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1662387774.
 * Generated on 2022-09-05 14:22:54  
 */
class PropelMigration_1662387774 
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

DROP TABLE IF EXISTS "prescription_has_medication" CASCADE;

ALTER TABLE "prescription"

  ALTER COLUMN "uuid" TYPE uuid USING NULL,
  ALTER COLUMN "patient_uuid" TYPE uuid USING NULL,
  ADD "medication_uuid" uuid NOT NULL,
  ADD "dose_quantity" VARCHAR(50),
  ADD "dose_morning" BOOLEAN DEFAULT 'f',
  ADD "dose_noon" BOOLEAN DEFAULT 'f',
  ADD "dose_evening" BOOLEAN DEFAULT 'f',
  ADD "dose_night" BOOLEAN DEFAULT 'f',
  ADD "max_dose_period" VARCHAR(50),
  ADD "patient_instruction" VARCHAR(255),
  ADD "medication_administration_method_id" INT8,
  ADD "start" DATE,
  ADD "end" DATE,
  ADD "interval" INT2,
  DROP COLUMN "name",
  DROP COLUMN "description",
  DROP COLUMN "created_at",
  DROP COLUMN "updated_at";

ALTER TABLE "prescription" ADD CONSTRAINT "prescription_fk_5c05b1"
    FOREIGN KEY ("medication_administration_method_id")
    REFERENCES "medication_administration_method" ("id");

ALTER TABLE "prescription" ADD CONSTRAINT "prescription_fk_1df5b9"
    FOREIGN KEY ("medication_uuid")
    REFERENCES "medication" ("uuid");

ALTER TABLE "prescription_dose_instruction"
  DROP CONSTRAINT "prescription_dose_instruction_pkey",
  ADD "prescription_uuid" uuid NOT NULL,
  DROP COLUMN "prescription_has_medication_uuid",
  ADD PRIMARY KEY ("medication_dose_instruction_id","prescription_uuid");

ALTER TABLE "prescription_dose_instruction" ADD CONSTRAINT "prescription_dose_instruction_fk_924420"
    FOREIGN KEY ("prescription_uuid")
    REFERENCES "prescription" ("uuid");

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

CREATE TABLE "prescription_has_medication"
(
    "uuid" VARCHAR NOT NULL,
    "medication_uuid" VARCHAR NOT NULL,
    "prescription_uuid" VARCHAR NOT NULL,
    "dose_quantity" VARCHAR(50) NOT NULL,
    "dose_morning" BOOLEAN DEFAULT 'f',
    "dose_noon" BOOLEAN DEFAULT 'f',
    "dose_evening" BOOLEAN DEFAULT 'f',
    "dose_night" BOOLEAN DEFAULT 'f',
    "max_dose_period" VARCHAR(50),
    "patient_instruction" VARCHAR(255),
    "medication_administration_method_id" INT8,
    "start" DATE,
    "end" DATE,
    "interval" INT2,
    PRIMARY KEY ("uuid")
);

ALTER TABLE "prescription" DROP CONSTRAINT "prescription_fk_5c05b1";

ALTER TABLE "prescription" DROP CONSTRAINT "prescription_fk_1df5b9";

ALTER TABLE "prescription"
  ALTER COLUMN "uuid" TYPE VARCHAR USING NULL,
  ALTER COLUMN "patient_uuid" TYPE VARCHAR USING NULL,
  ADD "name" VARCHAR(255) NOT NULL,
  ADD "description" TEXT,
  ADD "created_at" TIMESTAMP,
  ADD "updated_at" TIMESTAMP,
  DROP COLUMN "medication_uuid",
  DROP COLUMN "dose_quantity",
  DROP COLUMN "dose_morning",
  DROP COLUMN "dose_noon",
  DROP COLUMN "dose_evening",
  DROP COLUMN "dose_night",
  DROP COLUMN "max_dose_period",
  DROP COLUMN "patient_instruction",
  DROP COLUMN "medication_administration_method_id",
  DROP COLUMN "start",
  DROP COLUMN "end",
  DROP COLUMN "interval";

ALTER TABLE "prescription_dose_instruction" DROP CONSTRAINT "prescription_dose_instruction_fk_924420";

ALTER TABLE "prescription_dose_instruction"
  DROP CONSTRAINT "prescription_dose_instruction_pkey",
  ADD "prescription_has_medication_uuid" VARCHAR NOT NULL,
  DROP COLUMN "prescription_uuid",
  ADD PRIMARY KEY ("medication_dose_instruction_id","prescription_has_medication_uuid");

ALTER TABLE "prescription_dose_instruction" ADD CONSTRAINT "prescription_dose_instruction_fk_04958c"
    FOREIGN KEY ("prescription_has_medication_uuid")
    REFERENCES "prescription_has_medication" ("uuid");

ALTER TABLE "prescription_has_medication" ADD CONSTRAINT "prescription_has_medication_fk_1df5b9"
    FOREIGN KEY ("medication_uuid")
    REFERENCES "medication" ("uuid");

ALTER TABLE "prescription_has_medication" ADD CONSTRAINT "prescription_has_medication_fk_5c05b1"
    FOREIGN KEY ("medication_administration_method_id")
    REFERENCES "medication_administration_method" ("id");

ALTER TABLE "prescription_has_medication" ADD CONSTRAINT "prescription_has_medication_fk_924420"
    FOREIGN KEY ("prescription_uuid")
    REFERENCES "prescription" ("uuid")
    ON DELETE CASCADE;

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

}