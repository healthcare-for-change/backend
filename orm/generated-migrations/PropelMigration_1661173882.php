<?php
use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1661173882.
 * Generated on 2022-08-22 13:11:22  
 */
class PropelMigration_1661173882 
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;


ALTER TABLE "medical_history" DROP CONSTRAINT "medical_history_fk_935655";
ALTER TABLE "medical_history" RENAME COLUMN "users_id" TO "user_id";
ALTER TABLE "medical_history" ADD CONSTRAINT "medical_history_fk_69bd79"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id");

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        $connection_HealtchareForChange = <<< 'EOT'

BEGIN;

ALTER TABLE "medical_history" DROP CONSTRAINT "medical_history_fk_69bd79";
ALTER TABLE "medical_history" RENAME COLUMN "user_id" TO "users_id";
ALTER TABLE "medical_history" ADD CONSTRAINT "medical_history_fk_935655"
    FOREIGN KEY ("users_id")
    REFERENCES "users" ("id");

COMMIT;
EOT;

        return array(
            'HealtchareForChange' => $connection_HealtchareForChange,
        );
    }

}