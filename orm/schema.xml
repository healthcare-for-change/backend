<?xml version="1.0"?>
<database name="HealtchareForChange">
  <table name="patient" description="Contains patient information">
    <column name="uuid" description="Unique ID" type="Varchar" size="36" required="true" primaryKey="true" sqlType="uuid"/>
    <column name="first_name" description="First name" type="Varchar" size="255" required="true"/>
    <column name="name" description="Family name" type="Varchar" size="255" required="true"/>
    <column name="gender" description="Gender defined as male, female or divers" type="Varchar" size="1" required="true" valueSet="m,f,d"/>
    <column name="date_of_birth" description="Date of birth" type="Date" required="true"/>
    <column name="date_of_death" description="Recording if patient died" type="Date" default="NULL"/>
    <column name="unclear_birthdate" description="Set to 1 if birthdate is not clear and the suspected year should only be displayed" type="Boolean" default="0"/>
    <column name="mobile_number" description="Godians phone number" type="Varchar" size="255" default="NULL"/>
    <column name="tenant_uuid" description="Reference to tenant" type="Varchar" size="36" required="true" sqlType="uuid"/>
    <column name="religion_id" type="Integer"/>
    <index name="patient_search">
      <index-column name="first_name"/>
      <index-column name="name"/>
    </index>
    <foreign-key foreignTable="tenant">
      <reference foreign="uuid" local="tenant_uuid"/>
    </foreign-key>
    <foreign-key foreignTable="religion">
      <reference foreign="id" local="religion_id"/>
    </foreign-key>
    <behavior name="timestampable"/>
  </table>
  <table name="tenant" description="Core table for holding tenant information">
    <column name="uuid" description="Unique identification" type="Varchar" size="36" required="true" primaryKey="true" sqlType="uuid"/>
    <column name="name" description="Visible descriptive mane" type="Varchar" size="255" required="true"/>
    <behavior name="timestampable"/>
  </table>
  <table name="vaccine" description="Table containing the available vaccine types as reference for further usage in the medical and vaccine history">
    <column name="uuid" description="PK as UUID" type="Varchar" size="36" required="true" primaryKey="true" sqlType="uuid"/>
    <column name="name" description="Publicly visible name of vaccine type" type="Varchar" size="255" required="true"/>
    <column name="description" description="Optional long description of the vaccine type" type="LongVarchar" default="NULL"/>
    <column name="first_vaccination" description="Recommended first vaccination in month starting from date of birth" type="Integer"/>
    <behavior name="timestampable"/>
  </table>
  <table name="vaccine_dose_schedule" description="Contains the required dose schedule of this vaccine in order to be able to display recommendation on further vaccinations">
    <column name="uuid" description="PK as UUID" type="Varchar" size="36" required="true" primaryKey="true" sqlType="uuid"/>
    <column name="min_month" description="Minimum month in which the additional dose shall be vaccinated" type="Integer" required="true"/>
    <column name="max_month" description="Maximum month in which the dose shall be vaccinated" type="Integer" default="NULL"/>
    <column name="comment" description="Additional visible comment to vaccination strategy" type="Varchar" size="255" default="NULL"/>
    <column name="vaccine_uuid" type="Varchar" size="36" required="true" sqlType="uuid"/>
    <foreign-key foreignTable="vaccine">
      <reference foreign="uuid" local="vaccine_uuid"/>
    </foreign-key>
  </table>
  <table name="vaccination_history">
    <column name="uuid" description="PK as UUID" type="Varchar" size="36" required="true" primaryKey="true" sqlType="uuid"/>
    <column name="timestamp" description="Timestamp of vaccination" type="Timestamp" required="true"/>
    <column name="comment" description="Optional comment to be recorded by doctor" type="Varchar" size="255" default="NULL"/>
    <column name="patient_uuid" type="Varchar" size="36" required="true" sqlType="uuid"/>
    <column name="vaccine_uuid" type="Varchar" size="36" required="true" sqlType="uuid"/>
    <column name="user_id" type="Integer" required="true"/>
    <column name="lot" description="Optional LOT Number" type="Varchar" size="255" default="NULL"/>
    <foreign-key foreignTable="patient">
      <reference foreign="uuid" local="patient_uuid"/>
    </foreign-key>
    <foreign-key foreignTable="vaccine">
      <reference foreign="uuid" local="vaccine_uuid"/>
    </foreign-key>
    <foreign-key foreignTable="users">
      <reference foreign="id" local="user_id"/>
    </foreign-key>
  </table>
  <table name="vaccination_plan" description="Vaccination plan based on the tenant configuration. This shall enable the tenent to provide an individual vaccination plan that is shown in the patient datasheet as upcoming vaccinations">
    <column name="id" description="PK" type="Varchar" size="36" required="true" primaryKey="true" sqlType="uuid"/>
    <column name="tenant_uuid" type="Varchar" size="36" required="true" sqlType="uuid"/>
    <column name="vaccine_uuid" type="Varchar" size="36" required="true" sqlType="uuid"/>
    <foreign-key foreignTable="tenant">
      <reference foreign="uuid" local="tenant_uuid"/>
    </foreign-key>
    <foreign-key foreignTable="vaccine">
      <reference foreign="uuid" local="vaccine_uuid"/>
    </foreign-key>
  </table>
  <table name="physical_examination" description="Contains physical examination history of the respective patient">
    <column name="uuid" description="PK" type="Varchar" size="36" required="true" primaryKey="true"/>
    <column name="timestamp" description="Timestamp of examination" type="Timestamp" required="true"/>
    <column name="blood_pressure_systolic" description="Blood pressure Systolic value" type="TinyInt" default="NULL"/>
    <column name="blood_pressure_diastolic" description="Blood pressure Diastolic value" type="TinyInt" default="NULL"/>
    <column name="heart_rate" description="Pulse rate of the patients heart" type="TinyInt" default="NULL"/>
    <column name="weight" description="Weight of patient in kg" type="Decimal" size="5" default="NULL" scale="2"/>
    <column name="height" description="Height of patient in cm" type="TinyInt" default="NULL" scale="2"/>
    <column name="muac" description="Patients Mid upper arm circumference" type="Decimal" size="5" default="NULL" scale="2"/>
    <column name="temperature" description="Temperature (fever)" type="Decimal" size="5" default="NULL" scale="2"/>
    <column name="skin_examination" description="Free text for skin examination" type="LongVarchar" default="NULL"/>
    <column name="details_ent" description="Optional details on E.N.T." type="LongVarchar" default="NULL"/>
    <column name="details_eyes" description="Optional details on eyes" type="LongVarchar" default="NULL"/>
    <column name="details_head" description="Optional details on head and brain" type="LongVarchar" default="NULL"/>
    <column name="details_muscles_bones" description="Optional details on muscles and bones" type="LongVarchar" default="NULL"/>
    <column name="details_heart" description="Optional details on heart" type="LongVarchar" default="NULL"/>
    <column name="details_lung" description="Optional details on lung" type="LongVarchar" default="NULL"/>
    <column name="details_gastrointestinal" description="Optional details on gastrointestinal" type="LongVarchar" default="NULL"/>
    <column name="details_urinary_tract" description="Optional details on urinary tract" type="LongVarchar" default="NULL"/>
    <column name="details_reproductive_system" description="Optional details on reproductive system" type="LongVarchar" default="NULL"/>
    <column name="details_other" description="Optional details not fitting to other group" type="LongVarchar" default="NULL"/>
    <column name="general_impression" description="General impression on a scale from 1-5 where 5 ist best" type="TinyInt" default="NULL" required="true"/>
    <column name="patient_uuid" type="Varchar" size="36" required="true" sqlType="uuid"/>
    <column name="user_id" type="Integer" required="true"/>
    <foreign-key foreignTable="patient">
      <reference foreign="uuid" local="patient_uuid"/>
    </foreign-key>
    <foreign-key foreignTable="users">
      <reference foreign="id" local="user_id"/>
    </foreign-key>
    <behavior name="timestampable"/>
  </table>
  <table name="deworming_history" description="History of deworming">
    <column name="uuid" description="PK" type="Varchar" size="36" required="true" primaryKey="true" sqlType="uuid"/>
    <column name="timestamp" description="Timestamp of deworming" type="Timestamp" required="true"/>
    <column name="patient_uuid" description="Patient reference" type="Varchar" size="36" required="true" sqlType="uuid"/>
    <column name="user_id" type="Integer" required="true"/>
    <column name="medication_id" type="Varchar" size="36" sqlType="uuid"/>
    <column name="comment" type="Varchar" size="255" default="NULL"/>
    <foreign-key foreignTable="patient">
      <reference foreign="uuid" local="patient_uuid"/>
    </foreign-key>
    <foreign-key foreignTable="users">
      <reference foreign="id" local="user_id"/>
    </foreign-key>
    <foreign-key foreignTable="medication">
      <reference foreign="uuid" local="medication_id"/>
    </foreign-key>
  </table>
  <table name="religion" description="Table containing religions to be assigned to patients">
    <column name="id" type="Integer" required="true" autoIncrement="true" primaryKey="true"/>
    <column name="name" description="Name of religion" type="Varchar" size="255" required="true"/>
    <column name="impact" description="Impact on medical therapy or use of vaccines" type="LongVarchar" default="NULL"/>
  </table>
  <table name="medication">
    <column name="uuid" type="Varchar" size="36" required="true" primaryKey="true" sqlType="uuid"/>
    <column name="name" type="Varchar" size="255" required="true"/>
    <column name="code" description="Codes that identify this medication e.g. SNOMED CT Medication Codes" type="Varchar" size="255" default="NULL"/>
    <column name="active" description="Identifies if this record is currently active" type="Boolean" default="0"/>
    <column name="medication_dose_form_id" type="BigInt" required="true"/>
    <column name="medication_marketing_authorization_holder_id" type="Integer"/>
    <column name="strength" description="Optional information to strength" type="Varchar" size="255" default="NULL"/>
    <foreign-key foreignTable="medication_dose_form">
      <reference foreign="id" local="medication_dose_form_id"/>
    </foreign-key>
    <foreign-key foreignTable="medication_marketing_authorization_holder">
      <reference foreign="id" local="medication_marketing_authorization_holder_id"/>
    </foreign-key>
    <behavior name="timestampable"/>
  </table>
  <table name="medication_dose_form">
    <column name="id" description="SNOMED dose code as of https://build.fhir.org/valueset-medication-form-codes.html" type="BigInt" required="true" primaryKey="true"/>
    <column name="description" description="Plain text description" type="Varchar" size="255" required="true"/>
  </table>
  <table name="medication_marketing_authorization_holder" description="Contains the manufacturer information of a medication">
    <column name="id" type="Integer" required="true" autoIncrement="true" primaryKey="true"/>
    <column name="name" type="Varchar" size="255" required="true"/>
  </table>
  <table name="medication_administration_method">
    <column name="id" type="BigInt" required="true" autoIncrement="true" primaryKey="true"/>
    <column name="description" description="SNOMED administration method code as of http://www.hl7.org/fhir/valueset-administration-method-codes.html" type="Varchar" size="255" required="true"/>
  </table>
  <table name="medication_dose_instruction">
    <column name="id" type="BigInt" required="true" autoIncrement="true" primaryKey="true"/>
    <column name="description" description="SNOMED code as of http://www.hl7.org/fhir/valueset-additional-instruction-codes.html" type="Varchar" size="255" required="true"/>
  </table>
  <table name="medical_history">
    <column name="uuid" type="Varchar" size="36" required="true" primaryKey="true" sqlType="uuid"/>
    <column name="patient_uuid" type="Varchar" size="36" required="true" sqlType="uuid"/>
    <column name="user_id" type="Integer" required="true"/>
    <column name="clinical_status" description="1 active | 2 inactive | 3 resolved" type="TinyInt" default="NULL"/>
    <column name="verification_status" description="1 unconfirmed | 2 provisional | 3 refuted" type="TinyInt" default="NULL"/>
    <column name="severity" description="1 severe | 2 moderate | 3 mild" type="TinyInt"/>
    <column name="note" type="CLOB" default="NULL"/>
    <column name="medical_diagnosis_code_id" type="BigInt"/>
    <column name="onset_date" description="Estimated or actual date" type="Date" default="NULL"/>
    <column name="onset_age" description="Estimated or actual age" type="Integer" default="NULL"/>
    <column name="abatement_date" description="When in resolution/remission" type="Date" default="NULL"/>
    <column name="abatement_age" description="When in resolution/remission" type="Integer" default="NULL"/>
    <foreign-key foreignTable="users">
      <reference foreign="id" local="user_id"/>
    </foreign-key>
    <foreign-key foreignTable="patient">
      <reference foreign="uuid" local="patient_uuid"/>
    </foreign-key>
    <foreign-key foreignTable="medical_diagnosis_code">
      <reference foreign="id" local="medical_diagnosis_code_id"/>
    </foreign-key>
    <behavior name="timestampable"/>
  </table>
  <table name="prescription">
    <column name="uuid" type="Varchar" size="36" required="true" primaryKey="true" sqlType="uuid"/>
    <column name="medication_uuid" type="Varchar" size="36" required="true" sqlType="uuid"/>
    <column name="patient_uuid" type="Varchar" size="36" required="true" sqlType="uuid"/>
    <column name="dose_quantity" type="Varchar" size="50"/>
    <column name="dose_morning" type="Boolean" default="0"/>
    <column name="dose_noon" type="Boolean" default="0"/>
    <column name="dose_evening" type="Boolean" default="0"/>
    <column name="dose_night" type="Boolean" default="0"/>
    <column name="max_dose_period" description="Defines the max dosis per period" type="Varchar" size="50" default="NULL"/>
    <column name="patient_instruction" description="Clear text instruction for patient" type="Varchar" size="255" default="NULL"/>
    <column name="medication_administration_method_id" type="BigInt"/>
    <column name="dose_start" description="Startdate of medication" type="Date" default="NULL"/>
    <column name="dose_end" description="Enddate of medication" type="Date" default="NULL"/>
    <column name="interval" description="Interval in full days" type="TinyInt" default="NULL"/>
    <column name="user_id" type="Integer" required="true"/>
    <column name="comment" description="Comment" type="Varchar" size="255" default="NULL"/>
    <foreign-key foreignTable="medication_administration_method">
      <reference foreign="id" local="medication_administration_method_id"/>
    </foreign-key>
    <foreign-key foreignTable="users">
      <reference foreign="id" local="user_id"/>
    </foreign-key>
    <foreign-key foreignTable="patient">
      <reference foreign="uuid" local="patient_uuid"/>
    </foreign-key>
    <foreign-key foreignTable="medication">
      <reference foreign="uuid" local="medication_uuid"/>
    </foreign-key>
    <behavior name="timestampable"/>
  </table>
  <table name="medical_diagnosis_code" description="Contains the diagnosis code as of http://www.hl7.org/fhir/valueset-condition-code.html">
    <column name="id" type="BigInt" required="true" autoIncrement="true" primaryKey="true"/>
    <column name="description" type="Varchar" size="255" required="true"/>
  </table>
  <table name="prescription_dose_instruction" isCrossRef="true">
    <column name="medication_dose_instruction_id" type="BigInt" required="true" primaryKey="true"/>
    <column name="prescription_uuid" type="Varchar" size="36" required="true" primaryKey="true" sqlType="uuid"/>
    <column name="quantifier_1" type="Varchar" size="255" default="NULL"/>
    <column name="quantifier_2" type="Varchar" size="255" default="NULL"/>
    <foreign-key foreignTable="medication_dose_instruction">
      <reference local="medication_dose_instruction_id" foreign="id"/>
    </foreign-key>
    <foreign-key foreignTable="prescription">
      <reference local="prescription_uuid" foreign="uuid"/>
    </foreign-key>
  </table>
  <table name="user_has_tenant" isCrossRef="true">
    <column name="tenant_uuid" type="Varchar" size="36" required="true" primaryKey="true" sqlType="uuid"/>
    <column name="user_id" type="Integer" required="true" primaryKey="true"/>
    <foreign-key foreignTable="tenant">
      <reference local="tenant_uuid" foreign="uuid"/>
    </foreign-key>
    <foreign-key foreignTable="users">
      <reference local="user_id" foreign="id"/>
    </foreign-key>
  </table>
  <table name="users" phpName="User">
    <column name="id" type="Integer" required="true" autoIncrement="true" primaryKey="true" onDelete="CASCADE"/>
    <column name="username" type="Varchar" size="255" default="NULL"/>
    <column name="display_name" type="Varchar" size="255" default="NULL"/>
    <column name="status" type="Varchar" size="255" default="NULL"/>
    <column name="status_message" type="Varchar" size="255" default="NULL"/>
    <column name="active" type="TinyInt" default="0"/>
    <column name="last_active" type="Timestamp" default="NULL"/>
    <column name="deleted_at" type="Timestamp" default="NULL"/>
    <unique name="unique_username">
      <unique-column name="username"/>
    </unique>
    <behavior name="timestampable"/>
  </table>
  <table name="auth_identities" phpName="AuthIdentity">
    <column name="id" type="Integer" required="true" autoIncrement="true" primaryKey="true"/>
    <column name="user_id" type="Integer" required="true"/>
    <column name="type" type="Varchar" size="255" required="true"/>
    <column name="name" type="Varchar" size="255" default="NULL"/>
    <column name="secret" type="Varchar" size="255" required="true"/>
    <column name="secret2" type="Varchar" size="255" default="NULL"/>
    <column name="expires" type="Timestamp" default="NULL"/>
    <column name="extra" type="LongVarchar" default="NULL"/>
    <column name="force_reset" type="TinyInt" default="0"/>
    <column name="last_used_at" type="Timestamp" default="NULL"/>
    <index name="type_secret">
      <index-column name="type"/>
      <index-column name="secret"/>
    </index>
    <foreign-key foreignTable="users" onDelete="CASCADE">
      <reference foreign="id" local="user_id"/>
    </foreign-key>
    <behavior name="timestampable"/>
  </table>
  <table name="auth_logins" phpName="AuthLogin">
    <column name="id" type="Integer" required="true" autoIncrement="true" primaryKey="true"/>
    <column name="user_id" type="Integer"/>
    <column name="ip_address" type="Varchar" size="255" default="NULL"/>
    <column name="user_agent" type="Varchar" size="255" default="NULL"/>
    <column name="id_type" type="Varchar" size="255" default="NULL"/>
    <column name="identifier" type="Varchar" size="255" default="NULL"/>
    <column name="date" type="Timestamp" required="true"/>
    <column name="success" type="TinyInt" default="0"/>
    <index name="type_identifier">
      <index-column name="id_type"/>
      <index-column name="identifier"/>
    </index>
    <foreign-key foreignTable="users" onDelete="CASCADE">
      <reference foreign="id" local="user_id"/>
    </foreign-key>
  </table>
  <table name="auth_token_logins" phpName="AuthTokenLogin">
    <column name="id" type="Integer" required="true" autoIncrement="true" primaryKey="true"/>
    <column name="ip_address" type="Varchar" size="255"/>
    <column name="user_agent" type="Varchar" size="255" default="NULL"/>
    <column name="id_type" type="Varchar" size="255" default="NULL"/>
    <column name="identifier" type="Varchar" size="255" default="NULL"/>
    <column name="date" type="Timestamp" required="true"/>
    <column name="success" type="TinyInt" default="0"/>
    <column name="user_id" type="Integer"/>
    <foreign-key foreignTable="users" onDelete="CASCADE">
      <reference foreign="id" local="user_id"/>
    </foreign-key>
  </table>
  <table name="auth_remember_tokens" phpName="AuthRememberToken">
    <column name="id" type="Integer" required="true" autoIncrement="true" primaryKey="true"/>
    <column name="selector" type="Varchar" size="255" required="true"/>
    <column name="hashedValidator" type="Varchar" size="255" required="true"/>
    <column name="expires" type="Timestamp" required="true"/>
    <column name="user_id" type="Integer" required="true"/>
    <unique name="selector">
      <unique-column name="selector"/>
    </unique>
    <foreign-key foreignTable="users" onDelete="CASCADE">
      <reference foreign="id" local="user_id"/>
    </foreign-key>
    <behavior name="timestampable"/>
  </table>
  <table name="auth_groups_users" phpName="AuthGroupsUser">
    <column name="id" type="Integer" required="true" autoIncrement="true" primaryKey="true"/>
    <column name="user_id" type="Integer" required="true"/>
    <column name="group" type="Varchar" size="255" required="true"/>
    <foreign-key foreignTable="users" onDelete="CASCADE">
      <reference foreign="id" local="user_id"/>
    </foreign-key>
    <behavior name="timestampable"/>
  </table>
  <table name="auth_permissions_users" phpName="AuthPermissionsUser">
    <column name="id" type="Integer" required="true" autoIncrement="true" primaryKey="true"/>
    <column name="permissions" type="Varchar" size="255" required="true"/>
    <column name="user_id" type="Integer" required="true"/>
    <foreign-key foreignTable="users" onDelete="CASCADE">
      <reference foreign="id" local="user_id"/>
    </foreign-key>
    <behavior name="timestampable"/>
  </table>
  <table name="settings" description="CodeIgniter Settings Table" phpName="CodeigniterSettings">
    <column name="id" type="Integer" required="true" autoIncrement="true" primaryKey="true"/>
    <column name="type" type="Varchar" size="255" required="true"/>
    <column name="key" type="Varchar" size="255" required="true"/>
    <column name="value" type="LongVarchar" default="NULL"/>
    <column name="context" type="Varchar" size="255" default="NULL"/>
    <column name="class" type="Varchar" size="255" default="NULL"/>
    <index name="search">
      <index-column name="type"/>
      <index-column name="key"/>
    </index>
    <behavior name="timestampable"/>
  </table>
</database>
